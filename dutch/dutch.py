
import sys, os, glob
from random import shuffle

file_dutch_to_eng = 'nieuw-worden.list'
file_het_words = 'het-worden.list'

#################################################################################
#################################################################################
#################################################################################
#################################################################################
def run_word_quiz(dic):
    """
    The user can play a little quiz game, where the meaning of each word is asked, and the user must
    answer with the correct meaning of the word. Upon return, he receives a feedback whether or not his
    answer was correct
    @param dic: dictionary with the whole database available as key, value pairs
    @type dic: dictionary
    @return: None
    @rtype: None
    """
    print '\n -> Welcome to the Dutch to English word quiz.'
    print '    To ask for help at anytime, press H.'
    print '    To exit gracefully, press Q.'
    print '     To leave at any point, and get your final score, press L'
    print '    To start the quiz, press Enter.'

    keys = dic.keys()
    shuffle(keys)
    n_keys = len(keys)
    i_keys = 0
    n_correct = 0
    n_wrong = 0
    score = 0

    while i_keys < n_keys:
        key = keys[i_keys]
        answer = raw_input('    {0} means? '.format(key)).lower().rstrip().lstrip()
        if answer == 'h': manual()
        if answer == 'q': goodbye()
        if answer == 'l': break

        if answer in dic[key]:
            n_correct += 1
            score = n_correct - n_wrong
            print '      Correct: {0} - {1} = {2} out of {3} tries.'.format(n_correct, n_wrong, score, i_keys+1)
        else:
            n_wrong += 1
            score = n_correct - n_wrong
            print '      Wrong: {0} - {1} = {2} out of {3} tries.'.format(n_correct, n_wrong, score, i_keys+1)
            print '      {0} means {1}'.format(key, dic[key])

        i_keys += 1

    print '\n -> Congratulations: Your score is {0} after {1} tries.'.format(score, i_keys)
    print '    Keep pushing baby, keep pushing ... ;-)\n'

    return None

#################################################################################
def run_shuffled_dictionary(dic):
    """
    The user can just rehearse with a shuffled list of the Dutch words and their meaning(s).
    @param dic: dictionary with the whole database available as key, value pairs
    @type dic: dictionary
    @return: None
    @rtype: None
    """
    print '\n -> Welcome to the Dutch to English rehearsal.'
    print '    To ask for help at anytime, press H.'
    print '    To exit gracefully, press Q.'
    print '    To keep practicing, press Enter.'

    keys = dic.keys()
    shuffle(keys)
    n_keys = len(keys)
    i_keys = 0

    while i_keys < n_keys-1:
        prompt = raw_input(' [{0}/{1}] -> '.format(i_keys+1, n_keys))
        if prompt == 'h': manual()
        if prompt == 'q': goodbye()

        key = keys[i_keys]
        print '    {0} means {1}'.format(key, dic[key])
        i_keys += 1

    return None

#################################################################################
def run_het_checkup(dic):
    """
    The user can check if a word that he enters is among those that take "het" in indefinite form, or not.
    """
    print '\n -> Welcome to the Het lookup.'
    print '    To ask for help at anytime, press H.'
    print '    To exit gracefully, press Q.'
    print '    Please either make a choice or enter the first guess:'

    keys = dic.keys()

    while True:
        query = raw_input(' -> ').lower()
        if query == 'h': manual()
        if query == 'q': goodbye()

        if query not in keys:
            print '   {0} is not a het-word; Maybe the list is not complete yet. \r'.format(query)
            print '   Enter Q/H, or the next word'
            continue
        if query in keys:
            print '   {0} is a het word'.format(query)
            print '   Enter Q/H, or the next word'

    return None

#################################################################################
def run_e_to_d_dic(dic):
    """
    The user can search for a number of Dutch words by entering an English word. This works more and less
    like synonim finder, or English to Dutch thesarus.
    @param dic: dictionary with the whole database information available. It is produced by calling e.g.
                   read_dic_database()
    @type dic: dictionary
    @return: None
    @rtype: None
    """
    print '\n -> Welcome to the English to Dutch thesarus.'
    print '       To ask for help at anytime, press H.'
    print '       To exit gracefully, press Q.'
    print '       Please either make a choice or enter the first Dutch word:'

    keys = dic.keys()
    vals = dic.values()

    while True:
        query = raw_input(' -> ').lower()
        if query == 'h': manual()
        if query == 'q': goodbye()

        answers = [val for val in vals if query in val]
        n_ans = len(answers)

        if n_ans == 0:
            print '      No word found for {0}. Try another.'
            continue
        else:
            text = '      Found {0} answers: '.format(n_ans)
            list_keys = [key for key in keys if dic[key] in answers]
            n_list = len(list_keys)
            for i in range(n_list): text += list_keys[i] + ', '
            print text

    return None

#################################################################################
def run_d_to_e_dic(dic):
    """
    The user must enter a Dutch word which already exists in the database. If so, then it is available as the
    key to the input dictionary, dic. Then, the meaning of that key is returned.
    @param dic: dictionary with the whole database information available. It is produced by calling e.g.
                   read_dic_database()
    @type dic: dictionary
    @return: None
    @rtype: None
    """
    print '\n -> Welcome to the Dutch to English word lookup.'
    print '       To ask for help at anytime, press H.'
    print '       To exit gracefully, press Q.'
    print '       Please either make a choice or enter the first Dutch word:'

    keys = dic.keys()
    print keys[0:10]


    while True:
        query = raw_input(' -> ').lower()
        if query == 'h': manual()
        if query == 'q': goodbye()

        if query not in keys:
            print '   {0} is not available yet. Is the spelling correct?'.format(query)
            print '   Enter Q/H, or the next word'
            continue

        meaning = dic[query]
        print '    {0} means {1}'.format(query, meaning)

    return None

#################################################################################
def read_het_database(filename):
    """
    Read a single column file containing the words that take "het" as indefinite article
    @param filename: full path to the file that lists the het-words, one per line
    @type filename: string
    @return: dictionary with key, being the het-words, and the value of 0
    @rtype: dictionary
    """
    if not os.path.exists(filename):
        raise SystemExit, ' Exit: read_het_database: dictionary file: {0} does not exist'.format(filename)

    dic = {}

    with open(filename, 'r') as r: lines = r.readlines()
    n_lines = len(lines)
    print '   The input database has {0} entries'.format(n_lines)
    for i, line in enumerate(lines):
        key = line.rstrip().lstrip()
        if key not in dic.keys(): dic[key] = 0

    return dic

#################################################################################
def read_dic_database(filename):
    """
    Read the Dutch to English database file
    """
    if not os.path.exists(filename):
        raise SystemExit, ' Exit: read_dic_database: dictionary file: {0} does not exist'.format(filename)

    with open(filename, 'r') as r: lines = r.readlines()
    n_lines = len(lines)
    print '   The input database has {0} entries'.format(n_lines)
    dic = {}
    for i, line in enumerate(lines):
        line = line.rstrip('\r\n').split('=')
        len_line = len(line)
        key = line[0].lstrip().rstrip()
        if len_line == 2:
            val = line[1].lstrip().rstrip()
        elif len_line > 2:
            val = ''.join(line[1:])
        else:
            print '   Warning: key "{0}" in file {1} is problematic'.format(key, filename)
            continue
        dic[key] = val

    return dic

#################################################################################
def offer_tasks():
    """
    Assign specific characters to different tasks
    """
    task = raw_input(' -> What would you like to do? ').lower()
    if task == 'd':
        return task
    if task == 't':
        return task
    if task == 'r':
        return task
    if task == 'w':
        return task
    if task == 'e':
        return task
    else:
        raise SystemExit, ' Exit: You asked for an undefined task! Please refer to help (H).'

#################################################################################
def manual():
    """
    Print out a simple manual
    """
    print
    print ' There are few key strokes that have specific meanings, and few rules:'
    print ' Q: To exit gracefully.'
    print ' H: ask for help; essentially showing this manual.'
    print ' D: Dutch to English dictionary.'
    print ' T: Check if a word you enter is among "het" word list.'
    print ' R: Rehearse Dutch to English words.'
    print ' W: Word quiz.'
    print ' E: English to Dutch lookup.'
    print
    print " We don't distinguish between lower case or upper case characters."
    print ' This saves you a lot of Shift key strokes!'
    print
    print ' I am sure there are bugs, mistakes, and issues in this first version.'
    print ' So, please be patient with them, and help me fix them.'
    print ' For any feedback, or bugs, please contact me at:'
    print '              e.moravveji at gmail dat kaam'
    print ''

    return None

#################################################################################
def goodbye():
    """
    Say goodbye, and exit
    """
    print
    print ' -> Thanks for your try. Come, visit again!'
    print

    sys.exit()

#################################################################################
def intro():
    """
    Print out the welcome message
    """
    print
    print ' ####################################'
    print ' # Dutch Vocabulary for Dummies :-D #'
    print ' ####################################'
    print
    key = raw_input(' -> Type "H" for help, or press Enter: ').lower()
    if key == 'h':
        manual()
    elif key == 'q':
        goodbye()
    else:
        key = None

    return key

#################################################################################
def main():
    """
    The main caller
    """
    result = intro()
    which_task = offer_tasks()
    if which_task == 'q':
        goodbye()
    if which_task == 'd':
        dic = read_dic_database(file_dutch_to_eng)
        run_d_to_e_dic(dic)
    if which_task == 't':
        dic = read_het_database(file_het_words)
        run_het_checkup(dic)
    if which_task == 'r':
        dic = read_dic_database(file_dutch_to_eng)
        run_shuffled_dictionary(dic)
    if which_task == 'w':
        dic = read_dic_database(file_dutch_to_eng)
        run_word_quiz(dic)
    if which_task == 'e':
        dic = read_dic_database(file_dutch_to_eng)
        run_e_to_d_dic(dic)


    return None

#################################################################################
if __name__ == '__main__':
    status = main()
    sys.exit(status)
#################################################################################

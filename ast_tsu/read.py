
import sys, os, glob
import logging
import numpy as np
from astropy.io import fits

#=================================================================================================================
c_light = 299792458.0      # speed of light in m/s 

#=================================================================================================================
def get_line_from_linelist(linelist, line_name):
  """
  This function returns a dictionary associated to one of the lines in the linelist.txt (or any other linelist). The
  name of the querried line must be available in the input file
  @param linelist: numpy record array, which is already read by read.ast_linelist().
  @type linelist: numpy record array
  @param line_name: the name of the specific line as listed in the linelist, e.g. 'He_I_6678'
  @type line_name: string
  @return: dictionary with exactly the same keys as the header of the input linelist (equivalently the column names of 
       the input linelist), and the values from that row of data
  @rtype: dictionary
  """
  linelist_names = linelist.dtype.names
  line_names = linelist['line']
  n_lines = len(linelist)
  
  if line_name not in line_names:
    logging.error('read: get_line_from_linelist: {0} field not among the available line names'.format(line_name))
    raise SystemExit
  
  ind_line = [ind for ind in range(n_lines) if linelist[ind]['line']==line_name][0]
  line_data = linelist[ind_line]
  dic_line = {}
  for name in linelist_names:
    dic_line[name] = line_data[name]
    
  return dic_line

#=================================================================================================================
def ast_linelist(filename='linelist.txt'):
  """
  This function reads in the ascii file which gives the echelle order, wavelength range (A) of some selected lines 
  of interest in addition to a blue wing and red wing around each line to be used for continuum normalization
  @param filename: full path to the AST linelist file; by default, it is located in the same directory as this module is 
  @type filename: string
  @return: numpy record array with the column names taken from the file header
  @rtype: numpy record array
  """
  if not os.path.isfile(filename): 
    logging.error('read: ast_linelist: {0} does not exist!'.format(filename))
    raise SystemExit
  
  r = open(filename, 'r')
  lines = r.readlines()
  r.close()
  
  header = lines.pop(0).split()
  dtypes = lines.pop(0).split()
  n_col  = len(header)
  str_hdr = ''
  str_dtype = ''
  for i_col in range(n_col):
    str_hdr += header[i_col] + ','
    str_dtype += dtypes[i_col] + ','
  str_hdr = str_hdr[:-1]
  str_dtype = str_dtype[:-1]
  
  recarr = []
  for line in lines: recarr.append(line.split())
  recarr = np.core.records.fromarrays(np.asarray(recarr).transpose(), names=str_hdr, formats=str_dtype)
  
  return recarr

#=================================================================================================================
def multiple_fits(list_files, orders=[]):
  """
  This function receives a list of filenames and returns a list of dictionary with the fits data stored into the 
  dictionary.
  @param list_files: list of full path to the individual fits files to be read. Reading is performed by calling the 
      "single_fits" routine
  @type list_files: list of string
  @param orders: to only read a subset of echelles, you can specify the orders of interest and pass them as a list.
     default = []
  @type orders: list of integers
  @return: list of dictionaries
  @rtype: list of dictionaries
  """
  n_files = len(list_files)
  if n_files == 0:
    logging.error('read: multiple_fits: Input list is empty!')
    raise SystemExit
  
  list_dic = []
  for i_file, fits_file in enumerate(list_files):
    if not os.path.isfile(fits_file): 
      logging.error('read: multiple_fits: {0} does not exist'.format(fits_file))
      raise SystemExit
    list_dic.append(single_fits(fits_file, orders=orders))
  logging.info('read: multiple_fits: successfully read {0} files'.format(n_files))
  print ' - read: multiple_fits: successfully read {0} files'.format(n_files)
  
  return list_dic
  
#=================================================================================================================
def single_fits(filename, orders=[]):
  """
  Receives the full path to a Python-Compatible AST spectra, and returns the content in the form of a dictionary.
  Note that the output wavelengths per each echelle are corrected for the Earth radial velocity
  @param filename: full path to the fits file
  @type filename: string
  @param orders: to only read a subset of echelles, you can specify the orders of interest and pass them as a list.
     default = []
  @type orders: list of integers
  @return: dictionary with the header information as keys, and with the spectra data content given with the key: data
  @rtype: dictionary
  """
  if not os.path.isfile(filename):
    logging.error('read: single_fits: {0} does not exist'.format(filename))
    raise SystemExit
  
  data   = fits.getdata(filename, 0, header=False)
  
  hdu = fits.open(filename)
  hdu.verify('silentfix')
  header = hdu[0].header
  hdu.close()
  
  header_keys = header.keys()
  bad_keys = ['ORIGIN', 'TEMP_ECH']
  for bad_key in bad_keys:
    ind_bad = [ind for ind in range(len(header_keys)) if header_keys[ind]==bad_key][0]
    header_keys.pop(ind_bad)

  n_header = len(header_keys)

  dic = {}
  for i_key, key in enumerate(header_keys):
    dic[key.lower()] = header[key]
  
  if orders:
    list_orders = []
    n_orders = len(orders)
    for order in orders: list_orders.append(data[order, :])
    data = np.asarray(list_orders)
  
  dic['data'] = data
 
  n_pixel     = dic['naxis1']
  n_echelle   = dic['naxis2']
  pix_array   = np.arange(0, n_pixel, 1)
  
  if orders:
    wavelength = np.zeros((n_orders, n_pixel))
    list_echelle = orders
  else: 
    wavelength = np.zeros((n_echelle, n_pixel))
    list_echelle = range(n_echelle)
  
  i_row = 0
  coeff = np.zeros(4)
  for i_echelle in list_echelle:
    key_echelle = 'wp_%02d' % (i_echelle, )
    list_coeff = dic[key_echelle].split()
    for i in range(4): coeff[i] = float(list_coeff[i])
    dic[key_echelle] = coeff
    wavelength[i_row, :] = coeff[0] + coeff[1] * pix_array[:] + coeff[2] * pix_array[:]**2. + coeff[3] * pix_array[:]**3.
    i_row += 1

  rv_tel = dic['rv_tel']          # m/s
  corr_factor = np.sqrt((1.+rv_tel/c_light)/(1.-rv_tel/c_light))
  dic['wavelength'] = wavelength / corr_factor
    
  return dic

#=================================================================================================================

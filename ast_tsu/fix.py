
import sys, os, glob
import logging
import numpy as np
from lmfit import minimize, Parameters, Parameter, report_fit
from copy import deepcopy

#=================================================================================================================
def remove_cosmic_hits(x, y):
  """
  """
  sigma = np.std(y)
  sigma_2 = 2. * sigma
  sigma_3 = 3. * sigma
  n = len(y)
  
  def find_cubic(params, x_arr, real_prof):
    """ Fit a polynomial of order 3 to the line profile where there is a cosmic hit """
    a0 = params['a0'].value
    a1 = params['a1'].value
    a2 = params['a2'].value
    a3 = params['a3'].value
    model = a0 + a1*x_arr + a2*x_arr**2. + a3*x_arr**3.
    return model - real_prof
  
  params = Parameters()
  params.add('a0', value=0, vary=True)  
  params.add('a1', value=0, vary=True)
  params.add('a2', value=0, vary=True)
  params.add('a3', value=0, vary=True)

  for i in range(2,n-2):
    if y[i] >= sigma_3:
      full_x  = x[i-5:i+7]
      left_x  = x[i-5:i-1]
      left_y  = y[i-5:i-1]
      right_x = x[i-5:i-1]
      right_y = y[i+3:i+7]
      merge_x = np.hstack((left_x, right_y))
      merge_y = np.hstack((left_y, right_y))

      # the most simplistic approach
      y[i] = np.mean(y[i-2:i+3])
      
      #interp = interp1d(merge_x, merge_y, kind='cubic')
      #y[i-5:i+7] = interp(x[i-5:i+7])
      #y[i] = np.mean(y[i-2:i+3])
      
      ## Now, get the minimized parameters for fitting the line profile around the jump with a cubic spline
      #spline_out = minimize(find_cubic, params, args=(merge_x, merge_y))
      ## improved parameters
      #a0 = params['a0'].value
      #a1 = params['a1'].value
      #a2 = params['a2'].value
      #a3 = params['a3'].value
      ##new_line = a0 + a1*full_x + a2*full_x**2. + a3*full_x**3.
      ##y[i-2:i+3] = new_line[i-2:i+3]

  return y

#=================================================================================================================
def detrend_one_profile(dic_in, use_poly3=False, use_poly4=False, use_poly5=False):
  """
  This function performs a Marquart-Levenberg Least-Square fit to both sides of the line in the blue and red wing, 
  and returns the best fitting coefficients to construct a global "polynomial" fit to the blue and red wings of the 
  line. Then, the trend in the continuum is constructed, and the raw line profile is devided by this trend to "detrend"
  one line profile and normalize it to the unity.
  @param dic_in: the line and wing data and specifications are passed using this input dictionary
  @type dic_in: dictionary
  @param use_poly3, use_poly4, use_poly5: True/False, specifying the order of the polynomial to fit to the data
  @type use_poly3, use_poly4, use_poly5: Boolean
  @return: several new key,value pairs are added to the input dictionary, and the updated dictionary is returned.
     The new keys are:
     - coarse_lambda: wavelengths of the coarse points in the blue and red wings of the line
     - coarse_prof: profile of the coarse points in the blue and red wings of the line
     - lambda: original line wavelength
     - profile: original line profile
     - fit_prof: fitted polynomial to the coarse blue and red wings of the line
     - full_lambda: the composite wavelength array of the whole line from the bluest blue to the redest red
     - continuum: the trend in the raw line profile across the whole wavelength range (i.e. full_lambda); this is 
                  later used to detrend the full line profile by dividing the line by this trend
     - detr_prof: the final detrended full line profile
  @rtype: dictionary   
  """
  if not any([use_poly3, use_poly4, use_poly5]):
    message = 'Error: fix: detrend_one_profile: Choose one of the use_poly3/4/5 '
    raise SystemExit, message
  
  required_keys = ['lambda_line', 'prof_line', 'lambda_b_wing', 'prof_b_wing', 'lambda_r_wing', 'prof_r_wing']
  dic_keys = dic_in.keys()
  for i_name, name in enumerate(required_keys):
    if name not in dic_keys:  
      message = 'Error: fix: detrend_one_profile: %s not in required input keys' % (name, )
      raise SystemExit, message
  
  lambda_line   = dic_in['lambda_line']
  prof_line     = dic_in['prof_line']
  lambda_b_wing = dic_in['lambda_b_wing']
  prof_b_wing   = dic_in['prof_b_wing']
  lambda_r_wing = dic_in['lambda_r_wing']
  prof_r_wing   = dic_in['prof_r_wing']
  
  # Smooth for Cosmic Ray Hits
  prof_b_wing = remove_cosmic_hits(lambda_b_wing, prof_b_wing)
  prof_line   = remove_cosmic_hits(lambda_line, prof_line)
  prof_r_wing = remove_cosmic_hits(lambda_r_wing, prof_r_wing)
  
  # Combine the blue and the red wing with the line to make a composite line
  full_lambda   = np.hstack((lambda_b_wing, lambda_line, lambda_r_wing))
  full_prof     = np.hstack((prof_b_wing, prof_line, prof_r_wing))
  
  # start sampling the blue wing with a very coarse resolution
  n_b = len(lambda_b_wing)
  list_b_lambda = []
  list_b_prof   = []
  list_b_lambda.append(lambda_b_wing[0])            # consider the first three elements
  list_b_prof.append(np.mean(prof_b_wing[0:3]))
  for i_b in range(3, n_b-3, 7):
    list_b_lambda.append(np.mean(lambda_b_wing[i_b-3 : i_b+3]))
    list_b_prof.append(np.mean(prof_b_wing[i_b-3 : i_b+3]))
  
  # start sampling the red wing with a very coarse resolution
  n_r = len(lambda_r_wing)
  list_r_lambda = []
  list_r_prof   = []
  for i_r in range(3, n_r-3, 7):
    list_r_lambda.append(np.mean(lambda_r_wing[i_r-3 : i_r+3]))
    list_r_prof.append(np.mean(prof_r_wing[i_r-3 : i_r+3]))
  list_r_lambda.append(np.mean(lambda_r_wing[-1]))
  list_r_prof.append(np.mean(prof_r_wing[-3:-1]))
  
  # combine the blue and wing coarse points, and make one array for the wavelength and the line profile
  coarse_lambda = np.asarray(list_b_lambda + list_r_lambda)
  coarse_prof   = np.asarray(list_b_prof   + list_r_prof)

  def deviate_data_model(params, x, data):
    """
    model is a polynomial of order 3, 4 or 5
    """
    if use_poly3:
      offset = params['offset'].value
      a1     = params['a1'].value
      a2     = params['a2'].value
      a3     = params['a3'].value
      model  = offset + a1*x + a2*x**2. + a3*x**3.
      return model - data
      #fit_coeff, covar_matrx = curve_fit(poly_3, coarse_lambda, coarse_prof, p0=coeff)
      
    elif use_poly4:
      offset = params['offset'].value
      a1     = params['a1'].value
      a2     = params['a2'].value
      a3     = params['a3'].value
      a4     = params['a4'].value
      model  = offset + a1*x + a2*x**2. + a3*x**3. + a4*x**4.
      return model - data
    
    elif use_poly5:
      offset = params['offset'].value
      a1     = params['a1'].value
      a2     = params['a2'].value
      a3     = params['a3'].value
      a4     = params['a4'].value
      a5     = params['a5'].value
      model  = offset + a1*x + a2*x**2. + a3*x**3. + a4*x**4.
      return model - data
    else:
      message = 'Error: fix: detrend_one_profile: Something wrong with the choice of polynomial callable: poly_'
      raise SystemExit, message

  params = Parameters()
  params.add('offset', value=1, vary=True)  
  params.add('a1', value=1e-2, vary=True)
  params.add('a2', value=1e-2, vary=True)
  params.add('a3', value=1e-2, vary=True)
  params.add('a4', value=1e-2, vary=True)
  params.add('a5', value=1e-2, vary=True)
  
  # do fit, here with leastsq model
  fit_result = minimize(deviate_data_model, params, args=(coarse_lambda, coarse_prof))

  # calculate final result
  fit_prof = coarse_prof + fit_result.residual
  
  # write error report
  #report_fit(params)
  
  # Add the coarse blue and red wings and the interpolated profile to the input dictionary
  dic_in['coarse_lambda'] = coarse_lambda
  dic_in['coarse_prof']   = coarse_prof
  dic_in['lambda']        = lambda_line
  dic_in['profile']       = prof_line
  dic_in['fit_prof']      = fit_prof

  # Now, apply the best fit parameters to the whole lambda range from blue to the red
  # to find the theoretical continuum
  offset = params['offset'].value  
  a1     = params['a1'].value
  a2     = params['a2'].value
  a3     = params['a3'].value
  th_cntnum = offset + a1*full_lambda + a2*full_lambda**2. + a3*full_lambda**3.
  if use_poly3:
    th_cntnum = th_cntnum
  elif use_poly4:
    a4 = params['a4'].value
    th_cntnum += a4*full_lambda**4.
  elif use_poly5:
    a4 = params['a4'].value
    a5 = params['a5'].value
    th_cntnum += a4*full_lambda**4. + a5*full_lambda**5.
  
  dic_in['full_lambda'] = full_lambda
  dic_in['continuum']   = th_cntnum
  
  detrended_profile = full_prof / th_cntnum
  dic_in['detr_prof'] = detrended_profile

  return dic_in
  
#=================================================================================================================
def detrend_all_profiles(list_dic, dic_line, echelle=0):
  """
  Choose a line and detrend it from all data by fitting a polynomial to it per each spectra. This is done individually
  by calling fix.detrend_one_profile(). 
  @param list_dic: all spectra data are passed as a list of dictionaries
  @type list_dic: list of dictionary
  @param dic_line: dictionary containing the line information, as returned by e.g. read.get_line_from_linelist()
  @type dic_line: dictionary
  @param echelle: the specific echelle order - among the whole available orders - for detrending. 
  @type echelle: integer
  @return: tuple: the first is the list of dictionaries where each line profile is substituted by the detrended profile
     at exactly the same pixel number, and the second is the list of dictionaries containing the detrending scheme 
     and interpolation values and arrays
  @rtype: tuple
  """
  n_dic = len(list_dic)
  if n_dic == 0:
    logging.error('fix: detrend_all_profiles: Input list is empty')
    raise SystemExit
  
  first_dic = list_dic[0]
  first_lambda = first_dic['wavelength'][echelle]
  if dic_line['line_from'] < first_lambda[0] or dic_line['line_from'] > first_lambda[-1]:
    logging.error('fix: detrend_all_profiles: the line not in the echelle={0}'.format(echelle))
    raise SystemExit
  
  new_list_dic = []
  list_dic_interp = []
  for i_dic, dic in enumerate(list_dic):
    dic_copy     = {}
    dic_copy     = deepcopy(dic)
    wavelength   = dic_copy['wavelength'][echelle]
    line_profile = dic_copy['data'][echelle]
    
    ind_line_from = np.argmin(np.abs(wavelength-dic_line['line_from']))
    ind_line_to   = np.argmin(np.abs(wavelength-dic_line['line_to']))
    ind_b_from    = np.argmin(np.abs(wavelength-dic_line['b_wing_from']))
    ind_b_to      = np.argmin(np.abs(wavelength-dic_line['b_wing_to']))
    ind_r_from    = np.argmin(np.abs(wavelength-dic_line['r_wing_from']))
    ind_r_to      = np.argmin(np.abs(wavelength-dic_line['r_wing_to']))

    lambda_line   = wavelength[ind_line_from : ind_line_to]
    prof_line     = line_profile[ind_line_from : ind_line_to]
    lambda_b_wing = wavelength[ind_b_from : ind_b_to]
    prof_b_wing   = line_profile[ind_b_from : ind_b_to]
    lambda_r_wing = wavelength[ind_r_from : ind_r_to]
    prof_r_wing   = line_profile[ind_r_from : ind_r_to]
    
    dic_pre_corr = {'lambda_line':lambda_line, 'prof_line':prof_line, 
                    'lambda_b_wing':lambda_b_wing, 'prof_b_wing':prof_b_wing, 
                    'lambda_r_wing':lambda_r_wing, 'prof_r_wing':prof_r_wing}
    dic_interp_prof = detrend_one_profile(dic_pre_corr, use_poly4=True)
    detrended_profile = dic_interp_prof['detr_prof']

    line_profile[ind_b_from : ind_r_to] = detrended_profile[:]
    #dic_copy['data'][echelle, ind_b_from : ind_r_to] = detrended_profile[:]
    new_list_dic.append(dic_copy)
    list_dic_interp.append(dic_interp_prof)
  
  return new_list_dic, list_dic_interp      
  
#=================================================================================================================
def merge_one_line_two_orders(list_dic, dic={}):
  """
  This function merges the same line across two consequitive echelle orders into one line
  """

#=================================================================================================================
def exclude_low_snr(list_dic, low_snr=50, echelle=0, start=False, end=False):
  """"""
  n_dic = len(list_dic)
  if n_dic == 0:
    logging.error('fix: exclude_low_snr: Input list is empty')
    raise SystemExit

  if not all([start, end]):
    logging.error('fix: exclude_low_snr: You must specify both start and end keywords')
    raise SystemExit
  
  first_dic = list_dic[0]
  #first_lambda = first_dic['wavelength'][echelle, : ]
  first_lambda = first_dic['wavelength'][echelle]
  
  if start >= end:
    logging.error('Error: fix: exclude_low_snr: start >= end: as {0} >= {1}'.format(start, end))
    raise SystemExit
  if start < first_lambda[0] or end > first_lambda[-1]:
    message = 'Error: fix: exclude_low_snr: start (%s) OR end (%s) outside wavelength range of echelle: %s' % (start, end, echelle)
    print 'available range is: start={0} to end={1} A'.format(first_lambda[0], first_lambda[-1]) 
    raise SystemExit, message
  
  updated_list_dic = eval_snr(list_dic, echelle=echelle, start=start, end=end)

  arr_snr = np.zeros(n_dic)
  for i_dic, dic in enumerate(updated_list_dic):
    arr_snr[i_dic] = dic['snr']

  #ind_exclude = [ind for ind in range(n_dic) if arr_snr[ind]<=low_snr][0]
  ind_exclude = np.where((arr_snr<=low_snr))[0]
  num_exclude = len(ind_exclude)
  ind_maintain= np.where((arr_snr>low_snr))[0]
  #ind_maintain= [ind for ind in range(n_dic) if arr_snr[ind]>low_snr][0]
  
  if num_exclude>0:
    print '\n - fix: exclude_low_snr: %s spectra are excluded below SNR<%s' % (num_exclude, low_snr)
    print '   Remaining number of spectra = %s' % (len(ind_maintain), )
  
    new_list_dic = []
    for ind in ind_maintain: new_list_dic.append(updated_list_dic[ind])
    return new_list_dic
  
  else:
    print '\n - fix: exclude_low_snr: No spectra excluded.'
    print '   Total number of spectra = %s ' % (len(updated_list_dic), )

    return updated_list_dic

#=================================================================================================================
def eval_snr(list_dic, echelle=0, start=False, end=False):
  """
  Evaluate SNR for each dictionary of the data in the input list, focused on the echelle, and in the prescribed 
  wavelength range
  @param list_dic: list of dictionaries of the fits data
  @type list_dic: list of dictionaries
  @param echelle: (default=0); specify the row in the input data, not necessary the Echelle order of the raw data
  @type echelle: integer
  @param start, end: specify the wavelength range (A) for the evaluation of SNR
  @type start, end: float
  @return: a new key:value pair of 'snr' will be added to each dictionary in the list, and the updated list of dictionaries 
     is returned
  @rtype: list of dictionaries
  """
  n_dic = len(list_dic)
  if n_dic == 0:
    logging.error('fix: eval_snr: Input list is empty')
    raise SystemExit
  
  if not all([start, end]):
    logging.error('fix: eval_snr: You must specify both start and end keywords')
    raise SystemExit
  
  first_dic = list_dic[0]
  first_lambda = first_dic['wavelength'][echelle]
  
  if start >= end:
    logging.error('fix: eval_snr: start >= end: as {0} >= {1}'.format(start, end))
    raise SystemExit
  if start < first_lambda[0] or end > first_lambda[-1]:
    logging.error('fix: eval_snr: start ({0}) OR end ({1}) outside wavelength range of echelle = {2}'.format(start, end, echelle))
    raise SystemExit
  
  arr_snr = np.zeros(n_dic)
  arr_exp = np.zeros(n_dic)
  for i_dic, dic in enumerate(list_dic):
    wavelength = dic['wavelength'][echelle]
    n_pixel    = len(wavelength)
    #ind_lambda = np.where((wavelength >= start and wavelength <= end))
    ind_lambda = [i for i in range(n_pixel) if wavelength[i]>=start and wavelength[i]<=end]
    wavelength = wavelength[ind_lambda]
    data       = dic['data'][echelle, ind_lambda]
    
    mean       = np.mean(data)
    std_dev    = np.std(data)
    snr        = mean/std_dev
    arr_snr[i_dic] = snr
    arr_exp[i_dic] = dic['exposure']
    
    dic['snr'] = snr
    
  ind_min      = np.argmin(arr_snr)
  ind_max      = np.argmax(arr_snr)
  print '\n - fix.eval_snr():'
  print '   Min(SNR)  = {0:.2f}; exposure = {1:.0f} sec'.format(arr_snr[ind_min], arr_exp[ind_min])
  print '   Max(SNR)  = {0:.2f}; exposure = {1:.0f} sec'.format(arr_snr[ind_max], arr_exp[ind_max])
  print '   Mean(SNR) = {0:.2f}; exposure = {1:.0f} sec'.format(np.mean(arr_snr), np.mean(arr_exp))
  
  return list_dic
  
#=================================================================================================================

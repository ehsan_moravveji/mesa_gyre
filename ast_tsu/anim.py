
import sys, os, glob
import logging
import numpy as np
import pylab as plt
import images2gif
import PIL
import plot

#=================================================================================================================
#=================================================================================================================
def gen_gif_from_line(list_dic, echelle=0, gif_name='Sample.gif'):
  """
  images2gif has a nice feature to generate gif files from numpy arrays. We use that feature.
  @param list_dic: list containing the spectra data
  @type list_dic: list of dictionaries
  @param echelle: specify which (sub)order to pick for plotting.
  @type echelle: integer
  @param gif_name: full path to the output .gif file. Make sure it has the .gif extension, or the program exits.
  @type gif_name: string
  @return: None
  @rtype: None
  """
  n_dic = len(list_dic)
  if n_dic == 0: 
    logging.error('Error: anim: gen_gif_from_line: Input list is empty')
    return None
  
  return None

#=================================================================================================================
def gen_frame_sequence(list_dic, echelle=0, line_info=None, dir_out=None):
  """
  Generate a frame per each line from a spectra, and store them all at the dir_out address. plotting is carried out
  by calling plot.gen_line_frame(), iteratively; So, read that routine as well.
  @param list_dic: list containing the spectra data
  @type list_dic: list of dictionaries
  @param echelle: specify which (sub)order to pick for plotting.
  @type echelle: integer
  @param line_info: 1-row numpy record array passing the line specification; it is generically used by reading a 
      linelist, and returned by read.ast_linelist()
  @param dir_out: full path to where the files will be dumped
  @tyep dir_out: string
  @return: None
  @rtype: None
  """
  if dir_out is None:
    logging.error('Error: anim: gen_frame_sequence: You MUST specify dir_out')
    raise SystemExit
  if dir_out[-1] != '/': dir_out += '/'
  if not os.path.exists(dir_out): os.makedirs(dir_out)
  
  for i_dic, dic in enumerate(list_dic):
    fig  = plt.figure(figsize=(3, 2), dpi=60)
    axis = fig.add_subplot(111)
    frame_number = '{0:04d}'.format(i_dic)
    if line_info is None: line_name = 'line'
    else: line_name = line_info['line']
    mjd  = '{0:.5f}'.format(dic['hjd'] - 2400000.0)
    frame_name = dir_out + 'Seq-' + line_name + '-' + mjd + '.png'
    axis = plot.gen_line_frame(dic, ax=axis, echelle=echelle, line_info=line_info, file_out=frame_name)

  return None

#=================================================================================================================
  
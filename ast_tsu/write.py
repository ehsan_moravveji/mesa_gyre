import numpy as np
import glob, os

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================
def line_to_ascii(list_dic, echelle=0, line_info=None, start=None, end=None, prefix=None, dir_out=None):
  """
  """
  if dir_out is None:
    message = 'Error: write: line_to_ascii: Output directory is not specified!'
    raise SystemExit, message
  
  if not all([start, end]) and line_info is None:
    message = 'Error: write: line_to_ascii: Specify either line_info or start/end wavelength in Angstrom'
    raise SystemExit, message
  if line_info is not None:
    start = line_info['line_from']
    end  = line_info['line_to']
  
  if prefix is None: prefix = 'Line-'
  if dir_out[-1] != '/': dir_out += '/'
  if not os.path.exists(dir_out): os.makedirs(dir_out)
  extension = '.txt'
  
  n_dic = len(list_dic)
  info = []
  
  for i_dic, dic in enumerate(list_dic):
    wavelength   = dic['wavelength'][echelle, :]
    line_profile = dic['data'][echelle, :]
    hjd          = dic['hjd'] 
    mjd          = hjd - 2400000.0
    str_mjd     = str(mjd) 

    if end < start:
      message = 'Error: write: line_to_ascii: end < start'
      raise SystemExit, message
    
    if start < wavelength[0] or start > wavelength[-1] or end < wavelength[0] or end > wavelength[-1]:
      message = 'Error: write: line_to_ascii: the start AND/OR end do not fall in the wavelength range!'
      raise SystemExit, message

    ind_from = np.argmin(np.abs(wavelength - start))
    ind_to   = np.argmin(np.abs(wavelength - end))
    
    lines = []
    nl    = ' \n'
    for i_ind in range(ind_from, ind_to+1, 1):
      line = '%0.6f    %0.4f %s' % (wavelength[i_ind], line_profile[i_ind], nl)
      lines.append(line)
     
    filename  = prefix + str_mjd + extension
    full_path = dir_out + filename
    w = open(full_path, 'w')
    w.writelines(lines)
    w.close()
    
    info.append('%0.6f    %s %s' % (mjd, filename, nl))
  
  filename = dir_out + prefix[:-1] + '.info'   
  w = open(filename, 'w')
  w.writelines(info)
  w.close()
    
  print '\n - write: line_to_ascii: Output format: %s' % (dir_out + prefix + '*' + extension, ) 
  
  return 
#=================================================================================================================
  
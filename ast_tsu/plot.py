
import sys, os, glob
import logging
import numpy as np
import read
import matplotlib
import pylab as plt

clight = 2.99792458e5   # km/sec
#=================================================================================================================
def gen_line_frame(dic, ax=None, echelle=0, line_info=None, file_out=None):
  """
  Plot a specific line per each spectra, mainly useful to stack them later for creating a movie
  @param dic: dictionary with the spectra information
  @type dic: dictionary
  @param ax: matplotlib axis, if not available, will be defined on a fly
  @type ax: axis
  @param echelle: specify which row in the input spectra to use. It is not essentially the original echelle order
  @type echelle: integer
  @param file_out: full path to the output plot file, it not specified, no plot will be saved on disk.
  """
  ####################################################
  # Prepare the plot  
  ####################################################
  if ax is None:
    fig = plt.figure(figsize=(4.5, 3), dpi=60)
    ax  = fig.add_subplot(111)
    plt.subplots_adjust(left=0.25, right=0.98, bottom=0.30, top=0.97)

  xaxis = dic['wavelength'][echelle]
  xaxis = clight * (xaxis - line_info['lambda_0']) / line_info['lambda_0']
  yaxis = dic['data'][echelle]
  
  if line_info is None:
    print ' - plot: gen_line_frame: Uses the whole echelle range'
    ax.set_xlim(np.min(xaxis), np.max(xaxis))
  else:
    v_blue = clight * (line_info['line_from'] - line_info['lambda_0']) / line_info['lambda_0']
    v_red  = clight * (line_info['line_to']   - line_info['lambda_0']) / line_info['lambda_0']
    ax.set_xlim(v_blue, v_red)
  ax.set_ylim(0.5, 1.1)
  ax.set_yticklabels(())

  ax.axhline(1.0, linestyle='solid', color='black', lw=0.5)
  ax.plot(xaxis, yaxis, linestyle='solid', color='blue', lw=1.5)  
    
    
  if file_out:
    plt.savefig(file_out)
    plt.close()
  
  return ax   
    
#=================================================================================================================
def Journal(fits_name, file_out=None):
  """
  Plot the whole echelle orders for one fits file as a multiple-page plot. This is nice to show where each line is.
  @param fits_name: full path to a fits file to be plotted
  @type fits_name: string
  @file_out: full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  if not os.path.isfile(fits_name):
    logging.error('plot: Journal: {0} does not exist'.format(fits_name))
    raise SystemExit
  if file_out is None:
    logging.error('plot: Journal: Must specify an output plot filename in "pdf" format')
    raise SystemExit
  ind_point = file_out.rfind('.')
  if file_out[ind_point:] != '.pdf':
    logging.error('plot: Journal: Must specify a "PDF" file name')
    raise SystemExit
  
  ####################################################
  # Prepare the plot  
  # Prepare Pages
  ####################################################
  from matplotlib.backends.backend_pdf import PdfPages
  with PdfPages(file_out) as pdf: 
    dic    = read.single_fits(fits_name, orders=None)
    data   = dic['data']
    wave   = dic['wavelength']
    n_ord  = dic['naxis2'] 
    ord_per_page = 8
    n_page = int(n_ord/ord_per_page)
    if n_ord % ord_per_page > 0: n_page += 1
    #print type(wave), type(wave[0])
    #print np.shape(wave), np.shape(wave[0])
    #print np.shape(data)
    #sys.exit()

    for i_page in range(n_page):
      print ' - plot: Journal: Creating page {0} of {1}'.format(i_page+1, n_page)
      fig = plt.figure(figsize=(8.3,11.7)) # A4
      plt.subplots_adjust(left=0.05, right=0.98, bottom=0.05, top=0.98, hspace=0.20, wspace=0.05)
      
      for i_panel in range(ord_per_page):
        ax = fig.add_subplot(ord_per_page, 1, i_panel+1)
        i_ord = i_page * ord_per_page + i_panel
        xaxis = wave[i_ord]
        yaxis = data[i_ord]
        ax.plot(xaxis, yaxis, linestyle='solid', lw=1, color='black')
        ax.set_xlim(np.min(xaxis), np.max(xaxis))
        ax.set_ylim(0.6, 1.2)
        ax.annotate(r'{0}'.format(i_ord), xy=(0.04,0.80), xycoords='axes fraction', 
                    fontsize='xx-large', color='purple')
        
      pdf.savefig()
      plt.close()
      
    import datetime  
    info = pdf.infodict()
    info['Title']   = 'Full AST Spectra for HD {0}'.format(dic['object'])
    info['Author']  = r'Ehsan Moravveji'
    info['Subject'] = r'AST Echelle Spectra: {0} sec'.format(dic['exposure'])
    #info['CreationDate'] = r'{0}'.format(dic['date-obs'])
    info['ModDate'] = datetime.datetime.today()
  
  print ' - plot: Journal: {0} saved'.format(file_out)
  print '         It has {0} pages, and {1} panels per peage!'.format(n_page, ord_per_page)
  
  return None
  
#=================================================================================================================
def interpolated_prof(list_dic, file_out):
  """
  Plot the interpolated blue and wing profiles
  """
  first_dic = list_dic[0]
  coarse_lambda = first_dic['coarse_lambda']
  coarse_prof   = first_dic['coarse_prof']
  orig_lambda   = first_dic['lambda']
  orig_prof     = first_dic['profile']
  fit_prof      = first_dic['fit_prof']
  lambda_b_wing = first_dic['lambda_b_wing']
  prof_b_wing   = first_dic['prof_b_wing']
  lambda_r_wing = first_dic['lambda_r_wing']
  prof_r_wing   = first_dic['prof_r_wing']
  full_lambda   = first_dic['full_lambda']
  continuum     = first_dic['continuum']
  detrended_prof= first_dic['detr_prof']
  
  ####################################################
  # Prepare the plot  
  ####################################################
  fig = plt.figure(figsize=(6,4))
  ax = fig.add_subplot(111)
  plt.subplots_adjust()   
  
  ax.hlines(1.0, xmin=lambda_b_wing[0]-0.5, xmax=lambda_r_wing[-1]+0.5)
  ax.plot(lambda_b_wing, prof_b_wing, color='blue', linestyle='dashed', lw=1, label=r'Raw, B Continuum')
  ax.plot(lambda_r_wing, prof_r_wing, color='red', linestyle='dashed', lw=1, label=r'Raw, R Continuum')  
  ax.plot(orig_lambda, orig_prof, color='grey', linestyle='dashed', lw=2, label=r'Raw, Line')
  ax.scatter(coarse_lambda, coarse_prof, s=40, marker='s', facecolor='blue', edgecolor='red', alpha=0.5, 
             label=r'Raw, Continuum')
  ax.plot(full_lambda, continuum, linestyle='solid', color='green', lw=2, label=r'Trend')
  ax.plot(full_lambda, detrended_prof, linestyle='solid', color='black', lw=1, label=r'Detrended Line')
  leg = ax.legend(loc=4, fontsize='x-small')
  ax.set_xlim(lambda_b_wing[0]-0.5, lambda_r_wing[-1]+0.5)
  if file_out:
    plt.savefig(file_out)
    print '\n - plot: interpolated_prof: %s Created.' % (file_out, )
    
  return ax
  
#=================================================================================================================
def line(list_dic, echelle=None, line_info=None, from_lambda=None, to_lambda=None, yaxis_from=None, yaxis_to=None, 
         file_out=None):
  """
  This function plots all spectra from a specified order index in a specified wavelength range
  @param list_dic: list of dictionaries with the data, for every fits file. Note that the echelle number is not 
     essentially that in the original data, as a lot of orders are useless.
     For available keys in each dictionary, refer to ast.read.single_fits()
  @type list_dic: list of dictionaries
  @param line_info: a 1-row record array giving the line specifications. it is read and returned by e.g. read.ast_linelist()
     setting from_lambda and to_lambda will override this.
  @type line_info: numpy record array
  @return: a plot axis
  @rtype: matplotlib plot
  """
  n_dic = len(list_dic)
  if n_dic == 0:
    message = 'Error: plot: line: input list is empty'
    raise SystemExit, message
  if echelle is None:
    raise SystemExit, 'Error: plot: line: You must specify the echelle of interest'
  
  ####################################################
  # Prepare the plot  
  ####################################################
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.06, right=0.98, bottom=0.12, top=0.98)

  first_dic = list_dic[0]
  first_wavelength = first_dic['wavelength'][echelle,:]
  
  if line_info is not None:
    xaxis_from = line_info['b_wing_from']
    xaxis_to   = line_info['r_wing_to']
  if from_lambda:
     xaxis_from = from_lambda
  else:
     xaxis_from = first_wavelength[0]
  if to_lambda:
     xaxis_to = to_lambda
  else:
     xaxis_to = first_wavelength[-1]
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_xlabel(r'Wavelength $\lambda$ [$\AA$]')
  if not yaxis_from: yaxis_from = 0.70
  if not yaxis_to:   yaxis_to = 1.05
  ax.set_ylim(yaxis_from, yaxis_to)
  #ax.set_ylabel(r'Normalized Flux $F_\lambda$')

  ####################################################
  # Start looping over dictionaries and do the plotting
  ####################################################
  ax.hlines(1.0, xmin=xaxis_from, xmax=xaxis_to)
  for i_dic, dic in enumerate(list_dic):
    wavelength = dic['wavelength'][echelle, :]
    line       = dic['data'][echelle, :]
    
    ax.plot(wavelength, line, linestyle='solid', color='grey', lw=0.5)    
    
  if file_out:
    plt.savefig(file_out, dpi=200)
    print ' - plot: line: %s Created!' % (file_out, )
    plt.close()
  
  return ax
  
#=================================================================================================================
def all_echelle(filename, xaxis_from=None, xaxis_to=None, yaxis_from=None, yaxis_to=None, file_out=None):
  """
  Plot all echelle on the same canvas!
  @param filename: full path to a single filename to be read by read.single_fits(), and plotted
  @type filename: string
  @return: matplotlib axis
  @rtype: axis object
  """
  if not file_out: return None
  dic = read.single_fits(filename)
  all_wave = dic['wavelength']
  all_data = dic['data']
  n_ord, n_pix = np.shape(all_wave)
  
  ####################################################
  # Prepare the plot  
  ####################################################
  fig = plt.figure(figsize=(20,4), dpi=600)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.04, right=0.99, bottom=0.15, top=0.96)
  cmap = plt.get_cmap('seismic')
  norm = matplotlib.colors.Normalize(vmin=0, vmax=n_ord)
  color = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)

  for i_ord in range(n_ord):
    wavelength = all_wave[i_ord]
    profile    = all_data[i_ord]
    clr        = color.to_rgba(i_ord)

    ax.plot(wavelength, profile, color=clr, lw=1)

  ####################################################
  # Ranges and Annotations
  ####################################################
  if not xaxis_from: xaxis_from = all_wave[0, 0]
  if not xaxis_to:   xaxis_to   = all_wave[-1, -1]
  if not yaxis_from: yaxis_from = 0.5
  if not yaxis_to:   yaxis_to   = 1.5
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)

  ####################################################
  # Save the Plot
  ####################################################
  plt.savefig(file_out, dpi=600)
  print ' - plot: all_echelle: {0} stored'.format(file_out)
  plt.close()

  return ax 
#=================================================================================================================

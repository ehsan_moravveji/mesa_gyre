
import sys, os, glob
import numpy as np
import pylab as plt

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================
#=================================================================================================================
def show_array(x, y, x0=0, x1=1, y0=None, y1=None, file_out=None):
    """
    Present a simple graph of x versus y, and store it as file_out
    """
    fig = plt.figure(figsize=(6,4))
    ax  = fig.add_subplot(111)
    
    ax.plot(x, y)
    
    ax.set_xlim(x0, x1)
    if y0 is None: y0 = np.min(y)
    if y1 is None: y1 = np.max(y)
    ax.set_ylim(y0, y1)
    
    plt.tight_layout()
    plt.savefig(file_out)
    print ' - plotter: show_array: saved {0}'.format(file_out)
    plt.close()
    
#=================================================================================================================

#! /usr/bin/python

import sys, os, glob, shutil
import logging
import argparse
import numpy as np
import initials, rw, aux

#=================================================================================================================
# Logging settings
logging.basicConfig(filename='error.log', format='%(levelname)s: %(asctime)s: %(message)s', level=logging.DEBUG)
#=================================================================================================================

#=================================================================================================================
#=================================================================================================================
def main(namelist):
	"""
	Exectute the program here
	"""

	# Read in the inlist?
	dic_nmlst = rw.read_namelist(namelist)

	# Read in the input model(s)
	# Prepare the input, normalization and differentiation
	list_coeff = aux.gen_coefficients(dic_namelist=dic_nmlst)



#=================================================================================================================
if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('namelist', help='Full path to the input inlist file.', type=str)
	args = parser.parse_args()

	status = main(namelist=args.namelist)
	sys.exit(status)
#=================================================================================================================


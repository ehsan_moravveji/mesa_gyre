
import sys, os, glob, shutil
import logging
import numpy as np
import scipy
import initials, rw, plotter

G_grav = 6.67428e-8    # g^-1 cm^3 s^-2

n_extrap = 10

#=================================================================================================================
def gen_coefficients(dic_namelist, el=1):
	"""
	The input models will be read and the coefficients of the pulsation equations (Eqs. 5 - 8) will be returned.
	The coefficients are normalized, so they are dimensionless
	@param dic_namelist: the namelist items are passed as a dictionary. This dictionary is created by io.read_namelist()
	@type dic_namelist: dictionary
	@return: list of input models
	@rtype: list of dictionaries
	"""
	list_models = rw.read_models(dic_namelist=dic_namelist)
	n_mod       = len(list_models)
	if n_mod == 0:
		logging.error('aux: gen_coefficients: No models returned!')
		raise SystemExit
	el_in_range = (el >= dic_namelist['el_from']) & (el <= dic_namelist['el_to'])
	if not el_in_range:
		logging.error('aux: gen_coefficients: el={0} outside the requested range of {1} to {2}'.format(
		el, dic_namelist['el_from'], dic_namelist['el_to']))
		raise SystemExit

	print 'aux: gen_coefficients: ', list_models[0]['data'].dtype.names

	list_K_1 = eval_K_1(list_models, el=el)
	list_K_2 = eval_K_2(list_models, el=el)
	list_K_3 = eval_K_3(list_models, el=el)
	list_K_4 = eval_K_4(list_models, el=el)
	
	list_coefficients = get_K_matrix(list_K_1, list_K_2, list_K_3, list_K_4)

	return list_coefficients

#=================================================================================================================
def get_K_matrix(list_K_1, list_K_2, list_K_3, list_K_4):
    """
    This routine receives a list of K_1 to K_4 arrays for each input file, and combines them as a single numpy array with the column names K_1 to K_4.
    It returns a list of these matrixes for every individual profile file.
    @param list_K_1, list_K_2, list_K_3, list_K_4: list of numpy 1D arrays, that contain the K_1 to K_4 coefficients per each input file.
        The size of each coefficient is equal to the number of mesh points in the MESA output file.
    @type list_K_1, list_K_2, list_K_3, list_K_4: list of numpy 1D arrays
    @return: list of numpy record arrays, where each record array is just a merge of 1D arrays for the same input model.
    @rtype: list of numpy record arrays
    """
    n = len(list_K_1)
    if len(list_K_2) != n:
        logging.error('aux: get_K_matrix: size of list_K_2 is incompatible')
        raise SystemExit
    if len(list_K_3) != n:
        logging.error('aux: get_K_matrix: size of list_K_3 is incompatible')
        raise SystemExit
    if len(list_K_4) != n:
        logging.error('aux: get_K_matrix: size of list_K_4 is incompatible')
        raise SystemExit

    list_K = []
    for i in range(n):
        K_1 = list_K_1[i]
        K_2 = list_K_2[i]
        K_3 = list_K_3[i]
        K_4 = list_K_4[i]
        K    = [K_1, K_2, K_3, K_4]
        mx = np.core.records.fromarrays(np.array(K), names='K_1, K_2, K_3, K_4', formats='f8, f8, f8, f8')

        list_K.append(mx)

    return list_K

#=================================================================================================================
def eval_K_1(list_models, el=1):
	"""
	Evaluate K_1 coefficient based on Eq. 5
	@param list_models: list of dictionaries containing the model input information
	@type list_models: list of dictionaries
	@param el: the mode degree
	@type el: integer
	@return: list of arrays containing K_1(x), one array per each input model.
	@rtype: list of numpy arrays
	"""
	list_K_1 = []

	for i, dic in enumerate(list_models):
           header   = dic['header']
	   data     = dic['data']

	   R_star   = header['R_star']
	   freq_dyn = header['freq_dyn']
	   r        = data['radius']
	   x        = r / R_star
	   x2       = x * x
	   N2       = data['brunt_N2']
	   N2_adim  = N2 / np.power(freq_dyn, 2.)

	   K_1      = el*(el+1.) * N2_adim / x2

	   list_K_1.append(K_1)

	return list_K_1

#=================================================================================================================
def eval_K_2(list_models, el=1):
	"""
	Evaluate K_2 coefficient based on Eq. 6
	@param list_models: list of dictionaries containing the model input information
	@type list_models: list of dictionaries
	@param el: the mode degree
	@type el: integer
	@return: list of arrays containing K_2(x), one array per each input model.
	@rtype: list of numpy arrays
	"""
	list_K_2 = []
	for i, dic in enumerate(list_models):
                header   = dic['header']
                data     = dic['data']

                R_star   = header['R_star']
		M_star   = header['M_star']
		freq_dyn = header['freq_dyn']
		rho_factor = (M_star / (4. * np.pi * np.power(R_star, 3)))
		P_factor = (G_grav * M_star**2 / (4. * np.pi * np.power(R_star, 4)))
		c2_factor= G_grav * M_star / R_star

		r        = data['radius']
		x        = r / R_star

		rho      = data['density']
		rho_adim = rho / rho_factor
		P        = data['pressure']
		P_adim   = P / P_factor
		Gamma1   = data['gamma1']
		c2       = Gamma1 * P / rho
		c2_adim  = c2 / c2_factor

		# From now, every variable is dimensinless
		ln_rho   = np.log(rho_adim)
		rho_c2   = rho_adim * c2_adim
                ln_rho_c2= np.log(rho_c2)
		d_ln_rho_c2_dx = fin_diff(x=x, y=ln_rho_c2)
		d_ln_rho_dx    = fin_diff(x=x, y=ln_rho)
		K_2     = 2./x + 2.*d_ln_rho_c2_dx - d_ln_rho_dx

		list_K_2.append(K_2)


	return list_K_2

#=================================================================================================================
def eval_K_3(list_models, el=1):
    """
	Evaluate K_3 coefficient based on Eq. 7
	@param list_models: list of dictionaries containing the model input information
	@type list_models: list of dictionaries
	@param el: the mode degree
	@type el: integer
	@return: list of arrays containing K_2(x), one array per each input model.
	@rtype: list of numpy arrays
    """
    list_K_3 = []
    for i, dic in enumerate(list_models):
        header   = dic['header']
        data     = dic['data']

        R_star   = header['R_star']
        M_star   = header['M_star']
        freq_dyn = header['freq_dyn']
        g_factor = G_grav * M_star / np.power(R_star, 2)
        c2_factor= G_grav * M_star / R_star
        rho_factor = M_star / (4. * np.pi * np.power(R_star, 3))

        r        = data['radius']
        x        = r / R_star
        q_div_xq = data['q_div_xq'][::-1]
        q        = q_div_xq / (1 + q_div_xq)
        m        = M_star * q
        g        = G_grav * m / r
        g_adim   = g / g_factor
        ln_g     = np.log(g_adim)
        P        = data['pressure']
        rho      = data['density']
        Gamma1   = data['gamma1']
        c2       = Gamma1 * P / rho
        c2_adim  = c2 / c2_factor
        rho_adim = rho / rho_factor
        ln_rho   = np.log(rho_adim)
        rho_c2   = rho_adim * c2_adim
        ln_rho_c2= np.log(rho_c2)

        d_ln_g_dx      = fin_diff(x=x, y=ln_g)
        d_ln_rho_dx    = fin_diff(x=x, y=ln_rho)
        d_ln_rho_c2_dx = fin_diff(x=x, y=ln_rho_c2)
        d_rho_c2_dx    = fin_diff(x=x, y=rho_c2)
        d2_rho_c2_dx2  = fin_diff(x=x, y=d_rho_c2_dx)   # <- this needs a better treatment
        #d2_rho_c2_dx2  = second_deriv(x=x, y=rho_c2)

        term1    = -el*(el+1) / np.power(x, 2)
        term2    = 2. * g_adim / c2_adim * (d_ln_g_dx + 1./x)
        term3    = d_ln_rho_c2_dx * (2./x - d_ln_rho_dx)
        term4    = d2_rho_c2_dx2 / rho_c2

        K_3      = term1 + term2 + term3 + term4

        list_K_3.append(K_3)

        plotter.show_array(x, term4, x0=0.9, x1=1.01, y0=-10000, y1=10000, file_out='plots/test.png')


    return list_K_3

#=================================================================================================================
def eval_K_4(list_models, el=1):
    """
	Evaluate K_4 coefficient based on Eq. 8
	@param list_models: list of dictionaries containing the model input information
	@type list_models: list of dictionaries
	@param el: the mode degree
	@type el: integer
	@return: list of arrays containing K_2(x), one array per each input model.
	@rtype: list of numpy arrays
    """
    list_K_4 = []
    for i, dic in enumerate(list_models):
        header   = dic['header']
        data     = dic['data']

        R_star   = header['R_star']
        M_star   = header['M_star']
        freq_dyn = header['freq_dyn']
        g_factor = G_grav * M_star / np.power(R_star, 2)
        c2_factor= G_grav * M_star / R_star

        r        = data['radius']
        x        = r / R_star
        q_div_xq = data['q_div_xq'][::-1]
        q        = q_div_xq / (1 + q_div_xq)
        m        = M_star * q
        g        = G_grav * m / np.power(r, 2)
        g_adim   = g / g_factor
        ln_g     = np.log(g_adim)
        P        = data['pressure']
        rho      = data['density']
        Gamma1   = data['gamma1']
        c2       = Gamma1 * P / rho
        c2_adim  = c2 / c2_factor

        d_ln_g_dx= fin_diff(x=x, y=ln_g)        # <- we have a problem near surface

        K_4      = -2 * g_adim / c2_adim * (d_ln_g_dx - 1./x)

        list_K_4.append(K_4)
        #plotter.show_array(x, d_ln_g_dx, x0=0.8, x1=0.95, file_out='plots/test.png')

    return list_K_4

#=================================================================================================================
def fin_diff(x, y):
    """
    Evaluate dx/dy with a finite difference scheme.
    @param x, y: 1D numpy array for the numerator and denumerator
    @type x, y: 1D numpy array
    @return: the differential dx_dy
    @rtype: 1D numpy array
    """
    n = len(x)
    if len(y) != n:
        logging.error('aux: fin_diff: len(x) != len(y)')
        raise SystemExit

    dy_dx = np.zeros(n)

    h = x[1:] - x[:-1]
    s = (y[1:] - y[:-1]) / h

    dy_dx[1:n-1] = 0.5 * (s[1:] + s[:-1])
    dy_dx[0]     = dy_dx[1]
    dy_dx[n-1]   = dy_dx[n-2]

    return dy_dx

#=================================================================================================================
def second_deriv(x, y):
    """
    Evaluate d^2x/dy^2 with a finite difference scheme.
    @param x, y: 1D numpy array for the numerator and denumerator
    @type x, y: 1D numpy array
    @return: the differential d^2x_dy^2
    @rtype: 1D numpy array
    """
    n = len(x)
    if len(y) != n:
        logging.error('aux: fin_diff: len(x) != len(y)')
        raise SystemExit

    d2y_dx2 = np.zeros(n)

    for i in range(1, n-1):
        h2         = (x[i+1] - x[i])**2.0
        d2y_dx2[i] = (y[i+1] - 2*y[i] + y[i-1]) / h2

    d2y_dx2[0] = d2y_dx2[1]
    d2y_dx2[n-1] = d2y_dx2[n-2]

    return d2y_dx2

#=================================================================================================================
def center_to_face(data, arr):
    """
    Interpolate a cell-averaged (hence intensive) array to the cell face by the means of mass interpolation
    Note: The input array must be already from surface to core, i.e. in MESA style.
    @param data: the structure data as a named matrix. it is returned by rw.read_gyre_in()
    @type data: numpy record array
    @param arr: an intensive quantity to be interpolated. Note that in return, the output array is still in MESA style
    @type arr: numpy array
    """
    dq       = dq_surface_to_center(data)
    nz       = len(dq)
    output   = np.zeros(nz)

    for k in range(nz):
        if k == 0:
            alfa = 1.0
            beta = 0.0
        else:
            alfa = dq[k-1] / (dq[k] + dq[k-1])
            beta = 1.0 - alfa

        output[k] = alfa * arr[k] + beta * arr[k-1]

    return output

#=================================================================================================================
def diff_inten_over_exten(data, inten, exten):
    """
    Differentiate by interpolation for dA/dB, where A is originally defined as cell centered, hence an intensive
    quantity, and B is originally defined at cell faces, hence an extensive quantity.
    Note: The GYRE input files are stored from core to surface, which is the opposite of what MESA is naturally built.
          To make sure we carry out the interpolations for differentiation descently, we invert all arrays to adapt to
          MESA, and after the full differentiation, we revert the differential vector back to GYRE style.
    @param data: the structure data as a named matrix. it is returned by rw.read_gyre_in()
    @type data: numpy record array
    @param inten: an intensive quantity appearing in the numerator of the differential dA
    @type intent: numpy array
    @param exten: an extensive quantity appearing in the denumerator of the differential dB
    @type exten: numpy array
    @return: 1D numpy array giving the differential dA/dB defined at the cell "face".
    @rtype: numpy array
    """
    inten    = inten[::-1]
    exten    = exten[::-1]

    dq       = dq_surface_to_center(data)
    nz       = len(dq)
    q_div_xq = data['q_div_xq'][::-1]
    q        = q_div_xq / (1 + q_div_xq)

    inten_face = center_to_face(data, inten)     # is at face
    d_inten  = inten_face[1:] - inten_face[:-1]  # is at center
    d_exten  = exten[1:] - exten[:-1]            # is at center
    diff     = d_inten / d_exten                 # is at center

    # We still need to extrapolate from surface towards the core for the last point,
    # which is the innermost point
    m_extrap = q[nz-1-n_extrap : -1]              # q has nz points
    y_extrap = diff[nz-1-n_extrap : ]             # diff has nz-1 points, missing the "nz"-th point
    m_0      = q[-1]
    #extrap   = extrapolate_at_center(m_extrap, y_extrap, m_0, k=3)
    extrap  = 0.0

    # Extend the differential vector to contain the last interpolated point
    diff     = np.append(diff, extrap)

    # Now, bring the "diff" vector from center to the face
    diff     = center_to_face(data, diff)

    return diff[::-1]                             # from core to surface

#=================================================================================================================
def dq_surface_to_center(data):
    """
    The dq array is always needed for interpolation. This routine, just returns the dq array in MESA style, i.e. from
    surface to the center
     @param data: the structure data as a named matrix. it is returned by rw.read_gyre_in()
    @type data: numpy record array
    @return: the dq array from surface to center
    @rtype: 1D numpy array
    """
    q_div_xq = data['q_div_xq'][::-1]
    q        = q_div_xq / (1 + q_div_xq)
    dq       = q[1:] - q[:-1]
    dq       = np.append(dq, 0.0)               # to fix the length of the dq array

    return dq

#=================================================================================================================
def extrapolate_at_center(x, y, x0, k=3):
    """
    Extrapolate the
    """
    from scipy.interpolate import splrep
    #spline = scipy.interpolate.splrep(x, y, k=k, s=0)
    spline = splrep(x, y, k=k, s=0)
    return scipy.interpolate.splev(x0, spline)

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================

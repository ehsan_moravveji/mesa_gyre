
import logging
import numpy as np

import definitions as dfn 

##################################################################################################

dbg  = dfn.dbg
 
pi   = dfn.PI 
Msun = dfn.M_SUN
Rsun = dfn.R_SUN
Grav = dfn.G_GRAVITY

##################################################################################################
def get_mass(dic):
  """
  Reconstruct the array of mass coordinates in the model based on the column "q_div_xq" in 
  the input model.
  @param dic: dictionary containing gyre input file information
  @type dic: dictionary
  @return: an array of mass, m, in grams, with the size equivalent to the number of rows in 
         the input GYRE file
  @rtype: ndarray
  """
  M_star  = dic['M_star']
  gyre_in = dic['gyre_in']
  q       = gyre_in['q_div_xq']
  mass    = q * M_star / (1 + q)

  if mass[-1] != M_star:
    if dbg: print ' From get_mass: ', mass[-1], M_star
    logging.error('coefficients: get_mass: Inconsistent model mass!')
    raise SystemExit, 'Error: coefficients: get_mass: Inconsistent model mass!'

  return mass

##################################################################################################
def get_V(dic):
  """
  V = - d_ln_p / d_ln_r
  """
  R_star  = dic['R_star']
  gyre_in = dic['gyre_in']
  p       = gyre_in['pressure']
  d       = gyre_in['density']
  r       = gyre_in['radius']
  x       = r / R_star
  x2      = np.power(x, 2)
  m       = get_mass(dic)
  V2      = Grav * m * d / (p * r * x2)  # in gyre/src/ad/gyre_evol_model: V_2 = G_GRAVITY*m*rho/(p*r*x**2)
  V       = V2 * x2                      # in gyre/src/ad/gyre_evol_model: V   = V_2 * x**2

  return V

##################################################################################################
def get_D(dic):
  """
  D = dlnRho / dlnr
  """
  gyre_in = dic['gyre_in']
  d       = gyre_in['density']
  lnd     = np.log(d)
  r       = gyre_in['radius']
  lnr     = np.log(r)
  d_lnd   = np.zeros_like(d)
  d_lnr   = np.zeros_like(r)
  d_lnd[:-1] = lnd[1:] - lnd[:-1]
  d_lnr[:-1] = lnr[1:] - lnr[:-1]
  D       = np.zeros_like(d)
  D[:-1]  = d_lnd[:-1] / d_lnr[:-1]

  return D

##################################################################################################
def get_As(dic):
  """
  As = 1/Gamma_1 * dlnp/dlnr - dlnRho/dlnr 
     = r**3*N2/(G_GRAVITY*m)
  """
  gyre_in = dic['gyre_in']
  N2      = gyre_in['brunt_N2']
  r       = gyre_in['radius']
  r3      = np.power(r, 3.0)
  m       = get_mass(dic)

  As      = r3 * N2 / (Grav * m)

  return As 

##################################################################################################
def get_U(dic):
  """
  U = d_ln_m / d_ln_r
    = 4.0*PI*rho*r**3/m
  """
  gyre_in = dic['gyre_in']
  m       = get_mass(dic)
  r       = gyre_in['radius']
  r3      = np.power(r, 3.0)
  d       = gyre_in['density']

  U       = 4.0 * pi * d * r3 / m

  return U

##################################################################################################
def get_c1(dic):
  """
  c1 = (r/R)^3 x (M/m)
  """
  M_star  = dic['M_star']
  R_star  = dic['R_star']
  gyre_in = dic['gyre_in']
  m       = get_mass(dic)
  r       = gyre_in['radius']

  c1      = np.power(r/R_star, 3) * (M_star / m)

  return c1

##################################################################################################
def get_Gamma_1(dic):
  """
  Gamma_1 = (\partial p / \partial \rho)_{adiab}
  """
  return dic['gyre_in']['gamma1']

##################################################################################################

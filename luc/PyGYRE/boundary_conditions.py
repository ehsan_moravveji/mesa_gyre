import logging
import numpy as np

import definitions as dfn 

##################################################################################################

dbg = dfn.dbg

##################################################################################################
def get_inner_bc(el, w2, c1_0):
  """
  Based on Eq. (A5) in the GYRE instrument paper
  """
  n  = dfn.ad_n_y
  BC = np.zeros((2, n), dtype=dfn.ad_dtype)
  el = float(el)

  BC[0, 0] = c1_0 * w2
  BC[0, 1] = -el
  # BC[0, 2] = 0.0
  # BC[0, 3] = 0.0

  # BC[1, 0] = 0.0
  # BC[1, 1] = 0.0
  BC[1, 2] = el 
  BC[1, 3] = -1.0

  return BC 

##################################################################################################
def get_outer_bc(el, w2, U_N):
  """
  Based on Eq. (A6) in the GYRE instrument paper
  """
  n  = dfn.ad_n_y
  BC = np.zeros((2, n), dtype=dfn.ad_dtype)
  el = float(el)

  BC[0, 0] = 1.0
  BC[0, 1] = -1.0
  BC[0, 2] = 1.0
  # BC[0, 3] = 0.0

  BC[1, 0] = U_N
  # BC[1, 1] = 0.0
  BC[1, 2] = el + 1.0
  BC[1, 3] = 1.0

  return BC

##################################################################################################
def adapt_inner_bc(el, w2, c1_0):
  """
  To put the inner BC into the big S matrix, the inner BC is adapted to a n x n matrix where 
  the first two rows come from calling get_inner_bc(), and the latter two rows are zero
  """
  n  = dfn.ad_n_y
  BC = np.zeros((n, n), dtype=dfn.ad_dtype)
  IN = get_inner_bc(el, w2, c1_0)
  BC[0, :] = IN[0, :]
  BC[1, :] = IN[1, :]

  return BC

##################################################################################################
def adapt_outer_bc(el, w2, U_N):
  """
  To put the outer BC into the big S matrix, the outer BC is adapted to n x n matrix, where
  the first two rows are zero, and the latter two rows come from calling get_outer_bc()
  """
  n   = dfn.ad_n_y
  BC  = np.zeros((n, n), dtype=dfn.ad_dtype)
  OUT = get_outer_bc(el, w2, U_N)
  BC[2, :] = OUT[0, :]
  BC[3, :] = OUT[1, :]

  return BC

##################################################################################################

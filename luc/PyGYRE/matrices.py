
import logging
import numpy as np
from scipy import sparse
from scipy.sparse import linalg

import definitions as dfn 
import coefficients as cfs
import boundary_conditions as bc

##################################################################################################

dbg = dfn.dbg

##################################################################################################
def get_A_tilde(dic, el, w2):
  """
  Because the input model has N grid points, and because 
  """
  n  = dfn.ad_n_y
  N  = dic['nn']
  nxN= n  * N
  V  = cfs.get_V(dic)
  As = cfs.get_As(dic)
  U  = cfs.get_U(dic)
  D  = cfs.get_D(dic)
  c1 = cfs.get_c1(dic)
  Gamma_1 = cfs.get_Gamma_1(dic)

  Z  = get_zero(n=n, dtype=dfn.ad_dtype)
  I  = get_identity(n=n, dtype=dfn.ad_dtype)
  inner_bc = bc.adapt_inner_bc(el, w2, c1[0]) 
  outer_bc = bc.adapt_outer_bc(el, w2, U[-1])

  # The big matrix S 
  # S  = np.zeros((n*N, n*N), dtype=dfn.ad_dtype)
  # S  = make_csc_matrix(n=n*N, dtype=dfn.ad_dtype)

  S  = ()
  for i_row in range(N):
    row_0  = np.zeros(nxN, dtype=dfn.ad_dtype)
    row_1  = np.zeros(nxN, dtype=dfn.ad_dtype)
    row_2  = np.zeros(nxN, dtype=dfn.ad_dtype)
    row_3  = np.zeros(nxN, dtype=dfn.ad_dtype)

    if i_row == 0:
      Y    = inner_bc
      put_I= False
      nroll= 0
    elif 0 < i_row < N-1:
      Y    = get_A_tilde_k(i_row, el, w2, V, As, U, D, c1, Gamma_1)
      put_I= True
      nroll= n * (i_row - 1)
    elif i_row == N-1:
      Y    = outer_bc
      put_I= False
      nroll= n * (N - 1)

    row_0[0:n] = Y[0, 0:n]
    row_1[0:n] = Y[1, 0:n]
    row_2[0:n] = Y[2, 0:n]
    row_3[0:n] = Y[3, 0:n]

    if put_I:
      row_0[n:2*n] = I[0, 0:n]
      row_1[n:2*n] = I[1, 0:n]
      row_2[n:2*n] = I[2, 0:n]
      row_3[n:2*n] = I[3, 0:n]

    if nroll > 0:
      row_0    = np.roll(a=row_0, shift=nroll)
      row_1    = np.roll(a=row_1, shift=nroll)
      row_2    = np.roll(a=row_2, shift=nroll)
      row_3    = np.roll(a=row_3, shift=nroll)

    S += (row_0, row_1, row_2, row_3)

  S = np.vstack(S)
  S = sparse.csc_matrix(S)

  return S

##################################################################################################
def get_A_tilde_k(k, el, w2, V, As, U, D, c1, Gamma_1):
  """
  A_tilde is given by Eq. (A3) in the GYRE instrument paper.
  This routine computes A_tilde_k, after passing mesh index k, degree el and trial square 
  (adimensinal) frequency omega^2 (hence w2).
  """
  if w2 == 0:
    logging.error('matrices: get_A_tilde_k: omega^2 == 0')
    raise SystemExit, 'Error: matrices: get_A_tilde_k: omega^2 == 0'

  el      = float(el)
  L       = el * (el + 1.0)
  n       = dfn.ad_n_y
  dtype   = dfn.ad_dtype
  A_tilde = np.zeros((n, n), dtype=dtype)

  V_k     = V[k]
  As_k    = As[k]
  U_k     = U[k]
  D_k     = D[k]
  c1_k    = c1[k]
  G_k     = Gamma_1[k]

  A_tilde[0, 0] = V_k/G_k - 1.0 - el
  A_tilde[0, 1] = L / (c1_k * w2) - V_k/ G_k
  A_tilde[0, 2] = V_k / G_k
  # A_tilde[0, 3] = 0.0

  A_tilde[1, 0] = c1_k * w2 - As_k
  A_tilde[1, 1] = As_k - U_k + 3.0 - el
  A_tilde[1, 2] = -As_k
  # A_tilde[1, 3] = 0.0

  # A_tilde[2, 0] = 0.0
  # A_tilde[2, 1] = 0.0
  A_tilde[2, 2] = 3.0 - U_k - el
  A_tilde[2, 3] = 1.0

  A_tilde[3, 0] = U_k * As_k
  A_tilde[3, 1] = U_k * V_k / G_k
  A_tilde[3, 2] = L - A_tilde[3, 1]  # this is actually faster than "L - U_k x V_k / G_k"
  A_tilde[3, 3] = -U_k + 2.0 - el

  return A_tilde

##################################################################################################
def get_zero(n, dtype):
  """
  Return a null matrix of size n x n
  """
  return np.zeros((n, n), dtype=dtype)

##################################################################################################
def get_identity(n, dtype):
  """
  Return an identity matrix of size n x n
  """
  I = np.zeros((n, n), dtype=dtype)
  for k in range(n):
    I[k, k] = dtype(1.0)

  return I 

##################################################################################################
def make_csc_matrix(n, dtype):
  """
  The sparse matrix routnes in scipy better (or even only)
  """

  return sparse.csc_matrix((n, n), dtype=dtype)

##################################################################################################
def eval_slogdet(S):
  """
  Use np.linalg.slogdet(), and return the sign x log(Determinant(S))
  """
  return np.linalg.slogdet(S)

##################################################################################################
def eval_det(S):
  """
  Use np.linalg.det(), and return the determinant of the square matrix S
  """
  return np.linalg.det(S)

##################################################################################################
def LU_decomposition(S):
  """
  Use scipy.sparse.linalg.splu() for a canonical LU decomposition of the input square matrix S.
  """
  obj = linalg.splu(S, permc_spec=None, diag_pivot_thresh=None, drop_tol=None, 
                    relax=None, panel_size=None, options={})
  print type(obj)

  return obj

##################################################################################################
def count_nonZero_elements(n, N):
  """
  Estimate the number of non-zero elements in the sparse S matrix, given the degree of the problem
  n, and the number of blocks in the matrix N (i.e. the number of GYRE spatial grid points).
  This routine can be even made better by excluding zero elements of the Y matrix that constitute a
  block in S.
  """

  i_Y = 0
  j_Y = 0
  i_I = 0
  j_I = 0

  # Define and initiate with indices of Inner BC
  ind_nonZero  = [(i, j) for i in range(n) for j in range(n)]
  vals_nonZero = []

  for i_Y in range(1, N-1):
    j_Y      = i_Y - 1

    # for the Y block
    i_elem_0 = n * i_Y + 0
    i_elem_1 = n * i_Y + 1
    i_elem_2 = n * i_Y + 2
    i_elem_3 = n * i_Y + 3

    j_elem_0 = n * j_Y + 0
    j_elem_1 = n * j_Y + 1
    j_elem_2 = n * j_Y + 2
    j_elem_3 = n * j_Y + 3

    ind_nonZero  += [(i, j) for i in [i_elem_0, i_elem_1, i_elem_2, i_elem_3] for j in [j_elem_0, j_elem_1, j_elem_2, j_elem_3] ]
    vals_nonZero += []

    # For the identity matrix
    i_I      = j_I = i_Y
    i_e_I    = range(n * i_I, n * i_I + n)
    ind_nonZero  += [(i, i) for i in range( n * i_I, n * i_I + n )]
    vals_nonZero += [1.0 for i in range(n)]

  # Outer BC
  ind_nonZero += [(i, j) for i in range(n * (N-1), n * N) for j in range(n * (N-1), n*N)]
  vals_nonZero += []

  n_nonZero  = len(ind_nonZero)

  print ' The Matrix length = {0}'.format(n * N)
  print ' The Matrix Size = {0}'.format((n * N)**2)
  print ' The non-Zero elements = {0}'.format(n_nonZero)

  return n_nonZero

##################################################################################################
##################################################################################################
##################################################################################################

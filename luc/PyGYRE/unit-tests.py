
import sys, os, glob
import logging
import numpy as np
from mesa_gyre import read

import definitions as dfn 
import coefficients as cfs
import matrices as mtx
import boundary_conditions as bc
import plotter

##################################################################################################

#    P L O T T I N G    R O U T I N E S

##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
def plot_all_coefficients(dic):
  print ' - Test: Call plotter.show_coefficients()'
  plotter.show_coefficients(dic, xaxis='mass', dir_save='plots/', plot_prefix='Coeff-')

  return True 

##################################################################################################


#    T E S T   R O U T I N E S

##################################################################################################
##################################################################################################
def print_compare_a_block(dic, el, w2, i_block):
  print ' - Test: matrices: only print a block, and ensure it is not zero'
  n       = dfn.ad_n_y
  c1      = cfs.get_c1(dic)
  N       = len(c1)
  if i_block == 0 or i_block >= N-1: return False

  S       = mtx.get_A_tilde(dic=dic, el=el, w2=w2)
  i_from  = i_block * n 
  i_to    = (i_block + 1) * n
  j_from  = (i_block - 1) * n 
  j_to    = i_block * n
  block   = S[i_from : i_to , j_from : j_to]

  i_I_from= i_from
  i_I_to  = i_to
  j_I_from= j_from + n 
  j_I_to  = j_I_from + n
  I       = S[i_I_from : i_I_to , j_I_from : j_I_to]

  print '\n The block:'
  print block
  print '\n Identity block:'
  print I

  return True

##################################################################################################
def print_compare_inner_bc(dic, el, w2):
  print ' - Test: matrices: compare inner_bc with the last block in A_tilde matrix'
  c1      = cfs.get_c1(dic)
  N       = len(c1)
  n       = dfn.ad_n_y
  inner   = bc.adapt_inner_bc(el, w2, c1[0])
  S       = mtx.get_A_tilde(dic=dic, el=el, w2=w2)

  print inner
  print S[:n , :n]

##################################################################################################
def print_compare_outer_bc(dic, el, w2):
  print ' - Test: matrices: compare outer_bc with the last block in A_tilde matrix'
  U       = cfs.get_U(dic)
  N       = len(U)
  n       = dfn.ad_n_y
  outer   = bc.adapt_outer_bc(el, w2, U[-1])
  S       = mtx.get_A_tilde(dic=dic, el=el, w2=w2)

  print outer
  ind     = n * (N - 1) 
  print  n * (N-1), n * N
  print S[ind: , ind:]


##################################################################################################
def print_identity():
  print ' - Test: matrices: print the identity matrix'
  n       = dfn.ad_n_y
  dtype   = dfn.ad_dtype
  I       = mtx.get_identity(n, dtype)

  print I 

  return True

##################################################################################################
def test_LU_decomposition(dic, el, w2):
  print ' - Test: matrices: LU_decomposition()'
  S       = mtx.get_A_tilde(dic=dic, el=el, w2=w2)
  result  = mtx.LU_decomposition(S)
  print '   Shape: ', result.shape
  print '   Type: ', type(result)
  print '   length: ', len(result)

  return True 

##################################################################################################
def test_det(dic, el, w2):
  print ' - Test: matrices: get_A_tilde() and eval_det()'
  result  = mtx.get_A_tilde(dic=dic, el=el, w2=w2)
  det     = mtx.eval_det(result)
  print '   Det(S) = {0}'.format(det)

  return det

##################################################################################################
def test_slogdet(dic, el, w2):
  print ' - Test: matrices: get_A_tilde() and eval_slogdet()'
  result  = mtx.get_A_tilde(dic=dic, el=el, w2=w2)
  slogdet = mtx.eval_slogdet(result)
  print '   sign x log(Det(S)) = {0}'.format(slogdet)

  return slogdet

##################################################################################################
def test_mtx_get_A_tilde(dic, el, w2):
  print ' - Test: matrices: get_A_tilde()'

  result = mtx.get_A_tilde(dic=dic, el=el, w2=w2)
  print 'shape output:', result.shape
  print 'type  output:', type(result)
  print 'range output:', np.min(result), np.max(result)

  return True

##################################################################################################
if __name__ == '__main__':
  # Preparation
  test_file = 'KIC10526294.gyre'  # best Dmix model
  dic       = read.read_gyre_in(test_file)
  gyre_in   = dic['gyre_in']
  N         = len(gyre_in)
  el        = 1
  w2        = 1.2345

  # print dic['gyre_in'].dtype.names
  # sys.exit()

  # Tests
  if False:
    print_compare_a_block(dic, el, w2, i_block=N-2)

  if False:
    print_compare_inner_bc(dic=dic, el=el, w2=w2)

  if False:
    print_compare_outer_bc(dic=dic, el=el, w2=w2)

  if False:
    print_identity()

  if True:
    test_LU_decomposition(dic=dic, el=el, w2=w2)

  if False:
    test_mtx_get_A_tilde(dic=dic, el=el, w2=w2)
  
  if False:
    test_det(dic=dic, el=el, w2=w2)
    test_slogdet(dic=dic, el=el, w2=w2)

  # Making diagnostic plots
  if False:
    plot_all_coefficients(dic=dic)

##################################################################################################


import logging
import numpy as np

##################################################################################################
# Turn on debugging for all routines
dbg = False

##################################################################################################
# Specifications of the adiabatic and non-adiabatic problem

# Adiabatic problem
ad_n_y  = 4

# Non-adiabatic problem
nad_n_y = 6

##################################################################################################
# Array data types
ad_dtype  = np.float64
nad_dtype = np.complex128

##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
# Physical constants (cgs) copied from gyre/src/core_constants.f90

PI = np.arccos(-1.0)
TWOPI = 2.0*PI
HALFPI = np.arcsin(1.0)

DEG_TO_RAD = PI/180.0
RAD_TO_DEG = 1.0/DEG_TO_RAD

G_GRAVITY = 6.67428E-8                 # Gravitational constant
H_PLANCK =  6.62606896E-27             # Planck's constant
K_BOLTZMANN = 1.3806504E-16            # Boltzmann's constant
C_LIGHT = 2.99792458E10                # Speed of light in vacuuo
SIGMA_STEFAN = 5.670400E-5             # Stefan's constant
A_RADIATION = 4.0*SIGMA_STEFAN/C_LIGHT # Radiation constant
U_ATOMIC = 1.660538782E-24             # Atomic mass unit
ELECTRON_VOLT = 1.602176487E-12        # Electron volt
E_ELECTRON = 4.80320427E-10            # Electron charge
M_ELECTRON = 9.10938215E-28            # Electron mass
SIGMA_THOMSON = 6.6524586E-25          # Thomson cross section
N_AVOGADRO = 6.0221415E23              # Avogadro's number
SEC_YEAR = 24.0*365.25*3600.0          # Seconds in a year

M_SUN = 1.9891E33                      # Solar mass
R_SUN = 6.96E10                        # Solar radius
L_SUN = 3.826E33                       # Solar luminosity (Allen, 1973)
YEAR = 365.25*24.0*3600.0              # Solar Year
PARSEC = 3.0856776E18                  # Parsec

##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################

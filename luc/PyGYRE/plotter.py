
import sys, os, glob
import logging
import numpy as np
import pylab as plt
# from mesa_gyre import read

import definitions as dfn 
import coefficients as cfs
# import matrices as mtx

##################################################################################################

Msun = dfn.M_SUN
Rsun = dfn.R_SUN

##################################################################################################
def show_coefficients(dic, xaxis='mass', dir_save='plots/', plot_prefix='Coeff-'):
  """
  Plot U, D, c1, As, V coefficients versus "xaxis" which only accepts mass, radius or logT
  """
  if xaxis not in ['mass', 'radius', 'logT']:
    logging.error('plotter: show_coeffcients: Unsupported xaxis')
    raise SystemExit, 'Error: plotter: show_coeffcients: Unsupported xaxis'

  if dir_save[-1] != '/': dir_save += '/'
  if not os.path.exists(dir_save): os.makedirs(dir_save)

  M_star   = dic['M_star']
  R_star   = dic['R_star']
  gyre_in  = dic['gyre_in']
  if xaxis == 'mass':
    xvals = cfs.get_mass(dic) / Msun
    xname = r'Mass $q=m/M_\odot$'
  elif xaxis == 'radius':
    xvals = gyre_in['radius'] / Rsun
    xname = r'Radius $x=r/R_\odot$'
  elif xaxis == 'logT':
    xvals = np.log10(gyre_in['temperature'])
    xname = r'Temperature $\log T$ [K]'
  else:
    logging.error('plotter: show_coefficients: Problem with xaxis')
    raise SystemExit, 'Error: plotter: show_coefficients: Problem with xaxis'

  xmin    = np.min(xvals)
  xmax    = np.max(xvals)
  dx      = (xmax - xmin) / 100

  callers = [cfs.get_V, cfs.get_D, cfs.get_As, cfs.get_U, cfs.get_c1, cfs.get_Gamma_1]
  plot_names = ['V', 'D', 'As', 'U', 'c1', 'Gamma-1']
  ylabels = [r'$V$', r'$D$', r'$A^\star$', r'$U$', r'$c_1$', r'$\Gamma_1$']

  for ip, caller in enumerate(callers):
    fig, ax = plt.subplots(1, figsize=(6, 4))
    plot_name = dir_save + plot_prefix + plot_names[ip] + '.png'

    yvals   = caller(dic)
    ymin    = np.min(yvals)
    ymax    = np.max(yvals)
    dy      = (ymax - ymin) / 100

    # ax.plot(xvals, yvals)
    ax.scatter(xvals, yvals, marker='.', s=1)

    ax.set_xlim(xmin-dx, xmax+dx)  
    ax.set_ylim(ymin-dy, ymax+dy)
    ax.set_xlabel(xname)
    ax.set_ylabel(ylabels[ip])

    plt.tight_layout()
    plt.savefig(plot_name, transparent=True)
    print ' - plotter: show_coefficients: saved {0} '.format(plot_name)
    plt.close()

  return None 

##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################

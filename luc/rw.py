
"""
rw == Read and Write
"""

import sys, os, glob, shutil
import numpy as np
import logging
import initials, aux

G_grav = 6.67428e-8    # g^-1 cm^3 s^-2

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================
def read_multiple_gyre_in(list_files):
  """
  Iteratively call read_gyre_in, and collect the data as a list of tuples
  @param list_files: list of full path to the individual .gyre files
  @type list_files: list of strings
  @return: list of tuples, where the elements of the tuple are:
     -
  @rtype: list of tuples
  """
  list_tup = []
  for i, filename in enumerate(list_files): 
      dic = read_gyre_in(filename)
      list_tup.append(dic)

  return list_tup

#=================================================================================================================
def read_gyre_in(filename, h5=False):
  """
  Read input model to GYRE in ASCII or HDF5 (GSM) format.
  @param filename: full path to the input GYRE model
  @type filename: string
  @param h5: whether or not the input file is ascii (False) or HDF5 (GSM, True); default=False
  @type h5: boolean
  @return: tuple with these items:
   A dictionary with:
    - nn: number of mesh
    - M_star: total mass in gr
    - R_star: total radius in cm
    - L_star: total luminosity in cgs
    - ncol: number of data columns; default=19
    - freq_dyn: the frequency associated with the free fall, Hz
   A numpy record array passing the data
  @rtype: tuple
  """
  if not os.path.exists(filename):
    message = 'Error: rw: read_gyre_in: {0} does not exist'.format(filename)
    raise SystemExit, message

  dic = {}
  hdr_names = 'nn, M_star, R_star, L_star, freq_dyn, ncol'
  col_names = 'zone, radius, q_div_xq, luminosity, pressure, temperature, density, gradT, brunt_N2, gamma1, grada,' \
                 'chiT_div_chiRho, kappa, kappa_T, kappa_rho, epsilon, epsilon_T, epsilon_rho, omega'

  with open(filename, 'r') as f: lines = f.readlines()
  header = lines[0].rstrip('\r\n').split()
  nn     = int(header[0])
  M_star = float(header[1])
  R_star = float(header[2])
  L_star = float(header[3])
  ncol   = int(header[4])
  header.insert(4, np.sqrt(G_grav * M_star / np.power(R_star, 3)))   # Hz

  hdr_dtype = 'i8, f8, f8, f8, f8, i8'
  col_dtype = 'i8,' + 'f8,' * (ncol - 1)

  data   = []
  for line in lines[1:]:
    splitted = line.rstrip('\r\n').split()
    float_line = []
    float_line.append(int(splitted[0]))
    for i in range(1, ncol): float_line.append(float(splitted[i].replace('D', 'e')))
    data.append(float_line)

  header = np.core.records.fromarrays(np.array(header).transpose(), names=hdr_names, formats=hdr_dtype[:-1])
  recarr = np.core.records.fromarrays(np.array(data).transpose(), names=col_names, formats=col_dtype[:-1])

  dic['filename'] = filename
  dic['nn']     = nn
  dic['M_star'] = M_star
  dic['R_star'] = R_star
  dic['L_star'] = L_star
  dic['freq_dyn'] = np.sqrt(G_grav * M_star / np.power(R_star, 3))   # Hz
  dic['ncol']   = ncol

  return {'header':header, 'data':recarr}

#=================================================================================================================
def read_multiple_gyre_files(filenames, add_extra=False):
  """
  Return a list of dictionaries with each dictionary containng all information from each GYRE output file.
  The available keys for each output dictionary is the same as the keys returned by dic and the names of loc record array
  returned by gyre.read_output; thus, we do no longer use numpy record arrays
  This routine reads more than one gyre files. For reading a single file, it uses the gyre.py module
  @param filenames: list of filenames; one line per each file
  @type filenames: list of strings
  @return: dictionary and record array
  @rtype: list of dictionaries and record array
  """

  #from stellarmodels import read_gyre
  import gyre

  n_files = len(filenames)
  #dic_gyre = []
  loc_gyre = []
  n_modes = 0
  list_output = []

  if n_files == 1:
    file = filenames[0]
    dic_gyre, loc_gyre = gyre.read_output(file)
    n_modes = len(loc_gyre)
    list_output.append((dic_gyre, loc_gyre))

    return list_output

  if n_files > 1:
    n_failed = 0
    list_failed = []
    for i_file in range(n_files):
      file = filenames[i_file]

      try:
        dic_single_gyre, loc_single_gyre = gyre.read_output(file)
      except IOError:
        n_failed += 1
        print '   read_multiple_gyre_files: IOError: %s' % (file, )
        list_failed.append(file)
      n_modes += len(loc_single_gyre)

      list_output.append((dic_single_gyre, loc_single_gyre))
      # pt.update_progress(i_file/float(n_files))

  print ' - Total number of modes in the whole archive = %s' % (n_modes, )
  if n_files>1 and n_failed > 0:
    print ' - %s Jobs Failed to read.\n' % (n_failed, )

  return list_output

#=================================================================================================================
def read_namelist(namelist):
	"""
	Read the input ascii namelist file, and return the specifications as a dictionary
	@param namelist: Full path to the input namelist file
	@type namelist: string
	@return: dictionary with each field returned as a key:value pair
	@rtype: dictionary
	"""
	if type(namelist) is not type('string'):
		logging.error('rw: read_namelist: Input namelist is not of the string type')
		raise SystemExit
	if not os.path.exists(namelist):
		logging.error('rw: read_namelist: "{0}" does not exist.'.format(namelist))
		raise SystemExit

	with open(namelist, 'r') as r: lines = r.readlines()
	n_lines = len(lines)

	dic  = {}
	for i_line, line in enumerate(lines):
		line = line.rstrip('\r\n').split()
		if '=' not in line: continue
		
           	name = line[0]
                val  = line[-1]
        
                if name == 'el_from' or name == 'el_to':
          		val = int(val)
                elif name == 'n_freq':
                    val = int(val)
                elif name == 'freq_from' or name == 'freq_to':
                    val = float(val)
                else:
                    val = val
        
                dic[name] = val

	return dic

#=================================================================================================================
def read_models(dic_namelist):
	"""
	Read the input models in GYRE format, and return a list of the dictionary containing the data.
	The list of available files are taken from rw.get_list_of_models(), and then passed to rw.read_multiple_gyre_files().

	@param dic_namelist: dictionary with the user namelist specifications
	@type dic_namelist: dictionary
	@return: list of dictionaries containing the .gyre input files
	@rtype: list of dictionaries
	"""
	list_files = get_list_of_models(dic_namelist)

	return read_multiple_gyre_in(list_files)

#=================================================================================================================
def get_list_of_models(dic_namelist):
  """
  Based on the "dir_models", get the list of all available input .gyre files.
  @param dic_namelist: The namelist items are passed as a dictionary. This dictionary is created by rw.read_namelist()
  @type dic_namelist: dictionary
  """
  dir_models = dic_namelist['dir_models']
  if not os.path.isdir(dir_models):
    logging.error('rw: get_list_of_models: the dictionary {0} does not exist'.format(dir_models))
    raise SystemExit

  if dir_models[-1] != '/': dir_models += '/'
  file_search_string  = dic_namelist['file_search_string']
  list_files          = glob.glob(dir_models + file_search_string)

  return list_files

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================

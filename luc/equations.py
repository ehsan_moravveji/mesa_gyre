
import sys, os, glob, shutil
import logging
import numpy as np
import initials, aux

#=================================================================================================================
def ledoux_radial_y(zeta, y, sigma2, adim):
    """
    y = d_zeta_dx
    Based on Eq. (6.36) in Smeyers and Van Hoolst (2010)
    @param zeta: relative eigen-displacement: zeta = xi/r
    @type zeta: numpy ndarray
    @param y: first derivative of zeta w.r.t. x, i.e. y = d_zeta_dx. Note that y is dimensionless
    @type y: numpy ndarray
    @param sigma2: trial frequency
    @type sigma2: float
    @param adim: numpy record array containing the "dimensionless" structre variables
    @type adim: numpy record array
    @return: the first derivative of zeta w.r.t. x. This is already defined, and receive as y
    @rtype: numpy ndarray
    """
    return y

#=================================================================================================================
def ledoux_radial_dy_dx(zeta, y, sigma2, adim):
    """
    dy_dx = d2_zeta_dx2
    Based on Eq. (6.36) in Smeyers and Van Hoolst (2010)
    """
    x               = adim['x']
    rho_adim  = adim['rho_adim']
    c2_adim    = adim['c2_adim']
    g_adim      = adim['g_adim']

    rho_c2      = rho_adim * c2_adim

    d_rho_c2_dx = aux.fin_diff(x=x, y=rho_c2)
    d_ln_rho_c2_dx = d_rho_c2_dx / rho_c2

    term1       = - (d_ln_rho_c2_dx + 4./x) * y
    term2       = - (sigma2/c2_adim + 3.*d_ln_rho_c2_dx/x + 4.*g_adim/(x*c2_adim)) * zeta

    return term1 + term2

#=================================================================================================================
def d2_xi_dx2(el, sigma2, y1, y2, y3, y4, adim, coeff):
    """
    d2_xi_dx2 = G(el, sigma2, x, y1, y2, y3, y4, data, coeff)
    @param el: mode spherical degree
    @type el: integer
    @param sigma2: trial adimensional frequency
    @type sigma2: float
    @param y1, y2, y3, y4: vectors of unknowns. They are defiened in the functions below.
    @type y1, y2, y3, y4: numpy ndarray
    @param adim: dimensionless structure variables. E.g. g_adim = adim['g_adim']
    @type adim: numpy record array
    @param coeff: matrix of coefficients K_1 to K_4. They are accessible as e.g. K_1 = coeff['K_1']
    @type coeff: numpy record array
    @return:
    @rtype:
    """
    K_1 = coeff['K_1']
    K_2 = coeff['K_2']
    K_3 = coeff['K_3']
    K_4 = coeff['K_4']

    x       = adim['x']
    c2_adim = adim['c2_adim']
    g_adim  = adim['g_adim']

    if len(x) != len(K_1):
        loggig.error('equations: d2_xi_dx2: Incompatible array shapes.')
        raise SystemExit

    term1 = -4/x * y4
    term2 = (el*(el+1)-2.) / x**2.0 * y3
    term3 = y2
    term4 = (c2_adim/g_adim * K_1/sigma2 - 2. / x) * y1

    return term1 + term2 + term3 + term4

#=================================================================================================================
def d2_alpha_dx2(el, sigma2, y1, y2, y3, y4, adim, coeff):
    """
    d2_alpha_dx2 = F(el, omega2, x, y1, y2, y3, y4, coeff)
    @param el: mode spherical degree
    @type el: integer
    @param sigma2: trial adimensional frequency
    @type sigma2: float
    @param y1, y2, y3, y4: vectors of unknowns. They are defiened in the functions below.
    @type y1, y2, y3, y4: numpy ndarray
    @param coeff: matrix of coefficients K_1 to K_4. They are accessible as e.g. K_1 = coeff['K_1']
    @type coeff: numpy record array
    @return:
    @rtype:
    """
    K_1 = coeff['K_1']
    K_2 = coeff['K_2']
    K_3 = coeff['K_3']
    K_4 = coeff['K_4']

    x       = adim['x']
    c2_adim = adim['c2_adim']

    if len(x) != len(K_1):
        loggig.error('equations: d2_alpha_dx2: Incompatible array shapes.')
        raise SystemExit

    term1 = -K_2 * y2
    term2 = -(K_1/sigma2 + K_3 + sigma2/c2_adim) * y1
    term3 = -K_4 * y4

    return term1 + term2 + term3

#=================================================================================================================
def alpha(el, sigma2, y1, y2, y3, y4, coeff):
    """
    By definition:
        alpha = y1
    """
    return y1

#=================================================================================================================
def d_alpha_dx(el, sigma2, y1, y2, y3, y4, coeff):
    """
    By definition:
        d_alpha_dx = y2
    """
    return y2
#=================================================================================================================
def xi(el, sigma2, y1, y2, y3, y4, coeff):
    """
    By definition:
        xi = y3
    """
    return y3

#=================================================================================================================
def d_xi_dx(el, sigma2, y1, y2, y3, y4, coeff):
    """
    By definition:
        d_xi_dx = y4
    """
    return y4

#=================================================================================================================

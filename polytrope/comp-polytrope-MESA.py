#! /opt/local/bin/python

import numpy as np
import matplotlib.pyplot as plt

def comp_mesa_vs_polytrope():
  """
  To compare a pre-calculated MESA model (rho, P, T, radius, etc) with a 
  plytrope of index n. Set the path to the model, and the polytropic index manually.
  """
 
  from mesa_gyre_read import read_single_mesa_file as rd

  profile = ['/Users/ehsan/programs/models/school2013/ex-01/sun_zams/LOGS/profile17.data']
  n1 = 1.5; n2 = 3.0
  xi1, tht1, d_tht1 = poly(n1)
  xi1_norm = xi1/xi1.max()
  pres1 = np.power(tht1, n1+1)
  xi2, tht2, d_tht2 = poly(n2)
  xi2_norm = xi2/xi2.max()
  pres2 = np.power(tht2, n2+1)
  dic_prof = rd(1, profile, is_hist=False, is_prof=True)
  nlines = len(dic_prof)
  logP = dic_prof['logP']
  P = np.power(10.0, logP); Pc = P[nlines-1]
  P_norm = P/P.max()
  logR = dic_prof['logR']
  R = np.power(10.0, logR) ; Rs = R[0]
  R_norm = R/Rs
  logT = dic_prof['logT']
  T = np.power(10.0, logT)
  T_norm = T/T.max()
  Teff = dic_prof['Teff']
  
  plt.figure(figsize=(8,8), dpi=200)
  fig, (ax0, ax1) = plt.subplots(nrows=2)
  plt.xlim(-0.05, 1.05)
  plt.ylim(-0.05, 1.05)
  plt.xlabel(r'${\rm Normalized\, Radius}\, \xi=r/R$')
  plt.ylabel(r'${\rm Normalized\, Temperature}\, T/T_{\rm center}$')

  ax0.plot(xi1_norm, tht1, linewidth=2, linestyle='-', color='blue', label=r'$n=1.5$')
  ax0.plot(xi2_norm, tht2, linewidth=2, linestyle='-', color='red', label=r'$n=3.0$')
  ax0.plot(R_norm, T_norm, linewidth=2, linestyle='-', color='black', label=r'${\rm MESA}\, 1.0M_\odot$')
  ax0.legend(loc='upper right', shadow=True)
  ax0.set_title(r'${\rm Normalized\, Temperature}\, T/T_{\rm center}$')
  ax0.text(0.05, 0.1, r'$T_{\rm eff}=$'+str(int(Teff))+' K', color='b')

  ax1.plot(xi1_norm, pres1, linewidth=2, linestyle='-', color='blue', label=r'$n=1.5$')
  ax1.plot(xi2_norm, pres2, linewidth=2, linestyle='-', color='red', label=r'$n=3.0$')
  ax1.plot(R_norm, P_norm, linewidth=2, linestyle='-', color='black', label=r'${\rm MESA}\, 1.0M_\odot$')
  ax1.legend(loc='upper right', shadow=True)
  ax1.set_title(r'${\rm Normalized\, Pressure}\, P/P_{\rm center}$')
  ax1.text(0.05, 0.1, r'$P_{\rm center}=$'+str(int(Pc)), color='b')

  fig.savefig('comp_mesa_vs_polytrope.pdf', dpi=200)
  plt.close()  
  
  print dic_prof.keys()
  
  return  

def poly(n):
  """
  Solve the Lane-Embden Equation, by first-order numerical integration.
  @param n: the polytropic index. If n >= 5, then it stops after 1000 iterations
  @type n: float
  @return: numpy arrays for theta and d_theta_d_xi
  @rtype: numpy arrays  
  """
  
  #n = float(raw_input('Choose Polytropic Index, n: '))
  if n >= 5:
    infinite = True
  else:
    infinite = False
    
  eps = 1e-3
  i = 0
  xi = np.array([1e-6])
  tht = 1.0
  tht_arr = np.array([1.0])
  d_tht = 0.0
  d_tht_arr = np.array([0.0])
  d_tht_nxt = 0.0
  x = xi[0]
  
  if not infinite:
    while tht > 0:
      d_tht_nxt = d_tht - eps * (2.0/x*d_tht + tht**n)
      tht_nxt = tht + eps * d_tht_nxt
      tht = tht_nxt
      tht_arr = np.append(tht_arr, tht)
      d_tht = d_tht_nxt
      d_tht_arr = np.append(d_tht_arr, d_tht)
      x += eps
      xi = np.append(xi, x)
      i += 1
  #else:

  print 'Location near the theta(0):'      
  print i, x, tht

  return xi, tht_arr, d_tht_arr  


def plot_polytrope(n):
  """
  Plots the solution of the Lane-Embden equation, i.e. theta and its first derivative
  """
  xi, tht_arr, d_tht_arr = poly(n)  
  
  fig = plt.figure(figsize = (8,6), dpi=200)
  plt.xlabel(r'$\xi$')
  plt.ylabel(r'$\theta_n (\xi)$')
  plt.xlim(-0.1,xi.max())
  plt.ylim(d_tht_arr.min()-0.05, 1.05)
  plt.plot(xi, tht_arr, linewidth = 2, linestyle='-', color = 'r', label = r'$\theta_n(\xi)$')
  plt.plot(xi, d_tht_arr, linewidth = 2, linestyle='--', color = 'b', label = r'$d\theta_n(\xi)/d\xi$')
  plt.legend(loc='upper right', shadow=True)
  fig.savefig('polytrope.pdf', dpi=200)
  plt.close()
  
  return
  
  
  
  
  
  
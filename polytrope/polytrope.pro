

  Pro polytrope, n, tht_arr, d_tht_arr
 
  if n ge 5.0 then begin
     infinite = 1
  endif else begin
     infinite = 0
  endelse
  
  m = 1000
  xi = findgen(m) / (m-1) *2.
  xi[0] = 1d-6
  eps = xi[m-1] - xi[m-2]

  x = xi[0]
  d_tht = 0d0
  tht = 1d0
  i = 0
  tht_arr = dblarr(10*m)
  d_tht_arr = dblarr(10*m)
  xi = dblarr(10*m)
  while x ge 0.0 do begin

      if tht lt 0 then begin
         print, i, x, tht, d_tht
         goto, continue
      endif
      if infinite and i eq 10*m-1 then goto, continue
      
      d_tht_nxt = d_tht - eps * (2d0/x*d_tht + tht^n)
      tht_nxt = tht + eps * d_tht_nxt
      tht = tht_nxt
      tht_arr[i] = tht
      d_tht = d_tht_nxt
      d_tht_arr[i] = d_tht
      xi[i] = x
      x += eps
      i ++
  endwhile
  continue:

  indx = where(xi gt 0)
  xi = xi[indx]
  tht_arr = tht_arr[indx]
  d_tht_arr = d_tht_arr[indx]

    
; 
;   set_plot, 'x'
;   !p.font = 0
;   window, 0, title = 'Lane-Emden Solution'
;   plot, xi, tht_arr
  

  end

"""
This module provides several plotting routines to support the inversion module.
"""

import sys, os, glob
import logging
import numpy as np
import pylab as plt

###############################################################################
###############################################################################
###############################################################################
# def
###############################################################################
def std_vs_something(xvals, std_arr, x_from=None, x_to=None, xtitle=None, xlog=None, x_times=None,
        y_form=None, y_to=None, ytitle=None, ylog=None, y_times=None, file_out=None):
    """
    Because the standard deviation of the frequency residuals can be a measure of how well the quality of
    fits improve, it is nice to have a bookkeeping of the progress/departure towards/from solution as a
    function of some varying dimension. The latter can be e.g. the width of a Gaussian that explains the
    envelope of the shape function.
    @param xvals: the values for the x-axis
    @type xvals: list or ndarray
    @param std_arr: list of standard deviations obtained per the change in the parameter xvals
    @return: None
    @rtype: None
    """
    if file_out is None: return

    xvals = np.array(xvals)
    std_arr = np.array(std_arr)

    if xlog is not None: xvals = np.log10(xvals)
    if x_times is not None: xvals *= x_times
    if ylog is not None: std_arr = np.log10(std_arr)
    if y_times is not None: std_arr *= y_times

    if x_from is None: x_from = np.min(xvals)
    if x_to is None:      x_to     = np.max(xvals)
    if y_form is None: y_form  = np.min(std_arr) * 0.8
    if y_to is None:     y_to       = np.max(std_arr) * 1.2
    if xtitle is None:    xtitle = 'Varying Parameter'
    if ytitle is None:    ytitle = 'Frequency Standard Deviation [Hz]'

    ######################
    # Prepare the plot
    ######################
    from matplotlib import rcParams
    if rcParams['text.usetex']:
        rcParams['text.usetex'] = False

    with plt.xkcd():
        fig, ax = plt.subplots(1, figsize=(6, 4))

        ax.scatter(xvals, std_arr, s=36, color='green', marker='*')

        ax.set_xlim(x_from, x_to)
        if xtitle: ax.set_xlabel(xtitle)
        ax.set_ylim(y_form, y_to)
        if ytitle: ax.set_ylabel(ytitle)

        plt.tight_layout()
        plt.savefig(file_out)
        print ' - plotter: std_vs_something: saved {0}'.format(file_out)
        plt.close()

    return None

###############################################################################
def compare_structure_before_and_after_deformation(dic_gyre_before, dic_gyre_after, column_name,
               xaxis, x_from=None, x_to=None, xtitle=None, y_form=None, y_to=None, ytitle=None,
               xlog=False, ylog=False, file_out=None):
    """
    Compare the structure variable that is modified by multiplication with a new shape factor, before and
    after deformation.
    """
    if file_out is None: return

    before = dic_gyre_before['gyre_in']
    after    = dic_gyre_after['gyre_in']
    names = before.dtype.names
    if xaxis not in names:
        logging.error('plotter: compare_structure_before_and_after_deformation: {0} not among the availble GYRE columns'.format(xaxis))
        sys.exit('Error: plotter: compare_structure_before_and_after_deformation: {0} not among the availble GYRE columns'.format(xaxis))
    if column_name not in names:
        logging.error('plotter: compare_structure_before_and_after_deformation: {0} not among the availble GYRE columns'.format(column_name))
        sys.exit('Error: plotter: compare_structure_before_and_after_deformation: {0} not among the availble GYRE columns'.format(column_name))

    xvals_before = before[xaxis]
    xvals_after    = after[xaxis]
    yvals_before = before[column_name]
    yvals_after    = after[column_name]

    if xlog:
        xvals_before = np.log10(xvals_before)
        xvals_after    = np.log10(xvals_after)
    if ylog:
        yvals_before = np.log10(yvals_before)
        yvals_after    = np.log10(yvals_after)
    if x_from is None: x_from = np.min(xvals_before)
    if x_to is None:      x_to     = np.max(xvals_before)
    if y_form is None: y_form  = np.min(yvals_before)
    if y_to is None:     y_to       = np.max(yvals_before)
    if xtitle is None:    xtitle = xaxis.replace('_', ' ')
    if ytitle is None:    ytitle = column_name.replace('_', ' ')

    ######################
    # Prepare the plot
    ######################
    from matplotlib import rcParams
    # from matplotlib import pyplot
    if rcParams['text.usetex']:
        rcParams['text.usetex'] = False
        # print '   Succeeded to turn off: rcParams["text.usetex"]'

    with plt.xkcd():
        # pyplot.style.use('ggplot')
        fig, ax = plt.subplots(1, figsize=(6, 4))

        ax.plot(xvals_before, yvals_before, linestyle='solid', color='blue', lw=2, label='Before')
        ax.plot(xvals_after, yvals_after, linestyle='dashed', color='red', lw=2, label='After')

        ax.set_xlim(x_from, x_to)
        if xtitle: ax.set_xlabel(xtitle)
        ax.set_ylim(y_form, y_to)
        if ytitle: ax.set_ylabel(ytitle)

        plt.tight_layout()
        plt.savefig(file_out)
        print ' - plotter: compare_structure_before_and_after_deformation: saved {0}'.format(file_out)
        plt.close()

    return None

###############################################################################
def show_shape_function(dic_gyre, shape, xaxis, x_from=None, x_to=None, xtitle=None, file_out=None):
    """
    This function plots the shape function that we use to modify the structure
    """
    if file_out is None: return

    gyre_in = dic_gyre['gyre_in']
    names = gyre_in.dtype.names
    if xaxis not in names:
        logging.error('plotter: show_shape_function: {0} not among the availble GYRE columns'.format(xaxis))
        sys.exit('Error: plotter: show_shape_function: {0} not among the availble GYRE columns'.format(xaxis))

    xvals = gyre_in[xaxis]
    N2 = gyre_in['brunt_N2']
    N2[N2 == 0.0] = 1e-50
    log_N2 = np.log10(N2)
    if xaxis == 'temperature': xvals = np.log10(xvals)
    if x_from is None: x_from = np.min(xvals)
    if x_to is None:      x_to     = np.max(xvals)
    if xtitle is None:    xtitle = xaxis.replace('_', ' ')

    ######################
    # Prepare the plot
    ######################
    from matplotlib import rcParams
    # from matplotlib import pyplot
    if rcParams['text.usetex']:
        rcParams['text.usetex'] = False
        # print '   Succeeded to turn off: rcParams["text.usetex"]'

    with plt.xkcd():
        # pyplot.style.use('ggplot')
        fig, ax = plt.subplots(1, figsize=(6, 4))
        other = ax.twinx()

        ax.plot(xvals, shape, linestyle='solid', color='blue', lw=2, label='Factor')
        other.plot(xvals, log_N2, linestyle='dashed', color='red', lw=2, label='Brunt')

        ax.set_xlim(x_from, x_to)
        ax.set_xlabel(r''+xtitle)
        ax.set_ylim(0.5, 1.5)
        ax.set_ylabel('Shape Function')

        other.set_ylim(-7, -4.5)
        other.set_ylabel('Brunt-Vaisala Frequency')

        plt.tight_layout()
        plt.savefig(file_out)
        print ' - plotter: show_shape_function: saved {0}'.format(file_out)
        plt.close()

    return None

###############################################################################


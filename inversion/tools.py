
"""
This module provides some tools for poor-man's inversion with GYRE
"""

import sys, os, glob
import logging
import subprocess
import numpy as np
from mesa_gyre import stars, read, write
from submit_jobs import job_array_commons as jac
from submit_jobs import job_array_gyre as jag

###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################
def modify_structure(gyre_in_file, column_name, shape_function, dic_shape, new_gyre_in_file):
    """
    This function starts with an initial GYRE input file, and reads it. Then, one of the columns, specifies
    by column_name is picked up and modified by multiplication with the an ad-hoc shape function,
    passed by shape_function.
    Subsequently, the full modified column along with the rest of the original columns that are kept
    intact are written again on disc as a new GYRE compatible file specifid by the name new_gyre_in_file.
    @param gyre_in_file: full path to the input GYRE file to be read.
    @type gyre_in_file: string
    @param column_name: the name of a column that we are going to tweak. It should be already available
           as one of the original columns in the input GYRE file.
    @type column_name: string
    @param shape_function: a callable function that returns the shape of the modification profile. This function
           should already receive the input structure, so we pass the gyre_in dictionary to it.
    @type shape_function: function
    @param dic_shape: accompanying dictionary that passes the specific setup and shape of the shape_function.
            Depending on the adopted function, it can receive very different keys, value pairs. E.g. we pass the
            amplitude A0, logT0, mu and the frequency nu for specification of exp_sin() through this dictionary.
    @type dic_shape: dictionary
    @param new_gyre_in_file: The full path to a file where we will subsequently store the modified structure
           in GYRE-compatible format. For this purpose, we use mesa_gyre.write.dic_gyre_in_to_ascii()
           This new file may or maynot fulfill the hydrostatic equilibrium, as it used to be before tweaking.
           Specially, if profiles of pressure, temperature, density or temperature gradients are tweaked, then
           internal MESA iterations are required to converge to a consistent structure. This is our "heel of Achilis".
    @type new_gyre_in_file: string
    @return: None
    @rtype: None
    """
    if not os.path.exists(gyre_in_file):
        logging.error('inversion: tools: modify_structure: {0} does not exist'.format(gyre_in_file))
        raise SystemExit, 'Error: inversion: tools: modify_structure: {0} does not exist'.format(gyre_in_file)

    dic_gyre = read.read_gyre_in(gyre_in_file)
    gyre_in = dic_gyre['gyre_in']
    n = len(gyre_in)
    names = gyre_in.dtype.names
    if column_name not in names:
        logging.error('inversion: tools: modify_structure: {0} not among gyre_in valid columns!'.format(column_name))
        sys.exit('Error: inversion: tools: modify_structure: {0} not among gyre_in valid columns!'.format(column_name))

    modifying_shape = shape_function(dic_gyre, dic_shape)

    original_column = gyre_in[column_name]
    modified_column = original_column * modifying_shape
    gyre_in[column_name] = modified_column

    write.dic_gyre_in_to_ascii(dic_gyre, ascii_out=new_gyre_in_file)

    return None

###############################################################################
def exp_sin(dic_gyre, dic_shape):
    """
    The function which is used to perturb any structure quantity by multiplication. Here, the assume
    shape is
             exp_sin = 1 + A0 * A(T, T0, mu) * sin(nu * logT)
    where the A0 is a constant and the amplitude function is
             A(T, T0, mu) = exp( - (logT - logT0)**2 / mu**2  )
    This produces a sinusoidal pattern to capture fast variations inside an exponential bump for slow
    variations around unity. Thus, it preserves the shape of the structure quantity far from logT0, and
    modifies it around logT0.
    @param dic_gyre: dictionary with the input GYRE data. it is mainly returned after reading a file by
             mesa_gyre.read.read_gyre_in()
    @type dic_gyre: dictionary
    @return: an array that specifies the shape of the modifying function. It will be used later to multiply
             a structure variable with this shape function
    @rtype: ndarray
    """
    gyre_in = dic_gyre['gyre_in']
    n = len(gyre_in)
    T = gyre_in['temperature']
    logT = np.log10(T)

    if dic_shape.has_key('nu'):
        nu = dic_shape['nu']
    else:
        nu = 100.0
    if dic_shape.has_key('logT0'):
        logT0 = dic_shape['logT0']
    else:
        logT0 = 7.1
    if dic_shape.has_key('mu'):
        mu = dic_shape['mu']
    else:
        mu = 0.1
    if dic_shape.has_key('A0'):
        A0 = dic_shape['A0']
    else:
        A0 = 0.1

    amp = np.zeros(n) + A0 * np.exp( - (logT - logT0)**2 / mu**2)
    sin   = np.zeros(n) + np.sin( nu * logT)
    func = np.ones(n) + amp * sin

    return func

###############################################################################
def gen_a_trial_filename(dir_path, i_try, extension):
    """
    This function creates a full path to a file that is already stored, or is going to be stored in some
    directory, specified by dir_path. The naming is based on the try number, i_try, and a suffix will
    be appended to the name specified by extension.
    @param dir_path: full path to the directory where the file already exists or will be stored
    @type dir_path: string
    @param i_try: the iterator count, which specifies for which iteration we are storing information
    @type i_try: integer
    @param extension: the file extension to be appended to the end of the filename. Do not forget the
            seperator ".". possible extensions are '.gyre', '.h5', '.nmlst' or '.png'
    @type extension: string
    @return: full path to a hypothetical file
    @rtype: string
    """
    if dir_path[-1] != '/': dir_path += '/'

    return dir_path + get_try_name(i_try) + extension

###############################################################################
def get_try_name(i_try):
    """
    Based on the number of the attempt trial, we have to specify a unique name to the input model that
    is used, or the output gyre_out file, or the inlist for GYRE. This is achieved by labelling the files based
    on their iteration number, i_try.
    @param i_try: specifies the iteration number
    @type i_try: integer
    @return: a core name to be used to create input files, output files, and inlists
    @rtype: string
    """
    return 'Try-{0:03d}'.format(i_try)

###############################################################################
def attempt(gyre_in_file, base_nmlst_file, which_gyre, dic_star, sigma):
    """
    This function receives essential settings to load an existing GYRE model, and a sample inlist to
    run GYRE, and compute frequencies. Then, it compares the received frequencies with the observed
    frequencies and returns an array of deviations from observations.
    @param gyre_in_file: full path to the initial input GYRE model to begin with. This is either an original file
             already computed from forward grid of models, or is one of iterative models produced in the
             previous steps of this program, during the inversion iterations.
    @type gyre_in_file: string
    @param base_nmlst_file: full path to the existing base GYRE namelist file. The settings in this file is
             used to generate a new inlist with the input file and output file specified.
    @type base_nmlst_file: string
    @param which_gyre: full path to the GYRE executable or a bash-compatible command name for
             execution of GYRE. E.g. which_gyre = 'gyre_ad', or which_gyre = '/path/to/GYRE/bin/gyre_nad'
    @type which_gyre: string
    @param dic_star: dictionary containing all required information for a star that we are trying to invert
             its frequencies. This dictionary is normally returned from mesa_gyre.stars module
    @type dic_star: dictionary
    @param sigma: The uncertainty box around the observed lower and higher two frequencies to chop off
             the modelled data, and do the comparison.
    @type sigma: integer
    @return: The following information for each array of deviations is returned as a dictionary with the following
             keys:
             - dev: an array of deviations, with the same length as the number of observed frequencies. The deviations
                        are returned by a call to get_obs_vs_model_freq_deviations(). Read there for more information
             - var: the variance of the deviations
             - std: the standard deviation of deviations
             - min_dev: the minimum value of the absolute size of deviation
             - max_dev: the maximum value of the absolute size of deviation
    @rtype: dictionary
    """
    # Read the input GYRE file as a dictionary
    dic_gyre = read.read_gyre_in(gyre_in_file)

    # Read the sample namelist file and store it as a list of lines
    base_nmlst = jag.read_sample_namelist_to_list_of_strings(base_nmlst_file)

    # Adapt the sample namelist to the starting input GYRE file, and store the namelist
    path_new_nmlst = jag.gen_namelists(base_nmlst, [gyre_in_file])[0]

    # Regenerate the name of the expected output GYRE file, if gyre executes successfully.
    gyre_out_file = recreate_gyre_out_from_gyre_in_filename(gyre_in_file)

    # Call GYRE to get the list of frequencies
    call_status = call_gyre(which_gyre, path_new_nmlst)
    if not call_status:
        logging.error('inversion: tools: attempt: call_gyre() failed!')
        sys.exit('Error: inversion: tools: attempt: call_gyre() failed!')

    dic_gyre_out = read.read_multiple_gyre_files([gyre_out_file])[0]

    # Compare the observed and modelled frequencies, and retrieve the array of frequency deviations
    deviations = get_obs_vs_model_freq_deviations(dic_star, dic_gyre_out, sigma=sigma)
    if deviations is None:
        return None
    min_dev = np.min(np.abs(deviations))
    max_dev = np.max(np.abs(deviations))
    variance = np.var(deviations, dtype=np.float64)
    std_dev  = np.std(deviations, dtype=np.float64)

    # Write a modified GYRE file to disc with a new name?
    # write.dic_gyre_in_to_ascii(dic_gyre, ascii_out='output_models/test1.gyre')
    dic = {'dev':deviations, 'var': variance, 'std':std_dev, 'min_dev':min_dev, 'max_dev':max_dev}
    print ' - tools: attempt: deviations: std={0}, min_dev={1}, max_dev={2}'.format(std_dev, min_dev, max_dev)

    return dic

###############################################################################
def get_obs_vs_model_freq_deviations(dic_star, dic_gyre_out, sigma=10):
    """
    This function receives the dictionary for an observed stars, and takes the list_dic_freq out of it.
    Then, within the "sigma" box of uncertainty below the lowest frequency, and above the highest
    frequency, the modelled frequency list from dic_gyre_out is clipped off for comparison.
    For now, the number of modelled and observed frequencies within the observed range must exactly
    match. Otherwise, it is not possible to assess the point-to-point frequency deviations. In such a case
    the program returns False
    @param dic_star: dictionary with all star info, specifically list_dic_freq, received from mesa_gyre.stars
    @type dic_star: dictionary
    @param dic_gyre_out: GYRE output information as a dictionary, with extended list of mode order,
           frequencies, degree, etc. This is retrieved normally by a call to mesa_gyre.read.read_multiple_gyre()
    @type dic_gyre_out: dictionary
    @return: array with point to point deviations of the observed versus modelled frequencies in the sense
                      deviations = obs_freq - model_freq
           Thus, the array of deviations can attain both positive and negative values
    @rtype: ndarray
    """
    list_dic_freq = dic_star['list_dic_freq']
    obs_freq = np.array([ dic['freq'] for dic in list_dic_freq ])
    obs_freq_err = np.array([ dic['freq_err'] for dic in list_dic_freq ])
    ind_sort = np.argsort(obs_freq)
    obs_freq = obs_freq[ind_sort]
    obs_freq_err = obs_freq_err[ind_sort]
    n_obs_freq = len(obs_freq_err)

    lo_freq = obs_freq[0] - sigma * obs_freq_err[0]
    hi_freq = obs_freq[-1] + sigma * obs_freq_err[-1]

    mod_freq = np.real(dic_gyre_out['freq'])
    ind = np.where( (mod_freq <= hi_freq) & (mod_freq >= lo_freq) )[0]
    n_mod_freq = len(ind)
    if n_mod_freq != n_obs_freq:
        logging.warning('inversion: tools: get_obs_vs_model_freq_deviations: n_obs_freq != n_mod_freq :: {0} != {1}'.format(n_obs_freq, n_mod_freq))
        print '   Warning: inversion: tools: get_obs_vs_model_freq_deviations: n_obs_freq != n_mod_freq :: {0} != {1}'.format(n_obs_freq, n_mod_freq)
        return None
    mod_freq = mod_freq[ind]

    return obs_freq - mod_freq

###############################################################################
def recreate_gyre_out_from_gyre_in_filename(gyre_in_file):
    """
    Recreate the name of the output GYRE file by replacing the '/gyre_in/' string in the input path string
    by '/gyre_out/', and also the file extension from '.gyre' to '.h5'
    """
    return gyre_in_file.replace('gyre_in', 'gyre_out').replace('.gyre', '.h5')

###############################################################################
def call_gyre(which_gyre, path_nmlst):
    """
    This funtion creates an appropriate bash command to call and execute GYRE. Then, it waits until
    the run is over, and returns True if the execution succeeds. Otherwise, it returns False.
    @param which_gyre: full path to the GYRE executable. This should be decently provided by the calling
            script. It can be either gyre_ad or gyre_nad.
            I advice to provide the full path to the specific gyre executable that also specified the GYRE verison.
            E.g. which_gyre = '/home/user/gyre4.0/bin/gyre_nad'
            An additional space will be automatically added aftre which_gyre
    @type which_gyre: string
    @param path_nmlst: full path to where the input namelist for this specific job is stored, and GYRE will
            use the specified settings therein
    @return: True if succeeds, or False if fails
    @rtype: boolean
    """
    if not os.path.exists(path_nmlst):
        logging.error('inversion: tools: call_gyre: {0} does not exist'.format(path_nmlst))
        raise SystemExit, 'Error: inversion: tools: call_gyre: {0} does not exist'.format(path_nmlst)

    if not isinstance(which_gyre, str) or not isinstance(path_nmlst, str):
        logging.error('inversion: tools: call_gyre: which_gyre and path_nmlst should be string')
        raise SystemExit, 'Error: inversion: tools: call_gyre: which_gyre and path_nmlst should be string'

    cmnd = which_gyre + ' ' + path_nmlst

    exe = subprocess.Popen(cmnd, stdout=subprocess.PIPE, shell=True)
    output, error = exe.communicate()
    status = exe.wait()

    return status == 0

###############################################################################


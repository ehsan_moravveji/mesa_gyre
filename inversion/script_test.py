
import sys, os, glob
import logging
import tools, plotter
from mesa_gyre import stars, read, write
from submit_jobs import job_array_commons as jac
from submit_jobs import job_array_gyre as jag

###############################################################################
# JAG settings
jag.path_namelists = 'output_models/'
jag.gyre_out_prefix = ''  # e.g. 'ad-sum-'
jag.gyre_out_suffix = '.h5'  # e.g. '-sum.h5'

###############################################################################
# Choose the target star
dic_star_orig = stars.KIC_10526294()
dic_star         = stars.change_freq_err_by_factor(dic_star_orig, factor=3)
list_dic_freq  = dic_star['list_dic_freq']
list_dic_freq  = stars.check_obs_vs_model_freq_unit('Hz', list_dic_freq)
dic_star['list_dic_freq'] = list_dic_freq

###############################################################################
###############################################################################
###############################################################################
def main():
    """
    The main caller
    """
    if True:
        print ' - Oh boy, Invert it all :-)'
        # Setting Paths
        cwd = os.getcwd() + '/'
        dir_gyre_in = cwd + 'gyre_in/'
        dir_gyre_out = cwd + 'gyre_out/'
        dir_inputs = cwd + 'input_models/'
        dir_outputs = cwd + 'output_models/'
        dir_plots = cwd + 'plots/'
        init_gyre_in_file = dir_gyre_in + 'Model-00.gyre'
        base_nmlst_file  = dir_inputs + 'gyre.nmlst'

        # Specification of the run
        max_try = 25
        logT0_from = 7.4
        logT0_step = 0.02

        # Definition of few lists for bookkeeping of results
        list_result_i_try = []
        list_result_param = []
        list_result_deviations = []

        # Read the sample namelist file and store it as a list of lines
        base_nmlst = jag.read_sample_namelist_to_list_of_strings(base_nmlst_file)

        # Adapt the sample namelist to the starting input GYRE file, and store the namelist
        init_nmlst  = jag.gen_namelists(base_nmlst, [init_gyre_in_file])[0]

        dic_deviations = tools.attempt(init_gyre_in_file, init_nmlst, jag.which_gyre, dic_star, sigma=50)

        for i_try in range(max_try):
            if i_try == 0:
                init_gyre_in_file = init_gyre_in_file
            else:
                init_gyre_in_file  = tools.gen_a_trial_filename(dir_gyre_in, i_try - 1, '.gyre')
            new_gyre_in_file = tools.gen_a_trial_filename(dir_gyre_in, i_try, '.gyre')
            new_nmlst_file = tools.gen_a_trial_filename(dir_outputs, i_try, '.nmlst')

            logT0 = logT0_from - i_try * logT0_step
            dic_exp_sin = {'logT0':logT0, 'nu': 500}

            print ' - Try: {0}; logT0={1}'.format(i_try, logT0)
            print '   gyre_in: {0}'.format(init_gyre_in_file)
            print '   nmlst: {0}'.format(new_nmlst_file)
            print '   new gyre_in: {0}'.format(new_gyre_in_file)
            print

            tools.modify_structure(init_gyre_in_file, 'brunt_N2', tools.exp_sin, dic_exp_sin, new_gyre_in_file)
            dic_deviations = tools.attempt(new_gyre_in_file, init_nmlst, jag.which_gyre, dic_star, sigma=50)
            if isinstance(dic_deviations, dict):
                list_result_i_try.append(i_try)
                list_result_param.append(logT0)
                list_result_deviations.append(dic_deviations)


            if False:
                # For plotting only
                dic_gyre = read.read_gyre_in(init_gyre_in_file)
                shape = tools.exp_sin(dic_gyre, dic_exp_sin)
                plot_name = tools.gen_a_trial_filename(dir_plots, i_try, '-shape.png')
                plotter.show_shape_function(dic_gyre, shape, xaxis='temperature', x_from=7.5, x_to=4,
                    xtitle='Log Temperature [K]', file_out=plot_name)

            if False:
                # For plotting only
                dic_before = read.read_gyre_in(init_gyre_in_file)
                dic_after    = read.read_gyre_in(new_gyre_in_file)
                plot_name = tools.gen_a_trial_filename(dir_plots, i_try, '-deform.png')
                plotter.compare_structure_before_and_after_deformation(dic_before, dic_after, 'brunt_N2',
                   'temperature', x_from=7.3, x_to=6.5, xtitle='Log Temperature', ytitle='Brunt-Vaisala Frequency',
                   y_form=-6.1, y_to=-4.5, xlog=True, ylog=True, file_out=plot_name)

        if len(list_result_i_try) > 0:
            # To plot the std of results versus sliding parameter, logT0
            list_std = [dic['std'] for dic in list_result_deviations]
            plot_name = dir_plots + 'STD-vs-logT0.png'
            plotter.std_vs_something(list_result_param, list_std, x_from=7.5, x_to=6, xtitle='Pos. of Sliding Bump:   log(T0) [K]',
                ytitle='Stand. Dev. [x10^8 Hz]', y_times=1e8, file_out=plot_name)


    return None

###############################################################################
if __name__ == '__main__':
    status = main()
    sys.exit(status)
###############################################################################

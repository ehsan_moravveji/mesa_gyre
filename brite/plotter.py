
"""
This module is a suite of Python routines to visualize some of the analysis related to the
BRITE data.
"""

import sys, os, glob
import logging
import numpy as np 
import pylab as plt

from ivs.timeseries.pergrams import scargle
from ivs.timeseries.windowfunctions import getWindowFunction 

###################################################################################################
__author__  = 'Ehsan Moravveji'
__date__    = '27.07.2016'
__version__ = '1'

###################################################################################################
###################################################################################################
###################################################################################################
def show_mirrored_scargles(self1, self2, file_out=None):
  """
  For two filters, show the resulting Scargle for each filter as a mirrored figure.
  """
  if file_out is None: return False 
  if self1.has_dft is False: return False 
  if self2.has_dft is False: return False 

  f1 = self1.dft_freq
  a1 = self1.dft_amp
  f2 = self2.dft_freq
  a2 = self2.dft_amp

  fig, ax = plt.subplots(1, figsize=(6, 6))
  plt.subplots_adjust(left=0.09, right=0.98, bottom=0.10, top=0.97)

  ax.plot(f1, a1, linestyle='solid', color=self1.passband, lw=1, zorder=1)
  ax.plot(f2, -a2, linestyle='solid', color=self2.passband, lw=1, zorder=1)

  ax.set_xlim(f1[0], f1[-1])
  ax.set_xlabel(r'Frequency [day$^{-1}$]')
  ax.set_ylim(-np.max(a2)*1.05, np.max(a1)*1.05)
  ax.set_ylabel(r'Amplitude [{0}]'.format(self1.flux_unit))

  if self1.has_spec_win:
    inset1 = fig.add_axes([0.60, 0.75, 0.35, 0.20])
    ft     = self1.spec_win_freq
    at     = self1.spec_win_amp
    at     /= np.max(at)  # normalize it to unity
    inset1.plot(ft, at, linestyle='solid', lw=1, color=self1.passband, zorder=1)
    inset1.set_xlim(ft[0], ft[-1])
    inset1.set_ylim(0, np.max(at)*1.05)

  if self2.has_spec_win:
    inset2 = fig.add_axes([0.60, 0.16, 0.35, 0.20])
    fb     = self2.spec_win_freq
    ab     = self2.spec_win_amp
    ab     /= np.max(ab)  # normalize it to unity
    inset2.plot(fb, ab, linestyle='solid', lw=1, color=self2.passband, zorder=1)
    inset2.set_xlim(fb[0], fb[-1])
    inset2.set_ylim(0, np.max(ab)*1.05)

  plt.savefig(file_out, transparent=True)
  print ' - brite: plotter: show_mirrored_scargles: saved {0}'.format(file_out)
  plt.close()

  return None

###################################################################################################
def show_scargle(self, file_out=None):
  """
  Plot the Scargle periodogram, if computed, and the spectral window, if computed, as a figure
  """
  if file_out is None: return False 
  if self.has_dft is False: return False 

  f = self.dft_freq
  a = self.dft_amp

  fig, ax = plt.subplots(1, figsize=(8, 4))
  plt.subplots_adjust(left=0.07, right=0.98, bottom=0.13, top=0.97)

  ax.plot(f, a, linestyle='solid', color='grey', lw=1, zorder=1)

  ax.set_xlim(f[0], f[-1])
  ax.set_xlabel(r'Frequency [day$^{-1}$]')
  ax.set_ylim(0, np.max(a)*1.05)
  if self.flux_unit is not None:
    ax.set_ylabel(r'Amplitude [{0}]'.format(self.flux_unit))
  else:
    ax.set_ylabel(r'Normalized Amplitude')

  if self.has_spec_win:
    inset = fig.add_axes([0.60, 0.60, 0.35, 0.35])
    f2    = self.spec_win_freq
    a2    = self.spec_win_amp
    a2    /= np.max(a2)  # normalize it to unity
    inset.plot(f2, a2, linestyle='solid', lw=1, color='grey', zorder=1)

    inset.set_xlim(f2[0], f2[-1])
    inset.set_ylim(0, np.max(a2)*1.05)

  plt.savefig(file_out, transparent=True)
  print ' - brite: plotter: show_scargle: saved {0}'.format(file_out)
  plt.close()

  return None

###################################################################################################

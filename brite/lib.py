
"""
This module is a suite of Python routines to interpret and analyze BRITE photometry
"""

import sys, os, glob
import copy
import logging
import numpy as np 

from ivs.timeseries.pergrams import scargle, pdm
from ivs.timeseries.windowfunctions import getWindowFunction 

###################################################################################################
__author__  = 'Ehsan Moravveji'
__date__    = '27.07.2016'
__version__ = '1'

###################################################################################################
class light_curve:

  def __init__(self, filename):
    self.filename = filename

    # the following attributes are set to a default (meaningless) value
    self.instrument = '' # e.g. UniBRITE, or list of BRITEs
    self.passband = ''   # allowed values: 'b' and 'r'
    self.response_function = 0

    self.hjd = 0
    self.hjd0 = 0        # offset in HJD, typically 2456000.0000
    self.flux = 0        # whether or not the measurements are in mag or flux, we call it flux
    self.flux_err = 0
    self.flux_unit = ''  # options: 'mag', 'mmag', 'mumag', 'rel_f', 'ppt', 'ppm'
    self.n_obs_per_orb = 0 # number of observations per one orbit

    self.weight = 0      # point weight based on flux error and the number of observed points per orbit

    self.num_points = 0
    self.timespan = 0

    self.has_spec_win  = False
    self.spec_win_freq = 0    # spectral window frequency
    self.spec_win_amp  = 0    # spectral window amplitude 

    self.has_dft  = False
    self.dft_freq = 0         # Scargle Discrete Fourier Transform frequency
    self.dft_amp  = 0         # Scargle Discrete Fourier Transform amplitude


  ###############
  # class methods
  ###############

  ###############
  def set_instrument(self, instrument):
    self.instrument = instrument

  ###############
  def set_passband(self, passband):
    if passband not in ['b', 'r']:
      logging.error('lib: light_curve: set_passband: Unsupported passband.')
      raise SystemExit, 'lib: light_curve: set_passband: Unsupported passband.'
    self.passband = passband

  ###############
  def set_response_function(self, response_function):
    self.response_function = response_function

  ###############
  def set_hjd(self, hjd):
    self.hjd = hjd 

  ###############
  def set_hjd0(self, hjd0):
    self.hjd0 = hjd0

  ###############
  def set_flux(self, flux):
    self.flux = flux 

  ###############
  def set_flux_err(self, flux_err):
    self.flux_err = flux_err

  ###############
  def set_flux_unit(self, unit):
    self.flux_unit = unit

  ###############
  def convert_flux_unit(self, unit):
    if unit not in ['mag', 'mmag', 'mumag', 'rel_f', 'ppt', 'ppm']:
      logging.error('lib: class light_curve: convert_flux_unit: unsupported unit')
      raise SystemExit, 'lib: class light_curve: convert_flux_unit: unsupported unit'
    old_unit = self.flux_unit
    
    if unit == old_unit: return True
    
    if unit in ['mag', 'mmag', 'mumag']: # allowed conversion

      if old_unit == 'mag' and unit == 'mmag':
        conv      = 1e3
      if old_unit == 'mag' and unit == 'mumag':
        conv      = 1e6
      if old_unit == 'mmag' and unit == 'mag':
        conv      = 1e-3
      if old_unit == 'mmag' and unit == 'mumag':
        conv      = 1e3
      if old_unit == 'mumag' and unit == 'mag':
        conv      = 1e-6
      if old_unit == 'mumag' and unit == 'mmag':
        conv      = 1e-3
    
      self.flux      *= conv
      self.flux_err  *= conv
      self.flux_unit = unit

    elif unit in ['rel_f', 'ppt', 'ppm']: # allowed conversion

      if old_unit == 'rel_f' and unit == 'ppt':
        conv      = 1e3 
      if old_unit == 'rel_f' and unit == 'ppm':
        conv      = 1e6 
      if old_unit == 'ppt' and unit == 'rel_f':
        conv      = 1e-3
      if old_unit == 'ppt' and unit == 'ppm':
        conv      = 1e3
      if old_unit == 'ppm' and unit == 'rel_f':
        conv      = 1e-6
      if old_unit == 'ppm' and unit == 'ppt':
        conv      = 1e-3

      self.flux      *= conv
      self.flux_err  *= conv
      self.flux_unit = unit
    
    else: # conversion between magnitude-base and flux-base are not permitted/supported
      logging.error('lib: class light_curve: convert_flux_unit: unsupported conversion between magnitude and flux!')
      raise SystemExit, 'lib: class light_curve: convert_flux_unit: unsupported conversion between magnitude and flux!'

  ###############
  def set_weight(self):
    return False # not supported yet

  ###############
  def read_light_curve(self):
    rec = read_pigulski(self.filename)
    self.hjd = rec['hjd']
    self.flux = rec['flux']
    self.flux_err = rec['flux_err']
    self.n_obs_per_orb = rec['n_obs']

    self.num_points = len(rec)
    self.timespan   = self.hjd[-1] - self.hjd[0]

  ###############
  def do_dft(self, f0=None, fn=None, df=None, norm='amplitude', weights=None, single=False):
    """
    norm can be: 'distribution', 'amplitude', 'density'
    """
    hjd  = self.hjd
    flux = self.flux
    if weights is not None:
      pass
    elif self.weight is not 0:
      weights = self.weight

    f, a = do_scargle(hjd, flux, f0=f0, fn=fn, df=df, norm=norm, weights=weights, single=single)
    
    self.dft_freq = f 
    self.dft_amp  = a
    self.has_dft  = True

  ###############
  def do_spec_win(self, f0=None, fn=None, df=None, norm='amplitude', weights=None, single=False):
    hjd  = self.hjd
    ones = np.ones(len(hjd))
    if weights is not None:
      pass
    elif self.weight is not 0:
      weights = self.weight

    f, a = do_scargle(hjd, ones, f0=f0, fn=fn, df=df, norm=norm, weights=weights, single=single)

    self.spec_win_freq = f 
    self.spec_win_amp  = a
    self.has_spec_win  = True

  ###############
  def get_dft(self):
    if not self.has_dft:
      logging.error('lib: get_dft: DFT not computed. Use method do_dft() first.')
      raise SystemExit, 'lib: get_dft: DFT not computed. Use method do_dft() first.'

    return self.dft_freq, self.dft_amp


###################################################################################################
###################################################################################################
def multiply_scargles(self1, self2):
  """
  In case of BRITE data, the duty cycle is poor, there are complicated gaps, the reduction is tricky,
  and different instruments have different window functions. For long-period variables, the DFT in 
  different filters can be multiplied by one another, provided they have identical frequency bins, 
  and then the product "may" exhibit a strong peak which is present in more than one filters; thus, 
  the uncorrelated signals will die out.
  @param self1: the first light_curve instance 
  @type self1: class object
  @param self2: the second light_curve instance
  @type self2: class object
  @return: an intance of the light_curve class, with most fields set back to default, but the following
           attributes are adapted:
           - has_dft = True
           - dft_freq = the common frequency array of both input objects
           - dft_amp = multiplication of both scargle amplitudes after normalization
           - flux_unit = None
  @rtype: class object           
  """
  if not self1.has_dft: 
    logging.error('lib: multiply_scargles: Scargle for first light curve not computed yet!')
    print 'lib: multiply_scargles: Scargle for first light curve not computed yet!'
    return False 
  if not self2.has_dft: 
    logging.error('lib: multiply_scargles: Scargle for second light curve not computed yet!')
    print 'lib: multiply_scargles: Scargle for second light curve not computed yet!'
    return False 

  f1 = self1.dft_freq
  a1 = normalize_scargle(self1)
  f2 = self2.dft_freq
  a2 = normalize_scargle(self2)

  if len(f1) != len(f2): 
    logging.error('lib: multiply_scargles: The frequency bins for both scargles must have identical length.')
    print 'lib: multiply_scargles: The frequency bins for both scargles must have identical length.'
    return False

  a  = a1 * a2
  u  = copy.deepcopy(self1)

  u.filename = ''
  u.instrument = 'Multiplication of Blue and Red'
  u.passband = 'b and r'
  u.response_function = 0
  u.hjd = 0
  u.hjd0 = 0
  u.flux = 0
  u.flux_err = 0
  u.flux_unit = None
  u.n_obs_per_orb = 0
  u.weight = 0
  u.num_points = 0
  u.timespan = 0
  u.has_spec_win = False 
  u.spec_win_freq = 0
  u.spec_win_amp = 0

  # and now ...
  u.has_dft = True 
  u.dft_freq = f1 
  u.dft_amp = a 

  return u

###################################################################################################
def normalize_scargle(self):
  """
  To multiply more than one Scargle periodogram with one another, we better normalize them first,
  so that the highet peak in each light_curve.dft_amp is set to unity. However, we do not overwrite 
  the normalized self.dft_amp, since that may confuse other routines. Instead, we just return the 
  normalized amplitude spectrum as a numpy ndarray.
  """
  return self.dft_amp / np.max(self.dft_amp)

###################################################################################################
def do_pdm(times, signal, f0=None, fn=None, df=None, Nbin=5, Ncover=2, D=0, forbit=None, 
           asini=None, e=None, omega=None, nmax=10):
  """
  Compute the Phase Dispersion Minimization, hence PDM (Jurkevich and Stellingwerf, 1978), using the 
  pdm() routine from IvS Python repository.
  """
  f, theta = pdm(times, signal, f0=f0, fn=fn, df=df, Nbin=Nbin, Ncover=Ncover, D=D, 
                 forbit=None, asini=None, e=None, omega=None, nmax=10)

  return f, theta

###################################################################################################
def do_scargle(times, signal, f0=None, fn=None, df=None, norm='amplitude', weights=None, single=False):
  """
  Compute the Scargle periodogram using the scargle() routine in the IvS Python repository.
  """

  f, a = scargle(times, signal, f0=f0, fn=fn, df=df, norm=norm, weights=weights, single=single)

  return f, a 

###################################################################################################
def read_pigulski(filename):
  """
  Read the BRITE data reduced by Andrej Pigulski.
  For now, we only support .ave orbit averaged reduced light curves. For .ndatc files, additional
  development is required (only if necessary later).
  
  After communication with Andrej Pigulski, the following is the format of the .ave files:
    "*.ave - orbit averages. These files contain a single point per orbit (unless there is more than 
     100 points per orbit; in such a case two points are provided). There are four columns in *.ave files: 
     (1) average HJD - 2456000.0, 
     (2) average magnitude (in mmag), 
     (3) uncertainty of (2), 
     (4) number of data points per orbit. "

  @param filename: full path to the input reduced light curve file with .ave format.
  @type filename: string
  @return: an array containing four fields: 
     - hjd 
     - flux
     - flux_err
     - n_obs
  @rtype: numpy recarray
  """
  if not os.path.exists(filename):
    logging.error('lib: read_pigulski: "{0}" does not exist.'.format(filename))
    raise SystemExit, 'lib: read_pigulski: "{0}" does not exist.'.format(filename)

  with open(filename, 'r') as r: lines = r.readlines()
  n_lines = len(lines)

  dtype = [('hjd', 'f8'), ('flux', 'f8'), ('flux_err', 'f8'), ('n_obs', 'i8')]
  data  = []

  for i, line in enumerate(lines):
    row = line.rstrip('\n\p').split()
    data.append([ float(row[0]), float(row[1]), float(row[2]), int(row[3]) ])

  rec   = np.core.records.fromarrays( np.array(data).transpose(), dtype=dtype )

  print ' - light curve "{0}" has {1} measurements.\n'.format(filename, n_lines)

  return rec

###################################################################################################

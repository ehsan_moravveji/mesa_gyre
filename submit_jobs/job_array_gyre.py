
"""
Submit an array of GYRE jobs with SLURM job array.
"""

import os, glob, sys
import logging
import time
import subprocess
import re
import numpy as np

import job_array_commons as jac

#=================================================================================================================
# Logging settings
# logging.basicConfig(filename='Log-JobArray-GYRE.log', format='%(levelname)s: %(asctime)s: %(message)s', level=logging.DEBUG)

#=================================================================================================================
# Urgent Paths and Specifications To Change Per Each Project
#path_base      = '/STER/ehsan/models/comp_Sch_Led/'
#nmlst_filename = '/STER/ehsan/models/comp_Sch_Led/gyre.in'
#search_string  = path_base + 'M03.2/gyre_in/*.gyre'
path_base      = ''
nmlst_filename = ''
search_string  = ''

#=================================================================================================================
queue          = 'normal'
batch_prefix   = 'JobArray-GYRE'
param_prefix   = 'JobArray-GYRE'
job_name       = 'GYRE'
n_max_jobs     = 500 # 1000    # fixed by SLURM settings
pause_min      = 120     # minutes
delay          = 15       # minutes
n_srun_per_sbatch = 600

GYRE_dir       = os.getenv('GYRE_DIR')
# GYRE_dir       = os.getenv('GYRE_DIR')
if GYRE_dir[-1] != '/': GYRE_dir += '/'
if GYRE_dir[0] != '/': GYRE_dir = '/' + GYRE_dir
path_namelists = GYRE_dir + 'bin/JobArray-namelists/'
path_errs      = GYRE_dir + 'bin/JobArray-errs/'
path_logs      = GYRE_dir + 'bin/JobArray-logs/'
gyre_ad        = GYRE_dir + 'bin/gyre_ad'
gyre_nad       = GYRE_dir + 'bin/gyre_nad'

which_gyre     = gyre_ad
gyre_out_prefix = ''  # e.g. 'ad-sum-'
gyre_out_suffix = ''  # e.g. '-sum.h5'

important_dirs = [path_namelists, path_logs, path_errs]
for dr in important_dirs:
  if not os.path.exists(dr): os.makedirs(dr)

#=================================================================================================================
#gyre_in_files  = glob.glob(search_string)
#n_files        = len(gyre_in_files)
#print '\n - {0} files found in {1}'.format(n_files, search_string)
#logging.info('preamble: {0} files found in {1}'.format(n_files, search_string))

#=================================================================================================================
def submit_namelists(list_nmlst_filenames, sbatch_preamble):
  """
  Receive a list of GYRE inlist filenames, that are already stored disc and exist. Then, submit
  many sbatch jobs, by packing n_srun_per_sbatch into one script, and submitting that script.
  This is useful, when in a grid, every inlist is different from another inlist (e.g. omega_uni).
  Note: We use "n_srun_per_sbatch", "batch_prefix" and "which_gyre", so make sure you have already 
        tuned them before calling this function.

  This routine was used in /models/IS_LMC/ for submitting GYRE jobs where each job had a different
  omega_uni, because all of them had a fixed eta_rot.

  @param list_nmlst_filenames: list of full path to the GYRE inlists that are going to be executed
         Note: we internally check that all these files exist; otherwise, and exception is raised
  @type list_nmlst_filenames: list of strings
  @param sbatch_preamble: list of strings that give the whole "SBATCH" preamble settings 
         of the script. Each item stands of one line of the sbatch commands
  @type sbatch_preamble: list of strings
  """
  sbatch_preamble += ['\n']
  n_nmlst = len(list_nmlst_filenames)
  for nmlst in list_nmlst_filenames:
    if not os.path.exists(nmlst):
      logging.error('jag: submit_namelists: {0} does not exist'.format(nmlst))

  flag = jac.check_password()
  if not flag:
    logging.error('submit_namelists: Submission Aborted')
    return flag

  # prepare patches of jobs
  n_jobs = n_nmlst / n_srun_per_sbatch + 1
  for i_job in range(n_jobs):
    ind_from = i_job * n_srun_per_sbatch
    ind_to   = ind_from + n_srun_per_sbatch
    if i_job == n_jobs - 1:
      ind_to = n_nmlst - 1
    subset   = list_nmlst_filenames[ind_from : ind_to]

    # make serial list of gyre + inlists for one patch
    script   = []
    script   = sbatch_preamble[:]
    for k, subset_file in enumerate(subset):
      txt    = 'srun {0} {1} \n'.format(which_gyre, subset_file)
      script.append(txt)

    # give the script some name for writing to disk
    script_name = '{0}-{1:04d}.sh'.format(batch_prefix, i_job)
    with open(script_name, 'w') as w: w.writelines(script)

    submit_cmnd = 'sbatch {0}'.format(script_name)

    # Now, submit the patch
    process = subprocess.Popen(submit_cmnd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = process.communicate()
    returncode = process.returncode
    if returncode:
      print ' - jag: submit_namelists: failed. Error:{0}'.format(err)
      logging.error('JAG: submit_namelists: SLURM complains about: {0}'.format(err))
      raise SystemExit
    else:
      print ' - JAG: submit_namelists: SLURM says: {0}.'.format(out)
      # logging.info('submit_namelists: Master Batch File: {0}'.format(out))

  return True

#=================================================================================================================
def execute_list_of_sbatch_Python_commands(sbatch_preamble, py_cmnd, gyre_in_files, i):
  """
  This function is specifically built for execution of Python wrapper(s) around GYRE.
  One example is ~/py-codes/plotting-scripts/KIC-7760680-TAR.py. The latter executable 
  receives a name of a GYRE input file, and should be called like 
  >>> python ~/py-codes/plotting-scripts/KIC-7760680-TAR.py <GYRE-input-filename>
  Thus, it can be perfectly parallelized to run on the cluster
  @param sbatch_preamble: list of strings that give the whole "SBATCH" preamble settings 
         of the script. Each item stands of one line of the sbatch commands
  @type sbatch_preamble: list of strings
  @param py_cmnd: The exact python command to be run by the "srun" command in the batch script.
         Note that the "srun" will be inserted by this function at the begining of every 
         call to the Python command, so do not include the "srun" in py_cmnd
  @type py_cmnd: string
  @parm gyre_in_files: list of the full path to the whole gyre_in files.
  @type gyre_in_files: list of strings  
  @param i: the iteration index, specific to that job
  @type i: integer
  """
  sbatch_preamble += ['\n']
  for f in gyre_in_files:
    cmnd = 'srun {0} {1} \n'.format(py_cmnd, f)
    sbatch_preamble += cmnd

  script_name = 'exec-Py-{0:04d}.sh'.format(i)
  with open(script_name, 'w') as w:
    w.writelines(sbatch_preamble)

  submit_cmnd = 'sbatch {0}'.format(script_name)
  process = subprocess.Popen(submit_cmnd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
  out, err = process.communicate()
  returncode = process.returncode
  if returncode:
    print ' - jag: execute_list_of_sbatch_Python_commands: failed. Error:{0}'.format(err)
    logging.error('JAG: execute_list_of_sbatch_Python_commands: SLURM complains about: {0}'.format(err))
    raise SystemExit
  else:
    print ' - JAG: execute_list_of_sbatch_Python_commands: SLURM says: {0}.'.format(out)
    # logging.info('execute_list_of_sbatch_Python_commands: Master Batch File: {0}'.format(out))

  return True

#=================================================================================================================
def execute_list_of_sbatch_commands(nmlst_list, gyre_in_files):
  """
  This function receives a list of sbatch commands and executes them. Each sbatch command is sperformed on
  one batch file, which already contains up to 1000 srun commands to run GYRE on a namelist file
  @param nmlst_list: list of sample namelist items to be used by gen_namelists to prepare every individual
     namelist file
  @type nmlst_list: list of strings
  @parm gyre_in_files: list of the full path to the whole gyre_in files.
  @type gyre_in_files: list of strings
  """
  flag = jac.check_password()
  if not flag:
    logging.error('submit_jobs: Submission Aborted')
    return flag

  list_commands = prepare_all_srun(nmlst_list, gyre_in_files)
  n_cmnd = len(list_commands)

  for i_cmnd in range(n_cmnd):
    cmnd = list_commands[i_cmnd]
    process = subprocess.Popen(cmnd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = process.communicate()
    returncode = process.returncode
    if returncode:
      print ' - jag: execute_list_of_sbatch_commands: failed. Error:{0}'.format(err)
      logging.error('JAG: execute_list_of_sbatch_commands: SLURM complains about: {0}'.format(err))
      raise SystemExit
    else:
      print ' - JAG: execute_list_of_sbatch_commands: SLURM says: {0}. {1} of {2}'.format(out, i_cmnd+1, n_cmnd)
      logging.info('execute_list_of_sbatch_commands: Master Batch File: {0} submitted: {1}'.format(cmnd, out))

    time.sleep(0.1)

  return None

#=================================================================================================================
def get_queue_walltime(queue):
  """
  To make sure we comply with the strict walltime settings for each queue, they are retrieved correctly
  by a call tho this function
  @param queue: The specified SLURM queue for submitting the jobs. They should be any of the following:
         - 'high': then, walltime should be below 6 hours
         - 'normal': then, walltime should be below 24 hours
         - 'low': then, walltime should be below 24 hours
         - 'long': then, walltime should be below 14 days
  @type queue: string 
  @return: the walltime string is returned, so e.g. if queue='short', we return '06:00:00', or if 
         queue is 'normal', we return '24:00:00'.
  @rtype: string
  """
  if queue != 'high' and queue != 'normal' and queue != 'low' and queue != 'long':
    logging.error('JAG: get_queue_walltime: input queue:{0} is invalid. Check again'.format(queue))
    raise SystemExit

  if queue == 'high':   return '06:00:00'
  if queue == 'normal': return '24:00:00'
  if queue == 'low':    return '24:00:00'
  if queue == 'long':   return '14:00:00:00'

#=================================================================================================================
def prepare_1000_srun(i_iter, nmlst_list, gyre_in_files):
    """
    Prepare up to 1000 srun commands all in one batch file, ready to be executed.
    The structure of each command is:
       srun <gyre command> <full path to the namelist>
    Each batch file has a name starting like "GYRE-exec-" and are enumerated based on the call number
       received by i_iter. E.g. GYRE-exec-037.sh
       Then, in the terminal prompt, the following command can submit the job to the queue
       $> sbatch GYRE-exec-037.sh
    @param i_iter: The iteration number where this routine is called. It is used to label the batch files
    @type i_iter: integer
    @param nmlst_list: list of sample namelist iterms to be used by gen_namelists to prepare every individual
       namelist file
    @type nmlst_list: list of strings
    @parm gyre_in_files: list of the full path to the whole gyre_in files.
    @type gyre_in_files: list of strings
    @return: sbatch command to submit this specific script to the SLURM job queue.
    @rtype: string
    """
    list_nmlst_filenames = gen_namelists(nmlst_list, gyre_in_files)
    n_srun = len(gyre_in_files)
    batch_filename = 'GYRE-exec-{0:04d}.sh'.format(i_iter)

    walltime = get_queue_walltime(queue)

    lines = []

    lines.append('#!/bin/bash \n')
    lines.append('\n')
    lines.append('#SBATCH --account=ivsusers \n')
    lines.append('#SBATCH --job-name={0}-Job-{1:04d} \n'.format(job_name, i_iter))
    # lines.append('#SBATCH --array=1-{0} \n'.format(n_srun))
    #lines.append('#SBATCH --begin=now+1 \n')
    lines.append('#SBATCH --partition={0} \n'.format(queue))
    lines.append('#SBATCH --time={0} \n'.format(walltime))
    lines.append('#SBATCH --output=/dev/null \n')
    lines.append('#SBATCH --error=/dev/null \n')
    #lines.append('#SBATCH --exclusive \n')
    lines.append('#SBATCH --ntasks=1 \n')
    lines.append('#SBATCH --cpus-per-task=1 \n')
    lines.append('#SBATCH --mem-per-cpu=3000 \n')
    lines.append('\n')
    lines.append('hostname \n')
    lines.append('date \n')
    lines.append('\n')
    lines.append('export OMP_NUM_THREADS=1 \n')
    lines.append('echo "My SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID \n')
    lines.append('\n')
    lines.append('# Execution: \n')

    for i_run in range(n_srun):
      line = 'srun {0} {1} \n'.format(which_gyre, list_nmlst_filenames[i_run])
      lines.append(line)

    with open(batch_filename, 'w') as w: w.writelines(lines)

    return 'sbatch {0}'.format(batch_filename)

#=================================================================================================================
def prepare_all_srun(nmlst_list, gyre_in_files):
    """
    Prepare many submission scripts, each by a call to prepare_1000_srun
    @param nmlst_list: list of strings for the sample namelist file. This is reaturned, e.g. by a call to
        jag.read_sample_namelist_to_list_of_strings()
    @type nmlst_list: list of strings
    @param gyre_in_files: list of the entire input gyre_in files.
    @type gyre_in_files: list of strings
    @return: list of execution commands for sbatch
    @rtype: list of strings
    """
    n_files = len(gyre_in_files)
    if n_files == 0:
        logging.error('jag: prepare_all_srun: Input list of gyre_in files is empty')
        raise SystemExit, 'jag: prepare_all_srun: Input list of gyre_in files is empty'

    n_each = n_srun_per_sbatch
    n_iters = n_files / n_each + 1
    list_commands = []
    print ' - jag: prepare_all_srun will submit {0} jobs, with each up to {1} srun commands'.format(n_iters, n_each)

    n_offset = int(raw_input(' - jag: prepare_all_srun: Enter an integer to offset batch script names, and avoid overwriting: '))
    i_iter_and_offset = 0

    for i_iter in range(n_iters):
      ind_from = i_iter * n_each
      ind_to     = ind_from + n_each
      if i_iter == n_iters - 1: ind_to = n_files - 1
      list_1000_files = gyre_in_files[ind_from : ind_to]
      i_iter_and_offset = i_iter + n_offset
      sbatch_command = prepare_1000_srun(i_iter_and_offset, nmlst_list, list_1000_files)
      list_commands.append(sbatch_command)

    return list_commands  

#=================================================================================================================
def read_sample_namelist_to_list_of_strings(a_file):
  """
  Read the sample namelist to a list of strings
  @param a_file: full path to the sample namelist that makes the basis of the entire namelists
  @type a_file: string
  @return: list of strings containing every line in the sample file, an item per line.
  @rtype: list
  """
  if not os.path.isfile(a_file):
    logging.error('read_sample_namelist_to_list_of_strings: {0} does not exist'.format(a_file))
    print 'Error: JAG: read_sample_namelist_to_list_of_strings: {0} does not exist'.format(a_file)
    raise SystemExit

  with open(a_file, 'r') as r: lines = r.readlines()
  logging.info('read_sample_namelist_to_list_of_strings: {0} successfully read'.format(a_file))

  return lines

#=================================================================================================================
def gen_param_file(gyre_in_files, a_file):
  """
  This function iterates over the whole parameters defined on the header of this file, and stores one set per each
  line for the job array.
  @param a_file: Full path to the file containing the list of parameters to run
  @type a_file: string
  @return: dictionary with the full information about the file and the parameters of the Jobs being submitted.
     It has the following keys:
     -
  @rtype: dictionary
  """
  dic = {}
  lines = []

  n_param = 0
  for i_file, f in enumerate(gyre_in_files):
    ind_slash = f.rfind('/')
    ind_point = f.rfind('.')
    core_name = f[ind_slash+1 : ind_point]
    a_nmlst   = path_namelists + core_name + '.nmlst'
    lines.append('{0} \n'.format(a_nmlst))
    n_param   += 1

  dic['lines'] = lines
  dic['n_param'] = n_param

  with open(a_file, 'w') as w: w.writelines(lines)
  logging.info('gen_param_file: {0} created'.format(a_file))

  return dic

#=================================================================================================================
def gen_namelists(nmlst_list, gyre_in_files, eta_rot=0.0):
  """
  Generate a series of namelists based mainly on the sample namelist, which is read/imported now as a list of strings;
  one line per each list item
  @param: nmlst_list: list of lines associated with every line in the sample namelist
  @type nmlst_list: list of strings
  @param gyre_in_files: list of strings, giving full path to the input GYRE files. The list is used to create inlists.
  @type gyre_in_files: list of strings
  @param eta_rot: The rotation frequency w.r.t. the break up frequency; thus, 0 <= eta_rot <= 1
        Internally, we multiply eta_rot by 100 to express it in percentage w.r.t. break up, and
        add it to the end of the filename. For backward compatibility, if eta_rot is set to a negative
        number, this feature is ignored.
  @type eta_rot: float
  @return: None
  @rtype: None
  """
  ind_input_file  = None
  ind_output_file = None
  for i, line in enumerate(nmlst_list):
    # regex = re.search('\s file\s * =\s *', line)
    regex = re.search('\s file\s*=', line)
    if regex is not None:
      ind_input_file = i
      break

  if ind_input_file is None:
    print i, line, ind_input_file
    logging.error('gen_namelists: Could not capture ind_input_file')
    raise SystemExit

  for i, line in enumerate(nmlst_list):
    # regex = re.search('\s summary_file\s * =\s *', line)
    regex = re.search('\s summary_file\s*=', line)
    if regex is not None:
    #if 'summary_file' in line: ind_output_file = i
      ind_output_file = i
      break
  if ind_output_file is None:
    logging.error('gen_namelists: Could not capture ind_output_file')
    raise SystemExit
  if gyre_out_suffix == '':
    logging.error('gen_namelists: gyre_out_suffix not specified')
    raise SystemExit

  if eta_rot > 1.0:
    logging.error('gen_namelists: eta_rot cannot exceed 1.0')
    raise SystemExit

  include_eta_rot = eta_rot >= 0.0

  n_nmlst = 0
  list_nmlst_filenames = []

  for i, a_file in enumerate(gyre_in_files):
    ind_slash      = a_file.rfind('/')
    ind_point      = a_file.rfind('.')
    core_name      = a_file[ind_slash+1 : ind_point]
    prefix         = a_file[:ind_slash+1]

    if include_eta_rot: 
      str_eta      = '-eta{0:05.2f}'.format(eta_rot * 100)
    else:
      str_eta      = '' 

    if '/gyre_in/' not in prefix:
      logging.warning('gen_namelists: {0} not handled; does not come from gyre_in folder'.format(a_file))
      continue

    nmlst_list[ind_input_file]  = '   file = "{0}" \n'.format(a_file)
    output_dir  = prefix.replace('/gyre_in/', '/gyre_out/')
    if not os.path.exists(output_dir): os.makedirs(output_dir)
    if output_dir[-1] != '/': output_dir += '/'

    output_file = output_dir + gyre_out_prefix + core_name + str_eta + gyre_out_suffix
    nmlst_list[ind_output_file] = '   summary_file = "{0}" \n'.format(output_file)

    nmlst_filename = path_namelists + core_name + '.nmlst'
    n_nmlst += 1

    with open(nmlst_filename, 'w') as w: w.writelines(nmlst_list)
    list_nmlst_filenames.append(nmlst_filename)

  logging.info('gen_namelists: {0} namelists stored in {1}'.format(n_nmlst, path_namelists))

  return list_nmlst_filenames

#=================================================================================================================
def prepare_batch(gyre_in_files, batch_filename, param_filename):
  """
  Prepare the batch file to be stored and executed from the local directory
  @param gyre_in_files: list of strings, giving full path to the input GYRE files. The list is used to create inlists.
  @type gyre_in_files: list of strings
  @param batch_filename: full path to the batch file to be produced, and written to disk for a specific job(s).
  @type batch_filename: string
  @param param_filename: full name to the file containing the list of parameters to be generated
  @type param_filename: string
  @return: None
  @rtype: None
  """
  dic_Q  = jac.get_Q_report(report=True)
  if dic_Q is None:
    SLURM_ARRAY_JOB_ID = 1
  else:
    max_id = dic_Q['max_id']
    SLURM_ARRAY_JOB_ID  = max_id + 1
    SLURM_ARRAY_TASK_ID = 0

  dic_param = gen_param_file(gyre_in_files, param_filename)
  n_param   = dic_param['n_param']
  if n_param == 0: return None
  print ' - JAG: prepare_batch: batch:{0}, input:{1}, N_jobs:{2}'.format(batch_filename, param_filename, n_param)

  if not os.path.isfile(nmlst_filename):
    logging.error('prepare_batch: the sample namelist {0} not found'.format(nmlst_filename))
    raise SystemExit
  list_of_sample_nmlst = read_sample_namelist_to_list_of_strings(nmlst_filename)
  gen_namelists(list_of_sample_nmlst, gyre_in_files)
  walltime = get_queue_walltime(queue)

  lines = []
  lines.append('#!/bin/bash \n')
  lines.append('\n')
  lines.append('#SBATCH --account=ivsusers \n')
  lines.append('#SBATCH --job-name={0} \n'.format(job_name))
  lines.append('#SBATCH --array=1-{0} \n'.format(n_max_jobs))
  #lines.append('#SBATCH --begin=now+1 \n')
  lines.append('#SBATCH --partition={0} \n'.format(queue))
  lines.append('#SBATCH --time={0} \n'.format(walltime))
  lines.append('#SBATCH --output=/dev/null \n')
  lines.append('#SBATCH --error=/dev/null \n')
  #lines.append('#SBATCH --exclusive \n')
  lines.append('#SBATCH --ntasks=1 \n')
  lines.append('#SBATCH --cpus-per-task=1 \n')
  lines.append('#SBATCH --mem-per-cpu=3000 \n')
  lines.append('\n')
  lines.append('hostname \n')
  lines.append('date \n')
  lines.append('\n')
  lines.append('export OMP_NUM_THREADS=1 \n')
  lines.append('echo "My SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID \n')
  lines.append('\n')
  lines.append('# Execution: \n')
  #lines.append('cd {0} \n'.format(GYRE_dir))
  lines.append('ParamFile={0} \n'.format(param_filename))
  lines.append('ParamFromFile=$( sed -n -e "${SLURM_ARRAY_TASK_ID}p" $ParamFile ) \n')
  lines.append('BaseName=$( basename --suffix .nmlst $ParamFromFile ) \n')
  lines.append('log_file={0}$BaseName.out \n'.format(path_logs))
  lines.append('err_file={0}$BaseName.err \n'.format(path_errs))
  lines.append('srun {0} $ParamFromFile 1>>$log_file 2>>$err_file \n'.format(which_gyre))
  lines.append('\n')
  lines.append('echo Remove input if no errors occured \n')
  lines.append('if [ -e $err_file ] \n')
  lines.append('then \n')
  lines.append('     if ! [ -s $err_file ] \n')
  lines.append('     then \n')
  lines.append('          rm $err_file $log_file $ParamFromFile \n')
  lines.append('     fi \n')
  lines.append('fi \n')
  lines.append('\n')
  lines.append('hostname \n')
  lines.append('date \n')
  lines.append('echo Done \n')

  with open(batch_filename, 'w') as w: w.writelines(lines)
  logging.info('prepare_batch: Master Batch File = {0}'.format(batch_filename))

  return None

#=================================================================================================================
def submit_jobs(gyre_in_files):
  """
  Submit GYRE jobs for the list of input filenames
  @param gyre_in_files: list of strings, giving full path to the input GYRE files. The list is used to create inlists.
  @type gyre_in_files: list of strings
  @return: return code of subprocess after submitting the batch file
  @rtype: integer
  """
  if not os.path.exists(path_base):
    logging.error('submit_jobs: {0} not found'.format(path_base))
    raise SystemExit

  n_files = len(gyre_in_files)
  if n_files == 0:
    logging.error('submit_jobs: No gyre_in files found in {0}'.format(search_string))
    raise SystemExit

  flag = jac.check_password()
  if not flag:
    logging.error('submit_jobs: Submission Aborted')
    return flag

  n_jobs = int(n_files/n_max_jobs) + 1
  n_last = n_files % n_max_jobs
  for i_job in range(n_jobs):

    # Only submit jobs if the queue is reasonably unbusy.
    while True:
      n_my_PD = jac.get_Q_report()['n_my_PD']
      #if n_my_PD is None: n_my_PD = 0
      if n_my_PD > n_max_jobs*2:
        print '   submit_jobs: Num. my PD: {0}; Sleep for {1} min'.format(n_my_PD, delay)
        logging.warning('JAG: submit_jobs: Num. my PD: {0}; Sleep for {1} min'.format(n_my_PD, delay))
        time.sleep(delay*60)
      else:
        print '   submit_jobs: Time:{0}. N_PD={1}; OK to Go!'.format(time.strftime("%d %b %Y %H:%M:%S"), n_my_PD)
        break

    batch_filename = batch_prefix + '-{0:02d}.sh'.format(i_job)
    param_filename = param_prefix + '-{0:02d}.input'.format(i_job)
    if i_job == n_jobs - 1:
      files_for_submit = gyre_in_files[i_job*n_max_jobs : ]
    else:
      files_for_submit = gyre_in_files[i_job*n_max_jobs : (i_job+1)*n_max_jobs]

    prepare_batch(files_for_submit, batch_filename, param_filename)
    #returncode = subprocess.call('sbatch {0}'.format(batch_filename), shell=True)
    process = subprocess.Popen('sbatch {0}'.format(batch_filename), stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE, shell=True)
    out, err = process.communicate()
    returncode = process.returncode
    if returncode:
      print ' - job_array_gyre: submit_jobs: failed. Error:{0}'.format(err)
      logging.error('JAG: submit_jobs: SLURM complains about: {0}'.format(err))
      raise SystemExit
    else:
      print ' - JAG: submit_jobs: SLURM says: {0}. {1} of {2}'.format(out, i_job+1, n_jobs)
      logging.info('submit_jobs: Master Batch File: {0} submitted: {1}'.format(batch_filename, out))

    if i_job == n_jobs-1: continue
    print '   Sleeping for {0} min ...'.format(pause_min)
    time.sleep(pause_min*60.)

  return returncode

#=================================================================================================================
def main():
  """
  Submit the jobs
  """
  logging.info('main: Start')

  if True:
    status = submit_jobs(gyre_in_files)

  logging.info('main: End')
  return status

#=================================================================================================================
if __name__ == '__main__':
  status = main()
  sys.exit(status)
#=================================================================================================================


import os, glob
from collections import OrderedDict
from math import ceil
import matplotlib.pyplot as plt
import subprocess

#def commons(platform):
"""
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Define Platform-Specific Variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% """

MacBookPro = 1
Pleiad = 2
Clusty = 3
VSCentrum = 4

queue_list = ['allnodes', 'express', 'short', 'long', 'test'] # Available as of Oct 2013.
queue = 'express'

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Choose between Adiabatic versus Non-Adiabatic GYRE
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
which_gyre = 'gyre_ad'    # or 'gyre_nad'
gyre_version = '4.2'
print ' - Which GYRE? %s v.%s' % (which_gyre, gyre_version)

ProjectName = 'MESA-GYRE-Models'

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Resource-Dependent Variables
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

num_threads_avail = 100.0
omp_num_threads   = 1.0
run_time_min_per_track = 35.0 # minutes for OMP_NUM_THREADS = 1
disk_size_per_job_mb = 50.0  # MB of disk space required to store all files per track

run_time_min_per_gyre = 30.0
n_max_slurm_jobs = 10000

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Define Paths Based on the Choice of the Platform
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#=================================================================================================================
def set_params():
  """
  This function returns the range of physical parameters that are reflected in the grid filenames to facilitate a
  coherent search and interpretation in the gird.
  @return: OrderedDict that remembers the order the items are inserted into the dictionary.
  @rtype: dictionary.
  """
  evol = 'MS'

  mass_start = 3.1
  mass_end   = 3.1
  mass_step  = 0.05
  mass_fmt   = "{:04.1f}"

  eta_start  = 0.00
  eta_end    = 0.00
  eta_step   = 0.02
  eta_fmt    = "{:04.2f}"

  ov_start   = 0.000
  ov_end     = 0.030
  ov_step    = 0.003
  ov_fmt     = "{:05.3f}"

  sc         = 0.01
  sc_fmt     = "{:04.2f}"

  z_start    = 0.014
  z_end      = 0.014
  z_step     = 0.001
  z_fmt      = "{:05.3f}"

  Yc_start   = 0.26
  Yc_end     = 0.99
  Yc_step    = 0.02
  Yc_fmt     = "{:06.4f}"

  Xc_start   = 0.72
  Xc_end     = 0.00
  Xc_step    = 0.02
  Xc_fmt     = "{:06.4f}"

  dic = OrderedDict({})

  dic['evol'] = evol

  dic['mass_start'] = mass_start
  dic['mass_end'] = mass_end
  dic['mass_step'] =  mass_step
  dic['mass_fmt'] = mass_fmt

  dic['eta_start'] = eta_start
  dic['eta_end'] = eta_end
  dic['eta_step'] =  eta_step
  dic['eta_fmt'] = eta_fmt

  dic['ov_start'] = ov_start
  dic['ov_end'] = ov_end
  dic['ov_step'] =  ov_step
  dic['ov_fmt'] = ov_fmt

  dic['sc'] = sc
  dic['sc_fmt'] = sc_fmt

  dic['z_start'] = z_start
  dic['z_end'] = z_end
  dic['z_step'] = z_step
  dic['z_fmt'] = z_fmt

  dic['eta_start'] = eta_start
  dic['eta_end'] = eta_end
  dic['eta_step'] =  eta_step
  dic['eta_fmt'] = eta_fmt

  dic['Yc_start'] = Yc_start
  dic['Yc_end'] = Yc_end
  dic['Yc_step'] =  Yc_step
  dic['Yc_fmt'] = Yc_fmt

  dic['Xc_start'] = Xc_start
  dic['Xc_end'] = Xc_end
  dic['Xc_step'] =  Xc_step
  dic['Xc_fmt'] = Xc_fmt

  return dic


#=================================================================================================================
def set_paths(platform):


  if (platform == MacBookPro):
     path_home = '/Users/ehsan/programs/'
     path_ehsan = path_home
     path_mesa_gyre = path_home
     equil_dir = 'models/interactive/LOGS/'
     path_slurm = path_home + 'slurm/'
     path_scratch = path_home

  if (platform == Pleiad):
     path_home = '/home/ehsan/'
     path_ehsan = '/STER/ehsan/'
     path_mesa_gyre = '/STER/mesa-gyre/'
     equil_dir = 'equil/'
     path_slurm = '/STER/ehsan/slurm/'
     path_scratch = '/scratch/ehsan/'

  if (platform == VSCentrum):
     path_home = '/user/leuven/307/vsc30745/'
     path_data = '/data/leuven/307/vsc30745/'
     path_scratch = '/scratch/leuven/307/vsc30745/'
     path_ehsan = path_data
     path_slurm = '/data/leuven/307/vsc30745/scripts/'
     equil_dir = 'Grid/'
     path_mesa_gyre = path_scratch + equil_dir

  if (platform == Clusty):
     print 'Settings for Clusty are not implemented yet. Returning ... '
     raise SystemExit, 1

  path_mesa = '/home/ehsan/mesa-5548/'
  path_models = path_ehsan + 'models/'
  path_work_dir = path_models + 'interactive_papics/'

  if gyre_version == '3.2.2':
    path_gyre_src = path_ehsan + 'gyre3.2.2/bin/' # version 3.2.2
  if gyre_version == '3.0':
    path_gyre_src = path_ehsan + 'gyre3.0/bin/'  # version 3.0
  if gyre_version == '2.3':
    path_gyre_src = path_ehsan + 'gyre2.3/bin/'  # version 2.3
  if gyre_version == '2.2':
    path_gyre_src = path_ehsan + 'gyre2.2/bin/'  # version 2.2
  path_gyre_ad  = path_gyre_src
  path_gyre_nad = path_gyre_src
  path_gyre_namelists = path_gyre_src + 'slurm_namelists/'
  path_gyre_logs = path_gyre_src + 'logs/'
  path_gyre_errs = path_gyre_src + 'errs/'
  path_gyre_bash = path_slurm

  path_equil = path_mesa_gyre         # For mesa-gyre grid
  #path_equil = '/STER/ehsan/papics/' + equil_dir  # For Peter Papics grid
  path_slurm_plots = path_slurm + 'plots/'       # To store general plots
  path_mesa_input_files = path_work_dir + 'slurm_inputs/'
  path_gyre_output = path_mesa_gyre + 'gyre_out/'
  path_mesa_logs = path_work_dir + 'logs/'
  path_mesa_errs = path_work_dir + 'errs/'
  path_resubmit_mesa_jobs = path_work_dir + 'resubmit_jobs/'

  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  # Create Some Directories
  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if not os.path.exists(path_mesa_input_files): os.makedirs(path_mesa_input_files)
  if not os.path.exists(path_mesa_logs): os.makedirs(path_mesa_logs)
  if not os.path.exists(path_mesa_errs): os.makedirs(path_mesa_errs)
  if not os.path.exists(path_slurm_plots): os.makedirs(path_slurm_plots)
  if not os.path.exists(path_resubmit_mesa_jobs): os.makedirs(path_resubmit_mesa_jobs)
  if not os.path.exists(path_gyre_namelists): os.makedirs(path_gyre_namelists)
  if not os.path.exists(path_gyre_logs): os.makedirs(path_gyre_logs)
  if not os.path.exists(path_gyre_errs): os.makedirs(path_gyre_errs)

  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  # Return All Paths as a Dictionary
  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  dic = OrderedDict({})
  dic['platform'] = platform
  dic['MacBookPro'] = MacBookPro
  dic['Pleiad'] = Pleiad
  dic['Clusty'] = Clusty
  dic['VSCentrum'] = VSCentrum
  dic['queue'] = queue

  dic['path_home'] = path_home
  dic['path_ehsan'] = path_ehsan
  dic['path_mesa_gyre'] = path_mesa_gyre
  dic['equil_dir'] = equil_dir
  dic['path_scratch'] = path_scratch
  dic['path_slurm'] = path_slurm
  dic['path_slurm_plots'] = path_slurm_plots
  dic['path_mesa'] = path_mesa
  dic['path_models'] = path_models
  dic['path_work_dir'] = path_work_dir
  dic['path_equil'] = path_equil
  dic['path_mesa_input_files'] = path_mesa_input_files
  dic['path_mesa_logs'] = path_mesa_logs
  dic['path_mesa_errs'] = path_mesa_errs
  dic['path_resubmit_mesa_jobs'] = path_resubmit_mesa_jobs

  dic['which_gyre'] = which_gyre
  dic['gyre_version'] = gyre_version
  dic['path_gyre_src'] = path_gyre_src
  dic['path_gyre_ad'] = path_gyre_ad
  dic['path_gyre_nad'] = path_gyre_nad
  dic['path_gyre_namelists'] = path_gyre_namelists
  dic['path_gyre_logs'] = path_gyre_logs
  dic['path_gyre_errs'] = path_gyre_errs
  dic['path_gyre_output'] = path_gyre_output
  dic['path_gyre_bash'] = path_gyre_bash

  dic['ProjectName'] = ProjectName

  dic['num_threads_avail'] = num_threads_avail
  dic['omp_num_threads'] = omp_num_threads
  dic['run_time_min_per_track'] = run_time_min_per_track
  dic['run_time_min_per_gyre'] = run_time_min_per_gyre
  dic['n_max_slurm_jobs'] = n_max_slurm_jobs
  dic['disk_size_per_job_mb'] = disk_size_per_job_mb

  return dic


#=================================================================================================================
def get_num_current_slurm_jobs():
  """
  This querries for the number of jobs that are currently run on SLURM.
  """

  result = subprocess.Popen('squeue | grep ehsan | grep PD | wc -l', shell=True, stdout=subprocess.PIPE)
  lines = result.stdout.readlines()
  n_current_jobs = lines

  return n_current_jobs


#=================================================================================================================
def plot_file_dir_stat(platform, list_dic):
  """
  This function receives a list of dictionaries. Each dictionary contains file information for a specific mass
  directory. The key, value pairs for each dictionary are introduced in get_list_dic_files; so read it first.
  @param platform: Either a laptop/desktop (so, MacBookPro option) or the cluster (Pleiad or Clusty) or a supercomputer
         (e.g. VSCentrum). The paths depend on the choice of the platform
  @type platform: integer; for more information see the top of slurm_commons.py
  @param list_dic: list of dictionaries, where the number of dictionaries is the same as the number of directories
         for which we are studying
  @type list_dic: list of dictionaries
  @return: None, but outputs a plot in the current directory in plots subdirectory.
  @rtype: None
  """

  n_dic = len(list_dic)    # = n_dir
  if n_dic == 0:
    message = 'Error: slurm_commons: plot_file_dir_stat: the input list is empty!'
    raise SystemExit, message

  path_slurm_plots = set_paths(platform)['path_slurm_plots']
  mass_from = set_params()['mass_start']
  mass_to   = set_params()['mass_end']
  mass_fmt  = set_params()['mass_fmt']
  fname = '-M' + mass_fmt.format(mass_from) + '-M' + mass_fmt.format(mass_to)
  pdf_file = path_slurm_plots + 'Stat-file-dir' + fname +  '.pdf'
  plt.figure()
  fig, (ax1, ax2, ax3) = plt.subplots(figsize=(5, 8), nrows=3)
  plt.subplots_adjust(left=0.15, right=0.98, bottom=0.07, top=0.99, hspace=0.02, wspace=0.02)
  ax1.minorticks_on()
  ax2.minorticks_on()
  ax3.minorticks_on()
  ax1.tick_params('both', length=10, width=1.2, which='major')
  ax1.tick_params('both', length=5, width=1.2, which='minor')
  ax2.tick_params('both', length=10, width=1.2, which='major')
  ax2.tick_params('both', length=5, width=1.2, which='minor')
  ax3.tick_params('both', length=10, width=1.2, which='major')
  ax3.tick_params('both', length=5, width=1.2, which='minor')

  list_m_ini = []
  list_n_hist = []
  list_n_prof = []
  list_n_gyre_in = []
  list_n_gyre_out  = []
  n_accum_hist = 0
  n_accum_prof = 0
  n_accum_gyre_in = 0
  n_accum_gyre_out = 0

  for i in range(n_dic):
    which_dic = list_dic[i]
    dir_name = which_dic.get('full_path')
    ind_mass = dir_name.rfind('M') + 1
    str_mass = dir_name[ind_mass:ind_mass+4]
    dbl_mass = float(str_mass)
    list_m_ini.append(dbl_mass)

    hist_files = which_dic['hist_files']
    n_hist_files = len(hist_files)
    n_accum_hist += n_hist_files
    list_n_hist.append(n_hist_files)
    prof_files = which_dic['prof_files']
    n_prof_files = len(prof_files)
    n_accum_prof += n_prof_files
    list_n_prof.append(n_prof_files)
    gyre_in_files = which_dic['gyre_in_files']
    n_gyre_in_files = len(gyre_in_files)
    n_accum_gyre_in += n_gyre_in_files
    list_n_gyre_in.append(n_gyre_in_files)
    gyre_out_files = which_dic['gyre_out_files']
    n_gyre_out_files = len(gyre_out_files)
    n_accum_gyre_out += n_gyre_out_files
    list_n_gyre_out.append(n_gyre_out_files)

  xlim = [min(list_m_ini)*0.95, max(list_m_ini)*1.05]

  ax1.set_xlim(xlim[0], xlim[1])
  ax1.plot(list_m_ini, list_n_hist, 'om')
  ax1.set_ylabel(r'Num. Tracks')
  ax1.set_ylim(min(list_n_hist)*0.98, max(list_n_hist)*1.02)
  txt1 = r'Total Tracks = ' + r'' + str(n_accum_hist) + r''
  ax1.annotate(txt1, xy=(0.06, 0.10), xycoords='axes fraction')
  ax2.set_xlim(xlim[0], xlim[1])
  ax2.set_ylim(min(list_n_prof)*0.98, max(list_n_prof)*1.02)
  txt2 = r'Total Profiles = ' + r'' + str(n_accum_prof) + r''
  txt3 = r'Total GYRE Inputs = ' + r'' + str(n_accum_gyre_in) + r''
  ax2.annotate(txt2, xy=(0.06, 0.10), xycoords='axes fraction')
  ax2.annotate(txt3, xy=(0.06, 0.20), xycoords='axes fraction')
  ax2.scatter(list_m_ini, list_n_prof, s=100, c='b', marker='s', alpha=0.60, label=r'.prof files')
  ax2.scatter(list_m_ini, list_n_gyre_in, s=9, c='r', marker='o', label=r'.gyre files')
  ax2.set_ylabel(r'Num. Input Files')
  leg = ax2.legend(loc='lower right', shadow=True)
  ax3.set_xlim(xlim[0], xlim[1])
  ax3.plot(list_m_ini, list_n_gyre_out, 'ob')
  ax3.set_xlabel(r'Initial Mass [M$_\odot$]')
  ax3.set_ylabel(r'Num. GYRE Output')
  ax3.set_ylim(min(list_n_gyre_out)*0.98, max(list_n_gyre_out)*1.02)
  txt4 = r'Total GYRE Output = ' + r'' + str(n_accum_gyre_out) + r''
  ax3.annotate(txt4, xy=(0.06, 0.10), xycoords='axes fraction')
  if n_accum_gyre_in - n_accum_gyre_out > 0:
    txt5 = r'Failed GYRE calls $\approx$ ' + r'' + str(n_accum_gyre_in - n_accum_gyre_out) + r''
    ax3.annotate(txt5, xy=(0.06, 0.20), xycoords='axes fraction')

  plt.savefig(pdf_file)
  plt.close()
  print ' - Plot %s created. \n' % (pdf_file, )


#=================================================================================================================
def detect_incomplete_gyre_jobs(list_gyre_in, list_gyre_out):
  """
  This function receives two lists of files relevant to GYRE:
  - list_gyre_in: with extension *.gyre
  - list_gyre_out: with extension *.h5
  Then, it makes sure if for each input file in list_gyre_in, there exists a corresponding output file in list_gyre_out.
  In case there are missing output files (i.e. unsuccessful job submission for any reason), then this function returns a
  list of input gyre files from list_gyre_in for REsubmission of gyre jobs.
  @param list_gyre_in: list of strings giving full path to individual gyre_in files
  @type list_gyre_in: list of strings
  @param list_gyre_out: list of strings giving full path to the invidual gyre_out files
  @type list_gyre_out: list of strings
  @return: list of "incomplete" gyre_in files.
  @rtype: list of strings
  """

  len_in = len(list_gyre_in)
  len_out = len(list_gyre_out)
  if (len_in == 0):
    message = 'Error: slurm_commons: detect_incomplete_gyre_jobs: input list is empty: ', len_in
    #raise SystemExit, message
    print message
  if (len_out == 0):
    message = 'Error: slurm_commons: detect_incomplete_gyre_jobs: output list is empty: ', len_out
    #raise SystemExit, message
    print message

  unsuccessful = []
  n_failure = 0
  for i in range(len_in):
    input_file = list_gyre_in[i]
    loc_slash = input_file.rfind('/') + 1
    if loc_slash <= 1: raise SystemExit, 'slurm_commons: detect_incomplete_gyre_jobs: rfind failed!'
    address   = input_file[0 : loc_slash]
    address_out = address.replace('gyre_in', 'gyre_out')
    core_name = input_file[loc_slash : -5]
    equivalent_h5 = address_out + core_name + '-sum.h5'

    if equivalent_h5 not in list_gyre_out:
      n_failure += 1
      unsuccessful.append(input_file)

  return unsuccessful

#=================================================================================================================
def get_list_dic_files(path_repo, mass_from, mass_to, mass_step, fmt, srch_regex=None):
  """
  This function receives a path to the repository where directories of models are named by their initial mass,
  e.g. if path_repo = '/STER/mesa-gyre/equil/', then a lot of subdirectories with masses are present in it.
  then, we scan through mass_from to mass_to in steps of mass_step with fixed directory format fmt for all
  *.hist, *.prof, *.gyre and *.h5 files.
  In return, a list is provided with N dictionaries, where N is equal to the number of mass subdirectory found
  in path_repo and matching the mass_from, mass_to and mass_step combinations.
  Dictionaries are returned by a call to get_files_lists; so, take a look at the latter for more information.

  @param path_repo: Full path to the repository with as many mass directory as calculated from the grid
  @type path_repo: string
  @param mass_from: scan in directories with mass starting from this value
  @type mass_from: float
  @param mass_to: scan in directories up to this mass
  @type mass_to: float
  @param mass_step: scan in directories with this step
  @type mass_step: float
  @param srch_regex: optional argument to constrain the search with a prescribed regular expression
  @type srch_regex: string
  @type fmt: format string to match directory names.
  @type fmt: string
  @return: list of dictionaries; each dictionary provides all files of different extension in that subdirectory.
  @rtype: list
  """

  flag = os.path.exists(path_repo)
  if not flag:
    message = 'Error: slurm_commons: get_list_dic_files: %s does not exist!' % (path_repo, )
    raise SystemExit, message
  flag = os.path.isdir(path_repo)
  if not flag:
    message = 'Error: slurm_commons: get_list_dic_files: %s exists, but is not a directory!' % (path_repo, )
    raise SystemExit, message

  n_dir = int( ceil( (mass_to - mass_from)/mass_step ) + 1 )

  list_of_dic = []
  for i_dir in range(n_dir):
    which_mass = mass_from + i_dir * mass_step
    n_temp, mass_srch_str = gen_mass_dir_srch_str(which_mass, which_mass, mass_step, fmt)
    if n_temp != 1: raise SystemExit, 'Error: slurm_commons: get_list_dic_files: n_temp /= 1; %s' % (n_temp, )
    which_path = path_repo + mass_srch_str + '/'

    if srch_regex is None:
      this_dic = get_files_lists(which_path)
    else:
      this_dic = get_files_lists(which_path, srch_regex=srch_regex)
    list_of_dic.append(this_dic)

  return list_of_dic

#=================================================================================================================
def get_files_lists(path, srch_regex=None):
  """
  This function receives the full path to a directory whose name is specified by the mass of the tarck,
  e.g. /home/ehsan/tracks/M13.6/. This directory has at least four subdirectories with the following names:
  - hist/ --> to store the history files, i.e. evolutionary tracks
  - profiles/ --> to store profile snapshots during the evolutin of the model at various phases of evolution.
  - gyre_in/ --> input GYRE files are stored in this directory. Each file here have an equivalent in the "profiles"
                 subdirectory.
  - gyre_out/ --> the output of GYRE calculations are sotred in this directory with *.h5 file extension.

  An ordered dictionary with minimum four items will be returned. The keys are: hist_files, prof_files, gyre_in_files and
  gyre_out_files. The values are lists of files per each subdirectory in path

  @param path: full path to the directory with four subdirectories with names "hist", "profiles", "gyre_in" and
               "gyre_out"
  @type path: string
  @param srch_regex: optional argument to constrain the search with a prescribed regular expression
  @type srch_regex: string
  @return: dictionary with at leat four items is returned. Each value is a list of files present in "path" subdirectories.
  @rtype: dictionary of lists.
  """

  flag = os.path.exists(path)
  if not flag:
    print 'Error: slurm_commons: get_files_lists: %s does not exist!' % (path, )
    print '       Maybe the directory exists but access is not permitted.'
    raise SystemExit, 0
  flag = os.path.isdir(path)
  if not flag:
    message = 'Error: slurm_commons: get_files_lists: %s exists, but is not a directory!' % (path, )
    raise SystemExit, message

  sub_dir = os.listdir(path)
  n_sub_dir = len(sub_dir)
  if n_sub_dir == 0:
    message = 'Error: slurm_commons: get_files_lists: %s has no subdirectories!' % (path, )
    raise SystemExit, message
  sub_dir = sorted(sub_dir)

  dic = OrderedDict({})
  dic['full_path'] = path

  for i in range(n_sub_dir):
    which_sub_dir = sub_dir[i]
    if sub_dir[i] == 'hist':
      key = 'hist_files'; ext = 'hist'
    elif sub_dir[i] == 'profiles':
      key = 'prof_files'; ext = 'prof'
    elif sub_dir[i] == 'gyre_in':
      key = 'gyre_in_files'; ext = 'gyre'
    elif sub_dir[i] == 'gyre_out':
      key = 'gyre_out_files'; ext = 'h5'
    else:
      continue

    full_path = path + sub_dir[i] + '/'

    if srch_regex is None:
      value = get_files(full_path, ext)
    else:
      value = get_files(full_path, ext, srch_regex=srch_regex)

    this_dic = {key: value}

    dic.update(this_dic.items())

  return dic


#=================================================================================================================
def get_files(path, ext, srch_regex=None):
  """
  This function simply returns the list of specified files in a directory
  specified by the input path
  @param path: string, specifying the location of the history files, e.g. /Users/home/files/M05.6/hist/
  Make sure that the path ends with a slash "/".
  @type path: string
  @param ext: the file extension to look for, e.g. "hist" or "gyre" without * or .
  @type: string
  @return: list of the full path to the whole files found
  @rtype: list of strings
  """
  flag = os.path.exists(path)
  if not flag:
    message = 'Error: slurm_commons: get_files: %s does not exist!' % (path, )
    raise SystemExit, message

  dic_params = set_params()
  evol = dic_params['evol']
  #use_hist_regex = dic_params['use_hist_regex']
  #use_prof_regex = dic_params['use_prof_regex']
  #use_h5_regex   = dic_params['use_h5_regex']


  if srch_regex is None:
    if evol == '*':
      srch_str = 'M*.' + ext
    else:
      srch_str = 'M*-' + evol + '-*.' + ext
    if ext == 'hist': srch_str = 'M*.' + ext

  if srch_regex is not None:
    srch_str = srch_regex + '*' + ext

  files = glob.glob(path + srch_str)
  len_files = len(files)

  if len_files == 0:
    print 'Warning: slurm_commons: get_files: No *.%s files found in %s ' % (ext, path)
    print 'Warning: slurm_commons: get_files: srch_str = %s' % (srch_str, )

  return files

#=================================================================================================================
def gen_mass_dir_srch_str(start, end, step, fmt):
  """
  This function receives start, end, stap and fmt for mass directories of interest,
  and returns the corresponding search string.

  @param start, end, step, fmt: Inputs, specifying the range, step and output format
  @type start: float
  @type end: float
  @type step: float
  @type fmt: format string
  @return: number of directories and a search string
  @rtype: integer and string
  """

  if start > end:
    message = 'Error: slurm_commons: gen_mass_dir_srch_str: start > end'
    raise SystemExit, message

  if start == end:
    n = 1
    mass_srch_str = 'M' + fmt.format(start)
    return n, mass_srch_str

  ceil_start = float(ceil(start))
  new_end = ceil_start - step

  if end <= new_end:
    n = int( (end - start)/step ) + 1
    str_start = fmt.format(start)
    str_end   = fmt.format(end)
    len_end   = len(str_end)
    mass_srch_str = 'M' + str_start
    mass_srch_str = mass_srch_str[:4] + '[' + mass_srch_str[4:]
    mass_srch_str += '-' + str_end[len_end-1:] + ']'
    return n, mass_srch_str

  if end > new_end:
    print 'Warning: slurm_commons: gen_mass_dir_srch_str: upper bound "end" shifted from %s to %s.' % (end, new_end)
    print '         You must repeat sepaarately from %s to %s later.' % (ceil_start, end)

    end = new_end
    n = int( (end - start)/step ) + 1
    str_start = fmt.format(start)
    mass_srch_str = 'M' + str_start
    len_str = len(mass_srch_str)
    mass_srch_str = mass_srch_str[:4] + '[' + mass_srch_str[4:]
    mass_srch_str += '-9]'

    return n, mass_srch_str

#=================================================================================================================
def find_incomplete_jobs(gyre_in, gyre_out, regex):
  """
  """
  if not os.path.exists(gyre_in):
    message = 'Error: slurm_commons: find_incomplete_jobs: %s does not exist' % (gyre_in, )
    raise SystemExit, message

  if gyre_in[-1] != '/': gyre_in += '/'
  if gyre_out[-1] != '/': gyre_out += '/'

  files_in = glob.glob(gyre_in + regex)
  files_in.sort()
  n_in = len(files_in)
  files_out = glob.glob(gyre_out + '*')
  n_out = len(files_out)
  if n_out>1: files_out.sort()

  print '\n - slurm_commons: find_incomplete_jobs: N_input=%s, and N_output=%s' % (n_in, n_out)

  if n_in == 0:
    message = 'Error: slurm_commons: find_incomplete_jobs: Input directory: %s is empty' % (gyre_in, )
    raise SystemExit, message

  if n_in == n_out:
    print '   slurm_commons: find_incomplete_jobs: n_in=%s, n_out=%s. All jobs are Completed' % (n_in, n_out)
    return []

  if n_out > n_in:
    print '   slurm_commons: find_incomplete_jobs: (n_out=%s) > (n_in=%s). Over completed!' % (n_out, n_in)
    return []

  if n_in > n_out:
    n_fail = n_in - n_out
    print '\n - slurm_commons: find_incomplete_jobs: %s jobs possibly failed' % (n_fail, )

    list_in_cores = []
    for file_in in files_in:
      ind_slash = file_in.rfind('/')
      ind_point = file_in.rfind('.')
      list_in_cores.append(file_in[ind_slash+1 : ind_point])
    suffix_in = file_in[ind_point:]
    list_out_cores = []
    for file_out in files_out:
      ind_slash = file_out.rfind('/')
      ind_point = file_out.rfind('.')
      list_out_cores.append(file_out[ind_slash+1 + 7 : ind_point])
    suffix_out = file_out[ind_point:]

    list_incomplete = []
    for i, core_in in enumerate(list_in_cores):
      if core_in not in list_out_cores:
        list_incomplete.append(gyre_in + core_in + suffix_in)

  n_fail = len(list_incomplete)
  print '\n - slurm_commons: find_incomplete_jobs: Number of Incomplete jobs = %s \n' % (n_fail, )

  return list_incomplete

#=================================================================================================================

  
import sys, os, glob
import logging
from random import shuffle
import job_array_commons as jac
import job_array_gyre as jag

#=================================================================================================================
# Paths
KIC_repos = '/STER/mesa-gyre/KIC-10526294-aov/'
srch_in   = 'M03.*/gyre_in/*.gyre'
srch_out  = 'M03.*/gyre_out/*.h5'
files_in  = glob.glob(KIC_repos + srch_in)
files_out = glob.glob(KIC_repos + srch_out)
n_in      = len(files_in)
n_out     = len(files_out)
print ' - Found {0} input and {1} output files'.format(n_in, n_out)

# Edit Urgent Path/Specs in jag
jag.path_base      = KIC_repos
jag.nmlst_filename = KIC_repos + 'gyre2.3.nmlst'
jag.search_string  = srch_in
jag.pause_min      = 25
jag.delay          = 3
jag.gyre_out_prefix= 'ad-sum-'
jag.gyre_out_suffix= '.h5'

#jag.path_base      = '/STER/ehsan/models/comp_Sch_Led/'
#jag.nmlst_filename = '/STER/ehsan/models/comp_Sch_Led/gyre.in'
#jag.search_string  = jag.path_base + 'M03.2/gyre_in/*.gyre'

n_submit = 500000      # only submit the first n_submit jobs

# shuffle
ind_rand = range(n_in)
shuffle(ind_rand)
files_in = [files_in[i] for i in ind_rand]

#=================================================================================================================
def main():
  if True:
    set_in  = jac.get_set_filenames(files_in)
    set_out = jac.get_set_filenames(files_out)
    
    list_missed = []
    for i, f_in in enumerate(set_in):
      found = f_in in set_out
      if not found: list_missed.append(f_in)
    n_miss = len(list_missed)
    print ' - Missed {0} jobs'.format(n_miss)
    
    if n_miss > 0:
      fp_missing_gyre_in = jac.reconstruct_gyre_in_out(list_missed, path_repos=KIC_repos, 
                                                       gyre_in_out='gyre_in', suffix='.gyre')
      print ' - Check if all missing inputs truly exist on disk ...'
      for i, f_in in enumerate(fp_missing_gyre_in): 
        if not os.path.isfile(f_in):
          raise SystemExit, '{0}: {1} does not exist!'.format(i, f_in)

      print ' - Check if missing inputs have no output conterparts ...'
      fp_missing_gyre_out = jac.reconstruct_gyre_in_out(list_missed, path_repos=KIC_repos, 
                                                        gyre_in_out='gyre_out', prefix='ad-sum-', suffix='.h5')
      for j, f_out in enumerate(fp_missing_gyre_out):
        if os.path.isfile(f_out):
          #raise SystemExit, '{0}: {1} already exists!'.format(j, f_out)
          print '{0}: {1} already exists!'.format(j, f_out)
      
      fp_gyre_in = []
      n_gyre_in  = min([n_submit, n_miss])
      for i, f_in in enumerate(fp_missing_gyre_in): 
        fp_gyre_in.append(f_in)
        if i == n_gyre_in-1: break
      status = jag.submit_jobs(fp_gyre_in)  
      
      #status = jag.submit_jobs( list(fp_missing_gyre_in) )
      
  return status

#=================================================================================================================
if __name__ == '__main__':
  status = main()
  sys.exit(status)
#=================================================================================================================

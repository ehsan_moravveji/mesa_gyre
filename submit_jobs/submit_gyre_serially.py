
import sys, os, glob
import logging
import time
import job_array_commons as jac 

#=================================================================================================================
# Logging settings
logging.basicConfig(filename='Log-JobArray-Serial.log', format='%(levelname)s: %(asctime)s: %(message)s', level=logging.DEBUG)
#=================================================================================================================

path_repos    = '/STER/ehsan/models/BSG-instability-strips/LOGS/'
gyre_in_files = sorted(glob.glob(path_repos + '*.gyre'))
gyre_namelist = '/STER/ehsan/models/BSG-instability-strips/gyre.nmlst'
n_gyre_in     = len(gyre_in_files)


print '\n - Found {0} input files.'.format(n_gyre_in)


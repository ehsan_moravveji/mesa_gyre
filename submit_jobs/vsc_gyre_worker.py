
import sys, os, glob
import logging
import numpy as np
import slurm_commons
import job_array_commons as jac
import job_array_gyre as jag


##################################################################################
jag.path_namelists = '/data/leuven/307/vsc30745/reports/namelists/'
jag.GYRE_dir = '/data/leuven/307/vsc30745/gyre4.2/'
jag.gyre_out_prefix = 'ad-sum-'  # e.g. 'ad-sum-'
jag.gyre_out_suffix = '.h5'  # e.g. '-sum.h5'
##################################################################################
path_mesa = '/data/leuven/307/vsc30745/models/KIC-7760680/'
nmlst_master_file = '/scratch/leuven/307/vsc30745/KIC-7760680/gyre-4.2.nmlst'
##################################################################################
# logging.basicConfig(filename='delete.log', format='%(levelname)s: %(asctime)s: %(message)s', level=logging.DEBUG)
##################################################################################
##################################################################################
##################################################################################
def main():
    """
    The main caller
    """
    if False:
        print ' - Resubmit ~ 1000 unfinished jobs'
        base_path = '/scratch/leuven/307/vsc30745/KIC-7760680/'
        # dirs = ['a09/', 'a05_Ne/', 'OP_a09/', 'OP_a09_p13/', 'OP_a05_Ne/']
        dirs = ['a09/']
        list_missing_inputs = []
        for i_dir, dr in enumerate(dirs):
            path_in = base_path + dr
            list_input_files = slurm_commons.get_files(path_in, 'gyre', srch_regex='M*/gyre_in/')
            list_output_files = slurm_commons.get_files(path_in, 'h5', srch_regex='M*/gyre_out/')
            n_input = len(list_input_files)
            n_output = len(list_output_files)
            print '   In "{0}" directory, there are {1} inputs and {2} outputs'.format(dr, n_input, n_output)
            if n_input == n_output: continue
            if n_output == 0:
                list_missing_gyre_in = list_input_files
            else:
                list_missing_gyre_in = jac.find_missing_gyre_in(path_in, list_input_files, list_output_files)
            list_missing_inputs += list_missing_gyre_in

        n_missing = len(list_missing_inputs)
        print ' - Found {0} missing input files without output'.format(n_missing)

        access = jac.check_password()
        if not access:
            logging.error('vsc_gyre_worker: main: Wrong password.')
            raise SystemExit, 'Error: main: Wrong password. Retry ...'

        list_master_nmlst =  jag.read_sample_namelist_to_list_of_strings(nmlst_master_file)
        list_nmlst_files = jag.gen_namelists(list_master_nmlst, list_missing_inputs)
        jac.collect_nmlst_files_for_worker(list_nmlst_files, ascii_out=path_mesa + 'GYRE-namelists-a09.txt', trim=True)


    if True:
        print ' - Get the list of all available input GYRE files'
        base_path = '/scratch/leuven/307/vsc30745/KIC-7760680/'
        print '   base_path: {0}'.format(base_path)
        gyre_in_files = slurm_commons.get_files(base_path, '.gyre', srch_regex='M*/gyre_in/')
        n_input = len(gyre_in_files)
        print '   Found {0} Input GYRE files'.format(n_input)
        access = jac.check_password()
        if not access:
            logging.error('vsc_gyre_worker: main: Wrong password.')
            raise SystemExit, 'Error: main: Wrong password. Retry ...'

        list_master_nmlst = jag.read_sample_namelist_to_list_of_strings(nmlst_master_file)
        list_nmlst_files  = jag.gen_namelists(list_master_nmlst, gyre_in_files)
        jac.collect_nmlst_files_for_worker(list_nmlst_files, ascii_out=path_mesa + 'GYRE-namelists.txt', 
                                           trim=True)

    return None

##################################################################################
if __name__ == '__main__':
    status = main()
    sys.exit(status)

##################################################################################

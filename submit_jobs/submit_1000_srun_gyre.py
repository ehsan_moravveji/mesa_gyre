
"""
This script creates 1000 jobs, where each job is one line of srun command.
This is good for keeping the Pleiad CPUs, configured with SLURM busy for
nearly 24 hours, hence fitting into the normal queue.
"""

import sys, os, glob
import numpy as np
import logging
import job_array_commons as jac
import job_array_gyre as jag


###########################################################################
# # JAG settings
# jag.queue = 'normal' # 'normal', 'low', 'high'
# jag.which_gyre     = jag.gyre_ad
# jag.gyre_out_prefix= 'ad-sum-'
# jag.gyre_out_suffix= '.h5'
# jag.n_srun_per_sbatch = 700
# jag.job_name = 'CoRoT-HD50230'

# ###########################################################################
# # This Script settings
# path_in = '/STER/mesa-gyre/HD50230-new-logD/'
# srch_in = 'M0*/gyre_in/*.gyre'
# srch_out = 'M0*/gyre_out/*.h5'
# nmlst_file = path_in + 'gyre3.2.2.nmlst'

########################################################################### For Opacity Project
# JAG settings
jag.queue = 'normal' # 'normal', 'low', 'high'
jag.which_gyre     = jag.gyre_nad
jag.gyre_out_prefix= 'nad-sum-'
jag.gyre_out_suffix= '.h5'
jag.n_srun_per_sbatch = 10
jag.job_name = 'IS-GYRE'

###########################################################################
# This Script settings
path_in = '/STER/mesa-gyre/IS_Fex1.75_Nix1.75_hiM/'
srch_in = 'M*/gyre_in/*.gyre'
srch_out = 'M*/gyre_out/*.h5'
nmlst_file = '/STER/mesa-gyre/IS_Fex1.75_Nix1.75_hiM/gyre-nad-4.4.nmlst'

###########################################################################
def main(path_in, srch_in, srch_out, nmlst_file):
    """
    The main caller.
    @param path_in: Full path to the input gyre_in files.
    @type path_in: string
    @param srch_in: search string for globbing the input files.
    @type srch_in: string
    @param srch_out: search string for globbing the available output files.
    @type srch_out: string
    @param nmlst_file: Full path to the file which serves as the general namelist to be commonly
             used among all jobs. Only the path to the input and output files will be altered.
    @type nmlst_file: string
    """
    list_input_files = jac.glob_files(path_in, srch_in)
    list_output_files = jac.glob_files(path_in, srch_out)
    list_missing_gyre_in = jac.find_missing_gyre_in(path_in, list_input_files, list_output_files)
    list_str_sample_namelist = jag.read_sample_namelist_to_list_of_strings(nmlst_file)
    jag.execute_list_of_sbatch_commands(list_str_sample_namelist, list_missing_gyre_in)

    return None

###########################################################################
if __name__ == "__main__":

    status = main(path_in, srch_in, srch_out, nmlst_file)
    sys.exit(status)

###########################################################################

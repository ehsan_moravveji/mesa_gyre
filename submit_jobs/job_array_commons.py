
import sys, os, glob
import logging
import subprocess
from random import shuffle

#=================================================================================================================
#=================================================================================================================
def collect_nmlst_files_for_worker(list_nmlst_files, ascii_out, trim=False):
    """
    Write the full path to all namelist files as a single ascii file for the worker to pick up and use to call
    GYRE, one per each line.
    @param list_nmlst_files: list containing the full path to where the namelists are stored on disk.
            A line break character, '\n' is appended to the end of each item, to make sure each line refers to
            a single namelist file
    @type list_nmlst_files: list of strings
    @param ascii_out: full path to the file that stores the namelist filenames. This file is later used as an input
            to the worker frame on VSC to execute the jobs sequentially.
    @type ascii_out: string
    @return: None
    @rtype: None
    """
    n_files = len(list_nmlst_files)
    output = ['namelist \n']
    for i, nmlst in enumerate(list_nmlst_files):
        if trim:
          ind = nmlst.rfind('/') + 1
          nmlst = nmlst[ind : ]
        output.append(nmlst + '\n')

    with open(ascii_out, 'w') as w: w.writelines(output)
    print ' - JAC: collect_nmlst_files_for_worker: saved {0}'.format(ascii_out)

    return None

#=================================================================================================================
def find_missing_gyre_in(path_repos, gyre_in_files, gyre_out_files):
    """
    Return only those missing input files that do not have an output counterpart. This is
    necessary when resubmiting an incomplete jobs, and you want to avoid repeating the
    whole computations.
    WARNING: For now, some of the prefices and suffices used in calling reconstruct_gyre_in_out()
        are hard-coded. For more flexibility, the arguments shall change. Thus, use with care.
    @param path_repos: Full path to the repository, where the files sit
    @type path_repos: string
    @param gyre_in_files: list of full path to the input gyre files
    @type gyre_in_files: list of string
    @param gyre_out_files: list of full path to the output gyre files
    @type gyre_out_files: list of string
    @return: list of full path to those files that have no output counterparts, i.e. the missing files
    @rtype: list of strings
    """
    if len(gyre_out_files) == 0:
      return gyre_in_files

    set_in  = get_set_filenames(gyre_in_files)
    set_out = get_set_filenames(gyre_out_files)

    list_missed = []

    for i, f_in in enumerate(set_in):
      found = f_in in set_out
      if not found:
        list_missed.append(f_in)

    n_miss = len(list_missed)
    print ' - jac: find_missing_gyre_in: Found {0} missing files'.format(n_miss)

    if n_miss == 0:
      logging.error('jac: find_missing_gyre_in: Found no missing file')
      raise SystemExit, ' - jac: find_missing_gyre_in: Found no missing file'

    if n_miss > 0:
      fp_missing_gyre_in = reconstruct_gyre_in_out(list_missed, path_repos=path_repos,
                                                       gyre_in_out='gyre_in', suffix='.gyre')

      print ' - jac: find_missing_gyre_in: Check if missing inputs have no output conterparts ...'
      fp_missing_gyre_out = reconstruct_gyre_in_out(list_missed, path_repos=path_repos,
                                                        gyre_in_out='gyre_out', prefix='ad-sum-', suffix='.h5')
      for j, f_out in enumerate(fp_missing_gyre_out):
        if os.path.isfile(f_out):
          #raise SystemExit, '{0}: {1} already exists!'.format(j, f_out)
          print '   jac: find_missing_gyre_in: {0}: {1} already exists!'.format(j, f_out)

      return list(fp_missing_gyre_in)

#=================================================================================================================
def glob_files(path, srch, do_sort=False):
    """
    return the list of available files residing in "path", and filtered through "srch" for regex matching.
    @param path: full path to where the files exist
    @type path: string
    @param srch: search string to be used as a glob argument.
    @type srch: string
    @param do_sort: Decide wether or not to sort the list of output files; takes extra time. default=False
    @type do_sort: boolean
    @return: list of full path to the files in the "path" that match "srch" criteria
    @rtype: list of string
    """
    if not os.path.exists(path):
        logging.error('jac: glob_files: {0} does not exist'.format(path))
        raise SystemExit, 'jac: glob_files: {0} does not exist'.format(path)

    if path[-1] != '/': path += '/'
    list_out = glob.glob(path + srch)
    n_out = len(list_out)
    if n_out == 0:
        logging.info('jac: glob_files: Found No Files!')
        # raise SystemExit, 'jac: glob_files: Found No Files!'
        return []
    else:
        print ' - jac: glob_files: Found {0} files in {1} using {2}'.format(n_out, path, srch)
    if do_sort:
      list_out.sort()
    else:
      ind_rand = range(n_out)
      shuffle(ind_rand)
      list_out = [list_out[ind] for ind in ind_rand]

    return list_out

#=================================================================================================================
def get_set_filenames(files):
  """
  remove trailing '/' and leading '.' from filenames to get their core. return a "set" with filename cores
  @param files: list of the full path to the available files
  @type files: list of strings
  @return: a set with only the core of the filename remained
  @rtype: set
  """
  a = set()
  for i, f in enumerate(files):
    if '-sum.h5' in f:
      ind_from = f.rfind('/') + 1
      ind_to   = f.rfind('-sum.h5')
    elif '/nad-sum-' in f:
      ind_from = f.rfind('/nad-sum-') + 9
      ind_to   = f.rfind('.h5')
    elif '/ad-sum-' in f:
      ind_from = f.rfind('/ad-sum-') + 8
      ind_to   = f.rfind('.h5')
    elif '.gyre' in f:
      ind_from = f.rfind('/') + 1
      ind_to     = f.rfind('.gyre')
    else:
      raise SystemExit, 'get_set_filenames: Error trimming {0}'.format(f)
    a.add(f[ind_from : ind_to])

  return a

#=================================================================================================================
def reconstruct_gyre_in_out(strs, path_repos='', gyre_in_out=None, prefix='', suffix=None):
  """
  Receives a set/list of strings of filename cores, and returns a set of full path to the input file.
  @param strs: a set of core names of the gyre_in or gyre_out files
  @type strs: set
  @param path_repos: the full path to where the repository sits, e.g. /STER/mesa-gyre/Grid/
  @type path_repos: string
  @param gyre_in_out: choose either 'gyre_in' or 'gyre_out' without leading/trainling '/'
  @type gyre_in_out: string
  @param prefix: what to add after the directory path and in front of the core name, e.g. prefix='ad-sum-'; default=''
  @type prefix: string
  @param suffix: mainlly the file extension, e.g. '.gyre' for gyre_in files or '-sum.h5' for gyre_out files. it cannot
     be an empty field.
     WARNING: Do not forget '.' in the suffix, if it is just a file extension
  @type suffix: string
  @return a set of full path to the file names, reconstructed using the prefix and suffix
  @rtype: set
  """
  if path_repos == '' or not os.path.exists(path_repos):
    raise SystemExit, 'reconstruct_gyre_in_out: path_repos:{0} does not exist'.format(path_repos)
  if gyre_in_out is None:
    raise SystemExit, 'reconstruct_gyre_in_out: Retry by setting "gyre_in_out" path'
  if suffix is None:
    raise SystemExit, 'reconstruct_gyre_in_out: Retry by setting: "suffix"'
  if '/' in gyre_in_out:
    raise SystemExit, 'reconstruct_gyre_in_out: Retry after removing "/" from gyre_in_out={0}'.format(gyre_in_out)
  if path_repos[-1] != '/': path_repos += '/'

  a = set()
  for i, s in enumerate(strs):
    pieces = s.split('-')
    # m_str  = pieces[0]    # this is the original form and should work when the format of m_str is identical everywhere on the filename
    m_str = '{0:05.2f}'.format(float(pieces[0][1:]))  # this is a dirty trick to work around it in those cases where m_str has different formats in one single filename
    m_str = 'M' + str(m_str)
    if 'M' not in m_str:
      print i, s, m_str
      raise SystemExit, 'reconstruct_gyre_in_out: problem with filenames!'
    fp     = path_repos + m_str + '/' + gyre_in_out + '/' + prefix + s + suffix
    a.add(fp)

  return a

#=================================================================================================================
def check_password():
  """
  Introduce a password, and ask the user to insert it correctly. if failed, no authentication is granted to submit jobs
  @return flag: the result of comparing the password as introduced to the user
  @rtype flag: boolean
  """
  from random import randrange
  the_pass = 'Y{0}wF{1}'.format(randrange(10), randrange(10))
  print '\n - Check Password '
  user_pass=raw_input('   Insert "{0}": '.format(the_pass))
  flag = the_pass == user_pass
  if flag: print '   Access granted.'
  else: print '   Access Failed!'

  return flag

#=================================================================================================================
def get_Q_report(report=False):
  """
  Get the status of the queue, returns the number of pending/running/total jobs in the queue as a dictionary.
  @param report: if True, an on-screen message will appear giving a report of the queue
  @type report: boolean
  @return: dictionary with the following key-value items:
    - n_jobs: the number of total jobs in the queue, i.e. n_jobs = n_PD + n_R
    - n_PD: the number of jobs in pending (PD) status
    - n_R: the number of jobs in the running (R) status
    - max_id: the maximum id number of jobs in the queue. The job could be in PD or R status
  @rtype: dictionary
  """
  # To check the priority of the jobs: squeue --format "%.18i %.9P %.8j %.8u %.8T %.10M %.9l %.6D %R %Q"
  lsq = "squeue -o \"%.7i %.9P %.55j %.8u %.2t %.10M %.6D %R\" -r "
  the_Q = subprocess.Popen(lsq, shell=True,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)

  dic = {}
  dic['n_jobs'] = 0
  dic['n_PD']   = 0
  dic['n_R']    = 0
  dic['max_id'] = 0
  dic['n_my_PD']= 0
  dic['n_my_R'] = 0

  # wait for the process to terminate
  with the_Q.stdout as r: lines = r.readlines()
  header_line = lines.pop(0)
  n_jobs = len(lines)
  errcode = the_Q.returncode
  if n_jobs == 0:
    print '\n - get_Q_report: No jobs in the queue'
    logging.info('JAC: get_Q_report: No jobs in the queue')
    return dic

  if errcode is not None:
    logging.errcode('JAC: get_Q_report: Queue exit error code is {0}'.format(errcode))
    raise SystemExit

  list_id = []
  list_Q_name = []
  list_Job_name = []
  list_user = []
  list_status = []
  list_runtime = []
  list_pleiad = []
  for i_line, line in enumerate(lines):
    line = line.rstrip('\r\n').split()
    list_id.append(int(line[0]))
    list_Q_name.append(line[1])
    list_Job_name.append(line[2])
    list_user.append(line[3])
    list_status.append(line[4])
    list_runtime.append(line[5])
    list_pleiad.append(line[7])

  ind_PD  = []
  ind_R   = []
  ind_PD  = [i for i in range(n_jobs-1) if list_status[i] == 'PD']
  ind_R   = [i for i in range(n_jobs-1) if list_status[i] == 'R']
  n_PD    = len(ind_PD)
  n_R     = len(ind_R)
  list_id = sorted(list_id)
  max_id  = max(list_id)

  if report:
    print '\n - get_Q_report: Num Running Jobs = {0}'.format(n_R)
    print '                 Num Pending Jobs = {0}'.format(n_PD)
    print '                 Num Total Jobs   = {0}'.format(n_jobs)
    print '                 Max ID           = {0}\n'.format(max_id)

  my_PD   = [i for i in range(n_jobs-1) if list_status[i] == 'PD' and list_user[i] == 'ehsan']
  my_R    = [i for i in range(n_jobs-1) if list_status[i] == 'R'  and list_user[i] == 'ehsan']
  n_my_PD = len(my_PD)
  n_my_R  = len(my_R)

  if report:
    print '                 Num My Pending   = {0}'.format(n_my_PD)
    print '                 Num My Running   = {0}\n'.format(n_my_R)

  dic['n_jobs'] = n_jobs
  dic['n_PD']   = n_PD
  dic['n_R']    = n_R
  dic['max_id'] = max_id
  dic['n_my_PD']= n_my_PD
  dic['n_my_R'] = n_my_R

  logging.info('JAC: get_Q_report: Running:{0}, Pending:{1}, Total:{2}, MaxID:{3}'.format(n_R, n_PD, n_jobs, max_id))

  return dic

#=================================================================================================================




import os, glob
import slurm_commons as scm
from slurm_commons import *
import gyre_namelist as gnl

#=================================================================================================================
def submit_gyre_project():
  """
  This is an example for submitting a set of GYRE jobs based on a directory of available .gyre files
  """
  platform = Pleiad
  
  # Customize paths
  dic_path = scm.set_paths(platform)
  dic_path['path_mesa_gyre'] = '/STER/ehsan/models/interactive_eta_ov/LOGS/'
  
  # Customize parameters 
  param = scm.set_params()
  param['mass_start'] = 3.1
  param['mass_end']   = 3.1

  # Get the list of available input files:
  input_files = glob.glob(dic_path['path_mesa_gyre'] + 'M03.1/gyre_in/M03.1-eta0.00-ov0.005*-MS-*.gyre')
  #input_files = glob.glob(dic_path['path_mesa_gyre'] + 'M03.2/gyre_in/M03.2-eta0.45-ov0.000*-MS-*.gyre')
  n_input = len(input_files)
  print '  %s input GYRE files are detected.' % (n_input)  

  # Load the dictionary with the desired namelist values
  dic_nmlst = gnl.namelist_disentangle_eta_ov()
  
  passwd = 'Zyq8Y'
  user_pass = raw_input(' - Insert the code %s to continue: ' % (passwd, )) 
  if user_pass != passwd:
    message = 'Error: submit_gyre_project: Wrong password. Authentication not granted!'
    raise SystemExit, message
  
  # Iteratively, generate a batch script for each identified input file, and submit one job for it.
  for i_in, input_file in enumerate(input_files):
    dic_nmlst['path_input'] = input_file
    dic_nmlst['path_output'] = gnl.gen_output_fname_from_input_fname(platform, input_file)
    
    namelist_name = gnl.gen_namelist_name_from_input_fname(input_file)      
    namelist_full_path = dic_path['path_gyre_namelists'] + namelist_name    
    gnl.write_gyre_namelist(namelist_full_path, dic_nmlst)
    # Submit & Execute each namelist
    gnl.write_exec_gyre_script(platform, namelist_full_path)  
  
#=================================================================================================================
if __name__ == '__main__':
  submit_gyre_project()
#=================================================================================================================

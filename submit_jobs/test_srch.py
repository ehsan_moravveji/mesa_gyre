#! /usr/bin/python

import glob, sys 
from collections import OrderedDict
import commons
import slurm_commons as scm
from slurm_commons import * 
import gyre_namelist as gnl


platform = commons.set_platform()['Platform']
dic_paths = commons.set_paths()
dic_params = commons.set_srch_param()

print ' .:. Available Common Path Information: '
for keys, vals in dic_paths.items():
  print ' - ', keys, ':', vals
print
path_equil = scm.set_paths(platform)['path_equil']
print ' .:. Grid Search Parameters: '
for keys, vals in dic_params.items():
  print ' - ', keys, ':', vals
print

mass_from = dic_params['mass_from']
mass_to   = dic_params['mass_from']
mass_step = dic_params['mass_step'] 
fmt = dic_params['mass_fmt']
srch_hist_regex = dic_params['srch_hist_regex']
list_dic_files = scm.get_list_dic_files(path_equil, mass_from, mass_to, mass_step, fmt, srch_hist_regex)
print ' From: ', list_dic_files[0]
print ' To:   ', list_dic_files[-1]
print

n_dir = len(list_dic_files)


# collect a list of failed GYRE calls
failed = []
for i_dir in range(n_dir):
  which_dic_file = list_dic_files[i_dir]
  
  # check if there are any -PMS- files
  which_gyre_in = which_dic_file['gyre_in_files']
  len_gyre_in = len(which_gyre_in)
  for k in range(len_gyre_in):
    evol_ind = which_gyre_in[k].rfind('PMS')
    if evol_ind > 0:
      print '  PMS found: %s, index = %s' % (which_gyre_in[k], evol_ind)
  
  # get a list of unsuccessful GYRE calls per each directory
  list_failed = commons.detect_incomplete_gyre_jobs(which_dic_file['gyre_in_files'], which_dic_file['gyre_out_files'])
  failed += list_failed  
n_failure = len(failed)  

num_current_slurm_jobs = commons.get_num_current_slurm_jobs()
max_jobs_slurm = dic_paths['n_max_slurm_jobs']
print ' - Number of Current Running Jobs on SLURM = %s' % (num_current_slurm_jobs, )
print ' - Maximum Number of Jobs to Run on SLURM = %s' % (max_jobs_slurm, )
num_max_allowed_jobs = max_jobs_slurm - num_current_slurm_jobs
  
print ' - Total Number of Requested/Failed Calls to GYRE: %s' % (n_failure, )
print '   from %s' % (failed[0], )
print '   to   %s' % (failed[n_failure-1], )
print




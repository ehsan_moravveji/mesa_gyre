"""
This script finds the failed/incomplete jobs in specified paths, and provides an input file for the worker frame 
to resubmit the incomplete jobs.
This is designed to use VSC.
"""

import glob, os
import slurm_commons as sc
import gyre_namelist as gnl


path_grid = '/scratch/leuven/307/vsc30745/Grid/'
path_dump = '/data/leuven/307/vsc30745/reports/namelists/'
if not os.path.exists(path_dump): os.makedirs(path_dump)
platform = sc.VSCentrum
dic_paths  = sc.set_paths(platform)
mass = 5.40

# Specify the location of the Grid
dic_paths['path_mesa_gyre'] = path_grid
dic_paths['path_gyre_namelists'] = path_dump

mass_str = str(mass)
mass_dir = 'M%05.2f' % (mass,)

list_incomplete = sc.find_incomplete_jobs(gyre_in=path_grid + mass_dir + '/gyre_in/',
                                          gyre_out = path_grid + mass_dir + '/gyre_out/',
                                          regex='*-MS-*.gyre')

# Specify the parameters  
dic_params = sc.set_params()
dic_params['mass_start'] = mass
dic_params['mass_end'] = mass
dic_params['mass_step'] = 0.10
dic_params['z_start'] = 0.014 
dic_params['z_end'] = 0.014
dic_params['z_step'] = 0.001
  
param_name = 'M%05.2f-%05.2f-eta%4.2f-%4.2f-ov%5.3f-%5.3f-sc%4.2f-Z%5.3f-%5.3f.re' % (dic_params['mass_start'], dic_params['mass_end'],
                                                                                      dic_params['eta_start'], dic_params['eta_end'],
                                                                                      dic_params['ov_start'], dic_params['ov_end'],
                                                                                      dic_params['sc'],
                                                                                      dic_params['z_start'], dic_params['z_end'])
ascii_out = sc.set_paths(platform)['path_gyre_src'] + param_name
  
# Get the list of available input files:
list_incomplete.sort()
print dic_paths['path_mesa_gyre'] + mass_str + '/gyre_in/M*-MS-*.gyre'
n_input = len(list_incomplete)
print '  %s input GYRE files are detected.' % (n_input, )  
print '  Namelist files will be stored in %s' % (dic_paths['path_gyre_namelists'], )
print '  %s' % (ascii_out, )
  
passwd = 'Ze6-x'
user_pass = raw_input(' - Insert the code %s to continue: ' % (passwd, )) 
if user_pass != passwd:
  message = 'Error: submit_gyre_project: Wrong password. Authentication not granted!'
  raise SystemExit, message
  
# Load the dictionary with the desired namelist values
dic_nmlst = gnl.namelist_mesa_gyre()

# Iteratively, generate a batch script for each identified input file, and save the filename in a list
# Note: We only save the relative path to the namelist, and write the relative path to the file.
list_path_namelists = []
for i_in, input_file in enumerate(list_incomplete):
  dic_nmlst['path_input'] = input_file
  dic_nmlst['path_output'] = gnl.gen_output_fname_from_input_fname(platform, input_file) # use it when $scratch disc is back to work
    
  namelist_name = gnl.gen_namelist_name_from_input_fname(input_file)     
  list_path_namelists.append(namelist_name + '\n') 
  namelist_full_path = dic_paths['path_gyre_namelists'] + namelist_name    
  gnl.write_gyre_namelist(namelist_full_path, dic_nmlst)

if ascii_out:
  w = open(ascii_out, 'w')
  w.write('namelist\n')
  w.writelines(list_path_namelists)
  w.close()
  
  print '   ascii_out: %s created.' % (ascii_out, )
  
print ' - Done!\n'
  
#=================================================================================================================







import glob, os
from slurm_commons import *

platform = VSCentrum

path_data = '/data/leuven/307/vsc30745/'
path_work = path_data + 'models/KIC-7760680/'
param_name = 'coarse2.input'

mass_from = 3.00
mass_to   = 3.60
mass_step = 0.05
n_mass    = int((mass_to-mass_from)/mass_step + 1)

eta_from  = 0.00
eta_to    = 0.00
eta_step  = 0.03
n_eta     = int((eta_to-eta_from)/eta_step + 1)

ov_from   = 0.030
ov_to     = 0.030
ov_step   = 0.003
n_ov      = int((ov_to-ov_from)/ov_step+1)

z_from    = 0.014
z_to      = 0.014
z_step    = 0.002
n_z       = int((z_to-z_from)/z_step + 1)

Xini_from = 0.71
Xini_to   = 0.71
Xini_step = 0.01
n_Xini       = int((Xini_from - Xini_to)/Xini_step + 1)

sc        = 0.01

logD_from = 0.00
logD_to   = 5.00
logD_step = 0.50
n_logD    = int((logD_to-logD_from)/logD_step + 1)

# param_name = 'M%05.2f-%05.2f-eta%4.2f-%4.2f-ov%5.3f-%5.3f-sc%4.2f-Z%5.3f-%5.3f.in' % (mass_from, mass_to,
#                                                                                       eta_from, eta_to,
#                                                                                       ov_from, ov_to,
#                                                                                       sc,
#                                                                                       z_from, z_to)
path_csv = path_work + param_name

line = []; lines = []
counter = 0
nl = ' \n'

# lines.append('m_ini,eta_ini,ov_ini,z_ini,Xini,sc_ini,logD_ini' + nl) # <-- good for HD 50230
lines.append('m_ini,ov_ini,Xini,z_ini,logD_ini' + nl) # <-- good for KIC 7760680

for i_logD in range(n_logD):
  logD = logD_from + i_logD * logD_step
  for i_Xini in range(n_Xini):
    Xini = Xini_from + i_Xini * Xini_step
    for i_z in range(n_z):
      z = z_from + i_z * z_step
      for i_ov in range(n_ov):
        ov = ov_from + i_ov * ov_step
        for i_eta in range(n_eta):
          eta = eta_from + i_eta * eta_step
          for i_mass in range(n_mass):
            mass = mass_from + i_mass * mass_step

            # the following is good for HD 50230
            # line = '{0:05.2f},{1:04.2f},{2:05.3f},{3:05.3f},{4:02d},{5:04.2f},{6:05.2f} {7}'.format(mass, eta, ov, z, Xini, sc, logD, nl)

            # the following is good for KIC 7760680
            line = '{0:05.2f},{1:05.3f},{2:04.2f},{3:05.3f},{4:05.2f} {5}'.format(
                    mass, ov, Xini, z, logD, nl)

            lines.append(line)
            counter += 1

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

print '\n - Writing CSV file: %s ' % (path_csv, )
w = open(path_csv, 'w')
w.writelines(lines)
w.close()
print ' - Total lines written = %s \n' % (counter, )

n_jobs = n_mass * n_eta * n_ov * n_z * n_logD
time_each_job_expected_min = 90.
time_each_job_exaggerated_min = 120.
total_time_min = n_jobs * time_each_job_expected_min
total_time_exaggerated_min = n_jobs * time_each_job_exaggerated_min
n_nodes = 1.
n_cores_per_node = 20.
n_total_cores = n_nodes * n_cores_per_node
time_per_core_min = total_time_min / n_total_cores
time_per_core_exaggerated_min = total_time_exaggerated_min / n_total_cores
walltime_per_core_hr  = time_per_core_min / 60.0
walltime_per_core_exaggerated_hr = time_per_core_exaggerated_min / 60.0

print ' - Consults:'
print ' - nodes=%d:ppn=%d' % (n_nodes, n_cores_per_node)
print ' - Total number of jobs = %s' % (n_jobs, )
print ' - Average Time per Core = %s [min]; Exaggerated Time per Core = %s [min]' % (time_each_job_expected_min, time_each_job_exaggerated_min)
print ' - Total Time = %s [min]; Exaggerated Time = %s [min]' % (total_time_min, total_time_exaggerated_min)
print ' - Time per Core = %s [min]; Exaggerated Time per Core = %s [min]' % (time_per_core_min, time_per_core_exaggerated_min)
print '\n - WallTime ~ %0.2f [Hr]; Exaggerated WallTime = %0.2f [Hr] \n' % (walltime_per_core_hr, walltime_per_core_exaggerated_hr)




#! /usr/bin/python

import glob, sys 
from collections import OrderedDict
import slurm_commons as scm
from slurm_commons import * 
import gyre_namelist as gnl


platform = Pleiad
dic_paths = scm.set_paths(platform)
dic_params = scm.set_params()

print ' .:. Available Common Path Information: '
for keys, vals in dic_paths.items():
  print ' - ', keys, ':', vals
print
path_equil = dic_paths['path_equil']
print ' .:. Grid Search Parameters: '
for keys, vals in dic_params.items():
  print ' - ', keys, ':', vals
print

mass_from = dic_params['mass_start']
mass_to   = dic_params['mass_end']
mass_step = dic_params['mass_step'] 
fmt = dic_params['mass_fmt']
list_dic_files = scm.get_list_dic_files(path_equil, mass_from, mass_to, mass_step, fmt)

n_dir = len(list_dic_files)
# collect a list of failed GYRE calls
failed = []
for i_dir in range(n_dir):
  which_dic_file = list_dic_files[i_dir]
  
  # check if there are any -PMS- files
  which_gyre_in = which_dic_file['gyre_in_files']
  len_gyre_in = len(which_gyre_in)
  for k in range(len_gyre_in):
    evol_ind = which_gyre_in[k].rfind('PMS')
    if evol_ind > 0:
      print '  PMS found: %s, index = %s' % (which_gyre_in[k], evol_ind)
  
  # get a list of unsuccessful GYRE calls per each directory
  list_failed = scm.detect_incomplete_gyre_jobs(which_dic_file['gyre_in_files'], which_dic_file['gyre_out_files'])
  failed += list_failed  
n_failure = len(failed)  

num_current_slurm_jobs = scm.get_num_current_slurm_jobs()
max_jobs_slurm = dic_paths['n_max_slurm_jobs']
print ' - Number of Current Running Jobs on SLURM = %s' % (num_current_slurm_jobs, )
print ' - Maximum Number of Jobs to Run on SLURM = %s' % (max_jobs_slurm, )
#num_max_allowed_jobs = max_jobs_slurm - num_current_slurm_jobs
  
print ' - Total Number of Requested/Failed Calls to GYRE: %s' % (n_failure, )
print '   from %s' % (failed[0], )
print '   to   %s' % (failed[n_failure-1], )
print

# Plot the number of files per each directory
#scm.plot_file_dir_stat(platform, list_dic_files)

dir_save_namelist = dic_paths['path_gyre_namelists']

# Load the dictionary with the settings for the Peter Papics Kepler binary system
#dic_nmlst = gnl.namelist_papics()
dic_nmlst = gnl.namelist_mesa_gyre()

passwd = 'Y7w0g'
print ' - Password is: %s' % (passwd, )
user_passwd = raw_input(' - Enter the Password to Continue: ' )
flag_passwd = (passwd == user_passwd)
if not flag_passwd:
  print '  Password mismatch. Authentication failed.'
  sys.exit()

for i_fail in range(n_failure):
  dic_nmlst['path_input'] = failed[i_fail]
  dic_nmlst['path_output'] = gnl.gen_output_fname_from_input_fname(failed[i_fail])

  namelist_name = gnl.gen_namelist_name_from_input_fname(failed[i_fail])
  namelist_full_path = dir_save_namelist + namelist_name
  gnl.write_gyre_namelist(namelist_full_path, dic_nmlst)
  
  #gnl.write_exec_gyre_script(platform, namelist_full_path)
  



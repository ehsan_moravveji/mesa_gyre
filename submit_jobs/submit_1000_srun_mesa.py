
"""
Submit an array of MESA jobs with SLURM job array.
"""

import os, glob, sys
import logging
import subprocess
import numpy as np

import job_array_commons as jac


#=================================================================================================================
# Logging settings
logging.basicConfig(filename='Log-JobArray-MESA.log', format='%(levelname)s: %(asctime)s: %(message)s', level=logging.DEBUG)

queue          = 'normal'
batch_filename = 'MW-IS'
param_filename = 'KIC.input'

mesa_dir       = '/STER/ehsan/models/IS_op_mono_tables_hi_mass/'
path_hist_repo = '/STER/mesa-gyre/IS_Fex1.75_Nix1.75_hiM/'
path_repo      = path_hist_repo

# mesa_dir       = '/STER/ehsan/IS_tracks_for_Peter/'
# path_hist_repo = mesa_dir + 'LOGS/'
# path_repo      = path_hist_repo

# mesa_dir       = '/STER/ehsan/models/KIC-7760680/'
# path_hist_repo = '/STER/mesa-gyre/KIC-7760680-HiZ/'
# path_repo      = '/STER/mesa-gyre/KIC-7760680-aov/'

hist_srch_string = 'M*/hist/*.hist'

path_errs      = mesa_dir + 'JobArray-errs/'
path_logs      = mesa_dir + 'JobArray-logs/'
path_scratch   = '/scratch/ehsan/equil/'

paths_to_check = [path_logs, path_errs, path_repo, path_repo+'hist/', path_repo+'profiles/',
                  path_repo+'gyre_in/', path_repo+'gyre_out/']
for a_path in paths_to_check[0:3]:
  if not os.path.exists(a_path): os.makedirs(a_path)

#=================================================================================================================

mass_from = 02.50
mass_to   = 25.00
if False:
  # Linear meshing
  mass_step = 01.00
  n_mass    = int(np.round((mass_to-mass_from)/mass_step, decimals=4) + 1)
  mass_vals = np.linspace(mass_from, mass_to, num=n_mass, endpoint=True)
else:
  # logarithmic meshing
  n_mass    = 101
  log_mass  = np.linspace(np.log10(mass_from), np.log10(mass_to), num=n_mass, endpoint=True)
  d_log_m   = log_mass[1] - log_mass[0]
  mass_vals = np.power(10, log_mass)

  # for equidistant meshing extension of IS_Fex1.75 to higher masses
  new_vals  = []
  new_m     = np.log10(mass_to)
  new_m_to  = np.log10(50)
  while new_m <= new_m_to:
    new_m += d_log_m
    new_vals.append(new_m)
  new_vals  = np.array(new_vals)
  mass_vals = np.power(10, new_vals)


eta_from  = 0.00
eta_to    = 0.00
eta_step  = 0.01
n_eta     = int(np.round((eta_to-eta_from)/eta_step, decimals=4) + 1)
eta_vals  = np.linspace(eta_from, eta_to, num=n_eta, endpoint=True)

ov_from   = 0.02
ov_to     = 0.02
ov_step   = 0.01
n_ov      = int(np.round((ov_to-ov_from)/ov_step, decimals=4) + 1)
ov_vals   = np.linspace(ov_from, ov_to, num=n_ov, endpoint=True)

sc_from   = 0.01
sc_to     = 0.01
sc_vals   = np.array([0.01])
n_sc      = len(sc_vals)

Z_from    = 0.020 
Z_to      = 0.020
Z_step    = 0.001
n_Z       = int(np.round((Z_to-Z_from)/Z_step, decimals=4) + 1)
Z_vals    = np.linspace(Z_from, Z_to, num=n_Z, endpoint=True)

logD_from = 1.00
logD_to   = 1.00
logD_step = 0.25
n_logD    = int(np.round((logD_to-logD_from)/logD_step, decimals=4) + 1)
logD_vals = np.linspace(logD_from, logD_to, num=n_logD, endpoint=True)

Xini_from = 0.71
Xini_to   = 0.71
Xini_step = 0.01
n_Xini    = int((Xini_to-Xini_from)/Xini_step + 1)
Xini_vals = np.linspace(Xini_from, Xini_to, num=n_Xini, endpoint=True)

n_param   = n_mass * n_eta * n_ov * n_sc * n_Z * n_Xini * n_logD
print ' - JAM will submit {0} tracks to the queue.'.format(n_param)
print '   mass: ', mass_vals
print '   eta:  ', eta_vals
print '   ov:   ', ov_vals
print '   Z:    ', Z_vals
print '   Xi:   ', Xini_vals
print '   logD: ', logD_vals
print

n_srun = 1 # 25

#=================================================================================================================
#=================================================================================================================
def identify_missing_tracks_by_hist_name(list_paths, srch, ascii_out=None):
  """
  Go look into the directories in list_paths, and glob all available hist files there with the passed srch search string.
  Split the names of the available tracks into the parameter pieces, and store them in a set.
  Then, loop over the desired parameter ranege (set on top of this module), and prepare the parameters of
  the missing tracks. They will be later submitted.
  @param path: full path to a directory were history files are stored. If the directory content do not match
          the srch searching criteria, then it is conclded that there are no tracks there, and all tracks will be
          computed. So, make sure srch is correctly passed
  @type list_paths: list of strings
  @param srch: search string to get the list of available history files by glob
  @type srch: string
  @return:
  """
  hist_files = []
  for i, path in enumerate(list_paths):
    if not os.path.exists(path):
      raise SystemExit, 'Error: identify_missing_tracks_by_hist_name: {0} is not a valid directory'.format(path)
    if path[-1] != '/': list_paths[i] += '/'
    hist_files += glob.glob(path + srch)

  # hist_files = glob.glob(path + srch)
  n_hist = len(hist_files)
  if n_hist == 0:
    print ' - identify_missing_tracks_by_hist_name: Found No hist files!'
    return None
  else:
    print ' - identify_missing_tracks_by_hist_name: Found {0} hist files'.format(n_hist)

  set_avail = set()
  for i_hist, name in enumerate(hist_files):
    ind_slash = name.rfind('/') + 1
    ind_pt = name.rfind('.')
    name = name[ind_slash : ind_pt]
    name = name.split('-')
    tup = tuple(name)
    set_avail.add(tup)

  todo = []
  # SLURM compatible header:
  # list_string = []
  # PBS/Torque compatible header for VSC:
  list_string = ['m_ini,eta_ini,ov_ini,z_ini,Xini,sc_ini,logD_ini \n']
  for i_Xini, Xini in enumerate(Xini_vals):
    for i_mass, mass in enumerate(mass_vals):
      for i_eta, eta in enumerate(eta_vals):
        for i_ov, ov in enumerate(ov_vals):
          for i_sc, sc in enumerate(sc_vals):
            for i_Z, Z in enumerate(Z_vals):
              for i_D, logD in enumerate(logD_vals):
                mass_str = 'M{0:05.2f}'.format(mass)
                eta_str    = 'eta{0:04.2f}'.format(eta)
                ov_str     = 'ov{0:05.3f}'.format(ov)
                sc_str     = 'sc{0:04.2f}'.format(sc)
                Z_str      = 'Z{0:05.3f}'.format(Z)
                Xini_str  = 'Xi{0:05.3f}'.format(Xini)
                logD_str = 'logD{0:05.2f}'.format(logD)

                # SLURM compatible format:
                tup         = (mass_str, ov_str, Z_str, logD_str)
                string     = '{0:05.2f} {1:5.3f} {2:05.3f} {3:05.3f} {4:05.2f} \n'.format(
                                    mass, ov, Xini, Z, logD)

                # PBS/Torque Compatible format for VSC:
                # tup         = (mass_str, eta_str, ov_str, sc_str, Z_str, Xini_str, logD_str)
                # string     = '{0:05.2f},{1:4.2f},{2:5.3f},{3:05.3f},{4:05.3f},{5:4.2f},{6:05.2f} \n'.format(
                #                     mass, eta, ov, Z, Xini, sc, logD)

                if tup not in set_avail:
                  todo.append('{0:05.2f} {1:5.3f} {2:05.3f} {3:05.3f} {4:05.2f} \n'.format(
                              mass, ov, Xini, Z, logD))
                  list_string.append(string)


  n_todo = len(todo)
  n_all = 1. * len(Xini_vals) * len(mass_vals) * len(eta_vals) * len(ov_vals) * len(sc_vals) * len(Z_vals) * len(logD_vals)
  print ' - identify_missing_tracks_by_hist_name: Do a subset of {0} jobs from {1} possible ones!'.format(n_todo, int(n_all))

  if ascii_out:
    with open(ascii_out, 'w') as w: w.writelines(list_string)
    print ' - identify_missing_tracks_by_hist_name: stored {0}'.format(ascii_out)

  return todo

#=================================================================================================================
def gen_param_file():
  """
  This function iterates over the whole parameters defined on the header of this file, and stores one set per each
  line for the job array.
  @return: dictionary with the full information about the file and the parameters of the Jobs being submitted.
     It has the following keys:
     -
  @rtype: dictionary
  """
  dic = {}
  dic['mass_vals'] = mass_vals
  dic['eta_vals']  = eta_vals
  dic['ov_vals']   = ov_vals
  dic['sc_vals']   = sc_vals
  dic['Z_vals']    = Z_vals
  dic['Xini_vals'] = Xini_vals
  dic['logD_vals'] = logD_vals
  dic['n_param']   = n_param

  lines = []
  for i_Xini, Xini in enumerate(Xini_vals):
    for i_mass, mass in enumerate(mass_vals):
      for i_eta, eta in enumerate(eta_vals):
        for i_ov, ov in enumerate(ov_vals):
          for i_sc, sc in enumerate(sc_vals):
            for i_Z, Z in enumerate(Z_vals):
              for i_D, logD in enumerate(logD_vals):
                lines.append('{0:05.2f} {1:5.3f} {2:04.2f} {3:05.3f} {4:05.2f} \n'.format(
                              mass, ov, Xini, Z, logD))

  return lines

#=================================================================================================================
def submit_jobs(job_subset=[]):
  """
  Sumbit the Jobs to the SLURM Queue
  @param job_subset: if passed, then only a subset of jobs will be submitted, instead of looping entirely over
          the whole gen_param_file(). This is good to finish incomplete parts of a grid
  @type job_subset: list of string, where each line gives the parameters of the run.
  @return: status of the submitted jobs
  @rtype: integer/boolean
  """
  if job_subset:
    lines_param = job_subset
  else:
    lines_param = gen_param_file()
  n_param = len(lines_param)
  n_sbatch = n_param / n_srun + 1

  print ' - submit_jobs: will submit {0} jobs, each up to {1} srun commands'.format(n_sbatch, n_srun)
  print '   Total number of jobs is {0}'.format(n_param)

  authenticate = jac.check_password()
  if not authenticate:
    logging.error(' - submit_jobs: submit_jobs: Wrong Password!')
    return authenticate

  m_sbatch = int(raw_input( ' - submit_jobs: To avoid overwriting, insert and offset integer: ' ))

  for i_sbatch in range(n_sbatch):
    ind_from = i_sbatch * n_srun
    if i_sbatch == n_sbatch - 1:
      ind_end   = n_param - 1
    else:
      ind_end = ind_from + n_srun

    this_line_param = lines_param[ind_from : ind_end]

    sbatch_command = prepare_batch(i_sbatch + m_sbatch, this_line_param)

    process    = subprocess.Popen(sbatch_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err   = process.communicate()
    returncode = process.returncode

    if returncode is  None or returncode == 0:
      print ' - submit_jobs: {0} SLURM says: {1}'.format(sbatch_command, out)
      logging.info('submit_1000_srun_mesa: submit_jobs: {0} executed'.format(sbatch_command))
    else:
      print ' - submit_jobs failed. returncode={0}, Error={1}'.format(returncode, err)
      logging.error('submit_1000_srun_mesa: submit_jobs: Error: {0}'.format(err))
      raise SystemExit

  return returncode

#=================================================================================================================
def prepare_batch(ind, list_param):
  """
  Prepare the batch file to be stored in executed from the local directory
  @param ind: The index counter for which this function is called. It is also used to generate sbatch files to write to disc for
           execution. Make sure it does not overwrite existing files, so that it does complicate stuff ...
  @type ind: integer
  @param list_param: list of strings, where each srting contains the input parameter for each MESA job. E.g.
           '05.20 010 0.024 0.014 69 0.01 2.50'
           The meaning of the values are the following:
           - mass
           - rot
           - overshooting
           - metallicity
           - initial hydrogen mass fraction in percentage
           - semi-convection
           - log Dmix
  @type list_param: list of strings
  @return: sbatch command to be executed
  @rtype: string
  """
  # dic_Q  = jac.get_Q_report()

  n_jobs = len(list_param)
  job_name = '{0}-{1:04d}'.format(batch_filename, ind)
  batch_file = job_name + '.sh'
  command = '{0} {1}'.format('sbatch', batch_file)

  if queue == 'high':   time = '06:00:00'
  if queue == 'normal': time = '24:00:00'
  if queue == 'low':    time = '24:00:00'
  if queue == 'long':   time = '14-00:00:00'

  lines = []
  lines.append('#!/bin/bash \n')
  lines.append('\n')
  lines.append('#SBATCH --account=ivsusers \n')
  lines.append('#SBATCH --job-name={0} \n'.format(job_name))
  # lines.append('#SBATCH --array=1-{0} \n'.format(n_param))  xxx
  #lines.append('#SBATCH --begin=now+1 \n')
  lines.append('#SBATCH --output=/dev/null \n')
  lines.append('#SBATCH --error=./sbatch.err \n')
  lines.append('#SBATCH --time={0} \n'.format(time))
  #lines.append('#SBATCH --exclusive \n')
  lines.append('#SBATCH --partition={0} \n'.format(queue))
  lines.append('#SBATCH --ntasks=1 \n')
  lines.append('#SBATCH --cpus-per-task=1 \n')
  lines.append('#SBATCH --mem-per-cpu=3000 \n')
  #lines.append('#SBATCH --nodelist=pleiad02')
  lines.append('\n')
  lines.append('hostname \n')
  lines.append('date \n')
  lines.append('\n')
  lines.append('export OMP_NUM_THREADS=1 \n')
  # lines.append('echo "My SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID \n')
  lines.append('\n')


  for i_job, job in enumerate(list_param):
    list_srun_string = get_a_srun_list(job)
    lines += list_srun_string

  with open(batch_file, 'w') as w: w.writelines(lines)
  logging.info('prepare_batch: Master Batch File {0} stored'.format(batch_file))

  return command

#=================================================================================================================
def get_a_srun_list(str_param):
  """
  This function prepares the body of the srun command and additional operations per one MESA job.
  The input string, str_param, is broken into pieces based on the space between them, and each piece serves as
  one input parameter, like mass, metallicity etc. For the full list see the documentation of prepare_batch()

  """
  line = str_param.rstrip().lstrip().split()
  # mass_str = line[0]
  # eta_str = line[1]
  # ov_str = line[2]
  # Z_str = line[3]
  # X_str = line[4]
  # sc_str = line[5]
  # D_str = line[6]

  mass_str = line[0]
  ov_str = line[1]
  X_str = line[2]
  Z_str = line[3]
  D_str = line[4]

  # tag = 'M{0}-eta{1}-ov{2}-sc{3}-Z{4}-Xi{5}-logD{6}'.format(mass_str, eta_str, ov_str, sc_str, Z_str, X_str, D_str)
  tag = 'M{0}-ov{1}-Xi{2}-Z{3}-logD{4}'.format(mass_str, ov_str, X_str, Z_str, D_str)


  lines = []
  lines.append('# Preparation: \n')
  lines.append('tag={0} \n'.format(tag))
  lines.append('echo tag  is $tag \n')
  # lines.append('\n')
  # lines.append('mkdir -p {0}M{1} \n'.format(path_repo, mass_str))
  # lines.append('mkdir -p {0}M{1}/hist \n'.format(path_repo, mass_str))
  # lines.append('mkdir -p {0}M{1}/profiles \n'.format(path_repo, mass_str))
  # lines.append('mkdir -p {0}M{1}/gyre_in \n'.format(path_repo, mass_str))
  # lines.append('mkdir -p {0}M{1}/gyre_out \n'.format(path_repo, mass_str))
  lines.append('\n')
  lines.append(r'log_file={0}$tag.out'.format(path_logs) + '\n')
  lines.append(r'err_file={0}$tag.err'.format(path_errs)  + '\n')
  lines.append(r'progress_file={0}JobArray-MESA.progress'.format(path_repo)  + '\n')
  lines.append('\n')
  lines.append('# Execution: \n')
  lines.append('cd {0}; echo path is $(pwd)'.format(mesa_dir) + '\n')
  #lines.append('srun ./rn <<< $( ParamFromFile ) \n')
  # lines.append('srun ./rn <<< $(echo {0} {1} {2} {3} {4} {5} {6}) 1>>$log_file  2>>$err_file'.format(mass_str, eta_str, ov_str, Z_str, X_str, sc_str, D_str) + '\n')
  lines.append('srun ./rn <<< $(echo {0} {1} {2} {3} {4}) 1>>$log_file  2>>$err_file'.format(mass_str, ov_str, X_str, Z_str, D_str) + '\n')
  lines.append('\n')
  lines.append('# Post-Processing: \n')
  lines.append('sleep 1 \n')
  lines.append('python /STER/ehsan/py-codes/mesa_gyre/mesa_gyre/ascii2h5.py {0}M{1}/hist/$tag.hist 1>>$log_file  2>>$err_file'.format(path_scratch, mass_str) + '\n')
  # lines.append('python /STER/ehsan/py-codes/mesa_gyre/mesa_gyre/ascii2h5.py {0}M{1}/profiles/{2}*.prof 1>>$log_file  2>>$err_file \n'.format(path_scratch, mass_str, tag))
  lines.append('mv {0}M{1}/hist/{2}.h5 {3}M{4}/hist/ 1>>$log_file  2>>$err_file'.format(path_scratch, mass_str, tag, path_repo, mass_str) + '\n')
  # lines.append('mv {0}M{1}/profiles/{2}*.h5 {3}M{4}/profiles/ 1>>$log_file  2>>$err_file \n'.format(path_scratch, mass_str, tag, path_repo, mass_str))
  lines.append('mv {0}M{1}/gyre_in/{2}*.gyre {3}M{4}/gyre_in/ 1>>$log_file  2>>$err_file'.format(path_scratch, mass_str, tag, path_repo, mass_str) + '\n')
  lines.append('\n')
  lines.append('echo Remove input if no errors occured \n')
  lines.append('if [ -e $err_file ] \n')
  lines.append('then \n')
  lines.append('     if ! [ -s $err_file ] \n')
  lines.append('     then \n')
  lines.append('          rm $err_file $log_file \n')
  lines.append('     fi \n')
  lines.append('fi \n')
  lines.append('\n')
  lines.append('hostname 1>>$log_file \n')
  lines.append('date 1>>$log_file \n')
  # lines.append('echo $(hostname) $(date) {0} {1} {2} {3} {4} {5} {6}  >>$progress_file \n'.format( mass_str, eta_str, ov_str, Z_str, X_str, sc_str, D_str ))
  lines.append('echo $(hostname) $(date) {0} {1} {2} {3} {4}  >>$progress_file \n'.format( mass_str, ov_str, X_str, Z_str, D_str ))
  lines.append('echo Done \n')

  return lines

#=================================================================================================================
def main():
  """
  Submit the jobs
  """
  logging.info('main: Start')

  if True:
    status = submit_jobs()

  if False:
    print ' - Submit a subset of jobs'

    path_aov  = '/STER/mesa-gyre/KIC-7760680-aov/'
    list_todo = identify_missing_tracks_by_hist_name([path_aov], 
                                        hist_srch_string, ascii_out='aov-Grid.input')
    
    # path_coarse = '/STER/mesa-gyre/KIC-7760680-Coarse/'
    # path_HiZ    = '/STER/mesa-gyre/KIC-7760680-HiZ/'
    # path_Fine   = '/STER/mesa-gyre/KIC-7760680-Fine/'
    # list_todo   = identify_missing_tracks_by_hist_name([path_coarse, path_HiZ, path_Fine], 
    #                                     hist_srch_string, ascii_out='Fine-Grid.input')

    status = submit_jobs(list_todo)

  logging.info('main: End')
  
  return None

#=================================================================================================================
if __name__ == '__main__':
  status = main()
  sys.exit(status)
#=================================================================================================================

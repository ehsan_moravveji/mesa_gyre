#!/usr/bin/python

import os, glob
from slurm_commons import *
import numpy as np

platform = VSCentrum

path_data = '/data/leuven/307/vsc30745/'
#path_data = '/Users/ehsan/programs/'
path_work = path_data + 'models/gen_op_mono_zams_repository/'
path_output = path_data + 'Output/'
path_csv = path_work + 'Input.csv'

if False:
  mass_from = 1.50
  mass_to   = 3.00
  mass_step = 0.05
  n_mass    = int((mass_to-mass_from)/mass_step + 1)
  masses    = np.linspace(mass_from, mass_to, num=n_mass, endpoint=True)
else:
  mass_from = 2.5
  mass_to   = 25.0
  n_mass    = 101
  masses    = np.linspace(np.log10(mass_from), np.log10(mass_to), num=n_mass, endpoint=True)
  masses    = np.power(10, masses)



z_from    = 0.014
z_to      = 0.014
z_step    = 0.001
n_z       = int((z_to-z_from)/z_step + 1)

eta_from  = 0.00
eta_to    = 0.00
eta_step  = 0.03
n_eta     = int((eta_to-eta_from)/eta_step + 1)

line = []; lines = []
counter = 0
nl = ' \n'
#writer = csv.writer(open(path_csv, 'w'))
# lines.append('M_ini,Z_ini,eta_ini' + nl)
lines.append('M_ini,Z_ini' + nl)
#writer.writerow(line)

mass = mass_from
z    = z_from
eta  = eta_from

for i_eta in range(n_eta):
  eta = eta_from + i_eta * eta_step
  for i_z in range(n_z):
    z = z_from + i_z * z_step
    for i_mass in range(n_mass):
      # mass = mass_from + i_mass * mass_step
      # line = '%4.2f,%5.3f,%4.2f %s' % (mass, z, eta, nl)
      mass = masses[i_mass]
      line = '{0:07.3f},{1:06.4f} \n'.format(mass, z)
      #writer.writerow(line)
      lines.append(line)
      counter += 1
  
w = open(path_csv, 'w')
w.writelines(lines)  
w.close()
  
print 'Total lines written in %s = %s' % (path_csv, counter) 
  
#return None


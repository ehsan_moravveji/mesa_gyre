#!/usr/bin/python

import os, glob
# from slurm_commons import *
from random import sample
import numpy as np

path_data = '/data/leuven/307/vsc30745/'
path_work = path_data + 'models/asamba-mesa/'
path_output = path_data + 'Output/'
path_csv = path_work + 'coarse-test.csv'

do_shuffle       = True
logarithmic_mass = True

if logarithmic_mass:
  mass_from = 1.40
  mass_to   = 35.0
  n_mass    = 5
  mass_vals = np.linspace(np.log10(mass_from), np.log10(mass_to), num=n_mass, endpoint=True)
  mass_vals = np.power(10, mass_vals)
else:
  mass_from = 2.50
  mass_to   = 25.00
  mass_step = 0.25
  n_mass    = int((mass_to-mass_from)/mass_step + 1)
  masses    = np.linspace(mass_from, mass_to, num=n_mass, endpoint=True)

eta_from  = 0.00
eta_to    = 0.00
eta_step  = 0.01
n_eta     = int(np.round((eta_to-eta_from)/eta_step, decimals=4) + 1)
eta_vals  = np.linspace(eta_from, eta_to, num=n_eta, endpoint=True)

ov_from   = 0.020
ov_to     = 0.040
ov_step   = 0.010
n_ov      = int(np.round((ov_to-ov_from)/ov_step, decimals=4) + 1)
ov_vals   = np.linspace(ov_from, ov_to, num=n_ov, endpoint=True)

sc_from   = 0.01
sc_to     = 0.01
sc_vals   = np.array([0.01])
n_sc      = len(sc_vals)

Z_from    = 0.010 
Z_to      = 0.018
Z_step    = 0.008
n_Z       = int(np.round((Z_to-Z_from)/Z_step, decimals=4) + 1)
Z_vals    = np.linspace(Z_from, Z_to, num=n_Z, endpoint=True)

logD_from = 0.00
logD_to   = 4.00
logD_step = 2.00
n_logD    = int(np.round((logD_to-logD_from)/logD_step, decimals=4) + 1)
logD_vals = np.linspace(logD_from, logD_to, num=n_logD, endpoint=True)

Xini_from = 0.71
Xini_to   = 0.71
Xini_step = 1
n_Xini    = int((Xini_to-Xini_from)/Xini_step + 1)
Xini_vals = np.linspace(Xini_from, Xini_to, num=n_Xini, endpoint=True)

n_param   = n_mass * n_eta * n_ov * n_sc * n_Z * n_Xini * n_logD
print ' - JAM will submit {0} tracks to the queue.'.format(n_param)
print '   mass: ', mass_vals
print '   eta:  ', eta_vals
print '   ov:   ', ov_vals
print '   Z:    ', Z_vals
print '   Xi:   ', Xini_vals
print '   logD: ', logD_vals
print


# list_string = ['m_ini,eta_ini,ov_ini,z_ini,Xini,sc_ini,logD_ini \n']
list_string = ['m_ini,ov_ini,z_ini,logD_ini \n']
for i_Xini, Xini in enumerate(Xini_vals):
  for i_mass, mass in enumerate(mass_vals):
    for i_ov, ov in enumerate(ov_vals):
      for i_Z, Z in enumerate(Z_vals):
        for i_D, logD in enumerate(logD_vals):
          mass_str = 'M{0:06.3f}'.format(mass)
          # eta_str  = 'eta{0:04.2f}'.format(eta)
          ov_str   = 'ov{0:05.3f}'.format(ov)
          # sc_str   = 'sc{0:04.2f}'.format(sc)
          Z_str    = 'Z{0:05.3f}'.format(Z)
          Xini_str = 'Xi{0:04.2f}'.format(Xini)
          logD_str = 'logD{0:05.2f}'.format(logD)
          # tup      = (mass_str, eta_str, ov_str, sc_str, Z_str, Xini_str, logD_str)
          tup      = (mass_str, ov_str, Xini_str, Z_str, logD_str)

          # PBS/Torque Compatible format for VSC:
          # string     = '{0:06.3f},{1:4.2f},{2:5.3f},{3:05.3f},{4:04.2f},{5:4.2f},{6:05.2f} \n'.format(
          #                     mass, eta, ov, Z, Xini, sc, logD)
          string     = '{0:06.3f},{1:5.3f},{2:05.3f},{3:05.2f} \n'.format(mass, ov, Z, logD)

          list_string.append(string)
if do_shuffle:
  nrows = len(list_string) 
  ind   = sample(range(1, nrows), nrows-1)
  hdr   = [list_string[0]]
  rows  = [list_string[i] for i in ind]
  list_string = hdr + rows

with open(path_csv, 'w') as w: w.writelines(list_string)
  
print 'Total jobs written in %s = %s' % (path_csv, len(list_string)-1) 




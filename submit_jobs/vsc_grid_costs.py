
import sys, os, glob
import numpy as np

#=================================================================================================================
def price(n_nodes, walltime):
	"""
	Calculations are based on the Formula in
	https://vscentrum.be/neutral/documentation/cluster-doc/running-jobs/credit-system-basics
	@param n_nodes: number of nodes a user requests
	@type n_nodes: integer
	@param walltime: The requested walltime in hr. We internally convert to seconds for the formula
	@type walltime: float
	@return: the credits required to run the spcific job
	@rtype: float
	"""
	walltime  = walltime * 36e2
	ivybridge = 4.77  # credit / hour
	startup   = 0.1

	return (0.000278 * n_nodes * walltime + startup) * ivybridge

#=================================================================================================================
mass_from = 1.40
mass_to   = 35.00
# mass_step = 0.05
# n_mass    = int((mass_to-mass_from)/mass_step + 1)
# mass_vals = np.linspace(mass_from, mass_to, num=n_mass, endpoint=True)
lg_m_from = np.log10(mass_from)
lg_m_to   = np.log10(mass_to)
n_mass    = 101
mass_step = (lg_m_to - lg_m_from) / n_mass 
mass_vals = np.linspace(lg_m_from, lg_m_to, num=n_mass, endpoint=True)

eta_from  = 0.10
eta_to    = 0.50
eta_step  = 0.05
n_eta     = int((eta_to-eta_from)/eta_step + 1)
eta_vals  = np.linspace(eta_from, eta_to, num=n_eta, endpoint=True)

ov_from   = 0.010
ov_to     = 0.040
ov_step   = 0.005
n_ov      = int((ov_to-ov_from)/ov_step+1)
ov_vals   = np.linspace(ov_from, ov_to, num=n_ov, endpoint=True)

sc_from   = 0.01
sc_to     = 0.01
sc_vals   = np.array([0.01])
n_sc      = len(sc_vals)

Z_from    = 0.010
Z_to      = 0.018
Z_step    = 0.004
n_Z       = int(np.rint((Z_to-Z_from)/Z_step + 1.))
Z_vals    = np.linspace(Z_from, Z_to, num=n_Z, endpoint=True)

logD_from = 0
logD_to   = 4 # to some costume made value, depending on initial mass
logD_step = 1
n_logD    = int((logD_to-logD_from)/logD_step + 1)
logD_vals = np.linspace(logD_from, logD_to, num=n_logD, endpoint=True)

Xini_from = 71
Xini_to   = 71
Xini_step = 1
n_Xini    = int((Xini_to-Xini_from)/Xini_step + 1)
Xini_vals = np.linspace(Xini_from, Xini_to, num=n_Xini, endpoint=True)

Xc_from   = 0.71
Xc_to     = 0.001
Xc_step   = 0.002
n_Xc      = 317.3 # averaged after testing the coarse grid
# Xc_vals   = np.linspace(Xc_from, Xc_to, num=n_Xc, endpoint=True)

# n_tracks  = 1. * n_mass * n_eta * n_ov * n_sc * n_Z * n_Xini * n_logD
n_tracks  = 1. * n_mass * n_ov * n_Z * n_logD  + 1500 # additional 1500 tracks included for F-type stars
n_models  = 1. * n_tracks * n_Xc

print '\n - Grid Specifications:'
print '   {0:16s} {1:16s} {2:16s} {3:16s} {4:16s}'.format('Parameter', 'From', 'To', 'Step', 'N')
print '   {0:16s} {1:<16.3f} {2:<16.3f} {3:<16.3f} {4:<16.3f}'.format('Mass', mass_from, mass_to, mass_step, n_mass)
print '   {0:16s} {1:<16.3f} {2:<16.3f} {3:<16.3f} {4:<16.3f}'.format('eta', eta_from, eta_to, eta_step, n_eta)
print '   {0:16s} {1:<16.3f} {2:<16.3f} {3:<16.3f} {4:<16.3f}'.format('f_ov', ov_from, ov_to, ov_step, n_ov)
print '   {0:16s} {1:<16.3f} {2:<16.3f} {3:<16.3f} {4:<16.3f}'.format('Z', Z_from, Z_to, Z_step, n_Z)
print '   {0:16s} {1:<16.3f} {2:<16.3f} {3:<16.3f} {4:<16.3f}'.format('Xini', Xini_from, Xini_to, Xini_step, n_Xini)
print '   {0:16s} {1:<16.3f} {2:<16.3f} {3:<16.3f} {4:<16.3f}'.format('logD', logD_from, logD_to, logD_step, n_logD)
print '   {0:16s} {1:<16.3f} {2:<16.3f} {3:<16.3f} {4:<16.3f}'.format('Xc', Xc_from, Xc_to, Xc_step, n_Xc)

print '\n - Number of MESA Tracks = {0}'.format(n_tracks)
print '   Number of GYRE input models = {0}'.format(n_models)

#=================================================================================================================
max_wtime_day= 7.0
overestimate = 1.0

cpu_per_node = 28.
n_nodes      = 1.
n_cpu        = n_nodes * cpu_per_node - 1.0  # one reserved as master
T_per_track  = 12 * 60. * overestimate   # min; for MESA
T_per_call   = 4.34 * overestimate        # min; for GYRE
MESA_size_MB = 2.25
HDF5_size_KB = 490.

T_MESA_1_CPU = n_tracks * T_per_track
T_MESA_min   = T_MESA_1_CPU / n_cpu
T_MESA_hr    = T_MESA_min / 60.
T_MESA_day   = T_MESA_hr / 24.
MESA_walltime= T_MESA_day #/ (n_nodes * cpu_per_node)
T_MESA_node_per_day = T_MESA_1_CPU / ((cpu_per_node-1) * 60. * 24)

n_call_per_GYRE = n_eta
n_call_GYRE  = n_models * n_call_per_GYRE
T_GYRE_1_CPU = T_per_call * n_call_GYRE
T_GYRE_min   = T_GYRE_1_CPU / n_cpu
T_GYRE_hr    = T_GYRE_min / 60.
T_GYRE_day   = T_GYRE_hr / 24.
GYRE_walltime= T_GYRE_day #/ (n_nodes * cpu_per_node)
T_GYRE_node_per_day  = T_GYRE_1_CPU / (n_cpu * 60. * 24)

T_total_node_per_day = T_MESA_node_per_day + T_GYRE_node_per_day

Grid_size_MB = n_models * MESA_size_MB
Grid_size_TB = Grid_size_MB / 1e6
GYRE_size_KB = n_call_GYRE * HDF5_size_KB
GYRE_size_MB = GYRE_size_KB / 1e3
GYRE_size_GB = GYRE_size_MB / 1e3
GYRE_size_TB = GYRE_size_GB / 1e3

n_wsub_jobs  = T_GYRE_node_per_day / n_nodes / max_wtime_day

MESA_credits = price(n_nodes, T_MESA_hr)
GYRE_credits = price(n_nodes, T_GYRE_hr)

if True:
	print '\n - Single-CPU estimate of the runtimes'
	print '   Computing MESA tracks take {0} [min] = {1} [day]'.format(T_MESA_1_CPU, T_MESA_1_CPU/(24.*60))
	print '   Computing GYRE modes take {0} [min] = {1} [day]'.format(T_GYRE_1_CPU, T_GYRE_1_CPU/(24.*60))

if True: 
	print '\n - PBS Specifications'
	print '   For {0} CPUs per node; if I request {1} nodes, '.format(cpu_per_node, n_nodes)
	print '   The number of available CPUs will be {0}'.format(n_cpu)
	print '   One MESA job takes {0:.4f} day, and one GYRE job takes {1:.4f} day, on one CPU'.format(T_per_track/(60.*24), T_per_call/(60.*24))
	print '   One MESA job takes {0:.6f} node-day on one CPU'.format(T_per_track/(60.*24*cpu_per_node))
	print '   One GYRE job takes {0:.6f} node-day on one CPU'.format(T_per_call/(60.*24*cpu_per_node))
	print '   If each MESA job takes {0} min, and each GYRE file takes {1} MB, '.format(T_per_track, MESA_size_MB)
	print '   If each GYRE calculation takes {0} min, then:\n'.format(T_per_call)

	print '   Required Time for MESA = {0:.2f} hr, or {1:.2f} day per node'.format(T_MESA_hr, MESA_walltime)
	print '   Required Time for GYRE = {0:.2f} hr, or {1:.2f} day per node'.format(T_GYRE_hr, GYRE_walltime)

if True:
	print '\n - Disk spare Specifications'
	print '   Required disk space for MESA = {0} [TB]'.format(Grid_size_TB)
	print '   Required disk space for GYRE = {0} [TB]'.format(GYRE_size_TB)

if True:
	print '\n - PBS Specifications in Node-per-day unit'
	print '   Required MESA time:  {0:.1f} node per day'.format(T_MESA_node_per_day)
	print '   Required GYRE time:  {0:.1f} node per day'.format(T_GYRE_node_per_day)
	print '   Required Total time: {0:.1f} node per day'.format(T_total_node_per_day)
	print '   Number of GYRE calls: {0}'.format(n_call_GYRE)

if True:
	print '\n - PBS number of jobs to submit'
	print '   Num. MESA jobs: 1'
	print '   Num. GYRE jobs: {0}'.format(int(np.ceil(n_wsub_jobs)))

#=================================================================================================================
if False:
	print '\n - Calculatin Costs'
	print '   MESA will need ~{0} credits'.format(MESA_credits)
	print '   GYRE will need ~{0} credits'.format(GYRE_credits)
	print
	print '   MESA will cost ~{0} EUR on MC or {1} EUR on FWO'.format(MESA_credits*120./1e3, MESA_credits*7./1e3)
	print '   GYRE will cost ~{0} EUR on MC or {1} EUR on FWO'.format(GYRE_credits*120./1e3, GYRE_credits*7./1e3)
	print
#=================================================================================================================
print




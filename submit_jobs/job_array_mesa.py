
"""
Submit an array of MESA jobs with SLURM job array.
"""

import os, glob, sys
import logging
import subprocess
import numpy as np

import job_array_commons as jac


#=================================================================================================================
# Logging settings
logging.basicConfig(filename='Log-JobArray-MESA.log', format='%(levelname)s: %(asctime)s: %(message)s', level=logging.DEBUG)

queue          = 'normal'
batch_filename = 'JobArray-MESA-3.sh'
param_filename = 'JobArray-MESA-3.input'

# mesa_dir       = '/STER/ehsan/models/KIC-10526294/'
# path_repo      = '/STER/mesa-gyre/KIC-10526294-logD/'
#mesa_dir       = '/STER/ehsan/models/HD50230/'
#path_repo      = '/STER/mesa-gyre/HD50230-Xini/'
mesa_dir       = '/home/ehsan/a/programs/models/HD50230-new-logD/'
path_repo      = '/STER/mesa-gyre/HD50230-new-logD/'

path_errs      = mesa_dir + 'JobArray-errs/'
path_logs      = mesa_dir + 'JobArray-logs/'
path_scratch   = '/scratch/ehsan/equil/'

paths_to_check = [path_logs, path_errs, path_repo, path_repo+'hist/', path_repo+'profiles/',
                  path_repo+'gyre_in/', path_repo+'gyre_out/']
for a_path in paths_to_check[0:3]:
  if not os.path.exists(a_path): os.makedirs(a_path)

#=================================================================================================================
mass_from = 07.70
mass_to   = 07.80
mass_step = 00.10
n_mass    = int(np.round((mass_to-mass_from)/mass_step, decimals=4) + 1)
mass_vals = np.linspace(mass_from, mass_to, num=n_mass, endpoint=True)

eta_from  = 0.00
eta_to    = 0.00
eta_step  = 0.01
n_eta     = int(np.round((eta_to-eta_from)/eta_step, decimals=4) + 1)
eta_vals  = np.linspace(eta_from, eta_to, num=n_eta, endpoint=True)

ov_from   = 0.012
ov_to     = 0.024
ov_step   = 0.003
n_ov      = int(np.round((ov_to-ov_from)/ov_step, decimals=4) + 1)
ov_vals   = np.linspace(ov_from, ov_to, num=n_ov, endpoint=True)

sc_from   = 0.01
sc_to     = 0.01
sc_vals   = np.array([0.01])
n_sc      = len(sc_vals)

Z_from    = 0.010
Z_to      = 0.020
Z_step    = 0.002
n_Z       = int(np.round((Z_to-Z_from)/Z_step, decimals=4) + 1)
Z_vals    = np.linspace(Z_from, Z_to, num=n_Z, endpoint=True)

logD_from = 2.50
logD_to   = 5.00
logD_step = 0.50
n_logD    = int(np.round((logD_to-logD_from)/logD_step, decimals=4) + 1)
logD_vals = np.linspace(logD_from, logD_to, num=n_logD, endpoint=True)

Xini_from = 71
Xini_to   = 71
Xini_step = 1
n_Xini    = int((Xini_to-Xini_from)/Xini_step + 1)
Xini_vals = np.linspace(Xini_from, Xini_to, num=n_Xini, endpoint=True)

n_param   = n_mass * n_eta * n_ov * n_sc * n_Z * n_Xini * n_logD
print ' - JAM will submit {0} tracks to the queue.'.format(n_param)
print '   mass: ', mass_vals
print '   eta:  ', eta_vals
print '   ov:   ', ov_vals
print '   Z:    ', Z_vals
print '   logD: ', logD_vals

param_name = 'M%05.2f-%05.2f-eta%4.2f-%4.2f-ov%5.3f-%5.3f-sc%8.6f-%8.6f-Z%5.3f-%5.3f.in' % (mass_from, mass_to,
                                                                                      eta_from, eta_to,
                                                                                      ov_from, ov_to,
                                                                                      sc_from, sc_to,
                                                                                      Z_from, Z_to)

#=================================================================================================================
def gen_param_file(a_file):
  """
  This function iterates over the whole parameters defined on the header of this file, and stores one set per each
  line for the job array.
  @param a_file: Full path to the file containing the list of parameters to run
  @type a_file: string
  @return: dictionary with the full information about the file and the parameters of the Jobs being submitted.
     It has the following keys:
     -
  @rtype: dictionary
  """
  dic = {}
  dic['param_filename'] = a_file
  dic['mass_vals'] = mass_vals
  dic['eta_vals']  = eta_vals
  dic['ov_vals']   = ov_vals
  dic['sc_vals']   = sc_vals
  dic['Z_vals']    = Z_vals
  dic['Xini_vals'] = Xini_vals
  dic['logD_vals'] = logD_vals
  dic['n_param']   = n_param

  lines = []
  for i_Xini, Xini in enumerate(Xini_vals):
    for i_mass, mass in enumerate(mass_vals):
      for i_eta, eta in enumerate(eta_vals):
        for i_ov, ov in enumerate(ov_vals):
          for i_sc, sc in enumerate(sc_vals):
            for i_Z, Z in enumerate(Z_vals):
              for i_D, logD in enumerate(logD_vals):
                lines.append('{0:05.2f} {1:4.2f} {2:5.3f} {3:05.3f} {4:2d} {5:4.2f} {6:05.2f} \n'.format(
                              mass, eta, ov, Z, int(Xini), sc, logD))
                # lines.append('{0:05.2f} {1:4.2f} {2:5.3f} {3:05.3f} {4:4.2f} \n'.format(
                #               mass, eta, ov, Z, sc))   # for interactive grid which uses only 5 input, not 7

  with open(a_file, 'w') as w: w.writelines(lines)
  logging.info('gen_param_file: {0} created'.format(a_file))

  return dic

#=================================================================================================================
def submit_jobs():
  """
  Sumbit the Jobs to the SLURM Queue
  @rtype: status of the submitted jobs
  @rtype: integer/boolean
  """
  prepare_batch()
  logging.info('submit_jobs: Master Batch File: {0} created'.format(batch_filename))

  authenticate = jac.check_password()
  if not authenticate:
    logging.error('JAM: submit_jobs: Wrong Password!')
    return authenticate

  #returncode = subprocess.call('sbatch {0}'.format(batch_filename), shell=True)
  #logging.info('submit_jobs: The MESA JobArray Submitted!')
  #return returncode

  process = subprocess.Popen('sbatch {0}'.format(batch_filename), stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE, shell=True)
  out, err = process.communicate()
  returncode = process.returncode
  if returncode is None or returncode == 0:
    print ' - JAM: submit_jobs: SLURM says: {0}'.format(out)
    logging.info('JAM: submit_jobs: Master Batch File: {0} submitted: {1}'.format(batch_filename, out))
  else:
    print ' - JAM: submit_jobs: failed. returncode={0}; Error:{1}'.format(returncode, err)
    logging.error('JAM: submit_jobs: Error: {0}'.format(err))
    raise SystemExit

  return returncode

#=================================================================================================================
def prepare_batch():
  """
  Prepare the batch file to be stored in executed from the local directory

  """
  dic_Q  = jac.get_Q_report()
  if dic_Q is None:
    SLURM_ARRAY_JOB_ID = 1
  else:
    max_id = dic_Q['max_id']
    SLURM_ARRAY_JOB_ID  = max_id + 1
    SLURM_ARRAY_TASK_ID = 0

  dic_param = gen_param_file(param_filename)
  n_param   = dic_param['n_param']

  lines = []
  lines.append('#!/bin/bash \n')
  lines.append('\n')
  lines.append('#SBATCH --account=ivsusers \n')
  lines.append('#SBATCH --job-name=JobArray-MESA \n')
  lines.append('#SBATCH --array=1-{0} \n'.format(n_param))
  #lines.append('#SBATCH --begin=now+1 \n')
  lines.append('#SBATCH --output=/dev/null \n')
  lines.append('#SBATCH --error=/dev/null \n')
  lines.append('#SBATCH --time=24:00:00 \n')
  #lines.append('#SBATCH --exclusive \n')
  lines.append('#SBATCH --partition={0} \n'.format(queue))
  lines.append('#SBATCH --ntasks=1 \n')
  lines.append('#SBATCH --cpus-per-task=1 \n')
  lines.append('#SBATCH --mem-per-cpu=3000 \n')
  #lines.append('#SBATCH --nodelist=pleiad02')
  lines.append('\n')
  lines.append('hostname \n')
  lines.append('date \n')
  lines.append('\n')
  lines.append('export OMP_NUM_THREADS=1 \n')
  lines.append('echo "My SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID \n')
  lines.append('\n')
  lines.append('# Preparation: \n')
  lines.append('ParamFile={0} \n'.format(param_filename))
  lines.append('ParamFromFile=$( sed -n -e "${SLURM_ARRAY_TASK_ID}p" $ParamFile ) \n')
  #$> echo $ParamFromFile | awk -F " "  '{print $1}'
  lines.append('mass_str=$(echo $ParamFromFile | awk -F " " ' + "'{print $1}'" + ') \n')
  lines.append('eta_str=$(echo $ParamFromFile | awk -F " " ' + "'{print $2}'" + ') \n')
  lines.append('ov_str=$(echo $ParamFromFile | awk -F " " ' + "'{print $3}'" + ') \n')
  lines.append('Z_str=$(echo $ParamFromFile | awk -F " " ' + "'{print $4}'" + ') \n')
  lines.append('X_str=$(echo $ParamFromFile | awk -F " " ' + "'{print $5}'" + ') \n')
  lines.append('sc_str=$(echo $ParamFromFile | awk -F " " ' + "'{print $6}'" + ') \n')
  lines.append('D_str=$(echo $ParamFromFile | awk -F " " ' + "'{print $7}'" + ') \n')
  lines.append('echo mass is $mass_str \n')
  lines.append('echo eta  is $eta_str \n')
  lines.append('echo f_ov is $ov_str \n')
  lines.append('echo Z    is $Z_str \n')
  lines.append('echo Xini is $X_str \n')
  lines.append('echo sc   is $sc_str \n')
  lines.append('echo logD is $D_str \n')
  lines.append('tag=M$mass_str-eta$eta_str-ov$ov_str-sc$sc_str-Z$Z_str-Xi$X_str-logD$D_str \n')
  lines.append('echo tag  is $tag \n')
  lines.append('\n')
  lines.append('mkdir -p {0}M$mass_str \n'.format(path_repo))
  lines.append('mkdir -p {0}M$mass_str/hist \n'.format(path_repo))
  lines.append('mkdir -p {0}M$mass_str/profiles \n'.format(path_repo))
  lines.append('mkdir -p {0}M$mass_str/gyre_in \n'.format(path_repo))
  lines.append('mkdir -p {0}M$mass_str/gyre_out \n'.format(path_repo))
  lines.append('\n')
  lines.append(r'log_file={0}$tag.out'.format(path_logs) + ' \n')
  lines.append(r'err_file={0}$tag.err'.format(path_errs) + ' \n')
  lines.append(r'progress_file={0}JobArray-MESA.progress'.format(path_repo) + ' \n')
  lines.append('\n')
  lines.append('# Execution: \n')
  lines.append('cd {0}; echo path is $(pwd) \n'.format(mesa_dir))
  #lines.append('srun ./rn <<< $( ParamFromFile ) \n')
  lines.append('srun ./rn <<< $(echo $mass_str $eta_str $ov_str $Z_str 0.$X_str $sc_str $D_str) 1>>$log_file  2>>$err_file \n')
  lines.append('\n')
  lines.append('# Post-Processing: \n')
  lines.append('sleep 5 \n')
  lines.append('python /STER/ehsan/py-codes/mesa_gyre/mesa_gyre/ascii2h5.py {0}M$mass_str/hist/$tag.hist 1>>$log_file  2>>$err_file \n'.format(path_scratch))
  lines.append('python /STER/ehsan/py-codes/mesa_gyre/mesa_gyre/ascii2h5.py {0}M$mass_str/profiles/$tag*.prof 1>>$log_file  2>>$err_file \n'.format(path_scratch))
  lines.append('mv {0}M$mass_str/hist/$tag.h5 {1}M$mass_str/hist/ 1>>$log_file  2>>$err_file \n'.format(path_scratch, path_repo))
  lines.append('mv {0}M$mass_str/profiles/$tag*.h5 {1}M$mass_str/profiles/ 1>>$log_file  2>>$err_file \n'.format(path_scratch, path_repo))
  lines.append('mv {0}M$mass_str/gyre_in/$tag*.gyre {1}M$mass_str/gyre_in/ 1>>$log_file  2>>$err_file \n'.format(path_scratch, path_repo))
  lines.append('\n')
  lines.append('echo Remove input if no errors occured \n')
  lines.append('if [ -e $err_file ] \n')
  lines.append('then \n')
  lines.append('     if ! [ -s $err_file ] \n')
  lines.append('     then \n')
  lines.append('          rm $err_file $log_file \n')
  lines.append('     fi \n')
  lines.append('fi \n')
  lines.append('\n')
  lines.append('hostname 1>>$log_file \n')
  lines.append('date 1>>$log_file \n')
  lines.append('echo $(hostname) $(date) $mass_str $eta_str $ov_str $Z_str $sc_str $D_str >>$progress_file \n'.format(path_repo))
  lines.append('echo Done \n')

  with open(batch_filename, 'w') as w: w.writelines(lines)
  logging.info('prepare_batch: Master Batch File = {0}'.format(batch_filename))

  return None

#=================================================================================================================
def main():
  """
  Submit the jobs
  """
  logging.info('main: Start')

  if True:
    status = submit_jobs()

  logging.info('main: End')
  return status

#=================================================================================================================
if __name__ == '__main__':
  status = main()
  sys.exit(status)
#=================================================================================================================

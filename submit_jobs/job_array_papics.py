
import sys, os, glob
import logging
from mesa_gyre import param_tools
import job_array_commons as jac
import job_array_gyre as jag

#=================================================================================================================
# Paths
papics_repos = '/STER/mesa-gyre/papics/'
srch_in   = 'M03.[7-8]*/gyre_in/*.gyre'
srch_out  = 'M03.[7-8]*/gyre_out/*.h5'
files_in  = glob.glob(papics_repos + srch_in)
#files_in  = param_tools.select_filenames_by_range(files_in, by='Xc', lower=0.0, higher=0.40, round_level=3)
files_out = glob.glob(papics_repos + srch_out)
n_in      = len(files_in)
n_out     = len(files_out)
print ' - Found {0} input and {1} output files'.format(n_in, n_out)

# Edit Urgent Path/Specs in jag
jag.path_base      = papics_repos
jag.nmlst_filename = papics_repos + 'papics.nmlst'
jag.search_string  = srch_in
jag.gyre_out_suffix= '-sum.h5'
jag.pause_min      = 20
jag.delay          = 3 
batch_prefix       = 'JobArray-Papics'
param_prefix       = 'JobArray-Papics'

n_submit  = 500000

#=================================================================================================================
def main():
  if True:
    set_in  = jac.get_set_filenames(files_in)
    set_out = jac.get_set_filenames(files_out)
    
    list_missed = []
    for i, f_in in enumerate(set_in):
      found = f_in in set_out
      if not found: 
        list_missed.append(f_in)
    n_miss = len(list_missed)
    print ' - Missed {0} jobs'.format(n_miss)

    if n_miss > 0:
      fp_missing_gyre_in = jac.reconstruct_gyre_in_out(list_missed, path_repos=papics_repos, 
                                                       gyre_in_out='gyre_in', suffix='.gyre')
      print ' - Check if all missing inputs truly exist on disk ...'
      for i, f_in in enumerate(fp_missing_gyre_in): 
        if not os.path.isfile(f_in):
          raise SystemExit, '{0}: {1} does not exist!'.format(i, f_in)
      
      list_repeat = []
      print ' - Check if missing inputs have no output conterparts ...'
      fp_missing_gyre_out = jac.reconstruct_gyre_in_out(list_missed, path_repos=papics_repos, 
                                                        gyre_in_out='gyre_out', suffix='-sum.h5')
      for j, f_out in enumerate(fp_missing_gyre_out):
        if os.path.isfile(f_out): list_repeat.append(f_out)
      if len(list_repeat) > 0:
        for f_out in list_repeat: print 'Repeating: {0}'.format(f_out)
        print '   {0} gyre_out already exist!'.format(len(list_repeat))
        decision = raw_input('   Enter d for delete, or s for skip: ')
        if decision.lower() == 'd':
          for a_repeat in list_repeat: os.unlink(a_repeat)
        elif decision.lower() == 's':
          pass
        else:
          raise SystemExit, 'job_array_papics: main: Wrong decision! Retry, please!'
      
      fp_gyre_in = []
      n_gyre_in  = min([n_submit, n_miss])
      for i, f_in in enumerate(fp_missing_gyre_in): 
        fp_gyre_in.append(f_in)
        if i == n_gyre_in-1: break
      status = jag.submit_jobs(fp_gyre_in)  
    else:
      print ' - job_array_papics: submit_jobs: n_miss={0}'.format(n_miss)
      status = None
      
  return status

#=================================================================================================================
if __name__ == '__main__':
  status = main()
  sys.exit(status)
#=================================================================================================================

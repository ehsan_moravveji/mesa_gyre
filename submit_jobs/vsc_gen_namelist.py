"""
This script generates a series of GYRE namelists into a specified path, and returns a CSV file listing the filenams
which is appropriate for a feed to the Worker frame on the VSC.
"""

import glob, os
import gyre_namelist as gnl
import slurm_commons as sc


#=================================================================================================================
path_dump = '/data/leuven/307/vsc30745/reports/namelists/'
if not os.path.exists(path_dump): os.makedirs(path_dump)
 
platform = sc.VSCentrum

dic_paths  = sc.set_paths(platform)

# Specify the parameters  
dic_params = sc.set_params()
dic_params['mass_start'] = 3.30
dic_params['mass_end'] = 3.35
dic_params['mass_step'] = 0.05
dic_params['ov_start'] = 0.000
dic_params['ov_end'] = 0.030
dic_params['ov_step'] = 0.003
dic_params['z_start'] = 0.010
dic_params['z_end'] = 0.020
dic_params['z_step'] = 0.001
  
# Specify the location of the Grid
dic_paths['path_mesa_gyre'] = '/scratch/leuven/307/vsc30745/KIC-10526294-hiM/'
dic_paths['path_gyre_namelists'] = path_dump

param_name = 'M%05.2f-%05.2f-eta%4.2f-%4.2f-ov%5.3f-%5.3f-sc%4.2f-Z%5.3f-%5.3f.in' % (dic_params['mass_start'], dic_params['mass_end'],
                                                                                      dic_params['eta_start'], dic_params['eta_end'],
                                                                                      dic_params['ov_start'], dic_params['ov_end'],
                                                                                      dic_params['sc'],
                                                                                      dic_params['z_start'], dic_params['z_end'])
ascii_out = sc.set_paths(platform)['path_gyre_src'] + param_name
  
# Get the list of available input files:
search_string = dic_paths['path_mesa_gyre'] + 'M03.3*/gyre_in/*.gyre'
n_trim_input  = search_string.rfind('/')
input_files = glob.glob(search_string)
input_files.sort()
print 'Glob Search String: ', search_string
n_input = len(input_files)
print '  {0} input GYRE files are detected.'.format(n_input)  
print '  Namelist files will be stored in {0}'.format(dic_paths['path_gyre_namelists'], )
print '  {0}'.format(ascii_out, )
  
# Exclude already calculated files
search_string = search_string.replace('/gyre_in/', '/gyre_out/*')
search_string = search_string.replace('.gyre', '.h5')
n_trim_output = search_string.rfind('/')
avail_files   = sorted(glob.glob(search_string))
n_avail       = len(avail_files)
print '  Num available files: {0}'.format(n_avail)
if n_avail > 0:
  print '  Should reduce the inputs ...'
  print '  New gsearch_string: ', search_string
  core_inputs   = [f[n_trim_input+1:-5] for f in input_files] # 4 is to skip e.g. 03.50 beside *
  core_outputs  = set( [f[n_trim_output+1+7:-3] for f in avail_files] ) # for 4 see above; 7 is for skipping 'ad-sum-' prefix
  incomplete    = []
  for i_in, input_file in enumerate(core_inputs):
    not_done = input_file not in core_outputs
    if not_done: 
      new_file = dic_paths['path_mesa_gyre'] + input_file[0:6] + '/gyre_in/' + input_file + '.gyre'
      incomplete.append(new_file)

  input_files = incomplete
  n_input     = len(input_files)
  print '  Num remaining jobs: {0}'.format(n_input)
  print input_files[0]

passwd = 'B9hX3'
user_pass = raw_input(' - Insert the code %s to continue: ' % (passwd, )) 
if user_pass != passwd:
  message = 'Error: submit_gyre_project: Wrong password. Authentication not granted!'
  raise SystemExit, message
  
# Load the dictionary with the desired namelist values
dic_nmlst = gnl.namelist_mesa_gyre()

# Iteratively, generate a batch script for each identified input file, and save the filename in a list
# Note: We only save the relative path to the namelist, and write the relative path to the file.
list_path_namelists = []
for i_in, input_file in enumerate(input_files):
  dic_nmlst['path_input'] = input_file
  dic_nmlst['path_output'] = gnl.gen_output_fname_from_input_fname(platform, input_file) # use it when $scratch disc is back to work
    
  namelist_name = gnl.gen_namelist_name_from_input_fname(input_file)     
  list_path_namelists.append(namelist_name + '\n') 
  namelist_full_path = dic_paths['path_gyre_namelists'] + namelist_name    
  gnl.write_gyre_namelist(namelist_full_path, dic_nmlst)

if ascii_out:
  w = open(ascii_out, 'w')
  w.write('namelist\n')
  w.writelines(list_path_namelists)
  w.close()
  
  print '   ascii_out: %s created.' % (ascii_out, )
  
print ' - Done!\n'
  
#=================================================================================================================


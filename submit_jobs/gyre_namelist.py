
import slurm_commons as scm
import os
#import subprocess
from collections import OrderedDict

nl = ' \n'  # new line 
sp = '  '   # two empty spaces
ap = "'"


#=================================================================================================================
def namelist_BSG_instability():
  """
  This function returns an ordered dictionary (i.e. OrderedDict) for the the project that revisits the instability 
  strip of OB type dwarfs and supergiants with different Iron composition.
  @return a dictionary with the following key:value pairs adapted to version >= 3.2.2. Some of the items must be set 
           here, and some of them (for instance the input and output file) must be set when the dictionary is retrieved.
  @rtype: OrderedDict
  """
  
  dic_nmlst = OrderedDict({})

  
  dic_nmlst['file'] = ' '             # Will be set iteratively in a loop
  dic_nmlst['model_type'] = 'EVOL'
  dic_nmlst['file_format'] = 'MESA' 
  dic_nmlst['deriv_type'] = 'MONO'
  dic_nmlst['regularize'] = False
  
  dic_nmlst['list_tup_const'] = []

  dic_nmlst['list_tup_mode'] = [(0, 0, '', -1, 100), (1, 0, '', -2000, 100)]
  
  dic_nmlst['list_tup_osc'] = [('DZIEM', 'DZIEM', 'BOTH', False), 
                              ('DZIEM', 'DZIEM', 'BOTH', False), ('DZIEM', 'DZIEM', 'BOTH', False)]
  dic_nmlst['reduce_order'] = False

  dic_nmlst['ivp_solver_type'] = 'MAGNUS_GL2'
  dic_nmlst['n_iter_max'] = 5500 
  dic_nmlst['use_banded'] = False
  dic_nmlst['deflate_roots'] = True
  
  dic_nmlst['list_tup_scan'] = [('LINEAR', 1e0, 15.0, 100, 'NONE'), 
                                ('INVERSE', 5, 45, 1000, 'UHZ')]

  dic_nmlst['list_tup_shoot'] = [('CREATE_CLONE', 0, 0, 5, 5, 0), 
                                ('RESAMP_DISPERSION', 0, 0, 5, 5, 0)] 
  dic_nmlst['list_tup_recon'] = [('CREATE_MIDPOINT', 0, 0, 5, 5, 0), 
                                ('RESAMP_DISPERSION', 0, 0, 5, 5, 0)] 
   
  dic_nmlst['summary_file'] = ' '            # Will be set iteratively in a loop
  dic_nmlst['summary_file_format'] = 'TXT' 
  dic_nmlst['summary_item_list'] = 'l,n_p,n_g,n_pg,omega,omega_im,freq,freq_units,f_T,f_g,psi_T,psi_g,E,E_norm,W,beta,M_star,R_star,L_star' 
  dic_nmlst['save_eig'] = False 
  dic_nmlst['mode_prefix'] = 'nad-eig-'
  chop1 = 'n,l,n_p,n_g,n_pg,omega,freq,freq_units,beta,E,E_norm,W,' 
  chop2 = 'x,xi_r,xi_h,Yt_1,Yt_2,phip,dphip_dx,delS,delL,delp,delrho,'
  chop3 = 'delT,dE_dx,dW_dx,prop_type,K,M_star,L_star,R_star,m'
  dic_nmlst['mode_item_list'] = chop1 + chop2 + chop3 
  dic_nmlst['freq_units'] = 'HZ'
    
  return dic_nmlst

#=================================================================================================================
def namelist_disentangle_eta_ov():
  """
  This function returns an ordered dictionary (i.e. OrderedDict) for the project that aims at distinguishing between 
  the effect of rotation and overshooting in period spacing.
  @return: a dictionary with the following key:value pairs. Some of the items must be set here, and some of them 
           (for instance the input and output file) must be set when the dictionary is retrieved.
  @rtype: OrderedDict
  """

  dic_nmlst = OrderedDict({})
  list_tup_osc = [(1, 'DZIEM')]
  list_tup_scan = [('LINEAR', 0.1, 15.0, 100, 'NONE'), 
                   ('INVERSE', 1e-1, 15.0, 4000, 'NONE')]
  list_tup_shoot = [('CREATE_CLONE', 0, 0, 10, 10, 0), 
                    ('RESAMP_DISPERSION', 0, 0, 10, 10, 0),
                    ('RESAMP_CENTER', 100, 0, 1, 1, 0),
                    ('RESAMP_THERMAL', 0, 0, 0, 0, 10 )]
  list_tup_recon = list_tup_shoot[:]

  for i_fail in range(1):
    dic_nmlst['file'] = ' '             # Will be set iteratively in a loop
    dic_nmlst['model_type'] = 'EVOL'
    dic_nmlst['file_format'] = 'MESA' 
    dic_nmlst['deriv_type'] = 'MONO'
    dic_nmlst['ivp_solver_type'] = 'MAGNUS_GL6'
    dic_nmlst['n_iter_max'] = 1500 
    dic_nmlst['reduce_order'] = False
    dic_nmlst['use_banded'] = True
    dic_nmlst['list_tup_osc'] = list_tup_osc
    dic_nmlst['list_tup_scan'] = list_tup_scan 
    dic_nmlst['list_tup_shoot'] = list_tup_shoot 
    dic_nmlst['list_tup_recon'] = list_tup_recon 
    dic_nmlst['summary_file'] = ' '            # Will be set iteratively in a loop
    dic_nmlst['summary_file_format'] = 'HDF' 
    dic_nmlst['summary_item_list'] = 'l,n_p,n_g,n_pg,omega,freq,E,E_norm,W,beta,M_star,R_star,L_star' 
    dic_nmlst['save_eig'] = False 
    dic_nmlst['mode_prefix'] = 'ad-eig-'
    chop1 = 'n,l,n_p,n_g,n_pg,omega,freq,freq_units,beta,E,E_norm,W,' 
    chop2 = 'x,xi_r,xi_h,Yt_1,Yt_2,phip,dphip_dx,delS,delL,delp,delrho,'
    chop3 = 'delT,dE_dx,dW_dx,prop_type,K,M_star,L_star,R_star,m'
    dic_nmlst['mode_item_list'] = chop1 + chop2 + chop3 
    dic_nmlst['freq_units'] = 'HZ'
    
  return dic_nmlst


#=================================================================================================================
def namelist_mesa_gyre():
  """
  This function returns an ordered dictionary (i.e. OrderedDict) with all the settings for the MESA-GYRE grid.
  @return: a dictionary with the following key:value pairs. Some of the items must be set here, and some of them 
           (for instance the input and output file) must be set when the dictionary is retrieved.
  @rtype: OrderedDict
  """

  dic_nmlst = OrderedDict({})
  list_tup_osc = [
                 # (0, 'DZIEM'), 
                 (1, 'DZIEM'), 
                 # (2, 'DZIEM')
                 ]
  list_tup_scan = [#('LINEAR', 0.08, 10.0, 50, 'NONE'), 
                   ('INVERSE', 0.06, 5.0, 250, 'NONE')]
  list_tup_shoot = [('CREATE_CLONE', 0, 0, 3, 3, 0), 
                    ('RESAMP_DISPERSION', 0, 0, 3, 3, 0),
                    #('RESAMP_CENTER', 100, 0, 2, 2, 0),
                    #('RESAMP_THERMAL', 0, 0, 0, 0, 10 )
                    ]
  list_tup_recon = list_tup_shoot[:]

  for i_fail in range(1):
    dic_nmlst['file'] = ' '             # Will be set iteratively in a loop
    dic_nmlst['model_type'] = 'EVOL'
    dic_nmlst['file_format'] = 'MESA' 
    dic_nmlst['deriv_type'] = 'MONO'
    dic_nmlst['ivp_solver_type'] = 'MAGNUS_GL4'
    dic_nmlst['n_iter_max'] = 1500 
    dic_nmlst['reduce_order'] = False
    dic_nmlst['use_banded'] = True
    dic_nmlst['list_tup_osc'] = list_tup_osc
    dic_nmlst['list_tup_scan'] = list_tup_scan 
    dic_nmlst['list_tup_shoot'] = list_tup_shoot 
    dic_nmlst['list_tup_recon'] = list_tup_recon 
    dic_nmlst['path_output'] = ' '            # Will be set iteratively in a loop
    dic_nmlst['summary_file_format'] = 'HDF' 
    dic_nmlst['summary_item_list'] = 'l,n_p,n_g,n_pg,omega,freq,E,E_norm,W,beta,M_star,R_star,L_star' 
    dic_nmlst['save_eig'] = False 
    dic_nmlst['mode_prefix'] = 'ad-eig-'
    dic_nmlst['mode_item_list'] = 'l,n_p,n_g,n_pg,omega,freq,E,E_norm,W,M_star,R_star,L_star,beta,x,xi_r,xi_h'
    dic_nmlst['freq_units'] = 'HZ'
    
  return dic_nmlst


#=================================================================================================================
def namelist_papics():
  """
  This function returns an ordered dictionary (i.e. OrderedDict) with all the settings for Peter Papics grid designed
  to match the observations for the Kepler SPB binary system.
  @return: a dictionary with the following key:value pairs. Some of the items must be set here, and some of them 
           (for instance the input and output file) must be set when the dictionary is retrieved.
  @rtype: OrderedDict
  """

  dic_nmlst = OrderedDict({})
  list_tup_osc = [(1, 'UNNO'), (2, 'UNNO')]
  list_tup_scan = [('LINEAR', 5.0, 25.0, 1000, 'PER_DAY'), 
                   ('INVERSE', 2e-1, 1e1, 20000, 'PER_DAY')]
  list_tup_shoot = [('CREATE_CLONE', 0, 0, 0, 0, 0), 
                    ('RESAMP_DISPERSION', 100, 0, 5, 1, 0)]
  list_tup_recon = list_tup_shoot[:]

  for i_fail in range(1):
    dic_nmlst['file'] = ' '             # Will be set iteratively in a loop
    dic_nmlst['model_type'] = 'EVOL'
    dic_nmlst['file_format'] = 'MESA' 
    dic_nmlst['deriv_type'] = 'MONO'
    dic_nmlst['ivp_solver_type'] = 'MAGNUS_GL4'
    dic_nmlst['n_iter_max'] = 500 
    dic_nmlst['reduce_order'] = False
    dic_nmlst['use_banded'] = True
    dic_nmlst['list_tup_osc'] = list_tup_osc
    dic_nmlst['list_tup_scan'] = list_tup_scan 
    dic_nmlst['list_tup_shoot'] = list_tup_shoot 
    dic_nmlst['list_tup_recon'] = list_tup_recon 
    dic_nmlst['path_output'] = ' '            # Will be set iteratively in a loop
    dic_nmlst['summary_file_format'] = 'HDF' 
    dic_nmlst['summary_item_list'] = 'l,n_p,n_g,n_pg,omega,freq,E,E_norm,W,beta,M_star,R_star,L_star' 
    dic_nmlst['save_eig'] = False 
    dic_nmlst['mode_prefix'] = 'eig-'
    dic_nmlst['mode_item_list'] = 'l,n_p,n_g,n_pg,freq,beta,x,xi_r,xi_h'
    dic_nmlst['freq_units'] = 'HZ'
    
  return dic_nmlst


#=================================================================================================================
def write_exec_gyre_script(platform, namelist_file):
  """
  This function receives the platform specification and the full path to the namelist file. It checks if the namelist
  file is already available on the disk; otherwise it exits. Then, calls another function to get the content of the
  bash script file as a list, and then writes the list into a bash file. Finally, it executes the script.
  @param platform: integer, specifying the platform on which the GYRE script will be executed.
  @type platform: integer 
  @param namelist_file: string, specifying the full path to an existing GYRE namelist file.
  @type namelist_file: string
  @return: None
  @rtype: None
  """
  
  flag = os.path.exists(namelist_file)
  if not flag:
    print 'Error: gyre_namelist: write_exec_gyre_script: This file does not exist: '
    print '       %s ' % (namelist_file, )
    raise SystemExit, 'Check the namelist file!'

  dic_path = scm.set_paths(platform)
  path_gyre_bash = dic_path['path_gyre_bash']
  gyre_bash_file = path_gyre_bash + 'exec-gyre.sh'
  
  # get the list with each item being a line in the bash script
  bash_script = gen_gyre_bash_list(platform, namelist_file)
  
  w = open(gyre_bash_file, 'w')
  w.writelines(bash_script)
  w.close()
  
  os.chmod(gyre_bash_file, 0755)
  cmnd = 'sbatch ' + gyre_bash_file
  #subprocess.call(cmnd, shell=True)
  output = os.popen(cmnd)
  

#=================================================================================================================
def gen_gyre_bash_list(platform, namelist_file, extra_tasks=None):
  """
  This is a preparation on running jobs using the SLURM job queueing system.
  This function receives the platform and the GYRE namelist full path, and generates a list of string, where each 
  item will be later written to the executable bash script to call GYRE.
  @param platform: integer, specifying the platform on which the GYRE script will be executed.
  @type platform: integer 
  @param namelist_file: string, specifying the full path to an existing GYRE namelist file.
  @type namelist_file: string
  @param extra_tasks: you can provide a list of strings, for as many extra tasks as you wish after the execution of 
     the main GYRE job. For instance, if you like to compress the output file, and move it to a different directory,
     you can provide a two-item list like 
     ['tar -zcf output.tar.gz output', 'mv output.tar.gz this_path'].
     You do NOT need to add newline format syntax, i.e. "\n" at the end of each item in the list.
  @type extra_tasks: list of strings
  @return: list of strings. each item is a line in the bash script that executes GYRE.
  @rtype: list of strings.
  """
  
  dic_path = scm.set_paths(platform)
  path_gyre_src  = dic_path['path_gyre_src']
  path_gyre_logs = dic_path['path_gyre_logs']
  path_gyre_errs = dic_path['path_gyre_errs']
  which_gyre     = dic_path['which_gyre']
  queue          = dic_path['queue']
  Pleiad         = dic_path['Pleiad']
  
  ind = namelist_file.rfind('/')
  nml_dir = namelist_file[:ind+1]
  nml_fname = namelist_file[ind+1:]
  job_name  = nml_fname.replace('nmlst', 'GYRE-job')
  out_file  = path_gyre_logs + nml_fname.replace('nmlst', 'out')
  err_file  = path_gyre_errs + nml_fname.replace('nmlst', 'err')
  
  out = []
  out.append('#!/bin/sh' + nl)
  out.append(nl)
  out.append('#SBATCH --account=GYRE-NonAdiab' + nl)
  out.append('#SBATCH --job-name=' + job_name + nl) # M04.1-eta0.00-ov0.030-sc0.00-Z0.018-MS-Yc0.9822-Xc0.0002.GYRE-job
  out.append('#SBATCH --output=' + out_file + nl)   # /STER/ehsan/gyre2.0/bin/logs/M04.1-eta0.00-ov0.030-sc0.00-Z0.018-MS-Yc0.9822-Xc0.0002.out
  out.append('#SBATCH --begin=now' + nl)
  out.append('#SBATCH --error=' + err_file + nl)     # /STER/ehsan/gyre2.0/bin/errs/M04.1-eta0.00-ov0.030-sc0.00-Z0.018-MS-Yc0.9822-Xc0.0002.err
  out.append('#SBATCH --cpus-per-task=1' + nl)
  out.append('#SBATCH --ntasks=1' + nl)
  if platform == Pleiad: out.append('#SBATCH --partition=' + queue + nl)
  out.append(nl)

  out.append('export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK' + nl)
  out.append('cd' + sp + path_gyre_src + sp + nl) 
  out.append(nl)

  out.append('srun --time="300:00"' + sp + which_gyre + sp + namelist_file + nl)
  out.append(nl)
  
  if extra_tasks:
    for extra_task in extra_tasks:
      out.append(extra_task + nl)
    out.append(nl)

  return out

#=================================================================================================================
def gen_namelist_name_from_input_fname(input_full_path):
  """
  This function receives the full path to the available .gyre file. Then, it generates a string for the namelist file
  to be written to the disk based on the input file name. It extracts the core of the filename, and replaces the 
  input file extension (i.e. *.gyre) with *.nmlst for the namelist. 
  
  @param input_full_path: string giving the full path to the available .gyre file
  @type input_full_path: string
  @return: A string is returned giving the filename (not the full path) for the namelist. The directory where the 
           namelist is store will be assigned separately.
           WARNING: The user must specify the directory to store the namelist file later. Otherwise, all files will be
           written in the current workind directory.
  @rtype: string
  """
  len_input_file = len(input_full_path)
  if len_input_file == 0:
    raise SystemExit, 'Error: gyre_namelist: gen_namelist_name_from_input_fname: empty input file!'

  ind = input_full_path.rfind('/')
  input_fnmae = input_full_path[ind+1:]
  namelist_name = input_fnmae.replace('gyre', 'nmlst')
  
  return namelist_name

  
#=================================================================================================================
def gen_output_fname_from_input_fname(platform, input_full_path):
  """
  This function receives a full name of an existing .gyre file for which the GYRE output is missing (either not 
  calculated or the calculation has crashed), and produces an appropriate output filename.
  """
  dic_path = scm.set_paths(platform)
  which_gyre     = dic_path['which_gyre']
  
  len_input_file = len(input_full_path)
  if len_input_file == 0:
    raise SystemExit, 'Error: gyre_namelist: gen_output_fname_from_input_fname: empty input file!'
  
  ind = input_full_path.rfind('/')
  input_dir = input_full_path[0 : ind+1]
  input_fnmae = input_full_path[ind+1 : -5]
  
  flag = os.path.exists(input_dir)
  if not flag: 
    print input_full_path
    raise SystemExit, 'Error: gyre_namelist: gen_output_fname_from_input_fname: Invalid input directory path %s' % (input_dir, )
  
  # Substitute gyre_in with gyre_out in the directory name
  output_dir = input_dir.replace('gyre_in', 'gyre_out')
  if which_gyre == 'gyre_ad':   output_fname = 'ad-sum-' + input_fnmae + '.h5'
  if which_gyre == 'gyre_nad':  output_fname = 'nad-sum-' + input_fnmae + '.h5'
  output_full_path = output_dir + output_fname
  
  return output_full_path
    

#=================================================================================================================
def write_gyre_namelist(path_namelist, dic):
  """
  This function receives a full path to store the namelist file, and a dictionary. The items() in the dictionary are
  fully explained in the function collect_gyre_namelist.
  @param path_namelist: string, specifying the full path to the location of the namelist file
  @type path_namelist: string
  @param dic: a dictionary with all required information to go into the namelist. For details, see the help under the 
        collect_gyre_namelist function.
  @return: None
  @rtype: None 
  """
  
  namelist = collect_gyre_namelist(dic)
  ind = path_namelist.rfind('/')
  save_dir = path_namelist[:ind]
  filename = path_namelist[ind+1:]
  
  flag = os.path.exists(save_dir)
  if not flag:
    message = 'Error: gyre_namelist: write_gyre_namelist: %s does not exist!' % (save_dir, )
    raise SystemExit, message
  
  w = open(path_namelist, 'w')
  w.writelines(namelist)
  w.close()
 
  return None

#=================================================================================================================
def collect_gyre_namelist(dic_in):                     
  """
  This function receives a dictionary with all required information as a key:value pair. values could be simple 
  integers, strings or more complicated data structures like lists of tuples! 
  You have to read the help from the whole functions in this module to know them right.
  
  @param dic_in: OrderedDict, with the following keys and values
  - path_input: string, giving the full path to the input .gyre file
  - model_type: string, specifying the input model being polytrope, or e.g. coming from MESA
  - file_format: string, the format of the input file, e.g. from MESA or FGONG, etc.
  - deriv_type: string, specifying the derivative type.
  - regularize: flag specifying whether or not to regularize the input model
  - ivp_solver_type: string, either of MAGNUS_GL2, 4 or 6 or FINDIFF.
  - n_iter_max: inteer, maximum iterations in root finding
  - use_banded: boolean, True or False.
  - list_tup_osc: list of tuples. Read gen_osc_list help for more information about the exact item orders.
  - list_tup_scan: list of tuples. Read gen_scan_list help for more information about the exact item orders.
  - list_tup_shoot: list of tuples. Read gen_shoot_grid_list help for more information about the exact item orders.
  - list_tup_recon: list of tuples. Read gen_recon_grid_list help for more information about the exact item orders.
  - path_output: string, specifying the exact path for the output file to be written to disk.
  - summary_file_format: string, either HDF or TXT
  - summary_item_list: string, comma separated list of items for the summary file
  - save_eig: boolean, True or False to decide wether or not saving the eigenfunction files to disk
  - mode_prefix: string, prefix for the eigenfunction filename
  - mode_item_list: string, comma separated list of items for the eigenfunction file
  - freq_units: string, specifying the output frequency unit.
  @type dic_in: OrderedDict
  
  @return: list of strings; It is a collection of shorter chunks of the namelist. This list will be written to a 
        file as a GYRE namelist. the newline string, i.e. \n is already included per each line, so one can use 
        writelines() for the purpose of writing the namelist to a file.
  @rtype: list of strings.
  """
  namelist = []
  
  path_input = dic_in['file']
  model_type = dic_in['model_type']
  file_format = dic_in['file_format']
  deriv_type = dic_in['deriv_type']
  regularize = dic_in['regularize']
  
  list_tup_const = dic_in['list_tup_const']
  
  list_tup_mode = dic_in['list_tup_mode']
  
  list_tup_osc = dic_in['list_tup_osc']

  ivp_solver_type = dic_in['ivp_solver_type']
  n_iter_max = dic_in['n_iter_max']
  use_banded = dic_in['use_banded']
  deflate_roots = dic_in['deflate_roots']
  
  list_tup_scan = dic_in['list_tup_scan']
  
  list_tup_shoot = dic_in['list_tup_shoot']
  
  list_tup_recon = dic_in['list_tup_recon']
  
  path_output = dic_in['summary_file']
  summary_file_format = dic_in['summary_file_format']
  summary_item_list = dic_in['summary_item_list']
  save_eig = dic_in['save_eig']
  mode_prefix = dic_in['mode_prefix']
  mode_item_list = dic_in['mode_item_list']
  freq_units = dic_in['freq_units']
  
  list_model  = gen_model_list(path_input, model_type, file_format, deriv_type, regularize)
  list_const  = gen_const_list(list_tup_const)
  list_mode   = gen_mode_list(list_tup_mode)
  list_osc    = gen_osc_list(list_tup_osc)
  list_num    = gen_num_list(ivp_solver_type, n_iter_max, use_banded, deflate_roots)
  list_scan   = gen_scan_list(list_tup_scan)
  list_shoot  = gen_shoot_grid_list(list_tup_shoot)
  list_recon  = gen_recon_grid_list(list_tup_recon)
  list_output = gen_output_list(path_output, summary_file_format, summary_item_list, save_eig, 
                                mode_prefix, mode_item_list, freq_units)

  for val in list_model:
    namelist.append(val)
  for val in list_const:
    namelist.append(val)
  for val in list_mode:
    namelist.append(val)
  for val in list_osc:
    namelist.append(val)
  for val in list_num:
    namelist.append(val)
  for val in list_scan:
    namelist.append(val)
  for val in list_shoot:
    namelist.append(val)
  for val in list_recon:
    namelist.append(val)
  for val in list_output:
    namelist.append(val)
  
  return namelist


#=================================================================================================================
def gen_output_list(path_output = 'sum.h5', summary_file_format = 'HDF', 
                    summary_item_list = 'M_star,R_star,L_star,l,n_p,n_g,n_pg,omega,freq,E,E_norm,W,beta', save_eig = False,
                    mode_prefix = 'eig-', mode_item_list = 'l,n_p,n_g,n_pg,freq,beta,x,xi_r,xi_h', freq_units = 'HZ'):
  """
  This function receives full information about how to output the results of the GYRE run. Most of the settings are
  set to an optimal default. The input variables are:
  - path_output: full path to where the short summary file is stored.
  - summary_file_format: either 'HDF' or 'TXT'
  - summary_item_list: see GYRE website for more info; the provided default is very optimal.
  - mode_prefix: prefix of HDF5 file containing detailed mode data 
  - mode_item_list: Items to appear in mode files; the provided default is minimal
  - freq_units: interpretation of output freq data; The options are:
    > 'NONE' : dimensionless angular frequency (default)
    > 'HZ' : linear frequency in Hz
    > 'UHZ' : linear frequency in uHz
    > 'ACOUSTIC_CUTOFF' : fraction of the acoustic cutoff frequency
    > 'GRAVITY_CUTOFF' : fraction of the gravity cutoff frequency                
    > 'PER_DAY'
    
  @param path_output: full path to the output
  @type path_output: string
  @param summary_file_format: either HDF or TXT
  @type summary_file_format: string
  @param summary_item_list: comma-separated list of items to write to summary file
  @type summary_item_list: string
  @param mode_prefix: eigenfunction file prefix
  @type mode_prefix: string
  @param mode_item_list: comma-separated list of items to write to mode file 
  @type mode_item_list: string
  @param freq_units: interpretation of output freq data
  @type freq_units: string
  
  @return: list of strings, where each string will be written to a namelist file later as an individual file.
  @rtype: list of strings.
  """
  
  out = []
  
  out.append('&output' + nl)
  out.append(sp + 'summary_file = ' + ap + path_output + ap + nl)
  out.append(sp + 'summary_file_format = ' + ap + summary_file_format + ap + nl)
  out.append(sp + 'summary_item_list = ' + ap + summary_item_list + ap + nl)
  if save_eig:
    ind_slash = path_output.rfind('/')
    address = path_output[:ind_slash+1]
    eig_name = path_output[ind_slash+1:-3].replace('ad-sum-', mode_prefix)
    eig_name = address + eig_name 
    out.append(sp + 'mode_prefix = ' + ap + eig_name + ap + nl)
    out.append(sp + 'mode_file_format = ' + ap + summary_file_format + ap + nl)
    out.append(sp + 'mode_item_list = ' + ap + mode_item_list + ap + nl)
  out.append(sp + 'freq_units = ' + ap + freq_units + ap + nl)
  out.append('/ ! End of GYRE namelist ' + nl)
  
  return out


#=================================================================================================================
def gen_recon_grid_list(list_tup):
  """
  This function receives a list of tuples. The number of tuples in the list determine how many reconstruction grids 
  to construct. The &recon_grid namelist will be iteratively crated for each tuple in the list. Each tuple has the 
  following in a row:
  - op_type: type of operation to apply; the options are: 
    > 'CREATE_GEOM' : create a grid with geometric spacing
    > 'CREATE_LOG' : create a grid with logarithmic spacing
    > 'CREATE_UNIFORM' : create a grid with uniform spacing
    > 'CREATE_CLONE' : create a grid by cloning the coefficients grid (default)
    > 'RESAMP_DISPERSION' : resample the grid using a local dispersion analysis
    > 'RESAMP_THERMAL' : resample the grid using a thermal timescale analysis
    > 'RESAMP_CENTER' : resample the grid to ensure resolution of the center
    > 'RESAMP_UNIFORM' : resample the grid uniformly
  - n: depends on op_type. Consult GYRE website
  - s: grid skewness parameter
  - alpha_osc: sampling rate in oscillatory regions (default=0)
  - alpha_exp: sampling rate in exponential regions (default=0)
  - alpha_thm: sampling rate for 'RESAMP_THERMAL' (default=0)
  
  @param list_tup: list of tuples. if it is empty, a default &recon_grid namelist will be created for 'CREATE_CLONE'.
         if (len(list_tup)) > 1, then it iterates over tuples and creates as many &recon_grid namelists as requested.
  @type list_tup: list of tuples.
  @return: list of strings, where each string will be written to a namelist file later as an individual file.
  @rtype: list of strings.
  """

  n_tup = len(list_tup)
  out = []
  
  if n_tup == 0:
    out.append('&recon_grid' + nl)
    out.append(sp + 'op_type = ' + ap + CREATE_CLONE + ap + nl)
    out.append(sp + '# n = 1' + nl)
    out.append(sp + '# s = 0' + nl)
    out.append(sp + 'alpha_osc = 0' + nl)
    out.append(sp + 'alpha_exp = 0' + nl)
    out.append(sp + 'alpha_thm = 0' + nl)
    out.append('/' + nl)    
    return out
  
  for i_tup in range(n_tup):
    op_type, n, s, alpha_osc, alpha_exp, alpha_thm = list_tup[i_tup]
    if type(op_type) is not type(' a string '):
      message = 'Error gyre_namelist: gen_recon_grid_list: The input op_type as the 1st item in the tuple is not a string!'
      raise SystemExit, message 
    
    str_n = str(n)
    str_s = str(s)
    str_alpha_osc = str(alpha_osc)
    str_alpha_exp = str(alpha_exp)
    str_alpha_thm = str(alpha_thm)
    
    out.append('&recon_grid' + nl)
    out.append(sp + 'op_type = ' + ap + op_type + ap + nl)
    out.append(sp + 'n = ' + str_n + nl)
    out.append(sp + 's = ' + str_s + nl)
    out.append(sp + 'alpha_osc = ' + str_alpha_osc + nl)
    out.append(sp + 'alpha_exp = ' + str_alpha_exp + nl)
    out.append(sp + 'alpha_thm = ' + str_alpha_thm + nl)
    out.append('/' + nl)    
    
  return out    


#=================================================================================================================
def gen_shoot_grid_list(list_tup):
  """
  This function receives a list of tuples. The number of tuples in the list determine how many shooting grids to 
  construct. The &shoot_grid namelist will be iteratively crated for each tuple in the list. Each tuple has the 
  following in a row:
  - op_type: type of operation to apply; the options are: 
    > 'CREATE_GEOM' : create a grid with geometric spacing
    > 'CREATE_LOG' : create a grid with logarithmic spacing
    > 'CREATE_UNIFORM' : create a grid with uniform spacing
    > 'CREATE_CLONE' : create a grid by cloning the coefficients grid (default)
    > 'RESAMP_DISPERSION' : resample the grid using a local dispersion analysis
    > 'RESAMP_THERMAL' : resample the grid using a thermal timescale analysis
    > 'RESAMP_CENTER' : resample the grid to ensure resolution of the center
    > 'RESAMP_UNIFORM' : resample the grid uniformly
  - n: depends on op_type. Consult GYRE website
  - s: grid skewness parameter
  - alpha_osc: sampling rate in oscillatory regions (default=0)
  - alpha_exp: sampling rate in exponential regions (default=0)
  - alpha_thm: sampling rate for 'RESAMP_THERMAL' (default=0)
  
  @param list_tup: list of tuples. if it is empty, a default &shoot_grid namelist will be created for 'CREATE_CLONE'.
         if (len(list_tup)) > 1, then it iterates over tuples and creates as many &shoot_grid namelists as requested.
  @type list_tup: list of tuples.
  @return: list of strings, where each string will be written to a namelist file later as an individual file.
  @rtype: list of strings.
  """
  
  n_tup = len(list_tup)
  out = []
  
  if n_tup == 0:
    out.append('&shoot_grid' + nl)
    out.append(sp + 'op_type = ' + ap + CREATE_CLONE + ap + nl)
    out.append(sp + '# n = 1' + nl)
    out.append(sp + '# s = 0' + nl)
    out.append(sp + 'alpha_osc = 0' + nl)
    out.append(sp + 'alpha_exp = 0' + nl)
    out.append(sp + 'alpha_thm = 0' + nl)
    out.append('/' + nl)    
    return out
  
  for i_tup in range(n_tup):
    op_type, n, s, alpha_osc, alpha_exp, alpha_thm = list_tup[i_tup]
    if type(op_type) is not type(' a string '):
      message = 'Error gyre_namelist: gen_shoot_grid_list: The input op_type as the 1st item in the tuple is not a string!'
      raise SystemExit, message 
    
    str_n = str(n)
    str_s = str(s)
    str_alpha_osc = str(alpha_osc)
    str_alpha_exp = str(alpha_exp)
    str_alpha_thm = str(alpha_thm)
    
    out.append('&shoot_grid' + nl)
    out.append(sp + 'op_type = ' + ap + op_type + ap + nl)
    out.append(sp + 'n = ' + str_n + nl)
    out.append(sp + 's = ' + str_s + nl)
    out.append(sp + 'alpha_osc = ' + str_alpha_osc + nl)
    out.append(sp + 'alpha_exp = ' + str_alpha_exp + nl)
    out.append(sp + 'alpha_thm = ' + str_alpha_thm + nl)
    out.append('/' + nl)    
    
  return out    


#=================================================================================================================
def gen_scan_list(list_tup):
  """
  This function receives a list of tuples. The number of tuples in the list determine how many scans to do. 
  The &osc namelist will be iteratively crated for each tuple in the list. Each tuple has the following in a row:
  - grid_type: the options are: 'LINEAR' or 'INVERSE'
  - freq_min: minimum frequency to start the search
  - freq_max: maximum frequency to end the search
  - n_freq: number of frequency points
  - freq_units: The output unit for frequency
  It returns a list of strings, where each string will sit as a new line in the output namelist file.
  
  @param list_tup: list of tuple(s). if it is empty, a default scan namelist will be created for a linear scanning.
          If len(list_tup) > 1, then it iterates over tuples and creates as many scan namelist as provided in the tuples.
  @type list_tup: list of tuples. Each tuple (grid_type, freq_min, freq_max, freq_units) consists of a string, two 
          integers, and another string.
  @return: A list of strings, where each string will be written to a namelist file later as an individual file.
  @rtype: list of strings.
  """

  n_tup = len(list_tup)
  out = []
  
  if n_tup == 0:
    out.append('&scan' + nl)
    out.append(sp + 'grid_type = LINEAR' + nl)    
    out.append(sp + 'freq_min = 0.1' + nl)
    out.append(sp + 'freq_max = 10' + nl)
    out.append(sp + 'n_freq = 20000' + nl)
    out.append(sp + 'freq_units = ' + sp + 'NONE' + sp + nl)
    out.append('/' + nl)
    return out

  for i_tup in range(n_tup):
    grid_type, freq_min, freq_max, n_freq, freq_units = list_tup[i_tup]
    if type(grid_type) is not type(' a string '):
      message = 'Error gyre_namelist: gen_scan_list: The input grid_type as the 1st item in the tuple is not a string!'
      raise SystemExit, message 
    if type(freq_units) is not type(' a string '):
      message = 'Error gyre_namelist: gen_scan_list: The input freq_units as the 5th item in the tuple is not a string!'
      raise SystemExit, message 

    str_fmin = str(freq_min)
    str_fmax = str(freq_max)
    str_nfeq = str(n_freq)      
    
    out.append('&scan' + nl)
    out.append(sp + 'grid_type = ' + ap + grid_type + ap + nl)    
    out.append(sp + 'freq_min = ' + str_fmin + nl)
    out.append(sp + 'freq_max = ' + str_fmax + nl)
    out.append(sp + 'n_freq = ' + str_nfeq + nl)
    out.append(sp + 'freq_units = ' + ap + freq_units + ap + nl)
    out.append('/' + nl)
    
  return out


#=================================================================================================================
def gen_osc_list(list_tup):
  """
  This function receives a list of tuples. The number of tuples in the list determine how many ell values to search for 
  and the &osc namelist will be iteratively crated for each ell value. Each tuple is a pair of ell value and the outer
  boundary condition (outer_bound_type). The options for the latter are 'ZERO', 'DZIEM', 'UNNO' and 'JCD'.
  
  @param list_tup: list of tuple(s). if it is empty, a default osc namelist will be created for radial (l=0) mode scanning.
          If len(list_tup) > 1, then it iterates over tuples and creates as many osc namelist as provided in the tuples.
  @type list_tup: list of tuples. Each tuple (el, string) consists of an integer el for the degree and a string specifying
          the outer bondary condition.
  @return: A list of strings, where each string will be written to a namelist file later as an individual file.
  @rtype: list of strings.
  """
  
  n_tup = len(list_tup)
  out = []
 
  if n_tup == 0:  # just write a default radial mode!
    print 'Warning: gyre_namelist: gen_osc_list: Creating the Default &osc namelist.'
    out.append('&osc' + nl)
    out.append(sp + 'l = 0' + nl)
    out.append(sp + 'outer_bound_type = ' + ap + 'ZERO' + ap + nl)    
    out.append(sp + 'variables_type = ' + ap + 'DZIEM' + ap + nl)
    out.append(sp + 'inertia_norm_type = ' + ap + 'BOTH' + ap + nl)
    out.append(sp + 'reduce_order = .false.' + nl)
    out.append('/' + nl)
    return out

  #for i_tup in range(n_tup):
  for tup in list_tup:
    outer_bound_type, variables_type, inertia_norm_type, reduce_order = tup
    if type(outer_bound_type) is not type(' a char '): 
      message = 'Error gyre_namelist: gen_osc_list: The input outer_bound_type as the 1st item in the tuple is not a string!'
      raise SystemExit, message 
    
    out.append('&osc' + nl)
    out.append(sp + 'outer_bound_type = ' + ap + outer_bound_type + ap + nl)    
    out.append(sp + 'variables_type = ' + ap + variables_type + ap + nl)
    out.append(sp + 'inertia_norm_type = ' + ap + inertia_norm_type + ap + nl)
    # out.append(sp + 'reduce_order = ' + '.' + str(reduce_order).lower() + '.' + nl)
    out.append('/' + nl)
    out.append(nl)

  return out


#=================================================================================================================
def gen_num_list(solver = 'MAGNUS_GL4', n_iter_max = 50, use_banded = False, deflate_roots=True):
  """
  This function receives four optional arguments, and creates a list where each item in the list is a line in the 
  namelist and will be written to a file later.
  @param solver: which solver to use? The options are: 'MAGNUS_GL2', 'MAGNUS_GL4' (=default), 'MAGNUS_GL6' and 
         'FINDIFF'.
  @type solver: string
  param n_iter_max: integer for maximum number of iterations in root-finding algorithms (default=50)
  @type n_iter_max: integer
  @param use_banded: Fortran boolean. flag to use banded algorithms on the system matrix (default .FALSE.)
  @return: list of strings, where each string sits in a new line.
  @rtype: list of strings
  """
  
  out = []
  out.append('&num' + nl)
  out.append(sp + 'n_iter_max = ' + str(n_iter_max) + nl)
  out.append(sp + 'ivp_solver_type = ' + ap + solver + ap + nl)
  out.append(sp + 'use_banded = ' + '.' + str(use_banded).lower() + '.' + nl)
  # out.append(sp + 'deflate_roots = ' + '.' + str(deflate_roots).lower() + '.' + nl)
  out.append('/' + nl)
  
  return out

#=================================================================================================================
def gen_mode_list(list_mode):
  """
  This function receives a list of modes and their specifications, and returns a list for the &mode part of inlist
  to be written in the output inlist.
  @return: list of strings specifying mode properties
  @rtype: list 
  """
  out = []
  n_modes = len(list_mode)
  if n_modes == 0:
    raise SystemExit, 'Error: gyre_namelist: gen_mode_list: Input list is empty.'
  for i_mode, tup_mode in enumerate(list_mode):
    el, em, tag,  n_pg_min, n_pg_max = tup_mode
    out.append('&mode' + nl)
    out.append(sp + 'l = ' + str(el) + nl)
    if em != 0:
      raise SystemExit, 'Error: gyre_namelist: gen_mode_list: m >0 not supported yet.'
    out.append(sp + 'm = 0' + nl)
    if len(tag) > 0:
      out.append(sp + 'tag = ' + ap + tag + ap + nl)
    out.append(sp + 'X_n_pg_min = ' + str(n_pg_min) + nl)
    out.append(sp + 'X_n_pg_max = ' + str(n_pg_max) + nl)
    out.append('/' + nl)
    
  return out

#=================================================================================================================
def gen_const_list(list_const):
  """
  This function receives the list of constants to change, and returns a simple dictionary with a list of lines for the &constants 
  section of the GYRE namelist.
  @return: list of strings specifying null constant specifications 
  @rtype: list
  """
  out = []
  out.append('&constants' + nl)
  out.append('/' + nl)
  if len(list_const) > 0:
    raise SystemExit, 'Error: gen_namelist: gen_const_list: First implement change of constants; then, use it!'

  return out  

#=================================================================================================================
def gen_model_list(path_input, model_type = 'EVOL', file_format = 'MESA', deriv_type = 'MONO', regularize=True):
  """
  This function receives a path to a GYRE input model, and creates a list to be returned to the main program to 
  generate the model GYRE namelist
  @param path_input: full path to the input .gyre file
  @type path_input: string
  @param model_type: string specifying the type of input. The options are: 'HOM', 'POLY' and 'EVOL' (=default).
  @type model_type: string
  @param file_format: string giving the format of the input file if model_type == 'EVOL'. The options are:
          'GMS', 'MESA', 'OSC' and 'FGONG'.
  @param deriv_type: spline interpolation derivatives type. The options are 'NATURAL', 'FINDDIFF' and 'MONO' (=default).
  @return: returns a list. Each item in the list will be written as a new line to a file later.
  @rtype: list of character strings
  """
  
  out = []
  out.append('&model' + nl)
  out.append(sp + "file = " + ap + path_input + ap + nl)
  out.append(sp + 'model_type = ' + ap + model_type + ap + nl)
  out.append(sp + 'file_format = ' + ap + file_format + ap + nl)
  out.append(sp + 'deriv_type  = ' + ap + deriv_type  + ap + nl)
  out.append(sp + 'regularize = ' + '.' + str(regularize).lower() + '.' + nl)
  out.append('/' + nl)
  
  return out


#=================================================================================================================
def get_mass_from_name(filename):
  """
  Split the filename by '-', and search for mass string
  @param filename: full path to the input GYRE file
  @type filename: string
  @return: mass string associated to the input file, e.g. 'M12.34'
  @rtype: string
  """
  ind_slash = filename.rfind('/')
  core_name = filename[ind_slash+1:]
  pieces    = core_name.split('-')

  return pieces[0]


#=================================================================================================================
def get_output_core(filename):
  """
  From an input gyre_in filename, extract the core of the name, dropping the leading directory name and trailing 
  extension name
  @param filename: full path to the input GYRE file
  @type filename: string
  @return: the core of the file name which contains the physical parameters of that specific models
  @rtype: string
  """
  ind_slash = filename.rfind('/')
  ind_point = filename.rfind('.')
  
  return filename[ind_slash+1 : ind_point]

  
#=================================================================================================================


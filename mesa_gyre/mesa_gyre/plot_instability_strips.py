
import sys, os, glob
import numpy as np 
import logging
import itertools
import matplotlib as mpl
import pylab as plt
import plot_commons as pcm
import write

#=================================================================================================================
Msun = 1.9892e33    # g
Rsun = 6.9598e10    # cm
Lsun = 3.8418e33    # cgs
G    = 6.67428e-8   # cm^3 g^-1 s^-2

#=================================================================================================================
def find_closest_GYRE_model_by_Teff_logg(recarr, log_Teff, log_g):
  """
  To search the grid (and actually the record array for the tabulated results), to find the closest 
  match to the required log_Teff and log_g.
  """
  arr_log_Teff = recarr['log_Teff']
  arr_log_g    = recarr['log_g']

  dist         = np.sqrt((arr_log_Teff-log_Teff)**2 + (arr_log_g-log_g)**2)
  ind_min      = np.argmin(dist)
  min_dist     = dist[ind_min]
  best_row     = recarr[ind_min]

  print
  for name in recarr.dtype.names:
    print name, best_row[name]
  print

  return best_row

#=================================================================================================================
def give_position_p_or_g_or_hybrid(dics, el, style, which='hybrid'):
  """
  Reduce the input arrays, based on "which" option for "p", "g", or "hybrid" is specified.
  The reduced x, y, z and the index array that produced the new x, y, z are returned.
  """ 
  if which not in ['p', 'g', 'hybrid']:
    logging.error('journal_module: give_position_p_or_g_or_hybrid: Wrong choice for which={0}'.format(which))
    raise SystemExit, 'Error: journal_module: give_position_p_or_g_or_hybrid: Wrong choice for which={0}'.format(which)

  if el == 0 and which != 'p':    
    logging.error('journal_module: give_position_p_or_g_or_hybrid: for el=0; which must be "p".')
    raise SystemExit, 'Error: journal_module: give_position_p_or_g_or_hybrid: for el=0; which must be "p".'

  x, y, excited_p, excited_g, scatter_size = give_what_is_needed(dics, el, style)

  if which == 'p':
    ind_pure_p  = np.where((excited_p > 0) & (excited_g == 0))[0]
    pure_p      = excited_p[ind_pure_p]
    pure_p_x    = x[ind_pure_p]
    pure_p_y    = y[ind_pure_p]
    pure_p_size = scatter_size[ind_pure_p]

    return (pure_p_x, pure_p_y, pure_p, ind_pure_p, pure_p_size)


  if el > 0 and which == 'g':
    ind_pure_g  = np.where((excited_g > 0) & (excited_p == 0))[0]
    pure_g      = excited_g[ind_pure_g]
    pure_g_x    = x[ind_pure_g]
    pure_g_y    = y[ind_pure_g]
    pure_g_size = scatter_size[ind_pure_g]

    return (pure_g_x, pure_g_y, pure_g, ind_pure_g, pure_g_size)


  if el > 0 and which == 'hybrid':
    ind_hybrid  = np.where((excited_p > 0) & (excited_g > 0))[0]
    hybrid      = excited_p[ind_hybrid] + excited_g[ind_hybrid]
    hybrid_x    = x[ind_hybrid]
    hybrid_y    = y[ind_hybrid]
    hybrid_size = scatter_size[ind_hybrid]

    return (hybrid_x, hybrid_y, hybrid, ind_hybrid, hybrid_size)

#=================================================================================================================
def give_what_is_needed(dics, el, style):
  n_dic        = len(dics)
  Xc_arr       = np.zeros(n_dic)
  M_arr        = np.zeros(n_dic)
  R_arr        = np.zeros(n_dic)
  L_arr        = np.zeros(n_dic)
  log_Teff_arr = np.zeros(n_dic)
  log_g_arr    = np.zeros(n_dic)
  log_L_arr    = np.zeros(n_dic)
  n_excited_p  = np.zeros(n_dic, dtype=int)
  n_excited_g  = np.zeros(n_dic, dtype=int)  

  for i_dic, dic in enumerate(dics):
    Xc                  = dic['Xc']
    Xc_arr[i_dic]       = Xc
    M_star              = dic['M_star'] / Msun
    R_star              = dic['R_star'] / Rsun
    L_star              = dic['L_star'] / Lsun
    M_arr[i_dic]        = M_star
    R_arr[i_dic]        = R_star
    L_arr[i_dic]        = L_star
    log_L_arr[i_dic]    = np.log10(L_star)
    log_Teff_arr[i_dic] = np.log10(5777.) + np.log10(L_star)/4 - np.log10(R_star)/2
    log_g_arr[i_dic]    = np.log10(G*Msun/np.power(Rsun, 2)) + np.log10(M_star) - 2 * np.log10(R_star)

    all_el              = dic['l']
    ind_el              = np.where(all_el == el)[0]
    n_pg                = dic['n_pg'][ind_el]
    freq_re             = np.real(dic['freq'])[ind_el]
    freq_im             = np.imag(dic['freq'])[ind_el]

    ind_p               = np.where(n_pg >= 0)[0]
    ind_g               = np.where(n_pg < 0)[0]
    ind_excited_p       = np.where(freq_im[ind_p] <= 0)[0]
    ind_excited_g       = np.where(freq_im[ind_g] <= 0)[0]
    # bad_Xc              = (Xc <= 1e-3) & (Xc >= 1e-4)
    # if bad_Xc:          continue
    n_excited_p[i_dic]  = len(ind_excited_p)
    n_excited_g[i_dic]  = len(ind_excited_g)

    ########################################
    # Setup your reference x- and y-axis values
    ########################################
    xvals  = log_Teff_arr
    if style == 'kiel':
      yvals = log_g_arr
    elif style == 'hrd':
      yvals = log_L_arr
    elif style == 'shrd':
      yvals = 4 * log_Teff_arr - log_g_arr
    else:
      logging.error('journal_module: give_what_is_needed: there is another issue with style') 
      raise SystemExit, 'Error: journal_module: give_what_is_needed: there is another issue with style'

  # Trick: by reversing all arrays, the S-hook and early-PMS models fall below the MS models
  # xvals       = xvals[::-1]
  # yvals       = yvals[::-1]
  # n_excited_p = n_excited_p[::-1]
  # n_excited_g = n_excited_g[::-1]

  scatter_size= get_scatter_size(M_arr) #[::-1]

  return (xvals, yvals, n_excited_p, n_excited_g, scatter_size)

#=================================================================================================================
def gen_contour(x, y, z):
  """
  Return the contour object of the non-uniformly spaced x, y and z grid points. Here, z is the Surface
  to be plotted. x, y and z are all 1D ndarray vectors.
  """
  from matplotlib.mlab import griddata
  n_pts = 201
  # z     = 1./z
  x_p   = np.linspace(np.min(x), np.max(x), n_pts, endpoint=True)
  y_p   = np.linspace(np.min(y), np.max(y), n_pts, endpoint=True)
  z_p   = griddata(x, y, z, x_p, y_p, interp='nn')
  levels= np.linspace(np.min(z_p), np.max(z_p), 3, endpoint=True)
  cont  = plt.contour(x_p, y_p, z_p, levels)

  return cont

#=================================================================================================================
def get_scatter_size(M_ini):
  """
  The symbol sizes are variable and defined to depend on the logarithm of the initial mass to make sure that 
  there is no empty (white or transparent) space between tracks at different masses
  """
  size_lo_mass = 15
  size_hi_mass = 0.75
  lo_mass      = np.log10(2.5)
  hi_mass      = np.log10(25.)
  slope        = (size_hi_mass - size_lo_mass) / (hi_mass - lo_mass)
  size         = size_lo_mass + slope * (np.log10(M_ini) - lo_mass)

  return size 

#=================================================================================================================
def get_colorbar_positions(axis):
  """
  return the exact position of the p-mode, hybrids and g-mode colorbars, based on the position 
  of the axis.
  """
  pos = axis.get_position()
  x0  = pos.x0
  y0  = pos.y0
  w   = pos.width
  h   = pos.height
  x1  = x0 + w
  y1  = y0 + h

  dh  = 0.015           # space between little colorbars
  dy  = (h - 2*dh) / 3  # the height of each little colorbar
  dx  = 0.01            # distance of colorbar axis from the axis
  dz  = 0.005           # little offsets of colorbars from the top/bottom

  p   = [x1+dx, y0+2*(dh+dy), 0.02, dy-dz]  # colorbar axis position for p-modes
  h   = [x1+dx, y0+dh+dy, 0.02, dy-dz]      # colorbar axis position for hybrids
  g   = [x1+dx, y0+dz, 0.02, dy-dz]         # colorbar axis position for g-modes

  return p, h, g

#=================================================================================================================
def scatter_instability_strips(recarr, list_hist=[], style='Kiel', 
                               logT_from=4.6, logT_to=4, yaxis_from=4.5, yaxis_to=3.2, 
                               tags=[], list_dic_annot=[], contour_file_prefix=None, file_out=None):
  """
  This is copied and modified using plot_gyre.scatter_instability_strips(). Refer to that function
  for more detailed documentation.
  @param contour_file_prefix: prefix for an ascii file to store contour vertices. the full name 
         is internally created after appending appropriate degree number to the file name.
  @type contour_file_prefix: string
  """
  if file_out is None: return
  n_hist   = len(list_hist)
  add_hist = n_hist > 0

  style = style.lower()
  if style not in ['kiel', 'hrd', 'shrd']:
    logging.error('journal_module: scatter_instability_strips: style={0} is not supported'.format(style.lower()))
    raise SystemExit, 'Error: journal_module: scatter_instability_strips: style={0} is not supported'.format(style.lower())

  ########################################
  # Create the plot
  ########################################
  from matplotlib.colors import BoundaryNorm
  from matplotlib.ticker import MaxNLocator
  fig, ax_tup = plt.subplots(3, 1, figsize=(5, 10), 
                         dpi=400, sharex=True, sharey=True)
  plt.subplots_adjust(left=0.112, right=0.885, bottom=0.045, top=0.99, hspace=0.02)

  ########################################
  # Loop over all el values
  ########################################
  for el in range(3):
    ax            = ax_tup[el]

    xvals         = recarr['log_Teff']
    if style == 'kiel': yvals = recarr['log_g']
    if style == 'hrd':  yvals = recarr['log_L']
    if style == 'shrd': yvals = 4*recarr['log_Teff'] - recarr['log_g'] - 10.61
    M_ini         = recarr['M_ini']
    sct_size      = get_scatter_size(M_ini)

    ref_max_p     = 0
    ref_max_g     = 0
    ref_max_h     = 0

    p_name        = 'n_p_{0}'.format(el)
    g_name        = 'n_g_{0}'.format(el)
    h_name        = 'n_h_{0}'.format(el)

    ref_pure_p    = recarr[p_name]
    ind_pure_p    = np.where(ref_pure_p > 0)[0]
    ref_pure_p    = ref_pure_p[ind_pure_p]
    ref_pure_p_x  = xvals[ind_pure_p]
    ref_pure_p_y  = yvals[ind_pure_p]
    sct_size_p    = sct_size[ind_pure_p]
    ref_max_p     = np.max(ref_pure_p)

    if el > 0:
      ref_pure_g    = recarr[g_name]
      ind_pure_g    = np.where(ref_pure_g > 0)[0]
      ref_pure_g    = ref_pure_g[ind_pure_g]
      ref_pure_g_x  = xvals[ind_pure_g]
      ref_pure_g_y  = yvals[ind_pure_g]
      sct_size_g    = sct_size[ind_pure_g]
      ref_max_g     = np.max(ref_pure_g)

    if el > 0:
      ref_hybrid    = recarr[h_name]
      ind_hybrid    = np.where(ref_hybrid > 0)[0]
      ref_hybrid    = ref_hybrid[ind_hybrid]
      ref_hybrid_x  = xvals[ind_hybrid]
      ref_hybrid_y  = yvals[ind_hybrid]
      sct_size_h    = sct_size[ind_hybrid]
      ref_max_h     = np.max(ref_hybrid)

    sort_p        = np.argsort(ref_pure_p)
    if el > 0:
      sort_g      = np.argsort(ref_pure_g)
      sort_h      = np.argsort(ref_hybrid)

    print ' - scatter_instability_strips: el={0}: num excited p={0}, g={1}'.format(el, ref_max_p, ref_max_g)

    ########################################
    # Set Colormap and Normalize the color
    ########################################
    lev_factor  = 0.33
    low_lev_p   = -lev_factor * ref_max_p
    levels_p    = MaxNLocator(nbins=ref_max_p).tick_values(low_lev_p, ref_max_p+1)
    if el > 0:
      low_lev_g = -lev_factor * ref_max_g
      low_lev_h = -lev_factor * ref_max_h
      levels_g  = MaxNLocator(nbins=ref_max_g).tick_values(low_lev_g, ref_max_g+1)
      levels_h  = MaxNLocator(nbins=ref_max_h).tick_values(low_lev_h, ref_max_h+1)

    cmap_p      = plt.get_cmap('Reds')
    norm_p      = mpl.colors.Normalize(vmin=1, vmax=ref_max_p)
    if el > 0:
      cmap_g    = plt.get_cmap('Blues')
      norm_g    = mpl.colors.Normalize(vmin=1, vmax=ref_max_g)
      cmap_h    = plt.get_cmap('Greys')
      norm_h    = mpl.colors.Normalize(vmin=1, vmax=ref_max_h)

    # cmap_p.set_under('w', alpha=0)
    # cmap_g.set_under('w', alpha=0)
    fig.patch.set_facecolor('none')
    fig.patch.set_alpha(0.0)
    ax.patch.set_facecolor('none')
    ax.patch.set_alpha(0.0)

    tup_pos_pgh = get_colorbar_positions(ax)
    pos_ax_p    = tup_pos_pgh[0]
    pos_ax_h    = tup_pos_pgh[1]
    pos_ax_g    = tup_pos_pgh[2]

    ########################################
    # Scatter Plots
    ########################################
    # ax.scatter(ref_xvals, ref_yvals, marker='.', s=1, color='0.9', zorder=1) 

    # Pure p-modes
    # sc_p  = ax.scatter(ref_pure_p_x[sort_p], ref_pure_p_y[sort_p], 
    #                    c=ref_pure_p[sort_p], s=sct_size_p[sort_p], 
    sc_p  = ax.scatter(ref_pure_p_x, ref_pure_p_y, 
                       c=ref_pure_p, s=sct_size_p, 
                       cmap=cmap_p, vmin=1, vmax=ref_max_p, 
                       marker='s', edgecolor='none')
    ax_p  = fig.add_axes(pos_ax_p)
    tvl_p = [1, ref_max_p/4, ref_max_p/2, 3*ref_max_p/4, ref_max_p]   # ticks, values and ticklabels for p-modes
    cb_p  = plt.colorbar(sc_p, cax=ax_p, norm=norm_p, format='%d', ticks=tvl_p) #, values=tvl_p)
    cb_p.set_ticklabels([str(a) for a in tvl_p], update_ticks=True)
    cb_p.set_ticks(tvl_p)
    cb_p.set_label(r'p-modes', fontsize=8)
    cb_p.ax.tick_params(labelsize=10) 
    cb_p.set_clim(-lev_factor*ref_max_p, ref_max_p)
    cb_p.update_ticks()


    if el > 0 and ref_max_g > 0:
      # Pure g-modes
      sc_g  = ax.scatter(ref_pure_g_x[sort_g], ref_pure_g_y[sort_g], 
                         c=ref_pure_g[sort_g], s=sct_size_g[sort_g], 
                         cmap=cmap_g, vmin=1, vmax=ref_max_g, 
                         marker='s', edgecolor='none')
      ax_g  = fig.add_axes(pos_ax_g)
      tvl_g = [1, ref_max_g/4, ref_max_g/2, 3*ref_max_g/4, ref_max_g] # ticks, values and ticklabels for g-modes
      cb_g  = plt.colorbar(sc_g, cax=ax_g, norm=norm_g, format='%d', ticks=tvl_g)
      cb_g.set_ticks(tvl_g)
      cb_g.set_ticklabels([str(a) for a in tvl_g], update_ticks=True)
      cb_g.set_label(r'g-modes', fontsize=8)
      cb_g.ax.tick_params(labelsize=10) 
      cb_g.set_clim(-lev_factor*ref_max_g, ref_max_g)
      cb_g.update_ticks()


    if el > 0 and ref_max_h > 0:
      # Hybrids
      sc_h  = ax.scatter(ref_hybrid_x[sort_h], ref_hybrid_y[sort_h], 
                         c=ref_hybrid[sort_h], s=sct_size_h[sort_h], 
                         cmap=cmap_h, vmin=1, vmax=ref_max_h, 
                         marker='s', edgecolor='none', alpha=0.5)
      ax_h  = fig.add_axes(pos_ax_h)
      tvl_h = [1, ref_max_h/4, ref_max_h/2, 3*ref_max_h/4, ref_max_h]   # ticks, values and ticklabels for hybrids
      cb_h  = plt.colorbar(sc_h, cax=ax_h, norm=norm_h, format='%d', ticks=tvl_h)
      cb_h.set_ticks(tvl_h)
      cb_h.set_ticklabels([str(a) for a in tvl_h], update_ticks=True)
      cb_h.set_label(r'Hybrids', fontsize=8)
      cb_h.ax.tick_params(labelsize=10) 
      cb_h.set_clim(-lev_factor*ref_max_h, ref_max_h)
      cb_h.update_ticks()

    ########################################
    from matplotlib.mlab import griddata

    n_pts = 201 
    # ref_excited_p = np.ma.masked_where(ref_excited_p==0, ref_excited_p)
    # ref_excited_g = np.ma.masked_where(ref_excited_g==0, ref_excited_g)

    if False:
      x_p   = np.linspace(np.min(ref_xvals), np.max(ref_xvals), n_pts, endpoint=True)
      y_p   = np.linspace(np.min(ref_yvals), np.max(ref_yvals), n_pts, endpoint=True)
      z_p   = griddata(ref_xvals, ref_yvals, ref_excited_p, x_p, y_p, interp='nn')
      c_p   = ax.contour(x_p, y_p, z_p, [1], linewidths=0.5, colors='red', linestyles='dashed')
      cf_p  = ax.contourf(x_p, y_p, z_p, range(1,ref_max_p+1), cmap=cmap_p, norm=norm_p, alpha=1)
      ax_p  = fig.add_axes([0.91, 0.56, 0.02, 0.41])
      cb_p  = plt.colorbar(cf_p, cax=ax_p)
      cb_p.set_label(r'Num. excited p-modes', fontsize=8)

      if el > 0:
        x_g   = np.linspace(np.min(ref_xvals), np.max(ref_xvals), n_pts, endpoint=True)
        y_g   = np.linspace(np.min(ref_yvals), np.max(ref_yvals), n_pts, endpoint=True)
        z_g   = griddata(ref_xvals, ref_yvals, ref_excited_g, x_g, y_g, interp='nn')
        c_g   = ax.contour(x_g, y_g, z_g, [1], linewidths=0.5, colors='blue', linestyles='solid')
        cf_g  = ax.contourf(x_g, y_g, z_g, range(1,ref_max_g+1), cmap=cmap_g, norm=norm_g, alpha=0.5)
        ax_g  = fig.add_axes([0.91, 0.11, 0.02, 0.41])
        cb_g  = plt.colorbar(cf_g, cax=ax_g)
        cb_g.set_label(r'Num. excited g-modes', fontsize=8)

    ########################################
    # Annotations, and Legend
    ########################################
    tag = tags[el]
    ax.annotate(tag, xy=(0.92, 0.92), xycoords='axes fraction', color='black', fontsize=12)

    if style  == 'kiel': xy = (0.03, 0.92)
    if style  == 'hrd':  xy = (0.05, 0.30)
    if style  == 'shrd': xy = (0.05, 0.30)
    dic_annot = {'txt':r'$\ell=${0}'.format(el), 'xy':xy, 
                 'xycoords':'axes fraction', 'fontsize':12}
    pcm.add_annotation(ax, [dic_annot])
    if list_dic_annot: pcm.add_annotation(ax, list_dic_annot)

    # Fake labels
    ax.plot([], [], linestyle='solid', color='blue', lw=2, label='g-modes')
    ax.plot([], [], linestyle='dashed', color='red', lw=2, label='p-modes')
    # leg = ax.legend(loc=1, fontsize=10, fancybox=False, shadow=False, frameon=False, framealpha=0)

    ########################################
    # Optionally add few selected tracks on 
    # top of the instability strips
    ########################################
    if add_hist:
      for i_dic, dic in enumerate(list_hist):
        header = dic['header']
        M_ini  = header['initial_mass'][0]
        hist   = dic['hist']
        mass   = hist['star_mass']
        log_g  = hist['log_g']
        mxg    = np.argmax(log_g)
        x_hist = hist['log_Teff']
        if style == 'kiel': y_hist = hist['log_g']
        if style == 'hrd':  y_hist = hist['log_L']
        if style == 'shrd': y_hist = 4*hist['log_Teff'] - hist['log_g'] - 10.61
        x_hist = x_hist[mxg : ]
        y_hist = y_hist[mxg : ]

        ax.plot(x_hist, y_hist, linestyle='solid', color='black', lw=0.5, zorder=2)
        txt    = r'{0:0.1f}'.format(M_ini) + r'\,M$_\odot$'
        if style == 'kiel': y_offset = 0.03
        if style == 'hrd':  y_offset = -0.03
        if style == 'shrd': y_offset = -0.05
        ax.annotate(txt, xy=(x_hist[0], y_hist[0]+y_offset), xycoords='data', color='black', 
                    ha='left', va='top', fontsize=8)

    ax.set_xlim(logT_from, logT_to)
    ax.set_ylim(yaxis_from, yaxis_to)
    if el < 2: ax.set_xticklabels(())
    xticklabels = ax.get_xticks()

    # Write contour vertices to a file if requested
    if contour_file_prefix is not None:
      if el == 0:
        pass 
      else:
        contour_file_out = contour_file_prefix + '-el-{0}.txt'.format(el)
        contour = gen_contour(ref_pure_g_x[sort_g], ref_pure_g_y[sort_g], 
                              ref_pure_g[sort_g])
        write.save_contour_vertices(contour, contour_file_out)

  ########################################
  # Fix the bottom axis
  ########################################
  ax_tup[-1].set_xlabel(r'Effective Temperature $\log T_{\rm eff}$ [K]')
  ax_tup[-1].set_xticklabels(xticklabels)

  if style == 'kiel': ytitle = r'Surface gravity  $\,\log g$ [cm$^2$ sec$^{-1}$]'
  if style == 'hrd':  ytitle = r'Surface Luminosity  $\,\log(L/L_\odot)$'
  if style == 'shrd': ytitle = r'$\log (\mathcal{ L/L}_\odot)$;  $\,\mathcal{L}=T_{\rm eff}^4/g$'
  ax_tup[1].set_ylabel(ytitle)

  ########################################
  # Finalize and save the plot
  ########################################
  plt.savefig(file_out, dpi=400, transparent=True, facecolor=fig.get_facecolor(), edgecolor='none')
  print ' - journal_module: scatter_instability_strips: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def scatter_instability_strips_one_panel(recarr, el=0, list_hist=[], style='Kiel', 
                               logT_from=4.6, logT_to=4, yaxis_from=4.5, yaxis_to=3.2, 
                               tag=None, list_dic_annot=[], list_stars=[], contour_file_prefix=None, 
                               file_out=None):
  """
  This is copied and modified using plot_gyre.scatter_instability_strips_one_panel(). Refer to that function
  for more detailed documentation.
  @param contour_file_prefix: prefix for an ascii file to store contour vertices. the full name 
         is internally created after appending appropriate degree number to the file name.
  @type contour_file_prefix: string
  """
  if file_out is None: return
  n_hist   = len(list_hist)
  n_stars  = len(list_stars)
  add_hist = n_hist > 0
  add_stars= n_stars > 0

  style = style.lower()
  if style not in ['kiel', 'hrd', 'shrd']:
    logging.error('journal_module: scatter_instability_strips_one_panel: style={0} is not supported'.format(style.lower()))
    raise SystemExit, 'Error: journal_module: scatter_instability_strips_one_panel: style={0} is not supported'.format(style.lower())

  ########################################
  # Create the plot
  ########################################
  from matplotlib.colors import BoundaryNorm
  from matplotlib.ticker import MaxNLocator
  fig, ax = plt.subplots(1, figsize=(5, 5), dpi=400)
  plt.subplots_adjust(left=0.12, right=0.89, bottom=0.105, top=0.975, hspace=0.02)

  ########################################
  # Loop over all el values
  ########################################
  xvals         = recarr['log_Teff']
  if style == 'kiel': yvals = recarr['log_g']
  if style == 'hrd':  yvals = recarr['log_L']
  if style == 'shrd': yvals = 4*recarr['log_Teff'] - recarr['log_g'] - 10.61
  M_ini         = recarr['M_ini']
  sct_size      = get_scatter_size(M_ini)

  ref_max_p     = 0
  ref_max_g     = 0
  ref_max_h     = 0

  p_name        = 'n_p_{0}'.format(el)
  g_name        = 'n_g_{0}'.format(el)
  h_name        = 'n_h_{0}'.format(el)

  ref_pure_p    = recarr[p_name]
  ind_pure_p    = np.where(ref_pure_p > 0)[0]
  ref_pure_p    = ref_pure_p[ind_pure_p]
  ref_pure_p_x  = xvals[ind_pure_p]
  ref_pure_p_y  = yvals[ind_pure_p]
  sct_size_p    = sct_size[ind_pure_p]
  ref_max_p     = np.max(ref_pure_p)

  if el > 0:
    ref_pure_g    = recarr[g_name]
    ind_pure_g    = np.where(ref_pure_g > 0)[0]
    ref_pure_g    = ref_pure_g[ind_pure_g]
    ref_pure_g_x  = xvals[ind_pure_g]
    ref_pure_g_y  = yvals[ind_pure_g]
    sct_size_g    = sct_size[ind_pure_g]
    ref_max_g     = np.max(ref_pure_g)

  if el > 0:
    ref_hybrid    = recarr[h_name]
    ind_hybrid    = np.where(ref_hybrid > 0)[0]
    ref_hybrid    = ref_hybrid[ind_hybrid]
    ref_hybrid_x  = xvals[ind_hybrid]
    ref_hybrid_y  = yvals[ind_hybrid]
    sct_size_h    = sct_size[ind_hybrid]
    ref_max_h     = np.max(ref_hybrid)

  sort_p        = np.argsort(ref_pure_p)
  if el > 0:
    sort_g      = np.argsort(ref_pure_g)
    sort_h      = np.argsort(ref_hybrid)

  print ' - scatter_instability_strips_one_panel: el={0}: num excited p={0}, g={1}'.format(el, ref_max_p, ref_max_g)

  ########################################
  # Set Colormap and Normalize the color
  ########################################
  lev_factor  = 0.33
  low_lev_p   = -lev_factor * ref_max_p
  levels_p    = MaxNLocator(nbins=ref_max_p).tick_values(low_lev_p, ref_max_p+1)

  if el > 0:
    low_lev_g = -lev_factor * ref_max_g
    low_lev_h = -lev_factor * ref_max_h
    levels_g  = MaxNLocator(nbins=ref_max_g).tick_values(low_lev_g, ref_max_g+1)
    levels_h  = MaxNLocator(nbins=ref_max_h).tick_values(low_lev_h, ref_max_h+1)

  cmap_p      = plt.get_cmap('Reds')
  norm_p      = mpl.colors.Normalize(vmin=0, vmax=ref_max_p)
  if el > 0:
    cmap_g    = plt.get_cmap('Blues')
    norm_g    = mpl.colors.Normalize(vmin=0, vmax=ref_max_g)
    cmap_h    = plt.get_cmap('Greys')
    norm_h    = mpl.colors.Normalize(vmin=0, vmax=ref_max_h)

  # cmap_p.set_under('w', alpha=0)
  # cmap_g.set_under('w', alpha=0)
  fig.patch.set_facecolor('none')
  fig.patch.set_alpha(0.0)
  ax.patch.set_facecolor('none')
  ax.patch.set_alpha(0.0)

  tup_pos_pgh = get_colorbar_positions(ax)
  pos_ax_p    = tup_pos_pgh[0]
  pos_ax_h    = tup_pos_pgh[1]
  pos_ax_g    = tup_pos_pgh[2]

  ########################################
  # Scatter Plots
  ########################################
  # ax.scatter(ref_xvals, ref_yvals, marker='.', s=1, color='0.9', zorder=1) 

  # Pure p-modes
  # sc_p  = ax.scatter(ref_pure_p_x[sort_p], ref_pure_p_y[sort_p], 
  #                    c=ref_pure_p[sort_p], s=sct_size_p[sort_p], 
  sc_p  = ax.scatter(ref_pure_p_x, ref_pure_p_y, 
                     c=ref_pure_p, s=sct_size_p, 
                     cmap=cmap_p, vmin=0, vmax=ref_max_p, 
                     marker='s', edgecolor='none')
  ax_p  = fig.add_axes(pos_ax_p)
  tvl_p = [1, ref_max_p/4, ref_max_p/2, 3*ref_max_p/4, ref_max_p]   # ticks, values and ticklabels for p-modes
  cb_p  = plt.colorbar(sc_p, cax=ax_p, norm=norm_p, format='%d', ticks=tvl_p) #, values=tvl_p)
  cb_p.set_ticklabels([str(a) for a in tvl_p], update_ticks=True)
  cb_p.set_ticks(tvl_p)
  cb_p.set_label(r'p-modes', fontsize=8)
  cb_p.ax.tick_params(labelsize=10) 
  cb_p.set_clim(-lev_factor*ref_max_p, ref_max_p)
  cb_p.update_ticks()


  if el > 0 and ref_max_g > 0:
    # Pure g-modes
    sc_g  = ax.scatter(ref_pure_g_x[sort_g], ref_pure_g_y[sort_g], 
                       c=ref_pure_g[sort_g], s=sct_size_g[sort_g], 
                       cmap=cmap_g, vmin=0, vmax=ref_max_g, 
                       marker='s', edgecolor='none')
    ax_g  = fig.add_axes(pos_ax_g)
    tvl_g = [1, ref_max_g/4, ref_max_g/2, 3*ref_max_g/4, ref_max_g] # ticks, values and ticklabels for g-modes
    cb_g  = plt.colorbar(sc_g, cax=ax_g, norm=norm_g, format='%d', ticks=tvl_g)
    cb_g.set_ticks(tvl_g)
    cb_g.set_ticklabels([str(a) for a in tvl_g], update_ticks=True)
    cb_g.set_label(r'g-modes', fontsize=8)
    cb_g.ax.tick_params(labelsize=10) 
    cb_g.set_clim(-lev_factor*ref_max_g, ref_max_g)
    cb_g.update_ticks()


  if el > 0 and ref_max_h > 0:
    # Hybrids
    sc_h  = ax.scatter(ref_hybrid_x[sort_h], ref_hybrid_y[sort_h], 
                       c=ref_hybrid[sort_h], s=sct_size_h[sort_h], 
                       cmap=cmap_h, vmin=0, vmax=ref_max_h, 
                       marker='s', edgecolor='none', alpha=0.5)
    ax_h  = fig.add_axes(pos_ax_h)
    tvl_h = [1, ref_max_h/4, ref_max_h/2, 3*ref_max_h/4, ref_max_h]   # ticks, values and ticklabels for hybrids
    cb_h  = plt.colorbar(sc_h, cax=ax_h, norm=norm_h, format='%d', ticks=tvl_h)
    cb_h.set_ticks(tvl_h)
    cb_h.set_ticklabels([str(a) for a in tvl_h], update_ticks=True)
    cb_h.set_label(r'Hybrids', fontsize=8)
    cb_h.ax.tick_params(labelsize=10) 
    cb_h.set_clim(-lev_factor*ref_max_h, ref_max_h)
    cb_h.update_ticks()

  ########################################
  # Annotations, and Legend
  ########################################
  if tag is not None:
    ax.annotate(tag, xy=(0.92, 0.92), xycoords='axes fraction', color='black', fontsize=12)
  
  if style == 'kiel':
    xy = (0.03, 0.92)
  elif style == 'hrd':
    xy = (0.25, 0.21)
  elif style == 'shrd':
    xy = (0.25, 0.21)

  dic_annot = {'txt':r'$\ell=${0}'.format(el), 'xy':xy, 
               'xycoords':'axes fraction', 'fontsize':12}
  pcm.add_annotation(ax, [dic_annot])
  if list_dic_annot: pcm.add_annotation(ax, list_dic_annot)

  ########################################
  # Optionally add few selected stars on 
  # top of the instability strips
  ########################################
  if add_stars:
    dx = 0.03
    dy = 0.05
    if False: # Names on top right
      x0 = 0.82
      y0 = 0.93
    elif True: # Names on bottom left
      x0 = 0.03
      y0 = 0.36
    for i_star, star in enumerate(list_stars):
      if 'log_Teff' not in star.keys(): 
        print ' skipping star #:', i_star
        # print ' - Warning: journal_module: compare_IS_different_Fe_Ni: {0} has no log_Teff'.format(star['name'])
        continue
      star_log_Teff     = star['log_Teff']
      star_log_Teff_err = star['err_log_Teff_1s']
      star_log_g        = star['logg']
      star_log_g_err    = star['err_logg_1s']
      star_log_L        = star['log_L']
      star_log_L_err    = star['err_log_L_1s']
      if style == 'kiel': 
        ypos = star_log_g
        yerr = star_log_g_err
      elif style == 'shrd':
        ypos = 4 * star_log_Teff - star_log_g - 10.61
        yerr = 4 * star_log_Teff_err + star_log_g_err
      elif style == 'hrd':
        ypos = star_log_L
        yerr = star_log_L_err
      else:
        logging.warning('journal_module: scatter_instability_strips_one_panel: star distances are unknown; so HRD is not supported')
        print ' - Warning: journal_module: scatter_instability_strips_one_panel: star distances are unknown; so HRD is not supported'
        break

      if 'marker' in star.keys(): 
        marker = star['marker']
      else: 
        marker = 's'
      if 'mfc' in star.keys():
        mfc    = star['mfc']
      else:
        mfc    = 'blue'
      if 'mec' in star.keys():
        mec    = star['mec']
      else:
        mec    = 'blue'
      if 'ms' in star.keys():
        ms     = star['ms']
      else:
        ms     = 10
      if 'ecolor' in star.keys():
        ecolor = star['ecolor']
      else:
        ecolor = 'blue'

      ax.errorbar([star_log_Teff], [ypos], xerr=[star_log_Teff_err], yerr=[yerr],
                  ecolor=ecolor, elinewidth=1, marker=marker, mfc=mfc, mec=mec, 
                  ms=ms, mew=1, zorder=4)

      annot_clr = 'white'
      if mfc == 'white': annot_clr = 'blue'
      ax.annotate(str(i_star+1), xy=(star_log_Teff, ypos+0.005), xycoords='data', fontsize=9, 
                  color=annot_clr, ha='center', va='center', zorder=5)
      
      ax.annotate(str(i_star+1)+': ', xy=(x0, y0 - i_star*dy), xycoords='axes fraction', fontsize=8,
                  color='blue')
      ax.annotate(star['name'][0], xy=(x0+dx, y0 - i_star*dy), xycoords='axes fraction', fontsize=8,
                  color='blue')


  ########################################
  # Optionally add few selected tracks on 
  # top of the instability strips
  ########################################
  if add_hist:
    for i_dic, dic in enumerate(list_hist):
      header = dic['header']
      M_ini  = header['initial_mass'][0]
      hist   = dic['hist']
      mass   = hist['star_mass']
      log_g  = hist['log_g']
      Xc     = hist['center_h1']
      mxg    = np.argmax(log_g)
      maxXc  = np.max(Xc) - 0.001
      zams   = np.argmin(np.abs(Xc - maxXc))
      x_hist = hist['log_Teff']
      if style == 'kiel': y_hist = hist['log_g']
      if style == 'hrd':  y_hist = hist['log_L']
      if style == 'shrd': y_hist = 4*hist['log_Teff'] - hist['log_g'] - 10.61
      x_hist = x_hist[zams : ]
      y_hist = y_hist[zams : ]

      ax.plot(x_hist, y_hist, linestyle='solid', color='black', lw=0.5, zorder=2)
      txt    = r'{0:0.1f}'.format(M_ini) + r'\,M$_\odot$'
      ax.annotate(txt, xy=(x_hist[0], y_hist[0]-0.03), xycoords='data', color='black', 
                  ha='left', va='top', fontsize=8)

  # if el < 2: ax.set_xticklabels(())
  # xticklabels = ax.get_xticks()

  # Write contour vertices to a file if requested
  if contour_file_prefix is not None:
    if el == 0:
      pass 
    else:
      contour_file_out = contour_file_prefix + '-el-{0}.txt'.format(el)
      contour = gen_contour(ref_pure_g_x[sort_g], ref_pure_g_y[sort_g], 
                            ref_pure_g[sort_g])
      write.save_contour_vertices(contour, contour_file_out)

  ########################################
  # Fix the axis
  ########################################
  ax.set_xlim(logT_from, logT_to)
  ax.set_ylim(yaxis_from, yaxis_to)
  ax.set_xlabel(r'Effective Temperature $\log T_{\rm eff}$ [K]')
  # ax.set_xticklabels(xticklabels)

  if style == 'kiel': ytitle = r'Surface gravity  $\,\log g$ [cm$^2$ sec$^{-1}$]'
  if style == 'hrd':  ytitle = r'Surface Luminosity  $\,\log(L/L_\odot)$'
  if style == 'shrd': ytitle = r'$\log (\mathcal{ L/L}_\odot$);  $\,\mathcal{L}=T_{\rm eff}^4/g$'
  ax.set_ylabel(ytitle)

  ########################################
  # Finalize and save the plot
  ########################################
  plt.savefig(file_out, dpi=400, transparent=True, facecolor=fig.get_facecolor(), edgecolor='none')
  print ' - journal_module: scatter_instability_strips_one_panel: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def plot_contour_vertices(list_path_vertices_files, list_lbls=[], 
                          xaxis_from=4.7, xaxis_to=4.0, yaxis_from=4.5, yaxis_to=3, 
                          file_out=None):
  """
  Load the contour vertices from a list of vertices files (created by e.g. mesa_gyre.write.save_contour_vertices())
  and plot them.
  """
  if file_out is None: return 
  files   = list_path_vertices_files
  n_files = len(files)
  for f in files:
    if not os.path.exists(f):
      logging.error('journal_module: plot_contour_vertices: Input file: {0} does not exist'.format(f))
      raise SystemExit, 'Error: journal_module: plot_contour_vertices: Input file: {0} does not exist'.format(f)

  data = []
  for i, f in enumerate(files):
    with open(f, 'r') as r: lines = r.readlines()
    n    = len(lines)
    xarr = np.zeros(n)
    yarr = np.zeros(n)

    for j, line in enumerate(lines):
      line = line.rstrip('\r\n').split()
      x    = float(line[0])
      y    = float(line[1])
      xarr[j] = x 
      yarr[j] = y

    data.append((xarr, yarr))

  ########################################
  # Plot 
  ########################################
  fig, ax = plt.subplots(1, figsize=(6,4), dpi=300)
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97)
  colors = itertools.cycle(['black', 'blue', 'red', 'cyan'])
  linestyles = itertools.cycle(['solid', 'dashed', 'dashdot', 'dotted'])

  for tup in data:
    x   = tup[0]
    y   = tup[1]
    col = colors.next()
    ls  = linestyles.next()

    # ax.plot(x, y, color=col, linestyle=ls, lw=1.5)
    ax.scatter(x, y, marker='o', s=10, color=col)

  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)

  fig.patch.set_facecolor('none')
  fig.patch.set_alpha(0.0)
  ax.patch.set_facecolor('none')
  ax.patch.set_alpha(0.0)

  plt.savefig(file_out, dpi=300, transparent=True, facecolor=fig.get_facecolor(), edgecolor='none')
  print ' - journal_module: plot_contour_vertices: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def contour_of_lowest_level(x, y, z, list_lbls=[], xaxis_from=4.7, xaxis_to=4.0, 
                            yaxis_from=4.5, yaxis_to=3, xtitle='', ytitle='',
                            file_out=None):
  """

  """
  if file_out is None: return
  from matplotlib.mlab import griddata
  n_pts = 201

  ########################################
  # Plot 
  ########################################
  fig, ax = plt.subplots(1, figsize=(6,4), dpi=300)
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97)
  colors = itertools.cycle(['black', 'blue', 'red', 'cyan'])
  linestyles = itertools.cycle(['solid', 'dashed', 'dashdot', 'dotted'])

  ########################################
  # Data
  ########################################
  x_p   = np.linspace(np.min(x), np.max(x), n_pts, endpoint=True)
  y_p   = np.linspace(np.min(y), np.max(y), n_pts, endpoint=True)
  z_p   = griddata(x, y, z, x_p, y_p, interp='nn')
  
  z_lo  = np.min(z)
  z_hi  = np.max(z)
  # lev   = range(z_lo, z_hi+1)
  lev   = [1, 2, 5]
  print ' contour_of_lowest_level: Contour levels = ', lev
  c_p   = ax.contour(x_p, y_p, z_p, lev, linewidths=0.5, colors='grey', linestyles='solid')

  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)

  fig.patch.set_facecolor('none')
  fig.patch.set_alpha(0.0)
  ax.patch.set_facecolor('none')
  ax.patch.set_alpha(0.0)

  plt.savefig(file_out, dpi=300, transparent=True, facecolor=fig.get_facecolor(), edgecolor='none')
  print ' - journal_module: contour_of_lowest_level: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def compare_IS_different_Fe_Ni(list_recarr, el=1, style='kiel', which='hybrid', list_lbls=[], 
                               list_hist=[], list_stars=[], tag=None, xaxis_from=4.7, xaxis_to=4.0, 
                               yaxis_from=4.5, yaxis_to=3, file_out=None):
  """
  plot e.g. the instability strip (IS) of el=2 g-moes, for different datasets that are computed for 
  different values of Fe and Ni enhancement factors.
  """
  n_rec     = len(list_recarr)
  style     = style.lower()
  add_hist  = len(list_hist) > 0
  add_stars = len(list_stars) > 0

  if which not in ['p', 'g', 'hybrid']:
    logging.error('journal_module: compare_IS_different_Fe_Ni: wrong choice for which={0}'.format(which))
    raise SystemExit, 'journal_module: compare_IS_different_Fe_Ni: wrong choice for which={0}'.format(which)
  if which == 'p': kind = 'p'
  if which == 'g': kind = 'g'
  if which == 'hybrid': kind = 'h'
  col_name = 'n_{0}_{1}'.format(kind, el)
  if col_name not in list_recarr[0].dtype.names:
    logging.error('journal_module: compare_IS_different_Fe_Ni: failed to reconstruct column name:{0}'.format(col_name))
    raise SystemExit, 'journal_module: compare_IS_different_Fe_Ni: failed to reconstruct column name:{0}'.format(col_name)


  ########################################
  # Plot 
  ########################################
  fig, ax = plt.subplots(1, figsize=(6,4), dpi=300)
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97)
  # cmap = plt.get_cmap('Greys')
  # norm_h = mpl.colors.Normalize(vmin=1, vmax=ref_max_h)
  colors = itertools.cycle(['0.25', '0.5', '0.75'])

  ########################################
  # Data 
  ########################################
  for i_arr, arr in enumerate(list_recarr):
    xvals = arr['log_Teff']
    if style == 'kiel': yvals = arr['log_g']
    if style == 'hrd':  yvals = arr['log_L']
    if style == 'shrd': yvals = 4 * arr['log_Teff'] - arr['log_g']
    zvals = arr[col_name]  # this keeps the count of excited modes

    keep  = np.where((zvals > 0))[0]
    xvals = xvals[keep]
    yvals = yvals[keep]
    zvals = zvals[keep]

    col   = colors.next()
    if list_lbls:
      lbl = list_lbls[i_arr]
    else:
      lbl = ''

    ax.scatter(xvals, yvals, color=col, s=8, marker='s', edgecolor='none', alpha=0.5, 
               zorder=i_arr+1)

  ########################################
  # Optionally add few selected tracks on 
  # top of the instability strips
  ########################################
  if add_hist:
    for i_dic, dic in enumerate(list_hist):
      header = dic['header']
      M_ini  = header['initial_mass'][0]
      hist   = dic['hist']
      mass   = hist['star_mass']
      log_g  = hist['log_g']
      mxg    = np.argmax(log_g)
      x_hist = hist['log_Teff']
      if style == 'kiel': y_hist = hist['log_g']
      if style == 'hrd':  y_hist = hist['log_L']
      if style == 'shrd': y_hist = 4*hist['log_Teff'] - hist['log_g']
      x_hist = x_hist[mxg : ]
      y_hist = y_hist[mxg : ]

      ax.plot(x_hist, y_hist, linestyle='solid', color='black', lw=0.5, zorder=4)
      txt    = r'{0:0.1f}'.format(M_ini) + r'\,M$_\odot$'
      ax.annotate(txt, xy=(x_hist[0], y_hist[0]+0.03), xycoords='data', color='black', 
                  ha='left', va='top', fontsize=8)

  ########################################
  # Optionally add few selected stars on 
  # top of the instability strips
  ########################################
  if add_stars:
    dx = 0.03
    dy = 0.05
    if False: # Names on top right
      x0 = 0.82
      y0 = 0.93
    elif True: # Names on top left
      x0 = 0.03
      y0 = 0.95
    elif False: # Names on bottom left
      x0 = 0.03
      y0 = 0.36
    for i_star, star in enumerate(list_stars):
      if 'log_Teff' not in star.keys(): 
        print ' skipping star #:', i_star
        # print ' - Warning: journal_module: compare_IS_different_Fe_Ni: {0} has no log_Teff'.format(star['name'])
        continue
      star_log_Teff     = star['log_Teff']
      star_log_Teff_err = star['err_log_Teff_1s']
      star_log_g        = star['logg']
      star_log_g_err    = star['err_logg_1s']
      if style == 'kiel': 
        ypos = star_log_g
        yerr = star_log_g_err
      elif style == 'shrd':
        ypos = 4 * star_log_Teff - star_log_g
        yerr = 4 * star_log_Teff_err + star_log_g_err
      else:
        logging.warning('journal_module: compare_IS_different_Fe_Ni: star distances are unknown; so HRD is not supported')
        print ' - Warning: journal_module: compare_IS_different_Fe_Ni: star distances are unknown; so HRD is not supported'
        break

      if 'marker' in star.keys(): 
        marker = star['marker']
      else: 
        marker = 's'
      if 'mfc' in star.keys():
        mfc    = star['mfc']
      else:
        mfc    = 'blue'
      if 'mec' in star.keys():
        mec    = star['mec']
      else:
        mec    = 'blue'
      if 'ms' in star.keys():
        ms     = star['ms']
      else:
        ms     = 10
      if 'ecolor' in star.keys():
        ecolor = star['ecolor']
      else:
        ecolor = 'blue'

      ax.errorbar([star_log_Teff], [ypos], xerr=[star_log_Teff_err], yerr=[yerr],
                  ecolor=ecolor, elinewidth=1, marker=marker, mfc=mfc, mec=mec, 
                  ms=ms, mew=1, zorder=4)

      annot_clr = 'white'
      if mfc == 'white': annot_clr = 'blue'
      ax.annotate(str(i_star+1), xy=(star_log_Teff, ypos+0.005), xycoords='data', fontsize=9, 
                  color=annot_clr, ha='center', va='center', zorder=5)
      
      ax.annotate(str(i_star+1)+': ', xy=(x0, y0 - i_star*dy), xycoords='axes fraction', fontsize=8,
                  color='blue')
      ax.annotate(star['name'][0], xy=(x0+dx, y0 - i_star*dy), xycoords='axes fraction', fontsize=8,
                  color='blue')



  ########################################
  # Cosmetics and saving
  ########################################
  if tag is not None:
    ax.annotate(tag, xy=(0.03, 0.92), xycoords='axes fraction', color='black', fontsize=12)
  txt = r'$\ell=$' + str(el)
  # ax.annotate(txt, xy=(0.03, 0.85), xycoords='axes fraction', color='black', fontsize=12)
  if which == 'p': txt = 'p-modes'
  if which == 'g': txt = 'g-modes'
  if which == 'hybrid': txt = 'Hybrids'
  # ax.annotate(txt, xy=(0.03, 0.78), xycoords='axes fraction', color='black', fontsize=12)

  if list_lbls:
    colors = itertools.cycle(['0.25', '0.5', '0.75'])
    for i in range(3):
      col = colors.next()
      ax.scatter([], [], marker='s', s=50, color=col, label=list_lbls[i])
    if which == 'p': loc = 4
    if which == 'g': loc = 1
    if which == 'hybrid': loc = 4
    leg = ax.legend(loc=loc, fontsize=10, labelspacing=0.25, fancybox=False, shadow=False, 
                    frameon=False, framealpha=0, scatterpoints=1, scatteryoffsets=[0.5],
                    mode=None)

  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)
  ax.set_xlabel(r'Effective Temperature $\log T_{\rm eff}$ [K]')
  if style == 'kiel': ytitle = r'Surface gravity  $\,\log g$ [cm$^2$ sec$^{-1}$]'
  if style == 'hrd':  ytitle = r'Surface Luminosity  $\,\log(L/L_\odot)$'
  if style == 'shrd': ytitle = r'$\log \mathcal{ L/L}_\odot$; $\mathcal{L}=T_{\rm eff}^4/g$'
  ax.set_ylabel(ytitle)

  fig.patch.set_facecolor('none')
  fig.patch.set_alpha(0.0)
  ax.patch.set_facecolor('none')
  ax.patch.set_alpha(0.0)

  plt.savefig(file_out, dpi=300, transparent=True, facecolor=fig.get_facecolor(), edgecolor='none')
  print ' - journal_module: compare_IS_different_Fe_Ni: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def richest_pulsators_by_Xc(recarr, list_stars=[], columns=['n_g_1', 'n_g_2'], Xc_from=0.71, Xc_to=0.01, 
                            yaxis_from=4.5, yaxis_to=3, file_out=None):
  """
  show instability strips of all degree el, but if the low-order modes with 
         -n_pg_lo_ord <= n_pg <= n_pg_lo_ord 
  are found excited together with high-order g-modes, i.e. 
         n_pg <= n_pg_hi_ord.
  The two critical parameters, i.e. n_pg_hi_ord=-3 and n_pg_lo_ord=2 are internally defined in 
  jornal_tables. We use the two provided columns n_lo and n_hi. The position of a hybrid is where
  n_lo and n_hi are simultaneously greater than one.
  This is useful for detecting hybrid pulsators.
  """
  n_rec     = len(recarr)
  n_cols    = len(columns)
  alpha     = 1.0 / n_cols
  add_stars = len(list_stars) > 0

  available_columns = recarr.dtype.names
  col_names = ['n_lo', 'n_hi']
  for name in col_names:
    if name not in available_columns:
      logging.error('journal_module: richest_pulsators_by_Xc: failed to reconstruct column name:{0}'.format(col_name))
      raise SystemExit, 'journal_module: richest_pulsators_by_Xc: failed to reconstruct column name:{0}'.format(col_name)

  ########################################
  # Plot 
  ########################################
  fig, ax = plt.subplots(1, figsize=(6,4), dpi=300)
  plt.subplots_adjust(left=0.11, right=0.98, bottom=0.12, top=0.97)
  cmaps = itertools.cycle(['Blues', 'Reds', 'Greens', 'Yellows'])

  ########################################
  # Data 
  ########################################
  xref      = recarr['Xc']
  M_ini     = recarr['M_ini']
  yref      = np.log10(M_ini)
  sct_size  = get_scatter_size(M_ini)

  for i_col, col in enumerate(columns):
    cmap  = plt.get_cmap(cmaps.next())
    zvals = np.copy(recarr[col])
    max_modes = np.max(zvals)

    ind   = np.where(zvals > 0)[0]
    xvals = np.copy(xref[ind])
    yvals = np.copy(yref[ind])
    zvals = zvals[ind]

    ind   = np.argsort(zvals)
    xvals = xvals[ind]
    yvals = yvals[ind]
    zvals = zvals[ind]

    ax.scatter(xvals, yvals, c=zvals, s=8, marker='s', cmap=cmap,
               vmin=1, vmax=max_modes, edgecolor='none', alpha=alpha, zorder=i_col+1)

  ########################################
  # Optionally add few selected stars on 
  # top of the instability strips
  ########################################
  if add_stars:
    x0 = 0.80
    dx = 0.03
    y0 = 0.52
    dy = 0.055
    for i_star, star in enumerate(list_stars):
      if not star.has_key('Xc') or not star.has_key('mass'): 
        print ' skipping star #:', i_star
        # print ' - Warning: journal_module: richest_pulsators_by_Xc: {0} has no log_Teff'.format(star['name'])
        continue
      star_Xc           = star['Xc']
      star_Mini         = star['mass']

      ax.errorbar([star_Xc], [np.log10(star_Mini)], xerr=[0], yerr=[0],
                  ecolor='blue', elinewidth=1, marker='s', mfc='blue', mec='blue', 
                  ms=10, mew=1, zorder=4)
      ax.annotate(str(i_star+1), xy=(star_log_Teff, ypos+0.005), xycoords='data', fontsize=9, 
                  color='white', ha='center', va='center', zorder=5)
      # ax.annotate(str(i_star+1)+': ', xy=(x0, y0 - i_star*dy), xycoords='axes fraction', fontsize=8,
      #             color='blue')
      # ax.annotate(star['name'][0], xy=(x0+dx, y0 - i_star*dy), xycoords='axes fraction', fontsize=10,
      #             color='blue')



  ########################################
  # Cosmetics and saving
  ########################################
  # if list_lbls:
  #   colors = itertools.cycle(['0.25', '0.5', '0.75'])
  #   for i in range(3):
  #     col = colors.next()
  #     ax.scatter([], [], marker='s', s=50, color=col, label=list_lbls[i])
  #   leg = ax.legend(loc=2, fontsize=10, labelspacing=0.25, fancybox=False, shadow=False, 
  #                   frameon=False, framealpha=0, scatterpoints=1, scatteryoffsets=[0.5],
  #                   mode=None)

  ax.set_xlim(Xc_from, Xc_to)
  ax.set_ylim(yaxis_from, yaxis_to)
  ax.set_xlabel(r'Center Hydrogen $X_{\rm c}$')
  ax.set_ylabel(r'Initial Mass   $\;\log(M_{\rm ini}/M_\odot)$')

  fig.patch.set_facecolor('none')
  fig.patch.set_alpha(0.0)
  ax.patch.set_facecolor('none')
  ax.patch.set_alpha(0.0)

  plt.savefig(file_out, dpi=300, transparent=True, facecolor=fig.get_facecolor(), edgecolor='none')
  print ' - journal_module: richest_pulsators_by_Xc: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def compare_IS_hybrids_lo_hi_order(list_recarr, style='kiel', which='hybrid', list_lbls=[], 
                               list_hist=[], list_stars=[], list_BRITE=[], tag=None, 
                               xaxis_from=4.7, xaxis_to=4.0, 
                               yaxis_from=4.5, yaxis_to=3, file_out=None):
  """
  plot e.g. the instability strip (IS) of el=2 g-moes, for different datasets that are computed for 
  different values of Fe and Ni enhancement factors.
  """
  n_rec     = len(list_recarr)
  style     = style.lower()
  add_hist  = len(list_hist) > 0
  add_stars = len(list_stars) > 0
  add_BRITE = len(list_BRITE) > 0

  if which not in ['p', 'g', 'hybrid']:
    logging.error('journal_module: compare_IS_hybrids_lo_hi_order: wrong choice for which={0}'.format(which))
    raise SystemExit, 'journal_module: compare_IS_hybrids_lo_hi_order: wrong choice for which={0}'.format(which)
  # if which == 'p': kind = 'p'
  # if which == 'g': kind = 'g'
  # if which == 'hybrid': kind = 'h'
  # col_name = 'n_{0}_{1}'.format(kind, el)
  col_name = 'n_hyb'
  if col_name not in list_recarr[0].dtype.names:
    logging.error('journal_module: compare_IS_hybrids_lo_hi_order: failed to reconstruct column name:{0}'.format(col_name))
    raise SystemExit, 'journal_module: compare_IS_hybrids_lo_hi_order: failed to reconstruct column name:{0}'.format(col_name)

  ########################################
  # Plot 
  ########################################
  fig, ax = plt.subplots(1, figsize=(6,4), dpi=300)
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97)
  # cmap = plt.get_cmap('Greys')
  # norm_h = mpl.colors.Normalize(vmin=1, vmax=ref_max_h)
  colors = itertools.cycle(['0.25', '0.5', '0.75'])

  ########################################
  # Data 
  ########################################
  for i_arr, arr in enumerate(list_recarr):
    xvals = arr['log_Teff']
    if style == 'kiel': yvals = arr['log_g']
    if style == 'hrd':  yvals = arr['log_L']
    if style == 'shrd': yvals = 4 * arr['log_Teff'] - arr['log_g']
    zvals = arr[col_name]  # this keeps the count of excited modes

    keep  = np.where((zvals > 0))[0]
    xvals = xvals[keep]
    yvals = yvals[keep]
    zvals = zvals[keep]

    col   = colors.next()
    if list_lbls:
      lbl = list_lbls[i_arr]
    else:
      lbl = ''

    ax.scatter(xvals, yvals, color=col, s=8, marker='s', edgecolor='none', alpha=0.5, 
               zorder=i_arr+1)

  ########################################
  # Optionally add few selected tracks on 
  # top of the instability strips
  ########################################
  if add_hist:
    for i_dic, dic in enumerate(list_hist):
      header = dic['header']
      M_ini  = header['initial_mass'][0]
      hist   = dic['hist']
      mass   = hist['star_mass']
      log_g  = hist['log_g']
      mxg    = np.argmax(log_g)
      x_hist = hist['log_Teff']
      if style == 'kiel': y_hist = hist['log_g']
      if style == 'hrd':  y_hist = hist['log_L']
      if style == 'shrd': y_hist = 4*hist['log_Teff'] - hist['log_g']
      x_hist = x_hist[mxg : ]
      y_hist = y_hist[mxg : ]

      ax.plot(x_hist, y_hist, linestyle='solid', color='black', lw=0.5, zorder=4)
      txt    = r'{0:0.1f}'.format(M_ini) + r'\,M$_\odot$'
      ax.annotate(txt, xy=(x_hist[0], y_hist[0]+0.03), xycoords='data', color='black', 
                  ha='left', va='top', fontsize=8)

  ########################################
  # Optionally add few selected stars on 
  # top of the instability strips
  ########################################
  if add_stars:
    x0 = 0.80
    dx = 0.03
    y0 = 0.55
    dy = 0.055
    for i_star, star in enumerate(list_stars):
      if not star.has_key('log_Teff'): 
        print ' skipping star #:', i_star
        # print ' - Warning: journal_module: compare_IS_hybrids_lo_hi_order: {0} has no log_Teff'.format(star['name'])
        continue
      star_log_Teff     = star['log_Teff']
      star_log_Teff_err = star['err_log_Teff_1s']
      star_log_g        = star['logg']
      star_log_g_err    = star['err_logg_1s']
      if style == 'kiel': 
        ypos = star_log_g
        yerr = star_log_g_err
      elif style == 'shrd':
        ypos = 4 * star_log_Teff - star_log_g
        yerr = 4 * star_log_Teff_err + star_log_g_err
      else:
        logging.warning('journal_module: compare_IS_hybrids_lo_hi_order: star distances are unknown; so HRD is not supported')
        print ' - Warning: journal_module: compare_IS_hybrids_lo_hi_order: star distances are unknown; so HRD is not supported'
        break

      ax.errorbar([star_log_Teff], [ypos], xerr=[star_log_Teff_err], yerr=[yerr],
                  ecolor='blue', elinewidth=1, marker='s', mfc='blue', mec='blue', 
                  ms=10, mew=1, zorder=4)
      ax.annotate(str(i_star+1), xy=(star_log_Teff, ypos+0.005), xycoords='data', fontsize=10, 
                  color='white', ha='center', va='center', zorder=5)
      ax.annotate(str(i_star+1)+': ', xy=(x0, y0 - i_star*dy), xycoords='axes fraction', fontsize=10,
                  color='blue')
      ax.annotate(star['name'][0], xy=(x0+dx, y0 - i_star*dy), xycoords='axes fraction', fontsize=10,
                  color='blue')

  if add_BRITE:
    x0 = 0.65
    dx = 0.03
    y0 = 0.75
    dy = 0.055
    for i_star, star in enumerate(list_BRITE):
      if not star.has_key('log_Teff'): 
        print ' skipping star #:', i_star
        # print ' - Warning: journal_module: compare_IS_hybrids_lo_hi_order: {0} has no log_Teff'.format(star['name'])
        continue
      star_log_Teff     = star['log_Teff']
      star_log_Teff_err = star['err_log_Teff_1s']
      star_log_g        = star['logg']
      star_log_g_err    = star['err_logg_1s']
      if style == 'kiel': 
        ypos = star_log_g
        yerr = star_log_g_err
      elif style == 'shrd':
        ypos = 4 * star_log_Teff - star_log_g
        yerr = 4 * star_log_Teff_err + star_log_g_err
      else:
        logging.warning('journal_module: compare_IS_hybrids_lo_hi_order: star distances are unknown; so HRD is not supported')
        print ' - Warning: journal_module: compare_IS_hybrids_lo_hi_order: star distances are unknown; so HRD is not supported'
        break

      ax.errorbar([star_log_Teff], [ypos], xerr=[star_log_Teff_err], yerr=[yerr],
                  ecolor='red', elinewidth=1, marker='s', mfc='red', mec='red', 
                  ms=10, mew=1, zorder=4)
      ax.annotate(str(i_star+1), xy=(star_log_Teff, ypos+0.005), xycoords='data', fontsize=10, 
                  color='white', ha='center', va='center', zorder=5)
      ax.annotate(str(i_star+1)+': ', xy=(x0, y0 - i_star*dy), xycoords='axes fraction', fontsize=10,
                  color='red')
      ax.annotate(star['name'][0], xy=(x0+dx, y0 - i_star*dy), xycoords='axes fraction', fontsize=10,
                  color='red')


  ########################################
  # Cosmetics and saving
  ########################################
  if tag is not None:
    ax.annotate(tag, xy=(0.03, 0.92), xycoords='axes fraction', color='black', fontsize=12)
  txt = r'$0\leq\ell\leq2$'
  ax.annotate(txt, xy=(0.03, 0.85), xycoords='axes fraction', color='black', fontsize=12)
  if which == 'p': txt = 'p-modes'
  if which == 'g': txt = 'g-modes'
  if which == 'hybrid': txt = 'Hybrids'
  ax.annotate(txt, xy=(0.03, 0.78), xycoords='axes fraction', color='black', fontsize=12)

  if list_lbls:
    colors = itertools.cycle(['0.25', '0.5', '0.75'])
    for i in range(3):
      col = colors.next()
      ax.scatter([], [], marker='s', s=50, color=col, label=list_lbls[i])
    if which == 'p': loc = 4
    if which == 'g': loc = 1
    if which == 'hybrid': loc = 1
    leg = ax.legend(loc=1, fontsize=10, labelspacing=0.25, fancybox=False, shadow=False, 
                    frameon=False, framealpha=0, scatterpoints=1, scatteryoffsets=[0.5],
                    mode=None)

  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)
  ax.set_xlabel(r'Effective Temperature $\log T_{\rm eff}$ [K]')
  if style == 'kiel': ytitle = r'Surface gravity  $\,\log g$ [cm$^2$ sec$^{-1}$]'
  if style == 'hrd':  ytitle = r'Surface Luminosity  $\,\log(L/L_\odot)$'
  if style == 'shrd': ytitle = r'$\log \mathcal{ L/L}_\odot$; $\mathcal{L}=T_{\rm eff}^4/g$'
  ax.set_ylabel(ytitle)

  fig.patch.set_facecolor('none')
  fig.patch.set_alpha(0.0)
  ax.patch.set_facecolor('none')
  ax.patch.set_alpha(0.0)

  plt.savefig(file_out, dpi=300, transparent=True, facecolor=fig.get_facecolor(), edgecolor='none')
  print ' - journal_module: compare_IS_hybrids_lo_hi_order: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================




#=================================================================================================================
# I/O
# The following routines are intended to interact with the ASCII tables that are produced or read from the 
# GYRE output HDF5 files. Thus, they are not for plotting purposes, but work perfectly when integrated with
# the plotting routines.
#=================================================================================================================



#=================================================================================================================
def read_mode_table(filename):
  """

  """
  if not os.path.exists(filename):
    logging.error('journal_tables: read_mode_table: {0} does not exist'.format(filename))
    raise SystemExit, 'journal_tables: read_mode_table: {0} does not exist'.format(filename)

  with open(filename, 'r') as r: lines = r.readlines()

  junk   = lines.pop(0).rstrip('\r\n').split()
  names  = ['row', 'M_ini', 'M_star', 'R_star', 'L_star', 'Xc', 'log_Teff', 'log_g', 'log_L',
            'n_p_0', 'n_p_1', 'n_g_1', 'n_h_1', 'n_p_2', 'n_g_2', 'n_h_2', 'n_lo', 'n_hi', 'n_hyb']

  # formats is used to define varable types and also later as a function to treat string values
  formats= [int, float, float, float, float, float, float, float, float,
                  int, int, int, int, int, int, int, int, int, int]
  dtype  = zip(names, formats)
  n_rows = len(lines)
  recarr = np.recarray((n_rows, ), dtype=dtype)
  sample = np.recarray((1, ), dtype=dtype)
  data   = []
  for i_row, row in enumerate(lines):
    row  = row.rstrip('\r\n').split()
    for j, val in enumerate(row):
      func = formats[j]
      name = names[j]
      sample[name] = func(val)
    recarr[i_row] = sample

  print ' - journal_tables: read_mode_table: successfully read {0} with {1} lines'.format(filename, n_rows)

  return recarr

#=================================================================================================================
def write_recarr_to_ascii(recarr, file_out=None):
  """
  This routine writes the whole recarr into an ascii file
  """
  n_rows  = len(recarr)
  names   = recarr.dtype.names
  formats = []
  row     = recarr[0]
  hdr     = ''
  for i, val in enumerate(row):
    name  = names[i]
    if isinstance(val, int): 
      hdr += '{0:>8s}'.format(name)
    elif isinstance(val, float):
      hdr += '{0:>16s}'.format(name)
    else:
      logging.error('Error: journal_tables: write_recarr_to_ascii: value has weird type')
      raise SystemExit, 'Error: journal_tables: write_recarr_to_ascii: value has weird type'

  lines = [hdr + '\n']
  for i_row, row in enumerate(recarr):
    line = ''
    for i_val, val in enumerate(row): 
      if isinstance(val, int): 
        line += '{0:>8d}'.format(val)
      if isinstance(val, float):
        line += '{0:>16.6e}'.format(val)
    lines.append(line + ' \n')

  with open(file_out, 'w') as w: w.writelines(lines)
  print ' - journal_tables: write_recarr_to_ascii: saved {0}\n'.format(file_out)

  return None 

#=================================================================================================================
def list_dic_to_recarray(list_dic):
  """
  This subroutine receives a list of GYRE output dictionaries, and creates a numpy record array from them.
  This is useful, then to tabulate, for much easier, faster and cleaner interaction with the grid
  For plottings, instead of reading the whole GYRE output every time, we only read the tabulated values.
  """
  n_dic        = len(list_dic)
  n_pg_lo      = 2
  n_pg_hi      = -3

  names        = ['row', 'M_ini', 'M_star', 'R_star', 'L_star', 'Xc', 'log_Teff', 'log_g', 'log_L',
                  'n_p_0', 'n_p_1', 'n_g_1', 'n_h_1', 'n_p_2', 'n_g_2', 'n_h_2', 'n_lo', 'n_hi', 'n_hyb']
  formats      = [int, float, float, float, float, float, float, float, float,
                  int, int, int, int, int, int, int, int, int, int]
  dtype        = zip(names, formats)
  recarr       = np.recarray((n_dic, ), dtype=dtype)
  row          = np.recarray((1, ), dtype=dtype)

  for i_dic, dic in enumerate(list_dic):
    row['row']          = i_dic
    row['M_ini']        = dic['M']
    row['M_star']       = dic['M_star'] / Msun
    row['R_star']       = dic['R_star'] / Rsun
    row['L_star']       = dic['L_star'] / Lsun
    row['Xc']           = dic['Xc']
    row['log_L']        = np.log10(row['L_star'])
    row['log_Teff']     = np.log10(5777.) + np.log10(row['L_star'])/4 - np.log10(row['R_star'])/2
    row['log_g']        = np.log10(G*Msun/np.power(Rsun, 2)) + np.log10(row['M_star']) - 2 * np.log10(row['R_star'])


    all_el              = dic['l']
    all_n_pg            = dic['n_pg']

    ind_el_0_p          = np.where(all_el == 0)[0]
    ind_el_1_p          = np.where((all_el == 1) & (all_n_pg >= 0))[0]
    ind_el_1_g          = np.where((all_el == 1) & (all_n_pg < 0))[0]
    ind_el_1_h_lo       = np.where((all_el == 1) & (all_n_pg >= -n_pg_lo) & 
                                   (all_n_pg <= n_pg_lo))[0]
    ind_el_1_h_hi       = np.where((all_el == 1) & (all_n_pg <= n_pg_hi))[0]
    ind_el_2_p          = np.where((all_el == 2) & (all_n_pg >= 0))[0]
    ind_el_2_g          = np.where((all_el == 2) & (all_n_pg < 0))[0]
    ind_el_2_h_lo       = np.where((all_el == 2) & (all_n_pg >= -n_pg_lo) & 
                                   (all_n_pg <= n_pg_lo))[0]
    ind_el_2_h_hi       = np.where((all_el == 2) & (all_n_pg <= n_pg_hi))[0]
    ind_lo_ord          = np.where((all_n_pg >= -n_pg_lo) & (all_n_pg <= n_pg_lo))[0]
    ind_hi_ord          = np.where(all_n_pg <= n_pg_hi)[0]

    if False:

      # For GYRE v. 4.2 and earlier
      freq_im             = np.imag(dic['freq'])
      freq_im_el_0        = freq_im[ind_el_0_p]
      freq_im_el_1_p      = freq_im[ind_el_1_p]
      freq_im_el_1_g      = freq_im[ind_el_1_g]
      freq_im_el_1_h_lo   = freq_im[ind_el_1_h_lo]
      freq_im_el_1_h_hi   = freq_im[ind_el_1_h_hi]
      freq_im_el_2_p      = freq_im[ind_el_2_p]
      freq_im_el_2_g      = freq_im[ind_el_2_g]
      freq_im_el_2_h_lo   = freq_im[ind_el_2_h_lo]
      freq_im_el_2_h_hi   = freq_im[ind_el_2_h_hi]
      freq_im_lo_ord      = freq_im[ind_lo_ord]
      freq_im_hi_ord      = freq_im[ind_hi_ord]

      ind_excited_el_0_p    = np.where(freq_im_el_0   <= 0)[0]
      ind_excited_el_1_p    = np.where(freq_im_el_1_p <= 0)[0]
      ind_excited_el_1_g    = np.where(freq_im_el_1_g <= 0)[0]
      ind_excited_el_1_h_lo = np.where(freq_im_el_1_h_lo <= 0)[0]
      ind_excited_el_1_h_hi = np.where(freq_im_el_1_h_hi <= 0)[0]
      ind_excited_el_2_p    = np.where(freq_im_el_2_p <= 0)[0]
      ind_excited_el_2_g    = np.where(freq_im_el_2_g <= 0)[0]
      ind_excited_el_2_h_lo = np.where(freq_im_el_2_h_lo <= 0)[0]
      ind_excited_el_2_h_hi = np.where(freq_im_el_2_h_hi <= 0)[0]

      ind_excited_lo_ord  = np.where(freq_im_lo_ord <= 0)[0]
      ind_excited_hi_ord  = np.where(freq_im_hi_ord <= 0)[0]

    else:

      # For GYRE v.4.3 and later
      W                 = dic['W']
      W_el_0            = W[ind_el_0_p]
      W_el_1_p          = W[ind_el_1_p]
      W_el_1_g          = W[ind_el_1_g]
      W_el_1_h_lo       = W[ind_el_1_h_lo]
      W_el_1_h_hi       = W[ind_el_1_h_hi]
      W_el_2_p          = W[ind_el_2_p]
      W_el_2_g          = W[ind_el_2_g]
      W_el_2_h_lo       = W[ind_el_2_h_lo]
      W_el_2_h_hi       = W[ind_el_2_h_hi]
      W_lo_ord          = W[ind_lo_ord]
      W_hi_ord          = W[ind_hi_ord]

      ind_excited_el_0_p    = np.where(W_el_0   >= 0)[0]
      ind_excited_el_1_p    = np.where(W_el_1_p >= 0)[0]
      ind_excited_el_1_g    = np.where(W_el_1_g >= 0)[0]
      ind_excited_el_1_h_lo = np.where(W_el_1_h_lo >= 0)[0]
      ind_excited_el_1_h_hi = np.where(W_el_1_h_hi >= 0)[0]
      ind_excited_el_2_p    = np.where(W_el_2_p >= 0)[0]
      ind_excited_el_2_g    = np.where(W_el_2_g >= 0)[0]
      ind_excited_el_2_h_lo = np.where(W_el_2_h_lo >= 0)[0]
      ind_excited_el_2_h_hi = np.where(W_el_2_h_hi >= 0)[0]

      ind_excited_lo_ord  = np.where(W_lo_ord >= 0)[0]
      ind_excited_hi_ord  = np.where(W_hi_ord >= 0)[0]


    # now enumerate the number of excited modes in each category
    n_p_0               = len(ind_excited_el_0_p)
    n_p_1               = len(ind_excited_el_1_p)
    n_g_1               = len(ind_excited_el_1_g)
    n_p_2               = len(ind_excited_el_2_p)
    n_g_2               = len(ind_excited_el_2_g)
    n_h_1_lo            = len(ind_excited_el_1_h_lo)
    n_h_1_hi            = len(ind_excited_el_1_h_hi)
    n_h_2_lo            = len(ind_excited_el_2_h_lo)
    n_h_2_hi            = len(ind_excited_el_2_h_hi)
    n_h_1               = 0
    n_h_2               = 0
    if n_h_1_lo > 0 and n_h_1_hi > 0: n_h_1 = n_h_1_lo + n_h_1_hi
    if n_h_2_lo > 0 and n_h_2_hi > 0: n_h_2 = n_h_2_lo + n_h_2_hi

    n_lo                = len(ind_excited_lo_ord)
    n_hi                = len(ind_excited_hi_ord)
    n_hyb               = 0
    if (n_lo > 0) & (n_hi > 0): n_hyb = n_lo + n_hi

    row['n_p_0']        = n_p_0
    row['n_p_1']        = n_p_1
    row['n_g_1']        = n_g_1
    row['n_h_1']        = n_h_1
    row['n_p_2']        = n_p_2
    row['n_g_2']        = n_g_2
    row['n_h_2']        = n_h_2
    row['n_lo']         = n_lo
    row['n_hi']         = n_hi
    row['n_hyb']        = n_hyb

    recarr[i_dic]       = row

  return recarr

#=================================================================================================================
#=================================================================================================================


"""
This module supports several operations for Bayesian model selection for asteroseismic observation of heat-driven
pulsators. In most parts of the documentation below, the following symbols are used:
D: data
M: models, or hypothesis
|: given
p: probability, or likelihood
"""

import sys, os, glob
import logging
import time
from multiprocessing import Pool, cpu_count
import numpy as np 
from mesa_gyre import stars, read, param_tools, chisq, chisq_multiproc

#=================================================================================================================
# Module constant parameters
Xc_decimals = 4
ln_Zero     = -999999.0        # To avoid illegal operation with logarithm of zero
Zero        = np.exp(ln_Zero)  # To avoid illegal operation with zero

#=================================================================================================================
def do_bayesian(path_repos, srch, dic_star, n_cpu=None):
  """
  Perform Bayesian inference of the observation, passed through dic_star, based on a grid of models that are stored
  in path_repos, and are searched in using the srch syntax.
  @param path_repos: Path to the repository where the files are stored. E.g. "/STER/mesa-gyre/HD50230/"
  @type path_repos: string
  @param srch: glob search syntax. E.g. "M05.50/gyre_out/*.h5"
  @type srch: string
  @param dic_star: observational star data returned e.g. by a call to mesa_gyre.stars module.
  @type dic_star: dictionary
  @return: dictionary with the following items:
     - dic_star: the input dic_star is readily passed back 
     - list_dic_h5: the list of dictionaries for the GYRE output information
     - prior_matrix: the matrix of the priors that shows the evolution of the prior per model per each iteration step
     - posterior_matrix: the matrix of the posteriors that shows the evolution of the posterior per model per each iteration step
  @rtype: dictionary
  """
  dic_bayes = {}
  list_dic_freq = dic_star['list_dic_freq']
  list_dic_freq = stars.check_obs_vs_model_freq_unit('Hz', list_dic_freq) # fix the unit
  dic_star['list_dic_freq']  = list_dic_freq
  dic_bayes['dic_star']      = dic_star

  h5_files = read.get_file_list(path=path_repos, srch=srch)
  n_h5     = len(h5_files)
  if n_h5 == 0:
    logging.error('bayesian: do_bayesian: No files found in {0} using "{1}" search string.'.format(path_repos, srch))
    raise SystemExit
  else:
    print ' - bayesian: do_bayesian: Found {0} input files'.format(n_h5)
  logging.info('bayesian: do_bayesian: Found {0} input GYRE h5 files'.format(n_h5))

  #--------------------------------
  # Read the whole input files using
  # multiprocessing
  #--------------------------------
  list_dic_h5 = read.multiproc_gyre_output(h5_files, n_cpu=n_cpu)
  dic_bayes['list_dic_h5']      = list_dic_h5

  #--------------------------------
  # Prepare the list of frequencies to begin the process
  #--------------------------------
  list_dic_freq = dic_star['list_dic_freq']
  n_obs_freq    = len(list_dic_freq)
  obs_freq      = np.array([ dic['freq'] for dic in list_dic_freq ])

  #--------------------------------
  # Initialize the vector of prior values 
  # for the first application of the Bayes' theorem
  #--------------------------------
  prior = get_initial_prior(n_h5)
  prior_matrix = np.zeros((n_obs_freq, n_h5))

  posterior_matrix = np.zeros((n_obs_freq, n_h5))

  #--------------------------------
  # Get logarithm of the likelihood
  # function for all input models.
  #--------------------------------
  log_likelihood_arr = multiproc_get_log_likelihood(dic_bayes, n_cpu=n_cpu)
  dic_bayes['log_likelihood'] = log_likelihood_arr
  print ' - bayesian: do_bayesian:'
  print '   log_likelihood: min={0:.2f}, max={1:.2f}'.format(np.min(log_likelihood_arr), np.max(log_likelihood_arr))

  #--------------------------------
  # Compute the posterior from the 
  # likelihood values
  #--------------------------------
  log_posterior = get_log_posterior(log_likelihood_arr)
  dic_bayes['log_posterior'] = log_posterior
  print '   log_posterior:  min={0:.2f}, max:{1:.2f}'.format(np.min(log_posterior), np.max(log_posterior))

  #--------------------------------
  # Loop over frequencies, and evaluate the
  # posterior, also updating the prior by
  # adding modes one after the other
  #--------------------------------
  if False:
    for i_f, mode in enumerate(list_dic_freq):
      print '\n   Adding mode {0}: freq={1:0.6f} ...'.format(i_f+1, mode['freq'])

      dic_star_updated = stars.filter_modes_by_number(dic_star, 0, i_f)

      prior_matrix[i_f, :] = prior[:]

      posterior = eval_posterior(dic_star_updated, list_dic_h5, prior)

      posterior_matrix[i_f, :] = posterior[:]

      prior = update_prior(posterior)

  # dic_bayes['prior_matrix']     = prior_matrix
  # dic_bayes['posterior_matrix'] = posterior_matrix

  logging.info('bayesian: do_bayesian: Successfully finished')

  return dic_bayes

#=================================================================================================================
def get_log_posterior(log_likelihood_arr):
  """
  The logarithm of the posterior probability distribution is evaluated by subtracting the log likelihood of the 
  most probable model from all log likelihood values. This ensures that the posterior for the best model equals
  unity.
  @param log_likelihood_arr: the array containing the log likelihood for each input model in the grid, given the 
     pulsation data, and their uncertainties
  @type log_likelihood_arr: ndarray
  @return: an array containing the log posterior. The order is identical to that of the input data.
  @rtype: ndarray
  """

  return log_likelihood_arr - np.max(log_likelihood_arr)

#=================================================================================================================
def eval_posterior(dic_star, list_dic_h5, prior):
  """
  Evaluate the posterior p(M|D) by using the Bayes' rule, with M standing for the models, and D standing for the data. 
  @param dic_star: star info as an input. The number of modes available in dic_star['list_dic_freq'] will be used
         to evaluate the likelihood of the data D being explained by input models p(D|M)
  @type dic_star: dictionary
  @param list_dic_h5: list of dictionaries with GYRE computations containing theoretical mode information
  @type list_dic_h5: list of dictionaries
  @param prior: The prior to start with.
  @type prior: ndarray
  @return: posterior p(M|D) for every input model 
  @rtype: ndarray
  """
  sys.exit('eval_posterior: Obsolete')
  likelihood = eval_likelihood(dic_star, list_dic_h5)

  marginal_likelihood = eval_marginal_likelihood(likelihood, prior)

  # to avoid dividing by very small or very large numbers, we do it in log scale first
  # log_posterior = np.log10(likelihood) + np.log10(prior) - np.log10(marginal_likelihood)
  posterior = likelihood * prior / marginal_likelihood
  # posterior = np.power(10.0, log_posterior)
  # print np.min(posterior), np.max(posterior)

  return posterior

#=================================================================================================================
def eval_likelihood(dic_star, list_dic_h5):
  """
  Evaluate the likelihood of the data D being represented by the given model M, i.e. p(D|M)
  We assume that the likelihood of each model is proportional to exp[-chi^2], where chi^2 is the chi-square for fitting
  the frequencies. This is done by calling the chisq_multiproc.submit() procedure.
  @param dic_star: full star information
  type dic_star: dictionary
  @param list_dic_h5: list of dictionaries with GYRE computations containing theoretical mode information
  @type list_dic_h5: list of dictionaries
  @return: likelihood for each modelling explaining the input seismic data
  @rtype: ndarray
  """
  sys.exit('eval_likelihood: Obsolete')

  list_dic_chisq = multiproc_get_chisq(list_dic_h5, dic_star, chisq_by='chisq_seism_freq',
          el=1, freedom=5, sigma=100, freq_unit='Hz', pg='g', consecutive=True, strict=False, 
          path_plots=None, n_cpu=8)

  list_chisq = np.array([dic['chisq_seism_freq'] for dic in list_dic_chisq])

  # return np.exp(-list_chisq)
  return 1.0/list_chisq

#=================================================================================================================
def eval_marginal_likelihood(likelihood, prior):
  """
  Evaluate the marginal likelihood p(D) of a given set of likelihood and prior values for a set of models.
  Here, p(D) = sum p(D|M) * p(M)
  @param likelihood: likelihood values for observation D being explained by model M, i.e. p(D|M)
  @type likelihood: ndarray
  @param prior: the prior distribution for the models, p(M)
  @type prior: ndarray
  @return: p(D) = sum (likelihood * prior) 
  @rtype: ndarray
  """

  return np.sum(likelihood * prior)

#=================================================================================================================
def get_initial_prior(n):
  """
  Initialize the prior before the first Baye's application, and set it to 1/n for each of the models, tu start with 
  a normalized uniform probability distribution function.
  @param n: The number of models available, or in other words, the number of hypothesis to be tested.
  @type n: integer
  @return: an array of n elements where the value of each element is 1/n. Thus, the sum of all priors is unity.
  @rtype: ndarray
  """
  if not isinstance(n, int):
    logging.error('bayesian: get_initial_prior: the input must be integer > 0')
    raise SystemExit
  if n == 0:
    logging.error('bayesian: get_initial_prior: the input must be > 0')
    raise SystemExit

  return np.ones(n) / float(n)

#=================================================================================================================
def update_prior(posterior):
  """
  At the end of each update loop, the prior must be updatd based on the values in posterior, so that it serves as the 
  new pior for the next step, or to serve as the final posterior.
  @param posterior: posterior values for all models.
  @type posterior: ndarray
  @return: a copy of the posterior is returned to serves as the new and updated priorrs.
  @rtype: ndarray
  """
  sys.exit('update_prior: Obsolete')
  
  return posterior[:]

#=================================================================================================================
def multiproc_get_chisq(list_input, dic_obs, chisq_by, consecutive, strict, pg, el, freedom, sigma, freq_unit, 
                        n_cpu=None, path_plots=None):
  """
  This function receives the whole required arguments to submit jobs to chisq_single_thread(), and collect
  them back.
  It is well adapted from an identical function called chisq_multiproc.submit()
  @param chisq_by: Specify which scheme to call chisq. Possible options are:
         - reduced_chisq_seism_freq:
           This only calls chisq.seismic_freq(), and is much faster. The good model from this will automatically
           produce a good fit to the period spacing.
         - reduced_chisq_seism_f_and_dP: Then call
           This calls period_spacing.gen_list_dic_dP(), period_spacing.update_dP_dip() and chisq.seismic_dP().
           Therefore, it takes much longer to run
  @type chisq_by: string
  @param pg: specify to compute chisquare for either 'p' or 'g' modes. The list_dic_freq in dic_obs will be 
         searched to use only those mode dictionaries whose 'pg' key has the value of either 'p' or 'g'.
         default='g'.
  @type pg: string
  @param consecutive: default=True; specify if the search for modes and their Chisquare evaluation has to be 
           performed for consecutive modes, or not. This is useful e.g. for strict search on g-modes showing
           a series of nearly euqally spaced spacings. This choice is passed as an argument to chisq_single_thread().
  @type consecutive: boolean
  @param strict: flag to limit the chi-square evaluation to those modes within the observed range (True), or be less 
       strict, and find the closest frequency, even if it lies well outside the observed frequency interval withing 
       "sigma" times the extrema of observed frequency list. default=True
  @type strict: boolean
  @return: list of models including their Chisquare values.
  @rtype: list of dictionaries
  """
  n_h5 = len(list_input)
  if n_h5 == 0:
    logging.error('bayesian: multiproc_get_chisq: Input list is empty')
    raise SystemExit, 'Error: bayesian: multiproc_get_chisq: Input list is empty'

    logging.info('multiproc_get_chisq: Update dic_obs with period spacing of each mode')
    list_dic_freq = dic_obs['list_dic_freq']
    list_dic_freq = stars.check_obs_vs_model_freq_unit(freq_unit, list_dic_freq) # fix the unit
    dic_obs['list_dic_freq'] = list_dic_freq
    # update the list of frequencies to include the periods, period spacings and their associated errors
    dic_obs       = stars.list_freq_to_list_dP(dic_obs, freq_unit=freq_unit)

  if n_cpu is None:
    n_cpu = cpu_count()
  else:
    n_cpu = min([n_cpu, cpu_count()])

  pool = Pool(processes=n_cpu)

  n_chopes = n_h5 / n_cpu + 1

  results = []
  for i_cpu in range(n_cpu):
    ind_from = i_cpu * n_chopes
    ind_to   = ind_from + n_chopes 
    if i_cpu == n_cpu - 1: ind_to = n_h5

    chop     = list_input[ind_from : ind_to]
    args     = (chop, dic_obs, chisq_by, i_cpu, el, freedom, sigma, pg, consecutive, strict, freq_unit)
    
    results.append(pool.apply_async( chisq_multiproc.chisq_single_thread, args=args )) 

  pool.close()
  pool.join()
  
  outputs = []
  for i_cpu in range(n_cpu): outputs += results[i_cpu].get()
  
  return outputs

#=================================================================================================================
def get_best_model(dic_bayes):
  """
  The best model is the one that maximizes the posterior PDF. This model is found, and the properties are printed 
  on the terminal screen 
  @param dic_bayes: full information from the Bayesian inference. This is passed e.g. from mesa_gyre.bayesian.do_bayesian()
         It can have the following keys:
         - prior_matrix: prior distribution for each iteration
         - posterior_matrix: posterior distribution for each iteration
         - list_dic_h5: list of dictionaries containing the data from GYRE outputs
  @type dic_bayes: dictionary
  @return: the best model (hypothesis) that maximizes the posterior PDF. It has the same keys as identical to what 
         is returned by read.read_multiple_gyre_files()  
  @rtype: dictionary.
  """
  list_dic_h5 = dic_bayes['list_dic_h5']
  posterior   = dic_bayes['log_posterior']
  ind_best    = np.argmax(posterior)
  dic_best    = list_dic_h5[ind_best] 
  best_file   = dic_best['filename']
  print ' - bayesian: get_best_model: Posterior={0:0.6f}'.format(np.max(posterior))
  print '   filename:{0}'.format(best_file)

  return dic_best

#=================================================================================================================
def marginalize(rec_bayes, wrt):
  """
  Marginalize the likelihood values with respect to the input model parameter passed by "wrt". E.g. wrt='M', or 'ov',
  etc. This is done by integrating/summing the likelihood over the whole models that vary in the parameter "wrt".
  This results into the reduction of the dimension of the input record array by one.
  @param rec_bayes: full information from the Bayesian inference. This is passed from a former call to e.g. 
         mesa_gyre.bayesian.dic_bayes_to_rec_arr() (in case of the first use), or by a previous call to the same function, 
         mesa_gyre.bayesian.marginalize(). The maximum possible (number and names of the) fields is when it is the first call 
         and no dimension reduction has occured yet. Then, it has 7 fields as documented in mesa_gyre.bayesian.dic_bayes_to_rec_arr().
         They are:
         - M
         - ov 
         - Z 
         - Xini 
         - logD 
         - Xc 
         - log_posterior: which is the final log(posterior) obtained after the whole updating process is finished 
           In case of the first call, the posterior is that resulting from iterative Bayesian updating procedure. Otherwise,
           it is resulted from previous marginalization.
  @type rec_bayes: numpy record array
  @param wrt: marginalize with respect to this parameter, hence wrt. This should be available as the model parameter, and
         is retrieved from the model filename by using param_tools.get_param_from_single_hist_filename(). Current 
         possibilities are:
         - M 
         - eta 
         - ov 
         - Z 
         - logD 
         - Xc
  @type wrt: string
  @return: an array containing the whole structure similar to rec_bayes with the following two changes/differences:
         1. with "wrt" dimension is now reduced, and the log_posterior is marginalized with respect to that, thus it will 
            have one column less than the input 
         2. the log_posterior column is updated after having integrated over wrt 
  @rtype: numpy record array
  """
  #--------------------------------
  # Examine the input
  #--------------------------------
  rec_names = rec_bayes.dtype.names
  if 'log_posterior' not in rec_names:
    logging.error('bayesian: marginalize: the "log_posterior" field is missing from the input fields')
    raise SystemExit
  if wrt not in rec_names:
    logging.error('bayesian: marginalize: the "{0}" field is missing from the input fields'.format(wrt))
    raise SystemExit

  log_posterior = rec_bayes['log_posterior']
  posterior     = np.exp(log_posterior)

  if len(rec_names) == 2:
    if len(rec_bayes) > 1:
      result = np.sum(posterior)
    else:
      result = posterior
    return np.core.records.fromarrays(np.array( [np.log(result)] ), dtype=[('log_posterior', 'f8')])

  #--------------------------------
  # manipulate the input dictionary
  #--------------------------------
  n_h5       = len(rec_bayes)
  # posterior = rec_bayes['posterior'][:]

  #--------------------------------
  # define two schemes; one with all model
  # parameters + posterior, and one excluding
  # wrt. Use them to create two record arrays
  #--------------------------------
  # template   = ['M', 'ov', 'Z', 'Xini', 'logD', 'Xc']
  template   = [name for name in rec_names if name != 'log_posterior']
  ind_wrt    = template.index(wrt)
  iter_keys  = [name for name in template if name != wrt]
  n_iter     = len(iter_keys)  # this is the dimension of the intergration to be carried out
  # prepare appropriate dtype for two numy record arrays
  dtype_post = [('log_posterior', 'f8')]
  # dtype_all  = [(key, 'f8') for key in template] + dtype_post # has all template fields and the posterior
  dtype_work = [(key, 'f8') for key in iter_keys] # all parameters excluding the wrt; posterior not used here
  dtype_out  = [(key, 'f8') for key in iter_keys] + dtype_post # for the output record array

  list_exclud= []
  # list_wrt   = np.zeros(n_h5)
  # list_wrt   = rec_bayes[wrt]

  #--------------------------------
  # Retrieve data, and construct sets,
  # and lists for the two record arrays
  #--------------------------------
  for i, row in enumerate(rec_bayes):

    exclud_vals = []
    for key in iter_keys: exclud_vals.append(row[key])
    list_exclud.append(exclud_vals)

  #--------------------------------
  # Create two record arrays
  #--------------------------------
  rec_work      = np.core.records.fromarrays(np.array(list_exclud).transpose(), dtype=dtype_work)

  if 'Xc' in iter_keys and Xc_decimals < 4:
    rec_work['Xc'] = np.round(rec_work['Xc'], decimals=Xc_decimals).copy()
  n_work        = len(rec_work)
  n_start       = n_work
  post_work     = posterior[:]
  wrt_work      = rec_bayes[wrt]

  #--------------------------------
  # Reduce one dimension corresponding
  # to wrt, and return the remaining
  # data block
  #--------------------------------
  list_out   = []
  i_step     = 0

  while n_work > 0:
    row      = rec_work[0].copy()
    # row_work = np.core.records.fromarrays(np.array( [row[key] for key in iter_keys]), dtype=dtype_work )
    row_work = row.view(np.recarray)
    # print 'row:       ', row.dtype.names
    # print 'iter_keys: ', iter_keys
    # print type(row)
    # row_work = row[iter_keys]
    # sys.exit()

    # ind      = [ i for i in range(n_work) if row == rec_work[i] ]
    ind      = np.where(row == rec_work)[0]
    allow_flag = np.ones(n_work, dtype=bool)
    allow_flag[ind] = False
    n_ind    = len(ind)

    # integral = np.sum( post_work[ind] * d_wrt )  
    integral = np.sum(post_work[ind])
    with np.errstate(divide='ignore'):
      new_row  = [row[key] for key in iter_keys] + [np.log(integral)]
    list_out.append(new_row)

    # Avoid repetition by removing those rows that are already integrated over.
    rec_work = rec_work[allow_flag]
    post_work= post_work[allow_flag]
    # rec_work = np.delete(rec_work, obj=ind, axis=0)
    # post_work= np.delete(post_work, obj=ind, axis=0)
    # wrt_work = np.delete(wrt_work, obj=ind, axis=0)
    n_work   = len(rec_work)
    i_step   += 1
    # print '   wrt={0}: step={1}, n_work={2}'.format(wrt, i_step, n_work)

  rec_out = np.core.records.fromarrays(np.array(list_out).transpose(), dtype=dtype_out)
  print '\n - bayesian: marginalize: returning fields: ', rec_out.dtype.names
  print '   Reducing w.r.t. {0}: {1} --> {2}'.format(wrt, n_start, len(rec_out))

  return rec_out

#=================================================================================================================
def dic_bayes_to_rec_arr(dic_bayes):
  """
  Convert the posterior and paramter information passed by dic_bayes into a numpy record array for fast access
  and manipulation.
  @param dic_bayes: full information from the Bayesian inference. This is passed e.g. from mesa_gyre.bayesian.do_bayesian()
         It can have the following keys:
         - log_posterior: posterior distribution for each iteration
         - list_dic_h5: list of dictionaries containing the data from GYRE outputs
  @type dic_bayes: dictionary
  @return: a structured array containing the following fields:
         - M
         - ov 
         - Z 
         - Xini 
         - logD 
         - Xc 
         - posterior: which is the final posterior obtained after the whole updating process is finished 
         This record array can be later used for marginalization on any of the dimensions.
  @rtype: numpy record array
  """
  #--------------------------------
  # manipulate the input dictionary
  #--------------------------------
  # posterior_matrix = dic_bayes['posterior_matrix']
  # posterior        = posterior_matrix[-1, :]
  log_posterior    = dic_bayes['log_posterior']
  list_dic_h5      = dic_bayes['list_dic_h5']
  n_h5             = len(list_dic_h5)
  list_filenames   = [dic['filename'] for dic in list_dic_h5]
  list_dic_param   = [param_tools.get_param_from_single_gyre_filename(filename) for filename in list_filenames]

  #--------------------------------
  # Prepare dtypes
  #--------------------------------
  template   = get_list_available_parameters()
  n_iter     = len(template)  # 
  dtype_post = [('log_posterior', 'f8')]
  dtype_out  = [(key, 'f8') for key in template] + dtype_post # for the output record array

  list_out   = []
  for i, dic_param in enumerate(list_dic_param):
    vals = []
    for key in template: vals.append(dic_param[key])
    vals.append(log_posterior[i])
    list_out.append(vals)

  #--------------------------------
  # Create two record arrays
  #--------------------------------
  rec_out      = np.core.records.fromarrays(np.array(list_out).transpose(), dtype=dtype_out)
  if Xc_decimals < 4:
    rec_out['Xc'] = np.round(rec_out['Xc'], decimals=Xc_decimals)

  return rec_out

#=================================================================================================================
def marginalize_to_2d(rec_bayes, list_keep):
  """
  To marginalize the posterior with respect to all parameters except the two passed by the list: "list_keep".
  As input, rec_bayes may have several columns (associated with several parameters), but as output, there are only 
  three columns availbe: (a) list_keep[0], (b) list_keep[1], and (c) log_posterior. Valid keys for list_keep can be picked 
  from get_list_available_parameters().
  @param rec_bayes: full information from the Bayesian inference. This is passed from a former call to e.g. 
         mesa_gyre.bayesian.dic_bayes_to_rec_arr() (in case of the first use), or by a previous call to the same function, 
         mesa_gyre.bayesian.marginalize(). The maximum possible (number and names of the) fields is when it is the first call 
         and no dimension reduction has occured yet. Then, it has 7 fields as documented in mesa_gyre.bayesian.dic_bayes_to_rec_arr().
         They are:
         - M
         - ov 
         - Z 
         - Xini 
         - logD 
         - Xc 
         - log_posterior: which is the final log(posterior) obtained after the whole updating process is finished 
           In case of the first call, the posterior is that resulting from iterative Bayesian updating procedure. Otherwise,
           it is resulted from previous marginalization.
  @type rec_bayes: numpy record array
  @param list_keep: list of the two parameters that will not be marginalized. list_keep is restricted to have only two elements.
  @type list_keep: list of two strings
  @return: an array with three fields: (a) two with the names passed through list_keep, and (b) log_posterior
  @rtype: numpy record array
  """
  rec_marg = rec_bayes.copy()
  names    = rec_marg.dtype.names 
  if list_keep[0] not in names:
    logging.error('bayesian: marginalize_to_2d: {0} not among: {1}'.format(list_keep[0], names))
    raise SystemExit, 'Error: bayesian: marginalize_to_2d: {0} not among: {1}'.format(list_keep[0], names)
  if list_keep[1] not in names:
    logging.error('bayesian: marginalize_to_2d: {0} not among: {1}'.format(list_keep[1], names))
    raise SystemExit, 'Error: bayesian: marginalize_to_2d: {0} not among: {1}'.format(list_keep[1], names)
  keys_to_marg = [key for key in names if key != list_keep[0] and key != list_keep[1] and key != 'log_posterior'] 
  if set(keys_to_marg) == set(list_keep): return rec_marg

  logging.info('bayesian: marginalize_to_2d: list_keep={0}, keys_to_marg={1}'.format(list_keep, keys_to_marg))

  for i_marg, wrt in enumerate(keys_to_marg):
    # print ' - bayesian: marginalize_to_2d: available keys before marginalizing: ', rec_marg.dtype.names
    rec_marg   = marginalize(rec_marg, wrt=wrt)

  if len(rec_marg.dtype.names) != 3:
    logging.error('bayesian: marginalize_to_2d: Something went wrong with previous steps!')
    raise SystemExit, 'Error: bayesian: marginalize_to_2d: Something went wrong with previous steps!'

  return rec_marg

#=================================================================================================================
def marginalize_to_1d(rec_bayes, keep):
  """
  To marginalize the posterior with respect to all parameters, except "keep", you can use this function.
  As input, rec_bayes may have several columns (associated with several parameters), but as output, there are only 
  two columns availbe: (a) keep, and (b) log_posterior. Valid keys for keep can be picked from get_list_available_parameters().
  @param rec_bayes: full information from the Bayesian inference. This is passed from a former call to e.g. 
         mesa_gyre.bayesian.dic_bayes_to_rec_arr() (in case of the first use), or by a previous call to the same function, 
         mesa_gyre.bayesian.marginalize(). The maximum possible (number and names of the) fields is when it is the first call 
         and no dimension reduction has occured yet. Then, it has 7 fields as documented in mesa_gyre.bayesian.dic_bayes_to_rec_arr().
         They are:
         - M
         - ov 
         - Z 
         - Xini 
         - logD 
         - Xc 
         - log_posterior: which is the final log(posterior) obtained after the whole updating process is finished 
           In case of the first call, the posterior is that resulting from iterative Bayesian updating procedure. Otherwise,
           it is resulted from previous marginalization.
  @type rec_bayes: numpy record array
  @param keep: The only parameter (among the columns of rec_bayes) to retain, and not marginalize. The rest are marginalized.
  @type keep: string 
  @return: an array with two fields: (a) the one with the name passed as keep, and (b) log_posterior
  @rtype: numpy record array
  """
  rec_marg = rec_bayes.copy()
  names = rec_marg.dtype.names 
  if keep not in names:
    logging.error('bayesian: marginalize_to_1d: {0} not among: {1}'.format(keep, names))
    raise SystemExit, 'Error: bayesian: marginalize_to_1d: {0} not among: {1}'.format(keep, names)
  keys_to_marg = [key for key in names if key != keep]
  n_marg       = len(keys_to_marg)

  for i_marg, wrt in enumerate(keys_to_marg):
    rec_marg   = marginalize(rec_marg, wrt=wrt)

  return rec_marg

#=================================================================================================================
def marginalize_to_scaler(rec_bayes):
  """
  Iterate over all dimensions of the input array, and reduce the dimensions one after the other, to end up with a 
  single scalar being the integral over all parameter space of the posterior times the values of each dimension to 
  be reduced.
  @param rec_bayes: full information from the Bayesian inference. This is passed from a former call to e.g. 
         mesa_gyre.bayesian.dic_bayes_to_rec_arr() (in case of the first use), or by a previous call to the same function, 
         mesa_gyre.bayesian.marginalize(). The maximum possible (number and names of the) fields is when it is the first call 
         and no dimension reduction has occured yet. Then, it has 7 fields as documented in mesa_gyre.bayesian.dic_bayes_to_rec_arr().
         They are:
         - M
         - ov 
         - Z 
         - Xini 
         - logD 
         - Xc 
         - log_posterior: which is the final log(posterior) obtained after the whole updating process is finished 
           In case of the first call, the posterior is that resulting from iterative Bayesian updating procedure. Otherwise,
           it is resulted from previous marginalization.
  @type rec_bayes: numpy record array
  """
  list_params = get_list_available_parameters()[::-1]
  rec_params  = dic_bayes_to_rec_arr(dic_bayes)[:]
  for i, param in enumerate(list_params):
    print ' - marginalize_to_scaler: Step: {0}'.format(i+1)
    rec_bayes = marginalize(rec_bayes, wrt=param)

  print ' - bayesian: marginalize_to_scaler: integral={0}'.format(rec_bayes['log_posterior'])

  return rec_bayes

#=================================================================================================================
def get_list_available_parameters():
  """
  Based on the free parameters of the grid we are dealing with, there are different available keys for 
  marginalization. They can serve as keys to various record arrays passed through many procedure. In the current version, 
  we return these parameter names: 'M', 'ov', 'Z', 'Xini', 'logD', 'Xc'. In the future, 'eta' can also be included.
  @return: ['M', 'ov', 'Z', 'Xini', 'logD', 'Xc']
  @rtype: list of strings
  """

  return ['M', 'ov', 'Z', 'Xini', 'logD', 'Xc']

#=================================================================================================================
def get_d_wrt(wrt):
  """
  Marginalization and evaluation of the evidence, requires iterative reduction of dimensions, each time with respect to 
  one of the dimensions, hence "wrt". For a rectanbular grid, and "assuming that the step in all dimensions is fixed", then 
  you can use a fixed integration increment d_wrt based on the input wrt. This routine returns such pre-defined integration 
  increments for wrt, and should be adapted everytime based on the grid specifications
  @param wrt: marginalize with respect to this parameter, hence wrt. This should be available as the model parameter, and
         is retrieved from the model filename by using param_tools.get_param_from_single_hist_filename(). Current 
         possibilities are:
         - M 
         - eta 
         - ov 
         - Z 
         - logD 
         - Xc
  @type wrt: string
  @return: the integration increment, d_wrt
  @rtype: float
  """
  if wrt == 'M':    d_wrt = 0.10
  if wrt == 'ov':   d_wrt = 0.003
  if wrt == 'Z':    d_wrt = 0.002
  if wrt == 'Xini': d_wrt = 0.01
  if wrt == 'logD': d_wrt = 0.25
  if wrt == 'Xc':   d_wrt = 0.002

  return d_wrt

#=================================================================================================================
def get_log_likelihood_gaussian_all_modes(dic_star, list_dic_h5):
  """
  Retrieve the sum of the logarithmic likelihood for fitting individual frequencies
  """

  return None

#=================================================================================================================
def get_chisq_freq_one_mode(freq, freq_err, nu_n_el):
  """
  Evaluate the simple chi-square computation of mode with its 1-sigma uncertainty against a list of theoretical modes.
  @param freq: observed frequency
  @type freq: float 
  @param freq_err: the observed frequency uncertainty
  @type freq_err: float 
  @param nu_n_el: theoretical frequency. It can be either one float number or a vector of 1d frequencies associated to 
     a GYRE model.
  @type nu_n_el: float, or ndarray
  @return: chi-square value of fitting one frequency with one or a list of frequencies
  @rtype: float
  """

  return (freq - nu_n_el)**2 / freq_err**2

#=================================================================================================================
def get_log_likelihood_gaussian_one_mode(freq, freq_err, nu_n_el):
  """
  Compute the log_likelihood for a single observed mode (given its 1-sigma uncertainty) and a theoretical frequency.
  @param freq: observed frequency
  @type freq: float 
  @param freq_err: the observed frequency uncertainty
  @type freq_err: float 
  @param nu_n_el: theoretical frequency. It can be either one float number or a vector of 1d frequencies associated to 
     a GYRE model.
  @type nu_n_el: float, or ndarray
  @return: logarithm of the likelihood function
  @rtype: float, or ndarray
  """

  return -np.log(2.*np.pi)/2. - np.log(freq_err) - (freq - nu_n_el)**2.0 / (2. * freq_err**2.0)

#=================================================================================================================
def get_P_el(dic_bayes, dic_h5):
  """
  P(\ell) is a filter function whose return value depends on the degree, \ell, of the observed mode. It is a likelihood 
  of mode visibility, thus, should lie in the range [0, 1].
  For identified modes with degree \ell_0, then P_ell is a Kronecker delta function: P_ell(\ell) = \delta(\ell-\ell_0).
  For ambigious modes, P_ell can either be chosen as an average of all possible \ell values (e.g. 1./3 if we only expect
  three \ell degrees), of can scale as the mode visibility (e.g. partial cancellation effect in Aerts et al. 2010, 
  Fig. 1.5). 
  @param dic_bayes: dictionary with the following items:
     - dic_star: the input dic_star is readily passed back 
     - list_dic_h5: the list of dictionaries for the GYRE output information
     - prior_matrix: the matrix of the priors that shows the evolution of the prior per model per each iteration step
     - posterior_matrix: the matrix of the posteriors that shows the evolution of the posterior per model per each iteration step
  @type dic_bayes: dictionary
  @param dic_h5: a dictionary containing the GYRE output info for a single model (i.e. file). For more information, follow the
     documentation in read.read_multiple_gyre_files().
  @type dic_h5: dictionary
  @return: 2-d array giving P_ell for each mode. The values lie in the range [0, 1]. It has the dimension of 
         num_ell x n_modes.
  @rtype: ndarray
  """
  dic_star      = dic_bayes['dic_star']
  list_dic_freq = dic_star['list_dic_freq']
  n_obs         = len(list_dic_freq)
  if n_obs == 0:
    logging.error('bayesian: get_P_el: This star has no recorded mode in list_dic_freq')
    raise SystemExit, 'Error: bayesian: get_P_el: This star has no recorded mode in list_dic_freq'

  ell           = dic_h5['l']
  set_ell       = set(ell)
  n_ell         = len(set_ell)
  n_theo        = len(ell)
  ones          = np.ones(n_theo)
  zeros         = np.zeros(n_theo)

  P_ell         = np.zeros((n_obs, n_theo)) #+ Zero
  for i_dic, dic in enumerate(list_dic_freq):
    if 'l' in dic.keys():
      obs_ell   = dic['l'] 
      P_ell[i_dic, 0:n_theo] = np.where( ell == obs_ell, ones, zeros )
    else:
      P_ell[i_dic, 0:n_theo] = 1.0 

  return P_ell / np.sum(P_ell)

#=================================================================================================================
def get_P_n(dic_bayes, dic_h5):
  """
  P(n) is a filter function whose return value depends on the mode nature, i.e. being g-mode or p-mode. It is a likelihood
  of mode nature, thus, should lie in the range [0, 1].
  For identified high-order g-modes or low-order p-modes, the returned value is 1.0. However, for ambigious modes which 
  we are unsure about their being p- or g-mode, the returned value is 1./num_ell.
  @param dic_bayes: dictionary with the following items:
     - dic_star: the input dic_star is readily passed back 
     - list_dic_h5: the list of dictionaries for the GYRE output information
     - prior_matrix: the matrix of the priors that shows the evolution of the prior per model per each iteration step
     - posterior_matrix: the matrix of the posteriors that shows the evolution of the posterior per model per each iteration step
  @type dic_bayes: dictionary
  @param dic_h5: a dictionary containing the GYRE output info for a single model (i.e. file). For more information, follow the
     documentation in read.read_multiple_gyre_files().
  @type dic_h5: dictionary
  @return: 2-d array giving P_n for each mode (rows) and theoretical mode (column). The values lie in the range [0, 1]. 
           It has the dimension of num_ell x n_modes.
  @rtype: ndarray
  """
  dic_star      = dic_bayes['dic_star']
  list_dic_freq = dic_star['list_dic_freq']
  n_obs         = len(list_dic_freq)
  if n_obs == 0:
    logging.error('bayesian: get_P_n: This star has no recorded mode in list_dic_freq')
    raise SystemExit, 'Error: bayesian: get_P_n: This star has no recorded mode in list_dic_freq'

  n_pg          = dic_h5['n_pg']
  n_theo        = len(n_pg)
  ones          = np.ones(n_theo)
  zeros         = np.zeros(n_theo)

  P_n = np.zeros((n_obs, n_theo)) + Zero
  for i_dic, dic in enumerate(list_dic_freq):
    if not dic.has_key('pg'):
      logging.error('bayesian: get_P_n: mode with freq={0} has no "pg" information'.format(dic['freq']))
      raise SystemExit, 'Error: bayesian: get_P_n: mode with freq={0} has no "pg" information'.format(dic['freq'])

    if dic['pg'] == 'g':
      ind_g      = np.where( n_pg < 0 )[0]
      n_g        = len(ind_g)
      P_n[i_dic, ind_g]  = 1.0 / n_g
      # P_n[i_dic, 0:n_theo] = np.where( n_pg < 0, ones, zeros )
    if dic['pg'] == 'p':
      ind_p      = np.where( n_pg >= 0 )[0]
      n_p        = len(ind_p)
      P_n[i_dic, ind_p]  = 1.0 / n_p
      # P_n[i_dic, 0:n_theo] = np.where( n_pg >= 0, ones, zeros )
    if dic['pg'] == 'pg':
      P_n[i_dic, 0:n_theo] = 1.0 / n_theo

  return P_n

#=================================================================================================================
def get_P_n_el(dic_bayes, dic_h5):
  """
  P_n_el is the filter function that contains the matching information for both mode radial order, hence P_n, and also 
  the mode degree P_ell. It is generated by an element-by-element multiplication of P_n and P_ell.
  For more information, read get_P_n() and get_P_el().
  @param dic_bayes: dictionary with the following items:
     - dic_star: the input dic_star is readily passed back 
     - list_dic_h5: the list of dictionaries for the GYRE output information
     - prior_matrix: the matrix of the priors that shows the evolution of the prior per model per each iteration step
     - posterior_matrix: the matrix of the posteriors that shows the evolution of the posterior per model per each iteration step
  @type dic_bayes: dictionary
  @param dic_h5: a dictionary containing the GYRE output info for a single model (i.e. file). For more information, follow the
     documentation in read.read_multiple_gyre_files().
  @type dic_h5: dictionary
  @return: 2-d array giving P_n for each mode (rows) and theoretical mode (column). The values lie in the range [0, 1]. 
           It has the dimension of num_ell x n_modes.
  @rtype: ndarray
  """

  return get_P_n(dic_bayes, dic_h5) * get_P_el(dic_bayes, dic_h5)

#=================================================================================================================
def get_log_likelihood_one_model(dic_bayes, dic_h5):
  """
  Evaluate log_likelihood for all observed modes of the star. Filteration is carried out by calling get_P_n_el().
  From the possible list of modes that explain the frequency, only one is picked that "maximizes" the likelihood function.
  @param dic_bayes: dictionary with the following items:
     - dic_star: the input dic_star is readily passed back 
     - list_dic_h5: the list of dictionaries for the GYRE output information
     - prior_matrix: the matrix of the priors that shows the evolution of the prior per model per each iteration step
     - posterior_matrix: the matrix of the posteriors that shows the evolution of the posterior per model per each iteration step
  @type dic_bayes: dictionary
  @param dic_h5: a dictionary containing the GYRE output info for a single model (i.e. file). For more information, follow the
     documentation in read.read_multiple_gyre_files().
  @type dic_h5: dictionary
  @return: the log likelihood value for a single model 
  @rtype: float
  """
  filename = dic_h5['filename']
  logging.info('start: {0}'.format(filename))
  P_n_el   = get_P_n_el(dic_bayes, dic_h5)
  dic_star = dic_bayes['dic_star']
  list_dic_freq = dic_star['list_dic_freq']

  #--------------------------------
  # Cumulative Log Likelihood, after
  # marginalizing w.r.t. n and ell
  #--------------------------------
  cum_log_L_P_n_el = 0.0
  for i_obs, dic_obs_mode in enumerate(list_dic_freq):
    obs_freq        = dic_obs_mode['freq']         # this is a single float
    obs_freq_err    = dic_obs_mode['freq_err']     # this is a single floatf
    theo_freq       = np.real(dic_h5['freq'])      # this is an array
    log_likelihood  = get_log_likelihood_gaussian_one_mode(obs_freq, obs_freq_err, theo_freq)
    likelihood      = np.exp(log_likelihood)
    # chisquare       = get_chisq_freq_one_mode(obs_freq, obs_freq_err, theo_freq)
    # if np.max(likelihood) >=0.01: print np.max(likelihood)

    P_n_el_for_mode = P_n_el[i_obs, :]
    if all(P_n_el_for_mode == 0):
      logging.error('bayesian: get_log_likelihood_one_model: All theoretical models are excluded!')
      # raise SystemExit, 'Error: bayesian: get_log_likelihood_one_model: All theoretical models are excluded!'
      continue

    likelihood_one_mode = np.sum(likelihood * P_n_el_for_mode)
    # if likelihood_one_mode < Zero: likelihood_one_mode = Zero
    with np.errstate(divide='ignore'):
      log_L_P_n_el        = np.log(likelihood_one_mode)
      if log_L_P_n_el < ln_Zero: log_L_P_n_el = ln_Zero 
    cum_log_L_P_n_el    += log_L_P_n_el

  logging.info('end: {0}'.format(filename))

  return cum_log_L_P_n_el

#=================================================================================================================
def get_log_likelihood_models(dic_bayes, list_dic_h5):
  """
  Return the Cumulative log likelihood for a list of input models. This is achived by calling get_log_likelihood_one_model()
  iteratively.
  @param dic_bayes: dictionary with the following items:
     - dic_star: the input dic_star is readily passed back 
     - list_dic_h5: the list of dictionaries for the GYRE output information
     - prior_matrix: the matrix of the priors that shows the evolution of the prior per model per each iteration step
     - posterior_matrix: the matrix of the posteriors that shows the evolution of the posterior per model per each iteration step
  @type dic_bayes: dictionary
  @param list_dic_h5: a list of dictionaries containing the GYRE output info for models (i.e. files). For more information, follow the
     documentation in read.read_multiple_gyre_files().
  @type dic_h5: list of dictionaries
  @return: list of Cumulative log likelihood values. This choice (i.e. returning a list instead of an ndarray) is very suitable
     when this function is coupled to multiproc_get_log_likelihood(), since the former can be readilly added to increase the list,
     whereas the former does not trivially enlarge.
  @rtype: list of floats
  """
  logging.info('bayesian: get_log_likelihood_models: begin')
  list_out = []
  for i_dic, dic_h5 in enumerate(list_dic_h5):
    cum_log_L_P_n_el = get_log_likelihood_one_model(dic_bayes, dic_h5)
    list_out.append(cum_log_L_P_n_el)

  logging.info('bayesian: get_log_likelihood_models: Successfully finished')

  return list_out

#=================================================================================================================
def multiproc_get_log_likelihood(dic_bayes, n_cpu=None):
  """
  Use multiprocessing to compute log_likelihood for the input list of models.
  @param dic_bayes: dictionary with the following items:
     - dic_star: the input dic_star is readily passed back 
     - list_dic_h5: the list of dictionaries for the GYRE output information
     - prior_matrix: the matrix of the priors that shows the evolution of the prior per model per each iteration step
     - posterior_matrix: the matrix of the posteriors that shows the evolution of the posterior per model per each iteration step
  @type dic_bayes: dictionary
  @param n_cpu: Requested number of CPUs for using multiprocessing. If not specified, then it is querried by a call to 
     multiprocessing.cpu_count(), and the maximum possible number of available CPUs are elected. default=None
  @type n_cpu: integer
  @return: an array containing the log likelihood values for all models.
  @rtype: ndarray
  """
  t_start = time.time()

  dic_star    = dic_bayes['dic_star']
  list_dic_h5 = dic_bayes['list_dic_h5']
  n_h5        = len(list_dic_h5)

  if n_cpu is None:
    n_cpu = cpu_count()
  else:
    n_cpu = min([n_cpu, cpu_count()])

  pool = Pool(processes=n_cpu)

  n_chopes = n_h5 / n_cpu + 1

  results = []
  for i_cpu in range(n_cpu):
    ind_from = i_cpu * n_chopes
    ind_to   = ind_from + n_chopes 
    if i_cpu == n_cpu - 1: ind_to = n_h5

    chop     = list_dic_h5[ind_from : ind_to]
    args     = (dic_bayes, chop)
    
    results.append(pool.apply_async( get_log_likelihood_models, args=args )) 

  pool.close()
  # pool.terminate()
  print '   multiproc_get_log_likelihood: pool.close() done'
  logging.info('bayesian: multiproc_get_log_likelihood: pool.close() done')
  pool.join()
  print '   multiproc_get_log_likelihood: pool.join() done'
  logging.info('bayesian: multiproc_get_log_likelihood: pool.join() done')

  outputs = []
  for i_cpu in range(n_cpu): 
    print '   .get() from CPU #{0}'.format(i_cpu)
    result  = results[i_cpu].get()
    outputs += result
  print '   multiproc_get_log_likelihood: Outputs are successfully collected'

  dt_sec  = time.time() - t_start
  print '   Took {0} seconds'.format(int(dt_sec))
  logging.info('bayesian: multiproc_get_log_likelihood: N_cpu={0}, N_dics={1}, Time={2} [sec] \n'.format(n_cpu, n_h5, int(dt_sec)))

  return np.array(outputs)

#=================================================================================================================
def get_all_possible_2d(rec_bayes):
  """
  Depending on the grid, and the parameters that are varied, one can extract various 2D planes for log_posterior as 
  a function of two parameters, x and y, that are varied. This function automatically identifies those parameters 
  that are vaird, and from a collection of such, chooses "all possible unique combinations" of (par1, par2). 
  This allows plotting all possible 2D planes as a multi-panel plot.
  @param rec_bayes: full information from the Bayesian inference. This is passed from a former call to e.g. 
         mesa_gyre.bayesian.dic_bayes_to_rec_arr() (in case of the first use), or by a previous call to the same function, 
         mesa_gyre.bayesian.marginalize(). The maximum possible (number and names of the) fields is when it is the first call 
         and no dimension reduction has occured yet. Then, it has 7 fields as documented in mesa_gyre.bayesian.dic_bayes_to_rec_arr().
         They are:
         - M
         - ov 
         - Z 
         - Xini 
         - logD 
         - Xc 
         - log_posterior: which is the final log(posterior) obtained after the whole updating process is finished 
           In case of the first call, the posterior is that resulting from iterative Bayesian updating procedure. Otherwise,
           it is resulted from previous marginalization.
  @type rec_bayes: numpy record array
  @return: list of all possible couples of the parameters along with their log_posterior values as a list.
  @rtype: list of recarray
  """
  names       = rec_bayes.dtype.names 
  param_names = [name for name in names if name != 'log_posterior']
  n_params    = len(param_names)

  #--------------------------------
  # Only take those parames with their
  # values varied
  #--------------------------------
  use_params  = []
  for i_param, param in enumerate(param_names):
    set_param = set(rec_bayes[param])
    if len(set_param) > 1:
      use_params.append(param)
  n_use       = len(use_params)
  print ' - bayesian: get_all_possible_2d: Will use these parameters: ', use_params

  #--------------------------------
  # Only pack unique sets of (x, y)
  #--------------------------------
  n_iters     = n_use * (n_use-1) / 2
  set_couples = set()
  list_out    = []
  for i_x, param_x in enumerate(use_params):
    for i_y, param_y in enumerate(use_params):
      if param_x == param_y: continue
      tup_xy = (param_x, param_y)
      tup_yx = (param_y, param_x)
      avail  = (tup_xy in set_couples) or (tup_yx in set_couples)
      if avail: 
        continue
      else:
        set_couples.add(tup_xy)

      recarr_xy = marginalize_to_2d(rec_bayes, list_keep=[param_x, param_y])  
      list_out.append(recarr_xy)

  logging.info('bayesian: get_all_possible_2d: Returning {0} possible planes from {1} input parameters'.format(n_use, n_params))

  return list_out

#=================================================================================================================
#=================================================================================================================


#! /usr/bin/python

"""
This module is a test to compute chi-square values for every model in the grid, using multiprocessing facilities,
to gain significant speed up.
Here, the idea is to open a Pool and submit jobs of equal size on the single threaded function chisq_single_thread(),
and finally collect back the results.
"""

import sys, os, glob
import numpy as np
import logging
from multiprocessing import Pool, cpu_count
from mesa_gyre import stars, read, chisq, write

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================
def submit(list_input, dic_obs, chisq_by, consecutive, strict, pg, el, freedom, sigma, freq_unit, n_cpu=None, 
           path_plots=None):
  """
  This function receives the whole required arguments to submit jobs to chisq_single_thread(), and collect
  them back.
  @param chisq_by: Specify which scheme to call chisq. Possible options are:
         - reduced_chisq_seism_freq:
           This only calls chisq.seismic_freq(), and is much faster. The good model from this will automatically
           produce a good fit to the period spacing.
         - reduced_chisq_seism_f_and_dP: Then call
           This calls period_spacing.gen_list_dic_dP(), period_spacing.update_dP_dip() and chisq.seismic_dP().
           Therefore, it takes much longer to run
  @type chisq_by: string
  @param pg: specify to compute chisquare for either 'p' or 'g' modes. The list_dic_freq in dic_obs will be 
         searched to use only those mode dictionaries whose 'pg' key has the value of either 'p' or 'g'.
         default='g'.
  @type pg: string
  @param consecutive: default=True; specify if the search for modes and their Chisquare evaluation has to be 
           performed for consecutive modes, or not. This is useful e.g. for strict search on g-modes showing
           a series of nearly euqally spaced spacings. This choice is passed as an argument to chisq_single_thread().
  @type consecutive: boolean
  @param strict: flag to limit the chi-square evaluation to those modes within the observed range (True), or be less 
       strict, and find the closest frequency, even if it lies well outside the observed frequency interval withing 
       "sigma" times the extrema of observed frequency list. default=True
  @type strict: boolean
  @return: list of models including their Chisquare values.
  @rtype: list of dictionaries
  """
  n_h5 = len(list_input)
  if n_h5 == 0:
    logging.error('chisq_multiproc: submit: Input list is empty')
    raise SystemExit, 'Error: chisq_multiproc: submit: Input list is empty'

  logging.info('submit: Update dic_obs with period spacing of each mode')
  list_dic_freq = dic_obs['list_dic_freq']
  list_dic_freq = stars.check_obs_vs_model_freq_unit(freq_unit, list_dic_freq) # fix the unit
  dic_obs['list_dic_freq'] = list_dic_freq
  # update the list of frequencies to include the periods, period spacings and their associated errors
  if pg == 'g':
    dic_obs     = stars.list_freq_to_list_dP(dic_obs, freq_unit=freq_unit)

  if n_cpu is None:
    n_cpu = cpu_count()
  else:
    n_cpu = min([n_cpu, cpu_count()])

  pool = Pool(processes=n_cpu)

  n_chops = n_h5 / n_cpu + 1

  results = []
  for i_cpu in range(n_cpu):
    ind_from = i_cpu * n_chops
    ind_to   = ind_from + n_chops 
    if i_cpu == n_cpu - 1: ind_to = n_h5

    chop     = list_input[ind_from : ind_to]
    args     = (chop, dic_obs, chisq_by, i_cpu, el, freedom, sigma, pg, consecutive, strict, freq_unit)
    
    results.append(pool.apply_async( chisq_single_thread, args=args )) 

  pool.close()
  pool.join()
  
  outputs = []
  for i_cpu in range(n_cpu): outputs += results[i_cpu].get()

  list_chisq_sorted = chisq.sort_and_write_chisq(outputs, chisq_by)
  write.list_chisq_to_ascii(list_chisq_sorted, chisq_by, max_row=None)
  write.show_best_models(list_chisq_sorted[0], chisq_by=chisq_by)

  return list_chisq_sorted

#=================================================================================================================
def chisq_single_thread(list_h5_files, dic_obs, chisq_by, i_cpu, el, freedom, sigma, pg, 
                        consecutive, strict, freq_unit):
    """
    This function has many smililarities with chisq.do_chisq_freq_P_dP(). Read the documentation there.
    Here, we do not create any plot.
    @param chisq_by: Specify which scheme to call chisq. Possible options are:
	        - reduced_chisq_seism_freq:
	          This only calls chisq.seismic_freq(), and is much faster. The good model from this will automatically
	          produce a good fit to the period spacing.
	        - reduced_chisq_seism_f_and_dP: Then call
	          This calls period_spacing.gen_list_dic_dP(), period_spacing.update_dP_dip() and chisq.seismic_dP().
	          Therefore, it takes much longer to run
    @type chisq_by: string
    @param i_cp: The specifier for the core which is calling this function. This is used later to write
           the output based on the core which run this function.
    @type i_cpu: integer
    @param pg: specify to compute chisquare for either 'p' or 'g' modes. The list_dic_freq in dic_obs will be 
           searched to use only those mode dictionaries whose 'pg' key has the value of either 'p' or 'g'.
           default='g'.
    @type pg: string
    @param consecutive: default=True; specify if the search for modes and their Chisquare evaluation has to be 
           performed for consecutive modes, or not. This is useful e.g. for strict search on g-modes showing
           a series of nearly euqally spaced spacings.
    @type consecutive: boolean
    @param strict: flag to limit the chi-square evaluation to those modes within the observed range (True), or be less 
       strict, and find the closest frequency, even if it lies well outside the observed frequency interval withing 
       "sigma" times the extrema of observed frequency list. default=True
    @type strict: boolean
    @return: list of modes with the chisq_by key added to each dictionary.
    @rtype: list of dictionaries
    """
    do_chi2_f    = chisq_by == 'reduced_chisq_seism_freq' or chisq_by == 'chisq_seism_freq'
    do_chi2_f_dP = chisq_by == 'reduced_chisq_seism_f_and_dP'
    if not any([do_chi2_f, do_chi2_f_dP]):
    	logging.error('chisq_multiproc: chisq_single_thread: wrong choice for chisq_by')
    	raise SystemExit, 'Error: chisq_multiproc: chisq_single_thread: chisq_by can only be \
    	"reduced_chisq_seism_freq" or "reduced_chisq_seism_f_and_dP"'

    if isinstance(list_h5_files[0], str):
      logging.info('chisq_single_thread: Core #{0}: Read HDF5 files, Calculate peiod spacing'.format(i_cpu))
      list_dic_modes = read.read_multiple_gyre_files(list_h5_files)
    elif isinstance(list_h5_files[0], dict):
      list_dic_modes = list_h5_files
    else:
      print 'Error: chisq_multiproc: chisq_single_thread: Invalid type() of input items.'
      raise SystemExit

    if do_chi2_f:
    	list_input     = [dic for dic in list_dic_modes]
    	list_dic_modes = None
    elif do_chi2_f_dP:
    	list_dic_dP    = period_spacing.gen_list_dic_dP(list_dic_modes)
    	list_dic_dP    = period_spacing.update_dP_dip(list_dic_dP, el=1, by_P=True, P_from=0.98, P_to=2.12)
    	list_input     = [dic for dic in list_dic_dP]
    	list_dic_dP    = None

    if do_chi2_f:
      logging.info('chisq_single_thread: Perform Chisquare based on Frequencies')
      list_dic_modes_with_chisq = chisq.seismic_freq(list_input, dic_obs, freedom=freedom, el=el, 
                                        freq_unit=freq_unit, pg=pg, consecutive=consecutive, strict=strict, 
                                        sigma=sigma)

    if do_chi2_f_dP:
	    logging.info('chisq_single_thread: Perform Chisquare based on Periods and Period Spacings')
	    list_dic_modes_with_chisq  = chisq.seismic_dP(list_input, dic_obs, freedom=freedom, el=el,
                                         freq_unit=freq_unit, pg=pg, consecutive=consecutive, strict=strict, 
                                         sigma=sigma)

    return list_dic_modes_with_chisq

#=================================================================================================================
def different_obs_freq(list_input, chisq_by, freedom, freq_unit, 
                                list_dic_obs, list_sigma, list_el, list_pg, list_conseqcutive, list_strict,
                                list_chisq_names, n_cpu=8):
  """
  Use multiprocessing to 
  """
  n_h5 = len(list_input)

  if n_cpu is None:
    n_cpu = cpu_count()
  else:
    n_cpu = min([n_cpu, cpu_count()])

  if not isinstance(list_input[0], str):
    logging.error('different_obs_freq: Input listh should only be strins')
    raise SystemExit, 'Error: different_obs_freq: Input listh should only be strins'

  pool = Pool(processes=n_cpu)

  n_chops = n_h5 / n_cpu + 1

  results = []
  for i_cpu in range(n_cpu):
    ind_from = i_cpu * n_chops
    ind_to   = ind_from + n_chops 
    if i_cpu == n_cpu - 1: ind_to = n_h5

    chop     = list_input[ind_from : ind_to]
    args     = (chop, chisq_by, freedom, freq_unit, list_dic_obs, list_sigma, list_el, list_pg, 
                list_conseqcutive, list_strict, list_chisq_names)
    
    results.append(pool.apply_async( chisq.one_thread_different_obs_freq, args=args )) 

  pool.close()
  logging.info('chisq_multiproc: different_obs_freq: pool.close() done')
  pool.join()
  logging.info('chisq_multiproc: different_obs_freq: pool.join() done')
  
  outputs = []
  for i_cpu in range(n_cpu): 
    outputs += results[i_cpu].get()

  logging.info('chisq_multiproc: different_obs_freq: results.get() done')

  return outputs

#=================================================================================================================

"""
This module animates the live evolution of seismic and structure of a given MESA model.
"""
import sys, os, glob
import subprocess
import matplotlib 
# matplotlib.use('TkAgg') # do this before importing pylab
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import numpy as np
import commons, param_tools, period_spacing, stars

#=================================================================================================================
Msun = 1.9892e33    # g
Rsun = 6.9598e10    # cm
Lsun = 3.8418e33    # cgs
G    = 6.67428e-8   # cm^3 g^-1 s^-2
G_grav  = G

Hz_to_cd = commons.conversions()['Hz_to_cd']
sec_to_d = commons.conversions()['sec_to_d']
d_to_sec = commons.conversions()['d_to_sec']

#=================================================================================================================
def anim_mode_Kernels_with_dP(dic_sum, dics_eigs, xaxis_from=None, xaxis_to=None, K_from=None, K_to=None, 
                              movie_out=None):
    """
    This does not work, and I do not want to spend time fixing this.
    
    Copied and adapted from 
        http://matplotlib.org/1.4.0/examples/animation/moviewriter.html
    """
    # matplotlib.use("Agg")

    ########################################
    # Setup the movie
    ########################################
    FFMpegWriter = manimation.writers['ffmpeg']
    metadata = dict(title='KIC 7760680: Kernels and Period Spacing', 
                    artist='Ehsan Moravveji', comment='using matplotlib.manimation()')
    writer = FFMpegWriter(fps=2, metadata=metadata)

    ########################################
    # Extract the period spacing from the short summary file 
    ########################################
    freq    = np.real(dic_sum['freq']) * Hz_to_cd
    ind_sort= np.argsort(freq)[::-1]
    freq    = freq[ind_sort]
    per     = 1.0 / freq                       # P in days
    dP      = (per[1:] - per[:-1]) * d_to_sec  # dP in sec
    n_pg    = dic_sum['n_pg'][ind_sort]
    E_norm  = dic_sum['E_norm'][ind_sort]


    ########################################
    # Setup the Figure and the axis
    ########################################
    fig   = plt.figure(figsize=(6,6), dpi=200)
    plt.subplots_adjust(left=0.11, right=0.90, bottom=0.07, top=0.92, hspace=0.06, wspace=0.06)
    ax1   = fig.add_subplot(211)
    ax2   = fig.add_subplot(212)
    ax3   = ax2.twinx()
    ax4   = ax1.twinx()
   

    # l, = plt.plot([], [], 'k-o')

    # plt.xlim(-5, 5)
    # plt.ylim(-5, 5)

    # x0,y0 = 0, 0

    with writer.saving(fig, movie_out, 100):
        for i, dic_eig in enumerate(dics_eigs):

            # Extract the profiles from the datasets
            m       = dic_eig['m']
            m_rel   = m/dic_eig['M_star']
            mass    = m / Msun
            x_rel   = dic_eig['x']
            x       = dic_eig['x'] * dic_eig['R_star']
            el      = dic_eig['l']
            #xi_r    = np.real(dic_eig['xi_r'])
            #xi_h    = np.real(dic_eig['xi_h'])
            Knl     = np.real(dic_eig['K'])
            As      = dic_eig['As']
            prop    = dic_eig['prop_type']
            nz      = len(prop)

            S_l_sq  = np.zeros(nz)
            grav    = np.zeros(nz)
            N_sq    = np.zeros(nz)

            c_s_sq      = dic_eig['Gamma_1'] * dic_eig['p'] / dic_eig['rho']
            S_l_sq[1:]  = el * (el+1) * c_s_sq[1:] / x[1:]**2.0
            S_l_sq[0]   = S_l_sq[1]
            S_l         = np.sqrt(S_l_sq)
            log_S_l     = np.log10(S_l * Hz_to_cd)
            grav[1:]    = G_grav * m[1:]/x[1:]**2.0
            grav[0]     = 0
            N_sq[1:]    = grav[1:] * As[1:] / x[1:]
            N_sq[0]     = 0.0
            N_sq[N_sq<=0] = 1e-99
            N           = np.sqrt(N_sq)
            log_N       = np.log10(N * Hz_to_cd)
            log_freq    = np.log10(np.real(dic_eig['freq']) * Hz_to_cd)

            ########################################
            # Plot Period Spacing and Normalized Energy
            ########################################
            ax1.plot(per[:-1], dP, color='grey', marker='o', lw=1, zorder=1)
            ax4.plot(per, np.log10(E_norm), color='green', marker='o', lw=1, zorder=1)

            ########################################
            # Plot Kernel and BV profile vs. mass
            ########################################
            ax2.fill_between(mass, y1=-100, y2=100, where=(prop==0), color='blue', alpha=0.25)
            ax2.fill_between(mass, y1=-100, y2=100, where=(prop==1), color='green', alpha=0.25)
            ax2.plot(mass, Knl, lw=1.5, color='black', linestyle='solid', label=r'$K_{n,\ell}$')
            ax3.plot(mass, log_N, linestyle='dashed', color='r', lw=2, label=r'$\log N^2$')

            ########################################
            # Highlight the mode in red by its n_pg
            ########################################
            the_n_pg = dic_eig['n_pg']
            ind_n_pg = np.where(the_n_pg == n_pg)[0]
            ax1.scatter([per[ind_n_pg]], [dP[ind_n_pg]], marker='o', s=25, color='red', zorder=2)
            ax1.scatter([per[ind_n_pg]], [dP[ind_n_pg]], marker='o', s=125, facecolors='none', 
                         edgecolors='red')

            ax4.scatter([per[ind_n_pg]], [np.log10(E_norm[ind_n_pg])], marker='o', s=25, color='red', zorder=2)
            ax4.scatter([per[ind_n_pg]], [np.log10(E_norm[ind_n_pg])], marker='o', s=125, 
                        facecolors='none', edgecolors='red')

            ########################################
            # Annotations
            ########################################
            if not xaxis_from: xaxis_from = 0
            if not xaxis_to:   xaxis_to   = 1
            if not K_from:     K_from     = -0.01
            if not K_to:       K_to       = 30

            # ax1.set_xticklabels(())
            ax1.set_xlabel('Period [d]')    
            ax1.xaxis.set_label_position('top') 
            ax1.xaxis.set_tick_params(labelbottom='off', labeltop='on')
            ax1.set_ylabel(r'$\Delta P$ [sec]')

            ax2.set_xlim(xaxis_from, xaxis_to)
            ax2.set_xlabel(r'Enclosed Mass [M$_\odot$]')
            ax2.set_ylabel(r'Kernels \,$K_{n,\ell}(r)$')
            ax2.set_ylim(K_from, K_to)

            # ax2.set_xlim(xaxis_from, xaxis_to)
            ax3.set_ylabel(r'$\log N_{\rm BV}$ [d$^{-1}$]')
            ax3.set_ylim(-1,3)
            ax3.spines['right'].set_color('red')
            ax3.tick_params(axis='y', colors='red')
            ax3.yaxis.label.set_color('red')

            ax4.set_ylabel(r'Inertia \,$\log \mathcal{I}_{n,\ell}$')
            # ax4.set_ylim(-1,3)
            ax4.spines['right'].set_color('green')
            ax4.tick_params(axis='y', colors='green')
            ax4.yaxis.label.set_color('green')

            txt = r'$n_{\rm pg}$=' + '{0}'.format(dic_eig['n_pg'])
            ax1.annotate(txt, xy=(0.04, 0.22), xycoords='axes fraction', fontsize='large', color='red')
            txt = r'$P$='+'{0:0.3f} '.format(1./np.real(dic_eig['freq'])*sec_to_d) + r' [d]'
            ax1.annotate(txt, xy=(0.04, 0.14), xycoords='axes fraction', fontsize='large')
            txt = r'$f$='+'{0:0.3f} '.format(np.real(dic_eig['freq'])*Hz_to_cd) + r' [d$^{-1}$]'
            ax1.annotate(txt, xy=(0.04, 0.06), xycoords='axes fraction', fontsize='large')

            # x0 += 0.1 * np.random.randn()
            # y0 += 0.1 * np.random.randn()
            # l.set_data(x0, y0)
            writer.grab_frame()

    print ' - animated: anim_mode_Kernels_with_dP: saved {0}'.format(movie_out)

    return None 

#=================================================================================================================
def make_movie(filecode,fps=4,bitrate=None,resize=100,output='output.avi',cleanup=False):
    """
    Repeat last 10 frames!
    
    bitrate=10 seems to be good for avi
    """
    if os.path.splitext(output)[1]=='.avi':
        if bitrate is None:
            cmd = 'mencoder "mf://%s" -mf fps=%d -o %s -ovc lavc -lavcopts vcodec=mpeg4'%(filecode,fps,output)
        else:
            bitrate*=1000
            cmd = 'mencoder "mf://%s" -mf fps=%d -o %s -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=%d'%(filecode,fps,output,bitrate)
    elif os.path.splitext(output)[1]=='.gif':
        delay = 100./fps
        cmd = 'convert -delay {:.0f} -resize {}% -layers optimizePlus -deconstruct -loop 0 {} {}'.format(delay,resize, filecode,output)
    print('Executing {}'.format(cmd))
    subprocess.call(cmd,shell=True)
    if cleanup:
        print("Cleaning up files...")
        for ff in glob.glob(filecode):
            os.unlink(ff)
        print("Done!")

#=================================================================================================================
def gen_mpeg_from_img(img_dir, srch_str='*', framerate=10, fps=4, resolution='720x480', extension='jpg',
                      avi_out='Movie.avi'):
  """
  Convert a sequence of jpg or png files into a *.avi stream using ffmpeg
  @param img_dir: full path to where all jpg files sit
  @type img_dir: string
  @param srch_str: the regex pattern to search for the sorted and structured .jpg files. default='*'
  @type srch_str: string
  @param framerate: specify the quality, hence the size, of the output .avi movie
  @type framerate: integer
  @param fps: frames per second
  @type fps: integer
  @param extension: the extension of the input frame; default='jpg', but can also be 'png', etc
  @type extension: string
  @param resolution: the resolution of the output movie; default='720x480'
  @type resolution: string
  @param avi_out: full path to the location to store the output movie
  @type avi_out: string
  @return: None
  @rtype: None
  """
  if not os.path.exists(img_dir): 
    raise SystemExit, 'Error: animated: gen_mpeg_from_img: {0} does not exist'.format(img_dir)
  if img_dir[-1] != '/': img_dir += '/'
  
  img_files = sorted(glob.glob(img_dir + srch_str + extension))
  n_jpg = len(img_files)
  if n_jpg == 0:
    raise SystemExit, 'Error: animated: gen_mpeg_from_img: No image files found in {0}'.format(img_dir)
  
  if extension[0] == '.': extension = extension[1:]
  
  ####################################
  # Step 1: Convernt jpg --> jpg
  ####################################
  #for i, jpg in enumerate(img_files):
    #jpg = jpg.replace('.jpg', '.jpg')
    #cmd = ' convert -quality {0} {1} {2}'.format(quality, jpg, jpg)
    #process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    ##with open(process.stderr, 'r') as r: errs = r.readlines()
    #out, err = process.communicate()
    #if len(err) > 0:
      #print err
      #raise SystemExit, 'Error: animated: gen_mpeg_from_img: (1) stderr for: {0}'.format(jpg)
    
  #img_files = glob.glob(img_dir + '*.jpg')
  
  #cmd = 'convert -delay {0} -quality {1} {2}*.jpg {3}'.format(delay, quality, img_dir, mpeg_out)
  #cmd = 'mencoder -ovc lavc -lavcopts vcodec=mpeg1video:vbitrate=1500 -vf 
         #scale=320x240 -mf type=jpg:fps=5 -nosound -of mpeg -o output.mpg mf://*.jpg'
  #process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
  #out, err = process.communicate()
  #with open(process.stderr, 'r') as r: errs = r.readlines()
  #if len(err) > 0:
    #print err
    #raise SystemExit, 'Error: animated: gen_mpeg_from_img: (2) stderr '  
  
  cmd = """ffmpeg -y -f image2 -framerate {0} -pattern_type sequence -r {1} -i {2}{3}-%04d.{4} -s {5} {6}""".format(
                        framerate, fps, img_dir, srch_str, extension, resolution, avi_out)
  print '\n   {0} \n'.format(cmd)
  
  subprocess.call(cmd, shell=True)

  #process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
  #out, err = process.communicate()
  #with open(process.stderr, 'r') as r: errs = r.readlines()
  #if len(err) > 0:
    #print err
    #raise SystemExit, 'Error: animated: gen_mpeg_from_img: (2) stderr '  
  
  #for a_jpg in img_files: os.unlink(a_jpg)
  
  return None

#=================================================================================================================
def anim_freq_dP(list_dic_h5, dic_star, el=1, best_Xc=None, P_from=1, P_to=3, Xc_from=0.70, Xc_to=0.10, dP_from=1000, dP_to=10000,
                 dpi=100, save_dir='jpg', jpg_name='anim_freq_dP'):
  """
  Create a 3-panel frames for later animations. Top panel shows a sliding window on Xc, the middle panel shows how
  the frequencies match, and the bottom panel shows how period spacing dP changes with age.
  The output .jpg files will be named based on the iput filenames, exchanging the .h5 extension with .jpg
  @param list_dic_h5: list of GYRE frequency output, one dictionary per each model
  @type list_dic_h5: list of dictionaries
  @param dic_star: dictionary containing the observed information of a star, returned mainly by mesa_gyre.stars module
  @type dic_star: dictionary
  @param best_Xc: the value of Xc from the best-matching model. if not None, a sequence of observed modes will be overplotted
      with red squares at the position of the best Xc; default=None
  @type best_Xc: real
  @param P_from/P_to: minimum/maximum period shown on the top panel; Note, unit is in days
  @type P_from/P_to: float
  @param Xc_from/Xc_to: minimum/maximum period shown on the top panel; Note, Xc_from > Xc_to, since Xc_drops by evolution
  @type Xc_from/Xc_to: float
  @param dpi: Number of dots per inch for the plot. default is 100, and each file has ~190 KB. For dpi=200, the size 
      increases to ~480 KB.
  @param save_dir: full path to the directory to dump the jpg frames
  @type save_dir: string
  @param jpg_name: the prefix name of the jpg file to be stored locally in the "save_dir" directory. Note that the .jpg extension 
      is automatically appended, so do not provide it.
  @type jpg_name: string
  @return: None
  @rtype: None
  """
  import matplotlib.gridspec as gridspec
  n_dic = len(list_dic_h5)
  m_dic = len(list_dic_h5)
  if (n_dic) == 0:
    raise SystemExit, 'Error: animated: anim_freq_dP: Input list is empty!'
  
  if not os.path.exists(save_dir): os.makedirs(save_dir)
  if save_dir[-1] != '/': save_dir += '/'
  dic_conv = commons.conversions()
  Hz_to_cd = dic_conv['Hz_to_cd']
  sec_to_d = dic_conv['sec_to_d']
  
  list_obs_freq = dic_star['list_dic_freq']
  n_obs_freq         = len(list_obs_freq)
  stars.check_obs_vs_model_freq_unit('Hz', list_obs_freq)
  dic_star['list_dic_freq'] = list_obs_freq
  dic_obs_P_dP = stars.list_freq_to_list_dP(dic_star)
  list_star_freq     = np.array( [dic['freq'] * Hz_to_cd for dic in dic_obs_P_dP['list_dic_freq']] )
  list_star_freq_err = np.array( [dic['freq_err'] * Hz_to_cd for dic in dic_obs_P_dP['list_dic_freq']] )
  list_star_P        = np.array( [dic['P'] * sec_to_d for dic in dic_obs_P_dP['list_dic_freq']] )
  list_star_P_err    = np.array( [dic['P_err'] * sec_to_d for dic in dic_obs_P_dP['list_dic_freq']] )
  list_star_dP       = np.array( [dic['dP'] for dic in dic_obs_P_dP['list_dic_freq']] )
  list_star_dP_err   = np.array( [dic['dP_err'] for dic in dic_obs_P_dP['list_dic_freq']] )
  
  str_el = str(el)
  str_f  = 'el_' + str_el + '_gm_freq_re'
  str_P  = 'el_' + str_el + '_gm_P'
  str_dP = 'el_' + str_el + '_gm_dP'
  
  ####################################
  # Collect the whole information for 
  # the fixed background
  ####################################
  list_freq = []
  list_P    = []
  list_dP   = []
  list_Xc   = []
  list_n    = []
  for i_dic, a_dic in enumerate(list_dic_h5):
    list_freq.append(a_dic[str_f] * Hz_to_cd)
    list_P.append(a_dic[str_P] * sec_to_d)
    list_dP.append(a_dic[str_dP])
    list_n.append(len(a_dic[str_f]))
    list_Xc.append(np.ones(list_n[i_dic]) * a_dic['Xc'])

  ####################################
  # Loop over all dictinaries, and update
  # all panels
  ####################################
  for i_dic in range(n_dic): # , dic in enumerate(list_dic_h5):
    dic = list_dic_h5[i_dic]
  
    ####################################
    # Prepare the plot
    ####################################
    fig = plt.figure(figsize=(6,8), dpi=dpi)
    gs = gridspec.GridSpec(10, 1)
    ax1 = plt.subplot(gs[0:5, 0])
    ax2 = plt.subplot(gs[6, 0])
    ax3 = plt.subplot(gs[7:, 0])
    gs.update(left=0.11, right=0.98, bottom=0.06, top=0.98, hspace=0.20, wspace=0.20)

    ####################################
    # Populate panels with all model periods
    # and observations.
    # This is a fixed background for all frames
    ####################################
    # Panel 1: all periods as a function of Xc
    ax1.fill_between([Xc_from, Xc_to], y1=P_from, y2=P_to, color='0.10', alpha=0.25, zorder=0)
    for j in range(m_dic):
      a_Xc_arr  = list_Xc[j]
      a_P       = list_P[j]
      ax1.scatter(a_Xc_arr, a_P, marker='o', s=1, color='blue', zorder=2)
    if best_Xc is not None:
      ax1.scatter(np.ones(n_obs_freq) * best_Xc, list_star_P, marker='s', s=9, color='red', zorder=3)
  
    # Panel 2: Observed Periods as a Harps diagram
    for i_per in range(n_obs_freq): ax2.axvline(list_star_P[i_per], color='red', zorder=1)
  
    # Panel 3: Observed Period Spacing
    ax3.errorbar(list_star_P[:-1], list_star_dP[:-1], xerr=list_star_P_err[:-1], yerr=list_star_dP_err[:-1],
                 color='red', ecolor='red', elinewidth=1.5, mfc='black', mec='red', ms=4, mew=1, zorder=1, 
                 label=r'KIC 10526294')
    ax3.plot([], linestyle='solid', lw=4, color='blue', label=r'GYRE')

    ####################################
    # Plot individual files
    ####################################
    filename  = dic['filename']
    dic_par   = param_tools.get_param_from_single_gyre_filename(filename)
    Xc        = dic['Xc']
    freq_re   = dic[str_f] * Hz_to_cd
    P         = dic[str_P] * sec_to_d
    dP        = dic[str_dP]
    n_per     = len(P)
    
    ax1.axvline(Xc, color='white', lw=8, zorder=1)
    ax2.scatter(P, np.ones(n_per)*0.5, s=36, marker='s', color='blue', zorder=2)
    ax3.scatter(P[:-1], dP, marker='s', s=9, color='blue', zorder=2)
    ax3.plot(P[:-1], dP, linestyle='solid', lw=1, color='blue', zorder=2)

    ####################################
    # Legend, Annotations, Ranges
    ####################################
    ax1.set_xlabel(r'Center Hydrogen Mass Fraction $X_c$')
    ax1.set_ylabel(r'Mode Perod $P_n$ [days]')
    ax1.set_xlim(Xc_from, Xc_to)
    ax1.set_ylim(P_from, P_to)
  
    #ax2.set_xlabel(r'Observed Period [d$^{-1}$]')
    ax2.set_xticklabels(())
    ax2.set_xlim(P_from, P_to)
    ax2.set_ylim(0, 1)
    ax2.set_yticklabels(())

    ax3.set_xlabel(r'Observed Period [d$^{-1}$]')
    ax3.set_ylabel(r'Period Spacing $\Delta P$ [sec]')
    ax3.set_xlim(P_from, P_to)
    ax3.set_ylim(dP_from, dP_to)
    
    txt = r'$X_c=${0:6.4f}'.format(Xc)
    ax3.annotate(txt, xy=(0.05, 0.05), xycoords='axes fraction', fontsize='xx-large', color='black')
    
    leg3 = ax3.legend(loc=4, fontsize='medium', scatterpoints=1)

    ####################################
    # Store the figures
    ####################################
    figure_index = '-{0:04d}'.format(i_dic)
    figure_name = save_dir + jpg_name + figure_index + '.jpg'
    plt.savefig(figure_name, dpi=dpi)
    print ' - animated: anim_freq_dP: saved {0}'.format(figure_name)
    plt.close()

  return None

#=================================================================================================================
def anim_hist_prof_dP(list_dic_hist, list_list_dic_prof, list_list_rec_gyre=[], list_lbls=None, ref_indx=0, el=1,
                           dP_by_P=False, dP_by_freq=False, BV_by_mass=False, BV_by_logT=False, BV_by_logRho=False,
                           logD_by_logRho=False, logD_by_mass=False, logD_by_logT=False, 
                           P_grande_from=0, P_grande_to=10, P_klein_from=1, P_klein_to=2.5,
                           freq_grande_from=0, freq_grande_to=10, freq_klein_from=3, freq_klein_to=5,
                           dP_grande_from=1000, dP_grande_to=15000, dP_klein_from=1000, dP_klein_to=15000,
                           logT_from=4.5, logT_to=3.8, logg_from=4.4, logg_to=3.0,
                           BV_x_from=None, BV_x_to=None, BV_y_from=-9.5, BV_y_to=1,
                           logD_x_from=7.5, logD_x_to=7.0, logD_from=0, logD_to=16,
                           freq_unit='Hz', jpg_name='Evol-Hist-Prof-dP'):
  """
  This function receives three mandatory input:
    - list_dic_hist
    - list_list_dic_prof
    - list_list_rec_gyre 
  and returns a sequence of animated plots which shows the simulatneous evolution of the pulsation and internal 
  structure of the given model(s).
  There are a lot of controls for fine tuning the panels.
  @param list_dic_hist: list of dictionaries for the history information from MESA. The number of dictionaries in the
      list_dic_hist equals the number of tracks shown on the Kiel diagram.
  @type list_dic_hist: list of dictionaries.
  @param list_list_dic_prof: list of list of dictionaries for profile information: [ [dic, dic, dic], [dic, dic, dic, ...], ...]  
      Each dictionary item in list_dic_hist is associated to a list of dictionaries in list_list_dic_prof
  @type list_list_dic_prof: list of list of dictionaries
  @param list_list_rec_gyre: list of list of recarrays for the oscillation quantities: [ [rec, rec, ...], [rec, rec, rec, ...], ...]
      Each dictionary item in list_dic_hist is associated to a list of record arrays in list_list_rec_gyre. 
      Also, each dictionary item in list_list_dic_prof is associated to a recarray item in list_list_rec_gyre
  @param jpg_name: the prefix name of the jpg file to be stored locally in the '/jpg/' directory. Note that the .jpg extension 
      is automatically appended, so do not provide it.
  @type jpg_name: string
  @return: None
  @rtype: None
  """
  from commons import conversions
  
  n_hist = len(list_dic_hist)
  n_list_prof = len(list_list_dic_prof)
  n_list_gyre = len(list_list_rec_gyre)
  
  ####################################
  # Check appropriate inputs
  ####################################
  #if np.mean(np.asarray([n_hist, n_list_prof, n_list_gyre])) != n_hist:
    #message = 'Error: plot_evol: anim_hist_prof_dP: Number of objects in input lists do not match!'
    #raise SystemExit, message
  
  if not any([dP_by_freq, dP_by_P]):
    message = 'Error: plot_evol: anim_hist_prof_dP: You must set either of dP_by_freq OR dP_by_P to True!'
    raise SystemExit, message
  
  if not any([BV_by_mass, BV_by_logT, BV_by_logRho]):
    message = 'Error: plot_evol: anim_hist_prof_dP: You must set either of BV_by_mass, BV_by_logT OR BV_by_logRho to True!'
    raise SystemExit, message
  
  if not any([logD_by_mass, logD_by_logT, logD_by_logRho]):
    message = 'Error: plot_evol: anim_hist_prof_dP: You must set either of logD_by_mass, logD_by_logT OR logD_by_logRho to True!'
    raise SystemExit, message
  
  ####################################
  # Reference track
  ####################################
  logg_sun = np.log10(27542.29)
  el_str = str(el)
  sec_to_d = conversions()['sec_to_d']
  
  ref_dic_hist = list_dic_hist[ref_indx]
  ref_hist = ref_dic_hist['hist']
  ref_list_dic_prof = list_list_dic_prof[ref_indx]
  n_ref_dic_prof = len(ref_list_dic_prof)
  ref_list_rec_gyre = list_list_rec_gyre[ref_indx]
  n_ref_dic_gyre = len(ref_list_rec_gyre)
  ref_list_dic_dP = period_spacing.gen_list_dic_dP(ref_list_rec_gyre, freq_unit=freq_unit)
  if n_ref_dic_gyre != n_ref_dic_prof:
    print 'Error: plot_evol: anim_hist_prof_dP: Reference: n_prof != n_gyre: %s and %s' % (n_ref_dic_prof, n_ref_dic_gyre)
    #raise SystemExit, message
    
  # history information
  ref_hist_log_Teff = ref_hist['log_Teff']
  ref_hist_logg     = ref_hist['log_g']
  # all profile header/data information
  ref_list_prof_header_mass = []
  ref_list_prof_header_Xc = []
  ref_list_prof_header_Yc = []
  ref_list_prof_header_R = []
  ref_list_prof_header_log_Teff = []
  ref_list_prof_header_logg = []
  ref_list_prof_data_vrot = []
  ref_list_prof_data_surface_n14 = []
  ref_list_prof_data_surface_he4 = []
  for i_dic, ref_dic_prof in enumerate(ref_list_dic_prof):
    ref_dic_prof_header = ref_dic_prof['header']
    ref_dic_prof_data   = ref_dic_prof['prof']
    
    # fetch information from header
    ref_prof_Teff = ref_dic_prof_header['Teff']
    ref_prof_mass = ref_dic_prof_header['star_mass']
    ref_prof_radi = ref_dic_prof_header['photosphere_r']  
    ref_prof_Xc   = ref_dic_prof_header['center_h1']
    ref_prof_Yc   = ref_dic_prof_header['center_he4']
    ref_prof_logg = np.log10(ref_prof_mass/(ref_prof_radi**2.0)) + logg_sun
    # fetch information from profile column data
    ref_prof_vrot = np.mean(ref_dic_prof_data['v_rot'][-6:-1])
    ref_prof_he4  = np.mean(ref_dic_prof_data['he4'][-6:-1])
    ref_prof_n14  = np.mean(ref_dic_prof_data['n14'][-6:-1])
    
    # accumulate profile points along the track on the Kiel diagram
    ref_list_prof_header_mass.append(ref_prof_mass)
    ref_list_prof_header_R.append(ref_prof_radi)
    ref_list_prof_header_Xc.append(ref_prof_Xc)
    ref_list_prof_header_Yc.append(ref_prof_Yc)
    ref_list_prof_header_log_Teff.append(np.log10(ref_prof_Teff))
    ref_list_prof_header_logg.append(ref_prof_logg)
    ref_list_prof_data_vrot.append(ref_prof_vrot)
    ref_list_prof_data_surface_he4.append(ref_prof_he4) 
    ref_list_prof_data_surface_n14.append(ref_prof_n14)
    
  # Convert lists to numpy arrays
  ref_list_prof_header_log_Teff = np.asarray(ref_list_prof_header_log_Teff)
  ref_list_prof_header_logg     = np.asarray(ref_list_prof_header_logg)
  ref_list_prof_header_mass     = np.asarray(ref_list_prof_header_mass)
  ref_list_prof_header_R        = np.asarray(ref_list_prof_header_R)
  ref_list_prof_header_Xc       = np.asarray(ref_list_prof_header_Xc)
  ref_list_prof_header_Yc       = np.asarray(ref_list_prof_header_Yc)
  ref_list_prof_data_vrot       = np.asarray(ref_list_prof_data_vrot)
  ref_list_prof_data_surface_he4= np.asarray(ref_list_prof_data_surface_he4)
  ref_list_prof_data_surface_n14= np.asarray(ref_list_prof_data_surface_n14)
  
  ####################################
  # Other Track(s)
  ####################################
  # Currently it only suppoerts one "other" track and profile information. To extend it to support
  # multiple "other" lists, needs a bit more work for later.
  for i_hist in range(n_hist):
    if i_hist == ref_indx: continue 
  
    other_dic_hist = list_dic_hist[i_hist]
    other_hist     = other_dic_hist['hist']
    other_list_dic_prof = list_list_dic_prof[i_hist]
    n_other_dic_prof = len(other_list_dic_prof)
    other_list_rec_gyre = list_list_rec_gyre[i_hist]
    n_other_dic_gyre = len(other_list_rec_gyre)
    other_list_dic_dP = period_spacing.gen_list_dic_dP(other_list_rec_gyre, freq_unit=freq_unit)
    if n_other_dic_gyre != n_other_dic_prof:
      print 'Error: plot_evol: anim_hist_prof_dP: Other: n_prof != n_gyre: %s and %s' % (n_other_dic_prof, n_other_dic_gyre)
      #raise SystemExit, message
      
    # history information
    other_hist_log_Teff = other_hist['log_Teff']
    other_hist_logg     = other_hist['log_g']

  # all profile header/data information
    other_list_prof_header_mass = []
    other_list_prof_header_Xc = []
    other_list_prof_header_Yc = []
    other_list_prof_header_R = []
    other_list_prof_header_log_Teff = []
    other_list_prof_header_logg = []
    other_list_prof_data_vrot = []
    other_list_prof_data_surface_n14 = []
    other_list_prof_data_surface_he4 = []
    for i_dic, other_dic_prof in enumerate(other_list_dic_prof):
      other_dic_prof_header = other_dic_prof['header']
      other_dic_prof_data   = other_dic_prof['prof']
    
    # fetch information from header
      other_prof_Teff = other_dic_prof_header['Teff']
      other_prof_mass = other_dic_prof_header['star_mass']
      other_prof_radi = other_dic_prof_header['photosphere_r']  
      other_prof_Xc   = other_dic_prof_header['center_h1']
      other_prof_Yc   = other_dic_prof_header['center_he4']
      other_prof_logg = np.log10(other_prof_mass/(other_prof_radi**2.0)) + logg_sun
      # fetch information from profile column data
      other_prof_vrot = np.mean(other_dic_prof_data['v_rot'][-6:-1])
      other_prof_he4  = np.mean(other_dic_prof_data['he4'][-6:-1])
      other_prof_n14  = np.mean(other_dic_prof_data['n14'][-6:-1])
    
      # accumulate profile points along the track on the Kiel diagram
      other_list_prof_header_mass.append(other_prof_mass)
      other_list_prof_header_R.append(other_prof_radi)
      other_list_prof_header_Xc.append(other_prof_Xc)
      other_list_prof_header_Yc.append(other_prof_Yc)
      other_list_prof_header_log_Teff.append(np.log10(other_prof_Teff))
      other_list_prof_header_logg.append(other_prof_logg)
      other_list_prof_data_vrot.append(other_prof_vrot)
      other_list_prof_data_surface_he4.append(other_prof_he4) 
      other_list_prof_data_surface_n14.append(other_prof_n14)
    
    # Convert lists to numpy arrays
    other_list_prof_header_log_Teff = np.asarray(other_list_prof_header_log_Teff)
    other_list_prof_header_logg     = np.asarray(other_list_prof_header_logg)
    other_list_prof_header_mass     = np.asarray(other_list_prof_header_mass)
    other_list_prof_header_R        = np.asarray(other_list_prof_header_R)
    other_list_prof_header_Xc       = np.asarray(other_list_prof_header_Xc)
    other_list_prof_header_Yc       = np.asarray(other_list_prof_header_Yc)
    other_list_prof_data_vrot       = np.asarray(other_list_prof_data_vrot)
    other_list_prof_data_surface_he4= np.asarray(other_list_prof_data_surface_he4)
    other_list_prof_data_surface_n14= np.asarray(other_list_prof_data_surface_n14)
  
    
  ####################################
  # Define the Main Plotting Work Frame
  ####################################
  if not os.path.isdir('jpg/'): os.mkdir('jpg/')
  
  # Now put a sliding point on the track for the ref profile
  for i_point in range(n_ref_dic_prof):
    fig = plt.figure(figsize=(12,9), dpi=60)
    plt.subplots_adjust(left=0.06, right=0.99, bottom=0.07, top=0.99, hspace=0.20, wspace=0.20)
    
    ax1 = fig.add_subplot(411)    # For dP Grande
    ax2 = fig.add_subplot(423)    # For dP Klein (i.e. zoom)
    ax3 = fig.add_subplot(234)    # For the Kiel diagram
    ax4 = fig.add_subplot(438)    # For the Brunt-Vaisala diagram Grande
    ax5 = fig.add_subplot(4,3,11) # For the Brunt-Vaisala diagram Klein
    ax6 = fig.add_subplot(439)    # For the 'Reference' Diffiusive Mixing diagram
    ax7 = fig.add_subplot(4,3,12) # For the 'Other' Diffiusive Mixing diagram
    inf = fig.add_subplot(424)    # For the Information

    #print list_lbls    
    #ref_label = list_lbls[ref_indx]
    #del list_lbls[ref_indx]
    #print list_lbls
    #other_label = list_lbls
    
    left = 0.04; top = 0.88; shift = 0.20
     
    ####################################
    # Start Filling up all panels
    ####################################
    # The dP Grande
    if dP_by_P: 
      ax1.set_xlabel(r'Period [d]')
      ax1.set_xlim(P_grande_from, P_grande_to)
    if dP_by_freq: 
      ax1.set_xlabel(r'Frequency [Hz]')
      ax1.set_xlim(freq_grande_from, freq_grande_to)
    ax1.set_ylabel(r'Period Spacing')
    ax1.set_ylim(dP_grande_from, dP_grande_to)
    
    if dP_by_P:    dP_key = 'el_' + el_str + '_gm_P'
    if dP_by_freq: dP_key = 'el_' + el_str + '_gm_freq_re'
    # plot the reference track
    grande_xaxis = ref_list_dic_dP[i_point][dP_key][:-1] * sec_to_d
    grande_yaxis = ref_list_dic_dP[i_point]['el_'+el_str+'_gm_dP']
    ax1.plot(grande_xaxis, grande_yaxis, linestyle='solid', color='black')
    ax1.scatter(grande_xaxis, grande_yaxis, marker='o', s=9, color='black')
    # plot the other track(s)
    grande_xaxis = other_list_dic_dP[i_point][dP_key][:-1] * sec_to_d
    grande_yaxis = other_list_dic_dP[i_point]['el_'+el_str+'_gm_dP']
    #ax1.plot(grande_xaxis, grande_yaxis, linestyle='dashed', color='blue')
    #ax1.scatter(grande_xaxis, grande_yaxis, marker='o', s=9, color='blue')

    # The dP Klein
    if dP_by_P: 
      ax2.set_xlabel(r'Period [d]')
      ax2.set_xlim(P_klein_from, P_klein_to)
    if dP_by_freq: 
      ax2.set_xlabel(r'Frequency [Hz]')
      ax2.set_xlim(freq_klein_from, freq_klein_to)
    ax2.set_ylabel(r'$\Delta P_n$ [sec]')
    ax2.set_ylim(dP_klein_from, dP_klein_to)

    # plot the reference track
    grande_xaxis = ref_list_dic_dP[i_point][dP_key][:-1] * sec_to_d
    grande_yaxis = ref_list_dic_dP[i_point]['el_'+el_str+'_gm_dP']
    ax2.plot(grande_xaxis, grande_yaxis, linestyle='solid', color='black')
    ax2.scatter(grande_xaxis, grande_yaxis, marker='o', s=9, color='black')
    # plot the other track(s)
    grande_xaxis = other_list_dic_dP[i_point][dP_key][:-1] * sec_to_d
    grande_yaxis = other_list_dic_dP[i_point]['el_'+el_str+'_gm_dP']
    #ax2.plot(grande_xaxis, grande_yaxis, linestyle='dashed', color='blue')
    #ax2.scatter(grande_xaxis, grande_yaxis, marker='o', s=9, color='blue')
    
    
    # The Kiel Diagram
    ax3.set_xlim(logT_from, logT_to)
    ax3.set_xlabel(r'Effective Temperature $\log T_{\rm eff}$')
    ax3.set_ylim(logg_from, logg_to)
    ax3.set_ylabel(r'Surface Gravity $\log g$ [cgs]')
  
    # plot the reference track
    ax3.plot(ref_hist_log_Teff, ref_hist_logg, linestyle='solid', color='black', label=list_lbls[ref_indx])
    ax3.scatter(ref_list_prof_header_log_Teff, ref_list_prof_header_logg, marker='o', s=9, color='black')
    # plot the other track(s)
    ax3.plot(other_hist_log_Teff, other_hist_logg, linestyle='dashed', color='blue', label=list_lbls[1])
    ax3.scatter(other_list_prof_header_log_Teff, other_list_prof_header_logg, marker='o', s=9, color='blue')
    if list_lbls:
      #leg3 = ax3.legend(loc=4, shadow=True, fancybox=True, fontsize='small')
      leg3 = ax3.legend(loc=4, shadow=True, fancybox=True)

    # put a single data point associated with a single profile    
    ax3.scatter([ref_list_prof_header_log_Teff[i_point]], ref_list_prof_header_logg[i_point], marker='*',
               s=50, color='red')
    ax3.scatter([other_list_prof_header_log_Teff[i_point]], other_list_prof_header_logg[i_point], marker='*',
               s=50, color='red')

    # The Brunt-Vaisala profile
    ref_dic_prof = ref_list_dic_prof[i_point]['prof']
    ref_dic_prof_names = ref_dic_prof.dtype.names
    if not all([ ('logT' in ref_dic_prof_names), ('brunt_N2' in ref_dic_prof_names) ]):
      message = 'Error: animated: anim_hist_prof_dP: logT OR brunt_N2 are missing from profile columns!'
      raise SystemExit, message
    ref_prof_logT = ref_dic_prof['logT']
    ref_prof_N2 = ref_dic_prof['brunt_N2'] # Hz^2
    ref_prof_N2[ref_prof_N2<=0] = 1e-20
    ref_prof_N = np.sqrt(ref_prof_N2)
    ref_prof_logN = np.log10(ref_prof_N)
    
    other_dic_prof = other_list_dic_prof[i_point]['prof']
    other_prof_logT = other_dic_prof['logT']
    other_prof_N2 = other_dic_prof['brunt_N2']
    other_prof_N2[other_prof_N2<=0] = 1e-20
    other_prof_N = np.sqrt(other_prof_N2)
    other_prof_logN = np.log10(other_prof_N)
    
    if BV_by_mass: 
      BV_x_label = r'Mass [$M_\odot$]'
      if not BV_x_from: BV_x_from = 0.0
      if not BV_x_to:   BV_x_to   = 0.5 * ref_prof_mass
    if BV_by_logT: 
      BV_x_label = r'Temperature $\log T$'
      BV_x_from = np.max(ref_prof_logT)
      if not BV_x_to:   BV_x_to   = 7.0
    if BV_by_logRho: 
      BV_x_label = r'Density $\log\rho$'
      if not BV_x_from: BV_x_from = np.max(ref_dic_prof['logRho'])
      if not BV_x_to:   BV_x_to   = 7.0
    
    
    # BV Grande
    ax4.set_xlim(BV_x_from, 4.0)
    ax4.set_xticklabels(())
    ax4.set_ylim(BV_y_from, BV_y_to)
    ax4.set_ylabel(r'$\log N$ [Hz]')
    ax4.plot(ref_prof_logT, ref_prof_logN, color='black', linestyle='solid', lw=1.2)
    ax4.plot(other_prof_logT, other_prof_logN, color='blue', linestyle='dashed', lw=1.2)
    
    # BV Klien
    ax5.set_xlim(BV_x_from, BV_x_to)
    ax5.set_xlabel(BV_x_label)
    ax5.set_ylim(BV_y_from, BV_y_to)
    ax5.set_ylabel(r'$\log N$')
    ax5.plot(ref_prof_logT, ref_prof_logN, color='black', linestyle='solid')
    ax5.plot(other_prof_logT, other_prof_logN, color='blue', linestyle='dashed')
    
    # The Diffiusive Mixing 
    log_D_avail = [ ('log_D_mix' in ref_dic_prof_names), ('log_D_conv' in ref_dic_prof_names), 
                ('log_D_ovr' in ref_dic_prof_names), ('log_D_semi' in ref_dic_prof_names), 
                ('log_D_mix_non_rotation' in ref_dic_prof_names) ]
    if not all(log_D_avail):
      print 'Error: animated: anim_hist_prof_dP: Missing log_D_... column: ', log_D_avail
      
    ref_prof_loD_mix   = ref_dic_prof['log_D_mix']
    ref_prof_logD_conv = ref_dic_prof['log_D_conv']
    ref_prof_logD_ovr  = ref_dic_prof['log_D_ovr']
    ref_prof_logD_semi = ref_dic_prof['log_D_semi']
    ref_prof_logD_rot  = ref_prof_loD_mix - (ref_prof_logD_conv+ref_prof_logD_ovr+ref_prof_logD_semi)
    other_prof_logD_mix  = other_dic_prof['log_D_mix']
    other_prof_logD_conv = other_dic_prof['log_D_conv']
    other_prof_logD_ovr  = other_dic_prof['log_D_ovr']
    other_prof_logD_semi = other_dic_prof['log_D_semi']
    other_prof_logD_rot  = other_prof_logD_mix - (other_prof_logD_conv+other_prof_logD_ovr+other_prof_logD_semi)
   
    if logD_by_mass: logD_x_label = r'Mass [$M_\odot$]'
    if logD_by_logT: 
      logD_x_label = r'Temperature $\log T$'
      logD_x_from = np.max(ref_prof_logT)
    if logD_by_logRho: 
      logD_x_label = r'Density $\log\rho$ [g.cm$^{-3}$]'
      logD_x_from = np.max(ref_dic_prof['logRho'])
    
    # logD Reference 
    ax6.set_xlim(logD_x_from, logD_x_to)
    ax6.set_xticklabels(())
    ax6.set_ylim(logD_from, logD_to)
    ax6.set_ylabel(r'$\log D_{\rm mix}$ [cm$^2$/sec]')
    ax6.plot(ref_prof_logT, ref_prof_logD_conv, lw=1.2, color='black')
    ax6.plot(ref_prof_logT, ref_prof_logD_rot, lw=1.2, color='red')    
    ax6.plot(ref_prof_logT, ref_prof_logD_ovr, lw=1.2, color='green')
    ax6.plot(ref_prof_logT, ref_prof_logD_semi, lw=1.2, color='grey')    
    ax6.annotate(r'(Ref)', xy=(0.80, 0.85), xycoords='axes fraction')

    ax7.set_xlim(logD_x_from, logD_x_to)
    ax7.set_xlabel(logD_x_label)
    ax7.set_ylim(logD_from, logD_to)
    ax7.set_ylabel(r'$\log D_{\rm mix}$')
    ax7.plot(other_prof_logT, other_prof_logD_conv, lw=1.2, color='black', label=r'Conv')
    ax7.plot(other_prof_logT, other_prof_logD_rot, lw=1.2, color='red', label=r'Rot')    
    ax7.plot(other_prof_logT, other_prof_logD_ovr, lw=1.2, color='green', label=r'Over')
    ax7.plot(other_prof_logT, other_prof_logD_semi, lw=1.2, color='grey', label=r'Semi')
    #leg7 = ax7.legend(loc=1, shadow=True, fancybox=True, fontsize='small')    
    leg7 = ax7.legend(loc=1, shadow=True, fancybox=True)    
    
    ####################################
    # Add Information on the Plot
    ####################################
    inf.set_xticklabels(())
    inf.set_yticklabels(())
    txt_mass = '%16s: %5.2f ... %5.2f' % (r'M [M$_\odot$]', ref_list_prof_header_mass[i_point], other_list_prof_header_mass[i_point])
    inf.annotate(txt_mass, xy=(left, top), xycoords='axes fraction', color='black', fontsize='medium')
    txt_Teff = '%16s: %5d ... %5d' % (r'T$_{\rm eff}$ [K]', 10.0**ref_list_prof_header_log_Teff[i_point], 10.0**other_list_prof_header_log_Teff[i_point])
    inf.annotate(txt_Teff, xy=(left, top-shift), xycoords='axes fraction', color='black', fontsize='medium')
    txt_Xc = '%16s: %6.4f ... %6.4f' % (r'X$_{\rm c}$', ref_list_prof_header_Xc[i_point], other_list_prof_header_Xc[i_point])
    inf.annotate(txt_Xc, xy=(left, top-2*shift), color='black', xycoords='axes fraction', fontsize='medium')
    txt_Yc = '%16s: %6.4f ... %6.4f' % (r'Y$_{\rm c}$', ref_list_prof_header_Yc[i_point], other_list_prof_header_Yc[i_point])
    inf.annotate(txt_Yc, xy=(left, top-3*shift), color='black', xycoords='axes fraction', fontsize='medium')
    txt_vrot = '%16s: %3d ... %3d' % (r'$v_{\rm rot}$ [km/s]', ref_list_prof_data_vrot[i_point], other_list_prof_data_vrot[i_point])
    inf.annotate(txt_vrot, xy=(left, top-4*shift), xycoords='axes fraction', color='black', fontsize='medium')

    # Store every snapshot as a .jpg file
    frame_name = 'jpg/' + jpg_name + "{:04d}".format(i_point) + '.jpg'
    plt.savefig(frame_name, dpi=60)
    #print '  %s saved.' % (frame_name, )
    plt.close('all')

  print ' - anim_hist_prof_dP: Done'
  
  return None


#=================================================================================================================


"""
This module provides several functionalities to generate outputs and write them as files on disk.
"""

import sys, os, glob, logging
import numpy as np
import h5py
import read, param_tools, plot_commons

Msun = 1.9892e33
Rsun = 6.9598e10
Lsun = 3.8418e33

#=================================================================================================================
#=================================================================================================================
def dic_freq_to_gyre_out_h5(dic_freq):
  """
  The mesa_gyre.tar module manipulates the list of frequencies (for various models with varying rotation
  rate) iteratively, and at the end, it should be able to flush the results of the best model as a file
  containing all necessary information. This subroutine takes care of storing all default GYRE outputs
  in addition to the rotation information, if available.
  """
  filename = dic_freq['filename']

  #############################
  # Organize what to be written
  #############################
  headers  = ['M_star', 'R_star', 'L_star', 'freq_units']
  extras   = ['omega_crit', 'eta_rot', 'n_poly']
  columns  = ['l', 'l_0', 'n_p', 'n_g', 'n_pg', 'omega', 'omega_int', 'freq', 
              'f_T', 'f_g', 'psi_T', 'psi_g', 'beta', 'E', 'E_norm', 'W']

  num_attr = len(headers)
  for attr in extras:
    if attr in dic_freq: num_attr += 1

  #############################
  # Create the HDF5 file
  #############################
  f = h5py.File(name=filename, mode='w', libver='latest')
  f.attrs['check'] = 12

  #############################
  # Put the dic items to h5 file
  #############################
  # for attr in headers:
  #     # dset = f.create_dataset(attr, (1, ))
  #     group.attrs[attr] = dic_freq[attr]
  # dset.attrs['M_star'] = dic_freq['M_star']
  # dset.attrs['R_star'] = dic_freq['R_star']
  # dset.attrs['L_star'] = dic_freq['L_star']

  if 'omega_crit' in dic_freq: omega_crit = dic_freq['omega_crit']
  if 'eta_rot' in dic_freq:    omega_crit = dic_freq['eta_rot']
  if 'n_poly' in dic_freq:     omega_crit = dic_freq['n_poly']

  # if dic_freq.has_keys('l'):

  f.flush()
  f.close()

  return None

#=================================================================================================================
def save_contour_vertices(contour, file_out):
  """
  This routine receives the matplotlib.contour() object, and extracts the vertices by 
  calling get_contour_vertices(). Then, it writes the vertices to an ASCII file.
  @param contour: a data object returned by a call to plt.contour()
  @type contour: no clue for now 
  @param file_out: full path to where the ASCII file will be stored
  @type file_out: string 
  @return: None 
  @rtype: None
  """
  tup_x_y = plot_commons.get_contour_vertices(contour)
  x       = tup_x_y[0]
  y       = tup_x_y[1]
  n       = len(x)
  data    = []
  for i in range(n):
    line  = '{0:0.6f}   {1:0.6f} \n'.format(x[i], y[i])
    data.append(line)

  with open(file_out, 'w') as w: w.writelines(data)

  print ' - save_contour_vertices: saved {0}'.format(file_out)

  return None

#=================================================================================================================
def show_best_models(dic, chisq_by):
  """
  On the standard output (shell or terminal), print out the parameters of the bes model, including the 
  different chi-square values obtained
  @param dic: dictionary of the best model, containing several fields:
    - filename
    - input parameters, like mass, overshooting, etc.
    - different reduced chi-square values, like reduced_chisq_seism_f_and_dP, etc.
  @type dic: dictionary
  @return: None
  @rtype: None
  """
  # keys = dic.keys()
  # chisq_keys = [key for key in keys if key.startswith('red')]
  filename   = dic['filename']
  dic_param  = param_tools.get_param_from_single_gyre_filename(filename)

  list_print = []
  list_print.append('{0}\n'.format(filename))
  list_print.append('{0}: {1:.2f}\n'.format(chisq_by, dic[chisq_by]))
  with open('best.chisq', 'w') as w: w.writelines(list_print)

  print ' - write: show_best_models:'
  # print '   filename: {0}'.format(filename)
  for key, value in dic_param.items():
    print '   {0}: {1}'.format(key, value)
  print 
  print '   Chi^2 {0}: {1:.2f}'.format(chisq_by, dic[chisq_by])
  print
  
  return None

#=================================================================================================================
def list_chisq_to_ascii(inputs, chisq_by, extra_keys=[], max_row=None, file_out='Output.chisq'):
  """
  Dump the output from e.g. chisq.seism_dP() to an ascii table
  @param: inputs: list of inputs updated with the chisquare values using mesa_gyre.chisq module
          Note: This input is either a list of chisquare dictionaries, or a numpy record array with the same info,
                where each row carries the full info of a GYRE output model, and the chi-square values.
  @type inputs: list of dictionaries or numpy recarray
  @param: chisq_by: sort and list the chisq values based on this field. It must be present in the input dictionaries
  @type chisq_by: string
  @param extra_keys: Additional keys for additonal columns in the table. The key length is limited to 30 characters;
           This can be perhaps improved to a more flexible width in the near future?
           Note: The elements of extra_keys should be already avalable in the header of inputs.
  @type extra_keys: list of strings.
  @param max_row: the number of first best rows to write in a file. If the list has less dictionaries than this, the
       minimum number of available rows will be dumped to the file. default=None
  @type max_row: integer
  @return: None
  @rtype: None
  """
  #############################
  # Some Decisions!
  #############################
  n_dic = len(inputs)
  if n_dic == 0:
    logging.error('write: list_chisq_to_ascii: Input list is empty')
    raise SystemExit

  input_is_dic  = False
  input_is_rec  = False
  if isinstance(inputs[0], dict):
    input_is_dic  = True 
    keys          = inputs[0].keys()
  else:
    input_is_rec = True
    keys         = inputs[0].dtype.names

  if chisq_by not in keys:
    logging.error('write: list_chisq_to_ascii: {0} not availabe in dictionaries'.format(chisq_by))
    print ' - Error: write: list_chisq_to_ascii: {0} not available'.foramt(chisq_by)
    raise SystemExit

  # sort the list based on "chisq_by"
  list_chisq = np.array([ dic[chisq_by] for dic in inputs ])
  ind_sort   = np.argsort(list_chisq)
  if input_is_dic: inputs     = [inputs[i] for i in ind_sort]
  if input_is_rec: inputs     = inputs[ind_sort]

  if max_row is None:
    n_print_best = n_dic
  else:
    n_print_best = min([max_row, n_dic])
  print ' - write: list_chisq_to_ascii: Storing {0} records out of {1} inputs'.format(n_print_best, n_dic)

  #############################
  # Flags and Header names
  #############################
  has_Mini     = 'M_ini' in keys 
  has_eta      = 'eta' in keys
  has_ov       = 'ov' in keys
  has_sc       = 'sc' in keys
  has_Z        = 'Z' in keys 
  has_logD     = 'logD' in keys 
  has_evol     = 'evol' in keys 
  has_Evol     = 'Evol' in keys
  has_Yc       = 'Yc' in keys
  has_Xc       = 'Xc' in keys
  has_mod_num  = 'model_number' in keys 
  has_num      = 'Num' in keys
  has_M_star   = 'M_star' in keys 
  has_R_star   = 'R_star' in keys 
  has_L_star   = 'L_star' in keys
  has_freq_rot = 'freq_rot' in keys
  has_omega_rot= 'omega_rot' in keys
  has_eta_rot  = 'eta_rot' in keys  
  has_AIC_c    = 'AIC_c' in keys  

  header       = '' 
  if not has_Mini:  header += '{0:>10s}'.format('M_ini')
  if has_Mini:      header += '{0:>10s}'.format('M_ini')
  if has_eta:       header += '{0:>10s}'.format('eta')
  if has_ov:        header += '{0:>10s}'.format('ov')
  if has_sc:        header += '{0:>10s}'.format('sc')
  if has_Z:         header += '{0:>10s}'.format('Z')
  if has_logD:      header += '{0:>10s}'.format('logD')
  if has_evol:      header += '{0:>10s}'.format('Evol')
  if has_Evol:      header += '{0:>10s}'.format('Evol')
  if has_Yc:        header += '{0:>10s}'.format('Yc')
  if has_Xc:        header += '{0:>10s}'.format('Xc')
  if has_num:       header += '{0:>10s}'.format('Num')
  if has_mod_num:   header += '{0:>10s}'.format('Num')
  if has_M_star:    header += '{0:>10s}'.format('M_star')
  if has_R_star:    header += '{0:>10s}'.format('R_star')
  if has_L_star:    header += '{0:>15s}'.format('L_star')    # <-- Note: L_star is longer!
  if has_freq_rot:  header += '{0:>15s}'.format('freq_rot')  # freq_rot and omega_rot take similar names
  if has_omega_rot: header += '{0:>15s}'.format('freq_rot')  # freq_rot and omega_rot take similar names
  if has_eta_rot:   header += '{0:>10s}'.format('eta_rot')
  if has_AIC_c:     header += '{0:>10s}'.format('AIC_c')

  if extra_keys:
    for key in extra_keys:
      header += '{0:>30s}'.format(key)
  header += '{0:>30s}'.format(chisq_by)
  header += ' \n'
  lines  = [header]

  # print keys

  #############################
  # Iteratively, Gather the Rows
  #############################
  # below, obj can be a dic or a recarray row
  for i_obj, obj in enumerate(inputs):
    line = ''

    if input_is_dic:
      div_M_by = Msun
      div_R_by = Rsun 
      div_L_by = Lsun
    else:
      div_M_by = 1
      div_R_by = 1
      div_L_by = 1

    if not has_Mini:  line += '{0:10.4f}'.format(obj['M']) 
    if has_Mini:      line += '{0:10.4f}'.format(obj['M_ini']) 
    if has_eta:       line += '{0:10.4f}'.format(obj['eta'])
    if has_ov:        line += '{0:10.4f}'.format(obj['ov'])
    if has_sc:        line += '{0:10.4f}'.format(obj['sc'])
    if has_Z:         line += '{0:10.4f}'.format(obj['Z'])
    if has_logD:      line += '{0:10.4f}'.format(obj['logD'])
    if has_evol:      line += '{0:>10s}'.format(obj['evol'])        # Note: string format
    if has_Evol:      line += '{0:>10s}'.format(obj['Evol'])        # Note: string format
    if has_Yc:        line += '{0:10.4f}'.format(obj['Yc'])
    if has_Xc:        line += '{0:10.4f}'.format(obj['Xc'])
    if has_num:       line += '{0:10d}'.format(int(obj['Num'])) # Note: integer format
    if has_mod_num:   line += '{0:10d}'.format(int(obj['model_number'])) # Note: integer format
    if has_M_star:    line += '{0:10.4f}'.format(obj['M_star'] / div_M_by)
    if has_R_star:    line += '{0:10.4f}'.format(obj['R_star'] / div_R_by)
    if has_L_star:    line += '{0:15.4e}'.format(obj['L_star'] / div_L_by)         # Note: different format
    if has_freq_rot:  line += '{0:15.4e}'.format(obj['freq_rot'])
    if has_omega_rot: line += '{0:15.4e}'.format(obj['omega_rot'] / (2.0 * np.pi)) # <-- store freq_rot
    if has_eta_rot:   line += '{0:10.4f}'.format(obj['eta_rot'])
    if has_AIC_c:     line += '{0:10.2e}'.format(obj['AIC_c'])      # Note: different format

    if extra_keys:
      for key in extra_keys:
        line += '{0:30.4e}'.format(obj[key])

    line     += '{0:30.4e}'.format(obj[chisq_by])
    line     += ' \n'

    lines.append(line)

  #############################
  # Flush the lines to ASCII file
  #############################
  with open(file_out, 'w') as w: w.writelines(lines)
  print ' - write: list_chisq_to_ascii The {0} best solutions based on "{1}" saved in: {2}'.format(
            n_print_best, chisq_by, file_out)

  return None

#=================================================================================================================
def prof_to_graco_in(list_dic_prof, save_dir='', path_lista='', factor=None):
  """
  To write the MESA output profile columns into GraCo input format.
  WARNING: alpha_MLT is set to 1.8. Change this manually if it is changed in MESA inlist
  WARNING: We do not have the information about the location of the largest convective zone flowing from MESA to profile
           columns. Therefore, the GLOB(7) and GLOB(8) are assessed only in the core
  @param list_dic_prof: list of profile data retrieved from MESA profile model(s). The full required gyre input columns
     must already be availabe in the input profile.
  @type list_dic_prof: list of dictionaries
  @param save_dir: full path to the directory to store the converted gyre_in files.
  @type save_dir: string
  @param path_lista: full path to save the file called "lista" that stores the names of input files
  @type path_lista: string
  @param factor: increase the number of mesh by this factor using cubic interpolation; default=None
  @type factor: integer
  @return: list of the new .gyre filenames produced and saved in the given path
  @rtype: list of string
  """
  from ivs.stellar_evolution.puls import generate_newx
  from ivs.sigproc.interpol import local_interpolation

  n_dic = len(list_dic_prof)
  if not save_dir:
    message = 'Error: write: prof_to_graco_in: the "save_dir" argument has length zero!'
    raise SystemExit, message
  if not os.path.exists(save_dir):
    print ' - write: prof_to_graco_in: %s directory created' % (save_dir, )
    os.makedirs(save_dir)
  if save_dir[-1] != '/': save_dir += '/'
  if not os.path.exists(path_lista):
    print ' - write: prof_to_graco_in: %s directory created' % (path_lista, )
    os.makedirs(path_lista)
  if path_lista[-1] != '/': path_lista += '/'

  sample_prof_names = list_dic_prof[0]['prof'].dtype.names
  required_keys = ['radius', 'lnq', 'temperature', 'pressure', 'density', 'gradT', 'luminosity',
                   'kappa', 'epsilon', 'gamma1', 'grada', 'chiT_div_chiRho', 'Cp', 'ln_free_e',
                   'A2', 'omega', 'kappa_T', 'kappa_rho', 'epsilon_T', 'epsilon_rho', 'Pg_div_P',
                   'gradr', 'h1', 'h2', 'he3', 'he4', 'li7', 'be7', 'c12', 'c13', 'n14', 'n15',
                   'o16', 'o17', 'be9', 'si28']
  dic_avail_keys = {}
  for key in sample_prof_names:
    dic_avail_keys[key] = 0.0
  dk = read.check_key_exists(dic_avail_keys, required_keys)  # dk == dic_keys
  for key, value in dk.items():
    if value == False:
      message = 'Error: write: prof_to_graco_in: key=%s not present in sample "prof" data' % (key, )
      raise SystemExit, message

  nl = '\n'
  fmt = '%16.9E' * 5  +' %s'
  a_mlt = 1.80
  list_files = []

  for i_dic, dic in enumerate(list_dic_prof):
    filename = dic['filename']
    prof     = dic['prof']
    header   = dic['header']
    nn       = len(prof)

    if factor is not None:
      do_interp = True
      nn = long(nn * factor)
    else:
      do_interp = False

    ind_slash= filename.rfind('/')
    path_file= filename[ : ind_slash+1]
    file_core= filename[ind_slash+1 : ]

    radius          = prof['radius'] * Rsun
    lnq             = prof['lnq']
    temperature     = prof['temperature']
    pressure        = prof['pressure']
    density         = prof['density']
    gradT           = prof['gradT']
    luminosity      = prof['luminosity']
    kappa           = prof['kappa']
    epsilon         = prof['epsilon']
    gamma1          = prof['gamma1']
    grada           = prof['grada']
    chiT_div_chiRho = prof['chiT_div_chiRho']
    Cp              = prof['Cp']
    ln_free_e       = prof['ln_free_e']
    A2              = prof['A2']
    omega           = prof['omega']
    kappa_T         = prof['kappa_T']
    kappa_rho       = prof['kappa_rho']
    epsilon_T       = prof['epsilon_T']
    epsilon_rho     = prof['epsilon_rho']
    Pg_div_P        = prof['Pg_div_P']
    gradr           = prof['gradr']
    h1              = prof['h1']
    h2              = prof['h2']
    he3             = prof['he3']
    he4             = prof['he4']
    li7             = prof['li7']
    be7             = prof['be7']
    c12             = prof['c12']
    c13             = prof['c13']
    n14             = prof['n14']
    n15             = prof['n15']
    o16             = prof['o16']
    o17             = prof['o17']
    be9             = prof['be9']
    si28            = prof['si28']

    M_star          = header['star_mass'] * Msun
    R_star          = radius[0]
    L_star          = luminosity[0]

    initial_z       = header['initial_z']
    # The following XYZ correction is taken from the way it is done in run_star_extras as of January 2014
    initial_x       = 0.710
    initial_y       = 0.276
    sum_XYZ_ini     = initial_x + initial_y + initial_z
    residual        = 1.0 - sum_XYZ_ini
    multi           = residual/(initial_x+initial_y)
    initial_x       = initial_x * (1.0 + multi)
    initial_y       = initial_y * (1.0 + multi)

    center_h1       = header['center_h1']
    center_he3      = header['center_he3']
    center_he4      = header['center_he4']
    Teff            = header['Teff']
    star_age        = header['star_age'] / 1e6     # Age in Myr

    lines = []
    lines.append('GraCo file created from MESA profile' + nl)
    lines.append(path_file + nl)
    lines.append(file_core + nl)
    lines.append(' ' + nl)
    lines.append('14 H1 H2 He3 He4 Li7 Be7 C12 C13 N14 N15 O16 O17 Be9 Si28' + nl)
    lines.append('%10d%10d%10d%10d%10d%s' % (nn, 15, 22, 14, 300, nl))

    lines.append(fmt % (M_star, R_star, L_star, initial_z, initial_x, nl))
    lines.append(fmt % (a_mlt, center_h1, center_he3+center_he4, 0.0, 0.0, nl))
    lines.append(fmt % (star_age, omega[0], 0.0, Teff, 0.0, nl))

    if do_interp:
      R = radius[::-1] / max(radius)
      brunt_A = np.sqrt(A2[::-1])
      brunt_A[np.isnan(brunt_A)] = 0.0
      new_R, weights = generate_newx(R, brunt_A, n_new=nn, type_mode='g', smooth_bruntA=0,
                            smooth_mesh=9, full_output=True)

      lnq              = local_interpolation(new_R, R, lnq[::-1], full_output=True)
      temperature      = local_interpolation(new_R, R, temperature[::-1], full_output=True)
      pressure         = local_interpolation(new_R, R, pressure[::-1], full_output=True)
      density          = local_interpolation(new_R, R, density[::-1], full_output=True)
      gradT            = local_interpolation(new_R, R, gradT[::-1], full_output=True)
      luminosity       = local_interpolation(new_R, R, luminosity[::-1], full_output=True)
      kappa            = local_interpolation(new_R, R, kappa[::-1], full_output=True)
      epsilon          = local_interpolation(new_R, R, epsilon[::-1], full_output=True)
      gamma1           = local_interpolation(new_R, R, gamma1[::-1], full_output=True)
      grada            = local_interpolation(new_R, R, grada[::-1], full_output=True)
      chiT_div_chiRho  = local_interpolation(new_R, R, chiT_div_chiRho[::-1], full_output=True)
      Cp               = local_interpolation(new_R, R, Cp[::-1], full_output=True)
      ln_free_e        = local_interpolation(new_R, R, ln_free_e[::-1], full_output=True)
      A2               = local_interpolation(new_R, R, A2[::-1], full_output=True)
      omega            = local_interpolation(new_R, R, omega[::-1], full_output=True)
      kappa_T          = local_interpolation(new_R, R, kappa_T[::-1], full_output=True)
      kappa_rho        = local_interpolation(new_R, R, kappa_rho[::-1], full_output=True)
      epsilon_T        = local_interpolation(new_R, R, epsilon_T[::-1], full_output=True)
      epsilon_rho      = local_interpolation(new_R, R, epsilon_rho[::-1], full_output=True)
      Pg_div_P         = local_interpolation(new_R, R, Pg_div_P[::-1], full_output=True)
      gradr            = local_interpolation(new_R, R, gradr[::-1], full_output=True)
      h1               = local_interpolation(new_R, R, h1[::-1], full_output=True)
      h2               = local_interpolation(new_R, R, h2[::-1], full_output=True)
      he3              = local_interpolation(new_R, R, he3[::-1], full_output=True)
      he4              = local_interpolation(new_R, R, he4[::-1], full_output=True)
      li7              = local_interpolation(new_R, R, li7[::-1], full_output=True)
      be7              = local_interpolation(new_R, R, be7[::-1], full_output=True)
      c12              = local_interpolation(new_R, R, c12[::-1], full_output=True)
      c13              = local_interpolation(new_R, R, c13[::-1], full_output=True)
      n14              = local_interpolation(new_R, R, n14[::-1], full_output=True)
      n15              = local_interpolation(new_R, R, n15[::-1], full_output=True)
      o16              = local_interpolation(new_R, R, o16[::-1], full_output=True)
      o17              = local_interpolation(new_R, R, o17[::-1], full_output=True)
      be9              = local_interpolation(new_R, R, be9[::-1], full_output=True)
      si28             = local_interpolation(new_R, R, si28[::-1], full_output=True)
      radius           = new_R * max(radius)

    if factor is None:
      ind_surf = 0
      ind_core = nn
      ind_step = 1
    else:
      ind_surf = nn - 1
      ind_core = -1
      ind_step = -1

    for i in range(ind_surf, ind_core, ind_step):
      lines.append(fmt % (radius[i], lnq[i], temperature[i], pressure[i], density[i], nl))
      lines.append(fmt % (gradT[i], luminosity[i], kappa[i], epsilon[i], gamma1[i], nl))
      lines.append(fmt % (grada[i], chiT_div_chiRho[i], Cp[i], ln_free_e[i], A2[i], nl))
      lines.append(fmt % (omega[i], kappa_T[i], kappa_rho[i], epsilon_T[i], epsilon_rho[i], nl))
      lines.append(fmt % (Pg_div_P[i], gradr[i], h1[i], h2[i], he3[i], nl))
      lines.append(fmt % (he4[i], li7[i], be7[i], c12[i], c13[i], nl))
      lines.append(fmt % (n14[i], n15[i], o16[i], o17[i], be9[i], nl))
      lines.append('%16.9E%s' % (si28[i], nl))

    ind_pt = filename.rfind('.')
    graco_file = filename[ind_slash+1 : ind_pt] + '.osc'
    graco_full_path = save_dir + graco_file
    list_files.append(graco_file[:-4] + nl)

    w = open(graco_full_path, 'w')
    w.writelines(lines)
    w.close()

  list_files.sort()
  lista = path_lista + 'lista'
  w = open(lista, 'w')
  w.writelines(list_files)
  w.close()

  return list_files

#=================================================================================================================
def prof_to_gyre_in(list_dic_prof, save_dir=''):
  """
  To write the MESA output profile columns into GYRE input format.
  @param list_dic_prof: list of profile data retrieved from MESA profile model(s). The full required gyre input columns
     must already be availabe in the input profile.
  @type list_dic_prof: list of dictionaries
  @param save_dir: full path to the directory to store the converted gyre_in files.
  @type save_dir: string
  @return: list of the new .gyre filenames produced and saved in the given path
  @rtype: list of string
  """
  n_dic = len(list_dic_prof)
  if not save_dir:
    message = 'Error: write: prof_to_gyre_in: the "save_dir" argument has length zero!'
    raise SystemExit, message
  if not os.path.exists(save_dir):
    print ' - write: prof_to_gyre_in: %s directory created' % (save_dir, )
    os.makedirs(save_dir)
    logging.info('write: prof_to_gyre_in: {0} directory created'.format(save_dir))
  if save_dir[-1] != '/': save_dir += '/'

  logging.info('prof_to_gyre_in: Converting {0} profiles to gyre_in'.format(n_dic))

  sample_prof_names = list_dic_prof[0]['prof'].dtype.names
  required_keys = ['mass', 'radius', 'luminosity', 'q_div_xq', 'pressure', 'density', 'temperature',
                   'brunt_N2', 'gamma1', 'grada', 'chiT_div_chiRho', 'gradT', 'kappa', 'kappa_rho',
                   'kappa_T', 'epsilon', 'epsilon_rho', 'epsilon_T', 'omega']
  dic_avail_keys = {}
  for key in sample_prof_names:
    dic_avail_keys[key] = 0.0
  dk = read.check_key_exists(dic_avail_keys, required_keys)  # dk == dic_keys
  for key, value in dk.items():
    if value == False:
      message = 'Error: write: prof_to_gyre_in: key=%s not present in sample "prof" data' % (key, )
      raise SystemExit, message

  list_filenames = []
  ncol = 19
  nl   = '\n'

  for i_dic, dic in enumerate(list_dic_prof):
    filename = dic['filename']
    prof = dic['prof']
    header = dic['header']
    nn   = len(prof)

    mass            = prof['mass'] * Msun
    radius          = prof['radius'] * Rsun
    luminosity      = prof['luminosity']
    q_div_xq        = prof['q_div_xq']
    pressure        = prof['pressure']
    Rho             = prof['density']
    temperature     = prof['temperature']
    brunt_N2        = prof['brunt_N2']
    gamma1          = prof['gamma1']
    grada           = prof['grada']
    gradT           = prof['gradT']
    chiT_div_chiRho = prof['chiT_div_chiRho']
    kappa           = prof['kappa']
    kappa_rho       = prof['kappa_rho']
    kappa_T         = prof['kappa_T']
    epsilon         = prof['epsilon']
    epsilon_rho     = prof['epsilon_rho']
    epsilon_T       = prof['epsilon_T']
    omega           = prof['omega']

    M_star          = mass[0]
    R_star          = radius[0]
    L_star          = luminosity[0]

    # write the above columns in gyre_in format as a list
    gyre_lines = []
    write_fmt = '%6d' + (ncol-1) * '     %22.16E' + '%s'

    M_str  = str(M_star)
    M_exp  = M_str.find('e+')
    M_conv = '0.' + M_str[0] + M_str[2:M_exp] + 'E+' + str(int(M_str[M_exp+2:])+1)
    R_str  = '%17.11E' % (R_star, )
    R_exp  = R_str.find('E+')
    R_conv = '0.' + R_str[0] + R_str[2:R_exp] + 'E+' + str(int(R_str[R_exp+2:])+1)
    L_str  = str(L_star)
    L_exp  = L_str.find('e+')
    L_conv = '0.' + L_str[0] + L_str[2:L_exp] + 'E+' + str(int(L_str[L_exp+2:])+1)
    File_Header = '%6d  %s  %s  %s  %4d %s' % (nn, M_conv, R_conv, L_conv, ncol, nl)
    gyre_lines.append(File_Header)

    #fwd_ind = range(0, nn)
    bwd_ind = range(nn-1, -1, -1)
    for i in bwd_ind:
      ind = nn - i
      line = write_fmt % (ind, radius[i], q_div_xq[i], luminosity[i], pressure[i], temperature[i], Rho[i], gradT[i],
                          brunt_N2[i], gamma1[i], grada[i], chiT_div_chiRho[i], kappa[i], kappa_T[i], kappa_rho[i],
                          epsilon[i], epsilon_T[i], epsilon_rho[i], omega[i], nl)
      gyre_lines.append(line)

    filename = filename[filename.rfind('/')+1 : filename.rfind('.')]
    filename_out = save_dir + filename + '.gyre'
    list_filenames.append(filename_out)
    logging.info('prof_to_gyre_in: {0} created'.format(filename_out))

    w = open(filename_out, 'w')
    w.writelines(gyre_lines)
    w.close()

  print ' - write: prof_to_gyre_in: %s .gyre files saved in %s' % (len(list_filenames), save_dir)

  return list_filenames

#=================================================================================================================
def gyre_in_to_ascii(list_filenames, save_dir=None):
  """
  Read a list of input GYRE files and write them as ascii
  @param list_filenames: list of full path to individual gyre input files
  @type list_filenames: list of string
  """
  n_files = len(list_filenames)
  if n_files == 0:
    logging.error('write: gyre_in_to_ascii: Inut list is empty')
    raise SystemExit
  if save_dir is None:
    save_dir = './'
  else:
    if not os.path.exists(save_dir): os.makedirs(save_dir)
    if save_dir[-1] != '/': save_dir += '/'

  for i_file, a_file in enumerate(list_filenames):
    dic    = read.read_gyre_in(a_file)
    rec    = dic['gyre_in']
    nn     = dic['nn']
    M_star = dic['M_star']
    R_star = dic['R_star']
    L_star = dic['L_star']
    ncol   = dic['ncol']

    # identify attributes and write them as header, one per line
    list_headers   = ['nn', 'ncol', 'M_star', 'R_star', 'L_star']
    column_headers = ''
    list_columns   = rec.dtype.names
    for i_col, col_name in enumerate(list_columns): column_headers += '{0:<30s}'.format(col_name)
    column_headers += '\n'

    lines = []
    for name in list_headers:
      lines.append('{0} = {1}\n'.format(name, dic[name]))
    lines.append('\n')
    lines.append(column_headers)

    # identify datasets and write them as a data block
    n_rows = len(rec)
    for i_row, row in enumerate(rec):
      line = ''
      for i_col in range(ncol):
        line += '{0:<30.16e}'.format(row[i_col])
      line += '\n'
      lines.append(line)

    # output filename
    output_name = a_file.replace('.gyre', '.txt')
    ind_slash   = output_name.rfind('/')
    output_name = save_dir + output_name[ind_slash+1:]

    with open(output_name, 'w') as w: w.writelines(lines)
    logging.info('write: gyre_in_to_ascii: store: {0}'.format(output_name))
    print ' - write: gyre_in_to_ascii: store: {0}'.format(output_name)

  return None

#=================================================================================================================
def gyre_in_to_gsm(input, output):
  """
  Read the GYRE input file in ascii format (saved by MESA), and store it in the GSM (HDF5) format.
  Note: This routine is only compatible with GYRE input version "19", neither with earlier, nor with later ones.

  @param input: The full path to the input GYRE ASCII file
  @type input: str
  @param output: The full path to where the GSM file will be stored.
  @type output: str
  @return:: None
  """
  if not os.path.exists(input):
    logging.error('write: gyre_in_to_gsm: Input file does not exist.')
    sys.exit('write: gyre_in_to_gsm: Input file does not exist.')

  dic     = read.read_gyre_in(input)
  n       = dic['nn']
  M_star  = dic['M_star']
  R_star  = dic['R_star']
  L_star  = dic['L_star']
  ncol    = dic['ncol']
  D       = dic['gyre_in']

  if ncol != 19:
    logging.error('write: gyre_in_to_gsm: Input ASCII file is incompatible with version "19".')
    sys.exit('write: gyre_in_to_gsm: Input ASCII file is incompatible with version "19".')

  # compression leven is an integer between 0 and 9 (default 4), which we choose here.
  c       = 9

  with h5py.File(output, 'w') as h5:
    # Attributes
    h5.attrs.create('n', n, dtype=np.int64)
    h5.attrs.create('version', ncol, dtype=np.int32)
    h5.attrs.create('M_star', M_star, dtype=np.float64)
    h5.attrs.create('R_star', R_star, dtype=np.float64)
    h5.attrs.create('L_star', L_star, dtype=np.float64)

    # Datasets
    r         = h5.create_dataset('r', compression='gzip', compression_opts=c, data=D['radius'])
    w         = h5.create_dataset('w', compression='gzip', compression_opts=c, data=D['q_div_xq'])
    L_r       = h5.create_dataset('L_r', compression='gzip', compression_opts=c, data=D['luminosity'])
    P         = h5.create_dataset('p', compression='gzip', compression_opts=c, data=D['pressure'])
    rho       = h5.create_dataset('rho', compression='gzip', compression_opts=c, data=D['density'])
    T         = h5.create_dataset('T', compression='gzip', compression_opts=c, data=D['temperature'])
    N2        = h5.create_dataset('N2', compression='gzip', compression_opts=c, data=D['brunt_N2'])
    G1        = h5.create_dataset('Gamma_1', compression='gzip', compression_opts=c, data=D['gamma1'])
    grada     = h5.create_dataset('nabla_ad', compression='gzip', compression_opts=c, data=D['grada'])
    delta     = h5.create_dataset('delta', compression='gzip', compression_opts=c, data=D['chiT_div_chiRho'])
    nabla     = h5.create_dataset('nabla', compression='gzip', compression_opts=c, data=D['gradT'])
    eps       = h5.create_dataset('epsilon', compression='gzip', compression_opts=c, data=D['epsilon'])
    eps_rho   = h5.create_dataset('epsilon_rho', compression='gzip', compression_opts=c, data=D['epsilon_rho'])
    eps_T     = h5.create_dataset('epsilon_T', compression='gzip', compression_opts=c, data=D['epsilon_T'])
    kap       = h5.create_dataset('kappa', compression='gzip', compression_opts=c, data=D['kappa'])
    k_rho     = h5.create_dataset('kappa_rho', compression='gzip', compression_opts=c, data=D['kappa_rho'])
    k_T       = h5.create_dataset('kappa_T', compression='gzip', compression_opts=c, data=D['kappa_T'])
    Omega_rot = h5.create_dataset('Omega_rot', compression='gzip', compression_opts=c, data=D['omega'])

  logging.info('gyre_in_to_gsm: saved {0}'.format(output))
  
  return None

#=================================================================================================================
def mesa_columns_to_ascii(dic, list_fields=[], file_out=None):
  """
  MESA profiles and history files normally contain many columns. But, to write a selected columns in a file, you can call this function
  @param dic: dictionary containing MESA profiles/history
  @type dic: dictionary
  @param list_fields: the subsample of columns to choose and then write to disk. e.g. list_fields = ['center_h1', 'star_age']
  @type list_fields: list of strings
  @param file_out: full path to the output ascii file
  @type file_out: string
  @return: None
  @rtype: None
  """
  n_col = len(list_fields)
  if n_col == 0:
    message = 'Error: write: mesa_columns_to_ascii: input list is empty!'
    raise SystemExit, message
  if file_out is None:
    message = 'Error: write: mesa_columns_to_ascii: file_out is not provided. Nowhere to save'
    raise SystemExit, message

  if 'prof' in dic.keys(): data = dic['prof']
  if 'hist' in dic.keys(): data = dic['hist']
  names = data.dtype.names
  n_row = len(data)

  selected = []
  for i, name in enumerate(list_fields):
    if name not in names:
      message = 'Error: write: mesa_columns_to_ascii: %s not available in the input profile/history data' % (name, )
      raise SystemExit, message
    else:
      selected.append(data[name])

  lines = []
  header = ''
  for i in range(n_col): header += '%27s ' % (list_fields[i], )
  lines.append(header + '\n')
  for i in range(n_row):
    line = ''
    for j, name in enumerate(list_fields): line += '%27s ' % (data[i][name])
    lines.append(line + '\n')

  #with open(file_out, 'w') as w: w.writelines(lines)
  w = open(file_out, 'w')
  w.writelines(lines)
  w.close()

  return None

#=================================================================================================================
def dP_to_h5_table(recarr, prefix=None, save_dir=''):
  """
  Write period spacing (dP) features (e.g. mean, min, max, dips, depths etc) to an HDF5 table.
  The input record array is produced by e.g. mesa_gyre.period_spacing.dP_features_to_recarr(). See this function for
  the record array column names, and those files that are skipped including.
  @param recarr: a named array that contanis a row per each GYRE file.
  @type recarr: numpy record array
  @param prefix: prefix name of the table to be written to disk
  @type prefix: string
  @param save_dir: the path to store the table, if not available will be created
  @type save_dir: string
  @return: the output is the same as the input record array
  @rtype: numpy record array
  """
  if save_dir:
    if save_dir[-1] != '/': save_dir += '/'
  else:
    save_dir = 'Tables/'
  if not os.path.exists(save_dir): os.makedirs(save_dir)
  if prefix is None: prefix = 'Table-dP'
  table_full_path = save_dir + prefix + '.h5'
  if os.path.isfile(table_full_path):
    print ' - Warning: write: dP_to_h5_table: %s already exists. Overwritting ...' % (table_full_path, )

  N_models = recarr.shape[0]
  column_dtype = recarr.dtype
  column_names = recarr.dtype.names
  N_columns = len(column_names)

  h5_table = h5py.File(table_full_path, 'w')
  group    = h5_table.create_group('dP')

  # Adding Attributes
  group.attrs['N_models'] = N_models
  group.attrs['N_columns'] = N_columns
  group.attrs['Columns'] = column_names

  # Create and Adding datasets, each column is written to a dataset
  # Create and Adding datasets, each column is written to a dataset
  for i_name, name in enumerate(column_names):
    dset      = group.create_dataset(name, (N_models, ), dtype=column_dtype[i_name])
    dset[...] = recarr[name]
  h5_table.close()

  print ' - %s Created.\n' % (table_full_path, )

  return recarr

#=================================================================================================================
def modes_to_h5_table(list_dic, prefix=None, save_dir=''):
  """
  """
  n_dic = len(list_dic)
  if n_dic == 0:
    message = 'Error: write: modes_to_h5_table: The input list is empty'
    raise SystemExit, message

  if save_dir:
    if save_dir[-1] != '/': save_dir += '/'
    if not os.path.exists(save_dir): os.makedirs(save_dir)
  else:
    save_dir = 'Tables/'
  if prefix is None: prefix = 'Table'
  table_full_path = save_dir + prefix + '.h5'
  if os.path.isfile(table_full_path):
    print ' - Warning: write: modes_to_h5_table: %s already exists. Overwritting ...' % (table_full_path, )

  mode_recarr = modes_to_recarray(list_dic)

  N_modes = mode_recarr.shape[0]
  N_columns = len(mode_recarr[0])
  column_dtype = mode_recarr.dtype
  column_names = mode_recarr.dtype.names

  h5_table = h5py.File(table_full_path, 'w')
  group    = h5_table.create_group('modes')

  # Adding Attributes
  group.attrs['N_modes'] = N_modes
  group.attrs['N_columns'] = N_columns
  group.attrs['Columns'] = column_names

  # Create and Adding datasets, each column is written to a dataset
  for i_name, name in enumerate(column_names):
    dset      = group.create_dataset(name, (N_modes, ), dtype=column_dtype[i_name])
    dset[...] = mode_recarr[name]
  h5_table.close()

  print ' - %s Created.\n' % (table_full_path, )

  return mode_recarr

#=================================================================================================================
def modes_to_recarray(list_dic):
  """
  """
  from commons import conversions
  dic_conv = conversions()
  Hz_to_cd = dic_conv['Hz_to_cd']

  n_dic = len(list_dic)
  if n_dic == 0:
    message = 'Error: write: modes_to_recarray: The input list is empty'
    raise SystemExit, message

  required_keys = ['m_ini', 'eta', 'ov', 'sc', 'Z', 'Xc', 'Yc', 'model_number', 'l', 'n_pg',
                   'n_p', 'n_g', 'freq', 'omega', 'E', 'E_norm', 'W', 'beta', 'M_star',
                   'L_star', 'R_star']
  sample_dic = list_dic[0]
  avail_keys = sample_dic.keys()

  for key in required_keys:
    if key not in avail_keys:
      message = 'Error: write: modes_to_recarray: the requested key: %s not available in input dictionary' % (key,)
      raise SystemExit, message

  all_modes = []
  for i_dic, dic in enumerate(list_dic):
    mesa_params = []
    m_ini   = dic['m_ini']
    eta     = dic['eta']
    ov      = dic['ov']
    sc      = dic['sc']
    Z       = dic['Z']
    evol    = dic['evol']
    Yc      = dic['Yc']
    Xc      = dic['Xc']
    model_number = dic['model_number']

    el      = dic['l']
    n_p     = dic['n_p']
    n_g     = dic['n_g']
    n_pg    = dic['n_pg']
    freq    = dic['freq']
    omega   = dic['omega']
    E       = dic['E']
    E_norm  = dic['E_norm']
    W       = dic['W']
    beta    = dic['beta']
    M_star  = dic['M_star']
    R_star  = dic['R_star']
    L_star  = dic['L_star']

    freq_cd = np.real(freq) * Hz_to_cd
    period_d= 1. / freq_cd

    mesa_params = [m_ini, eta, ov, sc, Z, Xc, Yc, model_number, M_star, R_star, L_star]

    n_modes = len(el)
    for i in range(n_modes):
      gyre_params = [int(el[i]), int(n_pg[i]), int(n_p[i]), int(n_g[i]),
                     freq_cd[i], np.real(omega[i]), period_d[i],
                     E_norm[i], W[i], np.real(beta[i])]
      all_params  = mesa_params + gyre_params
      all_modes.append(all_params)

  # convert all_modes to a numpy record array
  mesa_names = 'm_ini,eta,ov,sc,Z,Xc,Yc,model_number,M_star,R_star,L_star'
  gyre_names = 'l,n_pg,n_p,n_g,freq_cd,omega_re,period_d,E_norm,W,beta'
  all_names  = mesa_names + ',' + gyre_names
  recarr = np.core.records.fromarrays(np.array(all_modes).transpose(), names=all_names)

  print '\n - write: modes_to_recarray: Created and returned the mode record array.\n   Available keys are:'
  print '  ', all_names

  return recarr

#=================================================================================================================
def gyre_out_to_ascii(list_files, save_dir=None):
  """
  To write the GYRE output as a dictionary to an ascii file. The input dictionary contains either the short-summary
  frequency information or the eigenfunction profile information. The whole attributes will be written one per line,
  and the datasets will be written as columns
  @param list_dic_gyre
  """
  import gyre

  if save_dir is None:
    save_dir = './'
  else:
    if not os.path.exists(save_dir): os.makedirs(save_dir)
    logging.info('gyre_out_to_ascii: Create Directory: {0}'.format(save_dir))
    if save_dir[-1] != '/': save_dir += '/'

  sample_file = list_files[0]
  dic, rec = gyre.read_output(sample_file)
  attr_names = dic.keys()
  col_names  = rec.dtype.names
  col_header = ''
  a_row = rec[0]
  n_col = 0
  for i_col, name in enumerate(col_names):
    if type(a_row[i_col]) is np.complex128:
      col_header += 'Real_{0:25s}'.format(name)
      n_col      += 1
      col_header += 'Imag_{0:25s}'.format(name)
      n_col      += 1
    else:
      col_header += '{0:30s}'.format(name)
      n_col      += 1
  col_header += '\n'

  for i_file, a_file in enumerate(list_files):
    lines = ['filename = {0}\n'.format(a_file)]
    dic, rec = gyre.read_output(a_file)
    for key, val in dic.items():
      if type(val) == np.complex128:
        lines.append('Real_{0} = {1} \n'.format(key, np.real(val)))
        lines.append('Imag_{0} = {1} \n'.format(key, np.imag(val)))
      else:
        lines.append('{0} = {1} \n'.format(key, val))
    lines.append('\n')
    lines.append(col_header)

    n_rows = len(rec)
    for i_row, row in enumerate(rec):
      line = ''
      for i_val, val in enumerate(row):
        if type(val) == np.complex128:
          line += '{0:<30.16e}'.format(float(np.real(val)))
          line += '{0:<30.16e}'.format(float(np.imag(val)))
        else:
          line += '{0:<30.16e}'.format(val)

      line += '\n'
      lines.append(line)

    # create output filename
    ind_slash = a_file.rfind('/')
    ind_point = a_file.rfind('.')
    file_core = a_file[ind_slash+1 : ind_point]
    output_filename = save_dir + file_core + '.txt'

    with open(output_filename, 'w') as w: w.writelines(lines)
    print ' - write: gyre_out_to_ascii: saved {0}'.format(output_filename)

  return None

#=================================================================================================================
def dic_gyre_in_to_ascii(dic, ascii_out=None):
  """
  Write the columns in the input dictionary data as an ascii file in GYRE input format. This is useful, 
  e.g., when a column has changed, and one likes to write the whole new data (including the new column)
  as an ASCII file for later use. One application is re-writing GYRE input files after fixing bad brunt_N2
  values in the convective regions.
  @param dic: dictinary with the GYRE info. This is produced e.g. by calling mesa_gyre.read.read_gyre_in()
  @type dic: dict 
  @param ascii_out: full path to the output filename.
  @type ascii_out: string 
  """
  if ascii_out is None: return 

  def fortran_format(val, exponent='E+'):
    """ Returns a string. Be very careful with the exponent usage/definition"""
    if len(exponent) != 2: raise SystemExit, 'dic_gyre_in_to_ascii: fortran_format: Wrong exponent'
    exponent = exponent.lower()
    s     = str(val)
    s     = s.lower()
    i_e   = s.find(exponent) 
    i_exp = i_e + 2
    i_dec = s.find('.') + 1
    intg  = s[0]
    decim = s[i_dec : i_e]
    power = '{0:2s}'.format(str(int(s[i_exp : ]) + 1))
    fort  = '0.{0:1s}{1}{2}{3}'.format(intg, decim, exponent, power)

    return fort

  nn      = dic['nn']
  M_star  = fortran_format(dic['M_star'], 'E+')
  R_star  = fortran_format(dic['R_star'], 'E+')
  L_star  = fortran_format(dic['L_star'], 'E+')
  ncol    = dic['ncol']
  gyre_in = dic['gyre_in']

  col_names = gyre_in.dtype.names

  header  = '{0:6d}'.format(nn)
  header  += '  {0}'.format(M_star)
  header  += '  {0}'.format(R_star)
  header  += '  {0}'.format(L_star)
  header  += '    {0}'.format(ncol)
  header  += ' \n'

  data    = [header]
  for i in range(nn):
    row   = gyre_in[i]
    line  = '{0:6d}'.format(i+1)
    for key in col_names[1:]:
      line += '{0:5s}{1:.16e}'.format('', row[key]) 
    line  = line.replace('e', 'D')
    line  += ' \n'
    data.append(line)

  with open(ascii_out, 'w') as w: w.writelines(data)

  return None

#=================================================================================================================

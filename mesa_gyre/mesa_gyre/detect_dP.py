
import sys, os, glob
import numpy as np
import pylab as plt
import commons, stars

dP_mean  = 9418.
dP_tol   = 100.
dic_star = stars.HD50230()

dic_conv = commons.conversions()
sec_to_d = dic_conv['sec_to_d']
cd_to_Hz = dic_conv['cd_to_Hz']
Hz_to_cd = dic_conv['Hz_to_cd']

#######################################################################
def plot_freq_spectrum(recarr, highlight=[], file_out=None):
    """
    This function creates the frequency power spectrum similar to Figure 1 in 
    Degroote et al. (2012).
    @param recarr: numpy record array with the whole observed mode info per each row.
             Note that the 'per' field should be already available there
    @type recarr: numpy record array
    @param highlight: the id of selected modes that will be highlighted with a red bullet on the top panel
    @type highlight: list of strings
    """
    from matplotlib.patches import ConnectionPatch as MPC 
    if file_out is None: return 
    freq = recarr['f']

    fig, (top, mid, left, right, cent) = plt.subplots(1, 5, figsize=(8,8))
    top.set_position([0.05, 0.72, 0.94, 0.25])
    mid.set_position([0.09, 0.37, 0.89, 0.30])
    left.set_position([0.05, 0.03, 0.40, 0.26])
    right.set_position([0.58, 0.03, 0.40, 0.26])
    cent.set_position([0.50, 0.46, 0.30, 0.18])

    # Add the background patch
    mid.fill_between([0.0, 1.5], y1=[0, 0], y2=[1800, 1800], color='grey', alpha=0.25, zorder=1)
    mid.fill_between([2.0, 5.5], y1=[0, 0], y2=[200, 200], color='grey', alpha=0.25, zorder=1)
    mid.fill_between([6.0, 7.5], y1=[0, 0], y2=[20, 20], color='grey', alpha=0.25, zorder=1)
    mid.fill_between([8.0, 15.], y1=[0, 0], y2=[40, 40], color='grey', alpha=0.25, zorder=1)
    right.axhline(y=5.1, linestyle='dashed', color='grey', lw=2)

    # Include connecting lines (patches) between patches and different axes (panels)
    p1 = MPC(xyA=(6, 20), xyB=(6, 0), coordsA='data', coordsB='data', 
         axesA=mid, axesB=cent, arrowstyle=u'-', color='grey'); mid.add_artist(p1)
    p2 = MPC(xyA=(7.5, 20), xyB=(7.5, 0), coordsA='data', coordsB='data', 
         axesA=mid, axesB=cent, arrowstyle=u'-', color='grey'); mid.add_artist(p2)
    p3 = MPC(xyA=(0, 1800), xyB=(0.0, 0.0), coordsA='data', coordsB='data', 
         axesA=mid, axesB=top, arrowstyle=u'-', color='grey'); mid.add_artist(p3)
    p4 = MPC(xyA=(1.5, 1800), xyB=(1.5, 0.0), coordsA='data', coordsB='data', 
         axesA=mid, axesB=top, arrowstyle=u'-', color='grey'); mid.add_artist(p4)
    p5 = MPC(xyA=(2, 0), xyB=(2, 200), coordsA='data', coordsB='data', 
         axesA=mid, axesB=left, arrowstyle=u'-', color='grey'); mid.add_artist(p5)
    p6 = MPC(xyA=(5.5, 0), xyB=(5.5, 200), coordsA='data', coordsB='data', 
         axesA=mid, axesB=left, arrowstyle=u'-', color='grey'); mid.add_artist(p6)
    p7 = MPC(xyA=(8, 0), xyB=(8, 40), coordsA='data', coordsB='data', 
         axesA=mid, axesB=right, arrowstyle=u'-', color='grey'); mid.add_artist(p7)
    p8 = MPC(xyA=(15, 0), xyB=(15, 40), coordsA='data', coordsB='data', 
         axesA=mid, axesB=right, arrowstyle=u'-', color='grey'); mid.add_artist(p8)
    

    for i_row, row in enumerate(recarr):
        freq = row['f']
        amp  = row['A']

        top.vlines(freq, ymin=0, ymax=amp, linestyle='solid', color='black', lw=1, zorder=3)
        mid.vlines(freq, ymin=0, ymax=amp, linestyle='solid', color='black', lw=1, zorder=3)
        left.vlines(freq, ymin=0, ymax=amp, linestyle='solid', color='black', lw=1, zorder=3)
        right.vlines(freq, ymin=0, ymax=amp, linestyle='solid', color='black', lw=1, zorder=3)
        cent.vlines(freq, ymin=0, ymax=amp, linestyle='solid', color='black', lw=1, zorder=3)

    if highlight:
        ind = np.array( [i for i in range(len(recarr)) if recarr[i]['id'] in highlight] )
        top.scatter(recarr[ind]['f'], recarr[ind]['A']-50, color='red', marker='o', s=50)

    top.set_xlim(0.0, 1.5)
    top.set_ylim(0, 1800)

    mid.set_xlabel(r'Frequency [d$^{-1}$]')
    mid.set_ylabel(r'Amplitude [ppm]')
    mid.set_xlim(-0.2, 16)
    mid.set_ylim(-100, 2000)

    left.set_xlim(2.0, 5.5)
    left.set_ylim(0, 200)

    right.set_xlim(8, 15)
    right.set_ylim(0, 40)

    cent.set_xlim(6, 7.5)
    cent.set_ylim(0, 20)

    # Annotations
    mid.annotate('(a)', xy=(0.94, 0.85), xycoords='axes fraction', fontsize='large')
    top.annotate('(b)', xy=(0.94, 0.85), xycoords='axes fraction', fontsize='large')
    cent.annotate('(c)', xy=(0.88, 0.80), xycoords='axes fraction', fontsize='large')
    left.annotate('(d)', xy=(0.90, 0.90), xycoords='axes fraction', fontsize='large')
    right.annotate('(e)', xy=(0.90, 0.90), xycoords='axes fraction', fontsize='large')

    plt.savefig(file_out)
    print ' - detect_dP: plot_freq_spectrum: saved {0}'.format(file_out)
    plt.close()

    return None

#######################################################################
def plot_selected_dP(recarr, list_id, obs_recarr=None, file_out=None):
    """
    This funciton plots the period spacing for a subsection of modes among recarr for which
    their mode id is passed by list_id
    @param recarr: numpy record array with the whole observed mode info per each row.
             Note that the 'per' field should be already available there
    @type recarr: numpy record array
    @param list_id: list giving the ID of those modes that form a period spacing pattern.
             E.g list_id = ['f001', 'f002', ...]
    @type list_id: list of strings
    @param obs_recarr: The record array with all the observed peaks, taken from Degroote+12
    @type obs_recarr: numpy record array
    @return: None
    @rtype: None
    """
    ind = np.array([i for i in range(len(recarr)) if recarr['id'][i] not in list_id])
    new_rec = np.delete(recarr, ind, axis=0)
    recarr = new_rec

    per = recarr['per']
    dP = per[1:] - per[:-1]      # sec
    per_for_plot = per[:-1] * sec_to_d
    sz = recarr[:-1]['A']/10

    fig, ax = plt.subplots(1, figsize=(6,4))
    ax.plot(per_for_plot, dP, linestyle='solid', color='black', lw=1.5, zorder=1)
    for i in range(len(dP)):
        ax.scatter([per_for_plot[i]], [dP[i]], marker='o', color='black', s=sz[i], zorder=2)
    ax.set_xlabel(r'Period [day]')
    ax.set_ylabel(r'Period Spacing [sec]')

    if obs_recarr is not None:
        back = ax.twinx()
        obs_per = obs_recarr['per'] * sec_to_d
        obs_amp = obs_recarr['A']
        for i_obs, obs_P in enumerate(obs_per):
            back.plot([ obs_P, obs_P ], [ 0, obs_amp[i_obs] ], lw=0.5, color='grey', linestyle='solid', zorder=0)
        back.set_ylim(0, 2000)
        back.set_ylabel(r'Amplitude [ppm]')

    if file_out:
        plt.tight_layout()
        plt.savefig(file_out)
        print ' - plot_selected_dP: store {0}'.format(file_out)
        plt.close()

    return None

#######################################################################
def plot_clickable_echelle(recarr, dP):
    """
    This funciton plots an Echelle diagram which is clickble, and prints out the properites of
    each point that is clicked on.
    @param recarr: numpy record array with the whole observed mode info per each row.
             Note that the 'per' field should be already available there
    @type recarr: numpy record array
    @param dP: trial period spacing value to fold the Echelle diagram onto
    @type dP: float
    @return: None
    @rtype: None
    """
    global data
    modulus = get_period_echelle(recarr, dP)

    fig, ax = plt.subplots(1, figsize=(6,4))
    ax.set_title('Clickable Echelle Diagram')

    sz   = recarr['A'] / 100
    per = recarr['per'] * sec_to_d
    n_modes = len(per)
    min_P, max_P = np.min(per), np.max(per)
    # for i, per in enumerate(per):
    #     ax.scatter([modulus[i]], [per], marker='o', s=sz[i], zorder=2)
    ax.scatter(modulus, per, marker='o', s=sz, zorder=1)
    line, = ax.plot(modulus, per, 'o', ms=0.01, picker=1)     # 5 points tolerance
    # ax.set_ylim(min_P*0.80, max_P*1.20)
    ax.set_ylim(0, 3)

    # def onpick(event, recarr=recarr):
    def onpick(event):
        global data
        thisline = event.artist
        xdata = thisline.get_xdata()
        ydata = thisline.get_ydata()
        ind = event.ind
        print 'Mode: ', data[ind]['id']
        # print 'onpick points:', zip(xdata[ind], ydata[ind]), ind

    data = recarr

    fig.canvas.mpl_connect('pick_event', onpick)

    plt.show()

#######################################################################
def scan_period_echelle(recarr, dP_from, dP_to, n_dP, exclude=[]):
    """
    Loop over n_dP trial dP values (uniformly lying between dP_from and dP_to), and pass a
    function call to plot_period_echelle() in order to store one echelle diagram.
    @param recarr: numpy record array with the whole observed mode info per each row.
             Note that the 'per' field should be already available there
    @type recarr: numpy record array
    @param dP_from, dP_to: starting and ending trial period spacing values
    @type dP_from, dP_to: float
    @param n_dP: number of trial peirod spacing between dP_from and dP_to
    @type n_dP: integer
    @param exclude: list of mode ids to exclude from the Echelle diagram. default=[], i.e.
            include all. E.g. [ 'f001', 'f002', ... ]
    @return: None
    @rtype: None
    """
    if 'per' not in recarr.dtype.names:
        raise SystemExit, 'Error: scan_period_echelle: period information missing! consider get_list_rec_dP()'

    dP_trial = np.linspace(dP_from, dP_to, num=n_dP, endpoint=True)
    for i_dP, dP in enumerate(dP_trial):
        plot_name = 'CoRoT/Echelle/' + 'dP-{0}.png'.format(int(dP))

        plot_period_echelle(recarr, dP, x_from=0, x_to=dP_to, exclude=exclude, file_out=plot_name)

    return None

#######################################################################
def plot_period_echelle(recarr, dP, x_from=0, x_to=2000, P_from=None, P_to=None,
    exclude=[], highlight=[], file_out=None):
    """
    This function calls get_period_echelle to retrieve the modulus of the periods folded on the
    trial period spacing dP, and plots them.
    @param recarr: numpy record array with the whole observed mode info per each row.
             Note that the 'per' field should be already available there
    @type recarr: numpy record array
    @param exclude: list of mode ids to exclude from the Echelle diagram. default=[], i.e.
            include all. E.g. [ 'f001', 'f002', ... ]
    @type exclude: list of strings
    @param highlight: list of modes to plot with highlighted plotting color/symbol, etc., for 
            better visibility. If non-empty, then this list passes e.g. the identified dipole
            modes to be plotted using filled plotting symbols, while everything else is plotted 
            using empty symbols. The format for this list is identical to exclude. default=[].
    @type highlight: list of strings
    @return: None
    @rtype: None
    """
    n_row = len(recarr)

    if highlight:
        ind = np.array([ i for i in range(n_row) if recarr['id'][i] in highlight ])
        rec_highlight = recarr[ind]

    if exclude:
        ind = np.array([i for i in range(n_row) if recarr['id'][i] in exclude])
        new_rec = np.delete(recarr, ind, axis=0)
        recarr = new_rec

    fig, ax = plt.subplots(1, figsize=(6, 4), dpi=120)
    plt.subplots_adjust(left=0.10, right=0.98, bottom=0.12, top=0.97)
    # ax.set_position([0.10, 0.12, 0.75, 0.85])

    if highlight:
        per     = rec_highlight['per'] * sec_to_d
        sz      = rec_highlight['A'] / 10
        modulus = get_period_echelle(rec_highlight, dP)
        ax.scatter(modulus, per, marker='o', s=sz, facecolors='black', edgecolors='black', zorder=2)
    
    freq = recarr['f'] * Hz_to_cd
    per  = recarr['per'] * sec_to_d
    sz    = recarr['A'] / 10
    modulus = get_period_echelle(recarr, dP)

    ax.scatter(modulus, per, marker='o', s=sz, facecolors='white', edgecolors='grey', zorder=1)

    if x_from is None: x_from = 0
    if x_to is None:     x_to = np.max(recarr['per'])
    ax.set_xlim(x_from, x_to)
    ax.set_xlabel(r'Period modulo {0} [sec]'.format(int(dP)), fontsize='large')
    # ax.set_ylim(np.min(per)*0.9, np.max(per)*1.1)
    if P_from is None: P_from = np.min(per)
    if P_to   is None: P_to   = np.max(per)
    ax.set_ylim(P_from, P_to)
    ax.set_ylabel(r'Period [day]', fontsize='large')

    # Fake data points for plotting symbol amplitude scalings
    ax.scatter([], [], s=1, marker='o', facecolors='white', edgecolors='grey', label='10 ppm')
    ax.scatter([], [], s=10, marker='o', facecolors='white', edgecolors='grey', label='100 ppm')
    ax.scatter([], [], s=100, marker='o', facecolors='white', edgecolors='grey', label='1000 ppm')
    leg = ax.legend(loc=3, fontsize='medium', scatterpoints=1)

    # extra panel is on right, and only to show scaling of mode amplitudes

    if file_out:
        plt.savefig(file_out)
        print ' - plot_period_echelle: store {0}'.format(file_out)
        plt.close()

    return None

#######################################################################
def retrieve_modes(recarr, list_modes):
    """
    Print the properties of the mode from the record array for a subset of modes passed by list_modes.
    @param recarr: numpy record array with the whole observed mode info per each row.
             Note that the 'per' field should be already available there
    @type recarr: numpy record array
    @param list_modes: list of mode ids; e.g. ['f001', 'f002', ...]
    @type list_modes: list of strings
    @return: numpy record array for the selected list of modes 
    @rtype: numpy records array
    """
    n_modes = len(list_modes)
    if n_modes == 0:
        raise SystemExit, 'Error: retrieve_modes: Input mode list is empty.'

    n_row = len(recarr)
    ind = np.array( [i for i in range(n_row) if recarr['id'][i] in list_modes] )
    recarr = recarr[ind]

    print
    names  = ['id', 'f', 'f_e', 'per', 'per_e', 'A', 'A_e']
    factor = [1, Hz_to_cd, Hz_to_cd, sec_to_d, sec_to_d, 1, 1]
    header = ''.join(['id   '] + [ '{0:12s}  '.format(x) for x in names[1:] ])
    print header
    for i, row in enumerate(recarr):
        txt = '{0} '.format(row['id'])
        for j, name in enumerate(names[1:]):
            txt += '{0:.6e}  '.format(row[name] * factor[j+1])
        print txt
    print

    return recarr

#######################################################################
def get_period_echelle(recarr, dP):
    """
    This function receives a record array containing mode information, and uses the frequencies
    and periods to establish the echelle diagram for a trial period spacing pattern dP.
    @param recarr: numpy record array with the whole observed mode info per each row.
             Note that the 'per' field should be already available there
    @type recarr: numpy record array
    @param dP: trial period spacing to fold the periods with
    @type dP: float
    @return: numpy array with the same size as recarr.size(), and with the modulus being calculated
             like recarr['per'] % dP
    @rtype: ndarray
    """
    if 'per' not in recarr.dtype.names:
        raise SystemExit, 'Error: get_period_echelle: period information not found in the input array! consider get_list_rec_dP(). '

    return recarr['per'] % dP

#######################################################################
def get_list_rec_dP(recarr, n_accept_dP=3):
    """
    Start from the first frequency in the list, and walk through the whole frequencies, and get
    all possible spacings in the dataset
    """
    n_per = len(recarr)
    list_rec_dP = []
    for i_per in range(n_per):
        rec_dP = detect_dP(recarr[i_per:], dP_mean=dP_mean, dP_tol=dP_tol)
        # Evaluate dP +/- dP_e for those modes that form a period spacing pattern
        rec_dP = get_dP(rec_dP)
        n_dP = len(rec_dP)
        if n_dP >= n_accept_dP:
            print 'Step: ', i_per, '. Found: n_dP = ', n_dP
            list_rec_dP.append(rec_dP)
        else:
            continue

        # Plot the reconciled period spacing
        plot_name = 'CoRoT/HD50230-new-dP-el-1-N{0:03d}.png'.format(i_per)
        plot_dP(recarr, rec_dP, dic_obs=dic_star, plot_name=plot_name)
        ascii_name = plot_name.replace('.png', '.txt')
        ascii_name = ascii_name.replace('CoRoT/', 'CoRoT/mode-list/')
        write_recarr_ascii(rec_dP, ascii_name)

    return list_rec_dP

#######################################################################
def write_recarr_ascii(recarr, file_out):
    """
    Once a spacing is found (for whatever look it has), the information of that specific record
    array can be written to the disk as an ascii file.
    This function takes care of that writing to ascii.
    @param recarr: record array with several rows. Each row stands for the mode that forms a period spacing.
          The available fields can be checked up in read_freq_list() and extend_record_array_with_periods()
    @type recarr: numpy record array.
    @param file_out: full path to the output ascii file that will contain the list of modes
    @type file_out: string
    @return: None
    @rtype: None
    """
    names = ['id', 'A', 'A_e', 'f', 'f_e', 'per', 'per_e', 'dP', 'dP_e'] #recarr.dtype.names
    output = []
    header = ''.join( ['{0:20s}'.format(name) for name in names] ) + ' \n'
    output.append(header)
    for i_row, row in enumerate(recarr):
        txt = ''
        # for name in names: txt += '{0:20s}'.format(str(row[name]))
        txt += '{0:20s}'.format(row['id'])
        txt += '{0:20.6e}'.format(row['A'])
        txt += '{0:20.6e}'.format(row['A_e'])
        txt += '{0:20.6e}'.format(row['f'] * Hz_to_cd)
        txt += '{0:20.6e}'.format(row['f_e'] * Hz_to_cd)
        txt += '{0:20.6e}'.format(row['per'] * sec_to_d)
        txt += '{0:20.6e}'.format(row['per_e'] * sec_to_d)
        txt += '{0:20.6e}'.format(row['dP'])
        txt += '{0:20.6e}'.format(row['dP_e'])
        txt += ' \n'
        output.append(txt)

    with open(file_out, 'w') as w: w.writelines(output)
    print ' - write_recarr_ascii: saved {0}'.format(file_out)

    return None

#######################################################################
def plot_hist(recarr, plot_name):
    """
    Plot the histogram of the amplitudes, to check their distribution, and find a cut off
    """
    amp = recarr['A']
    n_amp = len(amp)
    n_bins = n_amp / 10
    min_amp = np.min(amp)
    max_amp = np.max(amp)
    bins = np.linspace(min_amp, max_amp, num=n_bins, endpoint=True)
    print 'Min Amp={0} ppm, and Max Amp={1} ppm'.format(min_amp, max_amp)

    fig = plt.figure(figsize=(6,4), dpi=200)
    ax  = fig.add_subplot(111)
    ax.hist(amp, bins=bins, range=(min_amp, max_amp), histtype='stepfilled', color='grey')
    ax.set_xlabel(r'Amplitude [ppm]')
    ax.set_ylabel(r'Number of Occurance')

    plt.tight_layout()
    plt.savefig(plot_name)
    plt.close()

    return None

#######################################################################
def plot_dP(obs_recarr, recarr, dic_obs=None, plot_name=None):
    """
    Plot the detected period spacing
    """

    obs_per = obs_recarr['per'] * sec_to_d
    min_per = np.min(obs_per) * 0.9
    max_per = np.max(obs_per) * 1.1

    n_per =len(recarr)
    fig = plt.figure(figsize=(8,4), dpi=200)
    ax  = fig.add_subplot(111)
    plt.subplots_adjust(wspace=0.05, hspace=0.10)

    # Top panel
    # sz = 5 * np.sqrt(recarr[:-1]['A'] )
    sz = 5 * np.log10( recarr[:-1]['A'] )**2.0
    # ax.errorbar(recarr[:-1]['per']*sec_to_d, recarr[:-1]['dP'], xerr=recarr[:-1]['per_e']*sec_to_d,
    #                    yerr=recarr[:-1]['dP_e'], ms=5, fmt='--o', ecolor='red', color='red', zorder=2)
    ax.plot(recarr[:-1]['per']*sec_to_d, recarr[:-1]['dP'], linestyle='dashed', ms=10, color='red', zorder=2)
    for i in range(n_per-1):
        per = recarr[i]['per']
        ax.scatter([ per*sec_to_d ], [recarr[i]['dP']], s=sz[i], color='red', zorder=3)

    ax.set_xlabel(r'Period [d]')
    ax.set_xlim(min_per, max_per)
    ax.set_ylabel(r'Period Spacing $dP$ [sec]')
    ax.set_ylim(dP_mean - dP_tol-20, dP_mean + dP_tol + 20)

    if dic_obs is not None:
        dic_obs = stars.list_freq_to_list_dP(dic_obs, freq_unit='Hz')
        list_obs_P = [dic['P'] * sec_to_d for dic in dic_obs['list_dic_freq']]
        list_obs_P_err = [dic['P_err'] * sec_to_d for dic in dic_obs['list_dic_freq']]
        list_obs_dP = [dic['dP']  for dic in dic_obs['list_dic_freq']]
        list_obs_dP_err = [dic['dP_err'] for dic in dic_obs['list_dic_freq']]
        ax.errorbar(list_obs_P, list_obs_dP, xerr=list_obs_P_err, yerr=list_obs_dP_err, fmt='-o', ms=5,
            ecolor='grey', color='grey', zorder=1)

    # other panel
    nxt = ax.twinx()
    for i_row, row in enumerate(obs_recarr):
        per = obs_per[i_row]
        nxt.plot([per, per], [0, row['A']], linestyle='solid', color='grey', lw=0.5)

    for i in range(n_per-1):
        per = recarr[i]['per']
        ind = np.where((per*sec_to_d == obs_per))[0]
        amp = obs_recarr[ind]['A'] * 1.05
        nxt.plot([per*sec_to_d, per*sec_to_d], [amp, 10000], linestyle='dotted', color='red', lw=0.5)

    # nxt.set_xlabel(r'Period [d]')
    # nxt.set_xlim(min_per, max_per)
    nxt.set_ylabel(r'Flux Amplitude [ppm]')
    nxt.set_ylim(-10, 2000)

    plt.tight_layout()
    plt.savefig(plot_name)
    plt.close()

    return None

#######################################################################
def reverse_recarr(recarr):
    """
    Sort the input recarr in ascending period order, so that P_{n+1} - P_{n} is positive valued
    """
    dtype = recarr.dtype
    reverse = np.empty(recarr.shape, dtype=dtype)
    n_freq = len(recarr)
    for i, row in enumerate(recarr):
        ind_reverse = n_freq - 1 - i
        reverse[ind_reverse] = row

    return reverse

#######################################################################
def get_dP(recarr):
    """
    Calculate dP for the list of modes that fall within the detected spacing
    """
    rec_dP = extend_record_array(recarr, [('dP', 'f8'), ('dP_e', 'f8')])
    n_per = len(recarr)
    for i_per in range(n_per - 1):
        per_hi = rec_dP[i_per+1]['per']
        per_hi_err = rec_dP[i_per+1]['per_e']
        per_lo = rec_dP[i_per]['per']
        per_lo_err = rec_dP[i_per]['per_e']
        dP, dP_e = stars.MC_dP(per_hi, per_hi_err, per_lo, per_lo_err)
        rec_dP[i_per]['dP'] = dP
        rec_dP[i_per]['dP_e'] = dP_e
        # print i_per, recarr[i_per]['f'], dP, dP_e
    # rec_dP[-1]['dP'] = 0.0
    # rec_dP[-1]['dP_e'] = 0.0

    return rec_dP

#######################################################################
def detect_dP(recarr, dP_mean, dP_tol):
    """
    Detect Period spacing pattern having a mean of dP_mean, and a tolorance of +/- dP_tol.
    @param recarr: numpy record array containing two fields for period and its associated error
           per mode:
           - per: mode period, evaulated using mesa_gyre.stars.MC_freq_to_per
           - per_err: mode period uncertainty, evaulated using mesa_gyre.stars.MC_freq_to_per
    @type recarr: numpy record array
    @param dP_mean: Mean period spacing in seconds.
    @type dP_mean: float
    @param dP_tol: The upper and lower range of period spacing around the mean.
    @type dP_tol: float
    @return: numpy record array of only those modes that conform to a structured perod spacing
           pattern
    @rtype: numpy record array
    """
    names = recarr.dtype.names
    if 'per' not in names or 'per_e' not in names:
        raise SystemExit, 'Error: detect_dP: per or per_e not among the available columns in the input record array'

    per_arr = recarr['per']
    n_per  = len(recarr)

    dP_min = dP_mean - dP_tol
    dP_max = dP_mean + dP_tol

    i = 0
    i_next = None
    last_i_next = [i_next]
    ind_arr = []
    while i < n_per-2:

        per = recarr['per'][i]
        per_e = recarr['per_e'][i]
        per_remain = per_arr[i+1 : ]

        in_series = False
        for j, next_per in enumerate(per_remain):
            next_per_e = recarr[i+j]['per_e']
            dP, dP_e = stars.MC_dP(next_per, next_per_e, per, per_e)
            in_series = (dP >= dP_min) & (dP <= dP_max)
            if (in_series):
                i_next = i + j + 1
                last_i_next.append(i_next)
                # print i, i_next, recarr[i]['f'], dP
                ind_arr.append(i)
                break
            else:
                continue
        if in_series:
            i = i_next
        else:
            break

    return recarr[ind_arr]

#######################################################################
def extend_record_array_with_periods(recarr):
    """
    Add period and its error to the available recrod array
    """
    rec_per = extend_record_array(recarr, extra_dtype=np.dtype([('per', 'f8'), ('per_e', 'f8')]))
    n_freq = len(rec_per)
    list_freq = np.array([rec_per[i]['f'] for i in range(n_freq)])
    list_freq_err = np.array([rec_per[i]['f_e'] for i in range(n_freq)])

    for i in range(n_freq):
        per, per_e = stars.MC_freq_to_per(list_freq[i], list_freq_err[i])
        rec_per['per'][i] = per
        rec_per['per_e'] = per_e

    return rec_per

#######################################################################
def extend_record_array(old, extra_dtype):
    """
    To extend an existing record array with new columns, passed through extra_dtype. The newly added
    filed(s) will be initialized to zero.
    @param old: numpy record array to be extended. The returned array will already have a copy of the whole data from old.
    @type old: numpy record array
    @param extra_dtype: numpy dtype object. If the input is not of the type: np.dtype(), then, it is "tried" to convert it to np.dtype
           by applying the np.dtype(extra_dtype). An accepted format is like this:
           extra_dtype = [('example', 'f8')]
    @return: numpy record array, extended to contain new fields passed by extra_dtype, and initialized to empty. They have to be
           filled up afterwards. The whole old record array is copied and returned in the new record array
    @rtype: numpy record array
    """

    if old.dtype.fields is None:
      raise ValueError, '"old" must be a structured numpy array'
    if type(extra_dtype) is not type(np.dtype([('example', 'f8')])):
        try:
            extra_dtype = np.dtype(extra_dtype)
        except:
            raise SystemExit, 'Error: extend_record_array: extra_dtype is not of the type np.dtype() !'

    old_names = old.dtype.names
    new_names = extra_dtype.names

    new = np.empty(old.shape, dtype=old.dtype.descr + extra_dtype.descr)
    for name in old_names:
       new[name] = old[name]
    for name in new_names:
        new[name] = 0.0

    return new

#######################################################################
def convert_frequencies_to_cgs(recarr):
    """
    Multiply the frequencies and their corresponding uncertainties by the correct conversion factor
    """
    recarr        = np.copy(recarr)
    recarr['f']   *= cd_to_Hz
    recarr['f_e'] *= cd_to_Hz

    return recarr

#######################################################################
def sort_frequencies(recarr):
    """
    Sort the frequencies in ascending order
    """
    n_freq = len(recarr)
    freqs = recarr['f']
    ind_sort = np.argsort(freqs)

    return recarr[ind_sort]

#######################################################################
def clib_list_by_lower_amp(recarr, lower_amp=10):
    """
    Clip the frequency list, and remove those whose amplitudes are below lower_amp [ppm]
    This procedure can be called anytime.
    """
    amp = recarr['A']
    ind = np.where((amp >= lower_amp))[0]

    return recarr[ind]

#######################################################################
def clip_list_by_lower_freq(recarr, lower_freq=0.5):
    """
    Clip the frequency list, and remove those whose frequency drop below lower_freq [d^-1]
    This procedure should be called once the frequency list is already sorted, otherwise, a lot
    of modes will be lost
    """
    list_freq = recarr['f']
    ind  = np.where((list_freq >= lower_freq))[0]

    return recarr[ind]

#######################################################################
def clip_list_by_upper_freq(recarr, upper_freq=3):
    """
    Clip the frequency list, and remove those whose frequency exceed upper_freq [d^-1]
    This procedure should be called once the frequency list is already sorted, otherwise, a lot
    of modes will be lost
    """
    list_freq = recarr['f']
    ind = np.where((list_freq <= upper_freq))[0]

    return recarr[ind]

#######################################################################
def clip_list_by_snr(recarr, snr=4.0):
    """
    Return all modes with their SNR above "snr".
    """
    list_snr = recarr['snr']
    ind = np.where((list_snr >= snr))[0]

    return recarr[ind]

#######################################################################
def remove_instrumental_freq(recarr):
    """
    Remove those frequencies that the term "instrumental" occurs in their notes.
    @param recarr: frequency list record array
    @type recarr: numpy record array
    """
    notes = recarr['note']
    n_freq = len(notes)

    ind_instrumental = [i for i in range(n_freq) if 'Instrumental' in notes[i] or 'instrumental' in notes[i]]
    ind_orbital = [i for i in range(n_freq) if 'orbit' in notes[i]]
    ind_delete = ind_instrumental + ind_orbital
    recarr = np.delete(recarr, obj=ind_delete)

    return recarr

#######################################################################
def fix_amplitude_to_ppm(recarr):
    """
    The amplitude list in papics list is in parts, and needs to be increased 
    by a factor one milion to be in ppm (similar to Degroote et al. 2012)
    """
    recarr['A'] = recarr['A'] * 1e6
    recarr['A_e'] = recarr['A_e'] * 1e6

    return recarr

#######################################################################
def fix_freq_err_scale(recarr, scale):
    """
    The frequency errors in Degroote et al. (2012) Table A.2 has a factor of 10^-3, not in the data
    """
    recarr = np.copy(recarr)
    recarr['f_e'] = recarr['f_e'] * scale

    return recarr

#######################################################################
def read_freq_list(filename, fixed_columns=False, skip=0):
    """
    Read the input frequency list file, and return a numpy record array associated with the data.
    The returned array has the following columns:
    - id: e.g. f001
    @param skip: specify the number of lines to skip in the header of the file 
    @param fixed_columns: if the table has exactly 12 columns, set it to True; else for flexible width
           tables (i.e. varying number of columns per each row), set this to False.
    """
    if not os.path.exists(filename):
        print 'Error: read_freq_list: {0} does not exist'.format(filename)
        raise SystemExit

    with open(filename, 'r') as r: lines = r.readlines()
    if skip >0:
        for i_skip in range(skip):
            throw_away = lines.pop(0)
    header = lines.pop(0).rstrip('\r\n').split()
    fmt      = lines.pop(0).rstrip('\r\n').split()
    n_col   = len(header)
    n_freq = len(lines)
    dtype = [(header[i], fmt[i]) for i in range(n_col)]

    data = []
    for i, line in enumerate(lines):
        line = line.rstrip('\r\n').split()
        n_line_col = len(line)

        if fixed_columns:
            this_line = [line[0]] + [float(line[i]) for i in range(1, 12)] 
        else:
            has_note = False
            has_note = n_line_col > 9

            note = ''
            if has_note:
                for i_extra in range(9, n_line_col):
                    note += line[i_extra] + ' '

            this_line = [line[0]] + [float(line[i]) for i in range(1, 9)] + [note]
        data.append(this_line)

    recarr = np.core.records.fromarrays(np.array(data).transpose(), dtype=dtype)

    return recarr

#######################################################################
def read_freq_list_fixed_columns(filename, skip=0):
    """
    Read the input frequency list file, and return a numpy record array associated with the data.
    The returned array has the following columns:
    - id: e.g. f001
    """
    if not os.path.exists(filename):
        print 'Error: read_freq_list: {0} does not exist'.format(filename)
        raise SystemExit

    with open(filename, 'r') as r: lines = r.readlines()
    if skip >0:
        for i_skip in range(skip):
            throw_away = lines.pop(0)
    header = lines.pop(0).rstrip('\r\n').split()
    fmt      = lines.pop(0).rstrip('\r\n').split()
    n_col   = len(header)
    n_freq = len(lines)
    dtype = [(header[i], fmt[i]) for i in range(n_col)]

    data = []
    for i, line in enumerate(lines):
        line = line.rstrip('\r\n').split()
        n_line_col = len(line)
        # has_note = False
        # has_note = n_line_col > 9

        # note = ''
        # if has_note:
        #     for i_extra in range(9, n_line_col):
        #         note += line[i_extra] + ' '

        this_line = [line[0]] + [float(line[i]) for i in range(1, 12)] 
        data.append(this_line)

    recarr = np.core.records.fromarrays(np.array(data).transpose(), dtype=dtype)

    return recarr

#######################################################################


import sys, os, glob
import logging
import smtplib

#=================================================================================================================
def warning(warn_msg, email=False):
  """
  Do not raise an error, but print out the message, store the log, and optionally email the log to myself
  @param warn_msg: the text to appear in the logging error log, and also printed on the command line when an error occurs
  @type warn_msg: string
  @param email: decide whether or not send an email with the error log; default=False
  @type email: boolean
  @return: None
  @rtype: None
  """
  print ' - ' + warn_msg
  logging.warning(warn_msg)
  
  if email: send_email
  
  return None

#=================================================================================================================
def err(err_msg, email=False):
  """
  Raise an error SystemExit exception. print out the message, store the log, and optionally email the log to myself
  @param err_msg: the text to appear in the logging error log, and also printed on the command line when an error occurs
  @type err_msg: string
  @param email: decide whether or not send an email with the error log; default=False
  @type email: boolean
  @return: None
  @rtype: None
  """
  print ' - ' + err_msg
  logging.error(err_msg)
  
  if email: send_email
  
  raise SystemExit, err_msg
  
  return None

#=================================================================================================================
def send_email(subject='Report', text='Text'):
  """
  Send an email along with error message to myself for long jobs that may come up with a surprise
  """

  gmail_user = "e.moravveji@gmail.com"
  gmail_pwd = "AstroMan8899"
  FROM = 'e.moravveji@gmail.com'
  TO = ['e.moravveji@gmail.com'] #must be a list
  SUBJECT = subject
  TEXT = text

  # Prepare actual message
  message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
  """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
  try:
     server = smtplib.SMTP("smtp.gmail.com", 587) #or port 465 doesn't seem to work!
     server.ehlo()
     server.starttls()
     server.login(gmail_user, gmail_pwd)
     server.sendmail(FROM, TO, message)
     server.close()
     logging.info('debug: send_email: Email Successfully Sent')
     print ' - debug: send_email: Email Successfully Sent'
  except:
     print " - debug: send_email: Failed"
                
  return None
#=================================================================================================================

# coding: utf-8

"""
This module aims at translating the mode periods (frequencies) from a non-rotating frame to a co-rotating frame,
and later on to the observer's inertial frame following the formulae in
- B13: Bouabid M. P. et al. 2013, MNRAS, 429, 2500–2514
- T00: Townsend R. H. D. 2000, MNRAS
- T03: Townsend R. H. D. 2003, MNRAS, 340, 1020-1030
Here, we do such translation using the Traditional Approximation for Rotation, TAR.
Additional material can be found in 
- Eckart (1960)
- Lee and Saio (1993, 1997)
- Townsend (2000)
"""

import sys, os, glob, shutil
import logging, warnings
import time
import h5py
import subprocess
import socket
import numpy as np
from mesa_gyre import commons
import read
from submit_jobs import job_array_gyre as jag
from scipy.optimize import curve_fit, brentq
import trad, tradtable               # developed by Richard Townsend

warnings.filterwarnings('error')     # capture warnings like errors!

dbg = False

#=========================================================================================
dic_conv = commons.conversions()
Hz_to_cd = dic_conv['Hz_to_cd']
#=========================================================================================

#=========================================================================================
#   B R E A K - U P   F R E Q U E N C Y / V E L O C I T Y   R O U T I N E S
#=========================================================================================
def freq_nonrot_to_freq_corot(el, em, nu, TradTable, freq_nonrot, freq_rot):
  """
  Using the tabulated coefficients for the Hough function (Townsend 2000, 2003, 2005), one can 
  just start from the unmodulated non-rotating frequencies, and correct for the effect of
  rotation within the Traditional Approximation (TAR). This function receives a list of 
  frequencies, and corrects them based on the input value of omega. 
  From Eq. (4) in B13:
     P_co(k) = sqrt(el(el+1))/sqrt(lambda) * P_nonrot
  """
  P_nonrot = 1.0 / freq_nonrot 
  lambdaa = get_lambda(el, em, nu, TradTable)

  return np.sqrt(el*(el+1.)) / np.sqrt(lambdaa) * P_nonrot

#=========================================================================================
def perturbative_omega_correction(freq, em, beta, omega, first_order=True):
  """
  First or higher-order perturbative rotation correction to the input frequencies in the 
  co-rotating frame. if first_order=True,
      freq_pert = freq + em * beta * omega
  where freq, em, beta and omega are defined below. For higher order corrections see 
  Goupil & ... (201?)
  @param freq: a single or an array of frequencies in the inertial (e.g. non-rotating frame).
         they are normally computed by GYRE.
  @type freq: float or ndarray
  @param em: mode azimuthal degree; em can take the following discrete values: em=0 (zonal),
         em>0 (prograde) or em<0 (retrograde).
  @type em: integer
  @param beta: an integer or array of Ledoux constants for the modes; see Aerts et al. (2010,
         Sect. ...).
  @type beta: float or ndarray
  @param omega: rotation rate in Hz; it is asssumed that the rotation is rigid (solid body),
         therefore, we cannot account for differentially-rotating stars for now
  @type omega: float
  @param first_type: flag, specifying how many perturbative correction terms to include in 
         the final frequencies; if False, then, 3rd order correction of Goupil et al. (...) 
         will be considered. default=True
  @type first_order: boolean
  @return: perturbed frequency(ies) based on the input em, beta and omega
  @rtype: float or ndarray
  """
  if not isinstance(em, int):
    logging.error('tar: perturbative_omega_correction: em not integer')
    raise SystemExit, 'tar: perturbative_omega_correction: em not integer'
    
  if not first_order:
    logging.error('tar: perturbative_omega_correction: Higher-order corrections not supported')
    raise SystemExit, 'tar: perturbative_omega_correction: Higher-order corrections not supported'
    
  # if freq has to be modified due to the Hough function and/or the frequencies need to be
  # clipped off to match the observed range, it should all happen here  
    
  return freq + em * beta * omega

#=========================================================================================
def get_lambda(el, em, nu, TradTable):
    """
    This is a shortcut to TradTable.lambda(el, em, nu)
    The lambda is defined as 
        lambda = np.sqrt(l_e * (l_e + 1))
    @param el: mode degree in the non-rotating frame 
    @type el: integer
    @param em: mode azimuthal degree in the rotating frame 
    @type em: integer
    @param nu: spin parameter. You can get it, for any mode frequency freq_nonrot, 
          by calling get_spin(), knowing the star rotation frequency
    @type nu: float
    @param TradTable: Tabulated dataset containng data for TAR. It can be retrieved by calling 
          tar.get_TradTable(path_to_table)
    @type TradTable:
    @return: lambda
    @rtype: float
    """

    return TradTable.lamb(el, em, nu)

#=========================================================================================
def get_el_eff(el, em, nu, TradTable):
    """
    This is a shortcut to TradTable.l_e(el, em, nu).
    The eigenvalues of the Laplace tidal equation, lambda can be written as
         lambda = el_eff * (el_eff + 1.0)
    @param el: mode degree in the non-rotating frame 
    @type el: integer
    @param em: mode azimuthal degree in the rotating frame 
    @type em: integer
    @param nu: spin parameter. You can get it, for any mode frequency freq_nonrot, 
          by calling get_spin(), knowing the star rotation frequency
    @type nu: float
    @param TradTable: Tabulated dataset containng data for TAR. It can be retrieved by calling 
          tar.get_TradTable(path_to_table)
    @type TradTable:
    @return: effective degree
    @rtype: float
    """

    return TradTable.l_e(el, em, nu)

#=========================================================================================
def get_surface_v_rot(M_star, R_star, eta_rot, to_cgs=True):
  """
  Compute the surface rotation velocity [in km/sec] using the following relation 
     v_surf = 2 * pi * R_star * eta_rot * Omega_crit * cm_to_km
  Omega_crit is returned by a call to tar.get_omega_crit().
  @param M_star, R_star: mass and radius of the star with respect to the Sun, e.g. M_star=2.
  @type M_star, R_star: float
  param eta_rot: the rotation rate w.r.t. the critical rotation rate
  @type eta_rot: float
  @param to_cgs: flag to convert the input M_star and R_star to CGS units, if they are expressed
        w.r.t. the Solar mass and radius
  @type to_cgs: boolean
  @return: surface rotation velocity in km/sec 
  @type float 
  """
  cm_to_km   = 1e-5
  Omega_crit = get_omega_crit(M_star, R_star, to_cgs)
  Omega_rot  = eta_rot * Omega_crit
  if to_cgs:
    R_sun    = 6.9598e10
    R_star   = R_star * R_sun
  v_rot      = R_star * Omega_rot * cm_to_km

  return v_rot

#=========================================================================================
def get_freq_crit(M_star, R_star, to_cgs=True):
  """
  To handle the "implicit" 2pi difference between the omega_crit and freq_crit, this function
  just calls tar.get_omega_crit(), and divides the result by a factor 2pi.
  For the meaning of the arguments, refer to tar.get_omega_crit()
  """
  
  return get_omega_crit(M_star, R_star, to_cgs) / (2.0 * np.pi)

#=========================================================================================
def get_Roche_freq_crit(M_star, R_star, to_cgs=True):
  """
  To handle the "implicit" 2pi difference between the omega_crit and freq_crit, this function
  just calls tar.get_Roche_omega_crit(), and divides the result by a factor 2pi.
  For the meaning of the arguments, refer to tar.get_omega_crit()
  """
  
  return get_Roche_omega_crit(M_star, R_star, to_cgs) / (2.0 * np.pi)

#=========================================================================================
def get_omega_crit(M_star, R_star, to_cgs=True):
  """
  Compute the critical angular rotation velocity at the surface based on the following formula:
     Omega_crit^2 = GM/R_p^3
  where we assume that the input radius, R_star is the polar radius R_p. 
  NOTE: There is a 2.0 pi factor implicit in this, that's why it is called "omega". 
  To get the critical rotation frequency call tar.get_freq_crit().
  @param M_star, R_star: mass and radius of the star with respect to the Sun, e.g. M_star=2.
  @type M_star, R_star: float
  @return: critical angular rotation frequency in Hertz
  @rtype: float
  """
  # raise SystemExit, 'Error: tar: get_omega_crit: You are calling an obsolete function.'
  
  G     = 6.67428e-8
  M_sun = 1.9892e33
  R_sun = 6.9598e10

  # print M_star, M_star/M_sun
  # print R_star, R_star/R_sun
  # sys.exit()
  if to_cgs:
    M_gr  = M_star * M_sun
    R_cm  = R_star * R_sun
  else:
    M_gr  = M_star
    R_cm  = R_star
  
  return np.sqrt( (G * M_gr) / (R_cm * R_cm * R_cm) )
  
#=========================================================================================
def get_Roche_omega_crit(M_star, R_star, to_cgs=True):
  """
  Compute the critical angular rotation velocity at the surface based on the following formula:
     Omega_crit^2 = 8/27 * GM/R_p^3
  where we assume that the input radius, R_star is the polar radius R_p. This is the so-called
  Roche-model approximation. With this assumption, one neglects the centrifugal force, and 
  maintains the sphericity of the star.
  NOTE: There is a 2.0 pi factor implicit in this, that's why it is called "omega". 
  To get the critical rotation frequency call tar.get_freq_crit().
  @param M_star, R_star: mass and radius of the star with respect to the Sun, e.g. M_star=2.
  @type M_star, R_star: float
  @return: critical angular rotation frequency in Hertz
  @rtype: float
  """
  G     = 6.67428e-8
  M_sun = 1.9892e33
  R_sun = 6.9598e10

  # print M_star, M_star/M_sun
  # print R_star, R_star/R_sun
  # sys.exit()
  if to_cgs:
    M_gr  = M_star * M_sun
    R_cm  = R_star * R_sun
  else:
    M_gr  = M_star
    R_cm  = R_star
  
  return np.sqrt( (8.0 * G * M_gr) / (27.0 * R_cm * R_cm * R_cm) )

#=========================================================================================
def get_omega_uni(dic, eta_rot, omega_func):
  """
  Evaluate and return the rotation angular frequency (rad/sec) which is eta_rot percent 
  below the breakup rotation frequency. The breakup angular frequency (already including
  the 2xpi factor) is retrieved by calling omega_func callable
  @param dic: GYRE input data normally returned by calling read.read_gyre_in()
  @type dic: dict
  @param eta_rot: percentage equatorial rotation angular frequency w.r.t. the break up 
        angular frequency
  @type eta_rot: float
  @param omega_func: callable that returns the break up angular frequency in rad/sec. Examples
        of such functions are tar.get_Roche_omega_crit()
  @type omega_func: func
  @return: rotation angular frequency: \Omega_rot (rad/sec). Thus, it already has the 2 x pi 
        factor
  @rtype: float
  """
  if eta_rot < 0 or eta_rot > 1:
    logging.error('tar: get_omega_uni: eta_rot out of range [0 - 1]')
    raise SystemExit, 'Error: tar: get_omega_uni: eta_rot out of range [0 - 1]'

  M_star     = dic['M_star']
  R_star     = dic['R_star']
  omega_crit = omega_func(M_star, R_star, to_cgs=False)

  return eta_rot * omega_crit

#=========================================================================================
def from_inertial_to_co_rotating_frame(freq_rot, em, freq_inert):
  """
  Apply the advective (Doppler) term -- based on Eq. (3.315) in Aerts et al. (2010) -- and 
  shift the frequencies from the inertial frame, hence "freq_inert", to the co-rotating frame.
  The formula to use is :
        freq_corot = freq_inert - em * freq_rot
  NOTE: We do not check if "freq_rot" and "freq_inert" have the same unit! Be careful here.
  @param freq_rot: rotation frequency
  @type freq_rot: float 
  @param em: azimuthal order 
  @type em: integer 
  @param freq_inert: frequency list in the co-rotating frame. This routine is elemental, and 
         can work for a single-value "freq_inert" too.
  @type freq_inert: float or ndarray
  @return: frequency list in the co-rotating frame.
  @rtype: float or ndarray
  """

  return freq_inert - em * freq_rot

#=========================================================================================
def get_P_corot(el, el_eff, P_norot):
    """
    The period of a mode in the non-rotating frame is translated into that in the co-rotating frame, by using Eqs. (3)
    and (4) in B13. Assume
         L = sqrt(el * (el+1))
         and
         L_eff = sqrt(el_eff * (el_eff+1))
         then
         P_corot = L * P_norot / L_eff
    @param el: The harmonic polar degree of the mode 0<= el
    @type el: integer
    @el_eff: the effective harmonic degree in the TAR approximation; see T00
    @type el_eff: float
    @return: The mode period in the co-rotating frame P_corot
    @rtype: float
    """
    L = np.sqrt(el * (el+1.0))
    L_eff = np.sqrt(el_eff * (el_eff+1.0))
    if isinstance(P_norot, list):
      P_norot = np.array(P_norot)

    return L * P_norot / L_eff

#=========================================================================================
def get_freq_interial(em, freq_rot, freq_corot):
    """
    This routine calculates the mode's frequency in the inertial (observer) frame, 
    from that in the co-rotating frame freq_corot, knowing the rotation frequency freq_rot and the 
    azimuthal order of the mode em.
    The formula is 
           freq_inert = freq_corot + m * freq_rot
    Note that freq_corot and freq_rot must have the same units. 
    This routine accepts a single valued freq_corot, 
    @param em: the azimuthal degree of the mode: |em| <= el
    @type em: integer
    @param freq_corot: the mode frequency in the co-rotating frame
    @type freq_corot: float or ndarray
    @param freq_rot: the star rotation period
    @type freq_rot: float 
    @return: the mode period in the inertial frame P_inert
    @rtype: float or ndarray
    """
    if isinstance(freq_corot, list):
      freq_corot = np.array(freq_corot)

    return freq_corot + float(em) * freq_rot 

#=========================================================================================
def get_P_inertial(em, P_corot, P_rot):
    """
    This routine calculates the period of a mode in the inertial (observer) frame, 
    from that in the co-rotating frame P_corot, knowing the rotation period P_rot and the 
    azimuthal order of the mode em.
    The formula is taken from Eq. 5 in B13:
           P_inert = P_corot / (1 - m * P_corot/P_rot)
    Note that P_corot and P_rot must have the same units.
    @param em: the azimuthal degree of the mode: |em| <= el
    @type em: integer
    @param P_corot: the mode frequency in the co-rotating frame
    @type P_corot: float
    @param P_rot: the star rotation period
    @type P_rot: float
    @return: the mode period in the inertial frame P_inert
    @rtype: float
    """
    if isinstance(P_corot, list):
      P_corot = np.array(P_corot)

    return P_corot / (1.0 - em * P_corot/P_rot)

#=========================================================================================
def get_spin(freq_rot, freq_corot):
    """
    Evaluate the spin parameter (B13, below Eq. 2)
          nu = 2 * omega_rot / omega_corot, 
    where omega_rot is the rotation frequency of the star
    and omega_corot is the mode frequency in co-rotating frame. This routine accepts a single frequency or a list 
    (or ndarray) of co-rotating frequencies.
    @param freq_rot: rotation frequency in Hz 
    @type freq_rot: float 
    @param freq_corot: mode frequency(ies) in co-rotating frame 
    @type freq_corot: float of ndarray
    @return: spin parameter for input mode(s)
    @rtype: float or ndarray
    """
    if isinstance(freq_corot, list):
      freq_corot = np.array(freq_corot)

    return 2 * freq_rot / freq_corot

#=========================================================================================
def potential_x_y_theta(y, theta, to_radian):
  """
  Determine the deformed shape of a star by computing the equi-potential surface Phi, which
  depends on local radius (i.e. x(r, theta)), fraction of break up velocity (y=v_eq/v_crit),
  and the co-latitute (theta). Note that theta=0 at the pole. Then, based on Eq. (1) and (2)
  in Aerts, Lamers & Molenberghs (2004, A&A):
      Phi(x, y, theta) = 1/x + 0.5 * x^2 * y^2 * sin(theta),                          Eq (1)
  and one can adopt 
      x(theta, y) = 2 * A * sin(B),                                                   Eq (2)
  where
      A = sqrt(2+y^2)/(sqrt(3) * y * sin(theta)),
      B = arcsin( 3*sqrt(3)*y*sin(theta) / (2 + y^2)^1.5 )/3,
  and x = r(theta, y)/R_equator is computed internally. 
  Note that the definition of break up velocity, hence y, is
      v_crit = sqrt( (2 * G * M) / (3 * R_polar) ), 
      y = v_eq / v_crit
  and one can "assume" R_polar to be equal to the radius from non-rotating models! Crazy ...

  @param y: rotation rate with respect to the critical (break up) rotation velocity.
  @type y: float
  @param theta: co-latitute. If it is passed in degrees, it will be internally converted to 
         radians.
  @type theta: float
  @param to_radian: flag to convert theta into radians, if it is passed in degrees.
  @type to_radian: boolean
  @return: x and Phi (potential)
  @rtype: tuple
  """
  if to_radian:
    t = theta * np.pi / 180.0
  else:
    t = theta

  n   = len(t)
  x   = np.zeros(n)

  for i, t_i in enumerate(t):
    sin_t = np.sin(t_i)
    if sin_t == 0.0:
      x[i]   = 2 * np.sqrt(2 + y*y) / (np.sqrt(3) * y)
    elif sin_t <= np.sin(6.0 * np.pi / 180) and sin_t > 0:
      x[i]   = 2.0 / (2.0 + y*y)
    else:
      fraction = np.sqrt(2 + y*y) / (np.sqrt(3) * y * sin_t)
      arg_arcsin = (np.sqrt(27) * y * sin_t) / np.power(2 + y*y, 1.5)
      arg_sin  = np.arcsin(arg_arcsin) / 3.0
      x[i]  = 2.0 * fraction * np.sin( arg_sin )

  Phi   = 1./x + x*x * y*y * sin_t*sin_t / 2.0

  return x, Phi

#=========================================================================================
def get_velocity_crit_Conny(M_star, R_polar, to_cgs):
  """
  Compute the critical velocity based on Sect. (3.1) in Aerts, Lamers & Molenbergh (2004, A&A):
          v_crit = sqrt( (2 * G * M) / (3 * R_polar) ),
  This is valid for Eddington factor, Gamma < 0.64
  """
  if to_cgs:
    M_star *= M_sun
    R_polar *= R_sun

  return np.sqrt(2 * G * M_star / (3.0 * R_polar))

#=========================================================================================




#=========================================================================================
#         I N P U T / O U T P U T     R O U T I N E S
#=========================================================================================

#=========================================================================================
def gen_numbered_filename(dir_name, prefix, num_try, suffix):
    """
    For every iteration of the Simplex algorithm, a new file should be written to disk.
    The filename should carry the information about which try number it belongs to. So, the
    output filename has a fixed format:
          <dir_name>/<prefix>-NNN.<suffix>
    @param dir_name: directory name where the namelist will be stored.
           A trailing '/' character is added automatically if not present; this directory
           will be created if it does not exist
    @type dir_name: string 
    @param prefix, suffix: the prefix and suffix used to create the full filename
    @type prefix, suffix: string
    @param num_try: the counter for the attempt. If provided, it will be appended to the 
           filename with the format '%03d'. If set to None, it is skipped
    @type num_try: integer or NoneType
    @return: full path to where the namelist will be written to, by calling e.g. 
             write_nmlst_to_ascii()
    @rtype: string
    """
    if isinstance(num_try, int) and num_try >= 1000:
        logging.error('tar: gen_numbered_filename: Only 999 attempts are allowed')
        raise SystemExit, 'Error: tar: gen_numbered_filename: Only 999 attempts are allowed'

    if suffix[0] == '.': suffix = suffix[1:]
    if dir_name[-1] != '/': dir_name += '/'
    if not os.path.exists(dir_name): 
        print ' - tar: gen_numbered_filename: automatically creating {0}'.format(dir_name)
        os.makedirs(dir_name)

    if isinstance(num_try, int):
      numb_name = '{0}{1}-{2:03d}.{3}'.format(dir_name, prefix, num_try, suffix)
    elif num_try is None:
      numb_name = '{0}{1}.{2}'.format(dir_name, prefix, suffix)

    return numb_name

#=========================================================================================
def write_nmlst_to_ascii(nmlst, filename):
    """
    Flush the GYRE namelist data to an ASCII file. The namelist data, nmlst here, can be 
    read/produced by e.g. jag.read_sample_namelist_to_list_of_strings(nmlst_file), and could
    have been modified by tar.modify_nmlst() for the substitution of few selected fields.
    @param nmlst: GYRE-compatible namelist that is ready to be written to file 
    @type nmlst: list of strings
    @param filename: full path to the ASCII file where the data will be written to
    @type filename: string
    @return: None 
    @rtype: None
    """
    with open(filename, 'w') as w:
        w.writelines(nmlst)

    return None 

#=========================================================================================
def modify_nmlst(default_nmlst, list_dic_change):
    """
    This routine modifies the GYRE inlist information (passed as a list of strings by first 
    argument), and substitutes by the key, value pairs in the list of dictionaries. E.g.
    to change the name of the input file, one can call this routine as 
       new_nmlst = modify_nmlst(default_nmlst, [{'file':'model.gyre'}, {'summary_file':'output.h5'}])
    Note that the keys introduced in list_dic_change should be already available somewhere in 
    the default_nmlst as a whole or part of one of the strings
    @param default_nmlst: The GYRE namelist as read, and returned by 
          jag.read_sample_namelist_to_list_of_strings(namelist_file). Each line in the inlist 
          is a string in this list
    @type default_nmlst: list of strings
    @param list_dic_change: a collection of those fields that you want to modify. The key of 
          each dictionary should be a variable name in default_nmlst and should be positioned
          already on the left-hand-side of the "=" sign.
    @type list_dic_change: list of dictionaries
    @return: updated namelist with the same format as received, and with modified values
    @rtype: list of strings
    """
    if not isinstance(default_nmlst, list):
        logging.error('tar: modify_nmlst: input default_nmlst is not a list')
        raise SystemExit, 'Error: tar: modify_nmlst: input default_nmlst is not a list'

    n_lines = len(default_nmlst)
    if n_lines == 0:
        logging.error('tar: modify_nmlst: input default_nmlst is empty')
        raise SystemExit, 'Error: tar: modify_nmlst: input default_nmlst empty'

    n_change = len(list_dic_change)
    if n_change == 0: 
        return default_nmlst

    variables = []
    line_numbers = []
    for i, line in enumerate(default_nmlst):
        if '=' not in line: continue
        variable = line.split('=')[0].strip()    # remove leading and trailing white spaces
        variables.append(variable)
        line_numbers.append(i)

    keys_to_change = [dic.keys()[0] for dic in list_dic_change]
    handle_with_care = ['file', 'summary_file', 'summary_file_format', 'summary_item_list',
                        'mode_file_format', 'freq_frame', 'freq_units', 'grid_frame']
    new_nmlst       = default_nmlst[:]
    for i, key in enumerate(keys_to_change):
        try:
            ind = variables.index(key)
            num = line_numbers[ind]
        except ValueError:
            logging.error('tar: modify_nmlst: key:{0} not available'.format(key))
            raise SystemExit, 'Error: tar: modify_nmlst: key:{0} not available'.format(key)
        value   = list_dic_change[i][key]
        if key in handle_with_care:
          new_nmlst[num] = "  {0} = '{1}' \n".format(key, value)
        else:
          new_nmlst[num] = "  {0} = {1} \n".format(key, value)

    return new_nmlst

#=========================================================================================


#=========================================================================================
#       O P T I M I Z A T I O N    R O U T I N E S
#=========================================================================================

#=========================================================================================
#=========================================================================================
#=========================================================================================
#=========================================================================================
def proceed_to_finish(gyre_in, attempt_out):
  """
  As soon as N_dev becomes zero for a given eta_rot, the optimization procedure has succeeded, 
  and the resulting temporary GYRE output file should be copied to a permanent directory for 
  storage, and later post-processing. 
  @param gyre_in: full path to the input GYRE file 
  @type gyre_in: string 
  @param attempt_out: Full path to the directory where the temporary file will be stored 
         permanently.
  @type attempt_out: string 
  @return: None 
  @rtype: None 
  """

  gyre_out    = gen_gyre_out_from_gyre_in(gyre_in)
  move_attempt_file_to_gyre_out_dir(attempt_out, gyre_out)

  return None

#=========================================================================================
def check_num_model_and_obs_match(list_dic_h5, dic_star, sigma):
  """
  The outcome of optimization, if everything goes as expected, is that the N_dev is zero, 
  or in other words, the number of model frequencies between the lower and upper observed
  (period) range perfectly matches the number of observed modes. If this basic requirement
  is not fulfilled, then something has seriously gone wrong with the optimization procedure,
  and that has to be found, and resolved.
  @param list_dic_h5: List of GYRE output summary files, after optimization.
  @type list_dic_h5: list of dictionaries
  @param dic_star: Observed information for a star that we're modelling. It can be returned 
         by e.g. a call to one of the objects in mesa_gyre.stars module.
  @type dic_star: dictionary
  @param sigma: the distance away from the lowest and highest frequencies to search for the 
         modes in between. For KIC 7760680, sigma ~ 6 to 9 is appropriate.
  @type sigma: float
  @return: - True: if all models have exactly the same number of modes withing the observed range, 
                   identical to the observations. 
           - False: if even one of the outputs does not fulfil this requirement, we return False.
  @rtype: boolean
  """
  n = len(list_dic_h5)
  if n == 0:
    logging.error('tar: check_num_model_and_obs_match: Input list is empty')
    raise SystemExit, 'Error: tar: check_num_model_and_obs_match: Input list is empty'

  list_dic_freq  = dic_star['list_dic_freq']
  N_obs_modes    = len(list_dic_freq)
  list_freq      = np.array([dic['freq'] for dic in list_dic_freq]) 
  list_freq_err  = np.array([dic['freq_err'] for dic in list_dic_freq])
  ind_sort       = np.argsort(list_freq)
  list_freq      = list_freq[ind_sort]
  list_freq_err  = list_freq_err[ind_sort]
  min_obs_freq   = list_freq[0]  - sigma * list_freq_err[0]
  max_obs_freq   = list_freq[-1] + sigma * list_freq_err[-1]
  max_obs_per    = 1.0 / min_obs_freq  # in days
  min_obs_per    = 1.0 / max_obs_freq  # in days

  out       = []
  for i, dic in enumerate(list_dic_h5):
    freq    = np.real(dic['freq']) * Hz_to_cd
    per     = 1.0 / freq               # in days
    ind     = np.where((per >= min_obs_per) & (per <= max_obs_per))[0]
    n_ind   = len(ind)
    flag    = n_ind == N_obs_modes
    out.append(flag)

  if all(out):
    print ' - tar: check_num_model_and_obs_match: All models have {0} modes in the observed range'.format(N_obs_modes)
    return True
  else:
    print '    WARNING'
    print ' - tar: check_num_model_and_obs_match: At least one problem found \n'
    return False

#=========================================================================================
def get_eta_rot_success(list_eta_rot, list_N_dev):
  """
  Walk through the provided list of deviations (of observations vs. models for number of modes),
  and return the eta_rot associated to the zero of list_N_dev. 
  In case there are no zeros in the supplied list of deviations "list_N_dev", return False, 
  meaning total failure.
  @param list_eta_rot: list of trial (and/or optimized) rotation rates, eta_rot.
  @type list_eta_rot: list of floats
  @param list_N_dev: list of deviations of the number of modelled modes vs. observations
  @type list_N_dev: list of integers
  @return: Two possibile return values and their meanings are: 
        - False: meaning that none of the eta_rot correspond to a zero in N_dev, thus, just failure.
        - float: associated with the optimized eta_rot value.
  @rtype: False or float
  """
  if len(list_eta_rot) != len(list_N_dev):
    logging.error('tar: get_eta_rot_success: The length of the two input lists are incompatible')
    raise SystemExit, 'Error: tar: get_eta_rot_success: The length of the two input lists are incompatible'

  list_success = [N_dev == 0 for N_dev in list_N_dev]
  if not any(list_success):
    return False

  for i, flag in enumerate(list_success):
    if list_success[i]: 
      return list_eta_rot[i]
      break

#=========================================================================================
def check_iterative_success(iter_eta_rot, iter_N_dev):
  """
  During the iterative optimization steps (by calling tar.iterative_attempts()),
  the number of modelled and observed modes between the observed range, i.e. N_dev, could become
  zero. Thus, no further iteration is needed, and we can immediately proceed to check out!
  @param iter_eta_rot: a list of several floats for [eta_rot_0, eta_rot_1, eta_rot_2, ...], 
         respectively.
  @type init_eta_rot: list of floats
  @param init_dev: a list of several integers giving the deviations per each trial eta_rot
         [N_dev_0, N_dev_1, N_dev_2, ...], respectively.
  @type init_dev: list of integers
  @return: True for "successful" initial attmpts, or False otherwise
  @rtype: boolean
  """
  num_iters   = len(iter_eta_rot)
  list_status = [check_N_dev_zero(iter_N_dev[i]) for i in range(num_iters)]
  if not any(list_status):
    return False 
  else:
    return True
  
#=========================================================================================
def check_initial_success(init_eta_rot, init_dev):
  """
  During the first two initial optimization steps (by calling tar.two_initial_eta_attempts()),
  the number of modelled and observed modes between the observed range, i.e. N_dev, could become
  zero. If so, this is a good luck, and has to be embraced ;-)
  Thus, no further iteration is needed in such cases, and we can immediately proceed to check out!
  @param init_eta_rot: a list of two floats for [eta_rot_0, eta_rot_1], respectively.
  @type init_eta_rot: list of two floats
  @param init_dev: a list of two integers giving the deviations per each trial eta_rot
  @type init_dev: list of integers
  @return: True for "successful" initial attmpts, or False otherwise
  @rtype: boolean
  """
  if not isinstance(init_eta_rot, list) or not isinstance(init_dev, list):
    logging.error('tar: check_initial_success: Inputs are not "list" objects')
    raise SystemExit, 'tar: check_initial_success: Inputs are not "list" objects'

  # shout success as soon as you capture any
  for N_dev in init_dev:
    success = check_N_dev_zero(N_dev)
    if success:
      return success

  # no success means failure:
  return False

#=========================================================================================
def check_N_dev_zero(N_dev):
  """
  During the eta_rot optimization process, if at any point, N_dev is zero, then the optimization
  should stop, and that specific eta_rot should be chosen as the optimal value.
  # @param eta_rot: trial eta_rot
  # @type eta_rot: float 
  @param N_dev: the resulting deviations from observations
  @type N_dev: integer
  @return: True if N_dev is zero, and False otherwise
  @rtype: boolean
  """

  return N_dev == 0

#=========================================================================================
def Secant_eta_rot_slope(eta_rot_a, eta_rot_b, N_dev_a, N_dev_b):
  """
  The slope of a line passing through two points, where the abscissa is associated with the trial
  rotation rate (w.r.t. the critical rotation rate), and the ordinate is the discreminant function
         slope = (Y_b - Y_a) / (X_b - X_a)
  @param eta_rot_a: first trial rotation rate 
  @type eta_rot_a: float 
  @param eta_rot_b: last trial rotation rate 
  @type eta_rot_b: float 
  @param N_dev_a: first value of the discreminant corresponding to eta_rot_a
  @type N_dev_a: integer
  @param N_dev_b: last value of the discreminant corresponding to eta_rot_b
  @type N_dev_b: integer
  @return: slope of the line connecting the two points 
  @rtype: float
  """

  return (N_dev_b - N_dev_a) / (eta_rot_b - eta_rot_a)

#=========================================================================================
def decide_Brent_or_rescue(list_eta_rot, list_N_dev):
  """
  Brent can only be called directly, if all N_dev values in the "list_N_dev" list are unique.
  In case there are repeating cases of N_dev for two different -- yet very close -- trial 
  eta_rot values, the Brent should not be called. In such cases, the rescue plan shall be 
  pursued. For this clear reason, we decide which one of the two approaches to pick up.
  @return: The decison being made:
           True: use Brent method, and call Brent_guess_next_eta_rot()
           False: pick up the alternate plan, and call Brent_rescue_plan()
  @rtype: boolean
  """
  n_orig   = len(list_N_dev)
  n_unik   = len(set(list_N_dev))
  decision = n_orig == n_unik

  return decision

#=========================================================================================
def prepare_input_for_Brent(list_eta_rot, list_N_dev):
  """
  The input to the Brent method (i.e. the Brent_guess_next_eta_rot() routine), or the alternative
  rescue plan should fulfil monotonicity and exclude repetitions of N_dev, for much better 
  result (faster convergence to the root). Thus, before asking Brent to return the potential 
  root, we "prepare" the input beforehand.
  @return: two items that are ready for Brent (or the rescue plan) to be used:
           - new list_eta_rot as ndarray
           - new list_N_dev as ndarray
  @rtype: tuple
  """
  n = len(list_eta_rot)
  if n < 3:
    logging.error('tar: prepare_input_for_Brent: Input should at least have 3 items')
    raise SystemExit, 'Error: tar: prepare_input_for_Brent: Input should at least have 3 items'

  if isinstance(list_eta_rot, list): list_eta_rot = np.array(list_eta_rot)
  if isinstance(list_N_dev, list):   list_N_dev   = np.array(list_N_dev)

  # First, sort based on eta_rot values
  ind            = np.argsort(list_eta_rot)
  list_eta_rot   = list_eta_rot[ind]
  list_N_dev     = list_N_dev[ind]

  orig_eta_rot   = np.copy(list_eta_rot)
  orig_N_dev     = np.copy(list_N_dev)
  n_orig         = len(orig_N_dev)

  set_N_dev      = set(orig_N_dev)
  n_set_N_dev    = len(set_N_dev)
  if n_orig == n_set_N_dev:
    # All N_dev values are unique => proceed to the Brent method directly
    return (orig_eta_rot, orig_N_dev)
  else:
    # There is at least one repeated element. Exclude them, and then use Brent method
    unik_N_dev, unik_ind = np.unique(orig_N_dev, return_index=True)
    list_eta_rot   = orig_eta_rot[unik_ind]
    list_N_dev     = orig_N_dev[unik_ind]
    n              = len(list_N_dev)

    for i in range(n-1):
      N_i   = list_N_dev[i]
      N_ip1 = list_N_dev[i+1]
      if N_i * N_ip1 < 0: break

    list_eta_rot   = list_eta_rot[i : i+3]
    list_N_dev     = list_N_dev[i : i+3]

    return (list_eta_rot, list_N_dev)

#=========================================================================================
def parabola(x, a, b, c):
  """
  Return elemental values of f(x) = a*x^2 + b*x + c 
  for finding the root, x0, where f(x0) ~ 0
  """
  return a*x*x + b*x + c

def parabola_minus_1(x, a, b, c):
  """
  Return elemental values of f(x) = g(x) - 1 = a*x^2 + b*x + c - 1
  for finding the root, x0, where g(x0) ~ 1
  """
  return a*x*x + b*x + c - 1

def parabola_plus_1(x, a, b, c):
  """
  Return elemental values of f(x) = h(x) + 1 = a*x^2 + b*x + c + 1
  for finding the root, x0, where h(x0) ~ -1
  """
  return a*x*x + b*x + c + 1

#=========================================================================================
def narrow_N_dev_range_by_interpol(which_gyre, dic_model, default_nmlst, gyre_in_file, 
              gyre_out_file, dir_nmlst, dic_star, eta_rot, sigma, previous_result,
              list_eta_rot, list_N_dev, third_eta_rot):
  """
  When N_dev values are very large, it is possible to narrow down the "guess" interval 
  to such eta_rot values, for which N_dev becomes -1, 0 and +1 respectively. This is done
  only by three interpolations.
  Note: - An extra call to GYRE is made here for the third trial eta_rot.
        - The result of this routine is that when it is called, list_eta_rot and list_N_dev
          have only two elements from previous calls to GYRE; however, after calling this 
          routine, the third call is made, and the interpolation will be based on three
          actuall GYRE results.
        - when calling one_attempt(), we set eta_rot=third_eta_rot.
        - third_eta_rot should come from a call to tar.third_eta_attempt() 
        - for the meaning of the input arguments, refer to, e.g. one_attempt()
  """
  # find the third N_dev for the third eta_rot by direct GYRE call
  N_obs_modes = len(dic_star['list_dic_freq'])
  tup_third   = one_attempt(which_gyre=which_gyre, dic_model=dic_model, default_nmlst=default_nmlst, 
                    gyre_in_file=gyre_in_file, gyre_out_file=gyre_out_file, dir_nmlst=dir_nmlst,
                    dic_star=dic_star, eta_rot=third_eta_rot, sigma=sigma, previous_result=previous_result)
  third_N_dev = tup_third[0] - N_obs_modes
  list_eta_rot.append(third_eta_rot)
  list_N_dev.append(third_N_dev)
  ind_sort    = np.argsort(list_N_dev)
  list_eta_rot= [list_eta_rot[i] for i in ind_sort]
  list_N_dev  = [list_N_dev[i] for i in ind_sort]

  list_eta_rot= np.array(list_eta_rot)
  list_N_dev  = np.array(list_N_dev)

  # fit a quadratic function and get the fit coefficients
  try:  
    fit = curve_fit(parabola, list_eta_rot, list_N_dev)
  except ValueError:
    logging.error('tar: narrow_N_dev_range_by_interpol: curve_fit ValueError')
    raise SystemExit, 'Error: tar: narrow_N_dev_range_by_interpol: curve_fit ValueError'
  c1, c2, c3    = fit[0]

  # find the zero of the parabola, x_mid
  y_a, y_b, y_c = parabola(list_eta_rot, c1, c2, c3)
  if y_a * y_b <= 0:
    a = list_eta_rot[0]
    b = list_eta_rot[1]
  elif y_b * y_c <= 0:
    a = list_eta_rot[1]
    b = list_eta_rot[2]
  else:
    logging.warning('tar: narrow_N_dev_range_by_interpol: Fitted function may have no zero!')
    a = list_eta_rot[0]
    b = list_freq_err[2]

  x_mid  = brentq(parabola, a, b, args=(c1, c2, c3), xtol=1e-6, rtol=1e-6, disp=True)
  if not isinstance(x_mid, float):
    logging.error('tar: narrow_N_dev_range_by_interpol: x_mid failed')
    raise SystemExit, 'Error: tar: narrow_N_dev_range_by_interpol: x_mid failed'

  # find the -1 value of the parabola
  if list_N_dev[0] < -1:
    a         = list_eta_rot[0]
    b         = x_mid
    x_left    = brentq(parabola_plus_1, a, b, args=(c1, c2, c3), xtol=1e-6, rtol=1e-6, disp=True)
  else:
    x_left    = list_eta_rot[0]

  # find the +1 value of the parabola
  if list_N_dev[2] > +1:
    a         = x_mid
    b         = list_eta_rot[2]
    x_right   = brentq(parabola_minus_1, a, b, args=(c1, c2, c3), xtol=1e-6, rtol=1e-6, disp=True)
  else:
    x_right   = list_eta_rot[2]

  if not isinstance(x_left, float):
    logging.error('tar: narrow_N_dev_range_by_interpol: x_left failed')
    raise SystemExit, 'Error: tar: narrow_N_dev_range_by_interpol: x_left failed'
  if not isinstance(x_right, float):
    logging.error('tar: narrow_N_dev_range_by_interpol: x_right failed')
    raise SystemExit, 'Error: tar: narrow_N_dev_range_by_interpol: x_right failed'

  y_left  = parabola(x_left, c1, c2, c3)
  y_mid   = parabola(x_mid, c1, c2, c3)
  y_right = parabola(x_right, c1, c2, c3)
  narr_eta_rot = np.array([x_left, x_mid, x_right])
  narr_N_dev   = np.array([y_left, y_mid, y_right])

  if dbg:
    print 'yleft: ', y_left 
    print 'ymid:  ', y_mid 
    print 'yright:', y_right
    print 'origal: ', list_eta_rot, list_N_dev
    print 'narrow: ', narr_eta_rot, narr_N_dev

  return (narr_eta_rot, narr_N_dev)

#=========================================================================================
def narrow_N_dev_range_by_one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file,
                    gyre_out_file, dir_nmlst, dic_star, eta_rot, sigma, previous_result,
                    list_eta_rot, list_N_dev):
  """
  Make one call to GYRE and add the third item to list_N_dev (which has two elements as input).
  Thus, list_N_dev is ready for the first Brent root-finding iteration.
  The trial eta_rot is the mid-point between the two values provided by "list_eta_rot".
  The two values in "list_N_dev" should already cross the zero.
  For the definition of the arguments, refer to tar.one_attempt().
  @return: Three items are returned:
           - list_eta_rot: with three elements, where the second element is the mid-point eta_rot.
           - list_N_dev: with three elements, where the second element is N_dev after calling GYRE 
                       using the midpoint eta_rot
           - last_dic: dictionary of the GYRE output
  @rtype: tuple
  """
  if len(list_eta_rot) != 2 or len(list_N_dev) !=2:
    logging.error('tar: narrow_N_dev_range_by_one_attempt: Input eta_rot and N_dev must exactly have two elements.')
    raise SystemExit, 'Error: tar: narrow_N_dev_range_by_one_attempt: Input eta_rot and N_dev must exactly have two elements.'
  
  x_mid = (list_eta_rot[0] + list_eta_rot[1]) / 2.0
  N_obs_modes = len(dic_star['list_dic_freq'])

  tup_res = one_attempt(which_gyre=which_gyre, dic_model=dic_model, default_nmlst=default_nmlst, 
                    gyre_in_file=gyre_in_file, gyre_out_file=gyre_out_file, dir_nmlst=dir_nmlst,
                    dic_star=dic_star, eta_rot=x_mid, sigma=sigma, previous_result=previous_result)
  N_dev   = tup_res[0] - N_obs_modes
  last_dic= tup_res[1]

  list_eta_rot = [list_eta_rot[0], x_mid, list_eta_rot[1]]
  list_N_dev   = [list_N_dev[0], N_dev, list_N_dev[1]]

  return (list_eta_rot, list_N_dev, last_dic)

#=========================================================================================
def narrow_N_dev_range_by_three_attempts(which_gyre, dic_model, default_nmlst, gyre_in_file,
                  gyre_out_file, dir_nmlst, dic_star, eta_rot, sigma, previous_result,
                  list_eta_rot):
  """
  Make three calls to GYRE to get the accurate N_dev. The eta_rot are taken from the previous
  call to narrow_N_dev_range_by_interpol() and are only estimated values for narrowing down the 
  root of N_dev. The actual values will be returned as a new array of "list_N_dev", together with
  the input trial "list_eta_rot".
  For the meaning of the input arguments, refer to the documentation below tar.one_attempt()
  """
  n_eta = len(list_eta_rot)
  if n_eta != 3:
    logging.error('tar: narrow_N_dev_range_by_three_attempts: Input "list_eta_rot" must exactly have 3 elements.')
    raise SystemExit, 'Error: tar: narrow_N_dev_range_by_three_attempts: Input "list_eta_rot" must exactly have 3 elements.'

  N_obs_modes = len(dic_star['list_dic_freq'])
  list_N_dev  = []
  for eta_rot in list_eta_rot:
    tup_res   = one_attempt(which_gyre=which_gyre, dic_model=dic_model, default_nmlst=default_nmlst, 
                      gyre_in_file=gyre_in_file, gyre_out_file=gyre_out_file, dir_nmlst=dir_nmlst,
                      dic_star=dic_star, eta_rot=eta_rot, sigma=sigma, previous_result=previous_result)
    N_dev     = tup_res[0] - N_obs_modes
    list_N_dev.append(N_dev)

  return (list_eta_rot, list_N_dev)

#=========================================================================================
def Brent_guess_next_eta_rot(list_eta_rot, list_N_dev):
  """
  Following Eqs. (9.3.2), (9.3.3) and (9.3.4) in Sect. 9.3 of the famous Numerical Recipes 
  (Press et al. 2007) for the Van Wijngaarden-Dekker-Brent scheme, this function tries to 
  update the guess for eta_rot around the middle point (which provides the closest point to 
  the zero). The three requird points are (a, f(a)), (b, f(b)) and (c, f(c)), where 
  a < b < c, and f(a) < f(b) < f(c).
  This is useful to use, specifically, after the first two initial attempts (by calling
  two_initial_eta_attempts()) followed by a Secant method (by calling Secant_guess_next_eta_rot())
  to guess the position of the midpoint (b).
  @return: Two possible outputs are:
           - False: if Q < tiny (=1e-6 by default). In this case, one has to set into a rescue plan 
           - float: x = b + P/Q, which is the new guess for the root
  @rtype: False or float
  """
  tiny = 1e-6

  if len(list_eta_rot) < 3:
    logging.error('tar: Brent_guess_next_eta_rot: Input lists should have at least three values!')
    raise SystemExit, 'Error: tar: Brent_guess_next_eta_rot: Input lists should have at least three values!'

  if len(list_eta_rot) > 3:
    list_eta_rot = list_eta_rot[-3 : ]
    list_N_dev   = list_N_dev[-3 : ]

  if dbg: print ' Brent: ', list_N_dev

  a   = list_eta_rot[0]
  b   = list_eta_rot[1]
  c   = list_eta_rot[2]
  f_a = list_N_dev[0]
  f_b = list_N_dev[1]
  f_c = list_N_dev[2]

  R   = float(f_b) / f_c
  S   = float(f_b) / f_a
  T   = float(f_a) / f_c

  P   = S * ( T * (R-T) * (c-b) - (1.0-R) * (b-a) )
  Q   = (T-1.0) * (R-1.0) * (S-1.0)

  if np.abs(Q) <= tiny: 
    return False

  x   = b + P / Q

  return x

#=========================================================================================
def Brent_rescue_plan(list_eta_rot, list_N_dev):
  """
  It can quite frequently (~30%) happen that the Q parameter becomes zero during the Brent 
  computation. This occurs when two (very close) eta_rot values have identical N_dev values.
  Consequently, one of R, S and/or T ratios becomes one, and Q=0. 
  To rescue, we omit the repeating root, and return a new root, eta_rot, which possibly 
  passes through zero, by linear interpolation.
  @param list_eta_rot: list of trial eta_rot from previous steps. Most probably, two different 
         eta_rot values in this list (which are close) have identical N_dev. We omit one of 
         them, and propose a new root by linear interpolation.
  @type list_eta_rot: list of floats 
  @param list_N_dev: list of deviations between observed and modelled number of modes in the 
         observed range. There is most probably two repeating values in this list, which we 
         exclude.
  @type list_N_dev: list of integers
  @return: The rescue tiral eta_rot is the zero, x0, of a line passing through two (eta_rot, N_dev)
         points that change sign. Since, this can still not solve the issue entirely, we reduce
         x0 by a "random" factor r, taken "uniformly" between 0.98 and 1.02, i.e. x = r*x0. Then,
         we return x for further attempts. The choice of r enforces searching for the root in a 
         narrow window.
  @rtype: float
  """
  linear = len(list_N_dev) == 2
  quadratic = len(list_N_dev) >= 3 

  if linear:
    # Linear interpolation: find the zero of y=mx+b, where y=0 for x=x0
    if dbg: print ' Rescue: Linear: ', list_N_dev
    i  = 0 # already prepared formerly by prepare_input_for_Brent()
    x1 = list_eta_rot[i]
    x2 = list_eta_rot[i+1]
    y1 = list_N_dev[i]
    y2 = list_N_dev[i+1]
    m  = float(y2 - y1) / (x2 - x1)
    b  = y1 - m * x1
    x0 = -b / m

    r  = np.random.uniform(low=0.99, high=1.01)
    x  = x0 * r

    return x

  elif quadratic:
    # Quadratic interpolation: Curve fitting + Brent root finding to optimize the guess
    if dbg: print ' Rescue: Quadratic: ', list_N_dev

    try:  
      fit = curve_fit(parabola, list_eta_rot, list_N_dev) #, check_finite=True)
    except ValueError:
      logging.error('tar: Brent_rescue_plan: curve_fit ValueError')
      raise SystemExit, 'Error: tar: Brent_rescue_plan: curve_fit ValueError'

    c1, c2, c3    = fit[0]
    y_a, y_b, y_c = parabola(list_eta_rot, c1, c2, c3)
    if y_a * y_b <= 0:
      a = list_eta_rot[0]
      b = list_eta_rot[1]
    elif y_b * y_c <= 0:
      a = list_eta_rot[1]
      b = list_eta_rot[2]
    else:
      logging.warning('tar: Brent_rescue_plan: Fitted function may have no zero!')
      a = list_eta_rot[0]
      b = list_freq_err[2]

    x0  = brentq(parabola, a, b, args=(c1, c2, c3), xtol=1e-6, rtol=1e-6, disp=True)

    r  = np.random.uniform(low=0.98, high=1.02)
    x  = x0 * r

    return x

  else:
    logging.error('tar: Brent_rescue_plan: Specify Either Linear or Quadratic Brent rescue plan')
    raise SystemExit, 'Error: tar: Brent_rescue_plan: Specify Either Linear or Quadratic Brent rescue plan'
    return False

#=========================================================================================
def Newton_Raphson_guess_next_eta_rot(list_eta_rot, list_N_dev, n_poly=1):
  """
  When more than three points are available for (eta_rot, N_dev), it is possible to fit a 
  low-order polynomial to these points, and analytically derive the function derivatives, 
  to fascilitate faster convergence to the root of the discreminant function. This is more
  useful for dipole prograde (m=-1, as defined by Bouabid+2013, Fig. 4, right panel) modes,
  where the Secant method can fall into weird issues in fine-tuning eta_rot. For the 
  Newton-Raphson algorithm to work progressively better and better, we need to use the history
  of former root-finding attempts (where in the first two steps is done with the Secant method).
  Therefore, we pass the list of eta_rot and deviations found in former steps for previous 
  attempts.
  """
  if isinstance(list_eta_rot, list): list_eta_rot = np.array(list_eta_rot)
  if isinstance(list_N_dev, list):   list_N_dev   = np.array(list_N_dev)
  ind_sort     = np.argsort(list_eta_rot)
  list_eta_rot = list_eta_rot[ind_sort]
  list_N_dev   = list_N_dev[ind_sort]

  #####################################
  # Curve Fitting; Function Definitions
  #####################################
  def cubic(x, a, b, c, d):
    """
    Fit function is f(x) = a*x^3 + b*x^2 + c*x + d 
    """
    return a*x*x*x + b*x*x + c*x + d

  def deriv_cubic(x, a, b, c, d):
    """
    derivative of a cubic spline is f'(x) = 3*a*x^2 + 2*b*x + c
    """
    return 3.0*a*x*x + 2*b*x + c

  def parabola(x, a, b, c):
    """
    Fit function is f(x) = a*x*x + b*x + c
    """
    return a * x * x + b * x + c

  def deriv_parabola(x, a, b, c):
    """
    derivative of a parabola is f'(x) = 2*a*x + b
    """
    return 2.0 * a * x + b

  def line(x, a, b):
    """
    Fit function is f(x) = a*x + b
    """
    return a * x + b

  def deriv_line(x, a, b):
    """
    derivative of a line is f'(x) = a
    """
    return a

  #####################################
  # Do Curve Fitting, and Compute the 
  # Next Rotation Rate
  #####################################
  eta_rot_n      = list_eta_rot[-1]
  n_data         = len(list_eta_rot)
  max_poly       = 3
  n_poly         = min([ n_poly, n_data - 1 ]) 
  n_poly         = min([ max_poly, n_poly])

  print 'n_data={0}, n_poly={1}'.format(n_data, n_poly)

  if n_poly == 3:
    fit = curve_fit(f=cubic, xdata=list_eta_rot, ydata=list_N_dev)
    optimal_params = fit[0]
    covar_matrix   = fit[1]
    a              = optimal_params[0]
    b              = optimal_params[1]
    c              = optimal_params[2]
    d              = optimal_params[3]
    f_n            = cubic(eta_rot_n, a, b, c, d)
    slope_n        = deriv_cubic(eta_rot_n, a, b, c, d)

  if n_poly == 2:
    fit = curve_fit(f=parabola, xdata=list_eta_rot, ydata=list_N_dev)
    optimal_params = fit[0]
    covar_matrix   = fit[1]
    a              = optimal_params[0]
    b              = optimal_params[1]
    c              = optimal_params[2]
    f_n            = parabola(eta_rot_n, a, b, c)
    slope_n        = deriv_parabola(eta_rot_n, a, b, c)

  if n_poly == 1:
    fit = curve_fit(f=line, xdata=list_eta_rot, ydata=list_N_dev)
    optimal_params = fit[0]
    covar_matrix   = fit[1]
    a              = optimal_params[0]
    b              = optimal_params[1]
    f_n            = line(eta_rot_n, a, b)
    slope_n        = deriv_line(eta_rot_n, a, b)

  eta_rot_n_plus_1 = eta_rot_n - f_n / slope_n

  return eta_rot_n_plus_1

#=========================================================================================
def Secant_guess_next_eta_rot(eta_rot_n, N_deviation_n, slope_n):
  """
  Use a Secant method to guess the zero of the discreminant function, i.e. 
  the zero of the number of model frequencies subtracting the number of observed frequencies
  (see two_initial_eta_attempts() for more info). The slope is assumed from a line passing the
  former two attempts. We are forced to use Secant method because the discreminant is not analytic
  and its derivative cannot be computed analytically.
  In all input variable naming, the extension "_n" means the input value at the iteration step "n".
  The Secant method is 
         x_{n+1} = x_n - f(x_n) / f'(x_n)
  where f(x_n) is the function (that we try to seek its zero) evaluated at x_n, and f'(x_n) is
  its slope at the same mesh point.
  @param eta_rot_n: the former trial rotation rate at step "n"-th.
  @type eta_rot_n: float
  @param N_deviation_n: deviations of the number of model frequencies from observed frequencies
         which serves as our discreminant function (to be minimized), at step "n"-th.
  @type N_deviation_n: integer
  @param slope_n: the derivative of the discreminant from the former two attempts, assuming a line
         passing through the two former points (hence Secant method)
  @type slope_n: float
  @return: the guess for the next eta_rot that can (hopefully) serve as the zero of the discreminant
         function. In case the input slope (from the initial two attempts) is zero, returns False. 
         In this case, the issue is handled by another function to resolve the case.
  @rtype: float
  """
  if slope_n == 0:
    logging.warning('tar: Secant_guess_next_eta_rot: slope_n == 0: Retrying with different eta_rot')
    # raise SystemExit, 'Error: tar: Secant_guess_next_eta_rot: slope_n == 0'
    return False

  eta_rot_n_plus_1 = eta_rot_n - float(N_deviation_n) / slope_n
  if eta_rot_n_plus_1 < 0:
    logging.error('tar: Secant_guess_next_eta_rot: \
                       eta_{n+1}={0:.2e}; eta_n={1:.2e}; N_dev={2}; slope={3:.2e}'.format(
                       eta_rot_n_plus_1, eta_rot_n, N_deviation_n, slope_n))
    raise SystemExit, 'tar: Secant_guess_next_eta_rot: \
                       eta_{n+1}={0:.2e}; eta_n={1:.2e}; N_dev={2}; slope={3:.2e}'.format(
                       eta_rot_n_plus_1, eta_rot_n, N_deviation_n, slope_n)

  return eta_rot_n_plus_1

#=========================================================================================
def bisection_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file,
                       dir_nmlst, dic_star, sigma, previous_result, list_eta_rot, list_N_dev):
  """

  """
  if isinstance(list_eta_rot, list): list_eta_rot = np.array(list_eta_rot)
  if isinstance(list_N_dev, list):   list_N_dev   = np.array(list_N_dev)
  # ind_sort     = np.argsort(list_N_dev)
  ind_sort     = np.argsort(list_eta_rot)
  list_eta_rot = list_eta_rot[ind_sort]
  list_N_dev   = list_N_dev[ind_sort]
  if list_N_dev[0] * list_N_dev[-1] > 0:
    logging.error('tar: bisection_attempt: N_dev did not change sign yet')
    raise SystemExit, 'Error: tar: bisection_attempt: N_dev did not change sign yet'

  n            = len(list_N_dev)  
  ind_sign     = -1
  for i in range(n-1):
    if list_N_dev[i] * list_N_dev[i+1] <= 0:
      ind_sign = i
      break
  if ind_sign == -1:
    logging.error('tar: bisection_attempt: Could not detect any change of sign!')
    raise SystemExit, 'Error: tar: bisection_attempt: Could not detect any change of sign!'

  eta_rot_lo   = list_eta_rot[ind_sign]
  eta_rot_hi   = list_eta_rot[ind_sign + 1]
  eta_rot_mid  = (eta_rot_lo + eta_rot_hi) / 2.0   # <- this will be used for the attempt
  eta_rot_bisec= eta_rot_mid

  # print ind_sign, eta_rot_lo, eta_rot_hi, eta_rot_bisec

  tup_bisec    = one_attempt(which_gyre=which_gyre, dic_model=dic_model, default_nmlst=default_nmlst, 
                    gyre_in_file=gyre_in_file, gyre_out_file=gyre_out_file, dir_nmlst=dir_nmlst,
                    dic_star=dic_star, eta_rot=eta_rot_bisec, sigma=sigma, previous_result=previous_result)

  if tup_bisec is False: return False

  N_obs_modes  = len(dic_star['list_dic_freq'])

  N_model      = tup_bisec[0]
  bisec_dic    = tup_bisec[1]
  bisec_N_dev  = N_model - N_obs_modes

  result       = (eta_rot_bisec, bisec_N_dev, bisec_dic)

  return result

#=========================================================================================
def iterative_attempts(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file,
                       dir_nmlst, dic_star, eta_rot_0, eta_rot_1, sigma, previous_result,
                       eta_rot_Secant, N_dev_Secant, slope_Secant, 
                       list_eta_rot, list_N_dev):
  """
  If the first two attempts fail, then, we attempt to optimize eta_rot iteratively. For the 
  third step, we use the Secant method, and later on, we use the Brent method.

  For the definition of most of the arguments, refer to the documentation following
  tar.one_attempt(), and tar.Secant_guess_next_eta_rot(. For the last three arguments, 
  see below: 

  @param list_eta_rot: latest list of all eta_rot values that were already tried in the 
         previous steps.
  @type list_eta_rot: list of floats
  @param list_N_dev: latest list of all N_dev results that were acquired, one for each eta_rot
         value in the input list_eta_rot.
  @type list_N_dev: list of integers
  @return: Two possible return values are:
           - False: in this case, Brent_guess_next_eta_rot() or one_attempt() have failed for 
                    some reason, and extra tests are needed 
           - (eta_rot, N_dev, dic_freq) for the last attempt in the iteration. This output 
                    is different from that of tar.two_initial_eta_attempts().
  @rtype: False or a 3-component tuple
  """
  num_try = len(list_eta_rot)

  if num_try < 2:
    logging.error('tar: iterative_attempts: Only call this after two_initial_eta_attempts!')
    raise SystemExit, 'Error: tar: iterative_attempts: Only call this after two_initial_eta_attempts!'

  # First, decide which of the two possible optimizers to call
  N_obs_modes = len(dic_star['list_dic_freq'])
  num_try += 1
  Do_Secant = num_try == 3
  Do_Brent  = num_try > 3

  # Secant Method
  if Do_Secant and True:
    tup_try = third_eta_attempt(which_gyre=which_gyre, dic_model=dic_model, 
                      default_nmlst=default_nmlst, gyre_in_file=gyre_in_file, 
                      gyre_out_file=gyre_out_file, dir_nmlst=dir_nmlst, dic_star=dic_star, 
                      eta_rot_0=eta_rot_0, eta_rot_1=eta_rot_1, sigma=sigma, 
                      previous_result=previous_result,
                      eta_rot_Secant=eta_rot_Secant, N_dev_Secant=N_dev_Secant, 
                      slope_Secant=slope_Secant)
    eta_rot_try = tup_try[0]
    last_dic    = tup_try[1]

  if Do_Secant and False:
    tup_interp  = narrow_N_dev_range_by_interpol(which_gyre=which_gyre, dic_model=dic_model, 
                      default_nmlst=default_nmlst, gyre_in_file=gyre_in_file,
                      gyre_out_file=gyre_out_file, dir_nmlst=dir_nmlst, dic_star=dic_star, 
                      eta_rot=None, sigma=sigma, previous_result=previous_result,
                      list_eta_rot=list_eta_rot, list_N_dev=list_N_dev, 
                      third_eta_rot=eta_rot_try)
    list_eta_rot= tup_interp[0]
    list_N_dev  = tup_interp[1]
    if dbg:
      print list_eta_rot
      print list_N_dev
    tup_narrow  = narrow_N_dev_range_by_three_attempts(which_gyre=which_gyre, dic_model=dic_model, 
                      default_nmlst=default_nmlst, gyre_in_file=gyre_in_file,
                      gyre_out_file=gyre_out_file, dir_nmlst=dir_nmlst, dic_star=dic_star, 
                      eta_rot=None, sigma=sigma, previous_result=previous_result,
                      list_eta_rot=list_eta_rot)
    list_eta_rot= tup_narrow[0]
    list_N_dev  = tup_narrow[1]
    if dbg:
      print 'after 3 narrow calls to gyre:'
      print list_eta_rot
      print list_N_dev

  if Do_Secant and False:
    if dbg:
      print ' - Bisection narrows down'
      print 'before:'
      print list_eta_rot
      print list_N_dev
    tup_Bisec = narrow_N_dev_range_by_one_attempt(which_gyre=which_gyre, dic_model=dic_model, 
                    default_nmlst=default_nmlst, gyre_in_file=gyre_in_file,
                    gyre_out_file=gyre_out_file, dir_nmlst=dir_nmlst, dic_star=dic_star, 
                    eta_rot=None, sigma=sigma, previous_result=previous_result,
                    list_eta_rot=list_eta_rot, list_N_dev=list_N_dev)
    list_eta_rot= tup_Bisec[0]
    list_N_dev  = tup_Bisec[1]
    if dbg:
      print 'after:'
      print list_eta_rot
      print list_N_dev
    sys.exit()


  # Or Brent Method
  if Do_Brent:
    decision    = decide_Brent_or_rescue(list_eta_rot=list_eta_rot, list_N_dev=list_N_dev)
    tup_Brent   = prepare_input_for_Brent(list_eta_rot=list_eta_rot, list_N_dev=list_N_dev)
    list_eta_rot= tup_Brent[0]
    list_N_dev  = tup_Brent[1]

    if decision is True:

      eta_rot_try = Brent_guess_next_eta_rot(list_eta_rot=list_eta_rot, list_N_dev=list_N_dev)
      if eta_rot_try is False:
        if dbg: print 'Brent Q=0: calling Brent_rescue_plan()'
        eta_rot_try = Brent_rescue_plan(list_eta_rot=list_eta_rot, list_N_dev=list_N_dev)

    else:
      if dbg: print 'Repeated N_dev: calling Brent_rescue_plan()'
      eta_rot_try = Brent_rescue_plan(list_eta_rot=list_eta_rot, list_N_dev=list_N_dev)

  # Then, use the trial eta_rot to make the next attept in the iteration
  tup_try = one_attempt(which_gyre=which_gyre, dic_model=dic_model, default_nmlst=default_nmlst, 
                    gyre_in_file=gyre_in_file, gyre_out_file=gyre_out_file, dir_nmlst=dir_nmlst,
                    dic_star=dic_star, eta_rot=eta_rot_try, sigma=sigma, previous_result=previous_result)

  if tup_try is False: return False

  last_eta_rot = eta_rot_try
  last_N_model = tup_try[0]
  last_dic     = tup_try[1]
  last_N_dev   = last_N_model - N_obs_modes
  result       = (last_eta_rot, last_N_dev, last_dic)

  return result

#=========================================================================================
def retry_first_three_attempts(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file,
                      dir_nmlst, dic_star, eta_rot_0, eta_rot_1, sigma, previous_result):
    """
    This is a "rescue" function :-D
    It can happen that during the first two initial attempts plus the Secant attempt, the slope 
    found after the Secant method is zero. In such cases, the two_initial_eta_attempts() should
    be called again, but with "stretched" initial (and trial) eta_rot, to make sure we do not fall
    into this gory situation again.
    For the meaning of the input, refer to one_attempt() function.
    """
    list_dic_freq = dic_star['list_dic_freq']
    N_obs_modes = len(list_dic_freq)

    max_iter     = 21
    stretch_low  = 0.90
    stretch_high = 1.10

    rescued     = False
    for i in range(max_iter):
      eta_rot_0 = eta_rot_0 * stretch_high
      eta_rot_1 = eta_rot_1 * stretch_high

      tup_res   = two_initial_eta_attempts(which_gyre, dic_model, default_nmlst, gyre_in_file, 
                              gyre_out_file, dir_nmlst, dic_star, eta_rot_0, eta_rot_1, sigma)
      list_dev  = tup_res[1]
      slope     = tup_res[2]
      last_dic  = tup_res[3]
      N_dev_0   = list_dev[0]
      N_dev_1   = list_dev[1]

      eta_rot_try = Secant_guess_next_eta_rot(eta_rot_n=eta_rot_1, N_deviation_n=N_dev_1, slope_n=slope)

      tup_try   = one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file, 
                                  dir_nmlst, dic_star, eta_rot_try, sigma, last_dic)
      N_modes   = tup_try[0]
      last_dic  = tup_try[3]
      N_dev     = N_modes - N_obs_modes

      rescued   = N_dev != N_dev_1
      if rescued:
        return eta_rot_try, last_dic

    if not rescued:
      logging.error('tar: retry_first_three_attempts: Could not resolve the issue after {0} loops'.format(max_iter))
      raise SystemExit, 'Error: tar: retry_first_three_attempts: Could not resolve the issue after {0} loops'.format(max_iter)


#=========================================================================================
def third_eta_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file,
                dir_nmlst, dic_star, eta_rot_0, eta_rot_1, sigma, previous_result,
                eta_rot_Secant, N_dev_Secant, slope_Secant):
  """
  On the third attepmt (and after two unsuccessful initial attempts), the deviations of the 
  number of modelled versus observed modes (in the observed range) should be assessed only 
  using the Secant method. This routine uses the tar.Secant_guess_next_eta_rot() for that 
  purpose, and in case of failure, resorts back to a rescue plan by calling the 
  tar.retry_first_three_attempts(), until it finds the eta_rot for the third step of the 
  optimization procedure. 

  For the definition of most of the arguments, refer to the documentation following
  tar.one_attempt(). For the last three arguments, see below 

  @param eta_rot_Secant: The trial rotation rate for the third (Secant) step. For this 
         specific argument, we use the eta_rot from the 2nd attempt, received from a former 
         call to tar.two_initial_eta_attempts().
  @type eta_rot_Secant: float 
  @param N_dev_Secant: The deviation for the third (Secant) step. For this specific argument, 
         we use the eta_rot from the 2nd attempt, received from a former call to 
         tar.two_initial_eta_attempts().
  @type N_dev_Secant: integer
  @param slope_Secant: The slope of the line connecting the results of the last two attempts. 
         The slope is returned as the 2nd argument of the return value of a call to 
         tar.two_initial_eta_attempts().
  @type slope_Secant: float
  @return: rotation rate, eta_rot, associated with the third (Secant) step. 
  @rtype: float
  """

  eta_rot_3 = Secant_guess_next_eta_rot(eta_rot_n=eta_rot_Secant, N_deviation_n=N_dev_Secant, 
                  slope_n=slope_Secant)
  last_dic  = None

  if eta_rot_3 is False:
    tup_res = retry_first_three_attempts(which_gyre=which_gyre, dic_model=dic_model, 
                default_nmlst=nmlst, gyre_in_file=gyre_in, gyre_out_file=attempt_out,
                dir_nmlst=dir_namelist, dic_star=dic_KIC, eta_rot_0=eta_rot_0, 
                eta_rot_1=eta_rot_1, sigma=sigma, previous_result=previous_result)
    eta_rot_3 = tup_res[0]
    last_dic  = tup_res[1]

  logging.info('tar: third_eta_attempt: Success with eta_rot={0}'.format(eta_rot_3))

  return (eta_rot_3, last_dic)

#=========================================================================================
def two_initial_eta_attempts(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file,
                dir_nmlst, dic_star, eta_rot_0, eta_rot_1, sigma):
    """
    Make two or more attempts to call GYRE, compute the frequencies for the two proposed rotation rates 
    (eta_rot_0 and eta_rot_1) with the desired GYRE version/executable, and return the number of 
    frequencies N(eta_rot) within the observed range + the slope of a line connecting N0 = N(eta_rot_0)
    and N1 = N(eta_rot_1), for later Newton-Raphson root finder.
    @param which_gyre: The specific gyre_ad or gyre_nad and the version to call GYRE. You can 
           supply the full path to the GYRE executable, e.g. /home/me/gyre/bin/gyre_ad
           This is actually the recommended approach.
    @type which_gyre: string 
    @param dic_model: the input data as a dictionary, which is read directly from the gyre_in file.
           This is done by e.g. calling read.read_gyre_in(filename). 
    @type dic_model: dictionary
    @param dir_nmlst: the directory where the intermediate-step GYRE inlist files will be stored.
          Note that it is created upon the first access if it does not exist.
    @type dir_nmlst: string 
    @param default_nmlst: The GYRE namelist as read, and returned by 
          jag.read_sample_namelist_to_list_of_strings(namelist_file). Each line in the inlist 
          is a string in this list
    @type default_nmlst: list of strings
    @param gyre_in_file: full path to the input GYRE file that contains the whole structure
          info, and is used to compute frequencies. 
    @type gyre_in_file: string
    @param gyre_out_file: the full path filename where the intermediate-step GYRE output files will 
          be stored. 
    @type gyre_out_file: string
    @param dic_star: dictionary containing information of the star; this is normally returned 
          from mesa_gyre.stars module
    @type dic_star: dict
    @param eta_rot_0, eta_rot_1: the first two initial trial rotation rates, w.r.t. the critical 
          rotation frequency.
    @type eta_rot_0, eta_rot_1: float
    @param sigma: multiplication factor for the frequency errors of the first and last mode, 
          around which the model frequencies are enumerated. One should make sure not to exceed
          the frequency difference between two consecutive modes, by allowing very large sigma.
          For KIC 7760680, sigma=9 would be appropriate, taking care of frequency difference between
          f_35 and f_36 in Papics et al. (2015, ApJL) Table 1.
    @type sigma: float
    @return: Four items are returned:
          1. a list of trial eta_rot
          2. a list of the deviations from observed number of modes per each attempt.
             In case, we made more than two attempts, all intermediate (trial) results 
             are in these two lists too. This is useful to keep track of the produced
             information (which is quite costy per each GYRE call).
          3. the slope of the line connecting (eta_rot_0, N_deviation_0) and 
             (eta_rot_1, N_deviation_1), which is a float
          4. dictionary of GYRE summary file from the last attempt
    @rtype: tuple
    """
    N_obs_freq      = len(dic_star['list_dic_freq'])

    stretch_high    = 1.10
    list_eta_rot    = []
    list_N_dev      = []

    # Make one attempt for the first trial frequency
    res_0           = one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, 
                                  gyre_out_file, dir_nmlst, dic_star, eta_rot_0, sigma, None)
    if res_0 is False:
      logging.info('tar: two_initial_eta_attempts: first attempt failed. Increasing eta_rot_0')
      while res_0 is False:
        eta_rot_0   = eta_rot_0 * stretch_high
        res_0       = one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, 
                                  gyre_out_file, dir_nmlst, dic_star, eta_rot_0, sigma, None)

    if res_0 is False:
      logging.error('tar: two_initial_eta_attempts: First call to one_attempt() failed')
      raise SystemExit, 'Error: tar: two_initial_eta_attempts: First call to one_attempt() failed'
    else:
      N_0           = res_0[0]
      N_deviation_0 = N_0 - N_obs_freq
      dic_freq_0    = res_0[1]
      list_eta_rot.append(eta_rot_0)
      list_N_dev.append(N_deviation_0)
      logging.info('tar: two_initial_eta_attempts: First call to one_attempt() succeeded')


    # Make a second attempt for the second trial frequency
    res_1           = one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, 
                                  gyre_out_file, dir_nmlst, dic_star, eta_rot_1, sigma, dic_freq_0)

    if res_1 is False:
      repeat        = True 
    elif res_1[0] < N_obs_freq:
      repeat        = True 
      N_1           = res_1[0]
      N_deviation_1 = N_1 - N_obs_freq
      list_eta_rot.append(eta_rot_1)
      list_N_dev.append(N_deviation_1)
    else:
      repeat        = False
      N_1           = res_1[0]
      N_deviation_1 = N_1 - N_obs_freq
      list_eta_rot.append(eta_rot_1)
      list_N_dev.append(N_deviation_1)

    if repeat:
      res_1         = False
      while res_1 is False:
        if dbg: print 'tar: two_initial_eta_attempts: Repeat ...'
        eta_rot_1   = eta_rot_1 * stretch_high
        res_1       = one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, 
                                  gyre_out_file, dir_nmlst, dic_star, eta_rot_1, sigma, dic_freq_0)

        N_1           = res_1[0]
        N_deviation_1 = N_1 - N_obs_freq
        list_eta_rot.append(eta_rot_1)
        list_N_dev.append(N_deviation_1)

        # N_dev from second attempt should necessarily change sign, otherwise, we repeat by stretching eta_rot
        if N_deviation_1 < 0: res_1  = False

    if res_1 is False:
      logging.error('tar: two_initial_eta_attempts: Second call to one_attempt() failed')
      raise SystemExit, 'Error: tar: two_initial_eta_attempts: Second call to one_attempt() failed'
    else:
      last_dic      = res_1[1]
      logging.info('tar: two_initial_eta_attempts: Second call to one_attempt() succeeded')
    
    slope           = Secant_eta_rot_slope(eta_rot_a=eta_rot_0, eta_rot_b=eta_rot_1, 
                                           N_dev_a=N_deviation_0, N_dev_b=N_deviation_1)

    logging.info('tar: two_initial_eta_attempts: Success!')

    # return ([N_deviation_0, N_deviation_1], slope, dic_freq_0, dic_freq_1)
    return (list_eta_rot, list_N_dev, slope, last_dic)

#=========================================================================================
def one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file, 
                dir_nmlst, dic_star, eta_rot, sigma, previous_result, func_freq_crit=get_freq_crit):
    """
    Make a single attempt to call GYRE, and compute the frequencies with the desired GYRE 
    version/executable, and the input namelist file, specified by its path.
    @param which_gyre: The specific gyre ad nor nad and the version to call GYRE. You can 
           supply the full path to the GYRE executable, e.g. /home/me/gyre/bin/gyre_ad
           This is actually the recommended approach.
    @type which_gyre: string 
    @param dic_model: the input data as a dictionary, which is read directly from the gyre_in file.
           This is done by e.g. calling read.read_gyre_in(filename). 
    @type dic_model: dictionary
    @param dir_nmlst: full path to a directory where the namelists would be dumped. 
    @type dir_nmlst: string
    @param default_nmlst: The GYRE namelist as read, and returned by 
          jag.read_sample_namelist_to_list_of_strings(namelist_file). Each line in the inlist 
          is a string in this list
    @type default_nmlst: list of strings
    @param gyre_in_file: full path to the input GYRE file that contains the whole structure
          info, and is used to compute frequencies. 
    @type gyre_in_file: string
    @param gyre_out_file: the full path filename where the intermediate-step GYRE output files will 
          be stored. Note that it is created upon the first call if it does not exist. For the next 
          attempts, it will be overwritten.
    @type gyre_out_file: string
    @param dir_nmlst: the directory where the intermediate-step GYRE inlist files will be stored.
          Note that it is created upon the first call if it does not exist. For the next attempts,
          it will be overwritten.
    @type dir_nmlst: string 
    @param dic_star: dictionary containing information of the star; this is normally returned 
          from mesa_gyre.stars module
    @type dic_star: dict
    @param eta_rot: The trial rotation rate w.r.t. the breakup rotation rate; thus, 
          0 <= eta_rot <= 1. 
    @type eta_rot: float
    @param sigma: multiplication factor for the frequency errors of the first and last mode, 
          around which the model frequencies are enumerated. One should make sure not to exceed
          the frequency difference between two consecutive modes, by allowing very large sigma.
          For KIC 7760680, sigma=9 would be appropriate, taking care of frequency difference between
          f_35 and f_36 in Papics et al. (2015, ApJL) Table 1.
    @type sigma: float
    @param previous_result: If the resulting summary output of the previous step is provided, 
          that will be used to get the lowest frequency, and fix the freq_min in the trial inlist
    @type previous_result: dictionary
    @param func_freq_crit: Specify which function the one_attempt() routine should use to get the 
          break up frequency? Several available functions are already on top of this module.
    @type func_freq_crit: function
    @return: number of model frequencies within the observed range (N_model_freq), and the full 
          list of resulting frequencies (dic_freq)
    @rtype: tuple
    """

    # Step 01) Find the two observed extreme frequency values that specify the observed range
    list_dic_freq  = dic_star['list_dic_freq']
    list_freq      = np.array([dic['freq'] for dic in list_dic_freq])
    list_freq_err  = np.array([dic['freq_err'] for dic in list_dic_freq])
    ind_sort       = np.argsort(list_freq)
    list_freq      = list_freq[ind_sort]
    list_freq_err  = list_freq_err[ind_sort]
    min_obs_freq   = list_freq[0]  - sigma * list_freq_err[0]
    max_obs_freq   = list_freq[-1] + sigma * list_freq_err[-1]

    # Step 02) Check if the range of frequency scan sufficiently covers very high-order g-modes
    freq_min       = get_inlist_freq_min(lowest_obs_freq=min_obs_freq, 
                                         em=None,
                                         list_nmlst=default_nmlst, 
                                         previous_result=previous_result)
    freq_max       = get_inlist_freq_max(highest_obs_freq=max_obs_freq, 
                                         em=None,
                                         list_nmlst=default_nmlst, 
                                         previous_result=previous_result)


    # Step 03) Substitute the gyre_in_file to default_nmlst to make an attempt
    #          Set the freq_rot and omega_rot
    #          Substitute a temporary file for the output for the first attempt
    # freq_rot_crit = get_freq_crit(M_star=dic_model['M_star'], R_star=dic_model['R_star'], to_cgs=False)
    freq_rot_crit = func_freq_crit(M_star=dic_model['M_star'], R_star=dic_model['R_star'], to_cgs=False)
    freq_rot      = eta_rot * freq_rot_crit
    omega_rot     = 2.0 * np.pi * freq_rot
    nmlst_step_01 = modify_nmlst( default_nmlst, list_dic_change=[
                    {'file':'"{0}"'.format(gyre_in_file)},
                    {'summary_file':'"{0}"'.format(gyre_out_file)}, 
                    {'Omega_uni':omega_rot},
                    {'freq_min':freq_min},
                    {'freq_max':freq_max} 
                    ] ) 

    # Step 04) Flush the nmlst from the last steps into an ASCII file for the attempt
    ind_slash     = gyre_in_file.rfind('/') + 1
    ind_point     = gyre_in_file.rfind('.')
    nmlst_name    = gyre_in_file[ind_slash : ind_point] 
    path_nmlst    = gen_numbered_filename(dir_nmlst, nmlst_name, None, 'nmlst')
    write_nmlst_to_ascii(nmlst_step_01, path_nmlst)

    # Step 05) Call GYRE, and store the frequencies in the file specified by gyre_out_file
    success = call_gyre(which_gyre, path_nmlst)
    if not success:
        logging.error('tar: one_attempt: call_gyre failed: {0}'.format(gyre_in_file))
        return False
        # raise SystemExit, 'Error: tar: one_attempt: call_gyre did not succeed.'

    # Step 06) Add the rotation rate, eta_rot, to the file attribute
    if not os.path.exists(gyre_out_file):
      logging.error('tar: one_attempt: {0} disappeared before adding attrs'.format(gyre_out_file))
      return False

    attrs     = [ {'eta_rot':eta_rot, 'dtype':np.float64}, 
                  {'omega_rot':omega_rot, 'dtype':np.float64} 
                ]
    attr_stat = add_extra_attributes_to_gyre_out_file(gyre_out_file, list_dic_attrs=attrs)
    if attr_stat is False:
      logging.error('tar: one_attempt: {0} disappeared after adding attrs'.format(gyre_out_file))
      return False

    # Step 07) Enumerate the resulting number of model frequencies within observed range
    try:
      dic_freq      = read.read_multiple_gyre_files([gyre_out_file])[0]
    except ValueError:
      logging.error('tar one_attempt: failed to read: {0}'.format(gyre_out_file))
      raise SystemExit, 'Error: tar one_attempt: failed to read: {0}'.format(gyre_out_file)

    dic_freq['omega_crit']  = 2.0 * np.pi * freq_rot_crit
    dic_freq['eta_rot']     = eta_rot
    dic_freq['freq_rot']    = freq_rot
    freq_mod      = np.real(dic_freq['freq']) * Hz_to_cd
    # n_pg          = dic_freq['n_pg']

    ind           = np.where( (freq_mod >= min_obs_freq) & (freq_mod <= max_obs_freq) )[0]
    N_model_freq  = len(ind)

    print '   one_attempt: eta_rot={0:0.4f}: n_freq={1}\n'.format(eta_rot, N_model_freq)

    return (N_model_freq, dic_freq)

#=========================================================================================
def call_gyre(which_gyre, path_nmlst):
    """
    This routine is copied from inversion.tools.py.
    This funtion creates an appropriate bash command to call and execute GYRE. Then, it waits until
    the run is over, and returns True if the execution succeeds. Otherwise, it returns False.
    @param which_gyre: full path to the GYRE executable. This should be decently provided by the calling
            script. It can be either gyre_ad or gyre_nad.
            I advice to provide the full path to the specific gyre executable that also specified the GYRE verison.
            E.g. which_gyre = '/home/user/gyre4.0/bin/gyre_nad'
            An additional space will be automatically added aftre which_gyre
    @type which_gyre: string
    @param path_nmlst: full path to where the input namelist for this specific job is stored, and GYRE will
            use the specified settings therein
    @return: True if succeeds, or False otherwise
    @rtype: boolean
    """
    if not os.path.exists(path_nmlst):
        logging.error('tar: call_gyre: {0} does not exist'.format(path_nmlst))
        raise SystemExit, 'Error: tar: call_gyre: {0} does not exist'.format(path_nmlst)
        return False

    if not isinstance(which_gyre, str) or not isinstance(path_nmlst, str):
        logging.error('tar: call_gyre: which_gyre and path_nmlst should be string')
        raise SystemExit, 'Error: tar: call_gyre: which_gyre and path_nmlst should be string'
        return False

    cmnd = which_gyre + ' ' + path_nmlst

    exe = subprocess.Popen(cmnd, stdout=subprocess.PIPE, shell=True)
    output, error = exe.communicate()
    status = exe.wait()

    if error is not None: 
      logging.error('tar: call_gyre: returned error: {0}'.format(error))
      raise SystemExit, 'Error: tar: call_gyre: returned error: {0}'.format(error)
      return False

    return status == 0

#=========================================================================================
def scan_eta_rot(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file, dir_nmlst,
                dic_star, sigma, eta_rot_from, eta_rot_to, eta_rot_num, func_freq_crit=get_freq_crit):
  """
  For only one input model, can one_attempt() iteratively, with trial rotation rates eta_rot 
  discretized between eta_rot_from and eta_rot_to in eta_rot_num number of steps, collect back
  the result as a list of tuple. Each item in the list (i.e. each tuple) contains the number 
  of modes between the observed frequency range (passed from inside dic_star), and the resulting 
  frequency list for later plotting. This is useful to show the evolution of the frequencies
  and/or periods as a function of trial 
  @return: if one_attempt() crashes, this function returns False just to capture the error
  """
  eta_rot_arr = np.linspace(eta_rot_from, eta_rot_to, eta_rot_num, endpoint=True)
  list_tup    = []

  last_dic  = None
  for eta_rot in eta_rot_arr:
    tup_res   = one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file, dir_nmlst,
                dic_star, eta_rot, sigma, last_dic, func_freq_crit)
    if tup_res is False: 
      logging.error('tar: scan_eta_rot: one_attempt failed')
      return False
    last_dic  = tup_res[1]
    list_tup.append(tup_res)

  return list_tup

#=========================================================================================
def get_inlist_freq_min(lowest_obs_freq, em, list_nmlst, previous_result):
  """
  GYRE works better when the frequency scan is performed in the co-rotating frame. However, 
  trial rotation rates (e.g. eta_rot ~ 25%), the longest computed period becomes shorter than 
  the longest observed mode, and falls inside the observed range. Therefore, the number of 
  computed modes within the observed range will be totally wrong, and limited by our improper
  handling of the lower frequency scanning range. To this end, we try to check and adapt 
  the "freq_min" parameter in the GYRE inlist (passed by the result from the former attempt)in 
  "previous_result".
  @param lowest_obs_freq: The lowest observed frequency in the list. We make sure that the 
        period of the longest calculated mode in the co-rotating frame is ~ 25% longer than 
        the longest observed period (= 1/lowest_obs_freq). 
  @type lowest_obs_freq: float
  @param em: mode azimuthal order
  @type em: integer
  @param list_nmlst: The GYRE namelist as read, and returned by 
        jag.read_sample_namelist_to_list_of_strings(namelist_file). Each line in the inlist 
        is a string in this list
  @type list_nmlst: list of strings
  @param previous_result: If the resulting summary output of the previous step is provided, 
        that will be used to get the lowest frequency, and fix the freq_min in the trial inlist
  @type previous_result: dictionary
  @return: the minimum frequency in the co-rotating frame for computing the frequencies.
        NOTE: We assume here that in the GYRE namelist, the "freq_frame = 'COROT_O' or 
        'COROT_I'". Otherwise, the return value from this routine would be wrong to use.
  @rtype: float
  """
  if em is None:
    logging.error('get_inlist_freq_min: em=None. Fix the em value in the calling function, not here')
    raise SystemExit

  ind_freq_min = [i for i in range(len(list_nmlst)) if 'freq_min' in list_nmlst[i]][0]
  freq_min     = float( list_nmlst[ind_freq_min].split()[2] ) # default namelist in co-rotating frame

  # In the first attempt, previous_result is still unkonwn, hence None
  if previous_result is None: return freq_min

  freq_rot     = previous_result['freq_rot'] * Hz_to_cd

  obs_freq_corot = from_inertial_to_co_rotating_frame(freq_rot, em, lowest_obs_freq)
  # to avoid negative frequencies
  if obs_freq_corot <= 0:
    # logging.error('Error: tar: get_inlist_freq_min: obs_freq_corot < 0')
    # return None
    freq_min   = em * freq_rot / 100.0
    return freq_min

  prev_freq    = np.real(previous_result['freq']) * Hz_to_cd # in inertial frame
  freq_inert   = prev_freq
  freq_corot   = from_inertial_to_co_rotating_frame(freq_rot, em, freq_inert) # in co-rotating frame
  # if not all(freq_corot >= 0):
  #   logging.error('Error: tar: get_inlist_freq_min: at least one of "freq_corot" <= 0')
  #   return None
  prev_per     = 1.0 / freq_corot
  max_per      = np.max(prev_per)

  hi_obs_per   = 1.0 / obs_freq_corot
  threshold    = 1.50 * hi_obs_per
  extend_scan  = max_per <= threshold

  new_per      = max_per # 1./freq_min
  if extend_scan:
    # lower the lower scan frequency in GYRE namelist
    new_per    = threshold
    freq_min   = 1.0 / new_per  # in co-rotating frame

  # print '   Upper? {0}, max_per={1:.2f}, hi_obs_per={2:.2f}, threshold={3:.2f}, new_per={4:.2f}'.format(
  #        extend_scan, max_per, hi_obs_per, threshold, new_per)

  return freq_min

#=========================================================================================
def get_inlist_freq_max(highest_obs_freq, em, list_nmlst, previous_result):
  """
  GYRE works better when the frequency scan is performed in the co-rotating frame. However, 
  trial rotation rates (e.g. eta_rot ~ 20%), the shortest computed period becomes too smaller 
  than the shortest observed mode, and falls way outside the observed range. Therefore, one 
  can over-compute modes, extensively adding to the computation overhead. To this end, we try 
  to check and adapt the "freq_max" parameter in the GYRE inlist (passed by the result from 
  the former attempt)in "previous_result".
  @param highest_obs_freq: The highest observed frequency in the list. We make sure that the 
        period of the shortest calculated mode in the co-rotating frame is ~ 5% shorter than 
        the shortest observed period (= 1/highest_obs_freq). 
  @type highest_obs_freq: float
  @param em: mode azimuthal order
  @type em: integer
  @param list_nmlst: The GYRE namelist as read, and returned by 
        jag.read_sample_namelist_to_list_of_strings(namelist_file). Each line in the inlist 
        is a string in this list
  @type list_nmlst: list of strings
  @param previous_result: If the resulting summary output of the previous step is provided, 
        that will be used to get the lowest frequency, and fix the freq_min in the trial inlist
  @type previous_result: dictionary
  @return: the minimum frequency in the co-rotating frame for computing the frequencies.
        NOTE: We assume here that in the GYRE namelist, the "freq_frame = 'COROT_O' or 
        'COROT_I'". Otherwise, the return value from this routine would be wrong to use.
  @rtype: float
  """
  if em is None:
    logging.error('get_inlist_freq_max: em=None. Fix the em value in the calling function, not here')
    raise SystemExit
    
  ind_freq_max = [i for i in range(len(list_nmlst)) if 'freq_max' in list_nmlst[i]][0]
  freq_max     = float( list_nmlst[ind_freq_max].split()[2] ) # default namelist in co-rotating frame

  # In the first attempt, previous_result is still unkonwn, hence None
  if previous_result is None: return freq_max

  freq_rot     = previous_result['freq_rot'] * Hz_to_cd

  obs_freq_corot = from_inertial_to_co_rotating_frame(freq_rot, em, highest_obs_freq)
  # to avoid negative frequencies
  if obs_freq_corot <= 0:
    # logging.error('Error: tar: get_inlist_freq_max: obs_freq_corot < 0')
    # return None
    freq_max   = freq_max + em * freq_rot / 100.0
    return freq_max

  prev_freq    = np.real(previous_result['freq']) * Hz_to_cd # in inertial frame
  freq_inert   = prev_freq
  freq_corot   = from_inertial_to_co_rotating_frame(freq_rot, em, freq_inert) # in co-rotating frame
  # if not all(freq_corot >= 0):
  #   logging.error('Error: tar: get_inlist_freq_max: at least one of "freq_corot" <= 0')
  #   return None
  prev_per     = 1.0 / freq_corot
  min_per      = np.min(prev_per)  # in the co-rotating frame

  lo_obs_per   = 1.0 / obs_freq_corot
  threshold    = 0.98 * lo_obs_per
  modify_scan  = min_per > threshold

  new_per      = min_per # 1./freq_max
  if modify_scan:
    # lower the lower scan frequency in GYRE namelist
    new_per    = threshold
    freq_max   = 1.0 / new_per  # in co-rotating frame

  # print '   Lower? {0}, min_per={1:.2f}, lo_obs_per={2:.2f}, threshold={3:.2f}, new_per={4:.2f}'.format(
  #        modify_scan, min_per, lo_obs_per, threshold, new_per)

  return freq_max

#=========================================================================================
def add_extra_attributes_to_gyre_out_file(gyre_out, list_dic_attrs):
  """
  The optimised rotation information should be stored along with the rest of the information
  that GYRE writes out by default. This is useful for reference to know later, what rotation
  rates gave the best match to the number of observed frequencies, and for later plottings.
  This routine adds any desired list of such information as file attributes to the originally
  existing GYRE output HDF5 file.
  @param gyre_out: full path to the GYRE output HDF5 file 
  @type gyre_out: string 
  @param list_dic_attrs: list of dictionaries that contain the attributes to be added. Each 
         attribute should be only passed as one key-value item in the dictionary. In other words,
         to pass N attributes, one should pass N dictionaries in this list.
  @type list_dic_attrs: list of dictionaries
  @return: True if after adding the attribute, the file is still present, and does not disappear;
          False, otherwise. 
  @rtype: boolean
  """
  if not os.path.exists(gyre_out):
    txt = 'Error: tar: {0}: {1} does not exist; hostname={2}'.format(
           'add_extra_attributes_to_gyre_out_file', gyre_out, commons.get_hostname())
    logging.error(txt)
    return False

  f = h5py.File(gyre_out, 'r+')
  for dic in list_dic_attrs:
    keys = dic.keys()

    if 'dtype' not in keys:
      logging.error('tar: add_extra_attributes_to_gyre_out_file: "dtype" for the attribute not provided')
      raise SystemExit, 'Error: tar: add_extra_attributes_to_gyre_out_file: "dtype" for the attribute not provided'
    if len(keys) != 2:
      logging.error('tar: add_extra_attributes_to_gyre_out_file: each attr dic should have two item()')
      raise SystemExit, 'Error: tar: add_extra_attributes_to_gyre_out_file: each attr dic should have two item()'

    # key = keys[0]
    # f.attrs[key] = dic[key]
    for key, value in dic.items():
      if key == 'dtype': 
        dtype = value
      else:
        name = key
        data = value

    f.attrs.create(name=name, data=data, shape=None, dtype=dtype)
  f.close()

  if not os.path.exists(gyre_out):
    logging.error('tar: add_extra_attributes_to_gyre_out_file: {0} disappeard after closing'.format(gyre_out))
    return False 
  else: 
    return True 

#=========================================================================================
def unit_test_add_extra_attributes_to_gyre_out_file():
  """
  Unit testing for the add_extra_attributes_to_gyre_out_file() function. We test if the new
  attribute is stored with the desired data type (dtype). 
  We create an empty test HDF5 file (if gyre_out not present), and add many arbitrary attributes
  of available numpy.dype to it; then, we check if the attribute is stored with the desired dtype.
  If all is well recovered, we return True, else, we return False (meaning that at least one issue
  occured).
  @return: test status 
  @rtype: bool
  """
  test_file = 'unit_test_delete.h5'
  if not os.path.exists(test_file):
    f = h5py.File(test_file, 'w')
    f.close()

  dic_01 = {'attr_bool':True, 'dtype':type(True)}
  dic_02 = {'attr_int8':-128, 'dtype':np.int8}
  dic_03 = {'attr_int16':-32768, 'dtype':np.int16}
  dic_04 = {'attr_int32':-1, 'dtype':np.int32}
  dic_05 = {'attr_int64':+1, 'dtype':np.int64}
  dic_06 = {'attr_float16':1.23, 'dtype':np.float16}
  dic_07 = {'attr_float32':3.45, 'dtype':np.float32}
  dic_08 = {'attr_float64':5.67, 'dtype':np.float64}
  dic_09 = {'attr_complex64':1.23+7.89j, 'dtype':np.complex64}
  dic_10 = {'attr_complex128':1.23+9.01j, 'dtype':np.complex128}

  tests  = [dic_01, dic_02, dic_03, dic_04, dic_05, dic_06, dic_07, dic_08, dic_09, dic_10]

  # add the attributes to the file
  status = add_extra_attributes_to_gyre_out_file(test_file, tests)
  if status is False:
    print 'Warning: unit_test_add_extra_attributes_to_gyre_out_file: add_extra_attributes_to_gyre_out_file failed'
    return status

  # assuming status=True, now, open the file, and get the attributes
  f = h5py.File(test_file, 'r')
  attrs  = f.attrs.items() 
  num_errs = 0
  for i_dic, dic in enumerate(tests):
    if attrs[i_dic][0] not in dic.keys():
      print 'Warning: unit_test_add_extra_attributes_to_gyre_out_file: {0} not found'.format(attrs[i_dic][0])
      num_errs += 1
    if dic['dtype'] != type(attrs[i_dic][1]):
      print 'Warning: unit_test_add_extra_attributes_to_gyre_out_file: dtype({0})={1}; expected: {2}'.format(
             attrs[i_dic][0], type(attrs[i_dic][1]), dic['dtype'])
      num_errs += 1

  if num_errs == 0:
    print 'Good News: unit_test_add_extra_attributes_to_gyre_out_file: All tests successfully passed.'
    return True
  else:
    print 'Bad News: unit_test_add_extra_attributes_to_gyre_out_file: {0} Errors/Warnings occured!'.format(num_errs)
    return False

#=========================================================================================
def move_attempt_file_to_gyre_out_dir(attempt_out, gyre_out):
  """
  This function simply moves the temporary attempt_out file to a new location given by 
  gyre_out.
  @param attempt_out: full path to the temporary filename from previous attempts to 
         optimize eta_rot
  @type attempt_out: string 
  @param gyre_out: full path to the permanent filename
  @type gyre_out: string 
  @return: None 
  @rtype: None
  """
  if not os.path.exists(attempt_out):
    logging.error('tar: move_attempt_file_to_gyre_out_dir: {0} does not exist!')
    raise SystemExit, 'Error: tar: move_attempt_file_to_gyre_out_dir: {0} does not exist!'

  try:
    shutil.move(attempt_out, gyre_out)
    # os.rename(attempt_out, gyre_out) # this call fails miserably in many cases! weird !!!
  except OSError:
    raise SystemExit, 'move_attempt_file_to_gyre_out_dir: mv {0} -> {1}'.format(attempt_out, gyre_out)

  if not os.path.exists(gyre_out):
    logging.error('tar: move_attempt_file_to_gyre_out_dir: {0} disappeard'.format(gyre_out))
    raise SystemExit, 'Error: tar: move_attempt_file_to_gyre_out_dir: {0} disappeard'.format(gyre_out)

  return None

#=========================================================================================
def gen_gyre_out_attempt_file(gyre_in, dir_attempt, use_slurm_jobid):
  """
  During the attempts for tuning the eta_rot, the intermediate-step outputs should be written
  out in a temporary directory. This ensures that if the job crashes for some reason, the 
  remaining file is not stored in the "gyre_out" sub-folders, and are not mistaken to be final
  files. Once the computations succeed to find the optimal eta_rot, then the attempt file 
  should be moved to the permanent "gyre_out" directory. The latter is achieved by a call 
  to move_attempt_file_to_gyre_out_dir().
  @param gyre_in: full path to the input GYRE filename.
  @type gyre_in: string 
  @param dir_attempt: The full path to the directory where the intermediate results will be 
         stored.
  @type dir_attempt: string
  @param use_slurm_jobid: if True, then the SLURM_JOBID number from a call to 
         commons.get_slurm_jobid() will be appended to the end of the dir_attempt, to make 
         the directory name unique. 
  @type use_slurm_jobid: boolean
  """
  if not isinstance(use_slurm_jobid, bool):
    logging.error('tar: gen_gyre_out_attempt_file: use_slurm_jobid should be True or False')
    raise SystemExit, 'Error: tar: gen_gyre_out_attempt_file: use_slurm_jobid should be True or False'

  if not os.path.exists(dir_attempt): os.makedirs(dir_attempt)
  if dir_attempt[-1] != '/': dir_attempt += '/'

  if use_slurm_jobid:
    job_id     = commons.get_slurm_jobid()
    job_id_dir = '{0:08d}/'.format(job_id)
  else:
    job_id_dir = ''
  attempt_file = gen_gyre_out_from_gyre_in(gyre_in)

  ind          = attempt_file.rfind('/')
  attempt_file = attempt_file[ind + 1 : ]

  job_dir      = dir_attempt + job_id_dir
  if not os.path.exists(job_dir): os.makedirs(job_dir)

  return job_dir + attempt_file

#=========================================================================================
def gen_dir_gyre_out_from_gyre_in(file_in):
  """
  Generate the GYRE output directory from the file pattern in the input file, by removing
  the trailing file name, and substituting the "gyre_in" phrase with "gyre_out", instead. 
  """
  ind_slash = file_in.rfind('/')
  dir_gyre_out = file_in[0 : ind_slash + 1]
  dir_gyre_out = dir_gyre_out.replace('gyre_in', 'gyre_out')
  if not os.path.exists(dir_gyre_out): os.makedirs(dir_gyre_out)

  return dir_gyre_out

#=========================================================================================
def gen_gyre_out_from_gyre_in(file_in, prefix='ad-sum-'):
  """
  Generate the GYRE output file from the GYRE input file pattern, by substituting the "gyre_in" 
  phrase with "gyre_out", and ".gyre" with ".h5". Additionally, all GYRE output files start with
  a prefix, e.g. prefix="ad-sum-" to highlight the fact that they are adiabatic summary files, or
  alternatively prefix="nad-sum-" for non-adiabatic summary files.
  """
  if '-sc' in file_in:
    ind_sc  = file_in.rfind('-sc')
    file_in = file_in[0 : ind_sc] + file_in[ind_sc + 7 : ]
  file_out  = file_in.replace('gyre_in', 'gyre_out')
  file_out  = file_out.replace('.gyre', '.h5')
  ind_slash = file_out.rfind('/')
  file_out  = file_out[0:ind_slash+1] + prefix + file_out[ind_slash+1:]

  return file_out

#=========================================================================================
def get_TradTable(path):
    """
    Specify the path to the tabulated eigenvalue HDF5 file, and load and return the table 
    @param path: full path to where the table file sits.
    @type path: string
    @return:
    @rtype:
    """
    if not os.path.exists(path):
        logging.error('tar: get_TradTable: {0} does not exist'.format(path))
        raise SystemExit, 'Error: tar: get_TradTable: {0} does not exist'.format(path)
    TradTable = tradtable.TradTable('trad_table.h5') # Change to the path of the HDF5 file
    print type(TradTable)

    return TradTable

#=========================================================================================



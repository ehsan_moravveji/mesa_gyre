
import os, glob, sys
import logging
import numpy as np 

###############################################################################
###############################################################################
def read_przybilla(filename):
  """
  Read the file przybilla.list into a numpy record array
  """
  if not os.path.exists(filename):
    logging.error('Error: read_przybilla: {0} does not exist'.format(filename))
    raise SystemExit, 'Error: read_przybilla: {0} does not exist'.format(filename)

  dtype = [('name', 'S8'), ('Teff', 'f8'), ('Teff_err', 'f8'), ('logg', 'f8'), ('logg_err', 'f8'), 
           ('vsini', 'f8'), ('vsini_err', 'f8'), ('Ys', 'f8'), ('Ys_err', 'f8'), ('eps_C', 'f8'),
           ('eps_C_err', 'f8'), ('eps_N', 'f8'), ('eps_N_err', 'f8'), ('eps_O', 'f8'), 
           ('eps_O_err', 'f8'), ('eps_CNO', 'f8'), ('eps_CNO_err', 'f8'), ('M', 'f8')]
  with open(filename, 'r') as r: lines = r.readlines()
  nlines= len(lines)
  # rec   = np.empty(nlines, dtype)
  data  = []
  for i, line in enumerate(lines):
    row = line.rstrip('\r\n').split()
    data.append(row)

  rec   = np.core.records.fromarrays(np.array(data).transpose(), dtype=dtype)

  return rec

###############################################################################


import glob, os
import logging
import numpy as np
from collections import OrderedDict
from copy import deepcopy
import multiprocessing

#from ivs.stellar_evolution.fileio import read_mesa
import gyre

import commons as cm
import param_tools as pt
import file_dir_stat as fds
import slurm_commons as slcm
from read import read_mesa
import period_spacing 


#=================================================================================================================
#=================================================================================================================
def choose_evol_and_seism_points(list_dic):
  """
  This function receives a list of dictionaries. Each dictionary corresponds to a point in the Kiel diagram within
  the error bax of the location of the star. This function looks into the value of the 'has_h5' key in each dictionary
  and only selects those with True. For more information, see the scan_grid.
  @param list_dic: Input list of dictionaries.
  @type list_dic: list of dictionaries
  @return: list of dictionaries with each dictionary corresponding to an evolutionary+seismic model in the gird. This
           returned list can be later used for frequency fitting etc.
  @rtype: list of dictionaries
  """
  
  n_dic = len(list_dic)
  n_evol_seism_pts = 0
  if n_dic == 0:
    message = 'Error: sample_grid: choose_evol_and_seism_points: Input list is empty!'
    raise SystemExit, message

  list_dic_evol_seism = []  
  for i_dic in range(n_dic):
    dic = list_dic[i_dic]
    has_h5 = dic['has_h5']
    if not has_h5: continue
    
    n_evol_seism_pts += 1
    list_dic_evol_seism.append(dic)
    
  return list_dic_evol_seism
    

#=================================================================================================================
def scan_grid_multiproc(obs_Teff, obs_Teff_e, obs_logg, obs_logg_e):
  """
  """
  
  print '\n - scan_grid_multiproc:'
  try:
    n_avail_cpu = multiprocessing.cpu_count()
    print '   Number of Available CPUs: %s' % (n_avail_cpu, )
    n_use_cpu = n_avail_cpu / 2
  except NotImplementedError:
    print '   multiprocessing.cpu_count() is not implemented!'
    n_use_cpu = 4
  
  list_hist_files = get_list_hist_files()
  n_hist = len(list_hist_files)
  print ' - Number of history files to read: %s' % (n_hist, )
  print '   From: %s' % (list_hist_files[0])
  print '   to:   %s' % (list_hist_files[n_hist-1])

  jobs = []
  
  pool = multiprocessing.Pool(processes=n_use_cpu)
  for i_hist in range(n_hist):
    proc = pool.apply_async(read_mesa_gyre_multiproc, args=(list_hist_files[i_hist], 
                                                            obs_Teff, obs_Teff_e, obs_logg, obs_logg_e))
    jobs.append(proc)
    result = proc.get()
    pt.update_progress(i_hist/float(n_hist))

  #for i_hist in range(n_hist):
    #which_hist = list_hist_files[i_hist]
    #proc = multiprocessing.Process(target=read_mesa_gyre_multiproc, args=(which_hist, obs_Teff, obs_Teff_e, obs_logg, obs_logg_e), 
                                   #name=which_hist)
    #proc.daemon = True
    #jobs.append(proc)
    #proc.start()
    #proc.join()

    #pt.update_progress(i_hist/float(n_hist))

  #print '\n - scan_grid_multiproc:'       
  #print ' - Total points scanned = %s' % (n_total_rows, )
  #print ' - Total points in box = %s, which is %s%% of all' % (n_in_box_rows, 100.*n_in_box_rows/n_total_rows)
  #print ' - Total points in box with available pulsation info files = %s \n' % (n_in_box_with_h5, )
  
  return jobs

#=================================================================================================================
def read_mesa_gyre_multiproc(hist_file, obs_Teff, obs_Teff_e, obs_logg, obs_logg_e):
  """ 
  """

  dic_param = cm.set_srch_param()
  Xc_fmt = dic_param['Xc_fmt']
  Yc_fmt = dic_param['Yc_fmt']
  evol_status = dic_param['evol']

  list_dic = []
  n_total_rows = 0
  n_in_box_rows = 0
  n_in_box_with_h5 = 0
  
  header, hist = read_mesa(filename = hist_file)
    
  hist_dtype_names = hist.dtype.names
  n_hist_names = len(hist_dtype_names)
    
  model_number = hist['model_number']
  nline = len(model_number)
   
  flag_Teff = 'Teff' in hist_dtype_names
  flag_log_Teff = 'log_Teff' in hist_dtype_names
  if flag_Teff and not flag_log_Teff: Teff = hist['Teff']
  elif flag_log_Teff and not flag_Teff: Teff = np.power(10., hist['log_Teff'])
  elif flag_Teff and flag_log_Teff: Teff = hist['Teff']
  else: raise SystemExit, 'Error: sample_grid: scan_grid: Neither Teff nor log_Teff exist in history file!'
    
  flag_g = 'gravity' in hist_dtype_names
  flag_logg = 'log_g' in hist_dtype_names
  if flag_g and not flag_logg: log_g = np.log10(hist['gravity'])
  elif flag_logg and not flag_g: log_g = hist['log_g']
  elif flag_g and flag_logg: log_g = hist['log_g']
  else: raise SystemExit, 'Error: sample_grid: scan: Neither gravity nor log_g exist in history file!'
  mxg = np.argmax(log_g)
  mxg_model_number = model_number[mxg]
    
  center_h1 = hist['center_h1']
  center_he4 = hist['center_he4']
    
  for i_row in range(mxg, nline):
    theo_Xc = center_h1[i_row]
    theo_Yc = center_he4[i_row]
    this_model_number = model_number[i_row]
    flag_after_ZAMS = (this_model_number >= mxg_model_number)
    if not flag_after_ZAMS: continue
    flag_until_TAMS = (theo_Xc > 1e-6)
    if not flag_until_TAMS: break
      
    n_total_rows += 1
    theo_Teff = Teff[i_row]    # Theoretical Teff from the grid
    theo_logg = log_g[i_row]   # Theoretical log_g from the gird
    flag_Teff_in_box = obs_Teff-obs_Teff_e <= theo_Teff <= obs_Teff+obs_Teff_e
    flag_logg_in_box = obs_logg-obs_logg_e <= theo_logg <= obs_logg+obs_logg_e
    flag_in_box = flag_Teff_in_box and flag_logg_in_box
      
    if flag_in_box:
      n_in_box_rows += 1
      # Remember to get the physical parameters like ov from the hist filename
      dic_phys_param = pt.get_param_from_single_hist_filename(hist_file)

      dic = OrderedDict({})    
      dic['filename'] = hist_file
      dic['initial_mass'] = dic_phys_param['m_ini']
      dic['eta'] = dic_phys_param['eta']
      dic['ov'] = dic_phys_param['ov']
      dic['sc'] = dic_phys_param['sc']
      dic['z'] = dic_phys_param['z']
      dic['Xc'] = theo_Xc
      dic['Yc'] = theo_Yc
      for i_key in range(n_hist_names):
        dic[hist_dtype_names[i_key]] = hist[i_row][i_key]
        
      str_Xc = Xc_fmt.format(theo_Xc)
      str_Yc = Yc_fmt.format(theo_Yc)
      hypotetical_h5 = pt.gen_hypothetic_h5_filename(hist_file, evol_status, str_Xc, str_Yc) 
      list_found_h5  = glob.glob(hypotetical_h5)        
      n_found_gyre   = len(list_found_h5)
      flag_h5_exists = n_found_gyre > 0
      if flag_h5_exists:
        dic['has_h5'] = True
      else:
        dic['has_h5'] = False
       
      if flag_h5_exists:
        n_in_box_with_h5 += 1
        if n_found_gyre == 1:
          which_h5 = list_found_h5[0]
          empty_dic, gyre_info = gyre.read_output(which_h5)
          dic['pulse_info'] = gyre_info
        else:
          list_gyre_info = []
          for i_gyre in range(n_found_gyre):
            which_h5 = list_found_h5[i_gyre]
            empty_dic, gyre_info = gyre.read_output(which_h5)
            list_gyre_info.append(gyre_info)
          tuple_gyre_info = tuple(list_gyre_info)  # Only tuples can be stacked
          pulse_info = np.hstack(tuple_gyre_info)
          dic['pulse_info'] = pulse_info
          #which_h5 = list_found_h5[0]
          #empty_dic, gyre_info = gyre.read_output(which_h5)
          #stacked_gyre_info = gyre_info
          #for i_gyre in range(1, n_found_gyre):
            #empty_dic, gyre_info = gyre.read_output(which_h5)
            ##stacked_gyre_info = np.vstack(stacked_gyre_info, gyre_info)
            #stacked_gyre_info = np.vstack(gyre_info)
          #dic['pulse_info'] = stacked_gyre_info
           
          dic_dP = period_spacing.gen_dP(pulse_info)
          n_gm = dic_dP['n_gm']
          gm_ng = dic_dP['gm_ng']
          gm_P = dic_dP['gm_P']
          gm_dP = dic_dP['gm_dP']
          gm_freq_im = dic_dP['gm_freq_im']
          dic['n_gm'] = n_gm
          dic['gm_ng'] = gm_ng
          dic['gm_P'] = gm_P
          dic['gm_dP'] = gm_dP
          dic['gm_freq_im'] = gm_freq_im

      dic_save = deepcopy(dic)
      list_dic.append(dic_save)

  return list_dic
  
  
#=================================================================================================================
def scan_grid(obs_Teff, obs_Teff_e, obs_logg, obs_logg_e):
  """
  """
  
  dic_param = cm.set_srch_param()
  Xc_fmt = dic_param['Xc_fmt']
  Yc_fmt = dic_param['Yc_fmt']
  
  list_hist_files = get_list_hist_files()
  n_hist = len(list_hist_files)
  print ' - Number of history files to read: %s' % (n_hist, )
  print '   From: %s' % (list_hist_files[0])
  print '   to:   %s' % (list_hist_files[n_hist-1])
  list_dic = []
  n_total_rows = 0
  n_in_box_rows = 0
  n_in_box_with_h5 = 0
  
  for i_hist in range(n_hist):
    which_hist = list_hist_files[i_hist]
    header, hist = read_mesa(filename = which_hist)
    
    hist_dtype_names = hist.dtype.names
    n_hist_names = len(hist_dtype_names)
    
    model_number = hist['model_number']
    nline = len(model_number)
   
    flag_Teff = 'Teff' in hist_dtype_names
    flag_log_Teff = 'log_Teff' in hist_dtype_names
    if flag_Teff and not flag_log_Teff: Teff = hist['Teff']
    elif flag_log_Teff and not flag_Teff: Teff = np.power(10., hist['log_Teff'])
    elif flag_Teff and flag_log_Teff: Teff = hist['Teff']
    else: raise SystemExit, 'Error: sample_grid: scan_grid: Neither Teff nor log_Teff exist in history file!'
    
    flag_g = 'gravity' in hist_dtype_names
    flag_logg = 'log_g' in hist_dtype_names
    if flag_g and not flag_logg: log_g = np.log10(hist['gravity'])
    elif flag_logg and not flag_g: log_g = hist['log_g']
    elif flag_g and flag_logg: log_g = hist['log_g']
    else: raise SystemExit, 'Error: sample_grid: scan: Neither gravity nor log_g exist in history file!'
    mxg = np.argmax(log_g)
    mxg_model_number = model_number[mxg]
    
    center_h1 = hist['center_h1']
    center_he4 = hist['center_he4']
    
    for i_row in range(mxg, nline):
      theo_Xc = center_h1[i_row]
      theo_Yc = center_he4[i_row]
      this_model_number = model_number[i_row]
      flag_after_ZAMS = (this_model_number >= mxg_model_number)
      if not flag_after_ZAMS: continue
      flag_until_TAMS = (theo_Xc > 1e-6)
      if not flag_until_TAMS: break
      
      n_total_rows += 1
      evol_status = 'MS'         # It must be between ZAMS and TAMS; hence, MS
      theo_Teff = Teff[i_row]    # Theoretical Teff from the grid
      theo_logg = log_g[i_row]   # Theoretical log_g from the gird
      flag_Teff_in_box = obs_Teff-obs_Teff_e <= theo_Teff <= obs_Teff+obs_Teff_e
      flag_logg_in_box = obs_logg-obs_logg_e <= theo_logg <= obs_logg+obs_logg_e
      flag_in_box = flag_Teff_in_box and flag_logg_in_box
      
      if flag_in_box:
        n_in_box_rows += 1
        # Remember to get the physical parameters like ov from the hist filename
        dic_phys_param = pt.get_param_from_single_hist_filename(which_hist)

        dic = OrderedDict({})    
        dic['filename'] = which_hist
        dic['initial_mass'] = dic_phys_param['m_ini']
        dic['eta'] = dic_phys_param['eta']
        dic['ov'] = dic_phys_param['ov']
        dic['sc'] = dic_phys_param['sc']
        dic['z'] = dic_phys_param['z']
        dic['Xc'] = theo_Xc
        dic['Yc'] = theo_Yc
        for i_key in range(n_hist_names):
          dic[hist_dtype_names[i_key]] = hist[i_row][i_key]
        
        str_Xc = Xc_fmt.format(theo_Xc)
        str_Yc = Yc_fmt.format(theo_Yc)
        hypotetical_h5 = pt.gen_hypothetic_h5_filename(which_hist, evol_status, str_Xc, str_Yc) 
        list_found_h5  = glob.glob(hypotetical_h5)        
        n_found_gyre   = len(list_found_h5)
        flag_h5_exists = n_found_gyre > 0
        if flag_h5_exists:
          dic['has_h5'] = True
        else:
          dic['has_h5'] = False
        
        if flag_h5_exists:
          n_in_box_with_h5 += 1
          if n_found_gyre == 1:
            which_h5 = list_found_h5[0]
            empty_dic, gyre_info = gyre.read_output(which_h5)
            dic['pulse_info'] = gyre_info
          else:
            list_gyre_info = []
            for i_gyre in range(n_found_gyre):
              which_h5 = list_found_h5[i_gyre]
              empty_dic, gyre_info = gyre.read_output(which_h5)
              list_gyre_info.append(gyre_info)
            tuple_gyre_info = tuple(list_gyre_info)  # Only tuples can be stacked
            pulse_info = np.hstack(tuple_gyre_info)
            dic['pulse_info'] = pulse_info
            #which_h5 = list_found_h5[0]
            #empty_dic, gyre_info = gyre.read_output(which_h5)
            #stacked_gyre_info = gyre_info
            #for i_gyre in range(1, n_found_gyre):
              #empty_dic, gyre_info = gyre.read_output(which_h5)
              ##stacked_gyre_info = np.vstack(stacked_gyre_info, gyre_info)
              #stacked_gyre_info = np.vstack(gyre_info)
            #dic['pulse_info'] = stacked_gyre_info
            
            dic_dP = period_spacing.gen_dP(pulse_info)
            n_gm = dic_dP['n_gm']
            gm_ng = dic_dP['gm_ng']
            gm_P = dic_dP['gm_P']
            gm_dP = dic_dP['gm_dP']
            gm_freq_im = dic_dP['gm_freq_im']
            dic['n_gm'] = n_gm
            dic['gm_ng'] = gm_ng
            dic['gm_P'] = gm_P
            dic['gm_dP'] = gm_dP
            dic['gm_freq_im'] = gm_freq_im

        dic_save = deepcopy(dic)
        list_dic.append(dic_save)

    pt.update_progress(i_hist/float(n_hist))
    
  print        
  print ' - Total points scanned = %s' % (n_total_rows, )
  print ' - Total points in box = %s, which is %s%% of all' % (n_in_box_rows, 100.*n_in_box_rows/n_total_rows)
  print ' - Total points in box with available pulsation info files = %s' % (n_in_box_with_h5, )
  print 
  
  return list_dic
    
 
#=================================================================================================================
def read_hist_grid(list_files):
  """
  This function receives a large list of strings to read. Each string gives a full path to a MESA history file. 
  The 
  """
  
  


#=================================================================================================================
def get_list_hist_files():
  """
  This function scans the location where the grid sits, and retrieves the list of hist files within the prescribed
  mass range and step in commons.set_srch_param.
  @return: list of strings, where each string gives a full path to a hist file which is alrady available in the grid.
  @rtype: list of strings
  """

  path_base = cm.set_paths()['path_base']  
  
  dic_param = pt.gen_track_srch_str()
  mass_from = dic_param['mass_from']
  mass_to = dic_param['mass_to']
  mass_step = dic_param['mass_step']
  mass_arr  = dic_param['mass_arr']
  n_mass = dic_param['n_mass']         # is also the number of directories to search in
  mass_fmt = dic_param['mass_fmt']
  if dic_param['use_hist_regex']:
    regex = dic_param['srch_hist_regex']
  else:
    regex = None

  list_dic_files = slcm.get_list_dic_files(path_base, mass_from, mass_to, mass_step, mass_fmt, srch_regex = regex)
  list_hist_files = []
  for which_dic in list_dic_files:
    which_hist_files = which_dic['hist_files'][:]
    for which_hist_full_path in which_hist_files: list_hist_files.append(which_hist_full_path)
  
  return list_hist_files
  
  
#=================================================================================================================




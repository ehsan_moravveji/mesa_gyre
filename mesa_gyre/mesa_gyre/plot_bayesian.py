
import sys, os, glob
import numpy as np 
import matplotlib
import pylab as plt

#=================================================================================================================
#=================================================================================================================
def prepare_for_marginalized_2d(recarr, xaxis, yaxis):
  """

  """
  names      = recarr.dtype.names
  n_row      = len(recarr)
  n_col      = len(names)
  if n_col != 3:
    logging.error('plot_bayesian: prepare_for_marginalized_2d: Input record array has {0} instead of 3 columns!'.format(n_col))
    raise SystemExit
  if xaxis not in names:
    logging.error('plot_bayesian: prepare_for_marginalized_2d: {0} not available in the input record array'.format(xaxis))
    raise SystemExit, 'Error: plot_bayesian: prepare_for_marginalized_2d: {0} not available in the input record array'.format(xaxis)
  if yaxis not in names:
    logging.error('plot_bayesian: prepare_for_marginalized_2d: {0} not available in the input record array'.format(yaxis))
    raise SystemExit, 'Error: plot_bayesian: prepare_for_marginalized_2d: {0} not available in the input record array'.format(yaxis)

  set_x      = set(recarr[xaxis])
  set_y      = set(recarr[yaxis])
  n_x        = len(set_x)
  n_y        = len(set_y)
  arr_x      = np.array(list(set_x))
  arr_y      = np.array(list(set_y))
  min_x      = np.min(arr_x)
  max_x      = np.max(arr_x)
  min_y      = np.min(arr_y)
  max_y      = np.max(arr_y)
  log_posterior = recarr['log_posterior']
  arr_x.sort()
  arr_y.sort()

  if n_x == 1:
    logging.error('plot_bayesian: prepare_for_marginalized_2d: The xaxis: "{0}" has only one value'.format(xaxis))
    return None
  if n_y == 1:
    logging.error('plot_bayesian: prepare_for_marginalized_2d: The yaxis: "{0}" has only one value'.format(yaxis))
    return None

  #--------------------------------
  # Prepare the mesh grid, and also
  # fill up the logarithm of posterior
  # in a 2D matrix
  #--------------------------------
  n_row      = n_y
  n_col      = n_x
  grid_x     = np.zeros((n_row, n_col))
  grid_y     = np.zeros((n_row, n_col))
  matrix     = np.zeros((n_row, n_col))
  for i_row in range(n_row): 
    grid_x[i_row, 0:n_col] = arr_x[0:n_x]
    grid_y[i_row, 0:n_col] = arr_y[i_row]

  for i_row, y_val in enumerate(arr_y):
    for i_col, x_val in enumerate(arr_x):
      ind_x = np.where(recarr[xaxis] == x_val, 1, 0)
      ind_y = np.where(recarr[yaxis] == y_val, 1, 0)
      ind_xy= ind_x * ind_y
      ind   = np.where(ind_xy == 1)[0]
      n_ind = len(ind)
      if n_ind == 0:
        matrix[i_row, i_col] = np.nan
      else:
        matrix[i_row, i_col] = log_posterior[ind]

  dic = {}
  dic['grid_x'] = grid_x
  dic['grid_y'] = grid_y
  dic['matrix'] = matrix

  return dic

#=================================================================================================================
def all_possible_2d(list_rec, file_out=None):
  """
  To plot the output from bayesian.get_all_possible_2d()
  """
  if file_out is None: return

  n_recarr = len(list_rec)
  if n_recarr == 3:
    n_rows = 1
    n_cols = 3
  elif n_recarr == 6:
    n_rows = 2
    n_cols = 3
  elif n_recarr == 10:
    n_rows = 5
    n_cols = 2
  else:
    logging.error('plot_bayesian: all_possible_2d: Num. recarr={0}. Only 3, 6 and 10 are supported'.format(n_recarr))
    raise SystemExit, 'Error: plot_bayesian: all_possible_2d: Num. recarr={0}. Only 3, 6 and 10 are supported'.format(n_recarr)

  figsize_x = n_cols * 3
  figsize_y = n_rows * 3

  print '\n - plot_bayesian: all_possible_2d: '
  print '   Number of input arrays = {0}'.format(n_recarr)
  print '   n_rows x n_cols = {0} x {1}'.format(n_rows, n_cols)
  print '   Canvas size = {0} x {1} inch'.format(figsize_x, figsize_y)

  #--------------------------------
  # Prepare the figure
  #--------------------------------
  from matplotlib.colors import BoundaryNorm
  from matplotlib.ticker import MaxNLocator
  cmap = plt.get_cmap('binary')

  fig, arr_axes = plt.subplots(n_rows, n_cols, figsize=(figsize_x, figsize_y), dpi=300)

  #--------------------------------
  # Unpack the data, and place them
  # appropriately in relevant panels
  #--------------------------------
  for i_panel in range(n_recarr):
    i_row  = i_panel / n_cols
    i_col  = i_panel - i_row * n_cols 
    if n_rows == 1: ax = arr_axes[i_panel]
    if n_rows > 1:  ax = arr_axes[i_row][i_col]

    recarr = list_rec[i_panel]
    names  = recarr.dtype.names
    xaxis  = names[0]
    yaxis  = names[1]
    dic    = prepare_for_marginalized_2d(recarr, xaxis, yaxis)
    grid_x = dic['grid_x']
    grid_y = dic['grid_y']
    matrix = dic['matrix']

    # if 'Xc' in [xaxis, yaxis]:
    #   print '   Warning: plot_bayesian: all_possible_2d: Skipping any panel with Xc'
    #   continue

    set_x  = set(grid_x[0,:])
    set_y  = set(grid_y[:,0])
    n_x    = len(set_x)
    n_y    = len(set_y)
    min_x  = min(set_x)
    max_x  = max(set_x)
    min_y  = min(set_y)
    max_y  = max(set_y)

    ind         = ~np.isnan(matrix)
    upper_level = matrix[ind].max()
    lower_level = upper_level - 5
    nbins       = min([n_x * n_y, MaxNLocator.MAXTICKS-1])
    levels      = MaxNLocator(nbins=nbins).tick_values(lower_level, upper_level)
    norm        = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

    im = ax.pcolormesh(grid_x, grid_y, matrix, cmap=cmap, norm=norm, 
                       shading='gouraud', edgecolors='face', zorder=1)

    ConfInterv = range(-1, -6, -1)
    ct = ax.contour(grid_x, grid_y, matrix, levels=ConfInterv, linestyles='dotted',
                    colors='grey', origin='lower', linewidths=2, zorder=2)
    ax.clabel(ct, levels=ConfInterv, colors='red', inline=1, fontsize=10, fmt='%d')

    if i_panel == n_recarr-1: # colorbar only for the last panel
      plt.colorbar(im)

    ax.set_xlim(min_x, max_x)
    ax.set_ylim(min_y, max_y)
    ax.set_xlabel(get_axis_label(xaxis))
    ax.set_ylabel(get_axis_label(yaxis))

  plt.tight_layout()
  plt.savefig(file_out)
  print '   saved: {0} \n'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def get_axis_label(param):
  """
  Return an appropriate name for the axis based on the name of the input parameter, passed by "param".
  @param param: The name of the x- or y-axis based on the grid parameters. It can be either of the following:
         M, Z, ov, Xc, logD, Xini.
  @type param: string
  """
  allowed = ['M', 'eta', 'ov', 'Z', 'Xc', 'logD', 'Xini']
  if param not in allowed:
    logging.error('plot_bayesian: get_axis_label: {0} is unavailable'.format(param))
    raise SystemExit, 'Error: plot_bayesian: get_axis_label: {0} is unavailable'.format(param)

  if param == 'M':
    label = r'Mass [M$_\odot$]'
  elif param == 'eta':
    label = r'$\eta \,[\%]$'
  elif param == 'ov':
    label = r'$f_{\rm ov}$'
  elif param == 'Z':
    label = r'$Z$'
  elif param == 'logD':
    label = r'$\log D_{\rm mix}$'
  elif param == 'Xini':
    label = r'$X_{\rm ini}$'
  elif param == 'Xc':
    label = r'$X_{\rm c}$'
  else:
    print 'Warning: plot_bayesian: get_axis_label: Perhaps, "{0}" does not match the current design'.format(param)
    label = None

  return label 

#=================================================================================================================
def marginalized_2d(rec_bayes, xaxis='ov', yaxis='logD', file_out=None):
  """
  Contour plot of the marginalized log_posterior values with abscissa specified by xaxis, and the ordinate specified 
  by yaxis.
  """
  if file_out is None: return

  dic_plot = prepare_for_marginalized_2d(rec_bayes, xaxis, yaxis)
  grid_x   = dic_plot['grid_x']
  grid_y   = dic_plot['grid_y']
  matrix   = dic_plot['matrix']

  set_x  = set(grid_x[0,:])
  set_y  = set(grid_y[:,0])
  n_x    = len(set_x)
  n_y    = len(set_y)
  min_x  = min(set_x)
  max_x  = max(set_x)
  min_y  = min(set_y)
  max_y  = max(set_y)

  #--------------------------------
  # Prepare the figure
  #--------------------------------
  fix, ax = plt.subplots(1, figsize=(6,4))
  # plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97)

  from matplotlib.colors import BoundaryNorm
  from matplotlib.ticker import MaxNLocator
  ind = ~np.isnan(matrix)
  upper_level = matrix[ind].max()
  lower_level = upper_level - 5
  nbins = min([n_x * n_y, MaxNLocator.MAXTICKS-1])
  levels = MaxNLocator(nbins=nbins).tick_values(lower_level, upper_level)

  cmap = plt.get_cmap('binary') #'gnuplot2'
  norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

  #--------------------------------
  # Make the Contour plot
  #--------------------------------
  im = ax.pcolormesh(grid_x, grid_y, matrix, cmap=cmap, norm=norm, 
                     shading='gouraud', edgecolors='face', zorder=1)
  plt.colorbar(im)

  # ConfInterv = range(-1, -6, -1)
  ConfInterv = [-1, -2, -3]
  ct = ax.contour(grid_x, grid_y, matrix, levels=ConfInterv, linestyles='dotted',
                  colors='grey', origin='lower', linewidths=2, zorder=2)
  ax.clabel(ct, levels=ConfInterv, colors='red', inline=1, fontsize=10, fmt='%d')
  wt = ax.contour(grid_x, grid_y, matrix, levels=ConfInterv, linestyles='dotted',
                  colors='white', origin='lower', linewidths=2, zorder=1)

  ax.set_xlim(min_x, max_x)
  ax.set_ylim(min_y, max_y)
  ax.set_xlabel(get_axis_label(xaxis))
  ax.set_ylabel(get_axis_label(yaxis))

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  plt.tight_layout()
  plt.savefig(file_out)
  print ' - plot_bayesian: marginalized_2d: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def marginalized_1d(rec_bayes, ylog=True, file_out=None):
  """
  Plot 1D marginalized posterior. 
  """
  if file_out is None: return

  names      = rec_bayes.dtype.names
  n_row      = len(rec_bayes)
  n_col      = len(names)
  if n_col != 2:
    logging.error('plot_bayesian: marginalized_1d: input record array has {0} instead of 2 columns!'.format(n_col))
    raise SystemExit

  x_key      = names[0]
  xvals      = rec_bayes[x_key]
  if ylog:
    yvals    = rec_bayes['log_posterior'][:]
  else:
    yvals    = np.exp(rec_bayes['log_posterior'])
  ind        = np.argsort(xvals)
  xvals      = xvals[ind]
  yvals      = yvals[ind]

  #--------------------------------
  # Define the plot
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(6,4))
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97)

  ax.plot(xvals, yvals, linestyle='solied', lw=2, color='grey', zorder=1)
  ax.scatter(xvals, yvals, marker='o', s=36, color='black', zorder=2)

  if ylog: 
    ytitle = r'Log Posterior $\,\,\log_{10} p(M|D)$'
  else:
    ytitle = r'Posterior $p(M|D)$'
  ax.set_ylabel(ytitle)
  ax.set_xlabel(r'{0}'.format(x_key))


  plt.savefig(file_out)
  print ' - plot_bayesian: marginalized_1d: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def final_posterior(dic_bayes, file_out=None):
  """
  Plot the posterior as a function of models. 
  To avoid very zigzag pattern, and to show how the PDF is smoothly distributed from 0 to 1, we sort the values.
  Thus, there is no one-to-one correspondance between the model orders plotted here, and the models found during 
  the globbing and/or file reading.
  The x-axis is just an arbitrary ordering.
  @param dic_bayes: full information from the Bayesian inference. This is passed e.g. from mesa_gyre.bayesian.do_bayesian()
         It can have the following keys:
         - prior_matrix: prior distribution for each iteration
         - posterior_matrix: posterior distribution for each iteration
         - list_dic_h5: list of dictionaries containing the data from GYRE outputs
  @type dic_bayes: dictionary
  @return: None
  @rtype: None
  """
  if file_out is None: return

  #--------------------------------
  # Define the plot
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(6,4))
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97)

  posterior_matrix = dic_bayes['posterior_matrix']
  n_iter, n_model = posterior_matrix.shape

  # Color map
  cmap = plt.get_cmap('brg')
  norm = matplotlib.colors.Normalize(vmin=0, vmax=n_iter)
  color = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)

  for i, posterior in enumerate(posterior_matrix):
    ax.plot(range(n_model), np.log10(posterior), color=color.to_rgba(i), lw=1.5, label=r'$p(D|M)_{0}$'.format(i))

  leg = plt.legend(loc=0, fontsize='x-small')
  ax.set_ylabel(r'Posterior PDF (sorted)')
  ax.set_xlim(-10, n_model+10)
  # ax.set_ylim(-0.05, 1.05)

  plt.savefig(file_out)
  print ' - plot_bayesian: final_posterior: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def histogram_log_posterior(dic_bayes, file_out=None):
  """
  The logarithm of posterior lies in a range of very negative values up to one. The histogram shows how many models 
  are close to one, regardless of those which are at very negative values.
  @param dic_bayes: full information from the Bayesian inference. This is passed e.g. from mesa_gyre.bayesian.do_bayesian()
         It can have the following keys:
         - prior_matrix: prior distribution for each iteration
         - posterior_matrix: posterior distribution for each iteration
         - list_dic_h5: list of dictionaries containing the data from GYRE outputs
  @type dic_bayes: dictionary
  """
  if file_out is None: return

  log_posterior = dic_bayes['log_posterior']

  #--------------------------------
  # Prepare the plot
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(6,4))
  n, bins, patches = ax.hist(log_posterior, bins=100, range=(-20, 0), normed=True, weights=None, cumulative=False, bottom=None, 
                             histtype=u'stepfilled', align=u'mid', orientation=u'vertical', rwidth=None, log=False, color='grey', 
                             label=None, stacked=False)
  ax.set_xlim(-20.5, 0.5)

  plt.savefig(file_out)
  print ' - plot_bayesian: histogram_log_posterior: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================

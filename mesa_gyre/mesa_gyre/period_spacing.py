
import sys, os, glob
import numpy as np
# from collections import OrderedDict
import time
from multiprocessing import Pool, cpu_count
import commons, read
# from scipy.signal import argrelmin, argrelmax #, argrelextrema

Msun = 1.9892e33
Rsun = 6.9598e10
Lsun = 3.8418e33
Tsun = 5777.
logg_sun = np.log10(27400.)

#=================================================================================================================
def check_n_pg_monotonicity(list_dic):
  """
  For a good collection of reasons, the GYRE output may not be monotonic, which means some modes being mis-ordered,
  or skipped. This is unfavorable situation, and such cases are dangerous in the grid to treat. So, they have to be
  captured, if present. Indeed, this case becomes more challenging when dealing with high-order mixed modes!
  This routine checkes n_pg of all available degree el for each input dictionary, and captures if any order is missing.
  Note: We currently only check g-modes, because high-order (radial and non-radial) p-modes are quite prone to such jumps.
  @param list_dic: each dictionary containts the GYRE output data. it is returned, e.g. by read.read_multiple_gyre_files()
  @type list_dic: list of dictionaries
  @return: list of dictionaries with the following keys:
     - filename: giving the filname of problematic files
     - n_g1: number of lost g-modes for ell=1
     - n_g2: number of lost g-modes for ell=2
  @rtype: list of dictionaries
  """
  n_dic = len(list_dic)
  if n_dic == 0:
    raise SystemExit, 'Error: period_spacing: check_n_pg_monotonicity: Input list is empty'

  list_bads = []

  for i_dic, dic in enumerate(list_dic):
    all_ell   = dic['l']
    set_ell   = set(all_ell)
    avail_ell = sorted(list(set_ell))
    has_el_0  = 0 in avail_ell
    has_el_1  = 1 in avail_ell
    has_el_2  = 2 in avail_ell
    has_el_3  = 3 in avail_ell
    ind_el_1  = np.where((all_ell == 1))[0]
    ind_el_2  = np.where((all_ell == 2))[0]

    all_n_pg  = dic['n_pg']

    # get rid of p-modes
    if has_el_1:
      n_pg_el_1 = all_n_pg[ind_el_1]
      n_pg_el_1 = n_pg_el_1[n_pg_el_1 < 0]
      n_g1      = len(n_pg_el_1)
      n_g1_expected = np.abs(np.min(n_pg_el_1)) - np.abs(np.max(n_pg_el_1)) + 1
      g1_ok     = n_g1 == n_g1_expected

    if has_el_2:
      n_pg_el_2 = all_n_pg[ind_el_2]
      n_pg_el_2 = n_pg_el_2[n_pg_el_2 < 0]
      n_g2      = len(n_pg_el_2)
      n_g2_expected = np.abs(np.min(n_pg_el_2)) - np.abs(np.max(n_pg_el_2)) + 1
      g2_ok     = n_g2 == n_g2_expected

    if g1_ok and g2_ok: continue

    dic_bad = {}
    dic_bad['filename'] = dic['filename']

    if not g1_ok:
      dic_bad['n_g1'] = n_g1_expected - n_g1
    if not g2_ok:
      dic_bad['n_g2'] = n_g2_expected - n_g2

    list_bads.append(dic_bad)

  return list_bads

#=================================================================================================================
def multiproc_update_dP_dip(list_dic, n_cpu=None, el=1, by_freq=False, by_P=False, freq_from=0, freq_to=10, P_from=0, P_to=3):
  """
  Use "multiprocessing" to update the input list of dictionaries with the quantified features in the period spacing.
  The argument list is identical to update_dP_dip().
  """
  n_dic = len(list_dic)
  if n_dic == 0:
    logging.error('period_spacing: multiproc_update_dP_dip: Input list is empty')
    raise SystemExit, 'Error: period_spacing: multiproc_update_dP_dip: Input list is empty'
  
  if n_cpu is None: n_cpu = cpu_count()
  pool = Pool(processes=n_cpu)
  
  n_chops = n_dic / n_cpu + 1
  
  t_start = time.time()
  
  result = []
  for i_cpu in range(n_cpu):
    n_from = i_cpu * n_chops
    n_to   = (i_cpu + 1) * n_chops
    if i_cpu == n_cpu - 1: n_to = n_dic
    dic_chop = list_dic[n_from : n_to]
    result.append(pool.apply_async( update_dP_dip, args=(dic_chop, el, by_freq, by_P, 
							  freq_from, freq_to, P_from, P_to) ))
  pool.close()
  pool.join()
  
  list_output = []
  for i_cpu in range(n_cpu): list_output += result[i_cpu].get()
  
  dt_sec = int(time.time() - t_start)
  
  print ' - period_spacing: multiproc_update_dP_dip:'
  print '   N_cpu={0}, N_dic={1}, Time={2} [sec] \n'.format(n_cpu, n_dic, dt_sec)
  
  return list_output
  
#=================================================================================================================
def parallel_update_dP_dip(list_dic, n_cpu=None, el=1, by_freq=False, by_P=False, freq_from=0, freq_to=10, P_from=0, P_to=3):
  """
  Use "pp" to update the input list of dictionaries with the quantified features in the period spacing.
  The argument list is identical to update_dP_dip().
  """
  try:
    import pp
  except:
    print 'Error: period_spacing: parallel_update_dP_dip: failed to import pp.'
    print '          Consult "http://www.parallelpython.com"'
    raise SystemExit

  job_server = commons.get_pp_job_server()
  if isinstance(n_cpu, int):
    job_server.set_ncpus(n_cpu)

  # Submit the jobs in parallel, and receive the results
  print 'Y1'
  start_time  = time.time()
  list_result = [ job_server.submit(update_single_dP_dip, (a_dic, el, by_freq, by_P, freq_from, freq_to, P_from, P_to), 
				    (), ('mesa_gyre.commons', 'numpy', 'scipy.signal')) for a_dic in list_dic ]
  end_time    = time.time()
  print 'Y2'
  #list_output = [job() for job in list_result if job() is not None]
  list_output = []
  for i_job, job in enumerate(list_result):
    if isinstance(job(), dict): 
      list_output.append(job())
  print 'Y3'
  
  return list_output

#=================================================================================================================
def multiproc_gen_list_dic_dP(list_dic, n_cpu=None, freq_unit='Hz'):
  """
  Use "multiprocessing" to generate a list of period spacing dictionaries by calling gen_dP(). This function is 
  identical to gen_list_dic_dP().
  @param list_dic: the list of dictionaries, where each record array is associated to one .h5 file.
  @type list_dic: list of dictionaries
  @param n_cpu: boolean, or integer; default=None. If it is None, then pp.Server() is initialized without specifiying
       the number of CPUs. Otherwise, the number of CPUs will be passed.
  @type n_cpu: boolean or integer.
  @param freq_unit: this specifies the frequency unit of the GYRE output. It must be specified to make the values
     in the right unit. It is passed later to gen_dP as an argument
  @type freq_unit: string
  @return: list of dictionaries, one dictionary per input record array. For the valid key-value pairs returned by
     each dictionary, see the help from gen_dP
  @rtype: list of dictionaries
  """
  n_dic = len(list_dic)
  if n_dic == 0:
    logging.error('period_spacing: multiproc_gen_list_dic_dP: Input list is empty')
    raise SystemExit, 'Error: period_spacing: multiproc_gen_list_dic_dP: Input list is empty'

  if n_cpu is None: n_cpu = cpu_count()
  pool = Pool(processes=n_cpu)

  n_chops = n_dic / n_cpu + 1
  
  t_start = time.time()
  
  result = []
  for i_cpu in range(n_cpu):
    n_from = i_cpu * n_chops 
    n_to   = (i_cpu + 1) * n_chops
    if i_cpu == n_cpu - 1: n_to = n_dic
    dic_chop = list_dic[n_from : n_to]
    result.append(pool.apply_async( gen_list_dic_dP, args=(dic_chop, freq_unit) ))
  pool.close()
  pool.join()
    
  list_output = []
  for i_cpu in range(n_cpu): list_output += result[i_cpu].get()
  
  dt_sec = time.time() - t_start
  print ' - period_spacing: multiproc_gen_list_dic_dP:'
  print '   N_cpu={0}, N_dic={1}, Time={2} [sec] \n'.format(n_cpu, n_dic, int(dt_sec))
  
  return list_output

#=================================================================================================================
def parallel_gen_list_dic_dP(list_dic, n_cpu=None, freq_unit='Hz'):
  """
  Use "pp" to generate a list of period spacing dictionaries by calling gen_dP(). This function is identical to
  gen_list_dic_dP().
  @param list_dic: the list of dictionaries, where each record array is associated to one .h5 file.
  @type list_dic: list of dictionaries
  @param n_cpu: boolean, or integer; default=None. If it is None, then pp.Server() is initialized without specifiying
       the number of CPUs. Otherwise, the number of CPUs will be passed.
  @type n_cpu: boolean or integer.
  @param freq_unit: this specifies the frequency unit of the GYRE output. It must be specified to make the values
     in the right unit. It is passed later to gen_dP as an argument
  @type freq_unit: string
  @return: list of dictionaries, one dictionary per input record array. For the valid key-value pairs returned by
     each dictionary, see the help from gen_dP
  @rtype: list of dictionaries
  """
  try:
    import pp
  except:
    print 'Error: period_spacing: parallel_gen_list_dic_dP: failed to import pp.'
    print '          Consult "http://www.parallelpython.com"'
    raise SystemExit

  job_server = commons.get_pp_job_server()
  if isinstance(n_cpu, int):
    job_server.set_ncpus(n_cpu)

  # Submit the jobs in parallel, and receive the results
  start_time = time.time()
  list_output = []
  for i_dic, a_dic in enumerate(list_dic):
    job = job_server.submit( gen_dP, (a_dic, freq_unit), (), ('numpy','scipy.signal') )
    if job() is not None: list_output.append(job())
    
  print 'outside loop'

  dT_sec = time.time() - start_time
  dT_min = dT_sec / 60.
  dT_hr = dT_min / 60.
  if dT_sec < 60:
    dT = dT_sec
    unit = 'sec'
  elif dT_sec > 60 and dT_min < 60:
    dT = dT_min
    unit = 'min'
  else:
    dT = dT_hr
    unit = 'hr'

  print ' - period_spacing: parallel_gen_list_dic_dP: Elapsed time = {0:0.2f} [{1}]'.format(dT, unit)
  if job_server.print_stats():
    print ' - period_spacing: parallel_gen_list_dic_dP: Job Server Status: {0}'.format(job_server.print_stats())

  return list_output

#=================================================================================================================
def gen_list_dic_dP(list_dic, freq_unit='Hz'):
  """
  This function receives a list of dictionaries retured from e.g. read.read_multiple_gyre_files, and returns a
  dictionary of period spacing information per each of the input record arrays by calling gen_dP.
  @param list_dic: the list of dictionaries, where each record array is associated to one .h5 file.
  @type list_dic: list of dictionaries
  @param freq_unit: this specifies the frequency unit of the GYRE output. It must be specified to make the values
     in the right unit. It is passed later to gen_dP as an argument
  @type freq_unit: string
  @return: list of dictionaries, one dictionary per input record array. For the valid key-value pairs returned by
     each dictionary, see the help from gen_dP
  @rtype: list of dictionaries
  """
  n_rec = len(list_dic)
  if n_rec == 0:
    message = 'Error: period_spacing: gen_list_dic_dP: input list is empty!'
    raise SystemExit, message

  list_dic_dP = []
  for i_dic, dic in enumerate(list_dic):
    dic_dP = gen_dP(dic, freq_unit=freq_unit)

    # update dic with period spacing information, and return the updated dictionary
    for key, value in dic_dP.items(): dic[key] = value
    list_dic_dP.append(dic)

  return list_dic_dP

#=================================================================================================================
def gen_dP(dic, freq_unit='Hz'):
  """
  This function receives a dictionary consisting of GYRE short summary output
  data, and produces an ordered g-mode periods, period spacing [sec] and g-mode
  order (n_g) as a dictionary.
  @param dic: dictionary consisting of GYRE short summary output
  @type dic: dictionary
  @param freq_unit: This is set in the GYRE &output namelist with variable name: "freq_units"
        By default it is in Hz, but if it is None or uHz, it must be specified.
        If the frequency unit is not in Hz, then it will be converted to Hz, and the delta P will be reported back
        in seconds.
  @type freq_unit: string
  @return: dictionary of period spacing information:
         n_gm: number of detected g-modes, gm_ng: g-mode order in descending order,
         gm_freq_im: imaginary part of the frequency (<0 means excited mode),
         gm_P: g-mode period [sec], and gm_dp: g-mode period spacing [sec]
  @rtype: dictionary
  """
  from mesa_gyre.commons import conversions
  from mesa_gyre.read import check_key_exists
  import numpy
  import scipy.signal

  names = dic.keys()
  dic_names = {}
  for key in names:
    dic_names[key] = 0.0
  requested_keys = ['l', 'n_p', 'n_g', 'n_pg', 'freq', 'omega', 'E', 'E_norm', 'W', 'M_star', 'R_star', 'L_star']
  dic_keys = check_key_exists(dic_names, requested_keys)
  flag_l     = dic_keys['l']
  flag_np    = dic_keys['n_p']
  flag_ng    = dic_keys['n_g']
  flag_npg   = dic_keys['n_pg']
  flag_freq  = dic_keys['freq']
  flag_E     = dic_keys['E']
  flag_E_norm= dic_keys['E_norm']
  flag_W     = dic_keys['W']
  flag_M_star= dic_keys['M_star']
  flag_R_star= dic_keys['R_star']
  flag_L_star= dic_keys['L_star']

  if not flag_l or not flag_npg or not flag_freq:
    message = 'Error: period_spacing: gen_dP: Essenstial fields (l, n_pg, freq) are missing!'
    raise SystemExit, message

  if freq_unit != 'Hz':
    dic_convert = conversions()
    uHz_to_Hz = dic_convert['uHz_to_Hz']
    cd_to_Hz  = dic_convert['cd_to_Hz']
    if freq_unit == 'None':
      message = 'Error: period_spacing: gen_dP: this freq_unit not supported yet.'
      raise SystemExit, message
    if freq_unit == 'uHz':
      freq_re_all = dic['freq'].real * uHz_to_Hz
      freq_im_all = dic['freq'].imag * uHz_to_Hz
    if freq_unit == 'per_day' or freq_unit == 'cd':
      freq_re_all = dic['freq'].real * cd_to_Hz
      freq_im_all = dic['freq'].imag * cd_to_Hz
  elif freq_unit == 'Hz':
    freq_re_all = dic['freq'].real
    freq_im_all = dic['freq'].imag
  else:
    message = 'Error: period_spacing: gen_dP: Sth wrong with freq_unit: %s' % (freq_unit)
    raise SystemExit, message

  # dic_dP = OrderedDict({})
  dic_dP = {}
  for key, val in dic.items(): dic_dP[key] = val

  el_all = dic['l']
  el_0_indx = numpy.where((el_all == 0))[0]
  n_el_0 = len(el_0_indx)
  el_1_indx = numpy.where((el_all == 1))[0]
  n_el_1 = len(el_1_indx)
  el_2_indx = numpy.where((el_all == 2))[0]
  n_el_2 = len(el_2_indx)
  el_3_indx = numpy.where((el_all == 3))[0]
  n_el_3 = len(el_3_indx)
  el_high   = numpy.where((el_all > 3))[0]
  n_el_high = len(el_high)
  has_el_0 = False; has_el_1 = False; has_el_2 = False; has_el_3 = False; has_el_high = False
  num_el_avail = 0
  el_avail = []

  if n_el_0 > 0:
    has_el_0 = True
    num_el_avail += 1
    el_avail.append(0)
  if n_el_1 > 0:
    has_el_1 = True
    num_el_avail += 1
    el_avail.append(1)
  if n_el_2 > 0:
    has_el_2 = True
    num_el_avail += 1
    el_avail.append(2)
  if n_el_3 > 0:
    has_el_3 = True
    num_el_avail += 1
    el_avail.append(3)
  if n_el_high > 0:
    has_el_high = True
    num_el_avail += 10
    el_avail.append([k for k in range(4, max(el_all)+1)])
    num_el_avail += len(range(4, max(el_all)+1))

  el_non_radial = el_avail[:]
  if has_el_0: el_non_radial.remove(0)
  dic_dP['el_avail'] = el_non_radial

  np_all  = dic['n_p']
  ng_all  = dic['n_g']
  npg_all = dic['n_pg']
  per_all = 1.0/freq_re_all              # sec
  if flag_W:  W_all = dic['W']
  if flag_E: E_all  = dic['E']
  if flag_E_norm: E_norm_all = dic['E_norm']

  for which in range(num_el_avail):
    which_el = el_avail[which]
    which_el_indx = numpy.where((el_all == which_el))[0]  # fetch only for one available el
    n_which_el = len(which_el_indx)
    if n_which_el < 1: continue

    which_el_freq_re = freq_re_all[which_el_indx]
    which_el_freq_im = freq_im_all[which_el_indx]
    which_el_np   = np_all[which_el_indx]
    which_el_ng   = ng_all[which_el_indx]
    which_el_npg  = npg_all[which_el_indx]
    which_el_per  = per_all[which_el_indx]
    if flag_W: which_el_W  = W_all[which_el_indx]
    else: which_el_W = numpy.zeros(n_which_el) - 99.0
    if flag_E: which_el_E  = E_all[which_el_indx]
    else: which_el_E = numpy.zeros(n_which_el) - 99.0
    if flag_E_norm: which_el_E_norm  = E_norm_all[which_el_indx]
    else: which_el_E_norm = numpy.zeros(n_which_el) - 99.0

    which_gm_indx = numpy.where((which_el_npg < 0))[0] # for this availale el, fetch only the relevant modes
    n_which_gm = len(which_gm_indx)
    if n_which_gm < 2: continue
    which_gm_freq_re = which_el_freq_re[which_gm_indx]
    which_gm_freq_im = which_el_freq_im[which_gm_indx]
    which_gm_ng   = which_el_ng[which_gm_indx]
    which_gm_np   = which_el_np[which_gm_indx]
    which_gm_npg  = which_el_npg[which_gm_indx]
    which_gm_per  = which_el_per[which_gm_indx]
    which_gm_W    = which_el_W[which_gm_indx]
    which_gm_E    = which_el_E[which_gm_indx]
    which_gm_E_norm = which_el_E_norm[which_gm_indx]

    gm_sort_indx  = numpy.argsort(which_gm_npg)[::-1] # sort in decreasing order
    which_gm_freq_re[:] = which_gm_freq_re[gm_sort_indx]
    which_gm_freq_im[:] = which_gm_freq_im[gm_sort_indx]
    which_gm_ng[:]      = which_gm_ng[gm_sort_indx]
    which_gm_np[:]      = which_gm_np[gm_sort_indx]
    which_gm_npg[:]     = which_gm_npg[gm_sort_indx]
    which_gm_per[:]     = which_gm_per[gm_sort_indx]
    which_gm_W[:]       = which_gm_W[gm_sort_indx]
    which_gm_E[:]       = which_gm_E[gm_sort_indx]
    which_gm_E_norm[:]  = which_gm_E_norm[gm_sort_indx]

    which_gm_stbl_indx = numpy.where((which_gm_W < 0))[0]
    which_gm_unst_indx = numpy.where((which_gm_W >= 0))[0]

    which_gm_dP_list = []     # initially defined as list, and will be converted to numpy.darray later
    for j in range(n_which_gm-1):
      which_gm_dP_list.append( which_gm_per[j+1] - which_gm_per[j] )
    which_gm_dP = numpy.asarray(which_gm_dP_list)
    which_gm_dP_min  = min(which_gm_dP)
    which_gm_dP_mean = numpy.mean(which_gm_dP)
    which_gm_dP_med  = numpy.median(which_gm_dP)
    which_gm_dP_max  = max(which_gm_dP)
    which_gm_dP_std  = numpy.std(which_gm_dP)

    ind_peak        = scipy.signal.argrelmax(which_gm_dP)[0]
    flag_peak       = len(ind_peak) > 0
    if flag_peak:
      which_gm_dip_P  = which_gm_per[ind_peak]
      which_gm_peak_P = which_gm_per[ind_peak]
      which_gm_peak_dP= which_gm_dP[ind_peak]
      which_gm_peak_dP_mean = numpy.mean(which_gm_peak_dP)

    ind_dip         = scipy.signal.argrelmin(which_gm_dP)[0]
    flag_dip        = len(ind_dip) > 0
    if flag_dip:
      which_gm_dip_P  = which_gm_per[ind_dip]
      which_gm_dip_dP = which_gm_dP[ind_dip]
      ind_below_mean  = [i for i in range(len(ind_dip)) if which_gm_dip_dP[i] < which_gm_peak_dP_mean]
      which_gm_dip_P  = which_gm_dip_P[ind_below_mean]
      which_gm_dip_dP = which_gm_dip_dP[ind_below_mean]
      which_gm_dip_depth = which_gm_peak_dP_mean - which_gm_dip_dP
      which_gm_dip_depth_mean = numpy.mean(which_gm_dip_depth)

    which_el_str = str(which_el)
    key_n_which_el = 'n_el_' + which_el_str
    dic_dP[key_n_which_el] = n_which_el
    key_which_gm_el = 'el_' + which_el_str
    dic_dP[key_which_gm_el] = el_all[which_el_indx]
    key_n_which_gm = key_n_which_el + '_gm'
    dic_dP[key_n_which_gm] = n_which_gm

    key_which_gm_freq_re = 'el_' + which_el_str + '_gm_freq_re'
    dic_dP[key_which_gm_freq_re] = which_gm_freq_re
    key_which_gm_freq_im = 'el_' + which_el_str + '_gm_freq_im'
    dic_dP[key_which_gm_freq_im] = which_gm_freq_im

    key_which_gm_np = 'el_' + which_el_str + '_gm_np'
    dic_dP[key_which_gm_np] = which_gm_np
    key_which_gm_ng = 'el_' + which_el_str + '_gm_ng'
    dic_dP[key_which_gm_ng] = which_gm_ng
    key_which_gm_npg = 'el_' + which_el_str + '_gm_npg'
    dic_dP[key_which_gm_npg] = which_gm_npg

    key_which_gm_per = 'el_' + which_el_str + '_gm_P'
    dic_dP[key_which_gm_per] = which_gm_per
    key_which_gm_dP = 'el_' + which_el_str + '_gm_dP'
    dic_dP[key_which_gm_dP] = which_gm_dP
    key_which_gm_dP_min = 'el_' + which_el_str + '_gm_dP_min'
    dic_dP[key_which_gm_dP_min] = which_gm_dP_min
    key_which_gm_dP_mean = 'el_' + which_el_str + '_gm_dP_mean'
    dic_dP[key_which_gm_dP_mean] = which_gm_dP_mean
    key_which_gm_dP_med = 'el_' + which_el_str + '_gm_dP_med'
    dic_dP[key_which_gm_dP_med] = which_gm_dP_med
    key_which_gm_dP_max = 'el_' + which_el_str + '_gm_dP_max'
    dic_dP[key_which_gm_dP_max] = which_gm_dP_max
    key_which_gm_dP_std = 'el_' + which_el_str + '_gm_dP_std'
    dic_dP[key_which_gm_dP_std] = which_gm_dP_std

    if flag_peak:
      key_which_gm_peak_P         = 'el_' + which_el_str + '_gm_peak_P'
      dic_dP[key_which_gm_peak_P] = which_gm_peak_P
      key_which_gm_peak_dP        = 'el_' + which_el_str + '_gm_peak_dP'
      dic_dP[key_which_gm_peak_dP]= which_gm_peak_dP
      key_which_gm_peak_mean      = 'el_' + which_el_str + '_gm_peak_dP_mean'
      dic_dP[key_which_gm_peak_mean] = which_gm_peak_dP_mean

      ind_peak        = scipy.signal.argrelmax(which_gm_dP)
      which_gm_peak_P = which_gm_per[ind_peak]
      which_gm_peak_dP= which_gm_dP[ind_peak]
      which_gm_peak_dP_mean = numpy.mean(which_gm_peak_dP)

    if flag_dip:
      key_which_dip_P  = 'el_' + which_el_str + '_gm_dip_P'
      dic_dP[key_which_dip_P] = which_gm_dip_P
      key_which_dip_dP = 'el_' + which_el_str + '_gm_dip_dP'
      dic_dP[key_which_dip_dP] = which_gm_dip_dP
      key_which_dip_depth         = 'el_' + which_el_str + '_gm_dip_depth'
      dic_dP[key_which_dip_depth] = which_gm_dip_depth
      key_which_gm_dip_depth_mean = 'el_' + which_el_str + '_gm_dip_depth_mean'
      dic_dP[key_which_gm_dip_depth_mean] = which_gm_dip_depth_mean

    key_which_gm_E = 'el_' + which_el_str + '_gm_E'
    dic_dP[key_which_gm_E] = which_gm_E
    key_which_gm_E_norm = 'el_' + which_el_str + '_gm_E_norm'
    dic_dP[key_which_gm_E_norm] = which_gm_E_norm

  #n_old = n_gm
  #fixed_dic = exclude_repeat(dic_dP)
  #dic_dP = fixed_dic
  #n_new = dic_dP['n_gm']
  #if n_old != n_new:
    #print 'gen_dP: n_gm reduced from ', n_old, ' to ', n_new

  return dic_dP


#=================================================================================================================
def exclude_repeat(dic):
  """
  Despite all cares, the GYRE output might be contaminated by some wrong modes, which destroy the
  order of calculated modes in period and n_g.
  This function goes through the list of g-modes by their n_g, and excludes those lines that haev
  a repeating n_g
  """

  return dic

  n = dic['n_gm']
  n_old = n
  ng = dic['gm_ng']
  per = dic['gm_P']
  freq_im = dic['gm_freq_im']
  dP = dic['gm_dP']

  n_skip = 0
  n_cases = 0
  skip_indices = []
  repeated_vals = []

  for i in range(n):
    this_ng = ng[i]
    ind_repeat = [ind for ind in range(n) if ng[ind] == this_ng]

    #ind_repeat = np.where((ng - this_ng == 0))
    len_ind = len(ind_repeat)
    if len_ind >1:
      n_cases += 1
      n_skip += len_ind
      ind_bad = ind_repeat[0]-1
      skip_indices.append(ind_bad)
      repeated_vals.append(this_ng)
      repeated_dP = dP[ind_repeat]
      print 'delta P: ', dP[ind_repeat[0]-1 : ind_repeat[len_ind-1]]

  if n_cases > 0:
    print 'Warning: exclude_repeat detected ', n_cases/2, ' repeated g-modes!'
    print '         Each case is detected twice.'
    print '         Repeated n_g are: ', repeated_vals

  # Now, reconstruct the arrays excluding the sick element(s)
  n_new = n_old - n_cases/2
  new_ng = np.zeros(n_new)
  new_per = np.zeros(n_new)
  new_dP  = np.zeros(n_new)
  new_freq_im = np.zeros(n_new)
  for i_case in range(n_cases/2):
    ind_bad = skip_indices[2*i_case]
    new_ng[0:ind_bad]   = ng[0:ind_bad]
    new_ng[ind_bad+1:]  = ng[ind_bad+1:]
    new_per[0:ind_bad]  = per[0:ind_bad]
    new_per[ind_bad+1:] = per[ind_bad+1:]
    new_dP[0:ind_bad]   = dP[0:ind_bad]
    new_dP[ind_bad+1:]  = dP[ind_bad+1:]
    new_freq_im[0:ind_bad]  = freq_im[0:ind_bad]
    new_freq_im[ind_bad+1:] = freq_im[ind_bad+1:]

  # convert the new lists to numpy array
  ng  = new_ng[:]
  per = new_per[:]
  dP  = new_dP[:]
  freq_im = new_freq_im[:]
  n = len(ng)

  # dic = OrderedDict({})
  dic = {}
  dic['n_gm'] = n
  dic['gm_ng'] = ng
  dic['gm_P'] = per
  dic['gm_dP'] = dP
  dic['gm_freq_im'] = freq_im

  return dic

#=================================================================================================================
def update_dP_dip(list_dic, el=1, by_freq=False, by_P=False, freq_from=0, freq_to=10, P_from=0, P_to=3):
  """
  Pass the list of dP dictionaries (returned e.g. by gen_list_dic_dP), and specify the range in period or frequency
  to trim off the modes, and update the input dictionaries with min, max, mean, med, std, dips, depts and peaks of
  the period spacing dP within the specified range. Checks are performed for n_pg being contiguous; otherwise, the
  model is skipped.
  @param list_dic: list of period spacing dictionaries returned by e.g. gen_list_dic_dP. Several keys associated with
      dP in each of these dictionaries will be updated to the specified range
  @type list_dic: list of dictionaries
  @param el: spherical mode degree; default = 1
  @type el: integer
  @param by_freq, by_P: trim out the modes either by frequency (in Hz) or period (in days).
  @type by_freq, by_P: boolean
  @param freq_from, freq_to, P_from, P_to: range in frequency/period
  @type freq_from, freq_to, P_from, P_to: float
  @return: updated list of period spacing dictionaries
  @rtype: list of dictionaries
  """
  from mesa_gyre.commons import conversions
  import numpy
  import scipy.signal

  dic_conv = conversions()
  sec_to_d = dic_conv['sec_to_d']
  d_to_sec = dic_conv['d_to_sec']

  n_dic = len(list_dic)
  if n_dic == 0:
    message = 'Error: write: modes_to_h5_table: The input list is empty'
    raise SystemExit, message
  if not any([by_freq, by_P]) or all([by_freq, by_P]):
    message = 'Error: period_spacing: update_dP_dip: You must choose either of by_freq OR by_P.'
    raise SystemExit, message

  # Reconstruct the dictionary keys of interest
  str_el = str(el)
  key_P           = 'el_' + str_el + '_gm_P'
  key_dP          = 'el_' + str_el + '_gm_dP'
  key_n_pg        = 'el_' + str_el + '_gm_npg'
  key_freq        = 'el_' + str_el + '_gm_freq_re'
  key_dP_min      = 'el_' + str_el + '_gm_dP_min'
  key_dP_mean     = 'el_' + str_el + '_gm_dP_mean'
  key_dP_med      = 'el_' + str_el + '_gm_dP_med'
  key_dP_max      = 'el_' + str_el + '_gm_dP_max'
  key_dP_std      = 'el_' + str_el + '_gm_dP_std'
  key_dP_peak_P   = 'el_' + str_el + '_gm_peak_P'
  key_dP_peak_dP  = 'el_' + str_el + '_gm_peak_dP'
  key_dP_peak_dP_mean = 'el_' + str_el + '_gm_peak_dP_mean'
  key_dP_dip_P    = 'el_' + str_el + '_gm_dip_P'
  key_dP_dip_dP   = 'el_' + str_el + '_gm_dip_dP'
  key_dP_dip_depth= 'el_' + str_el + '_gm_dip_depth'
  key_dP_dip_depth_mean = 'el_' + str_el + '_gm_dip_depth_mean'
  key_dP_dip_distance   = 'el_' + str_el + '_gm_dip_dist'

  # Iteratively, extract and correct the values for each of the keys
  list_update = []
  list_bad_P_edge = []
  for i_dic, dic in enumerate(list_dic):
    freq        = dic[key_freq]
    P           = dic[key_P]
    dP          = dic[key_dP]
    n_pg        = dic[key_n_pg]
    n_P         = len(P)

    # exclude those dictionaries that have missing modes, inherent in their n_pg not being contiguous
    max_n_pg    = numpy.max(n_pg)
    min_n_pg    = numpy.min(n_pg)
    len_n_pg    = len(n_pg)
    expect_n_pg = max_n_pg - min_n_pg + 1  # expected number of n_pg, if they are flawlessly contiguous
    if len_n_pg != expect_n_pg: continue

    if by_freq:
      ind = [i for i in range(n_P) if freq[i]>=freq_from and freq[i]<=freq_to]
    if by_P:
      ind = [i for i in range(n_P) if P[i]>=P_from*d_to_sec and P[i]<=P_to*d_to_sec]
    n_ind = len(ind)
    if n_ind == 0: continue

    n_P         = n_ind
    freq        = freq[ind]
    P           = P[ind]
    try:
      dP          = dP[ind]
    except:
      list_bad_P_edge.append(dic['filename'] + ' \n')
      continue

    # min, max, med, mean and std of dP within the specified range
    dic[key_dP_min] = numpy.min(dP)
    dic[key_dP_max] = numpy.max(dP)
    dic[key_dP_mean]= numpy.mean(dP)
    dic[key_dP_med] = numpy.median(dP)
    dic[key_dP_std] = numpy.std(dP)

    ind_peak        = scipy.signal.argrelmax(dP, mode='wrap')[0]
    if len(ind_peak)>0:
      dic[key_dP_peak_P] = P[ind_peak]
      dic[key_dP_peak_dP]= dP[ind_peak]
      dic[key_dP_peak_dP_mean] = numpy.mean(dic[key_dP_peak_dP])
    else:
      #filename = dic['filename'][dic['filename'].rfind('/')+1 : ]
      #print '   update_dP_dip: Skip using %s' % (filename, )
      continue

    ind_dip         = scipy.signal.argrelmin(dP, mode='wrap')[0]
    n_dips          = len(ind_dip)
    dic[key_dP_dip_P] = P[ind_dip]
    dic[key_dP_dip_dP]= dP[ind_dip]
    dic[key_dP_dip_depth] = dic[key_dP_peak_dP_mean] - dP[ind_dip]
    dic[key_dP_dip_depth_mean] = numpy.mean(dic[key_dP_dip_depth])

    if n_dips < 2:
      dic[key_dP_dip_distance] = 0
      #filename = dic['filename'][dic['filename'].rfind('/')+1 : ]
      #print '   update_dP_dip: dic[%s]=0 for %s' % (key_dP_dip_distance, filename)
    if n_dips == 2:
      dic[key_dP_dip_distance] = numpy.abs(dic[key_dP_dip_dP][0] - dic[key_dP_dip_dP][1])
    if n_dips > 2:
      list_dist = []
      for i in range(n_dips-1):
        list_dist.append(dic[key_dP_dip_dP][i] - dic[key_dP_dip_dP][i+1])
      dic[key_dP_dip_distance] = abs(sum(list_dist)/len(list_dist))

    list_update.append(dic)

  n_bad = len(list_bad_P_edge)
  if n_bad > 0:
    print ' - period_spacing: update_dP_dip: Detected {0} files with bad P edges'.format(n_bad)
    with open('bad-P-edge-gyre_out-files.txt', 'w') as w: w.writelines(list_bad_P_edge)
    print '   File list stored in {0}'.format('bad-P-edge-gyre_out-files.txt')

  return list_update

#=================================================================================================================
def update_single_dP_dip(a_dic, el=1, by_freq=False, by_P=False, freq_from=0, freq_to=10, P_from=0, P_to=3):
  """
  Pass a single dP dictionary (returned e.g. by gen_dP), and specify the range in period or frequency
  to trim off the modes, and update the input dictionary with min, max, mean, med, std, dips, depts and peaks of
  the period spacing dP within the specified range. Checks are performed for n_pg being contiguous; otherwise, the
  model is skipped.
  Note: In case anything goes wrong, this code returns None, so after using it, one would better exlcude the returned
	None values, and only keep those that are dictionaries
  @param a_dic: a single period spacing dictionary returned by e.g. gen_dP(). Several keys associated with
      dP in each of these dictionaries will be updated to the specified range
  @type a_dic: dictionary
  @param el: spherical mode degree; default = 1
  @type el: integer
  @param by_freq, by_P: trim out the modes either by frequency (in Hz) or period (in days).
  @type by_freq, by_P: boolean
  @param freq_from, freq_to, P_from, P_to: range in frequency/period
  @type freq_from, freq_to, P_from, P_to: float
  @return: updated list of period spacing dictionaries
  @rtype: list of dictionaries
  """
  import mesa_gyre.commons 
  import numpy
  import scipy.signal

  dic_conv = mesa_gyre.commons.conversions()
  sec_to_d = dic_conv['sec_to_d']
  d_to_sec = dic_conv['d_to_sec']

  if not any([by_freq, by_P]) or all([by_freq, by_P]):
    message = 'Error: period_spacing: update_single_dP_dip: You must choose either of by_freq OR by_P.'
    logging.error(message)
    raise SystemExit, message

  # Reconstruct the dictionary keys of interest
  str_el = str(el)
  key_P           = 'el_' + str_el + '_gm_P'
  key_dP          = 'el_' + str_el + '_gm_dP'
  key_n_pg        = 'el_' + str_el + '_gm_npg'
  key_freq        = 'el_' + str_el + '_gm_freq_re'
  key_dP_min      = 'el_' + str_el + '_gm_dP_min'
  key_dP_mean     = 'el_' + str_el + '_gm_dP_mean'
  key_dP_med      = 'el_' + str_el + '_gm_dP_med'
  key_dP_max      = 'el_' + str_el + '_gm_dP_max'
  key_dP_std      = 'el_' + str_el + '_gm_dP_std'
  key_dP_peak_P   = 'el_' + str_el + '_gm_peak_P'
  key_dP_peak_dP  = 'el_' + str_el + '_gm_peak_dP'
  key_dP_peak_dP_mean = 'el_' + str_el + '_gm_peak_dP_mean'
  key_dP_dip_P    = 'el_' + str_el + '_gm_dip_P'
  key_dP_dip_dP   = 'el_' + str_el + '_gm_dip_dP'
  key_dP_dip_depth= 'el_' + str_el + '_gm_dip_depth'
  key_dP_dip_depth_mean = 'el_' + str_el + '_gm_dip_depth_mean'
  key_dP_dip_distance   = 'el_' + str_el + '_gm_dip_dist'

  # Extract and correct the values for the keys
  freq        = a_dic[key_freq]
  P           = a_dic[key_P]
  dP          = a_dic[key_dP]
  n_pg        = a_dic[key_n_pg]
  n_P         = len(P)

  # exclude this dictionary if has missing modes, inherent in their n_pg not being contiguous
  max_n_pg    = numpy.max(n_pg)
  min_n_pg    = numpy.min(n_pg)
  len_n_pg    = len(n_pg)
  expect_n_pg = max_n_pg - min_n_pg + 1  # expected number of n_pg, if they are flawlessly contiguous
  if len_n_pg != expect_n_pg: return None

  if by_freq:
    ind = [i for i in range(n_P) if freq[i]>=freq_from and freq[i]<=freq_to]
  if by_P:
    ind = [i for i in range(n_P) if P[i]>=P_from*d_to_sec and P[i]<=P_to*d_to_sec]
  n_ind = len(ind)
  if n_ind == 0: return None

  n_P         = n_ind
  freq        = freq[ind]
  P           = P[ind]
  try:
    dP          = dP[ind]
  except:
    return None

  # min, max, med, mean and std of dP within the specified range
  a_dic[key_dP_min] = numpy.min(dP)
  a_dic[key_dP_max] = numpy.max(dP)
  a_dic[key_dP_mean]= numpy.mean(dP)
  a_dic[key_dP_med] = numpy.median(dP)
  a_dic[key_dP_std] = numpy.std(dP)

  ind_peak        = scipy.signal.argrelmax(dP, mode='wrap')[0]
  if len(ind_peak)>0:
    a_dic[key_dP_peak_P] = P[ind_peak]
    a_dic[key_dP_peak_dP]= dP[ind_peak]
    a_dic[key_dP_peak_dP_mean] = numpy.mean(a_dic[key_dP_peak_dP])
  else:
    filename = a_dic['filename'][a_dic['filename'].rfind('/')+1 : ]
    print '   update_single_dP_dip: Skip using %s' % (filename, )
    return None

  ind_dip         = scipy.signal.argrelmin(dP, mode='wrap')[0]
  n_dips          = len(ind_dip)
  a_dic[key_dP_dip_P] = P[ind_dip]
  a_dic[key_dP_dip_dP]= dP[ind_dip]
  a_dic[key_dP_dip_depth] = a_dic[key_dP_peak_dP_mean] - dP[ind_dip]
  a_dic[key_dP_dip_depth_mean] = numpy.mean(a_dic[key_dP_dip_depth])

  if n_dips < 2:
    a_dic[key_dP_dip_distance] = 0
    filename = a_dic['filename'][a_dic['filename'].rfind('/')+1 : ]
    print '   update_single_dP_dip: a_dic[%s]=0 for %s' % (key_dP_dip_distance, filename)
  if n_dips == 2:
    a_dic[key_dP_dip_distance] = numpy.abs(a_dic[key_dP_dip_dP][0] - a_dic[key_dP_dip_dP][1])
  if n_dips > 2:
    list_dist = []
    for i in range(n_dips-1):
      list_dist.append(a_dic[key_dP_dip_dP][i] - a_dic[key_dP_dip_dP][i+1])
    a_dic[key_dP_dip_distance] = abs(sum(list_dist)/len(list_dist))

  return a_dic

#=================================================================================================================
def dP_features_to_recarr(list_dic, el=1):
  """
  """
  n_dic = len(list_dic)
  if n_dic == 0:
    message = 'Error: period_spacing: dP_features_to_recarr: The input list is empty'
    raise SystemExit, message

  str_el = str(el)
  key_dP_min      = 'el_' + str_el + '_gm_dP_min'
  key_dP_mean     = 'el_' + str_el + '_gm_dP_mean'
  key_dP_med      = 'el_' + str_el + '_gm_dP_med'
  key_dP_max      = 'el_' + str_el + '_gm_dP_max'
  key_dP_std      = 'el_' + str_el + '_gm_dP_std'
  key_dP_peak_mean = 'el_' + str_el + '_gm_peak_dP_mean'
  key_dP_dip_depth_mean = 'el_' + str_el + '_gm_dip_depth_mean'
  key_dP_dip_dist       = 'el_' + str_el + '_gm_dip_dist'

  list_records = []
  for i_dic, dic in enumerate(list_dic):
    m_ini      = dic['m_ini']
    eta        = dic['eta']
    ov         = dic['ov']
    sc         = dic['sc']
    Z          = dic['Z']
    Xc         = dic['Xc']
    Yc         = dic['Yc']

    M_star     = dic['M_star'] / Msun
    R_star     = dic['R_star'] / Rsun
    L_star     = dic['L_star'] / Lsun
    Teff       = Tsun * np.power((L_star/R_star**2.), 1./4.)
    logg       = logg_sun + np.log10(M_star) - 2.*np.log10(R_star)

    dP_min     = dic[key_dP_min]
    dP_max     = dic[key_dP_max]
    dP_mean    = dic[key_dP_mean]
    dP_med     = dic[key_dP_med]
    dP_std     = dic[key_dP_std]

    dP_peak_mean = dic[key_dP_peak_mean]
    dP_dip_depth_mean = dic[key_dP_dip_depth_mean]
    dP_dip_dist  = dic[key_dP_dip_dist]

    records = [ov, Z, Xc, Yc, M_star, R_star, L_star, Teff, logg,
               dP_min, dP_max, dP_mean, dP_med, dP_std, dP_peak_mean,
               dP_dip_depth_mean, dP_dip_dist]

    list_records.append(records)

  # Collect the list of columns in the same was as they are going to be stored in the record array
  list_names = ['ov', 'Z',  'Xc', 'Yc', 'M_star', 'R_star', 'L_star', 'Teff', 'logg',
           key_dP_min, key_dP_max, key_dP_mean, key_dP_med, key_dP_std, key_dP_peak_mean,
           key_dP_dip_depth_mean, key_dP_dip_dist]
  names = ','.join(list_names)
  # convert to a numpy record array
  arr = np.array(list_records)
  recarr = np.core.records.fromarrays(arr.transpose(), names=names)

  return recarr, arr

#=================================================================================================================
#=================================================================================================================


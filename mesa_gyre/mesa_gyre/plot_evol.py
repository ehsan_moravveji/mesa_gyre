
"""
This module plots the evolution of various quantities either from multiple history files,
or from multiple profile files.
"""
import sys
import read, param_tools, plot_commons, plot_profile
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import itertools
from collections import OrderedDict
from ivs.aux.numpy_ext import recarr_join, recarr_addcols

Msun = 1.9892e33    # g
Rsun = 6.9598e10    # cm
G    = 6.67428e-8   # cm^3 g^-1 s^-2

#=================================================================================================================
def Kippenhahn(dic_hist, list_dic_prof, xaxis='model_number', yaxis='logT', xaxis_from=None, xaxis_to=None,
                          yaxis_from=None, yaxis_to=None, xtitle=None, ytitle=None, file_out=None):
  """
  Create the Kippenhahn diagram using multiple contour plots of the evolution of the burning and convective layers,
  stacked on top of one another. Most of the quantities are shown in logarithmic scale, e.g. log_D_conv, or log_eps_nuc, etc.
  @param dic_hist: dictionary with the MESA history information of the track. This is normally returned by read.read_h5()
  @type dic_hist: dictionary
  @param list_dic_prof: list of dictionaries containing the profile information from MESA
  @type list_dic_prof: list of dictionaries
  @param xaxis: The evolution is expressed in terms of this variable, valid variables could be 'center_h1', 'model_number'
     or 'star_age'
  @type xaxis: string
  @param yaxis: The internal structure is expressed in terms of this variable, valid variables could be 'mass', 'radius', 'logT', etc.
  @type yaxis: string
  @param file_out: full path to the output pdf plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  n_dic = len(list_dic_prof)
  if n_dic == 0:
    raise SystemExit, 'Error: plot_evol: Kippenhahn: Input list is empty'

  sample_dic   = list_dic_prof[0]
  if isinstance(sample_dic['header'], dict):
    hdr_names  = sample_dic['header'].keys()
  elif isinstance(sample_dic['header'], np.ndarray):
    hdr_names    = sample_dic['header'].dtype.names
  prof_names   = sample_dic['prof'].dtype.names

  if xaxis not in hdr_names:
    print hdr_names
    raise SystemExit, 'Error: plot_evol: Kippenhahn: {0} not in profile headers!'.format(xaxis)

  required_keys = [yaxis, 'mass', 'eps_nuc', 'log_D_conv', 'log_D_ovr', 'log_D_semi']
  optional_keys = ['log_D_soft', 'brunt_N2', 'brunt_N2_composition_term']
  dic_req_keys  = {}
  dic_opt_keys  = {}
  for name in prof_names: dic_req_keys[name] = 0.0
  for name in prof_names: dic_opt_keys[name] = 0.0
  dic_keys = read.check_key_exists(dic_req_keys, required_keys)
  for key in required_keys:
    if not dic_keys[key]:
      raise SystemExit, 'Error: plot_evol: Kippenhahn: {0} not available!'.format(key)
  dic_keys_opt  = read.check_key_exists(dic_opt_keys, optional_keys)
  include_mu_grad = dic_keys_opt['brunt_N2_composition_term']

  #--------------------------------
  # Prepare For Plotting; Load colors
  #--------------------------------
  from matplotlib import cm
  from matplotlib.colors import BoundaryNorm
  from matplotlib.ticker import MaxNLocator

  fig = plt.figure(figsize=(6,4), dpi=300)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.11, right=0.98, bottom=0.15, top=0.974, hspace=0.02, wspace=0.02)

  #--------------------------------
  # find the maximum nz among all input profiles;
  # all profiles will be interpolated to lie on the same mesh size
  #--------------------------------
  max_nz = 0
  nz_arr   = np.empty(n_dic, dtype=int)
  for i_dic, dic in enumerate(list_dic_prof):
    nz = len(dic['prof']['mass'])
    nz_arr[i_dic] = nz
  ind_max_nz   = np.argmax(nz_arr)
  max_nz       = nz_arr[ind_max_nz]
  ref_dic_prof = list_dic_prof[ind_max_nz]
  ref_header   = ref_dic_prof['header']
  ref_prof     = ref_dic_prof['prof']
  ref_mass     = ref_prof['mass']
  min_mass     = np.min(ref_mass)
  max_mass     = np.max(ref_mass)

  #--------------------------------
  # setup matrixes to store burning and mixing data
  #--------------------------------
  mtrx_x            = np.zeros((max_nz, n_dic))
  mtrx_y            = np.zeros((max_nz, n_dic))
  mtrx_eps_nuc      = np.zeros((max_nz, n_dic))
  mtrx_D_conv       = np.zeros((max_nz, n_dic))
  mtrx_D_ovr        = np.zeros((max_nz, n_dic))
  mtrx_D_semi       = np.zeros((max_nz, n_dic))

  # optional infor
  mtrx_D_soft       = np.zeros((max_nz, n_dic))
  mtrx_mu_grad      = np.zeros((max_nz, n_dic))  # relative to brunt_N2

  #--------------------------------
  # Loop over the profiles, store data in matrixes
  #--------------------------------
  for i_dic, dic in enumerate(list_dic_prof):
    header  = dic['header']
    prof    = dic['prof']

    mtrx_x[:, i_dic] = header[xaxis]

    mass    = prof['mass']
    yvals   = prof[yaxis]

    new_mass, yvals    = interpolate_array(max_nz, mass, yvals)
    mtrx_y[0:max_nz, i_dic] = yvals[0:max_nz]

    eps_nuc = prof['eps_nuc']
    new_mass, eps_nuc = interpolate_array(max_nz, mass, eps_nuc)
    mtrx_eps_nuc[0:max_nz, i_dic] = np.log10(eps_nuc[0:max_nz])

    D_conv  = prof['log_D_conv']
    new_mass, D_conv  = interpolate_array(max_nz, mass, D_conv)
    mtrx_D_conv[0:max_nz, i_dic] = D_conv[0:max_nz]

    D_ovr    = prof['log_D_ovr']
    new_mass, D_ovr = interpolate_array(max_nz, mass, D_ovr)
    mtrx_D_ovr[0:max_nz, i_dic] = D_ovr[0:max_nz]

    D_semi    = prof['log_D_semi']
    new_mass, D_semi = interpolate_array(max_nz, mass, D_semi)
    mtrx_D_semi[0:max_nz, i_dic] = D_semi[0:max_nz]

    if dic_keys_opt['log_D_soft']:
      D_soft  = prof['log_D_soft']
      new_mass, D_soft = interpolate_array(max_nz, mass, D_soft)
      mtrx_D_soft[0:max_nz, i_dic] = D_soft[0:max_nz]

    if include_mu_grad:
      brunt_N2= prof['brunt_N2']
      mu_grad = prof['brunt_N2_composition_term']
      new_mass, brunt_N2= interpolate_array(max_nz, mass, brunt_N2)
      new_mass, mu_grad = interpolate_array(max_nz, mass, mu_grad)
      rel_mu_grad       = mu_grad / brunt_N2
      mtrx_mu_grad[0:max_nz, i_dic] = np.log10(rel_mu_grad[0:max_nz])
      rel_mu_grad[rel_mu_grad<0] = 0.0
    
  include_semi = np.min(mtrx_D_semi) >= -10
  include_ov   = np.min(mtrx_D_ovr) >= 0


  #--------------------------------
  # Fix lower ranges of all matrixes
  #--------------------------------
  mtrx_eps_nuc[mtrx_eps_nuc<0] = 0.0
  mtrx_D_conv[mtrx_D_conv<0]   = 0.0
  mtrx_D_semi[mtrx_D_semi<0]   = 0.0

  mtrx_D_soft[mtrx_D_soft<0]   = 0.0
  mtrx_mu_grad[mtrx_mu_grad<0] = 0.0

  #--------------------------------
  # Create images
  #--------------------------------
  n_lev_10   = 10
  n_lev_100  = 100
  n_lev_1000 = 1000
  extent = (np.min(mtrx_x), np.max(mtrx_x), np.min(mtrx_y), np.max(mtrx_y))

  if True:
    levels = MaxNLocator(nbins=n_lev_100).tick_values(0, np.max(mtrx_D_conv))
    cmap   = plt.get_cmap('Blues') 
    norm   = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
    cont_D_conv = ax.contourf(mtrx_x, mtrx_y, mtrx_D_conv, extent=extent, hold='on', cmap=cmap,
                     norm=norm, origin='image', alpha=0.7, zorder=2, label=r'$\log D_{\rm conv}$')

  if include_ov and True:
    print ' - plot_evol: Kippenhahn: Including Overshooting layers'
    levels = MaxNLocator(nbins=n_lev_100).tick_values(0, np.max(mtrx_D_ovr))
    cmap   = plt.get_cmap('BuPu') 
    norm   = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
    cont_D_ovr = ax.contourf(mtrx_x, mtrx_y, mtrx_D_ovr, extent=extent, hold='on', cmap=cmap, 
                    norm=norm, origin='image', alpha=1.0, zorder=2, label=r'$\log D_{\rm over}$')

  if include_semi and True:
    print ' - plot_evol: Kippenhahn: Including Semi-convetive Contours: Range: {0} ... {1}'.format(np.min(mtrx_D_semi), np.max(mtrx_D_semi))
    levels = MaxNLocator(nbins=n_lev_1000).tick_values(0, np.max(mtrx_D_semi))
    cmap   = plt.get_cmap('Grey') 
    norm   = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
    cont_D_semi = ax.contourf(mtrx_x, mtrx_y, mtrx_D_semi, extent=extent, hold='on', cmap=cmap,
		                 norm=norm, origin='image', alpha=0.5, zorder=4, label=r'$\log D_{\rm semi}$')

  if dic_keys_opt['log_D_soft'] and True:
    print ' - plot_evol: Kippenhahn: Including Softening layers'
    levels = MaxNLocator(nbins=n_lev_100).tick_values(0, np.max(mtrx_D_soft))
    cmap   = plt.get_cmap('Greens') 
    norm   = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
    cont_D_soft = ax.contourf(mtrx_x, mtrx_y, mtrx_D_soft, extent=extent, hold='on', cmap=cmap,
                     norm=norm, origin='image', alpha=0.5, zorder=4, label=r'$\log D_{\rm soft}$')

  if include_mu_grad and False:
    print ' - plot_evol: Kippenhahn: Including "relative" mu-gradient layers'
    levels = MaxNLocator(nbins=n_lev_100).tick_values(0, np.max(mtrx_mu_grad))
    cmap   = plt.get_cmap('binary') 
    norm   = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
    cont_D_soft = ax.contourf(mtrx_x, mtrx_y, mtrx_mu_grad, extent=extent, hold='on', cmap=cmap,
                     norm=norm, origin='image', alpha=0.5, zorder=4, label=r'$N^2_{\mu}$')

  if True:
    levels = MaxNLocator(nbins=n_lev_10).tick_values(0, np.max(mtrx_eps_nuc))
    cmap   = plt.get_cmap('YlOrRd') 
    norm   = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
    cont_eps_nuc = ax.contourf(mtrx_x, mtrx_y, mtrx_eps_nuc, extent=extent, hold='on', cmap=cmap,
                      norm=norm, origin='image', alpha=0.5, zorder=5, label=r'$\log \epsilon_{\rm nuc}\geq2$')

  #--------------------------------
  # Add history data too
  #--------------------------------
  # hist = dic_hist['hist']
  hist_xaxis = mtrx_x[0, :]
  hist_yaxis = mtrx_y[-1, :]
  ax.plot(hist_xaxis, hist_yaxis, linestyle='solid', lw=1, color='black', zorder=6)

  #--------------------------------
  # Annotations, legend, Ranges, ...
  #--------------------------------
  if xaxis_from is None: xaxis_from = np.min(mtrx_x)
  if xaxis_to is None:   xaxis_to   = np.max(mtrx_x)
  if yaxis_from is None: yaxis_from = np.min(mtrx_y)
  if yaxis_to is None:   yaxis_to   = np.max(mtrx_y)
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)
  if xtitle: ax.set_xlabel(xtitle)
  if ytitle: ax.set_ylabel(ytitle)

  # cb_eps_nuc = plt.colorbar(cont_eps_nuc, orientation='vertical', shrink=0.8, pad=0.02, 
  #                           ticks=np.linspace(2, np.max(mtrx_eps_nuc), 5, endpoint=True),
  #                           format='%0.2f')
  # cb_eps_nuc.set_label(r'$\log\epsilon_{\rm nuc}$', fontsize='x-small')

  # Put a legend to the right of the current axis
  # box = ax.get_position()
  # ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
  # leg = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize='x-small')

  #--------------------------------
  # Finilize the plot
  #--------------------------------
  if file_out is not None:
    plt.savefig(file_out, transparent=True, dpi=300)
    print ' - plot_evol: Kippenhahn: saved {0}'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def Kippenhahn_basic(dic_hist, list_dic, xaxis='center_h1', mass_from=None, mass_to=None, file_out=None):
  """
  Plot the Kippenhahn diagram based on the list of MESA profile
  @param dic_hist: dictionary with the MESA history information of the track. This is normally returned by read.read_h5()
  @type dic_hist: dictionary
  @param list_dic: list of dictionaries containing the profile information from MESA
  @type list_dic: list of dictionaries
  @param xaxis: The evolution is expressed in terms of this variable, valid variables could be 'center_h1', 'model_number'
     or 'star_age'
  @type xaxis: string
  @param file_out: full path to the output pdf plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  n_dic = len(list_dic)
  if file_out is None:
    message = 'Error: plot_evol: Kippenhahn: output plot file is not specified'
    raise SystemExit, message

  #--------------------------------
  # Prepare For Plotting; Load colors
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.11, right=0.98, bottom=0.11, top=0.974, hspace=0.02, wspace=0.02)

  #--------------------------------
  # Fiddle around with the input fields
  #--------------------------------
  hist        = dic_hist['hist']
  filename    = dic_hist['filename']
  dic_params  = param_tools.get_param_from_single_hist_filename(filename)
  hist_wanted = ['conv_mx1_bot', 'conv_mx1_top', 'conv_mx2_bot', 'conv_mx2_top',
                 'epsnuc_M_1', 'epsnuc_M_2', 'epsnuc_M_3', 'epsnuc_M_4', 'epsnuc_M_5',
                 'epsnuc_M_6', 'epsnuc_M_7', 'epsnuc_M_8', 'mass_conv_core', 'model_number',
                 'mx1_bot', 'mx1_top', 'mx2_bot', 'mx2_top', 'star_age', 'star_mass',
                 'Teff', 'log_Teff', 'log_g']
  hist_names  = {}
  for name in hist.dtype.names: hist_names[name] = 0.0
  hist_fields = read.check_key_exists(hist_names, hist_wanted)
  hist_model_number= hist['model_number']
  n_hist          = len(hist_model_number)
  hist_age        = hist['star_age']/1e6  # in Myr
  hist_mass       = hist['star_mass']
  hist_Mcc        = hist['mass_conv_core']
  hist_Xc         = hist['center_h1']
  hist_Yc         = hist['center_he4']
  if hist_fields['conv_mx1_bot']:
    conv_mx1_bot  = hist['conv_mx1_bot'] * hist_mass
    conv_mx1_top  = hist['conv_mx1_top'] * hist_mass
    conv_mx2_bot  = hist['conv_mx2_bot'] * hist_mass
    conv_mx2_top  = hist['conv_mx2_top'] * hist_mass
  # Fix conv_mx... for unwanted switches between larger and smaller zone
  conv_up_top     = np.zeros(n_hist)
  conv_up_bot     = np.zeros(n_hist)
  conv_lo_top     = np.zeros(n_hist)
  conv_lo_bot     = np.zeros(n_hist)
  for i in range(n_hist):
    conv_up_top[i]= np.max([conv_mx1_top[i], conv_mx2_top[i]])
    conv_up_bot[i]= np.max([conv_mx1_bot[i], conv_mx2_bot[i]])
    #conv_lo_top[i]= np.min([np.min([hist_Mcc[i], conv_mx1_top[i]]), conv_mx2_top[i]])
    conv_lo_top[i]= np.min([conv_mx1_top[i], conv_mx2_top[i]])
    conv_lo_bot[i]= np.min([conv_mx1_bot[i], conv_mx2_bot[i]])

  if hist_fields['epsnuc_M_1']:
    epsnuc_M_1    = hist['epsnuc_M_1'] / Msun
    epsnuc_M_2    = hist['epsnuc_M_2'] / Msun
    epsnuc_M_3    = hist['epsnuc_M_3'] / Msun
    epsnuc_M_4    = hist['epsnuc_M_4'] / Msun
    epsnuc_M_5    = hist['epsnuc_M_5'] / Msun
    epsnuc_M_6    = hist['epsnuc_M_6'] / Msun
    epsnuc_M_7    = hist['epsnuc_M_7'] / Msun
    epsnuc_M_8    = hist['epsnuc_M_8'] / Msun

  if hist_fields['Teff']: log_Teff = np.log10(hist['Teff'])
  if hist_fields['log_Teff']: log_Teff = hist['log_Teff']
  if hist_fields['log_g']:
    log_g = hist['log_g']
    mxg           = np.argmax(log_g)
  else:
    # raise SystemExit, 'Kippenhahn: log_g not in hist; implement mxg'
    mxg           = 0
  mxg_Xc          = hist_Xc[mxg]
  mxg_age         = hist_age[mxg]
  mxg_mn= hist_model_number[mxg]

  if xaxis == 'center_h1': x_hist = hist_Xc
  if xaxis == 'model_number': x_hist = hist_model_number
  if xaxis == 'star_age': x_hist = hist_age

  # Profiles:
  a_prof      = list_dic[0]['prof']
  prof_wanted = ['mass', 'eps_nuc', 'h1', 'he4', 'mixing_type', 'mlt_mixing_type', 'conv_mixing_type']
  prof_names  = {}
  for name in a_prof.dtype.names: prof_names[name] = 0.0
  prof_fileds = read.check_key_exists(prof_names, prof_wanted)
  #if not prof_fileds[xaxis]: raise SystemExit, 'Kippenhahn: {0} cannot be xaxis; unavailable from profiles.'.format(xaxis)
  if not prof_fileds['mass']: raise SystemExit, 'Kippenhahn: mass not in profile'
  mxt_1 = prof_fileds['mixing_type']
  mxt_2 = prof_fileds['mlt_mixing_type']
  mxt_3 = prof_fileds['conv_mixing_type']
  if not any([mxt1, mxt_2, mxt_3]):
    raise SystemExit, 'Kippenhahn: mixing_type, mlt_mixing_type or convective_mixing not in profile'


  #--------------------------------
  # Loop over profiles
  #--------------------------------
  ages     = []
  xvals    = []
  Xc_vals  = []
  mn_vals  = []
  for i_dic, dic in enumerate(list_dic):
    header = dic['header']
    prof   = dic['prof']

    Teff      = header['Teff']
    Xc        = header['center_h1']
    Yc        = header['center_he4']
    M_ini     = header['initial_mass']
    Z_ini     = header['initial_z']
    prof_model_number = header['model_number']
    nz        = int(header['num_zones'][0])
    Lstar     = header['photosphere_L']  # in Lsun
    Rstar     = header['photosphere_r']  # in Rsun
    L_H       = header['power_h_burn']   # in Lsun
    L_He      = header['power_he_burn']  # in Lsun
    L_neu     = header['power_neu']      # in Lsun
    L_nuc     = header['power_nuc_burn'] # in Lsun
    star_age  = header['star_age']/1e6   # in Myr
    star_mass = header['star_mass']
    dt        = header['time_step']

    mass      = prof['mass']
    eps_nuc   = prof['eps_nuc']
    h1        = prof['h1']
    he4       = prof['he4']
    if mxt_1:
      mixing_type = prof['mixing_type']
    elif mxt_2: 
      mixing_type = prof['mlt_mixing_type']
    elif mxt_3:
      mixing_type = prof['conv_mixing_type']
    else:
      raise SystemExit, 'Error: Kippenhahn_basic: Neither of mxt_1, mxt_2 or mxt_3 are available'


    no = mixing_type == 0
    cz = mixing_type == 1
    ov = mixing_type == 2
    sc = mixing_type == 3
    rot= mixing_type == 5

    ages.append(star_age)
    age_arr   = np.array( [star_age] * nz )
    #zero_arr  = np.zeros(nz)
    Xc_vals.append(Xc)
    mn_vals.append(prof_model_number)

    # Set the xaxis
    if xaxis  == 'center_h1': xvals.append(Xc)
    if xaxis  == 'star_age':  xvals.append(star_age)
    if xaxis  == 'model_number': xvals.append(prof_model_number)
    x_prof    = np.array( [xvals[-1]]*nz )

    if any(rot):
      #ax.fill_between(x_prof, mass, zero_arr, where=rot, color="gray", alpha=0.01, zorder=0, label=r'Rot')
      ax.scatter(x_prof[rot], mass[rot], color='gray', marker='s', s=4, alpha=0.01, zorder=0)
    if any(cz):
      ##ax.fill_between(x_prof, mass, zero_arr, where=cz, color='blue', alpha=0.5, zorder=1, label=r'Conv')
      ax.scatter(x_prof[cz], mass[cz], color='black', marker='s', s=4, alpha=1.0, zorder=1)
    if any(ov):
      #ax.fill_between(x_prof, mass, zero_arr, where=ov, color='cyan', alpha=0.5, zorder=2, label=r'Overshoot')
      ax.scatter(x_prof[ov], mass[ov], color='cyan', marker='s', s=4, alpha=0.5, zorder=2)
    if any(sc):
      ax.scatter(x_prof[sc], mass[sc], color='0.10', marker='.', s=1, alpha=0.1, zorder=3)

  #--------------------------------
  # Plot Using History Data
  #--------------------------------
  # Convective zones
  if mass_to is None:
    ax.plot(x_hist, hist_mass, linestyle='solid', lw=1, color='black', zorder=1, label=r'M$_\star$')

  # ax.plot(x_hist, hist_Mcc, linestyle='dashed', lw=1.5, color='blue', zorder=1, label=r'M$_{\rm cc}$')
  #ax.fill_between(x_hist, conv_up_bot, conv_up_top, color='blue', alpha=0.01, zorder=1)
  #ax.fill_between(x_hist, conv_lo_bot, conv_lo_top, color='blue', alpha=0.25, zorder=1)

  # Burning zones
  ax.fill_between(x_hist, epsnuc_M_1, epsnuc_M_4, color='red', alpha=0.15, zorder=2, label=r'1$^{\rm st}$ Burning Zone')
  ax.fill_between(x_hist, epsnuc_M_2, epsnuc_M_3, color='red', alpha=0.25, zorder=3)
  ax.fill_between(x_hist, epsnuc_M_5, epsnuc_M_8, color='orange', alpha=0.15, zorder=2, label=r'2$^{\rm nd}$ Burning Zone')
  ax.fill_between(x_hist, epsnuc_M_6, epsnuc_M_7, color='orange', alpha=0.25, zorder=3)

  if mass_to is None:
    ax.plot(x_hist, log_Teff, linestyle='solid', lw=1, color='gray', label=r'$\log T_{\rm eff}$')

  #--------------------------------
  # Ranges, Annotations and Legend
  #--------------------------------
  if xaxis == 'star_age':
    ax.set_xlim(min([min(ages)*0.95, mxg_age]), max(ages)*1.01)
    ax.set_xlabel(r'Time [Myr]')
  if xaxis == 'center_h1':
    ax.set_xlim(max([mxg_Xc, max(Xc_vals)*1.01]), min(Xc_vals))
    ax.set_xlabel(r'Center $^1$H $X_c$')
  if xaxis == 'model_number':
    ax.set_xlim(max([mxg_mn, min(mn_vals)]), max(mn_vals))
    ax.set_xlabel(r'Model Number')
  if mass_from is None: mass_from = 0.0
  if mass_to is None: mass_to = star_mass * 1.05
  ax.set_ylim(mass_from, mass_to)
  ax.set_ylabel(r'Mass [M$_\odot$]')

  left = 0.04; top = 0.90; dy = 0.04; iy = 0
  if type(dic_params) is type({}):
    for key, val in dic_params.items():
      if key == 'filename': continue
      if key == 'M': key_tex = r'M$_{\rm ini}/$M$_\odot$'
      #if key == 'eta': key_tex = r'$\eta_{\rm rot}$'
      if key == 'ov': key_tex = r'$f_{\rm ov}$'
      if key == 'sc': key_tex = r'$\alpha_{\rm sc}$'
      if key == 'z': key_tex = r'Z'
      if False:
        txt = r'{0}={1}'.format(key_tex, val)
        print key, val, txt
        ax.annotate(txt, xy=(left, top-iy*dy), xycoords='axes fraction', fontsize='x-small')
      iy     += 1
  #txt = r'{0}={1}'.format(r'$\alpha_{\rm sc}$', dic_params['sc'])
  #ax.annotate(txt, xy=(left, 0.86), xycoords='axes fraction', fontsize='xx-large')

  # fake plots, just for legend
  ax.plot(-1-x_hist, -conv_lo_bot, color='black', alpha=1.0, lw=6, label=r'Convective Zone')
  # ax.plot(-1-x_hist, -epsnuc_M_6, color='red', alpha=0.25, zorder=3, lw=6, label=r'Burning Zone')
  # ax.plot(-x_prof[cz], -mass[cz], color='gray', lw=6, alpha=0.01, zorder=0, label=r'Rotating Zone')
  # ax.plot(-x_prof[cz], -mass[cz], color='cyan', lw=6, alpha=0.5, zorder=2, label=r'Overshooting')
  ax.plot(-x_prof[cz], -mass[cz], color='0.10', lw=6, alpha=0.1, zorder=3, label=r'Semi-Convection')
  leg = ax.legend(loc=1, fontsize='large', framealpha=0.5)

  #--------------------------------
  # Save the plot
  #--------------------------------
  if file_out:
    plt.savefig(file_out, transparent=True, dpi=200)
    print '\n - plot_evol: Kippenhahn: {0} saved'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def Kippenhahn_by_mix_qtop(dic_hist, xaxis, xaxis_from=None, xaxis_to=None, yaxis_from=None, 
                           yaxis_to=None, leg_loc=None, list_dic_annot=[], file_out=None):
  """
  It is possible to construct Kippenhahn diagram by using the extensive columns provided in MESA history 
  file that have names similar to mix_type_* and mix_qtop_*, where they are both appended with the number
  label of the zone(s).
  """
  header = dic_hist['header']
  hist   = dic_hist['hist']
  M_star = hist['star_mass']  
  M_ini  = M_star[0]
  keys   = hist.dtype.names

  #--------------------------------
  # Enumerate the zones
  #--------------------------------
  n_zones= 0
  for i, key in enumerate(keys):
    if 'mix_qtop_' in key: n_zones += 1

  if n_zones == 0:
    print ' - plot_evol: Kippenhahn_by_mix_qtop: No mixing zones found!'
    print '              Activate "mixing_regions <integer>" option in MESA history_column.list file'
    return 

  if xaxis not in keys:
    raise SystemExit, 'Error: plot_evol: Kippenhahn_by_mix_qtop: {0} not available!'.format(xaxis)

  #--------------------------------
  # Prepare the Figure
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(12, 4))
  plt.subplots_adjust(left=0.05, right=0.985, bottom=0.12, top=0.97)

  xvals   = hist[xaxis]
  lbl     = r'' + '{0:.1f}'.format(M_ini) + r' $M_\odot$'
  ax.plot(xvals, M_star / M_ini, lw=1, color='black', label=lbl)

  #--------------------------------
  # Fill between
  #--------------------------------
  for i_row, row in enumerate(hist[1:]):
    model_number= row['model_number']
    row_mx_type = np.empty(n_zones, dtype=int)
    row_mx_qtop = np.empty(n_zones, dtype=float)
    
    for i_col in range(1, n_zones+1):
      key_mx_type          = 'mix_type_{0}'.format(i_col)
      key_mx_qtop          = 'mix_qtop_{0}'.format(i_col)
      row_mx_type[i_col-1] = row[key_mx_type]
      row_mx_qtop[i_col-1] = row[key_mx_qtop] * row['star_mass'] / M_ini

    for i_mx in range(n_zones):
      mxt    = row_mx_type[i_mx]
      if mxt not in [1, 3, 4]: continue

      qtop   = row_mx_qtop[i_mx]
      if i_mx == 0:
        qbot = 0.0
      else:
        qbot = row_mx_qtop[i_mx-1]

      if mxt == 1:
        clr = 'blue'
      elif mxt == 3:
        clr = 'grey'
      elif mxt == 4:
        clr = 'green'
      else:
        clr = 'black'  # unclassified

      ax.fill_between([model_number], y1=qbot, y2=qtop, color=clr, zorder=2)

  #--------------------------------
  # Add 8 burning regions
  #--------------------------------
  M_ini_cgs = M_ini * Msun
  # ax.fill_between(x=xvals, y1=hist['epsnuc_M_1']/M_ini_cgs, y2=hist['epsnuc_M_4']/M_ini_cgs, 
  #                 where=(hist['epsnuc_M_1']>=0), color='yellow', alpha=0.5, zorder=3, label='Weak Burning')
  ax.fill_between(x=xvals, y1=hist['epsnuc_M_2']/M_ini_cgs, y2=hist['epsnuc_M_3']/M_ini_cgs, 
                  where=(hist['epsnuc_M_2']>=0), color='red', alpha=0.5, zorder=4, label='Strong Burning')

  ax.fill_between(x=xvals, y1=hist['epsnuc_M_5']/M_ini_cgs, y2=hist['epsnuc_M_8']/M_ini_cgs, 
                  where=(hist['epsnuc_M_5']>=0), color='yellow', alpha=0.5, zorder=4)
  ax.fill_between(x=xvals, y1=hist['epsnuc_M_6']/M_ini_cgs, y2=hist['epsnuc_M_7']/M_ini_cgs, 
                  where=(hist['epsnuc_M_6']>=0), color='red', alpha=0.5, zorder=4)

  #--------------------------------
  # Cosmetics
  #--------------------------------
  if xaxis_from is None: xaxis_from = min(xvals) * 0.99
  if xaxis_to is None:   xaxis_to   = max(xvals) * 1.01
  if yaxis_from is None: yaxis_from = -0.0
  if yaxis_to is None:   yaxis_to   = 1.02
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)

  if list_dic_annot: plot_commons.add_annotation(ax, list_dic_annot)

  # Only for Legend purpose:
  ax.plot([], [], lw=10, color='blue', label=r'Convective')
  ax.plot([], [], lw=10, color='grey', label=r'Overshooting')
  ax.plot([], [], lw=10, color='green', label=r'Semi-Convective')

  ax.plot([], [], lw=10, color='yellow', label=r'Weak Burning')
  ax.plot([], [], lw=10, color='red', label=r'Strong Burning')

  ax.legend(bbox_to_anchor=(0.15, 0.97), fontsize=10, frameon=False, 
            fancybox=False, shadow=False, borderpad=False)
  
  xtitle = None
  if xaxis == 'model_number': xtitle = 'Model Number'
  if xtitle is not None: ax.set_xlabel(xtitle)
  ax.set_ylabel(r'Relative Mass $\, m_r/M_\star$')

  plt.savefig(file_out, transparent=False)
  print ' - plot_evol: Kippenhahn_by_mix_qtop: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def plot_evol_prof_density(list_dic, xaxis, list_lbls=None, xaxis_from=None, xaxis_to=None,
                           yaxis_from=None, yaxis_to=None, xtitle=None, ytitle=None, leg_loc=None,
                           offset=False, file_out=None):
  """
  Plot the evolution of the density from the pre-main sequence phase up to the TAMS, and specify the convective extent
  by an overlying line
  """
  n_dic = len(list_dic)
#=================================================================================================================
def plot_evol_prof(list_dic, xaxis, yaxis, list_lbls=None, xaxis_from=None, xaxis_to=None,
                   yaxis_from=None, yaxis_to=None, xtitle=None, ytitle=None, leg_loc=None,
                   offset=False, trace_mixing_zones=False, box_from=None, box_to=None, file_out=None):
  """
  Plot the evolution of the profile of any quantity
  """
  n_dic = len(list_dic)
  if n_dic == 0:
    message = 'Error: plot_evol: plot_evol_prof: Input list is empty.'
    raise SystemExit, message

  dic_sample = list_dic[0]
  if 'prof' not in dic_sample.keys():
    message = 'Error: plot_evol: plot_evol_prof: Input dictionaries miss the "prof" field.'
    raise SystemExit, message
  prof_sample = dic_sample['prof']
  if not xaxis in prof_sample.dtype.names:
    message = 'Error: plot_evol: plot_evol_prof: %s field is missing in "prof" field.' % (xaxis, )
    raise SystemExit, message
  if not yaxis in prof_sample.dtype.names:
    message = 'Error: plot_evol: plot_evol_prof: %s field is missing in "prof" field.' % (yaxis, )
    raise SystemExit, message
  if trace_mixing_zones and not 'stability_type' in prof_sample.dtype.names:
    message = 'Error: plot_evol: plot_evol_prof: "stability_type" is missing from profiles.'
    raise SystemExit, message

  #--------------------------------
  # Prepare For Plotting; Load colors
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=160)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.11, right=0.98, bottom=0.11, top=0.98, hspace=0.02, wspace=0.02)
  if xtitle:
    ax.set_xlabel(xtitle)
  else:
    ax.set_xlabel(r''+xaxis)
  if ytitle:
    ax.set_ylabel(ytitle)
  else:
    ax.set_ylabel(r''+yaxis)

  cmap = plt.get_cmap('brg')
  norm = mpl.colors.Normalize(vmin=0, vmax=n_dic)
  color = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)

  # collect the min/max range along x/yaxis for all profile files
  list_min_xaxis = []
  list_max_xaxis = []
  list_min_yaxis = []
  list_n_yaxis = []

  for i_dic, dic in enumerate(list_dic):
    prof = dic['prof']
    header = dic['header']
    logT   = prof['logT']
    n_mesh = len(logT)

    xaxis_arr = prof[xaxis]
    yaxis_arr = prof[yaxis]
    list_min_xaxis.append(np.min(xaxis_arr))
    list_max_xaxis.append(np.max(xaxis_arr))
    list_min_yaxis.append(np.min(yaxis_arr))
    list_n_yaxis.append(np.max(yaxis_arr))

    # specify the extent of CZ if requested
    if trace_mixing_zones:
      mixing_type = prof['mixing_type']
      no = mixing_type == 0
      cz = mixing_type == 1
      ov = mixing_type == 2
      sc = mixing_type == 3
      rot= mixing_type == 5

      ax.fill_between(xaxis_arr, yaxis_arr, 0, where=no, color="white")
      ax.fill_between(xaxis_arr, yaxis_arr, 0, where=cz, color="0.90", alpha=0.7)
      ax.fill_between(xaxis_arr, yaxis_arr, 0, where=ov, color='blue', alpha=0.5)
      ax.fill_between(xaxis_arr, yaxis_arr, 0, where=sc, color='green', alpha=0.5)
      ax.fill_between(xaxis_arr, yaxis_arr, 0, where=rot, color='red', alpha=0.5)

      #yaxis_arr_cz = yaxis_arr[cz]
      #yaxis_arr_ov = yaxis_arr[ov]
      #yaxis_arr_sc = yaxis_arr[sc]
      #yaxis_arr_rot= yaxis_arr[rot]

    #if trace_mixing_zones:
      #mixing_type = prof['mixing_type']
      #no_mixing = np.where((mixing_type == 0))[0]
      #convective_mixing = np.where((mixing_type == 1))[0]
      #overshoot_mixing = np.where((mixing_type == 2))[0]
      #semiconvective_mixing = np.where((mixing_type == 3))[0]
      #thermo_haline_mixing = np.where((mixing_type == 4))[0]
      #rotation_mixing = np.where((mixing_type == 5))[0]
      #minimum_mixing = np.where((mixing_type == 6))[0]
      #anonymous_mixing = np.where((mixing_type == 7))[0]

      #list_dic_conv = plot_profile.identify_mixing_type_edges(convective_mixing)
      #list_dic_over = plot_profile.identify_mixing_type_edges(overshoot_mixing)
      #list_dic_semi = plot_profile.identify_mixing_type_edges(semiconvective_mixing)
      #list_dic_rot  = plot_profile.identify_mixing_type_edges(rotation_mixing)
      #list_dic_anon = plot_profile.identify_mixing_type_edges(anonymous_mixing)

      #if list_dic_conv:
        #for i_zone in range(len(list_dic_conv)):
          #if i_zone > 0: continue
          #zone_from_indx = list_dic_conv[i_zone]['from']
          #zone_to_indx   = list_dic_conv[i_zone]['to']
          #zone_from = xaxis_arr[zone_from_indx]
          #zone_to   = xaxis_arr[zone_to_indx]
          #zone_width = zone_to - zone_from
          ##rect_conv = plt.Rectangle((zone_from, 0), zone_width, n_mesh, facecolor="0.90", edgecolor=None,
                                  ##lw=None)
          ##plt.gca().add_patch(rect_conv)
          ##bbox = Bbox.from_bounds(zone_from, 0, zone_width, n_mesh)

      #if list_dic_over:
        #for i_zone in range(len(list_dic_over)):
          #zone_from_indx = list_dic_over[i_zone]['from']
          #zone_to_indx   = list_dic_over[i_zone]['to']
          #zone_from = xaxis_arr[zone_from_indx]
          #zone_to   = xaxis_arr[zone_to_indx]
          #zone_width = zone_to - zone_from
          #rect_over = plt.Rectangle((zone_from, 0), zone_width, n_mesh, facecolor="Pink")
          #plt.gca().add_patch(rect_over)
          ##bbox = Bbox.from_bounds(zone_from, 0, zone_width, n_mesh)

      #if list_dic_semi:
        #for i_zone in range(len(list_dic_semi)):
          #zone_from_indx = list_dic_semi[i_zone]['from']
          #zone_to_indx   = list_dic_semi[i_zone]['to']
          #zone_from_logT = logT[zone_from_indx]
          #zone_to_logT   = logT[zone_to_indx]
          #zone_width_logT = zone_to_logT - zone_from_logT
          #rect_semi = plt.Rectangle((zone_from_logT, 0), zone_width_logT, nlines, facecolor="green")
          #plt.gca().add_patch(rect_semi)
          ##bbox = Bbox.from_bounds(zone_from_logT, 0, zone_width_logT, nlines)

      #if  list_dic_rot:
        #for i_zone in range(len(list_dic_rot)):
          #zone_from_indx = list_dic_rot[i_zone]['from']
          #zone_to_indx   = list_dic_rot[i_zone]['to']
          #zone_from_logT = logT[zone_from_indx]
          #zone_to_logT   = logT[zone_to_indx]
          #zone_width_logT = zone_to_logT - zone_from_logT
          #rect_rot = plt.Rectangle((zone_from_logT, 0), zone_width_logT, nlines, facecolor="Blue")
          #plt.gca().add_patch(rect_rot)
          ##bbox = Bbox.from_bounds(zone_from_logT, 0, zone_width_logT, nlines)


      #if any(yaxis_arr_cz):  ax.plot(xaxis_arr[cz], yaxis_arr_cz, lw=2, color='gray')
      #if any(yaxis_arr_ov):  ax.plot(xaxis_arr[ov], yaxis_arr_ov, lw=2, color='blue')
      #if any(yaxis_arr_sc):  ax.plot(xaxis_arr[sc], yaxis_arr_sc, lw=2, color='cyan')
      #if any(yaxis_arr_rot): ax.plot(xaxis_arr[rot], yaxis_arr_rot, lw=2, color='red')

    ax.plot(xaxis_arr, yaxis_arr, lw=1, color=color.to_rgba(i_dic))

  # Fix the axis ranges
  list_min_xaxis = np.asarray(list_min_xaxis)
  list_max_xaxis = np.asarray(list_max_xaxis)
  list_min_yaxis = np.asarray(list_min_yaxis)
  list_n_yaxis = np.asarray(list_n_yaxis)
  xaxis_min = np.min(list_min_xaxis)
  xaxis_max = np.max(list_max_xaxis)
  yaxis_min = np.min(list_min_yaxis)
  yaxis_max = np.max(list_n_yaxis)

  if xaxis_from: xaxis_min = xaxis_from
  if xaxis_to:   xaxis_max = xaxis_to
  if yaxis_from: yaxis_min = yaxis_from
  if yaxis_to:   yaxis_max = yaxis_to
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)

  if not file_out:
     file_out = param.gen_path_plot() + 'Evol-Prof-' + xaxis + '-vs-' + yaxis + '.pdf'
  plt.savefig(file_out, transparent=True)
  print ' - Plot stored at %s' % (file_out, )
  plt.close()

  return None

#=================================================================================================================
def plot_evol_hist(list_dic_hist, xaxis, yaxis, list_lbls=None, list_list_dic_prof=None, xaxis_from=None,
                   xaxis_to=None, yaxis_from=None, yaxis_to=None, xtitle=None, ytitle=None, leg_loc=None,
                   ref_indx=0, file_out=None):
  """
  Plot the evolution of any quantity available in the history file specified by xaxis, versus another quantity set
  by yaxis. For instance, it is possible to plot the evolution of the size in mass of the hydrogen burning core as
  a function of time/timestep/center_h1/Teff/logg etc, and simultaneously compare (i.e. overplot) with that of another
  track.
  @param list_dic_hist: list of dictionaries, where each dictionary is read by read.read_mesa or read.read_multiple_mesa_files
      and has at least these two keys: 1- 'header', and 2- 'hist'
      The xaxis must match one of the available keys in 'hist'.
  @type list_dic_hist: list of dictionaries
  @param xaxis: a key in 'hist' for the xaxis
  @type xaxis: string
  @param yaxis: a key in 'hist' for the yaxis
  @type yaxis: string
  @param list_lbls: list of strings, for each of the dictionaries in list_dic_hist
  @type list_lbls: list of strings
  @param list_list_dic_prof: defalt=None; if provided, it is complicated! It gives a list of list of dictionaries.
      per each history file in list_dic_hist, there are several profile files stored. These profile files are
      identified and read into a list of dictionaries by read.read_multiple_mesa_files. each dictionary has
      these keys: filename, header, and prof. Here, we mainly use the header only to get model_number and center_h1 for
      each profile file, and put them on the track, to highlight each point along the track where a profile is stored.
  @type list_list_dic_prof: list of list of dictionaries.
  @param xaxis_from, xaxis_to, yaxis_from, yaxis_to: giving the range in xaxis/yaxis
  @type xaxis_from, xaxis_to, yaxis_from, yaxis_to: float
  @param xtitle, ytitle: LaTeX compatible titles for the xaxis/yaxis
  @type xtitle, ytitle: string
  @param leg_loc: specifying the exact location of the legend box. It complies with the matplotlib.pyplot.legend options.
  @type leg_loc: integer, or string.
  @param file_out: full path to the output pdf plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  n_dic = len(list_dic_hist)
  if n_dic == 0:
    message = 'Error: plot_evol: plot_evol_hist: Input list is empty!'
    raise SystemExit, message

  if list_list_dic_prof:
    n_list_list_dic_prof = len(list_list_dic_prof)
    if n_dic != n_list_list_dic_prof:
      print 'Error: plot_evol: plot_evol_hist: Each track MUST associate with a list of dictionaries in list_list_dic_prof.'
      message = 'n_hist = %s, and n_list = %s' % (n_dic, n_list_list_dic_prof)
      raise SystemExit, message

  print ' - mesa_gyre.plot_evol:  '
  #--------------------------------
  # Prepare For Plotting; Load colors
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=160)
  plt.axes([0.13, 0.13, 0.85, 0.85])
  if xtitle: plt.xlabel(xtitle)
  if ytitle: plt.ylabel(ytitle)

  cmap = plt.get_cmap('brg')
  norm = mpl.colors.Normalize(vmin=0, vmax=n_dic-1)
  color = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)
  clrs = itertools.cycle(['r', 'b', 'g', 'y', 'p', 'grey'])

  ls = itertools.cycle(['dotted', 'dashed', 'dashdot'])

  #--------------------------------
  # Unpack the dictionary contents iteratively
  #--------------------------------
  xaxis_list_min = []; xaxis_list_max = []
  yaxis_list_min = []; yaxis_list_max = []
  for i_hist, dic in enumerate(list_dic_hist):
    if not dic.has_key('hist'):
      print '   Warning: The key "hist" is missing in item number ', i_hist
      continue
    if not dic.has_key('header'):
      print '   Warning:  key "header" is missing in item number ', i_hist
      continue

    header = dic['header']
    hist = dic['hist']
    hist_names = hist.dtype.names
    if not xaxis in hist_names:
      print '   Warning: xaxis = %s is missing in "hist" ' % (xaxis, )
      continue
    if not yaxis in hist_names:
      print '   Warning: yaxis = %s is missing in "hist" ' % (yaxis, )
      continue
    if not 'log_g' in hist_names:
      print '   Warning: log_g not available, mxg not found for {0}'.format(i_hist)
      continue

    log_g     = hist['log_g']
    # mxg       = np.argmax(log_g)
    mxg = 0

    xaxis_arr = hist[xaxis][mxg : ]
    yaxis_arr = hist[yaxis][mxg : ]

    if i_hist == ref_indx:
      clr = 'black'
      lstyl = 'solid'
      #list_lbls[ref_indx] = r'Ref: ' + list_lbls[ref_indx]
    else:
      # clr = color.to_rgba(i_hist)
      clr   = clrs.next()
      lstyl = ls.next()

    if list_lbls:
      lbl = list_lbls[i_hist]
      plt.plot(xaxis_arr, yaxis_arr, color=clr, linestyle=lstyl, label=lbl)
    else:
      plt.plot(xaxis_arr, yaxis_arr, color=clr, linestyle=lstyl)

    xaxis_list_min.append(min(xaxis_arr))
    xaxis_list_max.append(max(xaxis_arr))
    yaxis_list_min.append(min(yaxis_arr))
    yaxis_list_max.append(max(yaxis_arr))

    #--------------------------------
    # Add profile data - if available - for each track
    #--------------------------------
    if list_list_dic_prof:
      list_dic_prof = list_list_dic_prof[i_hist]
      n_dic_in_list = len(list_dic_prof)
      if n_dic_in_list == 0: continue

      # Check if xaxis and yaxis are available in the HEADER of dic_prof:
      sample_dic_prof_names = list_dic_prof[0]['header'].dtype.names
      if xaxis not in sample_dic_prof_names:
        print '   Warning: plot_evol: plot_evol_hist: %s absent in prof data' % (xaxis)
        continue
      if yaxis not in sample_dic_prof_names:
        print '   Warning: plot_evol: plot_evol_hist: %s absent in prof data' % (yaxis)
        continue

      xaxis_prof_point = []; yaxis_prof_point = []
      for i_dic, dic_prof in enumerate(list_dic_prof):
        xaxis_prof_point.append(dic_prof['header'][xaxis][0])
        yaxis_prof_point.append(dic_prof['header'][yaxis][0])

      xaxis_prof_arr = np.asarray(xaxis_prof_point)
      yaxis_prof_arr = np.asarray(yaxis_prof_point)
      plt.scatter(xaxis_prof_arr, yaxis_prof_arr, s=35, marker='o', color=color.to_rgba(i_hist))

  #--------------------------------
  # Fixing the range, and annotations
  #--------------------------------
  if xaxis_from is not None: xaxis_min = xaxis_from
  else:          xaxis_min = min(xaxis_list_min)
  if xaxis_to is not None:   xaxis_max = xaxis_to
  else:          xaxis_max = max(xaxis_list_max)
  if yaxis_from is not None: yaxis_min = yaxis_from
  else:          yaxis_min = min(yaxis_list_min)
  if yaxis_to is not None:   yaxis_max = yaxis_to
  else:          yaxis_max = max(yaxis_list_max)
  plt.xlim(xaxis_min, xaxis_max)
  plt.ylim(yaxis_min, yaxis_max)

  #plt.annotate('(b)', xy=(0.03, 0.05), xycoords='axes fraction', fontsize='large')
  #plt.annotate('Instantaneous Mixing', xy=(0.50, 2.6), fontsize='medium', rotation=-23.5)

  if list_lbls:
    if not leg_loc: leg_loc = 1
    leg = plt.legend(loc=leg_loc, shadow=True, fontsize='small')
    leg.get_frame().set_alpha(0.50)

  if not file_out:
    #track_srch_str = param.gen_track_srch_str()['track_srch_str']
    file_out = param.gen_path_plot() + 'Evol-Hist-' + xaxis + '-vs-' + yaxis + '.pdf'
  fig.savefig(file_out, transparent=True, dpi=160)
  print '  Plot stored at: %s' % (file_out, )
  plt.close()

  return fig

#=================================================================================================================
def plot_evol_prof_N_adim(list_dic, xaxis, list_lbls=None, xaxis_from=None, xaxis_to=None,
                   yaxis_from=None, yaxis_to=None, xtitle=None, leg_loc=None,
                   offset=False, box_from=None, box_to=None, file_out=None):
  """
  This is just a wrapper for plot_evol_prof by adding another field to each dictionary for adimensional N: "N_adim"
  Then, plot_evol_prof is called passing all arguments to it, and using yaxis='N_adim'.
  We strongly suggest you to read the documentation for plot_evol_prof. Note that it is no longer required to provide
  yaxis and ytitle as input arguments.
  """
  import commons
  import numpy.lib.recfunctions as nlrf
  from matplotlib.transforms import Bbox
  from matplotlib.path import Path

  dic_conv = commons.conversions()
  cd_to_Hz = dic_conv['cd_to_Hz']

  n_dic = len(list_dic)
  if n_dic == 0:
    message = 'Error: plot_evol: plot_evol_prof_N_adim: Input list is empty.'
    raise SystemExit, message

  dic_sample = list_dic[0]
  if 'prof' not in dic_sample.keys():
    message = 'Error: plot_evol: plot_evol_prof_N_adim: Input dictionaries miss the "prof" field.'
    raise SystemExit, message
  prof_sample = dic_sample['prof']
  prof_names = prof_sample.dtype.names
  if not xaxis in prof_names:
    message = 'Error: plot_evol: plot_evol_prof_N_adim: %s filed is missing in "prof" field.' % (xaxis, )
    raise SystemExit, message

  required_keys = ['brunt_N2', 'brunt_N', 'brunt_N2_dimensionless', 'brunt_N_dimensionless', 'brunt_nu',
                   'brunt_frequency', 'log_brunt_N', 'log_brunt_N2', 'log_brunt_N2_dimensionless',
                   'lamb_S2', 'lamb_S', 'lamb_SL']
  dic_avail_keys = {}
  for name in prof_names: dic_avail_keys[name] = 0.0
  dic_keys = read.check_key_exists(dic_avail_keys, required_keys)

  if not any(dic_keys.values()):
    print 'Error: plot_evol: plot_evol_prof_N_adim: All these fields are missing: '
    print required_keys
    raise SystemExit, 'Repeat MESA tracks with "brunt_N2_dimensionless" preserved in profiles.'

  # The best possibility is when 'brunt_N_dimensionless' is available, and no unit conversion is requird.
  # The worst case is when e.g. 'log_brunt_N2' is provided where some conversion is required.
  # The following procedure is written from the worst to the best to ensure we end up with the neatest profile
  # for N_adim!

  plt.figure()
  if all([box_from, box_to]):
    rect = plt.Rectangle((xaxis_from, box_from), xaxis_to-xaxis_from, box_to-box_from, facecolor="gray", alpha=0.5)
    plt.gca().add_patch(rect)
    bbox = Bbox.from_bounds(xaxis_from, box_from, xaxis_to-xaxis_from, box_to-box_from)

  list_dic_prof_extended = []
  for i_dic, dic in enumerate(list_dic):
    prof = dic['prof']
    header = dic['header']
    prof_names = prof.dtype.names
    star_mass = header['star_mass']
    star_mass_cgs = star_mass * Msun
    radius = header['photosphere_r']
    radius_cgs = radius * Rsun
    GM_R3 = G * star_mass_cgs / radius_cgs**3.0  # Hz^2
    sqrt_GM_R3 = np.sqrt(GM_R3)  # Hz
    Hz_to_adim = 1.0/sqrt_GM_R3  # e.g. N_adim = N_Hz * Hz_to_adim

    # the following require unit conversion
    if dic_keys['brunt_nu']:
      N_Hz = prof['brunt_nu'] * 1e-6
      N_adim = N_Hz * Hz_to_adim
    if dic_keys['brunt_frequency']:
      N_cd = prof['brunt_frequency']
      N_Hz = N_cd * cd_to_Hz
      N_adim = N_Hz * Hz_to_adim
    if dic_keys['log_brunt_N2']:
      N2_Hz2 = np.power(10., prof['log_brunt_N2'])
      N_Hz = np.sqrt(N2_Hz2)
      N_adim = N_Hz * Hz_to_adim
    if dic_keys['log_brunt_N']:
      N_Hz = np.power(10.0, prof['log_brunt_N'])
      N_adim = N_Hz * Hz_to_adim
    if dic_keys['brunt_N2']:
      N_Hz = np.sqrt(prof['brunt_N2'])
      N_adim = N_Hz * Hz_to_adim
    if dic_keys['brunt_N']: N_adim = prof['brunt_N'] * Hz_to_adim
    # The following do not require unit conversion
    if dic_keys['log_brunt_N2_dimensionless']:
      N2_adim = np.power(10.0, prof['log_brunt_N2_dimensionless'])
      N_adim  = np.sqrt(N2_adim)
    if dic_keys['brunt_N2_dimensionless']: N_adim = np.sqrt(prof['brunt_N2_dimensionless'])
    if dic_keys['brunt_N_dimensionless']: N_adim = prof['brunt_N_dimensionless']

    # Include Lamb Frequency if available
    S_adim = np.zeros(len(N_adim))
    if dic_keys['lamb_S2']: S_adim = np.sqrt(prof['lamb_S2']) * Hz_to_adim
    if dic_keys['lamb_SL']: S_adim = prof['lamb_SL'] * Hz_to_adim
    if dic_keys['lamb_S']: S_adim = prof['lamb_S'] * Hz_to_adim

    # Add the new field to the previous record array
    #N_adim = np.core.records.fromrecords(N_adim, dtype=[('N_adim',float)])
    #new_prof = recarr_join(prof, )
    #test_rec_arr = np.core.rec.fromarrays(N_adim, [('N_adim',float)])
    #print type(test_rec_arr), len(test_rec_arr), test_rec_arr.shape
    #new_prof = recarr_addcols(prof, [list(N_adim)], [('N_adim', '<f8')])
    ##nlrf.append_fields(prof, 'N_adim', data=N_adim, dtypes=new_dtypes, asrecarray=True, usemask=False)
    #list_dic_prof_extended.append(new_prof)
    #new_prof = 0.0


    if offset:
      N_adim += i_dic * offset
      S_adim += i_dic * offset
    plt.plot(prof[xaxis], N_adim)
    plt.plot(prof[xaxis], S_adim)


  # Now, pass the new list of profile dictionaries with the new "N_adim" field to plot_evol_prof
  #plot_evol_prof(list_dic_prof_extended, xaxis, 'N_adim', list_lbls=list_lbls, xaxis_from=xaxis_from, xaxis_to=xaxis_to,
                   #yaxis_from=yaxis_from, yaxis_to=yaxis_to, xtitle=xtitle, leg_loc=leg_loc,
                   #ytitle=r'Brunt Vaisala Frequency $N$ [\sqrt{GM/R^3}]', offset=offset, box_from=box_from, box_to=box_to,
                   #file_out=file_out)

  #print ' - plot_evol: plot_evol_prof_N_adim: Redirecting the plotting to plot_evol.plot_evol_prof()'
  plt.xlim(xaxis_from, xaxis_to)
  plt.ylim(yaxis_from, yaxis_to)
  plt.savefig(file_out, transparent=True)
  print '   Plot %s created \n' % (file_out, )
  plt.close()

  return None

#=================================================================================================================
def plot_evol_overshoot_properties(ref_tracks=[], comp_tracks=[], ref_names=[], comp_names=[], file_out=None):
  """
  This is only specifically used for comparing exponentially decaying and step function overshoot presctiptions.
  In the history files, for each choice, six keys are mandatory to be available:
  - m_bot_ov [gr]
  - m_top_ov [gr]
  - r_bot_ov [cm]
  - r_top_ov [cm]
  - Hp_bot_ov [cm]
  - int_D_ov [cm/sec]
  @param ref_tracks, comp_tracks: reference/comparison tracks
  @type ref_tracks, comp_tracks: list of dictionaries, returned e.g. by read.read_multiple_h5
  @param ref_names, comp_names: names associated to every individual item in ref_tracks and comp_tracks. Used for plot legend
  @type ref_names, comp_names: list of strings
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  from plot_commons import smooth_1d
  n_ref = len(ref_tracks)
  n_com = len(comp_tracks)

  fig = plt.figure(figsize=(12,4), dpi=200)
  ax1 = fig.add_subplot(141)
  ax2 = fig.add_subplot(142)
  ax3 = fig.add_subplot(143)
  ax4 = fig.add_subplot(144)
  #ax5 = fig.add_subplot(235)
  #ax6 = fig.add_subplot(236)
  plt.subplots_adjust(left=0.055, right=0.99, bottom=0.105, top=0.975, hspace=0.5, wspace=0.3)

  cmap = plt.get_cmap('brg')
  ref_norm   = mpl.colors.Normalize(vmin=0, vmax=n_ref)
  ref_color  = mpl.cm.ScalarMappable(norm=ref_norm, cmap=cmap)
  comp_norm  = mpl.colors.Normalize(vmin=0, vmax=n_com)
  comp_color = mpl.cm.ScalarMappable(norm=comp_norm, cmap=cmap)

  # Start plotting the reference tracks
  for i_ref, dic in enumerate(ref_tracks):
    hist = dic['hist']
    Xc = hist['center_h1']
    m_bot_ov = hist['m_bot_ov'] # gr
    m_top_ov = hist['m_top_ov'] # gr
    m_ov     = (m_top_ov - m_bot_ov) / Msun
    m_ov     = smooth_1d(m_ov, m=5)
    r_bot_ov = hist['r_bot_ov'] # cm
    r_top_ov = hist['r_top_ov'] # cm
    r_ov     = (r_top_ov - r_bot_ov) / Rsun
    r_ov     = smooth_1d(r_ov, m=5)
    Hp_bot_ov= hist['Hp_bot_ov'] / Rsun
    int_D_ov = hist['int_D_ov'] # cm/sec
    M_cc     = hist['mass_conv_core']

    # First panel: Mass budget of the overshooting zone
    #clr = ref_color.to_rgba(i_ref)
    clr = 'black'
    if ref_names:
      ax1.plot(Xc, Hp_bot_ov, color=clr, linestyle='solid', lw=2, label=ref_names[i_ref])
    else:
      ax1.plot(Xc, Hp_bot_ov, color=clr, linestyle='solid', lw=2)

    # Second panel: radial extent of the overshooting zone
    if ref_names:
      ax2.plot(Xc, M_cc, color=clr, linestyle='solid', lw=2, label=ref_names[i_ref])
    else:
      ax2.plot(Xc, M_cc, color=clr, linestyle='solid', lw=2)

    # Third panel: Integral of D_ov over 1/Hp, in cm/sec
    if ref_names:
      ax3.plot(Xc, m_ov, color=clr, linestyle='solid', lw=2, label=ref_names[i_ref])
    else:
      ax3.plot(Xc, m_ov, color=clr, linestyle='solid', lw=2)

    # Forth panel: Local pressure scale height at the edge of convective core in Rsun
    if ref_names:
      ax4.plot(Xc, r_ov, color=clr, linestyle='solid', lw=2, label=ref_names[i_ref])
    else:
      ax4.plot(Xc, r_ov, color=clr, linestyle='solid', lw=2)

    # Fifth panel: The Mass of the convective core
    #if ref_names:
      #ax5.plot(Xc, int_D_ov, color=clr, linestyle='solid', lw=2, label=ref_names[i_ref])
    #else:
      #ax5.plot(Xc, int_D_ov, color=clr, linestyle='solid', lw=2)


  # Then, overplot with the comparison tracks
  for i_comp, dic in enumerate(comp_tracks):
    hist = dic['hist']
    Xc = hist['center_h1']
    m_bot_ov = hist['m_bot_ov'] # gr
    m_top_ov = hist['m_top_ov'] # gr
    m_ov     = (m_top_ov - m_bot_ov) / Msun
    m_ov     = smooth_1d(m_ov, m=5)
    r_bot_ov = hist['r_bot_ov'] # cm
    r_top_ov = hist['r_top_ov'] # cm
    r_ov     = (r_top_ov - r_bot_ov) / Rsun
    r_ov     = smooth_1d(r_ov, m=5)
    Hp_bot_ov= hist['Hp_bot_ov'] / Rsun
    int_D_ov = hist['int_D_ov'] # cm/sec
    M_cc     = hist['mass_conv_core']

    # First panel: Mass budget of the overshooting zone
    clr = comp_color.to_rgba(i_comp)
    if comp_names:
      ax1.plot(Xc, Hp_bot_ov, color=clr, linestyle='dotted', lw=2, label=comp_names[i_comp])
    else:
      ax1.plot(Xc, Hp_bot_ov, color=clr, linestyle='dotted', lw=2)

    # Second panel: radial extent of the overshooting zone
    if comp_names:
      ax2.plot(Xc, M_cc, color=clr, linestyle='dotted', lw=2, label=comp_names[i_comp])
    else:
      ax2.plot(Xc, M_cc, color=clr, linestyle='dotted', lw=2)

    # Third panel: Integral of D_ov over 1/Hp, in cm/sec
    if comp_names:
      ax3.plot(Xc, m_ov, color=clr, linestyle='dashed', lw=2, label=comp_names[i_comp])
    else:
      ax3.plot(Xc, m_ov, color=clr, linestyle='dashed', lw=2)

    # Forth panel: Local pressure scale height at the edge of convective core in Rsun
    if comp_names:
      ax4.plot(Xc, r_ov, color=clr, linestyle='dashed', lw=2, label=comp_names[i_comp])
    else:
      ax4.plot(Xc, r_ov, color=clr, linestyle='dashed', lw=2)

    # Fifth panel: The Mass of the convective core
    #if ref_names:
      #ax5.plot(Xc, int_D_ov, color=clr, linestyle='dotted', lw=2, label=comp_names[i_comp])
    #else:
      #ax5.plot(Xc, int_D_ov, color=clr, linestyle='dotted', lw=2)

  #--------------------------------
  # Annotations
  #--------------------------------
  ax1.set_xlabel('$X_c$')
  ax1.set_xlim(0.7, 0)
  ax1.set_ylabel(r'$H_p$ at Bottom of Overshoot Zone\, [$R_\odot$]')
  ax1.set_ylim(0.12, 0.23)

  ax2.set_xlabel('$X_c$')
  ax2.set_xlim(0.7, 0)
  ax2.set_ylabel(r'Mass of Convective Core\, $M_{\rm cc}$ [$M_\odot$]')
  ax2.set_ylim(0.30, 1.0)

  ax3.set_xlabel('$X_c$')
  ax3.set_ylabel(r'Mass of Overshoot Zone\,  $M_{\rm ov}$ [$M_\odot$]')
  ax3.set_xlim(0.7, 0)

  ax4.set_xlabel(r'$X_c$')
  ax4.set_ylabel(r'Size of Overshoot Zone\,  $d_{\rm ov}$ [$R_\odot$]')
  ax4.set_xlim(0.7, 0)

  #ax5.set_xlabel(r'Center Hydrogen $X_c$')
  #ax5.set_xlim(0.7, 0)
  #ax5.set_ylabel(r'Overshoot Mean Velocity [cm sec$^{-1}$]')

  annot_text = ['(a)', '(b)', '(c)', '(d)']
  annot_loc  = [ax1, ax2, ax3, ax4]
  for i, panel in enumerate(annot_loc): panel.annotate(annot_text[i], xy=(0.85, 0.90), xycoords='axes fraction',
                                                       fontsize='x-large')
  if True:
    ax3.annotate(r'$f_{\rm ov}=0.030$', xy=(0.4, 1.03), rotation=-40, color='black', fontsize='x-small')
    ax3.annotate(r'$f_{\rm ov}=0.015$', xy=(0.4, 0.51), rotation=-25, color='black', fontsize='x-small')
    ax3.annotate(r'$f_{\rm ov}=0.003$', xy=(0.4, 0.13), rotation=-06, color='black', fontsize='x-small')

    ax4.annotate(r'$f_{\rm ov}=0.030$', xy=(0.4, 0.151), rotation=-30, color='black', fontsize='x-small')
    ax4.annotate(r'$f_{\rm ov}=0.015$', xy=(0.4, 0.077), rotation=-15, color='black', fontsize='x-small')
    ax4.annotate(r'$f_{\rm ov}=0.003$', xy=(0.4, 0.018), rotation=-05, color='black', fontsize='x-small')

  if file_out:
    plt.savefig(file_out, transparent=True, dpi=200)
    print ' - plot_evol: plot_evol_overshoot_properties: %s created.' % (file_out, )
    plt.close()

  return 0

#=================================================================================================================
def prof_col_mesh_wire(list_dic_prof, xaxis=None, yaxis=None, zaxis=None,
                       add_x_contour=False, add_y_contour=False, add_z_contour=False,
                       use_max_yaxis_as_interp_ref=True, xaxis_from=None, xaxis_to=None,
                       yaxis_from=None, yaxis_to=None, zaxis_from=None, zaxis_to=None,
                       xtitle=None, ytitle=None, ztitle=None, file_out=None):
  """
  This routine creates a 3D mesh wire representation of a column (specified by zaxis) which has an interior profile
  (specified by yaxis) and shows how it evolves with timestep or time or step number (specified by xaxis).
  Due to the changing mesh during MESA runtime, the mesh of the first model is used throughout the whole profile list.

  This has an added value when, e.g. mass is used on yaxis, and mass loss is turned on or off; in all cases, the interpolation
  works. However, it would not work if yaxis='radius', because the radius will increase over the course of evolution,
  and interpolation is not meaningful.
  @param list_dic_prof: list of input profiles
  @type list_dic_prof: list of dictionaries
  @param xaxis, yaxis, zaxis: valid profile column names.
      The xaxis will specify the evolution, so 'star_age' or 'model_number' are two reasonable options for this.
          Note: xaxis can only be among profile header items. Other possibilities are not supported yet.
      The yaxis will specify the interior coordinate, 'mass', 'logT', 'radius', etc. can be reasonable options Here.
      The zaxis is the column for which we are tracing its 3D evolution. It can be e.g. 'opacity', 'brunt_N2' etc.
  @type xaxis, yaxis, zaxis: string
  @param use_max_yaxis_as_interp_ref: Use max of yaxis as interpolation reference!
  """
  from mpl_toolkits.mplot3d import axes3d
  from matplotlib import cm
  # from matplotlib.ticker import LinearLocator, FormatStrFormatter

  n_dic = len(list_dic_prof)
  if n_dic == 0:
    message = 'Error: plot_evol: prof_col_mesh_wire: Input list is empty'
    raise SystemExit, message

  # check coordinate availability
  if type(list_dic_prof[0]['header']) is type({}):
    header_dict  = True
    header_names = list_dic_prof[0]['header'].keys()
  else:
    header_dict  = False
    header_names = list_dic_prof[0]['header'].dtype.names
  if xaxis not in header_names:
    message = 'Error: plot_evol: prof_col_mesh_wire: {0} not among header list'.format(xaxis)
    raise SystemExit, message

  prof_names = list_dic_prof[0]['prof'].dtype.names
  required_keys = [yaxis, zaxis]
  dic_avail_keys = {}
  for name in prof_names: dic_avail_keys[name] = 0.0
  dic_keys = read.check_key_exists(dic_avail_keys, required_keys)
  for key in required_keys:
    if not dic_keys[key]:
      message = 'Error: plot_evol: prof_col_mesh_wire: {0} is missing from profiles'.format(key)
      raise SystemExit, message

  # Find the reference profile for interpolation of yaxis and zaxis
  if not use_max_yaxis_as_interp_ref:
    dic_ref = list_dic_prof[0]
  else:
    # should iterate over all dictionaries to find where the maximum of yaxis occurs.
    list_n_yaxis = np.zeros(n_dic)
    for i_dic, dic in enumerate(list_dic_prof): list_n_yaxis[i_dic] = len(dic['prof'][yaxis])
    max_n_yaxis = max(list_n_yaxis)
    ind_ref   = np.where((list_n_yaxis == max_n_yaxis))[0]
    print ' - plot_evol: prof_col_mesh_wire: Reference profile index = {0}'.format(ind_ref)
    dic_ref = list_dic_prof[ind_ref]

  # first_dic = list_dic_prof[0]
  # first_prof= first_dic['prof']
  # first_yaxis = first_prof[yaxis]

  ref_yaxis = dic_ref['prof'][yaxis]
  n_ref_mesh= len(ref_yaxis)
  x_mtrx    = np.zeros((n_dic, n_ref_mesh))
  y_mtrx    = np.zeros((n_dic, n_ref_mesh))
  z_mtrx    = np.zeros((n_dic, n_ref_mesh))

  #--------------------------------
  # Loop over profiles, interpolate,
  # and store them in a 2D matrix
  #--------------------------------
  for i_dic, dic in enumerate(list_dic_prof):
    header  = dic['header']
    prof    = dic['prof']
    if header_dict:
      xval  = header[xaxis]
    else:
      xval    = header[xaxis][0]
    yval    = prof[yaxis]         # not interpolated yet
    zval    = prof[zaxis]
    nz      = len(zval)

    y_interp, z_interp = interpolate_array(n_ref_mesh, yval, zval)

    x_mtrx[i_dic, : ]  = xval
    y_mtrx[i_dic, : ]  = y_interp[:]
    z_mtrx[i_dic, : ]  = z_interp[:]

  #--------------------------------
  # Prepare the plot, do it then ...
  #--------------------------------
  fig = plt.figure(figsize=(9,6), dpi=200)
  ax = fig.add_subplot(111, projection='3d')
  plt.subplots_adjust(left=0.02, right=0.90, bottom=0.05, top=0.98)

  surf = ax.plot_surface(x_mtrx, y_mtrx, z_mtrx, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=True)

  #--------------------------------
  # Annotations, Ranges, etc.
  #--------------------------------
  if not xaxis_from: xaxis_from = np.min(x_mtrx*0.90)
  if not xaxis_to:   xaxis_to   = np.max(x_mtrx*1.10)
  if not yaxis_from: yaxis_from = np.min(y_mtrx*0.90)
  if not yaxis_to:   yaxis_to   = np.max(y_mtrx*1.10)
  if not zaxis_from: zaxis_from = np.min(z_mtrx*0.90)
  if not zaxis_to:   zaxis_to   = np.max(z_mtrx*1.10)
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)
  ax.set_zlim(zaxis_from, zaxis_to)

  if xtitle: ax.set_xlabel(xtitle)
  if ytitle: ax.set_ylabel(ytitle)
  if ztitle: ax.set_zlabel(ztitle)

  #--------------------------------
  # Add Contour projections
  #--------------------------------
  if add_x_contour:
    cset = ax.contourf(x_mtrx, y_mtrx, z_mtrx, zdir='x', offset=np.min(xaxis_from), cmap=cm.coolwarm)
  if add_y_contour:
    cset = ax.contourf(x_mtrx, y_mtrx, z_mtrx, zdir='y', offset=np.min(yaxis_to), cmap=cm.coolwarm, alpha=0.75)
  if add_z_contour:
    cset = ax.contourf(x_mtrx, y_mtrx, z_mtrx, zdir='z', offset=np.min(zaxis_from), cmap=cm.coolwarm, alpha=0.75)

  # fig.colorbar(surf, shrink=0.5, aspect=5)


  #--------------------------------
  # Finalize the plot
  #--------------------------------
  if file_out:
    plt.savefig(file_out, transparent=True, dpi=200)
    print ' - plot_evol: prof_col_mesh_wire: saved {0}'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def interpolate_array(n, x, y):
  """
  Interpolate the x and y array over the new x mesh of length n.
  @param n: the length of the output mesh in x
  @type n: integer
  @param x, y: the original x and y axis before interpolation
  @type x, y:  numpy ndarray
  @return: new x and y axis after interpolation
  @rtype: tuple
  """
  from scipy.interpolate import interp1d
  
  do_reverse = x[0] > x[-1]
  if do_reverse:
    x = x[::-1]
    y = y[::-1]
  new_x  = np.linspace(np.min(x), np.max(x), num=n, endpoint=True)
  interp = interp1d(x, y, kind='zero')
  new_y  = interp(new_x)

  return new_x, new_y

#=================================================================================================================
def interpolate_on_same_Xc(ref_x, ref_y, otr_x, otr_y):
  """
  Interpolate two curves on the same x-axis mesh, so that we can compute differences
  of both curves on the same (commmon) x-axis points
       Delta(x_i) = f_other(x_i) - f_ref(x_i),
  where f_other and f_ref are alerady defined on two different mesh points, x_other 
  and x_ref. 
  @param ref_x, ref_y, otr_x, otr_y: arrays containing the x- and y-axis data for the 
         arrays that are going to be interpolated on a common axis.
  @type ref_x, ref_y, otr_x, otr_y: numpy ndarray
  @return: the following arrays:
         - the new common x-axis mesh points
         - the interpolated array for the reference curve
         - the interpolated array for the other curve 
  @rtype: tuple of ndarray
  """
  from scipy.interpolate import interp1d

  do_reverse = ref_x[0] > ref_x[-1]
  if do_reverse:
    ref_x    = ref_x[::-1]
    ref_y    = ref_y[::-1]
    otr_x    = otr_x[::-1]
    otr_y    = otr_y[::-1]

  x_from     = max((ref_x[0], otr_x[0]))
  x_to       = min((ref_x[-1], otr_x[-1]))
  x_new      = np.linspace(x_from, x_to, num=1001, endpoint=True)

  ref_intrp  = interp1d(ref_x, ref_y, kind='zero', assume_sorted=True)
  otr_intrp  = interp1d(otr_x, otr_y, kind='zero', assume_sorted=True)
  ref_result = ref_intrp(x_new)
  otr_result = otr_intrp(x_new)

  if do_reverse:
    x_new    = x_new[::-1]
    ref_result = ref_result[::-1]
    otr_result = otr_result[::-1]

  return x_new, ref_result, otr_result

#=================================================================================================================
def compare_Teff_logg_R_age_Mcc_vs_Xc(list_dic_hist, ref=0, list_lbls=[], xaxis_from=0.72, xaxis_to=0, 
                 file_out=None):
  """
  To compare surface and core features of different tracks during the evolutin, use this routine. This is 
  suitable, e.g. when comparing different tracks with different assumed mixture or input physics.
  @param list_dic_hist: list of history data, one per each track 
  @type list_dic_hist: list of dictionaries
  @param ref: which of the tracks shall be considered as the reference track?
  @type ref: integer 
  @param xaxis_from, xaxis_to: the range of Xc to plot on the xaxis 
  @type xaxis_from, xaxis_to: float
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  if file_out is None: return
  n_hist = len(list_dic_hist)
  if ref > n_hist:
    raise SystemExit, 'Error: plot_evol: compare_Teff_logg_R_age_Mcc_vs_Xc: ref cannot be larger than the number of input tracks'

  avail_keys    = list_dic_hist[0].keys()
  required_keys = ['star_mass', 'star_age', 'mass_conv_core', 'log_Teff', 'log_L', 'log_R', 'log_g', 'center_h1']
  dic_req_keys  = {}
  for name in required_keys: dic_req_keys[name] = 0.0
  dic_keys = read.check_key_exists(dic_req_keys, required_keys)
  for key in required_keys:
    if not dic_keys[key]:
      raise SystemExit, 'Error: plot_evol: compare_Teff_logg_R_age_Mcc_vs_Xc: {0} not available!'.format(key)

  #--------------------------------
  # prepare the plot
  #--------------------------------
  x_size_cm  = 8.5  # cm, MNRAS column width
  y_size_cm  = 15   # cm
  fig, axis_tuple = plt.subplots(6, figsize=(x_size_cm/2.54, y_size_cm/2.54), dpi=300)
  (axa, axb, axc, axd, axe, axf) = axis_tuple
  plt.subplots_adjust(left=0.18, right=0.98, bottom=0.075, top=0.98, wspace=0.04)
  
  lsts = itertools.cycle(['dashed', 'dashdot', 'dotted'])
  clrs = itertools.cycle(['blue', 'red', 'grey'])

  #--------------------------------
  # Loop over history files and plot them
  #--------------------------------
  for i_dic, dic in enumerate(list_dic_hist):
    header    = dic['header']
    hist      = dic['hist']

    log_g     = hist['log_g']
    mxg       = np.argmax(log_g)
    log_g     = log_g[mxg:]

    log_Teff  = hist['log_Teff'][mxg:]
    Teff      = np.power(10, log_Teff)
    log_L     = hist['log_L'][mxg:]
    lum       = np.power(10, log_L)
    log_R     = hist['log_R'][mxg:]
    radius    = np.power(10, log_R)
    star_age  = hist['star_age'][mxg:]
    star_mass = hist['star_mass'][mxg:]
    Mcc       = hist['mass_conv_core'][mxg:]
    Xc        = hist['center_h1'][mxg:]

    age_factor = 1e6
    if np.max(star_age>1e8): age_factor = 1e9
    star_age  = star_age / age_factor

    if i_dic  == ref:
      ls      = 'solid'
      col     = 'black'
    else:
      ls      = lsts.next()
      col     = clrs.next()

    axa.plot(Xc, log_Teff, lw=1, linestyle=ls, color=col)
    axb.plot(Xc, log_g, lw=1, linestyle=ls, color=col)
    axc.plot(Xc, radius, lw=1, linestyle=ls, color=col)
    axd.plot(Xc, log_L, lw=1, linestyle=ls, color=col)
    axe.plot(Xc, star_age, lw=1, linestyle=ls, color=col)
    axf.plot(Xc, Mcc, lw=1, linestyle=ls, color=col)

  # plt.locator_params(axis='y', nbins=5)
  for i_ax, ax in enumerate(axis_tuple):
    ax.set_xlim(xaxis_from, xaxis_to)
    ax.locator_params(axis='y', nbins=5)
    if i_ax == len(axis_tuple)-1: continue
    ax.set_xticklabels(())
  axf.set_xlabel(r'Center Hydrogen X$_{\rm c}$')

  axa.set_ylabel(r'$\log T_{\rm eff}$ [K]', fontsize=10)
  axb.set_ylabel(r'$\log g$ [cgs]', fontsize=10)
  axc.set_ylabel(r'Radius [R$_\odot$]', fontsize=10)
  axd.set_ylabel(r'$\log(L/L_\odot)$', fontsize=10)
  axe.set_ylabel(r'Age [Myr]', fontsize=10)
  axf.set_ylabel(r'$M_{\rm core}$ [M$_\odot$]', fontsize=10)

  # Legend
  lsts = itertools.cycle(['dashed', 'dashdot', 'dotted'])
  clrs = itertools.cycle(['blue', 'red', 'grey'])
  for i_hist in range(n_hist):
    if not list_lbls: break
    if i_hist == ref:
      ls      = 'solid'
      col     = 'black'
    else:
      ls      = lsts.next()
      col     = clrs.next()
    axa.plot([], [], lw=1, linestyle=ls, color=col, label=list_lbls[i_hist])
  leg_a = axa.legend(loc=3, fontsize=9, fancybox=False, frameon=False)
  # leg_a.set_frame_on(False)

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  plt.savefig(file_out, transparent=True)
  print ' - plot_evol: compare_Teff_logg_R_age_Mcc_vs_Xc: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def compare_diff_Teff_logg_R_age_Mcc_vs_Xc(list_dic_hist, ref=0, relative_diff=False, list_lbls=[], 
                 xaxis_from=0.72, xaxis_to=0, file_out=None):
  """
  To compare surface and core features of different tracks during the evolutin, use this routine. This is 
  suitable, e.g. when comparing different tracks with different assumed mixture or input physics.
  @param list_dic_hist: list of history data, one per each track 
  @type list_dic_hist: list of dictionaries
  @param ref: which of the tracks shall be considered as the reference track?
  @type ref: integer 
  @param relative_diff: flag to decide whether or not to show the relative differences or the absolute 
         differences
  @type relative_diff: boolean
  @param xaxis_from, xaxis_to: the range of Xc to plot on the xaxis 
  @type xaxis_from, xaxis_to: float
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  filter_avail = True
  try:
    from scipy.signal import savgol_filter as savitzky_golay
  except:
    filter_avail = False
  if file_out is None: return
  n_hist = len(list_dic_hist)
  if ref > n_hist:
    raise SystemExit, 'Error: plot_evol: compare_diff_Teff_logg_R_age_Mcc_vs_Xc: ref cannot be larger than the number of input tracks'

  avail_keys    = list_dic_hist[0].keys()
  required_keys = ['star_mass', 'star_age', 'mass_conv_core', 'log_Teff', 'log_L', 'log_R', 'log_g', 'center_h1']
  dic_req_keys  = {}
  for name in required_keys: dic_req_keys[name] = 0.0
  dic_keys = read.check_key_exists(dic_req_keys, required_keys)
  for key in required_keys:
    if not dic_keys[key]:
      raise SystemExit, 'Error: plot_evol: compare_diff_Teff_logg_R_age_Mcc_vs_Xc: {0} not available!'.format(key)

  #--------------------------------
  # prepare the plot
  #--------------------------------
  x_size_cm  = 8.5  # cm, MNRAS column width
  y_size_cm  = 15   # cm
  fig, axis_tuple = plt.subplots(6, figsize=(x_size_cm/2.54, y_size_cm/2.54), dpi=300)
  (axa, axb, axc, axd, axe, axf) = axis_tuple
  plt.subplots_adjust(left=0.21, right=0.98, bottom=0.075, top=0.98, hspace=0.15)
  
  lsts = itertools.cycle(['solid', 'dashed', 'dashdot', 'dotted'])
  clrs = itertools.cycle(['black', 'blue', 'red', 'grey'])

  #--------------------------------
  # Loop over history files and plot them
  #--------------------------------
  ref_dic     = list_dic_hist[ref]
  ref_hist    = ref_dic['hist']
  ref_log_g   = ref_hist['log_g']
  ref_mxg     = np.argmax(ref_log_g)
  ref_log_g   = ref_log_g[ref_mxg:]
  ref_len     = len(ref_log_g)
  ref_log_Teff= ref_hist['log_Teff'][ref_mxg:]
  ref_Teff    = np.power(10, ref_log_Teff)
  ref_log_L   = ref_hist['log_L'][ref_mxg:]
  ref_L       = np.power(10, ref_log_L)
  ref_log_R   = ref_hist['log_R'][ref_mxg:]
  ref_R       = np.power(10, ref_log_R)
  ref_age     = ref_hist['star_age'][ref_mxg:]
  ref_mass    = ref_hist['star_mass'][ref_mxg:]
  ref_Mcc     = ref_hist['mass_conv_core'][ref_mxg:]
  ref_Xc      = ref_hist['center_h1'][ref_mxg:]

  age_factor  = 1e6
  if np.max(ref_age>1e8): age_factor = 1e9
  ref_age     = ref_age / age_factor

  # To store other (hence, otr_) histories than the reference
  otr_log_g   = []
  otr_log_Teff= []
  otr_Teff    = []
  otr_log_L   = []
  otr_L       = []
  otr_log_R   = []
  otr_R       = []
  otr_age     = []
  otr_mass    = []
  otr_Mcc     = []
  otr_Xc      = []

  for i_dic, dic in enumerate(list_dic_hist):
    if i_dic == ref: continue
    header    = dic['header']
    hist      = dic['hist']

    log_g     = hist['log_g']
    mxg       = np.argmax(log_g)
    log_g     = log_g[mxg:]

    log_Teff  = hist['log_Teff'][mxg:]
    Teff      = np.power(10, log_Teff)
    log_L     = hist['log_L'][mxg:]
    lum       = np.power(10, log_L)
    log_R     = hist['log_R'][mxg:]
    radius    = np.power(10, log_R)
    star_age  = hist['star_age'][mxg:]
    star_mass = hist['star_mass'][mxg:]
    Mcc       = hist['mass_conv_core'][mxg:]
    Xc        = hist['center_h1'][mxg:]

    age_factor = 1e6
    if np.max(star_age>1e8): age_factor = 1e9
    star_age  = star_age / age_factor

    otr_log_g.append(log_g)
    otr_log_Teff.append(log_Teff)
    otr_Teff.append(Teff)
    otr_log_L.append(log_L)
    otr_L.append(lum)
    otr_log_R.append(log_R)
    otr_R.append(radius)
    otr_age.append(star_age)
    otr_mass.append(star_mass)
    otr_Mcc.append(Mcc)
    otr_Xc.append(Xc)


  # Compute the differences
  n_otr = len(otr_Xc)
  for i_otr in range(n_otr): 
    tup_Teff  = interpolate_on_same_Xc(ref_Xc, ref_Teff, otr_Xc[i_otr], otr_Teff[i_otr])
    tup_logg  = interpolate_on_same_Xc(ref_Xc, ref_log_g, otr_Xc[i_otr], otr_log_g[i_otr])
    tup_R     = interpolate_on_same_Xc(ref_Xc, ref_R, otr_Xc[i_otr], otr_R[i_otr])
    tup_L     = interpolate_on_same_Xc(ref_Xc, ref_L, otr_Xc[i_otr], otr_L[i_otr])
    tup_age   = interpolate_on_same_Xc(ref_Xc, ref_age, otr_Xc[i_otr], otr_age[i_otr])
    tup_Mcc   = interpolate_on_same_Xc(ref_Xc, ref_Mcc, otr_Xc[i_otr], otr_Mcc[i_otr])

    dif_Teff  = tup_Teff[2] - tup_Teff[1]
    dif_log_g = tup_logg[2] - tup_logg[1]
    dif_R     = tup_R[2]    - tup_R[1]
    dif_L     = tup_L[2]    - tup_L[1]
    dif_age   = (tup_age[2]  - tup_age[1]) 
    dif_Mcc   = tup_Mcc[2]  - tup_Mcc[1]

    if filter_avail:
      if relative_diff:
        soft_Teff = savitzky_golay(dif_Teff/tup_Teff[1]*100, 21, 3)
        soft_log_g= savitzky_golay(dif_log_g/tup_logg[1]*100, 21, 3)
        soft_R    = savitzky_golay(dif_R/tup_R[1]*100, 21, 3)
        soft_L    = savitzky_golay(dif_L/tup_L[1]*100, 21, 3)
        soft_age  = savitzky_golay(dif_age/tup_age[1]*100, 21, 3)
        soft_Mcc  = savitzky_golay(dif_Mcc/tup_Mcc[1]*100, 21, 3)
      else:
        soft_Teff = savitzky_golay(dif_Teff, 21, 3)
        soft_log_g= savitzky_golay(dif_log_g, 21, 3)
        soft_R    = savitzky_golay(dif_R, 21, 3)
        soft_L    = savitzky_golay(dif_L, 21, 3)
        soft_age  = savitzky_golay(dif_age, 21, 3)
        soft_Mcc  = savitzky_golay(dif_Mcc, 21, 3)
    else:
      if relative_diff:
          soft_Teff = dif_Teff/tup_Teff[1]*100
          soft_log_g= dif_log_g/tup_logg[1]*100
          soft_R    = dif_R/tup_R[1]*100
          soft_L    = dif_L/tup_L[1]*100
          soft_age  = dif_age/tup_age[1]*100
          soft_Mcc  = dif_Mcc/tup_Mcc[1]*100
      else:
          soft_Teff = dif_Teff
          soft_log_g= dif_log_g
          soft_R    = dif_R
          soft_L    = dif_L
          soft_age  = dif_age
          soft_Mcc  = dif_Mcc

    ls        = lsts.next()
    col       = clrs.next()

    axa.plot(tup_Teff[0], soft_Teff, lw=1.5, linestyle=ls, color=col)
    axb.plot(tup_logg[0], soft_log_g, lw=1.5, linestyle=ls, color=col)
    axc.plot(tup_R[0], soft_R, lw=1.5, linestyle=ls, color=col)
    axd.plot(tup_L[0], soft_L, lw=1.5, linestyle=ls, color=col)
    axe.plot(tup_age[0], soft_age, lw=1.5, linestyle=ls, color=col)
    axf.plot(tup_Mcc[0], soft_Mcc, lw=1.5, linestyle=ls, color=col)

  # plt.locator_params(axis='y', nbins=5)
  for i_ax, ax in enumerate(axis_tuple):
    ax.set_xlim(xaxis_from, xaxis_to)
    ax.locator_params(axis='y', nbins=5)
    if i_ax == len(axis_tuple)-1: continue
    ax.set_xticklabels(())
  axf.set_xlabel(r'Center Hydrogen X$_{\rm c}$')

  if relative_diff:
    axa.set_ylabel(r'$\delta (T_{\rm eff})\,[\%]$', fontsize=10)
    axb.set_ylabel(r'$\delta (\log g)\,[\%]$', fontsize=10)
    axc.set_ylabel(r'$\delta (R)$\,[\%]', fontsize=10)
    axd.set_ylabel(r'$\delta (L)$\,[\%]', fontsize=10)
    axe.set_ylabel(r'$\delta$ (Age)\,[\%]', fontsize=10)
    axf.set_ylabel(r'$\delta (M_{\rm core}$)\,[\%]', fontsize=10)
  else:
    axa.set_ylabel(r'$\Delta T_{\rm eff}$ [K]', fontsize=10)
    axb.set_ylabel(r'$\Delta\log g$ [cgs]', fontsize=10)
    axc.set_ylabel(r'$\Delta R$ [R$_\odot$]', fontsize=10)
    axd.set_ylabel(r'$\Delta L$ [L$_\odot$]', fontsize=10)
    axe.set_ylabel(r'$\Delta$Age [Myr]', fontsize=10)
    axf.set_ylabel(r'$\Delta M_{\rm core}$ [M$_\odot$]', fontsize=10)

  # Annotations
  left = 0.04; up = 0.80; dwn = 0.12
  axa.annotate('(a)', xy=(left, dwn), xycoords='axes fraction', fontsize=10)  
  axb.annotate('(b)', xy=(left, dwn), xycoords='axes fraction', fontsize=10)  
  axc.annotate('(c)', xy=(left, up), xycoords='axes fraction', fontsize=10)  
  axd.annotate('(d)', xy=(left, up), xycoords='axes fraction', fontsize=10)  
  axe.annotate('(e)', xy=(left, dwn), xycoords='axes fraction', fontsize=10)  
  axf.annotate('(f)', xy=(left, up), xycoords='axes fraction', fontsize=10)  

  # Legend
  lsts = itertools.cycle(['dashed', 'dashdot', 'dotted'])
  clrs = itertools.cycle(['blue', 'red', 'grey'])
  for i_hist in range(n_hist):
    if not list_lbls: break
    if i_hist == ref:
      ls      = 'solid'
      col     = 'black'
    else:
      ls      = lsts.next()
      col     = clrs.next()
    axa.plot([], [], lw=1, linestyle=ls, color=col, label=list_lbls[i_hist])
  # leg_a = axa.legend(loc=0, fontsize=9, fancybox=False)
  # leg_a.set_frame_on(False)

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  plt.savefig(file_out, transparent=True)
  print ' - plot_evol: compare_diff_Teff_logg_R_age_Mcc_vs_Xc: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def Mdot_Mini_Mfinal(dics, log=False, file_out=None):
  """
  Compare the final mass, the lost mass by wind, and the maximum value for mass loss rate (during the evolution),
  versus initial mass (abscissa). You can choose to present the abscissa and ordinate in log-scale, by setting
  log=True.
  """
  n_dics = len(dics)
  M_ini  = np.zeros(n_dics)
  M_fin  = np.zeros(n_dics)
  M_lost = np.zeros(n_dics)
  max_lgMdot = np.zeros(n_dics)

  for k, dic in enumerate(dics):
    filename = dic['filename']
    header   = dic['header']
    hist     = dic['hist']
    Mstar    = hist['star_mass']
    lgMdot   = hist['log_abs_mdot']

    M_ini[k] = header['initial_mass'][0]
    M_fin[k] = Mstar[-1]
    M_lost[k]= M_ini[k] - M_fin[k]
    max_lgMdot[k] = np.max(lgMdot)

  ################
  # Create the figure
  ################
  fig, (top, mid, bot) = plt.subplots(3, figsize=(5, 8), sharex=True)
  plt.subplots_adjust(bottom=0.06)

  xlbl    = r'Initial Mass $(M_{\rm ini}/M_\odot)$'
  ylbl1   = r'Final Mass $(M_{\rm fin}/M_\odot)$'
  ylbl2   = r'Lost Mass $(M_{\rm lost}/M_\odot)$'
  ylbl3   = r'Max. Log Mass Loss max($\log(\dot{M})$)'
  if log: 
    M_ini = np.log10(M_ini)
    M_fin = np.log10(M_fin)
    xlbl  = r'Logarithm Initial Mass $\log(M_{\rm ini}/M_\odot)$'
    ylbl1 = r'Logarithm Final Mass $\log(M_{\rm fin}/M_\odot)$'
    ylbl2 = r'Logarithm Lost Mass $\log(M_{\rm lost}/M_\odot)$'

  top.scatter(M_ini, M_fin, marker='o', color='k', s=5)
  mid.scatter(M_ini, M_lost, marker='s', color='b', s=5)
  bot.scatter(M_ini, max_lgMdot, marker='x', color='r', s=5)

  bot.set_xlabel(xlbl)
  top.set_ylabel(ylbl1)
  mid.set_ylabel(ylbl2)
  bot.set_ylabel(ylbl3)

  plt.savefig(file_out, transparent=True)
  print ' - saved {0}'.format(file_out)
  plt.close()

  return True

#=================================================================================================================
def show_fov_Dmix_matrix(dics, age, field, use_Xc=True,, use_Yc=False, 
                         lo=None, hi=None, zfactor=1.0, zlog=False, file_out=None):
  """
  call gen_fov_Dmix_matrix(), and show the result with imshow()
  If the 3rd axis, z, will be always multiplied by the zfactor (e.g. useful to show Giga year, instead of year),
  and if zlog is set to True, the log10() of the z-values will be taken afterwards.
  """
  if file_out is None: return 
  list_fov, list_logD, matrix = gen_fov_Dmix_matrix(dics=dics, age=age, field=field, use_Xc=use_Xc, use_Yc=use_Yc)
  n_fov   = len(list_fov)
  n_logD  = len(list_logD)

  ###########################
  # Adjust the matrix values
  ###########################
  matrix   *= zfactor
  if zlog:
    matrix = np.log10(matrix)

  dx       = list_logD[1] - list_logD[0]
  dy       = list_fov[1] - list_fov[0]

  ###########################
  # Create the figure
  ###########################
  fig, ax = plt.subplots(1, figsize=(5, 5))
  plt.subplots_adjust(left=0.145, right=0.98, bottom=0.09, top=0.90)

  if lo is None: lo = np.min(matrix)
  if hi is None: hi = np.max(matrix)

  image   = ax.imshow(matrix, cmap='gnuplot2', interpolation='none', vmin=np.min(matrix), 
                      vmax=np.max(matrix), origin='lower', aspect='auto',
                      extent=(np.min(list_logD)-dx/2., np.max(list_logD)+dx/2., 
                              np.min(list_fov)-dy/2., np.max(list_fov)+dy/2.) 
                      )

  ###########################
  # Cosmetics
  ###########################
  xlo = np.min(list_logD)
  xhi = np.max(list_logD)
  ylo = np.min(list_fov)
  yhi = np.max(list_fov)
  ax.set_xlim(xlo-dx/2., xhi+dx/2.)
  ax.set_ylim(ylo-dy/2., yhi+dy/2.)
  ax.set_xlabel(r'$\log D_{\rm mix}$ [cm$^2$\,sec$^{-1}$]')
  ax.set_ylabel(r'Overshoot $f_{\rm ov}$')

# # Set the major ticks at the centers and minor tick at the edges
  xlbls = ['{0:.2f}'.format(val) for val in list_logD]
  ylbls = ['{0:.3f}'.format(val) for val in list_fov]

  plt.xticks(list_logD[::2], xlbls[::2])
  plt.yticks(list_fov[::2], ylbls[::2])

  ###########################
  # Add color bar, after modifying
  # tick labels above!
  ###########################
  pos = ax.get_position()
  cax = fig.add_axes([pos.x0, pos.y1+0.01, pos.x1-pos.x0, 0.995-pos.y1-0.05])
  tks = np.linspace(lo, hi, 5, endpoint=True)
  B   = fig.colorbar(image, cax=cax, ticks=tks, orientation='horizontal')
  cax.xaxis.set_ticks_position('top')
  B.ax.set_xticklabels( [ '{0:.2f}'.format(val) for val in tks ], fontsize=8)


  ###########################
  # Save the figure
  ###########################
  plt.savefig(file_out, transparent=True)
  print ' - plotter: show_fov_Dmix_matrix: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def gen_fov_Dmix_matrix(dics, age, field, use_Xc=True, use_Yc=False):
  """
  Generate a 2D matrix (plane), with fov on the x-axis (i.e. rows), and Dmix on the y-axis (i.e. columns).
  Then, values are picked at the closest point to the selected Xc, e.g. Xc=0.01 (near TAMS), and are
  collected for a given field (column) in the hist values, e.g. field='center_he4', 'star_age', etc.
  The fov and log_Dmix values are retrieved from filenames
  The return value can be perfectly visualized with pylab.imshow()
  Note, the initial mass and metalicity must be identical for all tracks. Otherwise, you're not comparing
  apples with apples.

  For non-standard (yet, very interesting) "filed" options, read the code.
  """
  if all([use_Xc, use_Yc]) or not all([use_Xc, use_Yc]):
    raise SystemExit, 'Error: plot_evol.gen_fov_Dmix_matrix(): use either Xc or Yc to specify age.'

  n_dics   = len(dics)
  files    = [dic['filename'] for dic in dics]
  fields   = dics[0]['hist'].dtype.names

  if field == 'n/c':
    pass
  elif field == 'n/o':
    pass 
  elif field not in fields:
    raise SystemExit, 'gen_fov_Dmix_matrix: "{0}" not among history columns! Check again ...'.format(field)


  list_M   = []
  # list_fov = []
  list_Z   = []
  # list_logD= []

  set_fov  = set()
  set_logD = set()

  for k, f in enumerate(files):
    M, fov, Z, logD = get_params(f)
    list_M.append(M)
    set_fov.add(fov)
    list_Z.append(Z)
    set_logD.add(logD)

  list_M   = np.array(list_M)
  list_fov = np.sort(np.array(list(set_fov)))
  list_Z   = np.array(list_Z)
  list_logD= np.sort(np.array(list(set_logD)))

  if min(list_M) != max(list_M):
    raise SystemExit, 'gen_fov_Dmix_matrix: All tracks must have identical mass! Check again ...'
  if min(list_Z) != max(list_Z):
    raise SystemExit, 'gen_fov_Dmix_matrix: All tracks must have identical Z! Check again ...'

  n_fov    = len(list_fov)
  n_logD   = len(list_logD)
  if n_dics != n_fov * n_logD:
    raise SystemExit, 'gen_fov_Dmix_matrix: Something is wrong with the number of tracks! Check again ...'

  matrix   = np.zeros((n_fov, n_logD))
  for i, dic in enumerate(dics):
    f      = dic['filename']
    M, fov, Z, logD = get_params(f)
    hist   = dic['hist']
    Xc     = hist['center_h1']
    Yc     = hist['center_he4']

    if use_Xc:
      ind  = np.argmin(np.abs(Xc - age))
    elif use_Yc:
      ind  = np.argmin(np.abs(Yc - age))

    if field == 'n/c':
      c12  = hist['surface_c12']
      n14  = hist['surface_n14']
      div  = np.log10(12 * n14 / (14 * c12))
    elif field == 'n/o':
      n14  = hist['surface_n14']
      o16  = hist['surface_o16']
      div  = np.log10(16 * n14 / (14 * o16))
    else:
      div  = hist[field]
    val    = div[ind]

    ind_f  = np.argmin(np.abs(list_fov - fov))
    ind_logD = np.argmin(np.abs(list_logD - logD))
    matrix[ind_f, ind_logD] = val

  print ' - gen_fov_Dmix_matrix: {0} lies between {1:.4f}, and {2:.4f}'.format(field, np.min(matrix), np.max(matrix))

  return (list_fov, list_logD, matrix)

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================

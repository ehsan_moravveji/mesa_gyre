
import sys, os, glob, shutil, copy
import logging
import numpy as np 
import commons, read, tar
from plot_gyre import get_gradr

Msun = 1.9892e33    # g
Rsun = 6.9598e10    # cm
Lsun = 3.8418e33    # cgs
G    = 6.67428e-8   # cm^3 g^-1 s^-2
G_grav  = G

dbg = False

#=========================================================================================
def get_mass(dic):
  """
  Reconstruct the array of mass coordinates in the model based on the column "q_div_xq" in 
  the input model.
  @param dic: dictionary containing gyre input file information
  @type dic: dictionary
  @return: an array of mass, m, in grams, with the size equivalent to the number of rows in 
         the input GYRE file
  @rtype: ndarray
  """
  M_star  = dic['M_star']
  gyre_in = dic['gyre_in']
  q       = gyre_in['q_div_xq']
  mass    = q * M_star / (1 + q)

  if mass[-1] != M_star:
    if dbg: print ' From get_mass: ', mass[-1], M_star
    raise SystemExit, 'Error: tar_diff_rot: get_mass: Inconsistent model mass!'

  return mass

#=========================================================================================
def get_dm(dic):
  """
  return an array of mass increments, dm, from the core to the surface of the model.
  @param dic: dictionary containing gyre input file information
  @type dic: dictionary
  @return: an array of dm, with size equivalent to the number of rows in the input GYRE file
  @rtype: ndarray
  """
  M_star = dic['M_star']
  mass = get_mass(dic)
  n    = len(mass)
  dm   = np.zeros(n)
  dm[0]  = mass[0]
  dm[1:] = mass[1:] - mass[:-1]
  if np.sum(dm) != M_star:
    if dbg: print ' From get_dm: ', np.sum(dm), M_star, mass[0], mass[1]
    raise SystemExit, 'Error: tar_diff_rot: get_dm: Inconsistent total masses'

  return dm

#=========================================================================================
def get_dJ(dic, user_Omega=None):
  """
  In one-dimensional form, the angular momentum increment, dJ, in physical cgs units is 
                  dJ = (r sin theta)^2 x Omega * dm,
  where theta = 90 degrees, r, Omega and dm are local radius (cm), angular rotation frequency
  (in rad/sec), and mass increment (gr), respectively.
  @param dic: dictionary containing gyre input file information
  @type dic: dictionary
  @param user_Omega: If this is None, we use the last column, "omega", in the gyre input info.
         Otherwise, the user can supply user_Omega, to override that. This provided rotation
         rate can either be a float (to signify solid body rotation across the entire model),
         or a numpy.ndarray for every point inside the model, from core to the surface. In the 
         latter case, the length of user_Omega must match that of "gyre_in" numpy.recarray.
         Note: user_Omega must have cgs units of radians / sec.
  @type user_Omega: None, float or ndarray
  @return: dJ, the array of local angular momentum increment, so that the total angular momentum
         is J = Sum(dJ)  
  @rtype: ndarray
  """
  dm      = get_dm(dic)
  gyre_in = dic['gyre_in']
  Omega   = gyre_in['omega']
  radius  = gyre_in['radius']
  if isinstance(user_Omega, type(None)):
    psss
  elif isinstance(user_Omega, float):
    if user_Omega < 0:
      raise SystemExit, 'Error: tar_diff_rot: get_dJ: supplied user_Omega cannot be negative!'
    Omega = user_Omega
  elif isinstance(user_Omega, np.ndarray):
    n = len(gyre_in)
    m = len(user_Omega)
    if n != m:
      raise SystemExit, 'Error: tar_diff_rot: get_dJ: length of supplied user_Omega is incompatible.'
    Omega = user_Omega

  dJ = np.power(radius, 2) * Omega * dm 

  return dJ

#=========================================================================================
def get_J(dic, user_Omega=None):
  """
  The total angular momentum, J, is the sum of the increments dJ, i.e. J = sum(dJ)
  This routine essentially calls get_dJ() function to get dJ, and them sums it up.
  @param dic: dictionary containing gyre input file information
  @type dic: dictionary
  @param user_Omega: If this is None, we use the last column, "omega", in the gyre input info.
         Otherwise, the user can supply user_Omega, to override that. This provided rotation
         rate can either be a float (to signify solid body rotation across the entire model),
         or a numpy.ndarray for every point inside the model, from core to the surface. In the 
         latter case, the length of user_Omega must match that of "gyre_in" numpy.recarray.
         Note: user_Omega must have cgs units of radians / sec.
  @type user_Omega: None, float or ndarray
  @return: Net angular momentum, J in cgs units (gr x cm^2 / sec).
  @rtype: float
  """
  dJ = get_dJ(dic=dic, user_Omega=user_Omega)

  return np.sum(dJ)

#=========================================================================================
def update_gyre_in_with_user_Omega(dic, user_Omega):
  """
  The input GYRE files prodcued by MESA contain the angular rotation frequency in the last
  column, called omega. For non-rotating models, omega is zero, and for rotating models,
  it can have a non-zero value with radial dependence.
  The user can override this by supplying "user_Omega" as a float or numpy.ndarray.
  This routine makes a deep copy of the input dictionary "dic", and substitutes "user_Omega"
  in place of "omega".
  @param dic: dictionary containing gyre input file information
  @type dic: dictionary
  @param user_Omega: If this is None, we use the last column, "omega", in the gyre input info.
         Otherwise, the user can supply user_Omega, to override that. This provided rotation
         rate can either be a float (to signify solid body rotation across the entire model),
         or a numpy.ndarray for every point inside the model, from core to the surface. In the 
         latter case, the length of user_Omega must match that of "gyre_in" numpy.recarray.
         Note: user_Omega must have cgs units of radians / sec.
  @type user_Omega: None, float or ndarray
  @return: a new copy of the input dictionary, with updated "omega" values with "user_Omega".
  @rtype: dictionary
  """
  if isinstance(user_Omega, type(None)): return dic 

  out = copy.deepcopy(dic)
  if isinstance(user_Omega, float):
    pass
  elif isinstance(user_Omega, np.ndarray):
    gyre_in = out['gyre_in']
    n       = len(gyre_in)
    m       = len(user_Omega)
    if n != m: 
      raise SystemExit, 'Error: tar_diff_rot: update_gyre_in_with_user_Omega: incompatible length for user_Omega.'

  out['gyre_in']['omega'] = user_Omega

  return out

#=========================================================================================

#   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#   Z O N I N G   F U N C T I O N S
#   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#   Function signatures: 
#        func(dic, **kwargs)

#=========================================================================================
def zoning_uniform(dic, **kwargs):
  """
  This function splits the whole star into N (i.e. n_zone) regions (zones), which are 
  uniformly distributed in mass, radius, logT etc. as the user specifies through the passed
  keyword arguments **kwargs
  Note: The uniform zoning can be done with different items, e.g. mass, radius, logT, etc.
        Therefore, this piece of information should be explicitly passed through **kwargs, 
        by specifying the field: by='mass', or by='logT', etc.
        An exception is raised if this is missing.
        The current supported options for "by" are the following: ['mass', 'radius', 'logT'],
        otherwise, we raise an exception.
  Note: The kwargs must contain two keywords:
        1. by: string. Specifies which variable to use for uniform zoning
        2. n_zones: integer. Specifies n_zones and n_zones+1 edges. It is the number of 
           desired zones (regions, k=0, ..., n_zone-1) to split the model into.

  @param dic: dictionary containing gyre input file information
  @type dic: dictionary
  @return: a tuple with the following two elements (each numpy.ndarray):

        1. an array of size n_zone+1 that contains the indices k of the edges (boundaries) of
           each zone. Note the length edge array is greater than n_zones by 1. In this regard, 
           the (inner) edge of the innermost layer corresponds to the element 0, and the (outer)
           edge of the outermost layer corresponds to the element n_zones (noting that the array 
           has n_zone+1 elements).

        2. the locations of the edges of the zones, in the coordinate specified through "by" 
           keyword argument. E.g. if "by" is 'mass', then, the second element of the returned
           tuple will have n_zone+1 elements all in mass (gram) coordinates.

  @rtype: tuple
  """
  if 'by' not in kwargs:
    raise SystemExit, 'tar_diff_rot: zoning_uniform: kwargs should contain/specify the field "by".'
  if 'n_zones' not in kwargs:
    raise SystemExit, 'tar_diff_rot: zoning_uniform: kwargs should contain/specify the field "n_zones".'
  if kwargs['by'] not in ['mass', 'radius', 'logT']:
    raise SystemExit, 'Error: tar_diff_rot: zoning_uniform: Requested option for "by" not supported yet.'

  nn      = dic['nn']
  M_star  = dic['M_star']
  R_star  = dic['R_star']
  gyre_in = dic['gyre_in']

  if kwargs['by'] == 'mass':
    vals = get_mass(dic)
  elif kwargs['by'] == 'radius':
    vals = gyre_in['radius']
  elif kwargs['by'] == 'logT':
    T   = gyre_in['temperature']
    vals = np.log10(temperature)
  else:
    raise SystemExit, 'Error: tar_diff_rot: zoning_uniform: Something went wrong with: kwargs["by"].'

  min_val = np.min(vals)
  max_val = np.max(vals)

  # The initial meshing; these points may not necessarily be in the original coordinate (i.e. vals)
  n_zones = kwargs['n_zones']
  delta   = (max_val - min_val) / n_zones
  edges   = np.zeros(n_zones+1, dtype=np.float64)
  indices = np.zeros(n_zones+1, dtype=np.int64)

  for i in range(n_zones+1):
    guess = min_val + i * delta
    ind   = np.argmin(np.abs(vals - guess))
    indices[i] = ind 
    edges[i]   = vals[ind]

  # To correct for the very thin, and almost mass-less atmosphere, on top of the photosphere,
  # we shift the outer edge to the top of the topmost layer in the atmosphere.
  if ind < nn:
    indices[-1] = nn - 1
    edges[-1]   = vals[-1]

  return (indices, edges)

#=========================================================================================
def zoning_uniform_rad_zone(dic, **kwargs):
  """
  This function is similar to zoning_uniform(), except that we exclude the inner convective
  core, and only apply the uniform zoning in the radiative region on top of the core. The 
  decision is made by comparing gradr and grada.
  Note: the keyword n_zones here specifies the number of zones including the convective core; 
        we manually include the convective core as the innermost zone; thus, in practice, there
        are "n_zones - 1" radiative regions.
  For additional information, refer to the documentation of zoning_uniform().
  """
  if 'by' not in kwargs:
    raise SystemExit, 'tar_diff_rot: zoning_uniform_rad_zone: kwargs should contain/specify the field "by".'
  if 'n_zones' not in kwargs:
    raise SystemExit, 'tar_diff_rot: zoning_uniform_rad_zone: kwargs should contain/specify the field "n_zones".'
  if kwargs['by'] not in ['mass', 'radius', 'logT']:
    raise SystemExit, 'Error: tar_diff_rot: zoning_uniform_rad_zone: Requested option for "by" not supported yet.'

  nn      = dic['nn']
  M_star  = dic['M_star']
  R_star  = dic['R_star']
  gyre_in = dic['gyre_in']
  gradr   = get_gradr(dic)
  grada   = gyre_in['grada']
  inds    = np.where(gradr <= grada)[0]
  ind_c   = inds[0]  # core index

  if kwargs['by'] == 'mass':
    vals = get_mass(dic)
  elif kwargs['by'] == 'radius':
    vals = gyre_in['radius']
  elif kwargs['by'] == 'logT':
    T   = gyre_in['temperature']
    vals = np.log10(temperature)
  else:
    raise SystemExit, 'Error: tar_diff_rot: zoning_uniform_rad_zone: Something went wrong with: kwargs["by"].'

  rad_zone = vals[ind_c : ] # from top of the core, until the surface.

  min_val = np.min(rad_zone)
  max_val = np.max(rad_zone)

  # The initial meshing; these points may not necessarily be in the original coordinate (i.e. rad_zone)
  n_zones = kwargs['n_zones']
  delta   = (max_val - min_val) / (n_zones - 1)    # one zone is reserved for the core; split the rest
  edges   = np.zeros(n_zones+1, dtype=np.float64)  # conv. core is included here
  indices = np.zeros(n_zones+1, dtype=np.int64)    # conv. core is included here

  # Set the index and edge of the inner part of the convective core manually
  edges[0]   = np.min(vals)
  indices[0] = 0

  # Uniform zoning for the rest of the radiative zone.
  for i in range(1, n_zones+1):
    guess = min_val + (i-1) * delta
    ind   = np.argmin(np.abs(vals - guess))
    indices[i] = ind 
    edges[i]   = vals[ind]

  # To correct for the very thin, and almost massless atmosphere, on top of the photosphere,
  # we shift the outer edge to the top of the topmost layer in the atmosphere.
  if ind < nn:
    indices[-1] = nn - 1
    edges[-1]   = vals[-1]

  return (indices, edges)

#=========================================================================================
def zoning_by_fraction(dic, **kwargs):
  """
  This function applies non-uniform zoning in the radiative part, by excluding the convective
  core with mass coordinate m_cc. The zoning is done in relative fraction of the radiative zone:
  - We define a relative coordinate, q, across the radiative region with mass coordinate m
           q = (m - m_cc) / (M_star - m_cc)
  - Trivially, knowing q, the corresponding mass coordinate, m, is
           m = m_cc + q x (M_star - m_cc)
             = q x M_star + (1-q) x m_cc
  - q always provides a rough estimate of the relative location of the "edges" of zones
  - The boundary of the convective core (m_cc) is set to zero (q=0), while the surface is set 
    to unity (q=1).
  - The meshing is done by using the q array passed as the keyword argument, i.e. **kwargs.
  - The meshing is only possible "by" mass and radius. We refrain from using logT, because it is
    not scalable in a fractional way
  - If "by" = "radius", then q will be the fractional radius, q = (m - r_cc) / (R_star - R_cc)

  @param dic: dictionary containing gyre input file information
  @type dic: dictionary
  """
  if 'by' not in kwargs:
    logging.error('tar_diff_rot: zoning_by_fraction: kwargs should contain/specify the field "by".')
    raise SystemExit, 'tar_diff_rot: zoning_by_fraction: kwargs should contain/specify the field "by".'
  if kwargs['by'] not in ['mass', 'radius']:
    logging.error('Error: tar_diff_rot: zoning_by_fraction: Requested option for "by" not supported yet.')
    raise SystemExit, 'Error: tar_diff_rot: zoning_by_fraction: Requested option for "by" not supported yet.'
  if 'q' not in kwargs:
    logging.error('Error: tar_diff_rot: zoning_by_fraction: q array not passed through kwargs')
    raise SystemExit, 'Error: tar_diff_rot: zoning_by_fraction: q array not passed through kwargs'

  q  = kwargs['q']
  if isinstance(q, float): q = np.array([q])
  nq = len(q)
  if nq == 0:
    logging.error('tar_diff_rot: zoning_by_fraction: *args is absent!')
    raise SystemExit, 'Error: tar_diff_rot: zoning_by_fraction: *args is absent!'
  if not all(q <= 1.0):
    logging.error('tar_diff_rot: zoning_by_fraction: q must be <= 1.0')
    raise SystemExit, 'Error: tar_diff_rot: zoning_by_fraction: q must be <= 1.0'
  if not all(q >= 0.0):
    logging.error('tar_diff_rot: zoning_by_fraction: q must be >= 0.0')
    raise SystemExit, 'Error: tar_diff_rot: zoning_by_fraction: q must be >= 0.0'
  q  = np.sort(q)

  nn      = dic['nn']
  M_star  = dic['M_star']
  R_star  = dic['R_star']
  gyre_in = dic['gyre_in']
  gradr   = get_gradr(dic)
  grada   = gyre_in['grada']
  inds    = np.where(gradr <= grada)[0]
  ind_c   = inds[0]  # core index

  if kwargs['by'] == 'mass':
    vals  = get_mass(dic)
  elif kwargs['by'] == 'radius':
    vals = gyre_in['radius']
  else:
    logging.error('tar_diff_rot: zoning_uniform_rad_zone: Something went wrong with: kwargs["by"].')
    raise SystemExit, 'Error: tar_diff_rot: zoning_uniform_rad_zone: Something went wrong with: kwargs["by"].'

  rad_zone = vals[ind_c : ] # from top of the core, until the surface.
  q_cc     = vals[ind_c]    # m_cc or r_cc, depending on the choice of "by"
  q_surf   = vals[-1]       # M_star or R_star, depending on the choice of "by"

  min_val = np.min(rad_zone)
  max_val = np.max(rad_zone)
  # q_vals  = (rad_zone - q_cc) / (q_surf - q_cc)

  n_zones = nq + 2    # conv. core + nq rad zones + outermost zone
  edges   = np.zeros(n_zones+1, dtype=np.float64)  # conv. core is included here
  indices = np.zeros(n_zones+1, dtype=np.int64)    # conv. core is included here

  # Set the index and edge of the inner boundary of the convective core manually
  edges[0]   = np.min(vals)
  indices[0] = 0

  # Set the index and edge of the outer boundary of the convective core manually
  edges[1]   = vals[ind_c]
  indices[1] = ind_c

  # Non-uniform zoning for the rest of the radiative zone, based on q
  for iq in range(nq):
    guess = q[iq] * q_surf + (1.0 - q[iq]) * q_cc
    ind   = np.argmin(np.abs(vals - guess))
    k     = iq + 2
    indices[k] = ind 
    edges[k]   = vals[ind]

  # Set the index and edge of the outer boundary of the radiative zone (i.e. the surface), manually
  edges[-1]   = max_val
  indices[-1] = nn - 1

  logging.info('zoning_by_fraction: indices: {0}'.format(indices))

  return (indices, edges)

#=========================================================================================
def check_zoning_indices_uniqueness(tup_zones):
  """
  During MCMC iteration, a chain withina a walker may wish to try very tiny zones, specified
  by q values which result in mass difference of two zones being smaller than the cloned 
  mass grid in the MESA/GYRE input model. In such (unfavorable circumstances), the zoning
  indices returned from our zoning function will have a reapeated element, resulting into
  zoning with one zone of zero mass.
  To capture that, we compare the length of the original indices and a set of indices (to 
  exclude repeated elements), and verify the uniqueness of the indices
  @param tup_zones: The tuple that passes the zone indices and edges after a call to any of
         the zoning functions. In fact, tup_zones is the standard output of all zoning functions
  @type tup_zones: tuple
  @return: True, if all zones are unique, and False if at least one element is repeated 
  @rtype: boolean
  """
  indices  = tup_zones[0]
  n_inds   = len(indices)
  set_indx = set(indices)
  n_set    = len(set_indx)

  flag     = n_inds == n_set

  if not flag:
    logging.info('tar_diff_rot: check_zoning_indices_uniqueness: Non-unique zoning index:')
    logging.info('orig. indices: {0}'.format(indices))
    logging.info('set indices:   {1}'.format(set_indx))

  return flag

#=========================================================================================



#   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#   O M E G A   F U N C T I O N S
#   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#   Function signatures: 
#        func(dic, tup_zones, **kwargs)

#=========================================================================================
def omega_ramp(dic, tup_zones, **kwargs):
  """
  Construct increasing (or decreasing) omega profile that follows a linear trend with positive
  (or negative) slope. The discrete omega profile has a surface value specified by the user
  as "omega_surf", and has a core rotation frequency omega_core specified by omega_surf x core_to_surf.
  The latter ratio can be positive (or negative) value, associated with differential rotation
  for stars that rotates faster or slower in the core compared to the surface.
  Two key inputs shall be supplied by user through kwargs:
    1. omega_surf: float. Surface angular rotation frequency in radians per second.  
    2. core_to_surf: float. Core (or innermost zone) rotation frequency divided by the surface 
       (outermost zone) rotation frequency.
  If these fields are not available in kwargs, we raise an exception.

  @param dic: dictionary containing gyre input file information
  @type dic: dictionary
  @param tup_zones: This input tuple is actually an output of previous steps of zoning the model.
         tup_zones can be constructed by e.g. calling zoning_uniform(). Read that function for
         more info.
  @type: tup_zones: tuple
  @return: array of Omega_k, i.e. omega over zone k
  @rtype: 
  """
  if 'omega_surf' not in kwargs.keys():
    raise SystemExit, 'Error: tar_diff_rot: omega_ramp: "omega_surf" not supplied by **kwargs'
  if 'core_to_surf' not in kwargs.keys():
    raise SystemExit, 'Error: tar_diff_rot: omega_ramp: "core_to_surf" not supplied b **kwargs'

  omega_surf   = kwargs['omega_surf']
  core_to_surf = kwargs['core_to_surf']
  omega_core   = omega_surf * core_to_surf

  zone_inds    = tup_zones[0]
  zone_edges   = tup_zones[1]
  core         = zone_edges[0]
  surf         = zone_edges[-2]
  n_edges      = len(zone_edges)
  n_zones      = n_edges - 1 

  omega_slope  = (omega_surf - omega_core) / (surf - core)  # m = (y2 - y1) / (x2 - x1)
  omega_offset = omega_core - omega_slope * core            # b = y1 - m * x1

  omega_zones  = omega_offset + omega_slope * zone_edges[:-1] 

  return omega_zones  # Omega_k

#=========================================================================================
def omega_ramp_lock_conv_rad_zones(dic, tup_zones, **kwargs):
  """
  This function is basically similar (copied from) omega_ramp(), but with a significant 
  difference that the rotation angular frequency (Omega_k) of the convective core is set
  identical to that of the overlying radiative zone. In other words, the convective core
  and the innermost radiative zone are rotationally locked, hence the function naming.
  In this case, the omega_core will be the rotation frequency of the innermost radiative
  region (and also that of the underlying convective core). 
  The meaning and functionality of the rest stays similar to omega_ramp(), for which
  we refer to, for additional documentation.
  """
  if 'omega_surf' not in kwargs.keys():
    raise SystemExit, 'Error: tar_diff_rot: omega_ramp_lock_conv_rad_zones: "omega_surf" not supplied by **kwargs'
  if 'core_to_surf' not in kwargs.keys():
    raise SystemExit, 'Error: tar_diff_rot: omega_ramp_lock_conv_rad_zones: "core_to_surf" not supplied b **kwargs'

  omega_surf   = kwargs['omega_surf']
  core_to_surf = kwargs['core_to_surf']
  omega_core   = omega_surf * core_to_surf

  zone_inds    = tup_zones[0]
  zone_edges   = tup_zones[1]
  core         = zone_edges[1]
  surf         = zone_edges[-2]
  n_edges      = len(zone_edges)
  n_zones      = n_edges - 1 

  omega_slope  = (omega_surf - omega_core) / (surf - core)  # m = (y2 - y1) / (x2 - x1)
  omega_offset = omega_core - omega_slope * core            # b = y1 - m * x1

  omega_zones  = np.zeros(n_zones)
  omega_zones[1:] = omega_offset + omega_slope * zone_edges[1:-1] 
  omega_zones[0]  = omega_zones[1]

  return omega_zones  # Omega_k

#=========================================================================================
def smooth_omega_discontinuity(dic, omega_r, tup_zones, n_smooth, smooth_by='line', by='mass'):
  """
  To iron out the rapid jump between the inner and outer rotation frequencies, one can optionally
  apply a smoothing function at the location of the discontinuity, k, with "n_smooth" number
  of points in either sides of k. Thus, we include "2 x n_smooth + 1" points for this smoothing.
  The choice of smoothing function can be either a linear connecting line, or more sophisticated
  other shapes.
  Note: This function should be called when the omega(r) is available; this implies that 
        omega_zones_to_omega_r(), or any equivalent function, should be called first.

  @param omega_r: The spatial profile of the rotation angular velocity omega(r). This can be 
        retrieved by a call to e.g. omega_zones_to_omega_r() function.
  @type omega_r: ndarray
  @param tup_zones: This input tuple is actually an output of previous steps of zoning the model.
         tup_zones can be constructed by e.g. calling zoning_uniform(). Read that function for
         more info.
  @type: tup_zones: tuple
  @param n_smooth: number of smoothing points on either side of the jump
  @type n_smooth: integer
  @param smooth_by: specify the option by which the smoothing function will look like. 
         Currently, the options are:
         - line: just a connecting line from k-n_smooth to k+n_smooth, passing through k at midpint.
  @type smooth_by: string
  """
  if smooth_by not in ['line']:
    logging.error('tar_diff_rot: smooth_omega_discontinuity: smooth_by: {0} not supported'.format(smooth_by))
    raise SystemExit, 'Error: tar_diff_rot: smooth_omega_discontinuity: smooth_by: {0} not supported'.format(smooth_by)

  if by not in ['mass', 'radius']:
    logging.error('tar_diff_rot: smooth_omega_discontinuity: by: {0} not supported'.format(by))
    raise SystemExit, 'Error: tar_diff_rot: smooth_omega_discontinuity: by: {0} not supported'.format(by)

  if by == 'mass':
    xvals = get_mass(dic) / Msun 
  elif by == 'radius':
    xvals = dic['radius'] / Rsun
  else:
    logging.error('tar_diff_rot: smooth_omega_discontinuity: something went wrong about "by"')
    raise SystemExit, 'Error: tar_diff_rot :smooth_omega_discontinuity: something went wrong about "by"'

  ind_edges = tup_zones[0]
  # exclude the inner and outer edges of the convective core, and also the outer surface
  ind_work  = ind_edges[2 : -1]
  n_edges   = len(ind_work)
  if n_edges == 0:
    logging.error('tar_diff_rot: smooth_omega_discontinuity: No edges left for smoothing!')
    raise SystemExit, 'Error: tar_diff_rot: smooth_omega_discontinuity: No edges left for smoothing!'

  # ndarray elements cannot be overwritten. To hack this, we use list, and convert back later
  omega_r = list(omega_r)
  # check if there enough mesh points between the zones for the smoothing (by n_smooth points) to operate
  for j, k in enumerate(ind_work):

    ind_inner = ind_edges[j + 1]
    ind_outer = ind_edges[j + 3]
    smooth_OK = k - ind_inner > n_smooth
    if not smooth_OK: continue
    smooth_OK = ind_outer + 1 - k > n_smooth
    if not smooth_OK: continue

    y_in      = omega_r[k - n_smooth]
    y_out     = omega_r[k + n_smooth]
    x_in      = xvals[k - n_smooth]
    x_out     = xvals[k + n_smooth]

    slope     = (y_in - y_out) / (x_in - x_out)
    intercept = y_in - slope * x_in

    x_smooth  = xvals[k - n_smooth : k + n_smooth + 1]
    y_smooth  = slope * x_smooth + intercept
    omega_r[k - n_smooth : k + n_smooth + 1] = y_smooth

  if dbg:
    print 'smooth_omega_discontinuity: '
    print j, k
    print slope, intercept
    print y_smooth
    print omega_r[k - n_smooth : k + n_smooth + 1]

  return np.array(omega_r)

#=========================================================================================
def omega_zones_to_omega_r(dic, tup_zones, omega_zones):
  """ 
  This routine maps the discrete Omega_k values (stairs) with finite number of zones (n_zones)
  over the input GYRE model. Thus, we go from n_zones to n_mesh.
  @param dic: dictionary containing gyre input file information
  @type dic: dictionary
  @param tup_zones: This input tuple is actually an output of previous steps of zoning the model.
         tup_zones can be constructed by e.g. calling zoning_uniform(). Read that function for
         more info.
  @type: tup_zones: tuple
  @param omega_zones: Discrete (and finite number) omega values placed over the zones; in other
         words, omega_zones is Omega_k
  @type omega_zones: ndarray
  @return: array of the same Omega_k values maped on the the input model, and placed in between
         the zone edges.
  @rtype: ndarray
  """
  ind_edges = tup_zones[0]
  n_edges   = len(ind_edges) 
  n_zones   = len(omega_zones)
  if n_zones != n_edges-1 : 
    raise SystemExit, 'Error: tar_diff_rot: omega_zones_to_omega_r: Incompatible number of edges and zones.'

  nn        = dic['nn']
  omega_r   = np.zeros(nn)

  for i_edge in range(n_edges-1):
    ind_from = ind_edges[i_edge]
    ind_to   = ind_edges[i_edge+1]
    omega_r[ind_from : ind_to] = omega_zones[i_edge]
    if dbg: print i_edge, ind_from, ind_to, omega_zones[i_edge]
  omega_r[ind_from : ] = omega_zones[i_edge]  # to ensure surface Omega(r) is also set

  return omega_r

#=========================================================================================


#   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#   A N G U L A R   M O M E N T U M
#         F U N C T I O N S
#   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


#=========================================================================================
#=========================================================================================
def get_dJ_zoning(dic, zoning_func, omega_func, **kwargs):
  """
  This function constructs the Lagrangian mesh coordinates (e.g. in mass) in order to split the 
  star into n_zones regions. Then, Omega shall also be specified over each of these zones, too, 
  and mapped onto the full model. 

  @param dic: dictionary containing gyre input file information
  @type dic: dictionary
  @param zoning_func: an external function that receives the input structure data "dic", the
         desired number of zones n_zone, and additional arguments (*args), and keyword 
         arguments (**kwargs). This function returns the position boundaries of the sub-regions
         that span from m_{k} to m_{k+1} in the integral defined above.
  @type zoning_func: function
  @param n_zone: Number of desired zones (regions, k=0, ..., n_zone-1) to split the model into
  @type n_zone: integer
  @return: angular momentum integrand, dJ, in cgs units
  @rtype: ndarray
  """
  if 'n_zones' not in kwargs:
    raise SystemExit, 'Error: get_dJ_zoning: n_zones not passed in kwargs! Please, do so.'
  n_zones = kwargs['n_zones']
  if n_zones <= 1:
    raise SystemExit, 'Error: get_dJ_zoning: n_zones must be greater than 1'
  
  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  # Zoning
  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  tup_zones = zoning_func(dic, **kwargs)
  ind_zones = tup_zones[0]
  edg_zones = tup_zones[1]

  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  # Omega in each zone
  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  omega_k   = omega_func(dic, tup_zones, **kwargs)

  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  # Map Omega in zones to everywhere
  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  omega_r   = omega_zones_to_omega_r(dic, tup_zones, omega_k)

  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  # Construct Ang. Mom. Integrand
  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  dm        = get_dm(dic)
  r         = dic['gyre_in']['radius']
  dJ        = np.power(r, 2) * omega_r * dm

  return dJ 

#=========================================================================================
def get_J_zoning(dic, zoning_func, **kwargs):
  """
  Retrieve angular momentum integrand by calling get_dJ_zoning(), and sum over it to calculate
  the net angular momentum J = sum dJ
  Parameters have identical meaning as in get_dJ_zoning().
  """
  dJ = get_dJ_zoning(dic=dic, zoning_func=zoning_func, **kwargs)
  J  = np.sum(dJ)

  return J

#=========================================================================================
#=========================================================================================
#=========================================================================================


#   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#   C H E C K   Z O N I N G   A N D
#              O M E G A 
#   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#=========================================================================================
def check_zoning_func_omega_func_compatible(zoning_func, omega_func):
  """
  Not all zoning functions go with (is consistent with) all omega functions, and a thorough
  checks shall be performed before and/or during any zoning and omega computations for the 
  check of consistency. Here, we make sure that both zoning and omega functions passed 
  are self consistent.

  Note: So far, based on a unit testing in the file "assess-diff-rot.py", all six combinations
        of zoning and omega profile can give reasonable profiles with physical meaning. 
        Thus, in the future, by introduction of new routines, such tests shall be repeated
        and extended for consistency checks.
        
  @param zoning_func: a callable that carries out the task of zoning a model
  @type zoning_func: function
  @param omega_func: a callable that puts a discrete Omega_k rotation profile on the zoned model
  @type omega_func: function
  @return: a flag that specifies if the choices for zoning and omega functions are consistent
        (True), or inconsistent (False)
  @rtype: boolean
  """
  zoning_is_uniform = zoning_func == zoning_uniform
  zoning_is_uniform_rad = zoning_func == zoning_uniform_rad_zone
  zoning_is_by_frac = zoning_func = zoning_by_fraction

  if not any([zoning_uniform, zoning_is_uniform_rad, zoning_is_by_frac]):
    logging.error('tar_diff_rot: check_zoning_func_omega_func_compatible: Unsupported zoning function')
    raise SystemExit, 'Error: tar_diff_rot: check_zoning_func_omega_func_compatible: Unsupported zoning function'

  omega_is_ramp = omega_func == omega_ramp
  omega_is_rad = omega_func == omega_ramp_lock_conv_rad_zones

  if not any([omega_is_ramp, omega_is_rad]):
    logging.error('tar_diff_rot: check_zoning_func_omega_func_compatible: Unsupported omega function')
    raise SystemExit, 'Error: tar_diff_rot: check_zoning_func_omega_func_compatible: Unsupported omega function'

  passed = False  # default

  if zoning_is_uniform:
    if omega_is_ramp:
      passed = True
    if omega_is_rad:
      passed = True

  if zoning_is_uniform_rad:
    if omega_is_ramp:
      passed = True
    if omega_is_rad:
      passed = True

  if zoning_is_by_frac:
    if omega_is_ramp: 
      passed = True
    if omega_is_rad: 
      passed = True

  return passed

#=========================================================================================

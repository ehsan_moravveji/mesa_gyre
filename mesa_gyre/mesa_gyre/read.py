"""
  This module provides different tools to read in the data form MESA consistently.
  It supports reading a single hostory log file, a single profile, or a large number of
  files from a repository.

  log history:
  - 25 July 2013: created.

"""
import sys, glob, os, distutils
import logging
import time
from multiprocessing import Pool, cpu_count
import commons as cm
import param_tools as pt
import file_dir_stat as st

from ivs.io import ascii

try:
  import h5py
except:
  print 'h5py not installed! \n sudo pip install h5py'
import os
import numpy as np
import Queue
#import threading

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================
def extend_record_array(old, extra_dtype):
    """
    To extend an existing record array with new columns, passed through extra_dtype.
    @param old: numpy record array to be extended. The returned array will already have a copy of the whole data from old.
    @type old: numpy record array
    @param extra_dtype: numpy dtype object. If the input is not of the type: np.dtype(), then, it is "tried" to convert it to np.dtype
           by applying the np.dtype(extra_dtype). An accepted format is like this:
           extra_dtype = [('example', 'f8')]
    @return: numpy record array, extended to contain new fields passed by extra_dtype, and initialized to empty. They have to be
           filled up afterwards. The whole old record array is copied and returned in the new record array
    @rtype: numpy record array
    """
    if old.dtype.fields is None:
      raise ValueError, '"old" must be a structured numpy array'
    if type(extra_dtype) is not type(np.dtype([('example', 'f8')])):
        try:
            extra_dtype = np.dtype(extra_dtype)
        except:
            raise SystemExit, 'Error: extend_record_array: extra_dtype is not of the type np.dtype() !'

    old_names = old.dtype.names

    new = np.empty(old.shape, dtype=old.dtype.descr + extra_dtype.descr)
    for name in old_names:
       new[name] = old[name]

    return new

#=================================================================================================================
def get_file_list(path, srch):
  """
  This routine returns the sorted list of files existing in "path", and fulfiling the "srch" search string.
  Note that the "path" must be valid and existing, and the "path + srch" must be a valid input to the glob command
  @param path: full path to the directory where the files exist
  @type path: string
  @param srch: Search string to glob files with
  @type srch: string
  @return: list of files that are already sorted in their names
  @rtype: list
  """
  if not os.path.exists(path):
    logging.error('read: get_file_list: {0} does not exist'.format(path))
    print 'Error: read: get_file_list: {0} does not exist'.format(path)
    raise SystemExit
  if path[-1] != '/': path += '/'

  return sorted(glob.glob(path + srch), key=distutils.version.LooseVersion)

#=================================================================================================================
def graco_gen_inf(list_files):
  """
  GraCo outputs the eigenfrequency list into an ascii file with _gen_inf.dat extension. This function reads the contents
  of such GraCo files, provided their full path are passed in.
  @param list_files: full path to GraCo output files to be read
  @type list_files: list
  @return:
  @rtype:
  """
  n_files = len(list_files)
  if n_files == 0:
    message = 'Error: read: graco_gen_inf: Input list is empty!'
    raise SystemExit, message

  #col_names = 'l, n_pg, omega2, freq, per, Q, n_p, n_g'
  col_names = 'l, n_pg, n_p, n_g, omega2, freq, per, Q'
  col_dtype = 'i4, i4, i4, i4, f8, f8, f8, f8'

  list_dic_out = []
  for i_file, a_file in enumerate(list_files):
    if not os.path.isfile(a_file):
      message = 'Error: read: graco_gen_inf: %s is not a file!' % (a_file, )
      raise SystemExit, message

    r = open(a_file, 'r')
    lines = r.readlines()
    n_lines = len(lines)
    n_modes = n_lines - 9
    r.close()

    dic = {}
    dic['filename'] = a_file
    dic['model']    = lines[0].split()[2]
    dic['mass']     = float(lines[2].split()[2])
    dic['log_Teff'] = float(lines[3].split()[3])
    dic['log_g']    = float(lines[4].split()[3])
    dic['log_L']    = float(lines[5].split()[3])
    dic['Xc']       = float(lines[6].split()[3])

    table = []
    for i_line, line in enumerate(lines[9:]):
      line = line.split()
      el     = int(line[0][:-1])
      n_pg   = int(line[1])
      omega2 = float(line[2])
      freq   = float(line[3])
      per    = float(line[4])
      Q      = float(line[5])
      n_p    = int(line[6])
      n_g    = int(line[7])

      table.append([el, n_pg, n_p, n_g, omega2, freq, per, Q])

    recarr_all = np.core.records.fromarrays(np.array(table).transpose(), names=col_names, formats=col_dtype)
    sort_ind   = np.argsort(recarr_all['freq'])
    recarr_all = recarr_all[sort_ind]

    # GraCo output sometimes comes with repeated modes. They have to be identified and removed.
    def graco_exclude_repeated_modes(rec):
      """
      Exclude repeated modes based only on frequency.
      @parm rec: record array with mode list
      @type rec: numpy record array
      @return: cleaned list in the form of a record array
      @rtype: numpy record array
      """
      n_rec = len(rec)
      n_repeat = 0; list_ind = []; list_freq = []
      for i_rec, row in enumerate(rec):
        if not row['freq'] in list_freq:
          list_freq.append(row['freq'])
          list_ind.append(i_rec)
        else:
          n_repeat += 1
      return rec[list_ind]

    recarr = graco_exclude_repeated_modes(recarr_all)
    dic['modes'] = recarr

    list_dic_out.append(dic)

  return list_dic_out

#=================================================================================================================
def mode_table(table):
  """
  The HDF5 mode tables can be read using this function. It returns a numpy record array.
  For a full description of how the tables are written as file, and the available fileds, read meas_gyre.write
  @param table: full path to the HDF5 table
  @type table: string
  @return: numpy record array containing the data from the table. Each dataset from the main group ('modes') is a
      column in the returned record array
  @rtype: numpy record array
  """
  if not os.path.isfile(table):
    message = 'Error: read: mode_table: Input file %s does not exist' % (table, )
    raise SystemExit, message

  return None

#=================================================================================================================
def read_multiple_h5(list_files):
  """
  This function receives a list of HDF5 filenames which are either MESA history or profile files.
  Iteratively, read_h5 is called to output one dictionary per each input HDF5 file. Then, the dictionaries are
  collected into a list, and returned.
  @param list_files: list of full path to the files to be read.
  @type list_files: list of strings.
  @return: list of dictionaries with the history or profile information.
  @rtype: list of dictionaries
  """
  n_files = len(list_files)
  logging.info('read_multiple_h5: Starting to read {0} files'.format(n_files))
  if n_files == 0:
    message = 'Error: ascii2h5: input list is empty.'
    raise SystemExit, message

  list_dic = []
  for i_file, file in enumerate(list_files):
    dic = read_h5(file)
    if dic is not None: list_dic.append(dic)

    # add a progress bar
    if n_files >20: pt.update_progress(float(i_file)/n_files)

  logging.info('read_multiple_h5: Finished reading {0} files'.format(n_files))

  return list_dic

#=================================================================================================================
def read_h5(h5_file):
  """
  This function receives a full path to an HDF5 file containing MESA output, and store its content into a dictionary.
  It automatically identifies if the file is a hist file or a prof file. The output dictionary has the following keys:
   - filename: full path to the HDF5 file
   - source:
   - header: the header information of the ascii file
   - hist: the numpy record array for the history table, if the input file is a hist file
   - prof: the numpy record array for the profile table, if the input file is a prof file
  @param h5_file: full path to the h5 file
  @type h5_file: string
  @return: dictionary with all information stored as key:value pairs explained above
  @rtype: Dictionary
  """
  if not os.path.isfile(h5_file):
    message = 'Error: ascii2h5: read_h5: The input file: %s is missing.' % (h5_file, )
    raise SystemExit, message

  # Open the file
  try:
    f = h5py.File(h5_file, 'r')
  except:
    logging.error('read_h5: Unable to open {0}'.format(h5_file))
    return None
  import numpy

  data_group = f['/profile']
  header = dict( data_group.attrs.items() )
  data_names = data_group.keys()
  data = []
  for data_name in data_names:
    Dset = data_group[data_name][...]
    data.append(Dset.T)

  # convert the list of record arrays to named numpy record array.
  data = numpy.rec.fromarrays(data, names=data_names)

  # Close the file
  f.close()

  # decide whether the file is a history or a profile file
  if 'model_number' in header.keys():
    is_hist=False; is_prof=True
  else:
    is_hist=True; is_prof=False

  dic = {}
  dic['filename'] = h5_file
  dic['source'] = '' # read this from h5 attribute
  dic['header'] = header
  if is_hist and not is_prof: dic['hist'] = data
  if is_prof and not is_hist: dic['prof'] = data

  return dic

#=================================================================================================================
def read_mesa(filename='star.log',just_header=False):
    """
    Read star.log and .data files from MESA.
    This is a modified version of the ivs.stellar_evolution.fileio.read_mesa function.
    It checks and fixes (if necessary) if two data header names are similar. If so, it changes them, to new names
    with structure like copy_1, copy_2 and so on.

    This returns a record array with the global and local parameters (the latter
    can also be a summary of the evolutionary track instead of a profile if
    you've given a 'star.log' file.

    The stellar profiles are given from surface to center.

    @param filename: name of the log file
    @type filename: str
    @param just_header: read only the first model (or global parameters)
    @type just_header: bool
    @return: list of models in the data file (typically global parameters, local parameters)
    @rtype: list of rec arrays
    """
    models = []
    new_model = False
    header = None
    #-- open the file and read the data
    with open(filename,'r') as ff:
        #-- skip first 5 lines when difference file
        if os.path.splitext(filename)[1]=='.diff':
            for i in range(5):
                line = ff.readline()
            models.append([])
            new_model = True
        while 1:
            line = ff.readline()
            if not line: break # break at end-of-file
            line = line.strip().split()
            if not line: continue
            #-- begin a new model
            if line[0]=='1' and line[1]=='2':
                #-- wrap up previous model
                if len(models):
                    model = np.array(models[-1],float).T
                    models[-1] = np.rec.fromarrays(model,names=header)
                    if just_header: break
                models.append([])
                new_model = True
                continue
            #-- next line is the header of the data, remember it
            if new_model:
                header = line
                flag, new_header = check_fix_header(header)
                if flag: header = new_header
                new_model = False
                continue
            try:
              models[-1].append(line)  # original line
              #models.append(line)
            except IndexError:
              print '%s failed' % (filename, )
    if len(models)>1:
        model = np.array(models[-1],float).T
        models[-1] = np.rec.fromarrays(model,names=header)

    return models


#=================================================================================================================
def check_fix_header(header):
  """
  This function checks the repetition in the header names, and fixes the repeated names by new names like copy_1,
  copy_2 etc.
  @param header: list of strings
  @type param: list of string
  @return:
   - flag: boolean, which specifies whether a change in header is applied or not.
   - new_header: list of string. It is a fixed list
  @rtype: boolean, list
  """

  n_hdr = len(header)
  if n_hdr == 0:
    message = 'Error: read: check_fix_header: Empty header!'
    raise SystemExit, message

  n_tot_repeat = 0
  i_fix = 0
  for i_hdr in range(n_hdr-1):
    name = header[i_hdr]
    exclude_name = header[i_hdr+1:]
    flag = name in exclude_name
    if not flag: continue

    n_excluded = len(exclude_name)
    ind_repeat = [ind for ind in range(n_excluded) if name == exclude_name[ind]]
    n_repeat = len(ind_repeat)
    n_tot_repeat += n_repeat

    for i_repeat in range(n_repeat):
      i_fix += 1
      header[i_hdr+ind_repeat[i_repeat]+1] += '_fix' + str(i_fix)

  return flag, header


#=================================================================================================================
def multiproc_gyre_out_ascii(list_files, n_cpu=None):
  """
  Read a list of ASCII GYRE output files using a Pool and "multiprocessing".
  We use single_gyre_output_ascii() to read the whole list of files.
  @param list_files: list of gyre output files in ASCII format 
  @type list_files: list of strings
  @return: list of dictionaries, each corresponding to one file 
  @rtype: list of dictionaries
  """
  n_files = len(list_files)
  if n_files == 0:
    logging.error('read: multiproc_gyre_out_ascii: Input list is empty')
    raise SystemExit, 'Error: read: multiproc_gyre_out_ascii: Input list is empty'

  if n_cpu is None: n_cpu = cpu_count()
  if n_cpu == 1:
    return [single_gyre_output_ascii(f) for f in list_files]

  pool = Pool(processes=n_cpu)
  n_chops = n_files / n_cpu + 1
  t_start = time.time()

  result  = []
  for i_cpu in range(n_cpu):
    ind_start = i_cpu * n_chops
    ind_to    = ind_start + n_chops
    if i_cpu  == n_cpu - 1: ind_to = n_files
    chop      = list_files[ind_start : ind_to]
    result.append( pool.apply_async( single_gyre_output_ascii, args=(chop, ) ) )

  pool.close()
  pool.join()

  list_output = [] 
  for i_cpu in range(n_cpu): 
    list_output += result[i_cpu].get()

  dT_sec = int(time.time() - t_start)
  print ' - read: multiproc_gyre_out_ascii: Finished reading {0} files in {1} sec'.format(n_files, dT_sec)

  return list_output

#=================================================================================================================
def single_gyre_output_ascii(a_file):
  """
  This function reads only a single GYRE output in ascii format. It can be used stand-alone, or called by 
  parallel_gyre_output().
  @param a_file: full path to the file that is going to be read. Only HDF5 files are accepted.
  @type a_file: string
  @return: a dictionary with the whole data from the HDF5 files. In this case, the dictionary can have the following
         typical fileds, e.g.:
         - filename
         - M_star, L_star, R_star
         - n_p, n_g, n_pg
         - freq, omega, etc.
         If for any reason, there is an IOError exception raised, then it returns None.
         So, a careful capturing of None's are essential after calling this function
  @rtype: dictionary or NoneType
  """
  if not os.path.exists(a_file):
    logging.error('read: single_gyre_output_ascii: {0} does not exist'.format(a_file))
    raise SystemExit, 'Error: read: single_gyre_output_ascii: {0} does not exist'.format(a_file)
  
  with open(a_file, 'r') as r: lines = r.readlines()

  header_nums  = lines.pop(0).rstrip('\r\n').split()
  header_names = lines.pop(0).rstrip('\r\n').split()
  header_vals  = lines.pop(0).rstrip('\r\n').split()

  dic = {}
  dic['filename'] = a_file
  dic[header_names[0]] = header_vals[0]
  for i in range(1, 4): dic[header_names[i]] = float(header_vals[i])

  col_nums     = lines.pop(0).rstrip('\r\n').split()
  col_names    = lines.pop(0).rstrip('\r\n').split()
  n_cols       = len(col_nums)

  list_dtype   = []
  list_func    = []
  for name in col_names:
    if name == 'l':
      fmt = 'i8'
      list_func.append(int)
    elif name == 'n_p':
      fmt = 'i8'
      list_func.append(int)
    elif name == 'n_g':
      fmt = 'i8'
      list_func.append(int)
    elif name == 'n_pg':
      fmt = 'i8'
      list_func.append(int)
    else:
      fmt = 'f8'
      list_func.append(float)
    list_dtype.append( (name, fmt) )

  data = []
  for line in lines:
    line = line.rstrip('\r\n').split()
    data.append(line)

  recarr = np.core.records.fromarrays(np.array(data).transpose(), dtype=list_dtype)
  n_rec  = len(recarr)
  list_flags = []
  for i, row in enumerate(recarr):
    flag = True
    if row['Re(freq)'] <= 0.0: flag = False
    if row['Re(omega)'] <= 0.0: flag = False
    list_flags.append(flag)
  n_flag     = len(list_flags)
  list_flags = np.array(list_flags)
  if n_flag > 0: 
    recarr = recarr[list_flags]

  for name in col_names:
    dic[name] = recarr[name]

  return dic

#=================================================================================================================
def single_gyre_output(a_file):
  """
  This function reads only a single GYRE output in HDF5 format. It can be used as stand-alone, or called by
  parallel_gyre_output().
  @param a_file: full path to the file that is going to be read. Only HDF5 files are accepted.
  @type a_file: string
  @return: a dictionary with the whole data from the HDF5 files. In this case, the dictionary can have the following
         typical fileds, e.g.:
         - filename
         - M, ov, eta, Z, logD, sc, Xc, Yc, model_number
         - M_star, L_star, R_star
         - n_p, n_g, n_pg
         - freq, omega, etc.
         If for any reason, there is an IOError exception raised, then it returns None.
         So, a careful capturing of None's are essential after calling this function
  @rtype: dictionary or NoneType
  """
  from mesa_gyre.param_tools import get_param_from_single_gyre_filename
  import gyre

  dic = get_param_from_single_gyre_filename(a_file)
  dic['filename'] = a_file
  try:
    hdr, loc = gyre.read_output(a_file)
  except IOError:
    return None

  if hdr:
    for key, value in hdr.items(): dic[key] = value
  names = loc.dtype.names
  for name in names:
    dic[name] = loc[name]

  return dic

#=================================================================================================================
def multiproc_gyre_output(list_files, n_cpu=None):
  """
  Use multiprocessing to read GYRE output HDF5 files using multiple CPUs. This is identical to read_multiple_gyre_files().
  @param n_cpu: boolean, or integer; default=None. If it is None, then pp.Server() is initialized without specifiying
       the number of CPUs. Otherwise, the number of CPUs will be passed.
  @type n_cpu: boolean or integer.
  @return: list of dictionaries with the whole data stored as one dictioary per each file.
  @rtype: list of dictionaries
  """
  if isinstance(n_cpu, int):
    pool = Pool(processes=n_cpu)
  elif n_cpu is None:
    n_cpu = cpu_count()
    print ' - read: multiproc_gyre_output: Automatically setting n_cpu={0}'.format(n_cpu)
    pool = Pool(processes=n_cpu)
  else:
    logging.error('read: multiproc_gyre_output: n_cpu has wrong value')
    raise SystemExit, 'Error: read: multiproc_gyre_output: n_cpu has wrong value'
  
  n_files = len(list_files)
  if n_files == 0:
    logging.error('read: multiproc_gyre_output: Input list is empty')
    raise SystemExit, 'Error: read: multiproc_gyre_output: Input list is empty'
  
  n_chops = n_files / n_cpu + 1
  
  t_start = time.time()
  
  result = []
  for i_cpu in range(n_cpu):
    n_from = i_cpu * n_chops 
    n_to   = (i_cpu + 1) * n_chops
    if i_cpu == n_cpu - 1: n_to = n_files
    file_chop = list_files[n_from : n_to]
    result.append(pool.apply_async( read_multiple_gyre_files, args=(file_chop, False) ))
  pool.close()
  try: 
    pool.join()
  except ValueError:
    print 'failed here'
    sys.exit()

  list_output = []
  for i_cpu in range(n_cpu): list_output += result[i_cpu].get()

  dt_sec = time.time() - t_start
  
  print ' - read: multiproc_gyre_output:'
  print '   N_cpu={0}, N_files={1}, Time={2} [sec] \n'.format(n_cpu, n_files, int(dt_sec))
  logging.info('read: multiproc_gyre_output: N_cpu={0}, N_files={1}, Time={2} [sec] \n'.format(n_cpu, n_files, int(dt_sec)))
  
  return list_output

#=================================================================================================================
def parallel_gyre_output(list_files, n_cpu=None):
  """
  Use "pp" to read GYRE output HDF5 files using multiple CPUs. This is identical to read_multiple_gyre_files().
  @param n_cpu: boolean, or integer; default=None. If it is None, then pp.Server() is initialized without specifiying
       the number of CPUs. Otherwise, the number of CPUs will be passed.
  @type n_cpu: boolean or integer.
  @return: list of dictionaries with the whole data stored as one dictioary per each file.
  @rtype: list of dictionaries
  """
  try:
    import pp
  except:
    logging.error('read: parallel_gyre_output: pp is not installed.')
    raise SystemExit, 'Error: pp not installed. Refer to: "http://www.parallelpython.com"'

  n_files = len(list_files)
  if n_files == 0:
    logging.error('read: parallel_gyre_output: Input list is empty')
    raise SystemExit, 'read: parallel_gyre_output: Input list is empty'

  # tuple of all parallel python servers to connect with
  #ppservers = ('*', )

  #if isinstance(n_cpu, int):
      ## Creates jobserver with n_cpu workers
      #job_server = pp.Server(n_cpu, ppservers=ppservers)
      #job_server.set_ncpus(n_cpu)
      #print ' - read: parallel_gyre_output: Using {0} CPUs'.format(n_cpu)
  #elif n_cpu is None:
      ## Creates jobserver with automatically detected number of workers
      #job_server = pp.Server(ppservers=ppservers)
      #n_cpu = job_server.get_ncpus()
      #job_server.set_ncpus(n_cpu)
      #print ' - read: parallel_gyre_output: Using {0} CPUs'.format(job_server.get_ncpus())
  #else:
    #logging.error('read: parallel_gyre_output: n_cpu has invalid type')
    #raise SystemExit, 'read: parallel_gyre_output: n_cpu has invalid type'
  
  job_server = cm.get_pp_job_server()
  if isinstance(n_cpu, int):
    job_server.set_ncpus(n_cpu)

  # Submit the jobs in parallel, and receive the results
  start_time = time.time()
  list_output = []
  list_failed = []
  n_failed = 0
  for i_file, a_file in enumerate(list_files):
    a_job = job_server.submit( single_gyre_output, (a_file, ), (), ('gyre', ))
    if a_job() is None:
      list_failed.append(a_file)
      n_failed += 1
      continue
    list_output.append(a_job())

  dT_sec = time.time() - start_time
  dT_min = dT_sec / 60.
  dT_hr = dT_min / 60.
  if dT_sec < 60:
    dT = dT_sec
    unit = 'sec'
  elif dT_sec > 60 and dT_min < 60:
    dT = dT_min
    unit = 'min'
  else:
    dT = dT_hr
    unit = 'hr'

  print ' - read: parallel_gyre_output: Elapsed time = {0:0.2f} [{1}] \n'.format(dT, unit)
  if job_server.print_stats():
    print ' - read: parallel_gyre_output: Job Server Status: {0}'.format(job_server.print_stats())

  if n_failed > 0:
    print ' - Warning: read: parallel_gyre_output: {0} Jobs failed to read due to IOError.'.format(n_failed)
    print '    Managed to read {0} files from the total of {1}'.format(len(list_output), n_files)

  logging.info('read: parallel_gyre_output: Successfully read {0} files'.format(len(list_output)))

  return list_output

#=================================================================================================================
def parallell_mesa_h5(list_files, n_cpu=None):
  """
  Use "pp" to read MESA output HDF5 files using multiple CPUs. This is identical to read_multiple_h5().
  This function receives a large list of strings to read. Each string gives a full path to a MESA history file.
  Each file is read by the ivs.stellar_evolution.fileio.read_mesa() function; e.g.
       header, hist = read_mesa(filename=path)
  The header and the hist are both record arrays
  Each header and hist is then bundled as a key:value in a dictionary.
  @param list_files: list containing full path to all input MESA files to read. They are eaither history output files
       or profile output files. They must be in HDF5 format.
  @type list_files: list of strings
  @param n_cpu: boolean, or integer; default=None. If it is None, then pp.Server() is initialized without specifiying
       the number of CPUs. Otherwise, the number of CPUs will be passed.
  @type n_cpu: boolean or integer.
  """
  try:
    import pp
  except:
    logging.error('read: parallell_mesa_h5: pp is not installed.')
    raise SystemExit, 'Error: pp not installed. Refer to: "http://www.parallelpython.com"'

  n_files = len(list_files)
  if n_files == 0:
    logging.error('read: parallell_mesa_h5: Input list is empty')
    raise SystemExit, 'read: parallell_mesa_h5: Input list is empty'

  # tuple of all parallel python servers to connect with
  #ppservers = ('*', )

  #if isinstance(n_cpu, int):
      ## Creates jobserver with n_cpu workers
      #job_server = pp.Server(n_cpu, ppservers=ppservers)
      #job_server.set_ncpus(n_cpu)
      #print ' - read: parallell_mesa_h5: Using {0} CPUs'.format(n_cpu)
  #elif n_cpu is None:
      ## Creates jobserver with automatically detected number of workers
      #job_server = pp.Server(ppservers=ppservers)
      #print ' - read: parallell_mesa_h5: Using {0} CPUs'.format(job_server.get_ncpus())
  #else:
    #logging.error('read: parallell_mesa_h5: n_cpu has invalid type')
    #raise SystemExit, 'read: parallell_mesa_h5: n_cpu has invalid type'

  job_server = cm.get_pp_job_server()
  if isinstance(n_cpu, int):
    job_server.set_ncpus(n_cpu)

  # Submit the jobs in parallel, and receive the results
  start_time = time.time()
  list_jobs    = [ job_server.submit( read_h5, (a_h5, ), (), ('os', 'h5py', 'logging', 'numpy') ) for a_h5 in list_files]
  list_output = [job() for job in list_jobs]

  dT_sec = time.time() - start_time
  dT_min = dT_sec / 60.
  dT_hr = dT_min / 60.
  if dT_sec < 60:
    dT = dT_sec
    unit = 'sec'
  elif dT_sec > 60 and dT_min < 60:
    dT = dT_min
    unit = 'min'
  else:
    dT = dT_hr
    unit = 'hr'

  print ' - read: parallell_mesa_h5: Elapsed time = {0:0.2f} [{1}]'.format(dT, unit)
  if job_server.print_stats():
    print ' - read: parallell_mesa_h5: Job Server Status: {0}'.format(job_server.print_stats())

  return list_output

#=================================================================================================================
def read_multiple_mesa_files(files, just_header=None, is_hist = False, is_prof = False):
  """
  'Reads "multiple" MESA hist/prof files. Returns a list of dictionaries.'
  'Each item in the list is a dictionary and each dictionary is associated to'
  'each single hist/prof file.'
  'Keys are the header and column titles.'
  'Values for header keys are floats, and values for columns are numpy arrays.'
  """
  list_flags = [is_hist, is_prof]
  if all(list_flags) or not any(list_flags):
    message = 'Error: read: read_multiple_mesa_files: set ONLY "is_hist" OR "is_prof" flag to True'
    raise SystemExit, message
  if type(files) is not type([]):
    message = 'Error: read: read_multiple_mesa_files: You must provide a "list" of input filename(s)'
    raise SystemExit, message

  n_files = len(files)
  if n_files == 0:
    print 'File list is empty. Exit'
    raise SystemExit, 0

  # if is_prof:
  #   files = st.sort_list_files_by_model_number(files, extension='.prof')

  print ' - mesa_gyre.read.read_multiple_mesa_files:'

  # read the first file, and extract the datatype, and pass the dtype to read_mesa_ascii
  header, data = read_mesa_ascii(files[0])
  dtype = data.dtype

  data_lis = []
  n_skip = 0
  for i_file, mesa_file in enumerate(files):
    dic = {}
    if not os.path.isfile(mesa_file):     # Insert an empty dictionary, to keep track of the file
      print '   %s does not exist.' % (mesa_file, )
      n_skip += 1
      dic['filename'] = ''
      dic['header'] = 0.0
      if is_hist: dic['hist'] = np.rec.fromarrays([0], names=['empty'])
      if is_prof: dic['prof'] = np.rec.fromarrays([0], names=['empty'])
      continue

    header, data = read_mesa_ascii(mesa_file, dtype=dtype)
    dic['filename'] = mesa_file
    dic['header'] = header
    if is_hist: dic['hist'] = data
    if is_prof: dic['prof'] = data
    data_lis.append(dic)
    if n_files<10:
      print '   Read: %s, N_columns: %s, N_lines: %s' % (mesa_file, len(data.dtype.names), len(data))

  if n_skip>0: print '   %s files were skipped reading. \n' % (n_skip, )
  else: print '   All %s files successfully read.\n' % (n_files, )

  return data_lis

#=================================================================================================================
def read_single_mesa_file(qu, files, is_hist = True, is_prof = False):
  'Reads "single" MESA hist/prof file. Returns a dictionary. Keys are the header'
  'and column titles. Values for header keys are floats, and values for columns'
  'are numpy arrays.'

  if not (is_hist != is_prof):
    print 'Error: read: read_single_mesa_file: Problem with optional argument is_hist or is_prof.'
    print '       Choose is_hist or is_prof, and set them explicitly in the calling argument.'
    print '       Exit'
    raise SystemExit, 0

  n_files = len(files)

  if n_files == 0:
    print 'Error: read.py: read_single_mesa_file: n_files = 0'
    raise SystemExit, 0
  else:
    file = files[0]

  int_type = type(1)
  float_type = type(1.)
  qu_type = type(Queue.Queue)
  arg_type = type(qu)
  if arg_type is not int_type or arg_type is not float_type: Reads_by_Multi_Thrd = False
  if arg_type is qu_type: Reads_by_Multi_Thrd = True

  path_base = cm.set_paths()['path_base']
  len_path_base = len(path_base)
  n_lines = get_file_lines(file) - 6
  print 'Reading: ', file[len_path_base:], ' ... ', n_lines

  dic_prefix = cm.settings()
  if is_hist:
    prefix = dic_prefix['hist_prefix']
  if is_prof:
    prefix = dic_prefix['prof_prefix']
  len_prefix = len(prefix)

  fid = open(file, 'r')              # open the single file

  hdr_numb = fid.readline()          # read header column numbers
  hdr_numb = hdr_numb.split()
  n_hdr = len(hdr_numb)
  for i in range(n_hdr):
    hdr_numb[i] = int(hdr_numb[i])

  hdr_name = fid.readline()          # read header column names
  hdr_name = hdr_name.split()

  hdr_vals = fid.readline()          # read header column values
  hdr_vals = hdr_vals.split()
  for i in range(n_hdr):
    hdr_vals[i] = float(hdr_vals[i])


  fid.readline()                     # skip one line

  col_numb = fid.readline()          # read column numbers
  col_numb = col_numb.split()

  col_name = fid.readline()          # read column names
  col_name = col_name.split()
  n_col = len(col_numb)              # number of columns in file

  mtrx = np.zeros((n_lines, n_col))  # a 2D array to store the data
  line_float = np.zeros(n_col)       # a 1D array to read the data line by line

  i_line = 0                         # Now, read line by line and save in mtrx
  for line in fid:
    line = line.strip()
    columns = line.split()
    for i_col in range(n_col):
      line_float[i_col] = float(columns[i_col])
    mtrx[i_line, 0:n_col-1] = line_float[0:n_col-1]
    i_line += 1

  hdr_dic = {}
  for i in range(n_hdr):
    hdr_dic[hdr_name[i]] = hdr_vals[i]

  col_dic = {}
  for i_col in range(n_col):
    col_dic[col_name[i_col]] = mtrx[0:n_lines-1, i_col]

  #print hdr_dic

  fid.close()                     # close the single file

  dic_info = {}
  dic_info = {'filename':file, 'n_hdr':n_hdr, 'n_col':n_col, 'n_lines':n_lines}
  dic_param = pt.param_to_str()
  dic_info['mass_from'] = dic_param['mass_from']
  dic_info['eta_from']  = dic_param['eta_from']
  dic_info['ov_from']   = dic_param['ov_from']
  dic_info['sc_from']   = dic_param['sc_from']
  dic_info['z_from']    = dic_param['z_from']
  dic = dict(dic_info.items() + hdr_dic.items() + col_dic.items())

  if Reads_by_Multi_Thrd:
    qu.put(dic)
  else:
    return dic

#=================================================================================================================
def read_gyre_in(filename, h5=False):
  """
  Read input model to GYRE in ASCII or HDF5 (GSM) format.
  @param filename: full path to the input GYRE model
  @type filename: string
  @param h5: whether or not the input file is ascii (False) or HDF5 (GSM, True); default=False
  @type h5: boolean
  @return: dictionary with these keys and values:
   - nn: number of mesh
   - M_star: total mass in gr
   - R_star: total radius in cm
   - L_star: total luminosity in cgs
   - ncol: number of data columns; default=19
   - gyre_in: numpy record array giving the data
  @rtype: dictionary
  """
  if not os.path.exists(filename):
    message = 'Error: read: read_gyre_in: {0} does not exist'.format(filename)
    raise SystemExit, message

  if h5:
    message = 'Error: read: read_gyre_in: reading GSM/HDF5 is not supported yet'
    raise SystemExit, message
  else:
    dic = {}
    col_names = 'zone, radius, q_div_xq, luminosity, pressure, temperature, density, gradT, brunt_N2, gamma1, grada,' \
                 'chiT_div_chiRho, kappa, kappa_T, kappa_rho, epsilon, epsilon_T, epsilon_rho, omega'

    with open(filename, 'r') as f: lines = f.readlines()
    header = lines[0].rstrip('\r\n').split()
    nn     = int(header[0])
    M_star = float(header[1])
    R_star = float(header[2])
    L_star = float(header[3])
    ncol   = int(header[4])

    col_dtype = 'i8,' + 'f8,' * (ncol - 1)

    data   = []
    for line in lines[1:]:
      splitted = line.rstrip('\r\n').split()
      float_line = []
      float_line.append(int(splitted[0]))
      for i in range(1, ncol): float_line.append(float(splitted[i].replace('D', 'e')))
      data.append(float_line)

    recarr = np.core.records.fromarrays(np.array(data).transpose(), names=col_names, formats=col_dtype[:-1])

    dic['nn']     = nn
    dic['M_star'] = M_star
    dic['R_star'] = R_star
    dic['L_star'] = L_star
    dic['ncol']   = ncol
    dic['gyre_in']= recarr

  return dic

#=================================================================================================================
def read_osc(filename):
  """
  GraCo supports ESTA OSC format. This function reads in a given GraCo OSC model, and return a dictionary with data
  @param filename: full path to the input file
  @type filename: string
  @return: dictionary with the following keys:
    - nn
    - M_star
    - R_star
    - L_star
    - Z_ini
    - X_ini
    - a_mlt
    - Xc
    - Yc
    - Age
    - omega
    - Teff
    - osc: the value for this key is a numpy record array with all structure data
  @rtype: dictionary
  """
  if not os.path.exists(filename):
    message = 'Error: read: read_osc: {0} does not exist'.format(filename)
    raise SystemExit, message

  col_names = 'radius, lnq, temperature, pressure, density, gradT, luminosity, kappa, epsilon, gamma1,' \
              'grada, chiT_div_chiRho, Cp, ln_free_e, A2, omega, kappa_T, kappa_rho, epsilon_T, epsilon_rho,' \
              'Pg_div_P, gradr, h1, h2, he3, he4, li7, be7, c12, c13, n14, n15, o16, o17, be9, si28'
  col_dtype = 'f8,' * 36

  with open(filename, 'r') as f: lines = f.readlines()
  nn     = int(lines[5].rstrip('\r\n').split()[0])
  M_star = float(lines[6].rstrip('\r\n').split()[0])
  R_star = float(lines[6].rstrip('\r\n').split()[1])
  L_star = float(lines[6].rstrip('\r\n').split()[2])
  Z_ini  = float(lines[6].rstrip('\r\n').split()[3])
  X_ini  = float(lines[6].rstrip('\r\n').split()[4])
  a_mlt  = float(lines[7].rstrip('\r\n').split()[0])
  Xc     = float(lines[7].rstrip('\r\n').split()[1])
  Yc     = float(lines[7].rstrip('\r\n').split()[2])
  age    = float(lines[8].rstrip('\r\n').split()[0])
  omega  = float(lines[8].rstrip('\r\n').split()[1])
  Teff   = float(lines[8].rstrip('\r\n').split()[3])

  data   = []
  for i_block in range(nn):
    row = []
    for j in range(7):
      i_line = 9 + i_block * 8 + j
      #splitted = lines[i_line].rstrip('\r\n').split()
      a = lines[i_line]
      splitted = '{0} {1} {2} {3} {4}'.format(a[0:16], a[16:32], a[32:48], a[48:64], a[64:80]).split()
      for i_col in range(5): row.append(float(splitted[i_col]))
    i_line += 1
    row.append(float(lines[i_line]))
    data.append(row)

  rec = np.core.records.fromarrays(np.array(data).transpose(), names=col_names, formats=col_dtype[:-1])

  dic = {}
  dic['nn']     = nn
  dic['M_star'] = M_star
  dic['R_star'] = R_star
  dic['L_star'] = L_star
  dic['Z_ini']  = Z_ini
  dic['X_ini']  = X_ini
  dic['a_mlt']  = a_mlt
  dic['Xc']     = Xc
  dic['Yc']     = Yc
  dic['Age']    = age
  dic['omega']  = omega
  dic['Teff']   = Teff
  dic['osc']    = rec

  return dic

#=================================================================================================================
def read_ekstrom(list_files):
  """
  Read the evolutionary tracks published by Ekstrom et al. 2012, A&A, "Grids of stellar models with rotation -
  I. Models from 0.8 to 120 Msun at solar metallicity (Z = 0.014)"
  The track files have a strange problem; there are two lg(Teff) columns! We mark the second as "bad"
  @param list_files: list of strings giving full path to each track file
  @type list_files: list of strings
  @return: list of dictionaries giving the data content as a numpy record array
  @rtype: list of dictionaries
  """
  n_files = len(list_files)
  if n_files == 0:
    logging.error('Error: read: read_ekstrom: Input list is empty!')
    raise SystemExit

  list_dic = []
  for i_f, f in enumerate(list_files):
    if not os.path.isfile(f):
      logging.warning('Warning: read: read_ekstrom: {0} is missing'.format(f))
      print ' - Warning: read: read_ekstrom: {0} is missing'.format(f)
      continue
    with open(f, 'r') as r: lines = r.readlines()
    n_lines = len(lines)
    header  = lines[0].rstrip('\r\n').split()
    n_hdr   = len(header)
    ind_bad = [i for i in range(n_hdr) if header[i] == 'lg(Teff)']
    header[ind_bad[-1]] = 'bad'
    units   = lines[1]
    junk    = lines[2]

    data    = []
    for i_line, a_line in enumerate(lines[3:]):
      a_line = a_line.rstrip('\r\n').split()
      data.append(a_line)

    rec = np.core.records.fromarrays(np.array(data, float).T, names=header)

    dic = {}
    dic['filename'] = f
    dic['hist'] = rec

    list_dic.append(dic)

  return list_dic

#=================================================================================================================
def chisq_table(filename):
  """
  To read the contents of the ASCII file containing the chi-square values of a gird. Such a table can be produced 
  by e.g. write.list_chisq_to_ascii(). The header of the list should serve at the column names. All values are 
  actually single precision, but they are treated as double precision.
  @param filename: full path to the ASCII table
  @type filename: string 
  @return: numpy array with each row standing for a row in the file, and each column having the name that appears
      in the header. All fields are single precision, but stored as double precision
  @rtype: numpy record array
  """
  if not os.path.exists(filename):
    logging.error('read: chisq_table: {0} does not exist'.format(filename))
    raise SystemExit

  with open(filename, 'r') as r: lines = r.readlines()

  header = lines.pop(0).rstrip('\r\n').split()
  i_evol = header.index('Evol')
  dtype  = [(name, float) for name in header[:i_evol]] + [('Evol', '|S4')] + [(name, float) for name in header[i_evol+1:]]
  if 'num' in header:
    i_num  = dtype.index(('num', float))
    # dtype[i_num] = ('num', int)
  n_cols = len(dtype)

  data   = []
  for i, line in enumerate(lines):
    line = line.rstrip('\r\n').split()
    if len(line) != n_cols: 
      print i, len(line), len(dtype)
      print line
      print header
      raise SystemExit, 'Error: read: chisq_table: Mismatch in the number of columns'
    vals = [float(val) for val in line[:i_evol]] + [line[i_evol]] + [float(val) for val in line[i_evol+1:]]
    if 'num' in header: vals[i_num] = int(vals[i_num])
    data.append(vals)

  recarr = np.core.records.fromarrays(np.array(data).transpose(), dtype=dtype)
  if 'num' in header:
    recarr['num'] = recarr['num'].astype(np.int32, copy=True)

  logging.info('read: chisq_table: {0} successfully read and returned as a record array'.format(filename))
  print ' - read: chisq_table: {0} successfully read and returned as a record array'.format(filename)

  return recarr

#=================================================================================================================
def merge_multiple_chisq_tables(list_files):
  """
  Due to the immense number of files in different grids, the computation of chi-square scores, and immediate 
  storage of the results on the disk is not easily feasible. Thus, one solution is to chop it off into smaller
  pieces as ascii tables, and then "merge" the results back. This routine reads the ASCII output similar to 
  read.chisq_table(), keeps only the header of the first file, and returns a numpy record array.
  @param list_files: full path to all files that have to be merged. 
  @type list_files: list of strings
  @return: numpy record array with the data part of the all files included, and with the column names taken from 
      the header of the first file. Note that the output merged array is just a stack of all record arrays passed
      with their filenames; therefore, you need to "sort" the output array explicitly after calling this routine.
  @rtype: numpy record array 
  """
  n_files = len(list_files)
  if n_files == 0:
    logging.error('read: merge_multiple_chisq_tables: Input list is empty')
    raise SystemExit, 'Error: read: merge_multiple_chisq_tables: Input list is empty'

  for f in list_files:
    if not os.path.exists(f):
      logging.error('read: merge_multiple_chisq_tables: {0} does not exist'.format(f))
      raise SystemExit, 'Error: read: merge_multiple_chisq_tables: {0} does not exist'.format(f)

  list_recarr = []
  list_n_rows = []
  n_rows      = 0
  for f in list_files:
    recarr = chisq_table(f)
    n_rec  = len(recarr)
    n_rows += n_rec
    list_n_rows.append(n_rec)
    list_recarr.append( recarr )

  main_dtype  = list_recarr[0].dtype
  main_header = main_dtype.names
  for arr in list_recarr:
    arr_names = arr.dtype.names
    arr_dtype = arr.dtype
    try: 
      assert arr_dtype == main_dtype
    except AssertionError: 
      print main_dtype
      print arr_dtype
      logging.error('read: merge_multiple_chisq_tables: Inconsistent array dtype(s)! Fatal ...')
      raise SystemExit, 'Error: read: merge_multiple_chisq_tables: Inconsistent array dtype(s)! Fatal ...'

  merge = np.empty(n_rows, dtype=main_dtype, order='C')
  cnt   = 0
  for i_arr, arr in enumerate(list_recarr):
    ind_from = cnt 
    size_arr = list_n_rows[i_arr]
    ind_to   = cnt + size_arr
    merge[ind_from : ind_to] = arr[0 : size_arr]
    # merge[cnt : cnt+list_n_rows[i_arr]] = arr[0 : list_n_rows[i_arr]]
    cnt += size_arr

  logging.info('read: merge_multiple_chisq_tables: merged {0} files into one with {1} rows'.format(n_files, n_rows))
  print ' - read: merge_multiple_chisq_tables: merged {0} files into one with {1} rows'.format(n_files, n_rows)
  for i, r in enumerate(list_recarr):
    print '   {0} has {1} rows'.format(list_files[i], len(r))
  print

  return merge

#=================================================================================================================
def extend_recarray_vertically(list_recarr):
  """
  Similar to numpy.vstack(), one can stack a list of numpy.recarray and create a single merged record array by 
  calling this routine. We check that the dtype of all ingredient arrays are identical. The dtype of the first
  record array in the input list, list_recarr, is used as the dtype of the resulting merged record array.
  @param list_recarr: list of record arrays. This can be used by iterative calls to read.chisq_table().
  @type list_recarr: list of numpy.recarray
  @return: merged record array that contains all input recarrays passed through list_recarr
  @rtype: numpy.recarray
  """
#=================================================================================================================
def get_file_lines(filename):
  'Receives the filename, and enumerates the number of lines in the file.'

  fid = open(filename)
  fread = fid.read()
  fsplit = fread.split('\n')
  n_lines = len(fsplit)
  fid.close()

  return n_lines

#=================================================================================================================
def check_key_exists(dic, required_keys):
  #'Compares the keys in dic with the list of required_keys.
  #Returns a dictionary with True/False boolean logicals for
  #the fields that are present/missing.'

  keys = dic.keys()
  n_keys = len(keys)
  n_req_keys = len(required_keys)
  key_exists = []
  for i_req, which_req_key in enumerate(required_keys):
    for i_key, which_key in enumerate(keys):
      comp_two = cmp(which_req_key, which_key)
      flag = False
      if comp_two == 0:
        flag = True
        break
    key_exists.append(flag)

  flag_dic = {}
  for i, key in enumerate(required_keys):
    flag_dic[key] = key_exists[i]

  return flag_dic

#=================================================================================================================
def read_przybilla(filename):
  """
  Read the file przybilla.list into a numpy record array
  """
  if not os.path.exists(filename):
    logging.error('Error: read_przybilla: {0} does not exist'.format(filename))
    raise SystemExit, 'Error: read_przybilla: {0} does not exist'.format(filename)

  dtype = [('name', 'S8'), ('Teff', 'f8'), ('Teff_err', 'f8'), ('logg', 'f8'), ('logg_err', 'f8'), 
           ('vsini', 'f8'), ('vsini_err', 'f8'), ('Ys', 'f8'), ('Ys_err', 'f8'), ('eps_C', 'f8'),
           ('eps_C_err', 'f8'), ('eps_N', 'f8'), ('eps_N_err', 'f8'), ('eps_O', 'f8'), 
           ('eps_O_err', 'f8'), ('eps_CNO', 'f8'), ('eps_CNO_err', 'f8'), ('M', 'f8')]
  with open(filename, 'r') as r: lines = r.readlines()
  nlines= len(lines)
  # rec   = np.empty(nlines, dtype)
  data  = []
  for i, line in enumerate(lines):
    row = line.rstrip('\r\n').split()
    data.append(row)

  rec   = np.core.records.fromarrays(np.array(data).transpose(), dtype=dtype)

  return rec

#=================================================================================================================
def add_stars(filename):
  """
  Read lists of observed stars from different catalogs/files.
  @param filename: is path to the file.
  @type filename: string
  @return: data is a record array that can be referenced by line like data[2]
           or by column name like data['Teff']
  @rtype: numpy record array
  """

  data = ascii.read2recarray(filename)

  return data


#=================================================================================================================
def read_multiple_gyre_files(filenames, add_extra=False):
  """
  Return a list of dictionaries with each dictionary containng all information from each GYRE output file.
  The available keys for each output dictionary is the same as the keys returned by dic and the names of loc record array
  returned by gyre.read_output; thus, we do no longer use numpy record arrays
  This routine reads more than one gyre files. For reading a single file, it uses the gyre.py module
  @param filenames: list of filenames; one line per each file
  @type filenames: list of strings
  @return: dictionary and record array
  @rtype: list of dictionaries and record array
  """
  import param_tools
  import gyre

  n_files = len(filenames)
  #dic_gyre = []
  loc_gyre = []
  n_modes = 0

  if n_files == 1:
    file = filenames[0]
    dic_gyre, loc_gyre = gyre.read_output(file)
    n_modes = len(loc_gyre)

    dic = param_tools.get_param_from_single_gyre_filename(file)
    for name in dic_gyre.keys(): dic[name] = dic_gyre[name]
    for name in loc_gyre.dtype.names: dic[name] = loc_gyre[name]
    return [dic]

  if n_files > 1:
    list_output = []
    #filenames = st.sort_list_files_by_model_number(filenames, extension='.h5')

    n_failed = 0
    list_failed = []
    for i_file in range(n_files):
      file = filenames[i_file]
      dic = param_tools.get_param_from_single_gyre_filename(file)

      try:
        dic_single_gyre, loc_single_gyre = gyre.read_output(file)
      except IOError:
        n_failed += 1
        print '   read_multiple_gyre_files: IOError: {0}'.format(file)
        list_failed.append(file)
        continue
      except ValueError:
        n_failed += 1
        print '   read_multiple_gyre_files: ValueError: {0}'.format(file)
        list_failed.append(file)
        continue
      n_modes += len(loc_single_gyre)

      if dic_single_gyre:
        for key, value in dic_single_gyre.items(): dic[key] = value

      names = loc_single_gyre.dtype.names
      for name in names:
        dic[name] = loc_single_gyre[name]

      list_output.append(dic)
      param_tools.update_progress(i_file/float(n_files))

  # print '\n - read_multiple_gyre_files: Field names in the record array: '
  #print loc_single_gyre.dtype.names
  print ' - Total number of modes in the whole archive = %s' % (n_modes, )
  if n_files>1 and n_failed > 0:
    print ' - %s Jobs Failed to read.\n' % (n_failed, )

  return list_output

#=================================================================================================================
def get_dtype(list_names):
  """
  Return hard-coded data types for data read from MESA history files or profiles.
  We treat integer columns as integer (np.int32), 
  except they belong to the list of double precision columns (np.float64).
  @param list_names: list of column names from the data block of the MESA history or profile file
  @type list_names: list of strings
  @return: numpy-compatible list of tuples for data type, each tuple is associated with a column, and has two elements.
       The first element is the column name, and the second is the numpy data type
  @rtype: list of tuples
  """
  n_col = len(list_names)
  if n_col == 0:
    message = 'get_dtype: empty input list of names'
    logging.error(message)
    raise SystemExit, message

  set_int8_columns  = get_mesa_int8_col_names()    # for byte integer columns -128 < int < 128
  set_int16_columns = get_mesa_int16_col_names()   # for integer columns -32768 < int < 32767
  set_int32_columns = get_mesa_int32_col_names()   # for long integers -2147483648 < int < 2147483647
  set_dp_columns    = get_mesa_dp_col_names()

  list_tup_out = []
  for i_tup, name in enumerate(list_names):
    if name in set_int8_columns:
      list_tup_out.append( (name, np.int8) )
    elif name in set_int16_columns:
      list_tup_out.append( (name, np.int16) )
    elif name in set_int32_columns:
      list_tup_out.append( (name, np.int32) )
    elif name in set_dp_columns:
      list_tup_out.append( (name, np.float64) )
    else:
      list_tup_out.append( (name, np.float64) )

  return np.dtype(list_tup_out)

#=================================================================================================================
def get_mesa_int8_col_names():
  """
  Return the names of those columns that contain integer values in MESA history or profile files. They are listed
  in <mesa>/star/defaults/history_columns.list and <mesa>/star/defaults/profile_columns.list
  @return: set of strings with those columns that contain integer values in MESA
  @rtype: set
  """
  output = set([ 'mix_type_1', 'mix_type_2', 'mix_type_3', 'mix_type_4', 'mix_type_5', 'mix_type_6',             # hist
               'burn_type_1', 'burn_type_2', 'burn_type_3', 'burn_type_4', 'burn_type_5', 'burn_type_6',         # hist
               'mixing_type', 'mlt_mixing_type', 'sch_stable', 'ledoux_stable', 'stability_type'                 # prof
                ] )

  return output

#=================================================================================================================
def get_mesa_int16_col_names():
  """
  Return the names of those columns that contain integer values in MESA history or profile files. They are listed
  in <mesa>/star/defaults/history_columns.list and <mesa>/star/defaults/profile_columns.list
  @return: set of strings with those columns that contain integer values in MESA
  @rtype: set
  """
  output = set([ 'num_zones', 'cz_zone', 'cz_top_zone', 'num_backups', 'num_retries', 'zone'] )

  return output

#=================================================================================================================
def get_mesa_int32_col_names():
  """
  Return the names of those columns that contain integer values in MESA history or profile files. They are listed
  in <mesa>/star/defaults/history_columns.list and <mesa>/star/defaults/profile_columns.list
  @return: set of strings with those columns that contain integer values in MESA
  @rtype: set
  """
  output = set([ 'model_number', 'version_number', 'nse_fraction' ] )

  return output

#=================================================================================================================
def get_mesa_dp_col_names():
  """
  Return the names of those columns that contain double precision floats in MESA history or profile files. They are listed
  in <mesa>/star/defaults/history_columns.list and <mesa>/star/defaults/profile_columns.list
  These are those specific columns in the profiles that are used for the purpose of generating GYRE or GraCo input files.
  Since these codes require double precision input, these columns have to be preserved in double precision format.
  @return: set of strings with those columns that contain float values in MESA
  @rtype: set
  """
  output = set(['radius', 'mass', 'dq', 'eps_grav', 'cp', 'cv', 'Cv', 'brunt_N2_composition_term', 'lamb_S2',
                'q_div_xq', 'chiT_div_chiRho', 'kappa', 'kappa_rho', 'kappa_T', 'epsilon', 'epsilon_rho', 'epsilon_T',
                'omega', 'pressure', 'logP', 'pgas', 'prad', 'density', 'logRho', 'temperature', 'logT', 'luminosity',
                'luminosity_rad', 'luminosity_conv', 'gamma1', 'gradT', 'grada', 'brunt_N2', 'lnq', 'Cp', 'ln_free_e',
                'A2', 'Pg_div_P', 'gradr', 'h1', 'h2', 'he3', 'he4', 'li7', 'be7', 'c12', 'c13', 'n14', 'n15', 'o16',
                'o17', 'be9', 'si28' ])

  return output

#=================================================================================================================
def read_mesa_ascii(filename, dtype=None):
  """
  Read an history or profile ascii output from MESA.
  @param filename: full path to the input ascii file
  @type filename: string
  @param dtype: numpy-compatible dtype object. if it is not provided, it will be retrieved from read.get_dtype()
  @type dtype: list of tuples
  @return dictionary of the header of the file, and the record array for the data block. It can be called like this
     >>> header, data = read.read_mesa_ascii('filename')
  @rtype: dictionary and numpy record array
  """
  if not os.path.isfile(filename):
    message = 'read_mesa_ascii: {0} does not exist'.format(filename)
    logging.error(message)
    raise SystemExit, message

  if not check_file_is_ascii(filename):
    message = 'read_mesa_ascii: {0} is not ascii'.format(filename)
    logging.error(message)
    #raise SystemExit, message

  with open(filename, 'r') as r: lines = r.readlines()
  logging.info('read_mesa_ascii: {0} is read into list of lines'.format(filename))

  skip          = lines.pop(0)
  header_names  = lines.pop(0).rstrip('\r\n').split()
  header_vals   = lines.pop(0).rstrip('\r\n').split()
  temp          = np.array([header_vals], float).T
  header        = np.core.records.fromarrays(temp, names=header_names)
  skip          = lines.pop(0)
  skip          = lines.pop(0)

  col_names     = lines.pop(0).rstrip('\r\n').split()
  n_cols        = len(col_names)
  if dtype is None:
    col_dtype   = get_dtype(col_names)
  else:
    col_dtype   = dtype
  if n_cols != len(col_dtype):
    message = 'read_mesa_ascii: Incompatible number of provided datatype objects'
    logging.error(message)
    raise SystemExit, message

  data          = []
  for i_line, line in enumerate(lines):
    if not line.rstrip('\r\n').split(): continue  # skip empty lines
    data.append(line.rstrip('\r\n').split())

  data = np.core.records.fromarrays(np.array(data, float).transpose(), dtype=col_dtype)

  return header, data

#=================================================================================================================
def check_file_is_ascii(filename):
  """
  Check if the file is trely ascii. Sometimes the files are corrupted on the VSC at the instance of writing to the disk. 
  The file header is ruined, and and ascii file looks like a binary file to the OS. 
  For the Windows Operating System (and different variants), we ignore this test, because the 
  "file" is not a respected command in Microsoft Windows command prompt. In this case, we return
  True, and bypass the file checks.
  @param filename: full path to the file to be checked
  @type filename: string
  @return: flag about the file being ascii (True) or not (False)
  @rtype: boolean
  """
  operating_system = sys.platform
  if 'win' in operating_system: return True 

  cmd = 'file ' + filename
  file_type = os.popen(cmd).readlines()[0]
  if 'ASCII text' in file_type: 
    flag = True
  elif 'ERROR: line 163: regex error 17, (illegal byte sequence)' in file_type:
    flag = True
  else:
    flag = False
    
  return flag

#=================================================================================================================
#=================================================================================================================


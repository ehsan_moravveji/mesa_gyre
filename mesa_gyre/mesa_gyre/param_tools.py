
import commons as comm
import read as rd
import gyre

import os, time, sys
import numpy as np
from collections import OrderedDict

Z_sun = 0.0134   # from Asplund et al. (2009, ARA&A)
X_sun = 0.7381   # from Asplund et al. (2009, ARA&A)

#=================================================================================================================
def param_to_str():

 param = comm.set_srch_param()

 str_mass_from = str(param['mass_from'])
 str_mass_to   = str(param['mass_to'])
 str_mass_step = str(param['mass_step'])

 str_eta_from  = str(param['eta_from'])
 str_eta_to    = str(param['eta_to'])
 str_eta_step  = str(param['eta_step'])

 str_ov_from   = str(param['eta_from'])
 str_ov_to     = str(param['eta_to'])
 str_ov_step   = str(param['eta_step'])

 str_sc_from   = str(param['sc_from'])
 str_sc_to     = str(param['sc_to'])

 str_z_from    = str(param['z_from'])
 str_z_to      = str(param['z_to'])
 str_z_step    = str(param['z_step'])

 str_Xc_from   = str(param['Xc_from'])
 str_Xc_to     = str(param['Xc_to'])
 str_Xc_step   = str(param['Xc_step'])

 str_Yc_from   = str(param['Yc_from'])
 str_Yc_to     = str(param['Yc_to'])
 str_Yc_step   = str(param['Yc_step'])

 str_ell_from   = str(param['ell_from'])
 str_ell_to     = str(param['ell_to'])
 str_ell_step   = str(param['ell_step'])

 str_param = OrderedDict({})
 str_param = {'str_mass_from':str_mass_from, 'str_mass_to':str_mass_to, 'str_mass_step':str_mass_step,
        'str_eta_from':str_eta_from, 'str_eta_to':str_eta_to, 'str_eta_step':str_eta_step,
        'str_ov_from':str_ov_from, 'str_ov_to':str_ov_to, 'str_ov_step':str_ov_step,
        'str_sc_from':str_sc_from, 'str_sc_to':str_sc_to,
        'str_z_from':str_z_from, 'str_z_to':str_z_to, 'str_z_step':str_z_step}

 #dic = dict(param.items() + str_param.items())
 dic = OrderedDict({})
 for key, value in param.items(): dic[key] = value
 for key, value in str_param.items(): dic[key] = value

 return dic


#=================================================================================================================
def gen_track_srch_str():

  dic = param_to_str()

  track_srch_str = ''

  srch_str_mass = gen_str_by_range('M', dic['mass_from'], dic['mass_to'], all = dic['mass_all'], fmt = dic['mass_fmt'])
  srch_str_eta  = gen_str_by_range('eta', dic['eta_from'], dic['eta_to'], all = dic['eta_all'], fmt = dic['eta_fmt'])
  srch_str_ov   = gen_str_by_range('ov', dic['ov_from'], dic['ov_to'], all = dic['ov_all'], fmt = dic['ov_fmt'])
  srch_str_sc   = gen_str_by_range('sc', dic['sc_from'], dic['sc_to'], fmt = dic['sc_fmt'])
  srch_str_z    = gen_str_by_range('Z', dic['z_from'], dic['z_to'], all = dic['z_all'], fmt = dic['z_fmt'])

  track_srch_str = srch_str_mass + '-' + srch_str_eta + '-' + srch_str_ov + '-' + \
    srch_str_sc + '-' + srch_str_z
  dic_srch_str = OrderedDict({})
  dic_srch_str = {'srch_str_mass':srch_str_mass, 'srch_str_eta':srch_str_eta,
                    'srch_str_ov':srch_str_ov, 'srch_str_sc':srch_str_sc,
                    'srch_str_z':srch_str_z, 'track_srch_str':track_srch_str}

  dic = dict(dic.items() + dic_srch_str.items())

  return dic


#=================================================================================================================
def update_dic(key, value):
  """
  This function checks for the presence of a key:value pair in the param dictionary returned by gen_track_srch_str.
  If present, it chances the value, and returnes the updated dicitonary.
  @param key: the key in the dictionary to chenage the value
  @type key: string
  @param value: the value from the dictionary associated with the key that we are going to update
  @type value: integer, float, boolean or string
  @return: Updated dictionary with the value associated with the key being updated.
  @rtype: OrderedDict
  """
  dic = gen_track_srch_str()

  result = dic.get(key)
  if result == None and result is None:
    message = 'Error: param_tools: update_dic: The key does not exist in the dictionary!'
    raise SystemExit, message

  dic[key] = value

  return dic


#=================================================================================================================
def select_filenames_by_range(list_files, by='Xc', lower=0.10, higher=0.70, round_level=3):
  """
  From a list of filenames, take only those that a value of an identifier lies in the desired range. For instance,
  from a list of MESA profiles, only accept those with masses between 4 and 5 Msun, hence, by='M', lower=4.0, higher=5.0
  @param list_files: list of MESA or GYRE full path filenames
  @type list_files: list of files
  @param by: selection is based on this identifier. It must be a part of the input filename, default: by='Xc'
  @type by: string
  @param lower, higher: the lower and upper limit of the value of the identifier for which we accept/reject the filenames;
        default: lower=0.10 and higher=0.70
  @type lower, higher: float
  @param round_level: after extracting the identifier and converting to a float, round it up to (default=3) this
     number of significant figures
  @type round_level: integer
  @return: list of accepted files that identifier "by" has a value in a range specified from "lower" to "higher" within
     "round_level" significant figures
  @rtype: list of strings
  """
  n_files = len(list_files)
  if n_files == 0:
    message = 'Error: param_tools: select_filenames_by_range: Input list is empty!'
    raise SystemExit, message

  list_include = []
  for i_file, a_file in enumerate(list_files):
    if not os.path.isfile(a_file):
      print 'Warning: param_tools: select_filenames_by_range: %s does not exist' % (a_file, )
      continue

    if '/' in a_file:
      ind_slash = a_file.rfind('/')
      filename_core = a_file[ind_slash+1:]
    else:
      filename_core = a_file

    if by not in filename_core:
      print 'Warning: param_tools: select_filenames_by_range: filename:%s does not contain %s' % (a_file, by)
      continue

    pieces = filename_core.split('-')
    n_pieces = len(pieces)
    if n_pieces == 0:
      print 'Warning: param_tools: select_filenames_by_range: %s cannot be split by "-"' % (a_file, )
      continue

    dic_param = get_param_from_single_gyre_filename(filename_core)
    keys = dic_param.keys()
    if by not in keys:
      print 'Warning: param_tools: select_filenames_by_range: %s not among returned keys' % (by, )
      continue

    val = round(dic_param[by], round_level)
    val_in_range = (val>=lower) & (val<=higher)
    if val_in_range: list_include.append(a_file)

  if len(list_include) > 1: list_include = sorted(list_include)
  print ' - param_tools: select_filenames_by_range: From %s input files, %s match %s <= %s <= %s' % (n_files, len(list_include), lower, by, higher)

  return list_include

#=================================================================================================================
def select_filenames_by_value(list_files, by='Xc', value=0.05, round_level=3):
  """
  From a list of filenames, take only those with a specific value in the filename. e.g. Xc=0.0500
  @param list_files: list of MESA or GYRE full path filenames
  @type list_files: list of files
  @param by: selection is based on this identifier. It must be a part of the input filename, default: by='Xc'
  @type by: string
  @param value: the value of the identifier for which we accept/reject the filenames; default=0.05
  @type value: float
  @param round_level: after extracting the identifier and converting to a float, round it up to (default=3) this
     number of significant figures
  @type round_level: integer
  @return: list of accepted files that identifier "by" has the desired "value" within "round_level" significant figures
  @rtype: list of strings
  """
  n_files = len(list_files)
  if n_files == 0:
    message = 'Error: param_tools: select_filenames_by_value: Input list is empty!'
    raise SystemExit, message

  list_include = []
  for i_file, a_file in enumerate(list_files):
    if not os.path.isfile(a_file):
      print 'Warning: param_tools: select_filenames_by_value: %s does not exist' % (a_file, )
      continue

    if '/' in a_file:
      ind_slash = a_file.rfind('/')
      filename_core = a_file[ind_slash+1:]
    else:
      filename_core = a_file

    if by not in filename_core:
      print 'Warning: param_tools: select_filenames_by_value: filename:%s does not contain %s' % (a_file, by)
      continue

    pieces = filename_core.split('-')
    n_pieces = len(pieces)
    if n_pieces == 0:
      print 'Warning: param_tools: select_filenames_by_value: %s cannot be split by "-"' % (a_file, )
      continue

    dic_param = get_param_from_single_gyre_filename(filename_core)
    keys = dic_param.keys()
    if by not in keys:
      print 'Warning: param_tools: select_filenames_by_value: %s not among returned keys' % (by, )
      continue

    val = round(dic_param[by], round_level)
    if val == value: list_include.append(a_file)

  if len(list_include) > 1: list_include = sorted(list_include)
  print ' - param_tools: select_filenames_by_value: From %s input files, %s match %s = %s' % (n_files, len(list_include), by, value)

  return list_include


#=================================================================================================================
def gen_str_by_range(string, frm, to, all = False, fmt = "{}"):

  str_out = string
  diff_from = ''
  diff_to = ''

  if all == True:
    str_out = string + '*'
    return str_out

  if frm == to:
    str_out += fmt.format(frm)  # str(frm)

  if frm < to:
    frm_str = fmt.format(frm) # str(frm)
    to_str  = fmt.format(to) # str(to)
    frm_list = list(frm_str)
    to_list  = list(to_str)

    for i in range(len(frm_list)):
      if frm_list[i] == to_list[i]:
        str_out += frm_list[i]
      else:
        indx_diff = i
        diff_from = ''.join(frm_list[indx_diff:])
        diff_to   = ''.join(to_list[indx_diff:])
        break

    str_out += '[' + diff_from + '-' + diff_to + ']'

  return str_out


#=================================================================================================================
def gen_mass_dir_str():

  path_base = comm.set_paths()['path_base']

  dic = gen_track_srch_str()
  srch_dir_mass_str = dic['srch_str_mass']
  dir_mass_full_path = path_base + srch_dir_mass_str

  return dir_mass_full_path


#=================================================================================================================
def give_track_srch_str():

  dic = gen_track_srch_str()
  track_srch_str = dic['track_srch_str']

  return track_srch_str


#=================================================================================================================
def gen_hist_srch_str():

  path_mass_dir = gen_mass_dir_str() + '/hist/'

  dic_track = gen_track_srch_str()
  track_srch_str = dic_track['track_srch_str']
  hist_srch_str = track_srch_str + '.hist'
  hist_full_srch_str = path_mass_dir + hist_srch_str

  dic_hist = {'hist_srch_str':hist_srch_str, 'hist_full_srch_str':hist_full_srch_str}
  dic_track.update(dic_hist)

  return dic_hist

#=================================================================================================================
def gen_prof_srch_str():

  path_mass_dir = gen_mass_dir_str() + '/profiles/'

  dic_track = gen_track_srch_str()
  track_srch_str = dic_track['track_srch_str']
  dic_param = comm.set_srch_param()
  evol = dic_param['evol']
  Xc_from = dic_param['Xc_from']
  Xc_to   = dic_param['Xc_to']
  Xc_all  = dic_param['Xc_all']
  Yc_from = dic_param['Yc_from']
  Yc_to   = dic_param['Yc_to']
  Yc_all  = dic_param['Yc_all']

  Xc_srch_str = gen_str_by_range('Xc', Xc_from, Xc_to, all = Xc_all)
  Yc_srch_str = gen_str_by_range('Yc', Yc_from, Yc_to, all = Yc_all)

  prof_srch_str = track_srch_str + '-' + evol + '-' + Yc_srch_str + '-' + Xc_srch_str + '-' + '*.prof'
  prof_full_srch_str = path_mass_dir + prof_srch_str

  dic_prof = {'prof_srch_str':prof_srch_str, 'prof_full_srch_str':prof_full_srch_str}

  dic_track.update(dic_prof)

  return dic_prof


#=================================================================================================================
def gen_gyre_srch_str():

  path_mass_dir = gen_mass_dir_str() + '/gyre_out/'

  dic_track = gen_track_srch_str()
  track_srch_str = dic_track['track_srch_str']
  dic_param = comm.set_srch_param()
  evol = dic_param['evol']
  Xc_from = dic_param['Xc_from']
  Xc_to   = dic_param['Xc_to']
  Xc_all  = dic_param['Xc_all']
  Yc_from = dic_param['Yc_from']
  Yc_to   = dic_param['Yc_to']
  Yc_all  = dic_param['Yc_all']
  ell_from= dic_param['ell_from']
  ell_to  = dic_param['ell_to']
  ell_all = dic_param['ell_all']

  Xc_srch_str = gen_str_by_range('Xc', Xc_from, Xc_to, all = Xc_all)
  Yc_srch_str = gen_str_by_range('Yc', Yc_from, Yc_to, all = Yc_all)
  ell_srch_str = gen_str_by_range('el', ell_from, ell_to, all = ell_all)

  gyre_srch_str = track_srch_str + '-' + evol + '-' + Yc_srch_str + '-' + Xc_srch_str + \
                  '-' + ell_srch_str + '-sum.h5'
  gyre_full_srch_str = path_mass_dir + gyre_srch_str

  dic_gyre = {'gyre_srch_str':gyre_srch_str, 'gyre_full_srch_str':gyre_full_srch_str}

  dic_track.update(dic_gyre)

  return dic_gyre


#=================================================================================================================
def gen_path_plot():
  #'''
  #Based on the choice of Platform, finds the correct directory name to store the plots locally.
  #E.g. the plots for M = 12.8 Msun will be stored in this location:
  #/<path_base>/M12.8/plots/
  #Additionally, it checks for the availability of the directory and creates it if it does not exist.
  #'''

  dic_param = comm.set_srch_param()
  n_mass = dic_param['n_mass']
  mass_all = dic_param['mass_all']

  if n_mass == 1 and not mass_all:
    path_mass_dir = gen_mass_dir_str()
    path_plot = path_mass_dir + '/plots/'
  if n_mass > 1:
    path_base = comm.set_paths()['path_base']
    path_plot = path_base + 'composite-plots/'

  dir_exists = os.path.isdir(path_plot)
  if not dir_exists: os.makedirs(path_plot)

  return path_plot


#=================================================================================================================
def gen_hypothetic_h5_filename(hist_fname, evol, str_Xc, str_Yc):
  """
  This function creates a filename for every point in the grid which is inside the box of observed star's properties.
  The gyre_out full path returned will be checked for the existence and reading of the file afterwards.
  @param : all requird fields to generate a gyre output file with h5 extension.
  @return: It returns a full path to an HDF5 hypothetical file. If the file really exists in the grid, it will be
      read and appended to the data
  @rtype: string
  """

  if len(hist_fname) < 1:
    message = 'Error: param_tools: gen_hypothetic_h5_filename: Input filename %s is empty.' % (hist_fname, )
    raise SystemExit, message

  ind_slash = hist_fname.rfind('/')
  path_in = hist_fname[:ind_slash+1]
  path_out = path_in.replace('hist', 'gyre_out')
  core_name = hist_fname[ind_slash+1:-5]

  h5_core_name = core_name + '-' + evol + '-Yc' + str_Yc + '-Xc' + str_Xc + '*-sum.h5'
  h5_name = path_out + h5_core_name

  return h5_name

#=================================================================================================================
def get_param_from_single_hist_filename(filename):
  """
  This funciton receives a full path to a hist file, excludes the path to the directory where the file lives, and
  takes the core filename. Then, it extracts the physical parameters for which the hist was set up, i.e. mass, eta,
  ov, sc, and Z from the filename.
  @param filename: string giving the full path to a MESA history file.
  @type filename: string
  @return: OrderedDict with the following keys
   - M giving the initial mass of the track
   - eta: giving the initial angular rotation rate of the track with respect to the break up.
   - ov: overshooting extent f_ov in Herwig (2000) exponential prescription
   - sc: alpha_semiconvection
   - z: metalicity
  @rtype: OrderedDict
  """
  len_file = len(filename)
  if len_file < 1:
    message = 'Error: param_tools: get_param_from_single_hist_filename: Input filename is empty %s' % (filename, )
    raise SystemExit, message

  ind_slash = filename.rfind('/')
  core_name = filename[ind_slash+1:]
  ind_point = core_name.rfind('.')
  core_name = core_name[:ind_point]

  pieces = core_name.split('-')
  n_pieces = len(pieces)
  if n_pieces < 2:
    message = 'Error: param_tools: get_param_from_single_hist_filename: Something is wrong with filename %s' % (filename, )
    return None
    # raise SystemExit, message

  dic = {}
  dic['filename'] = filename
  if pieces[0][0] == 'M': dic['M'] = float(pieces[0][1:])
  for i in range(1, n_pieces):
    if 'eta'  in pieces[i]: dic['eta']  = float(pieces[i][3:]); continue
    if 'ov'   in pieces[i]: dic['ov']   = float(pieces[i][2:]); continue
    if 'sc'   in pieces[i]: dic['sc']   = float(pieces[i][2:]); continue
    if 'Z'    in pieces[i]: dic['z']    = float(pieces[i][1:]); continue
    if 'Xi'   in pieces[i]: dic['Xini'] = float('0.'+pieces[i][2:]); continue
    if 'logD' in pieces[i] and len(pieces[i])>4: dic['logD'] = float(pieces[i][4:]); continue

  return dic


#=================================================================================================================
def get_param_from_single_prof_filename(filename):
  """
  To extract the mass, eta (i.e. Omega_{eq}/Omega_{crit}), f_ov (in Herwig 2000 prescription), a_sc, Z, Yc and
  Xc from a single profile filename.
  @param filename: full path to MESA profile file
  @type filename: string
  @return: dictionary with the keys being 'mass', 'eta', 'ov', 'sc', 'Z', 'Yc' and 'Xc', and the values
           extracted from the string.
  @rtype: dictionary
  """

  # throw away the filename extension by locating the right-most point "."
  ind_p = filename.rfind('.')
  filename = filename[:ind_p]

  # throw away the base_path by locating the right-most /
  ind_slash = filename.rfind('/')
  if ind_slash < 1:
    core_name = filename[:]
  else:
    core_name = filename[ind_slash+1:]

  pieces = core_name.split('-')

  #dic = OrderedDict({})
  #dic['M'] = float(pieces[0][1:])
  #dic['eta'] = float(pieces[1][3:])
  #dic['ov'] = float(pieces[2][2:])
  #dic['sc'] = float(pieces[3][2:])
  #dic['z'] = float(pieces[4][1:])
  #dic['evol'] = pieces[5][:]
  #dic['Yc'] = float(pieces[6][2:])
  #dic['Xc'] = float(pieces[7][2:])
  #dic['model_number'] = int(pieces[8][:])

  dic = {}
  dic['filename'] = filename
  if pieces[0][0] == 'M': dic['M'] = float(pieces[0][1:])
  for i in range(1, n_pieces):
    if 'eta'  in pieces[i]: dic['eta']  = float(pieces[i][3:]); continue
    if 'ov'   in pieces[i]: dic['ov']   = float(pieces[i][2:]); continue
    if 'sc'   in pieces[i]: dic['sc']   = float(pieces[i][2:]); continue
    if 'Z'    in pieces[i]: dic['z']    = float(pieces[i][1:]); continue
    if 'Xi'   in pieces[i]: dic['Xini'] = float('0.'+pieces[i][2:]); continue
    if 'logD' in pieces[i]: dic['logD'] = float(pieces[i][4:]); continue
    if 'MS'   in pieces[i]: dic['evol'] = pieces[i][:];         continue
    if 'PMS'  in pieces[i]: dic['evol'] = pieces[i][:];         continue
    if 'ZAMS' in pieces[i]: dic['evol'] = pieces[i][:];         continue
    if 'Yc'   in pieces[i]: dic['Yc']   = float(pieces[i][2:]); continue
    if 'Xc'   in pieces[i]: dic['Xc']   = float(pieces[i][2:]); continue
  dic['model_number'] = int(pieces[-1][:])

  return dic


#=================================================================================================================
def get_param_from_single_gyre_filename(filename):
  """
  To extract the mass, eta (i.e. Omega_{eq}/Omega_{crit}), f_ov (in Herwig 2000 prescription), a_sc, Z, Yc and
  Xc from a single gyre filename, if present.
  @param filename: full path to the HDF5 GYRE file
  @type filename: string
  @return: dictionary with the keys being 'mass', 'eta', 'ov', 'sc', 'Z', 'Yc' and 'Xc', and the values
           extracted from the string. It may additionally have 'logD' as the logarithm of a constant diffusion term there
  @rtype: dictionary
  """
  #path_base = comm.set_paths()['path_base']
  #path_base = gen_mass_dir_str() + '/gyre_out/'
  #len_path_base = len(path_base)
  #len_filename = len(filename)
  #if len_filename <= len_path_base:
    #message = 'Error: param_tools: get_param_from_single_gyre_filename: filename shorter than the base path!'
    #raise SystemExit, message

  gyre_prefix = comm.settings()['gyre_prefix']
  len_gyre_prefix = len(gyre_prefix)

  if '/' in filename:
    ind_from = filename.rfind('/') + 1
  elif filename[0:2] == './':
    ind_from = 2
  elif filename[0] == '.':
    ind_from = 1
  elif '/' not in filename:
    ind_from = 0

  #ind_to   = -len_gyre_prefix
  ind_to     = filename.rfind('.')

  fname_core = filename[ind_from : ind_to]
  pieces  = fname_core.split('-')
  if 'ad'  in pieces: pieces.remove('ad')
  if 'nad' in pieces: pieces.remove('nad')
  if 'sum' in pieces: pieces.remove('sum')
  if 'eig' in pieces: pieces.remove('eig')
  n_pieces= len(pieces)
  numbers = '0123456789'
  dic = {}
  dic['filename'] = filename
  
  for i in range(n_pieces):
    if pieces[i] == 'ad': continue
    if pieces[i] == 'nad': continue
    if pieces[i] == 'sum': continue
    if pieces[i] == 'eig': continue

    if pieces[i][0] == 'M' and pieces[i][1] in numbers: dic['M'] = float(pieces[i][1:]); continue
    if 'Z'    in pieces[i] and pieces[i][1] in numbers: dic['Z'] = float(pieces[i][1:]); continue
    if 'eta'  in pieces[i]: dic['eta']  = float(pieces[i][3:]); continue
    if 'ov'   in pieces[i]: dic['ov']   = float(pieces[i][2:]); continue
    if 'sc'   in pieces[i]: dic['sc']   = float(pieces[i][2:]); continue
    if 'MS'   in pieces[i]: dic['evol'] = pieces[i][:];         continue
    if 'PMS'  in pieces[i]: dic['evol'] = pieces[i][:];         continue
    if 'ZAMS' in pieces[i]: dic['evol'] = pieces[i][:];         continue
    if 'Yc'   in pieces[i]: dic['Yc']   = float(pieces[i][2:]); continue
    if 'Xc'   in pieces[i]: dic['Xc']   = float(pieces[i][2:]); continue
    if 'Xini' in pieces[i]: dic['Xini'] = float('0.'+pieces[i][4:]); continue
    if 'Xi'   in pieces[i]: dic['Xini'] = float('0.'+pieces[i][2:]); continue
    if 'logD' in pieces[i] and len(pieces[i])>4: dic['logD'] = float(pieces[i][4:]); continue
    if pieces[-1][0] in numbers and pieces[-1][1] in numbers: dic['model_number'] = int(pieces[-1][:])

  return dic

#=================================================================================================================
def find_hist_for_gyre_file(gyre_file):
  """
  A GYRE output file comes from a saved .gyre file from a track. This routine finds the correct hist file
  for a given GYRE HDF5 file based on the similarities in their filename. For instance, the correct hist file
  for the GYRE file: "M12.8-eta0.08-ov0.015-sc0.01-Z0.012-MS-Yc0.4800-Xc0.5080-el2-sum.h5" is
  "M12.8-eta0.08-ov0.015-sc0.01-Z0.012.hist".
  Another function will find the best evolution instant in this hist file matching the GYRE file.
  @param gyre_file: the full path to the GYRE file
  @type gyre_file: string
  @return: a string specifying the full path to the .hist file associated with the GYRE file
  @rtype: string
  """

  path_base = comm.set_paths()['path_base']
  len_path_base = len(path_base)
  new_path = gyre_file.replace('gyre_out', 'hist')
  len_file = len(new_path)
  len_hist_file = len_file - 32
  hist_file = new_path[0:len_hist_file] + '.hist'

  return hist_file

#=================================================================================================================
def find_prof_for_gyre_file(gyre_file):
  """
  A GYRE output file comes from a saved .gyre file from a track. This routine finds the correct profile file
  for a given GYRE HDF5 file based on the similarities in their filename. For instance, the correct profile file
  for the GYRE file: "M12.8-eta0.08-ov0.015-sc0.01-Z0.012-MS-Yc0.4800-Xc0.5080-el2-sum.h5" is
  "M12.8-eta0.08-ov0.015-sc0.01-Z0.012-MS-Yc0.4800-Xc0.5080-01234.prof".
  @param gyre_file: full path to the GYRE output file.
  @type gyre_file: string
  @return: finds the appropriate profile filename, that is associated with the GYRE output file.
  @rtype: string
  """

  len_gyre_file = len(gyre_file)
  new_file = gyre_file.replace('gyre_out', 'profiles')

#=================================================================================================================
def get_evol_for_gyre_file(gyre_file):
  """
  Each GYRE output file comes from a snapshot of a model during an evolution of a given stellar model.
  This function receives the full path to a GYRE output filename; then, it calls find_hist_for_gyre_file()
  to fetch the correct history filename, and opens it to find the best-matching line in the file that gives
  the closest match to Xc and Yc.
  @param gyre_file: the full path to the GYRE output file
  @type gyre_file: string
  @return: dictionary with all GYRE output file information, including the evolutionary information
  @rtype: dictionary
  """

  # Read the GYRE output file, and store the data as a dictionary and a numpy record array
  dic_this_gyre, loc_this_gyre = gyre.read_output(gyre_file)
  # Exctract the physical parameters fromt he GYRE filename
  dic_gyre_param = get_param_from_single_gyre_filename(gyre_file)
  # Find the correct hist file associated to this GYRE output file
  hist_file = find_hist_for_gyre_file(gyre_file)
  # Read this single hist file, and store the column data in a dictionary
  hist = rd.read_single_mesa_file(1, [hist_file], is_hist = True, is_prof = False)

  gyre_Xc = dic_gyre_param['Xc']
  gyre_Yc = dic_gyre_param['Yc']

  hist_Xc = hist['center_h1']
  hist_Yc = hist['center_he4']

  Xc_srch_arr = np.absolute(hist_Xc - gyre_Xc)
  Yc_srch_arr = np.absolute(hist_Yc - gyre_Yc)

  Xc_min_indx = np.unravel_index(Xc_srch_arr.argmin(), Xc_srch_arr.shape)
  Yc_min_indx = np.unravel_index(Yc_srch_arr.argmin(), Yc_srch_arr.shape)

  if Xc_min_indx != Yc_min_indx:
    print 'Warning: param_tools: find_evol_for_gyre_file: Could not find a single line in hist file for the GYRE file.'
    return {}
  else:
    hist_line = int(''.join(map(str, Xc_min_indx)))

  #which_hist_line = hist[]



  return None

#=================================================================================================================
def get_prof_for_gyre_by_Xc(gyre_file, Xc = 0.50):
  """
  To find the MESA profile file that describes the internal structure of the model for each specific
  GYRE output file. This is done by the center mass fraction of hydrogen, i.e. Xc.

  """

#=================================================================================================================
def get_name_for_axis(name):
  """
  Assign an appropriate short name for plotting to the input large name. This is used in plot_gyre.check_dP_correlates
  @param name: axis name. They are mainly numpy record array column names defined in period_spacing.dP_features_to_recarr
  @type name: string
  @return: short name for the axis, e.g. Teff -> T, el_1_gm_dP_mean -> $\bar{\Delta}$, etc.
  @rtype: string
  """
  if len(name) == 0:
    message = 'Error: param_tools: get_name_for_axis: input string is empty'
    raise SystemExit, message

  if name == 'M_star': return r'M'
  if name == 'R_star': return r'R'
  if name == 'L_star': return r'L'
  if name == 'Teff':   return r'T'
  if name == 'logg':   return r'g'
  if name == 'ov':     return r'$f$'
  if name == 'eta':    return r'$\eta$'
  if name == 'Z':      return r'Z'
  if name == 'Xc':     return r'X'
  if name == 'Yc':     return r'Y'
  if name == 'el':     return r'$\ell$'
  if '_gm_dP_mean' in name: return r'$\bar{\Delta}$'
  if '_gm_dP_min' in name:  return r'Min'
  if '_gm_dP_max' in name:  return r'Max'
  if '_gm_dP_med' in name:  return r'Med'
  if '_gm_dP_std' in name:  return r'$\sigma$'
  if '_gm_peak_dP_mean'   in name: return r'pk'
  if '_gm_dip_depth_mean' in name: return r'dep'
  if '_gm_dip_dist'       in name: return r'dis'

  # if none of above, then the name is not defined yet
  message = 'Error: param_tools: get_name_for_axis: the name:%s is not defined yet! do it now!' % (name, )
  raise SystemExit, message

#=================================================================================================================
def update_progress(progress):
  """
  update_progress() : Displays or updates a console progress bar
  Accepts a float between 0 and 1. Any int will be converted to a float.
  A value under 0 represents a 'halt'.
  A value at 1 or bigger represents 100%
  """
  barLength = 20 # Modify this to change the length of the progress bar
  status = ""
  if isinstance(progress, int):
      progress = float(progress)
  if not isinstance(progress, float):
      progress = 0
      status = "error: progress var must be float\r\n"
  if progress < 0:
      progress = 0
      status = "Halt...\r\n"
  if progress >= 1:
      progress = 1
      status = "Done...\r\n"
  block = int(round(barLength*progress))
  text = "\r   Progress: [{0}] {1}% {2}".format( ">"*block + "-"*(barLength-block), progress*100, status)
  sys.stdout.write(text)
  sys.stdout.flush()

#=================================================================================================================
def Fe_H_to_Z(Fe_H, A=None, X=0.71):
  """
  convert [Fe/H] to Z using Asplund et al. 2009 solar composition
  @param Fe_H: The value of [Fe/H]
  @type Fe_H: float
  @param A: A is a conversion factor used in the formula; default=None, then A=0.9 is used
  @type A: float
  @return: Z is the metalicity in mass fraction for the object
  @rtype: float
  """
  if A is None: A = 0.90
  Z_div_X = Z_sun/X_sun * np.power(10.0, A*Fe_H)

  return X * Z_div_X

#=================================================================================================================


import numpy as np
import glob, os
import matplotlib.pylab as plt

#=================================================================================================================
def compare_graco_gyre(dic_graco, dic_gyre, el_from=1, el_to=1, n_pg_from=-100, n_pg_to=10,
                       file_out=None):
  """
  Compare the list of frequencies from GraCo, with the same list from GYRE.
  @param dic_graco: list of modes from GraCo for different input models
  @type dic_graco: list of dictionaries
  @param dic_gyre: list of modes from GYRE for different input models
  @type dic_gyre: list
  @return:
  @rtype:
  """
  #n_graco = len(dic_graco)
  #n_gyre  = len(dic_gyre)
  
  #if n_graco == 0 or n_gyre == 0:
    #message = 'Error: plot_graco: compare_graco_gyre: Empty input list: n_graco=%s, n_gyre=%s' % (n_graco, n_gyre)
    #raise SystemExit, message
  #if n_graco != n_gyre:
    #message = 'Error: plot_graco: compare_graco_gyre: Incompatible comparison! n_graco != n_gyre as %s != %s' (n_graco, n_gyre)
    #raise SystemExit, message 
  
  #list_graco = []; list_gyre = []
  #for i_dic, dic in enumerate(dic_graco):
  rec       = dic_graco['modes']
  n_modes   = len(rec)
  ind_modes = [i for i in range(n_modes) if all([ rec['l'][i] >= el_from, rec['l'][i] <= el_to,
                                               rec['n_pg'][i] >= n_pg_from, rec['n_pg'][i] <= n_pg_to ]) ] 
  if len(ind_modes) == 0:
    message = 'compare_graco_gyre: GRACO: no modes fit %s <= el <= %s and %s <= n_pg <= %s at the same time' % (el_from, el_to, n_pg_from, n_pg_to)
    raise SystemExit, message
    
  rec_graco = rec[ind_modes]
  n_modes = len(rec_graco)
  n_graco_modes = n_modes
  graco_n_pg = rec_graco['n_pg']
  graco_freq = rec_graco['freq']
    
  #list_graco.append((graco_n_pg, graco_freq))
    
  #for i_dic, dic in enumerate(dic_gyre):
  gyre_el    = dic_gyre['l']
  gyre_n_pg  = dic_gyre['n_pg']
  n_modes    = len(gyre_el)
  n_gyre_modes = n_modes
  ind_modes  = [i for i in range(n_modes) if all([ gyre_el[i] >= el_from, gyre_el[i] <= el_to,
                                               gyre_n_pg[i] >= n_pg_from, gyre_n_pg[i] <= n_pg_to ]) ]
    
  if len(ind_modes) == 0:
    message = 'compare_graco_gyre: GYRE: no modes fit %s <= el <= %s and %s <= n_pg <= %s at the same time' % (el_from, el_to, n_pg_from, n_pg_to)
    raise SystemExit, message
    
  gyre_el   = gyre_el[ind_modes]
  gyre_n_pg = gyre_n_pg[ind_modes]
  gyre_freq = np.real(dic_gyre['freq'][ind_modes]) 
      
  #list_gyre.append((gyre_n_pg, gyre_freq))
    
  ##########################################  
  # Prepare the plot
  ##########################################  
  fig = plt.figure(figsize=(4,6), dpi=200)
  plt.subplots_adjust(left=0.21, bottom=0.08, top=0.985)
  ax1 = fig.add_subplot(211)
  ax2 = fig.add_subplot(413)
  ax3 = fig.add_subplot(414)
  
  ax1.set_xticks(())
  ax1.set_xlim(n_pg_from, n_pg_to)
  ax1.set_ylim(min([min(graco_freq), min(gyre_freq)])*.95, max([max(graco_freq), max(gyre_freq)])*1.05)
  ax1.set_ylabel(r'Frequency [$\mu$Hz]')
  
  ax2.set_xticks(())
  ax2.set_xlim(n_pg_from, n_pg_to)
  ax2.set_ylabel(r'$\delta f=f^{\rm GYRE} - f^{\rm GraCo}$')

  ax3.set_xlabel(r'Mode Order $n_{\rm pg}$')
  ax3.set_xlim(n_pg_from, n_pg_to)
  ax3.set_ylabel(r'$\delta f/f$ [\%]')

  n_modes = np.min([n_gyre_modes, n_graco_modes])  

  ax1.scatter(graco_n_pg, graco_freq, s=50, facecolor='white', edgecolor='blue', marker='o', label=r'GraCo')
  ax1.scatter(gyre_n_pg, gyre_freq, s=10, color='red', marker='o', label=r'GYRE')
  leg1 = ax1.legend(loc=2, fontsize='medium')

  list_n_pg = []; list_diff = []; list_rel = []
  for i_mode in range(n_modes):
    ref_n_pg = gyre_n_pg[i_mode]
    ref_freq = gyre_freq[i_mode]
    
    ind_graco = [i for i in range(n_graco_modes) if graco_n_pg[i] == ref_n_pg]
    if len(ind_graco) == 0: continue
    comp_n_pg = graco_n_pg[ind_graco]
    comp_freq = graco_freq[ind_graco]
    
    freq_diff     = ref_freq - comp_freq
    rel_freq_diff = freq_diff / ref_freq
    
    list_n_pg.append(ref_n_pg)
    list_diff.append(freq_diff)
    list_rel.append(rel_freq_diff * 100.0)  # save percentages
    
  ax2.scatter(list_n_pg, list_diff, s=25, color='black', marker='o')
  ax3.scatter(list_n_pg, list_rel, s=25, color='purple', marker='s')
    
  ##########################################  
  # Annotations, Finalize the plot
  ##########################################  
  if el_from == el_to: txt_el = r'$\ell=$%s' % (str(el_from), )
  else: txt_el == r'%s$\leq\ell\leq$%s' % (el_from, el_to)
  ax1.annotate(txt_el, xy=(0.1, 0.6), xycoords='axes fraction', fontsize='large')
  
  if file_out:
    plt.savefig(file_out, dpi=200)
    print ' - plot_graco: compare_graco_gyre: %s saved.' % (file_out, )
    plt.close()
    
  return fig

#=================================================================================================================
def check_Hydr_Equil(dic, Lagr=True, Euler=False, file_out=None):
  """
  Plot and ensure that the input OSC model is in hydrostatic equilibrium point by point
  @param dic: dictinary provided by mesa_gyre.read.read_osc, containing data from the file. 
  @type dic: dictionary
  @param file_out: full path to the output plot
  @type file_out: string
  @param Lagr, Euler: to check the hydrostatic equilibrium in Lagrangian vs. Eulerian coordinate
  @type Lagr, Euler: boolean
  @return: None
  @rtype: None
  """
  from math import e as neper
  c_grav = 6.67428e-8 # cgs; as defined in mesa/const/const_def.f
  
  nn     = dic['nn']
  M_star = dic['M_star']
  R_star = dic['R_star']
  L_star = dic['L_star']
  Z_ini  = dic['Z_ini']
  X_ini  = dic['X_ini']
  a_mlt  = dic['a_mlt']
  Xc     = dic['Xc']
  Yc     = dic['Yc']
  Age    = dic['Age']
  omega  = dic['omega']
  Teff   = dic['Teff']
  
  rec    = dic['osc']
  
  r_cm   = rec['radius']
  P      = rec['pressure']
  lnq    = rec['lnq']
  Rho    = rec['density']
  logT   = np.log10(rec['temperature'])
  m_gr   = np.power(neper, lnq) * M_star
  
  dm     = np.zeros(nn)
  dm[-1] = m_gr[-1]
  for i in range(0, nn-1): dm[i] = m_gr[i] - m_gr[i+1]
  #dm[1:nn] = m_gr[1:nn] - m_gr[0:nn-1]
  #dm[0]  = m_gr[0]
  
  dr     = np.zeros(nn)
  dr[1:nn] = r_cm[1:nn] - r_cm[0:nn-1]
  dr[0]  = r_cm[0]
  
  gravity = - c_grav * m_gr[:] / r_cm[:]**2.0

  lhs = np.zeros(nn)
  rhs = np.zeros(nn)
  
  for i in range(1, nn):
    if Lagr:
      lhs[i] = (P[i-1] - P[i]) / ((dm[i-1] + dm[i]) / 2.0)
      rhs[i] = -c_grav * m_gr[i] / (4.0 * np.pi * r_cm[i]**4.0)
    if Euler:
      lhs[i] = (P[i-1] - P[i]) / ((dr[i-1] + dr[i]) / 2.0)
      rhs[i] = -Rho[i] * gravity[i]
      
  lhs[0] = lhs[1]
  rhs[0] = rhs[1]
  
  departure = np.abs(1.0 - lhs[:] / rhs[:])
  if Lagr: txt = 'Lagrangian'
  else: txt = 'Eulerian'
  print ' - plot_graco: check_Hydr_Equil: ' + txt + ' frame'
  print '   Median of departure = %s' % (np.median(departure), )
  print '   Min = %s, Max= %s' % (min(np.abs(departure)), max(departure))
  
  if file_out:
    fig = plt.figure(figsize=(6,4), dpi=200)
    ax  = fig.add_subplot(111)
    plt.subplots_adjust(left=0.12, bottom=0.12)
    ax.set_xlim(logT.max(), logT.min())
    ax.set_ylim(-20, 1)
    ax.set_xlabel(r'log T')
    ax.set_ylabel(r'$\log Q = \log|1.0 - \frac{\rm LHS}{\rm RHS}|$')
    ax.plot(logT, np.log10(departure), lw=1, color='black', label=r'$\log Q$')
    ax.plot(logT, np.log10(np.abs(lhs)), lw=1, color='blue', label=r'$\log$(LHS)')
    ax.plot(logT, np.log10(np.abs(rhs)), lw=1, color='red', label=r'$\log$(RHS)')
    leg = ax.legend(loc=0, fontsize='x-small')
    print lhs
    print dm
    
    plt.savefig(file_out, dpi=200)
    print ' - plot_graco: check_Hydr_Equil: %s saved' % (file_out, )
    plt.close()

  return None    
  
#=================================================================================================================
#=================================================================================================================
#=================================================================================================================

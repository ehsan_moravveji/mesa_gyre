
import os, sys, glob
import logging
import numpy as np 
import pylab as plt 
import commons, tar, plot_commons, plot_gyre
import tar_diff_rot as tdr

dic_conversions = commons.conversions()
Hz_to_cd        = dic_conversions['Hz_to_cd']
Hz_to_uHz       = dic_conversions['Hz_to_uHz']
cd_to_Hz        = dic_conversions['cd_to_Hz']

Msun = 1.9892e33    # g
Rsun = 6.9598e10    # cm
Lsun = 3.8418e33    # cgs
G    = 6.67428e-8   # cm^3 g^-1 s^-2

#=========================================================================================

#   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#   T A R   P L O T T I N G
#   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#=========================================================================================
#=========================================================================================
def freq_rot_vs_Xc(inputs, Xc_range=[0.72, -0.01], file_out=None):
  """
  Q: Does optimized rotation frequency, freq_rot, correlate with age?
  A: It probably should.
  To unravel this, we can make a simple scatter plot of freq_rot versus Xc.
  @param inputs: Two possible inputs are allowed:
         (a) List of dictionaries of GYRE summary output, after optimization. "freq_rot" should 
             be available as one of the keys
         (b) numpy recarray after reading and passing a chi-square table. The "freq_rot" should
             be available as one of the columns
  @type inputs: list of dictionaries or numpy recarray
  """
  if file_out is None: return 
  n = len(inputs)

  input_is_list = False
  input_is_rec  = False
  if isinstance(inputs, list):
    input_is_list = True
  else:
    input_is_rec  = True

  if input_is_list:
    arr_Xc = np.zeros(n)
    arr_freq_rot = np.zeros(n)
    for i, dic in enumerate(list_dic_h5):
      arr_Xc[i]  = dic['Xc']
      arr_freq_rot[i] = dic['omega_rot'] / (2.0 * np.pi) * Hz_to_cd
  elif input_is_rec:
    arr_Xc = inputs['Xc']
    arr_freq_rot = inputs['freq_rot'] * Hz_to_cd

  fig, ax = plt.subplots(1, figsize=(6, 4))
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97)

  ax.scatter(arr_Xc, arr_freq_rot, marker='o', facecolor='grey', edgecolor='k', s=16)

  ax.set_xlabel(r'Center Hydrogen Mass Fraction $X_{\rm c}$')
  ax.set_ylabel(r'Optimized Rotation Frequency $f_{\rm rot}$ [d$^{-1}$]')
  ax.set_xlim(Xc_range[0], Xc_range[1])

  plt.savefig(file_out, transparent=True)
  print ' - plot_tar: freq_rot_vs_Xc: saved {0}'.format(file_out)
  plt.close()

  return None

#=========================================================================================
def hist_freq_rot(inputs, x_range=[], list_dic_annot=[], file_out=None):
  """
  Show the histogram of the optimization outcome for rotation frequency, "freq_rot" which 
  is available as a key in all GYRE output summary dictionaries, i.e. "list_dic_h5".
  @param inputs: Two possible inputs are allowed:
         (a) List of dictionaries of GYRE summary output, after optimization. "freq_rot" should 
             be available as one of the keys
         (b) numpy recarray after reading and passing a chi-square table. The "freq_rot" should
             be available as one of the columns
  @type inputs: list of dictionaries or numpy recarray
  @param file_out: full path to the output plot file 
  @type file_out: string 
  @return: None
  @rtype: NonType
  """
  n = len(inputs)
  if n == 0: 
    logging.error('plot_tar: hist_freq_rot: Input list is empty')
    raise SystemExit, 'Error: plot_tar: hist_freq_rot: Input list is empty'

  input_is_list = False
  input_is_rec  = False
  if isinstance(inputs, list):
    input_is_list = True
  else:
    input_is_rec  = True

  omega_rot_Hz= np.zeros(n)
  if input_is_list:
    for i in range(n): omega_rot_Hz[i] = inputs[i]['omega_rot'] 
  elif input_is_rec:
    omega_rot_Hz = inputs['freq_rot'] * (2.0 * np.pi)
  omega_rot_rad_per_day = omega_rot_Hz * Hz_to_cd # rad/sec
  freq_rot_cd = omega_rot_rad_per_day / (2.0 * np.pi)

  if len(x_range) == 2:
    min_rot     = x_range[0]
    max_rot     = x_range[1]
  else:
    min_rot     = min(freq_rot_cd)
    max_rot     = max(freq_rot_cd)

  fig, ax = plt.subplots(1, figsize=(5, 3.75))
  plt.subplots_adjust(left=0.125, right=0.90, bottom=0.135, top=0.98)

  n_bins   = 50
  tup_hist = ax.hist(freq_rot_cd, bins=n_bins, range=(min_rot, max_rot), alpha=0)
  counts   = tup_hist[0]
  total    = float(sum(counts))
  prob     = counts / total
  max_prob = max(prob)

  bins     = tup_hist[1] # has n_bins+1 values
  left     = bins[:n_bins]
  width    = (max_rot - min_rot) / n_bins
  midpoint = left + width/2.0   # centralize the bins to their values 

  ax.bar(left=left, height=prob, width=width, bottom=0.0, color='purple', 
         linewidth=0.5, zorder=4, label='PDF')
  ax.plot([], [], 'b.-', lw=2, zorder=3, label='CDF')

  ax.set_xlim(min_rot*0.99, max_rot*1.01)
  ax.set_xlabel(r'Optimized Rotation Frequency \,\,$f_{\rm rot}^{\rm(opt)}$ [day$^{-1}$]')
  ax.set_ylim(0, max_prob * 1.05)
  ax.set_ylabel(r'Prob. Dist. Func. (PDF)')

  cumul    = np.zeros(n_bins)
  cumul[0] = prob[0]
  for i in range(n_bins):
    if i == 0:
      cumul[0] = prob[0]
    elif i == n_bins-1:
      cumul[n_bins-1] = np.sum(prob)
    else:
      cumul[i] = np.sum(prob[ : i+1])

  #--------------------------------
  # Adding 1sigma to 3sigma confidence
  # intervals for rotation frequency
  #--------------------------------
  ind_1sig_lo  = np.argmin(np.abs(cumul - 0.16))
  ind_1sig_hi  = np.argmin(np.abs(cumul - 0.84))
  rot_1sig_lo  = midpoint[ind_1sig_lo]
  rot_1sig_hi  = midpoint[ind_1sig_hi]

  ind_2sig_lo  = np.argmin(np.abs(cumul - 0.025))
  ind_2sig_hi  = np.argmin(np.abs(cumul - 0.975))
  rot_2sig_lo  = midpoint[ind_2sig_lo]
  rot_2sig_hi  = midpoint[ind_2sig_hi]

  ind_3sig_lo  = np.argmin(np.abs(cumul - 0.005))
  ind_3sig_hi  = np.argmin(np.abs(cumul - 0.995))
  rot_3sig_lo  = midpoint[ind_3sig_lo]
  rot_3sig_hi  = midpoint[ind_3sig_hi]

  ax.fill_between([rot_3sig_lo, rot_3sig_hi], y1=0, y2=2, color='0.75', 
                     alpha=0.5, zorder=1)
  ax.fill_between([rot_2sig_lo, rot_2sig_hi], y1=0, y2=2, color='0.50', 
                     alpha=0.5, zorder=2)
  ax.fill_between([rot_1sig_lo, rot_1sig_hi], y1=0, y2=2, color='0.25', 
                     alpha=0.5, zorder=3)

  #--------------------------------
  # Add CDF
  #--------------------------------
  right    = ax.twinx()
  right.plot(midpoint, cumul, 'w.-', lw=4, zorder=5)
  right.plot(midpoint, cumul, 'b.-', lw=2, zorder=6, label='CDF')

  #--------------------------------
  # Cosmetics
  #--------------------------------
  right.set_ylim(0, 1.05)
  right.set_ylabel(r'Cumul. Dist. Func. (CDF)')

  leg = ax.legend(loc=2, fontsize=12, fancybox=False, frameon=False, shadow=False)

  if list_dic_annot:
    plot_commons.add_annotation(ax, list_dic_annot)

  #--------------------------------
  # Terminal Summaries
  #--------------------------------
  sum_prob   = np.sum(prob)
  sum_f_rot  = np.sum(prob * midpoint)
  mean_f_rot = sum_f_rot / sum_prob

  if input_is_rec:
    mean_eta_rot = np.mean(inputs['eta_rot'])
    mean_mass    = np.mean(inputs['M_star'])
    mean_radius  = np.mean(inputs['R_star'])
    mean_crit_freq_Keplerian = tar.get_freq_crit(mean_mass, mean_radius, to_cgs=True) * Hz_to_cd
    mean_crit_freq_Roche     = tar.get_Roche_freq_crit(mean_mass, mean_radius, to_cgs=True) * Hz_to_cd
  elif input_is_list:
    mean_eta_rot = np.mean(np.array( [dic['eta_rot'] for dic in inputs] ))
    mean_crit_freq_Keplerian = 0.0 # not supported yet
    mean_crit_freq_Roche     = 0.0 # not supported yet

  v_sin_i = 62.0   # +/- 5 km / sec
  two_pi_R_f = 2.0 * np.pi * (mean_radius * Rsun) * (mean_f_rot * cd_to_Hz)
  sin_i   = (v_sin_i * 1e5) / two_pi_R_f
  inc_ang = (180.0 / np.pi) * np.arcsin(sin_i)


  print ' - Summary Reports: \n'

  print ' - 3sigma f_rot: {0:.4f} to {1:0.4f}'.format(rot_3sig_lo, rot_3sig_hi)
  print ' - 2sigma f_rot: {0:.4f} to {1:0.4f}'.format(rot_2sig_lo, rot_2sig_hi)
  print ' - 1sigma f_rot: {0:.4f} to {1:0.4f}'.format(rot_1sig_lo, rot_1sig_hi)
  print

  print ' - Weighted Average rotation frequency <f_rot>={0:.4f} [per day]'.format(mean_f_rot)
  print ' - <f_rot> / f_crit_Keplerian = {0:.2f} [%]'.format(mean_f_rot / mean_crit_freq_Keplerian * 100)
  print ' - <f_rot> / f_crit_Roche     = {0:.2f} [%]'.format(mean_f_rot / mean_crit_freq_Roche * 100)
  print ' - Average eta_rot            = {0:.2f} [%]'.format(mean_eta_rot * 100)
  print ' - 2 x pi x R x <f_rot>       = {0:.2f} [km/s]'.format(two_pi_R_f / 1e5)
  print ' - sin_i={0:.4f} => i = {1:.2f} [degree]   '.format(sin_i, inc_ang)

  #--------------------------------
  # Finalize and Save the Plot
  #--------------------------------
  if file_out is not None: 
    plt.savefig(file_out, transparent=True)
    print ' - plot_tar: hist_freq_rot: saved {0}'.format(file_out)
    plt.close()

  return None

#=========================================================================================
def per_vs_rot_freq(list_dic_h5, dic_star, per_from=0.5, per_to=3, list_dic_annot=[], file_out=None):
  """
  This routine shows the evolution of the mode periods with respect to rotation rate,
  here eta_rot, quite similar to Figs. 1 and 2 in Bouabid et al. (2013).
  @param list_dic_h5: list of frequencies for every step/iteration (of a given eta_rot). 
        In addition to the default fields returned by read.read_multiple_gyre_files(), the
        following additional keys are also present:
        - omega_crit: critical angular rotation velocity, 2*pi*freq_crit in rad/sec
        - eta_rot: dimensionless quantity, equal to eta_rot above
  @type list_dic_h5: list of dictionaries
  @param file_out: full path to the output plot file
  @type file_out: string 
  @return: None
  @rtype: None
  """
  if file_out is None: return 
  sec_to_d = dic_conversions['sec_to_d']
  n_dic    = len(list_dic_h5)
  # arr_eta_rot  = np.array([ dic['eta_rot'] for dic in list_dic_h5 ])
  # arr_freq_rot = np.array([ dic['freq_rot'] * Hz_to_cd for dic in list_dic_h5 ])
  arr_num_freq = np.array([ len(dic['l']) for dic in list_dic_h5 ])
  arr_freq     = [ np.real(dic['freq']) for dic in list_dic_h5 ]

  by_eta = False
  if by_eta:
    by_f_rot = False
  else:
    by_f_rot = True

  #--------------------------------
  # Prepare the plot
  #--------------------------------
  # fig, (top, bot) = plt.subplots(2, figsize=(4, 6), sharex=True)
  fig = plt.figure(figsize=(4, 6))
  l   = 0.135
  w   = 0.830
  b   = 0.076
  h1  = 0.60
  h2  = 0.30
  bot = fig.add_axes([l, b, w, h1])
  top = fig.add_axes([l, b+h1+0.015, w, h2])
  # plt.subplots_adjust(left=0.15, right=0.96, bottom=0.075, top=0.98)

  #--------------------------------
  # Do the plot
  #--------------------------------
  dics_obs   = dic_star['list_dic_freq']
  N_obs_freq = len(dics_obs)
  obs_freqs  = np.array([ dic['freq'] for dic in dics_obs ])
  min_obs_freq = np.min(obs_freqs)
  max_obs_freq = np.max(obs_freqs)
  min_per    = 1.0 / max_obs_freq
  max_per    = 1.0 / min_obs_freq

  min_n_pg   = 1000
  max_n_pg   = -1000
  list_n_pg  = []
  list_per   = []
  list_eta_rot = []
  list_f_rot = []
  list_N_mod = []
  list_per_GIW = []

  em = 1  # very bad assumption, just valid for prograde modes

  for i_dic, dic in enumerate(list_dic_h5):
    eta_rot  = dic['eta_rot'] 
    freq_rot = dic['freq_rot'] * Hz_to_cd
    freq     = np.real(dic['freq']) * Hz_to_cd
    n_freq   = len(freq)
    per      = 1.0 / freq 
    n_pg     = dic['n_pg']
    min_n_pg = min([min_n_pg, np.min(n_pg)])
    max_n_pg = max([max_n_pg, np.max(n_pg)])
    eta_arr  = np.ones(n_freq) * eta_rot

    ind_per  = np.where((per >= min_per) & (per <= max_per))[0]
    N_mod    = len(ind_per)

    # Find the mode with spin ~ 1
    freq_corot = freq - em * freq_rot
    spin     = 2 * freq_rot / freq_corot
    ind_spin = np.argmin(np.abs(spin - 1.0))
    freq_GIW = freq[ind_spin]
    per_GIW  = 1.0 / freq_GIW 

    list_eta_rot.append(eta_rot)
    list_f_rot.append(freq_rot)
    list_n_pg.append(n_pg)
    list_per.append(per)
    list_N_mod.append(N_mod)
    list_per_GIW.append(per_GIW)

    # bot.scatter(eta_arr * 100, per, s=1, marker='.', color='black', zorder=2)

  range_n_pg = range(min_n_pg, max_n_pg+1)
  keep_eta   = []
  keep_rot   = []
  keep_per   = []
  for i, n in enumerate(range_n_pg):
    vals_eta = []
    vals_rot = []
    vals_per = []
    for i_eta, eta_rot in enumerate(list_eta_rot):
      n_pg = list_n_pg[i_eta]
      ind  = np.where(n == n_pg)[0]
      if ind:
        vals_eta.append(eta_rot * 100)
        vals_rot.append(list_f_rot[i_eta])
        vals_per.append(list_per[i_eta][ind])
    keep_eta.append(vals_eta)
    keep_rot.append(vals_rot)
    keep_per.append(vals_per)

    if by_eta:
      bot.plot(vals_eta, vals_per, linestyle='solid', color='black', lw=0.5, zorder=3)
    elif by_f_rot:
      bot.plot(vals_rot, vals_per, linestyle='solid', color='black', lw=0.5, zorder=3)
    else:
      raise SystemExit, 'Error: plot_tar: per_vs_rot_freq: Could not decide to plot by eta or freq_rot.'

  list_eta_rot = np.array(list_eta_rot) * 100

  #---------------------------------
  # Mark GIW mode in red dashed line
  #---------------------------------
  if False:
    if by_eta:
      bot.plot(vals_eta[1:], list_per_GIW[1:], linestyle='dashed', color='r', lw=2, zorder=4)
    elif by_f_rot:
      u = 1.0 / ((2.0 + em) * np.array(vals_rot[1:]))
      bot.plot(vals_rot[1:], u, linestyle='dashed', color='r', lw=2, zorder=4)
      v = 1.0 / ((1.0 + em) * np.array(vals_rot[1:]))
      bot.plot(vals_rot[1:], v, linestyle='dashed', color='b', lw=2, zorder=4)

  #--------------------------------
  # Add observed box as a background patch
  #--------------------------------
  if by_eta:
    bot.fill_between(list_eta_rot, y1=min_per, y2=max_per, color='blue', alpha=0.10, zorder=1)
  else:
    bot.fill_between(list_f_rot, y1=min_per, y2=max_per, color='blue', alpha=0.10, zorder=1)
  #--------------------------------
  # Add the top panel now
  #--------------------------------
  top.axhline(y=N_obs_freq, linestyle='dashed', color='grey', lw=2, zorder=1)
  if by_eta:
    top.scatter(list_eta_rot, list_N_mod, facecolor='blue', edgecolor='red', 
                marker='o', s=25, zorder=2)
  else:
    top.scatter(list_f_rot, list_N_mod, facecolor='blue', edgecolor='red', 
                marker='o', s=25, zorder=2)

  #--------------------------------
  # Legend, Annotations and Ranges
  #--------------------------------
  if by_eta:
    dx = (max(list_eta_rot) - min(list_eta_rot)) * 0.02
    top.set_xlim(min(list_eta_rot)-dx, max(list_eta_rot)+dx)
  else:
    dx = (max(list_f_rot) - min(list_f_rot)) * 0.02
    top.set_xlim(min(list_f_rot)-dx, max(list_f_rot)+dx)
  top.set_ylim(min(list_N_mod)*0.90, max(list_N_mod)*1.10)
  top.set_ylabel(r'$\mathcal{M}$')
  top.set_xticklabels(())
  top.xaxis.set_tick_params(labeltop='on')
  top.annotate('(a)', xy=(0.04, 0.86), xycoords='axes fraction',
               bbox=dict(boxstyle="round4,pad=.5", fc="none", ec="none"))

  dy = (max(list_N_mod) - min(list_N_mod)) * 0.05
  txt = dic_star['name'][0]
  if by_eta:
    top.annotate(txt, xy=(min(list_eta_rot)+2*dx, N_obs_freq+dy), color='black', fontsize=10)
  else:
    top.annotate(txt, xy=(min(list_f_rot)+2*dx, N_obs_freq+dy), color='black', fontsize=10)

  if list_dic_annot:
    plot_commons.add_annotation(bot, list_dic_annot)

  if by_eta:
    bot.set_xlim(min(list_eta_rot)-dx, max(list_eta_rot)+dx)
    bot.set_xlabel(r'$\eta_{\rm rot}=\Omega_{\rm rot} / \Omega_{\rm crit} \,[\%]$')
  else:
    bot.set_xlim(min(list_f_rot)-dx, max(list_f_rot)+dx)
    bot.set_xlabel(r'$f_{\rm rot}$ [day$^{-1}$]')
  bot.set_ylim(per_from, per_to)
  bot.set_ylabel(r'Period in Inertial Frame [day]')
  bot.annotate('(b)', xy=(0.04, 0.92), xycoords='axes fraction',
               bbox=dict(boxstyle="round4,pad=.5", fc="w", ec="none"))

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  plt.savefig(file_out, transparent=False)
  print ' - plot_tar: per_vs_rot_freq: saved {0}'.format(file_out)
  plt.close()

  return

#=========================================================================================
def freq_vs_rot_freq(list_dic_h5, file_out=None):
  """
  This routine shows the evolution of the mode frequencies with respect to rotation rate,
  here eta_rot.
  @param list_dic_h5: list of frequencies for every step/iteration (of a given eta_rot). 
        In addition to the default fields returned by read.read_multiple_gyre_files(), the
        following additional keys are also present:
        - omega_crit: critical angular rotation velocity, 2*pi*freq_crit in rad/sec
        - eta_rot: dimensionless quantity, equal to eta_rot above
  @type list_dic_h5: list of dictionaries
  @param file_out: full path to the output plot file
  @type file_out: string 
  @return: None
  @rtype: None
  """
  if file_out is None: return 
  n_dic = len(list_dic_h5)
  arr_eta_rot  = np.array([ dic['eta_rot'] for dic in list_dic_h5 ])
  arr_num_freq = np.array([ len(dic['l']) for dic in list_dic_h5 ])
  arr_freq     = [ np.real(dic['freq']) * Hz_to_cd for dic in list_dic_h5 ]

  #--------------------------------
  # Prepare the plot
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(6,4))
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.11, top=0.97)

  #--------------------------------
  # Do the plot
  #--------------------------------
  N_obs_freq = 36     # From Papics et al. (2015)
  for i_dic, dic in enumerate(list_dic_h5):
    eta_rot  = dic['eta_rot'] 
    freq     = np.real(dic['freq']) * Hz_to_cd
    n_freq   = len(freq)
    eta_arr  = np.ones(n_freq) * eta_rot

    ax.scatter(eta_arr, freq, s=9, marker='o', color='grey')

  ax.set_xlim(min(arr_eta_rot)-0.5, max(arr_eta_rot)+0.5)
  # ax.set_ylim(min(arr_freq)-1, max(N_obs_freq, max(arr_num_freq))+1)
  ax.set_ylim(0, 6)
  ax.set_xlabel(r'$\Omega_{\rm rot} / \Omega_{\rm crit}$')
  ax.set_ylabel(r'Frequency [day$^{-1}$]')

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  plt.savefig(file_out)
  print ' - plot_tar: freq_vs_rot_freq: saved {0}'.format(file_out)
  plt.close()

  return

#=========================================================================================
def num_freq_vs_rot_freq(list_dic_h5, file_out=None):
  """
  The frequency density of prograde (m=+1) modes in a fixed frequency interval, say f_a to f_b, 
  increases with the increase in rotation frequency, and then it declines. This routine 
  shows the number of frequencies as a function of dimentional rotation rate, 
        eta_rot = Omega_rot/Omega_crit
  @param list_dic_h5: list of frequencies for every step/iteration (of a given eta_rot). 
        In addition to the default fields returned by read.read_multiple_gyre_files(), the
        following additional keys are also present:
        - omega_crit: critical angular rotation velocity, 2*pi*freq_crit in rad/sec
        - eta_rot: dimensionless quantity, equal to eta_rot above
  @type list_dic_h5: list of dictionaries
  @param file_out: full path to the output plot file
  @type file_out: string 
  @return: None
  @rtype: None
  """
  if file_out is None: return 
  n_dic = len(list_dic_h5)
  arr_eta_rot  = np.array([ dic['eta_rot'] for dic in list_dic_h5 ])
  arr_num_freq = np.array([ len(dic['l']) for dic in list_dic_h5 ])

  #--------------------------------
  # Prepare the plot
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(6,4))
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.11, top=0.97)

  #--------------------------------
  # Do the plot
  #--------------------------------
  N_obs_freq = 36     # From Papics et al. (2015)
  ax.scatter(arr_eta_rot, arr_num_freq, s=50, marker='o', facecolor='red', edgecolor='black', zorder=1)
  ax.axhline(y=N_obs_freq, linestyle='dashed', color='grey', lw=2, zorder=2)
  ax.annotate('KIC 7760680', xy=(0.0, N_obs_freq-2), fontsize=10, color='grey')

  ax.set_xlim(min(arr_eta_rot)-0.05, max(arr_eta_rot)+0.05)
  ax.set_ylim(min(arr_num_freq)-1, max(N_obs_freq, max(arr_num_freq))+1)
  ax.set_xlabel(r'$\Omega_{\rm rot} / \Omega_{\rm crit}$')
  ax.set_ylabel('Num. Computed Model Freequencies')

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  plt.savefig(file_out)
  print ' - plot_tar: num_freq_vs_rot_freq: saved {0}'.format(file_out)
  plt.close()

  return

#=========================================================================================
def find_n_pg_in_obs_range(dic_best, dic_star, el=1, sigma=3):
  """
  Find the number of modes in the observed range, and the min and max of the radial mode
  order, n_pg, corresponding to the two extreme values. 
  @param dic_best: GYRE summary output information for the best model to exploit information
         from. This can be created simply by calling mesa_gyre.read.read_multiple_gyre_files()
  @type dic_best: dictionary
  @return: Three items are returned:
           - n_in_range: the number of model modes within the observed range, 
           - min_n_pg: the minimum radial order in the series; for g-modes, quite a negative number!
           - max_n_pg: the maximum radial order in the series.
  @rtype: tuple
  """
  list_dic_freq = dic_star['list_dic_freq']
  list_obs_freq = np.array([ dic['freq'] for dic in list_dic_freq ])
  list_obs_err  = np.array([ dic['freq_err'] for dic in list_dic_freq ])
  ind_freq      = np.argsort(list_obs_freq)
  list_obs_freq = list_obs_freq[ind_freq]
  list_obs_err  = list_obs_err[ind_freq]
  min_freq      = list_obs_freq[0] - sigma * list_obs_err[0]
  max_freq      = list_obs_freq[-1] + sigma * list_obs_err[-1]

  obs_freq_unit = list_dic_freq[0]['freq_unit']
  if obs_freq_unit == 'Hz':
    factor      = 1
  elif obs_freq_unit == 'cd':
    factor      = cd_to_Hz

  list_el       = dic_best['l']
  ind_el        = np.where(list_el == el)[0]
  list_n_pg     = dic_best['n_pg'][ind_el]
  list_th_freq  = np.real(dic_best['freq'][ind_el]) * factor

  ind_in_range  = np.where((list_th_freq >= min_freq) & (list_th_freq <= max_freq))[0]
  n_in_range    = len(ind_in_range)
  min_n_pg      = list_n_pg[ ind_in_range[0] ]
  max_n_pg      = list_n_pg[ ind_in_range[-1] ]

  print ' - plot_tar: find_n_pg_in_obs_range:'
  print '   There are {0} modes in range between {1} <= n_pg <= {2}'.format(n_in_range, min_n_pg, max_n_pg)

  return (n_in_range, min_n_pg, max_n_pg) 

#=========================================================================================
def per_inertial_vs_rot_vel(list_dic_h5, dic_star, per_from=0.5, per_to=3, el=1, em=0, 
                            list_dic_annot=[], phase='MS', best_v_rot=None, file_out=None):
  """
  This routine shows the evolution of the mode periods with respect to surface rotation velocity,
  here eta_rot, quite similar to Figs. 1 and 2 in Bouabid et al. (2013).
  @param list_dic_h5: list of frequencies for every step/iteration (of a given eta_rot). 
        In addition to the default fields returned by read.read_multiple_gyre_files(), the
        following additional keys are also present:
        - omega_crit: critical angular rotation velocity, 2*pi*freq_crit in rad/sec
        - eta_rot: dimensionless quantity, equal to eta_rot above
        Note: the frequencies must be in the co-rotating frame!
  @type list_dic_h5: list of dictionaries
  @param em: azimuthal order, used to translate co-rotating frequencies (from GYRE) to the frequencies in the inertial frame
  @type em: integer
  @param phase: evolutionary phase, as the filename tag carries this info. Typical choices are: ['PreMS', 'MS', 'PMS'], 
        for pre-main sequence, main sequence and post-main sequence phases.
        This is mainly used for annotating the figure, and was employed in Zwintz et al. (2016, A&A)
  @type phase: string
  @param best_v_rot: highlight the best matching modes for the most reasonable rotation velocity; default: None
  @type best_v_rot: float
  @param file_out: full path to the output plot file
  @type file_out: string 
  @return: None
  @rtype: None
  """
  if file_out is None: return 
  sec_to_d = dic_conversions['sec_to_d']
  n_dic    = len(list_dic_h5)
  # arr_eta_rot  = np.array([ dic['eta_rot'] for dic in list_dic_h5 ])
  # arr_freq_rot = np.array([ dic['freq_rot'] * Hz_to_cd for dic in list_dic_h5 ])
  arr_num_freq = np.array([ len(dic['l']) for dic in list_dic_h5 ])
  arr_freq_re  = [ np.real(dic['freq']) for dic in list_dic_h5 ]
  arr_freq_im  = [ np.imag(dic['freq']) for dic in list_dic_h5 ]

  #--------------------------------
  # Prepare the plot
  #--------------------------------
  fig, bot = plt.subplots(1, figsize=(4, 6), sharex=True)
  plt.subplots_adjust(left=0.15, right=0.96, bottom=0.08, top=0.92)
  top = bot.twiny()

  #--------------------------------
  # Do the plot
  #--------------------------------
  dics_obs   = dic_star['list_dic_freq']
  N_obs_freq = len(dics_obs)
  obs_freqs  = np.array([ dic['freq'] for dic in dics_obs ])
  obs_per    = 1.0 / obs_freqs
  n_obs_per  = len(obs_per)
  min_obs_freq = np.min(obs_freqs)
  max_obs_freq = np.max(obs_freqs)
  min_per    = 1.0 / max_obs_freq
  max_per    = 1.0 / min_obs_freq

  min_n_pg   = 1000
  max_n_pg   = -1000
  list_n_pg  = []
  list_per_co= []
  list_per_in= []
  list_eta_rot = []
  list_f_rot = []
  list_v_rot = []
  list_N_mod = []
  list_per_GIW = []

  for i_dic, dic in enumerate(list_dic_h5):
    eta_rot  = dic['eta_rot'] 
    freq_rot = dic['freq_rot'] * Hz_to_cd  
    v_rot    = 2.0 * np.pi * dic['R_star'] * dic['freq_rot'] * 1e-5  # km / sec
    l        = dic['l']
    m        = dic['m']
    ind_l_m  = np.where((l == el) & (m == em))[0]
    if len(ind_l_m) == 0:
      raise SystemExit, 'Error: plot_tar: per_inertial_vs_rot_vel: no frequencies for this given (l,m)'

    l        = l[ind_l_m]
    m        = m[ind_l_m]
    freq     = np.real(dic['freq'][ind_l_m]) * Hz_to_cd # Co-Rotating frequencicies computed and stored by GYRE
    work     = np.real(dic['W'][ind_l_m])
    ind_exc  = np.where(work > 0)[0]
    ind_damp = np.where(work <= 0)[0]
    n_freq   = len(freq)
    freq_co  = freq 
    freq_in  = freq_co + em * freq_rot
    per_co   = 1.0 / freq_co
    per_in   = 1.0 / freq_in 
    n_pg     = dic['n_pg']
    min_n_pg = min([min_n_pg, np.min(n_pg)])
    max_n_pg = max([max_n_pg, np.max(n_pg)])
    eta_arr  = np.ones(n_freq) * eta_rot * 100.0
    v_rot_arr= np.ones(n_freq) * v_rot

    ind_per  = np.where((per_in >= min_per) & (per_in <= max_per))[0]
    N_mod    = len(ind_per)

    # Find the mode with spin ~ 1
    spin     = 2 * freq_rot / freq_co
    ind_spin = np.argmin(np.abs(spin - 1.0))
    freq_GIW = freq[ind_spin]
    per_GIW  = 1.0 / freq_GIW 

    list_eta_rot.append(eta_rot)
    list_f_rot.append(freq_rot)
    list_v_rot.append(v_rot)
    list_n_pg.append(n_pg)
    list_per_in.append(per_in)
    list_per_co.append(per_co)
    list_N_mod.append(N_mod)
    list_per_GIW.append(per_GIW)

    bot.scatter(v_rot_arr, per_in, s=1, marker='o', color='grey', edgecolor='none', zorder=1)
    bot.scatter(v_rot_arr[ind_exc], per_in[ind_exc], s=4, marker='o', color='b', edgecolor='none', zorder=2)

    top.scatter(eta_arr, per_in, color='none')

  list_eta_rot = np.array(list_eta_rot) * 100

  #---------------------------------
  # Highlight the best v_rot model
  #---------------------------------
  if best_v_rot is not None:
    list_v_rot = np.array(list_v_rot)
    ind_rot    = np.argmin(np.abs(list_v_rot - best_v_rot))
    best_v_rot = list_v_rot[ind_rot] 
    best_eta   = list_eta_rot[ind_rot]
    best_per   = list_per_in[ind_rot]
    ind_fit    = np.argmin(np.abs(best_per - obs_per.max()))  # match the highest observed period
    best_per   = best_per[ind_fit : ind_fit + n_obs_per]
    best_v_arr = np.ones(n_obs_per) * best_v_rot 
    print '   The best v_rot={0:.2f}, and best eta_rot={1:.2f}'.format(best_v_rot, best_eta)
    bot.scatter(best_v_arr, best_per, s=16, marker='s', color='k', edgecolor='b', zorder=5)

  #---------------------------------
  # Mark GIW mode in red dashed line
  #---------------------------------
  if False:
    u = 1.0 / ((2.0 + em) * np.array(vals_rot[1:]))
    bot.plot(vals_rot[1:], u, linestyle='dashed', color='r', lw=2, zorder=4)
    v = 1.0 / ((1.0 + em) * np.array(vals_rot[1:]))
    bot.plot(vals_rot[1:], v, linestyle='dashed', color='b', lw=2, zorder=4)

  #--------------------------------
  # Add observed box as a background patch
  #--------------------------------
  if False:
    bot.fill_between(list_v_rot, y1=min_per, y2=max_per, color='blue', alpha=0.10, zorder=1)

  #--------------------------------
  # Add Observed Periods of Modes
  #--------------------------------
  if True:
    for i_per, per in enumerate(obs_per):
      bot.axhline(y=per, xmin=-100, xmax=1000, linestyle='solid', color='red', lw=1, zorder=3)

  #--------------------------------
  # Legend, Annotations and Ranges
  #--------------------------------
  dx = (max(list_eta_rot) - min(list_eta_rot)) * 0.02
  top.set_xlim(min(list_eta_rot)-dx, max(list_eta_rot)+dx)
  top.set_xlabel(r'Rotation Rate $\Omega_{\rm rot} / \Omega_{\rm k} \,[\%]$')

  dy = (max(list_N_mod) - min(list_N_mod)) * 0.05
  txt = dic_star['name'][0]
  # top.annotate(txt, xy=(min(list_v_rot)+2*dx, N_obs_freq+dy), color='black', fontsize=10)

  if list_dic_annot:
    plot_commons.add_annotation(bot, list_dic_annot)

  dx = (max(list_v_rot) - min(list_v_rot)) * 0.02
  bot.set_xlim(min(list_v_rot)-dx, max(list_v_rot)+dx)
  # bot.set_xlim(80, 180)
  bot.set_xlabel(r'Rotation Velocity $v_{\rm rot}$ [km \,sec$^{-1}$]')
  bot.set_ylim(per_from, per_to)
  bot.set_ylabel(r'Period in Inertial Frame [day]')

  # Star name and mode degree
  txt_name = dic_star['name'][0].replace('HD\,', 'HD ')
  if em == 0:
    txt_l_m  = r'$(\ell, m)=$' + '({0}, {1:d})'.format(el, em)
  else:
    txt_l_m  = r'$(\ell, m)=$' + '({0}, {1:+d})'.format(el, em)
  txt      = txt_name + '\n' + txt_l_m
  bot.annotate(txt, xy=(0.80, 0.90), xycoords='axes fraction', ha='center', va='center',
               bbox=dict(boxstyle="round4,pad=.5", fc="w", ec="grey"))

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  plt.savefig(file_out, transparent=False)
  print ' - plot_tar: per_inertial_vs_rot_vel: saved {0}'.format(file_out)
  plt.close()

  return

#=========================================================================================

#   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#   T A R   D I F F   R O T
#   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#=========================================================================================

#=========================================================================================
def Omega_k(dic, tup_zones, omega_zones, by='mass', omega_r=None, file_out=None):
  """
  Show the zone distribution in the model, regardless of which zoning function is used from
  mesa_gyre.tar_diff_rot module. The required zoning information, i.e. the indices of the 
  edges of all zones (including the core and the surface) are all passed as the first element
  of tup_zones, i.e. indices = tup_zones[0]
  @param dic: dictionary containing gyre input file information
  @type dic: dictionary
  @param tup_zones: This input tuple is actually an output of previous steps of zoning the model.
         tup_zones can be constructed by e.g. calling zoning_uniform(). Read that function for
         more info.
  @type: tup_zones: tuple
  @param omega_zones: array of Omega_k in Hz
  @type omega_zones: list or ndarray
  @param omega_r: array of Omega(r), where the length of this array must be equal to dic['gyre_in'].
         E.g. omega_r can be fetched by calling tdr.omega_zones_to_omega_r(). By default, omega_r is
         set to None, but you must pass a numpy ndarray to practically show it.
  @type omega_r: None or ndarray
  @param by: show x-axis by using this coordinate
  @type by: string
  @return: None 
  @rtype: None 
  """
  nn      = dic['nn']
  # M_star  = dic['M_star']
  # R_star  = dic['R_star']
  gyre_in = dic['gyre_in']
  gradr   = plot_gyre.get_gradr(dic)
  grada   = gyre_in['grada']
  inds    = np.where(gradr <= grada)[0]
  ind_c   = inds[0]  # core index

  if by == 'mass':
    vals  = tdr.get_mass(dic) / Msun
    xlabel= r'Enclosed Mass [M$_\odot$]'
  elif by == 'radius':
    vals  = gyre_in['radius'] / Rsun
    xlabel= r'Enclosed Radius [R$_\odot$]'
  elif by == 'temperature':
    vals  = np.log10(gyre_in['temperature'])
    xlabel= r'Temperature $\log T$ [K]'
  else:
    raise SystemExit, 'Error: plot_tar: zoning: "by"="{0}" not supported'.format(by)

  indices = tup_zones[0]
  edges   = tup_zones[1]
  n_edges = len(edges)
  n_zones = n_edges - 1

  val_cc  = vals[ind_c]
  locs    = vals[indices]

  #--------------------------------
  # Creat the Plot and Do the Plotting
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(5, 3.75))
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97)

  ax.fill_between(vals, y1=-100, y2=+100, where=(vals <= val_cc), color='b', alpha=0.25, zorder=1)
  for loc in locs:
    ax.axvline(x=loc, ymin=0, ymax=1, linestyle='dashed', color='grey', lw=1, zorder=1)

  # Include Omega_k
  omega_zones *= Hz_to_uHz
  for i_z, omega_zone in enumerate(omega_zones):
    z_from = indices[i_z]
    z_to   = indices[i_z+1]
    v_from = vals[z_from]
    v_to   = vals[z_to]

    ax.plot([v_from, v_to], [omega_zone, omega_zone], linestyle='solid',
               color='k', lw=2, zorder=2)

  # Include Omega(r) optinally
  if omega_r is not None:
    if len(omega_r) == len(vals):
      omega_r *= Hz_to_uHz
      ax.scatter(vals, omega_r, marker='s', color='r', s=10, zorder=2)

  #--------------------------------
  # Finalize the Figure
  #--------------------------------
  ax.set_xlim(min(vals), max(vals))
  # ax.set_xlabel(xlabel)
  ax.set_ylim(min(omega_zones)*0.95, max(omega_zones)*1.05)
  # ax.set_ylabel(r'Rotation Frequency $\,\Omega_k$ [$\mu$Hz ??]')

  plt.savefig(file_out, transparent=True)
  print ' - plot_tar: zoning: saved {0}'.format(file_out)
  plt.close()

  return None 

#=========================================================================================
def per_inertial_in_2_zone_model(dic_star, arr, list_dics, yaxis='q', per_from=0.2, per_to=2, 
                                 file_out=None):
  """
  Evolution of the mode periods in the inertial frame in the 2-zone (or maybe more sophisticated) model(s)
  with changing the location of the boundary (edge) of the fitting point of the two zones.
  @param list_dics: list of GYRE outputs which can be produced by e.g. read.read_multiple_gyre_files(), or 
         as an output of other routines in mesa_gyre.tar_diff_rot.
  @type list_dics: list of dictionaries
  @param dic_star: dictionary containing star info. It can be called among available mesa_gyre.stars()
  @type dic_star: dictionary
  @param per_from, per_to: period range on the abscissa in days
  @type per_from, per_to: float
  @param yaxis: which aspect of the 2-zone is going to be plotted? The q? The omega_surf, or the
         core-to-surface rotation rate? This choice is specifiedy by setting either of 
         - q
         - omega_surf
         - omega_ratio
         The y-axis label is automatically placed.
  @tpe yaxis: string
  @param file_out: full path to the output figure file
  @type file_out: string
  @return: None 
  @rtype: None
  """
  if yaxis not in ['q', 'omega_surf', 'omega_ratio']:
    raise SystemExit, 'Error: plot_tar: per_inertial_in_2_zone_model: yaxis={0} not supported'.format(yaxis)

  n_bdry = len(arr)
  n_dics = len(list_dics)
  if n_bdry != n_dics:
    raise SystemExit, 'Error: plot_tar: per_inertial_in_2_zone_model: Size of arr and list_dics must match'

  #--------------------------------
  # Create the Figure
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(12, 4))
  plt.subplots_adjust(left=0.06, right=0.99, bottom=0.12, top=0.97)

  #--------------------------------
  # Add Observed Periods
  #--------------------------------
  list_dic_freq = dic_star['list_dic_freq']
  obs_freqs     = np.array([ dic['freq'] for dic in list_dic_freq ])
  obs_per       = 1./obs_freqs 
  for i, per in enumerate(obs_per):
    ax.axvline(x=per, linestyle='solid', color='grey', alpha=0.5, lw=1, zorder=1)

  #--------------------------------
  # Loop Through Models and Plot Them
  #--------------------------------
  for i, dic in enumerate(list_dics):
    freq   = np.real(dic['freq']) * Hz_to_cd
    xvals  = 1.0 / freq
    yvals  = np.zeros(len(xvals)) + arr[i]

    ax.scatter(xvals, yvals, marker='s', s=5, color='b', zorder=2)

  #--------------------------------
  # Finalize the Figure
  #--------------------------------
  ax.set_xlim(per_from, per_to)
  ax.set_ylim(min(arr)*0.95, max(arr)*1.05)
  ax.set_xlabel(r'Period in Inertial Frame [day]')
  if yaxis == 'q':
    ax.set_ylabel(r'Fraction of Boundary Position')
  elif yaxis == 'omega_surf':
    ax.set_ylabel(r'Surface Omega $\Omega_{\rm surf}$ [$\mu$Hz]')
  elif yaxis == 'omega_ratio':
    ax.set_ylabel(r'Core to Surface Rotation Rate $\Omega_{\rm core}/\Omega_{\rm surf}$')
  else:
    raise SystemExit, 'Error: plot_tar: per_inertial_in_2_zone_model: yaxis not supported'

  plt.savefig(file_out)
  print ' - plot_tar: per_inertial_in_2_zone_model: saved {0}'.format(file_out)
  plt.close()

  return None

#=========================================================================================


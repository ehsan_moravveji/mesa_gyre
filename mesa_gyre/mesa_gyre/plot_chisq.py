"""
This modules provides several functionalities for a graphical representation of chi-square in frequency
(period, or period spacing) matching and also for evolutionary matching of the paramters and the position
of a star on the HR or Kiel diagrams.
"""

import sys, glob, os
import logging
from collections import Counter
import itertools
import numpy as np
import matplotlib
import pylab as plt
import commons, stars, param_tools, plot_commons

#=================================================================================================================

Msun = 1.9892e33    # gr; as defined in mesa/const/const_def.f
Rsun = 6.9598e10    # cm; as defined in mesa/const/const_def.f
Lsun = 3.8418e33    # erg/sec as defined in mesa/const/const_def.f
Tsun = 5777.0
c_grav = 6.67428e-8 # cgs; as defined in mesa/const/const_def.f
logg_sun = np.log10(c_grav * Msun / Rsun**2.0)

#=================================================================================================================
#=================================================================================================================
def grid_param_vs_chisq(recarr, dic_star=None, by='reduced_chisq_seism_freq', mass_from=3, mass_to=20, 
         ov_from=0.0, ov_to=0.03, add_f0_to_fov=False, Z_from=0.01, Z_to=0.02, logD_from=1, logD_to=5, 
         Xc_from=0.72, Xc_to=0.0, logTeff_from=4.6, logTeff_to=3.8, logg_from=4.5, logg_to=3, 
         chisq_from=0, chisq_to=3, file_out=None):
  """
  Throw all chisquare values as scatterd points on different panels, where the axes for each panel comes from grid
  parameters. The plotting is done first from those points with the highest chisquare values being in the background,
  and proceeds to the best model with the minimum chisquare value in the very front of the panel. This is somehow similar
  to Fig. B1 in Moravveji et al. (2015, A&A), but without interpolation.
  @param list_dic_chisq: list of dictionaries with the mode frequency and/or period spacing information, in
         addition to chisq information in its items(). We check for the presence of the required chisq key in
         advance. This list can be retrieved from e.g. mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type list_dic_chisq: list of dictionaries
  @param by: specify whcih chisq criteria to be used. The allowd names are:
        - reduced_chisq_evol/chisq_evol: returned by mesa_gyre.chisq.evol()
        - chisq_one_fit: returned by mesa_gyre.chisq.fit_one_freq()
        - reduced_chisq_seism_freq/chisq_seism_freq: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P/chisq_seism_P; returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_dP/chisq_seism/dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P_and_dP/chisq_seism_P_and_dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type by: string
  @param add_f0_to_fov: In case in MESA models, f_0 was separated from f_ov in the exponential overshoot 
        formula (e.g. in the KIC 7760680 paper), then, I can add this offset to the f_ov values in panel (b)
  @type add_f0_to_fov: False or float
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: matplotlib figure object
  @rtype: object
  """
  # n_dic = len(list_dic_chisq)
  n_row      = len(recarr)
  if n_row == 0:
    logging.error('plot_chisq: grid_param_vs_chisq: Input list is empty')
    raise SystemExit
  names      = recarr.dtype.names
  if by not in names:
    logging.error('plot_chisq: grid_param_vs_chisq: "{0}" not found among available keys.'.format(by))
    raise SystemExit
  if file_out is None:
    print 'Warning: plot_chisq: grid_param_vs_chisq: No output file specified. Returning ...'
    return None

  chisq_top  = np.power(10, chisq_to)
  arr_chisq  = recarr[by]
  # arr_chisq  = arr_chisq[arr_chisq <= chisq_top]

  ind_sort   = np.argsort(arr_chisq)[::-1]     # from max to the min
  recarr     = recarr[ind_sort]
  arr_chisq  = recarr[by]
  log_chisq  = np.log10(arr_chisq)
  max_chisq  = arr_chisq[0]
  min_chisq  = arr_chisq[-1]

  best_row   = recarr[-1]
  if 'M_ini' in names: best_mass = best_row['M_ini']  # backward compatibility
  if 'M' in names:     best_mass = best_row['M']      # backward compatibility
  best_fov   = best_row['ov']
  best_Z     = best_row['Z']
  best_logD  = best_row['logD']
  best_Xc    = np.round(best_row['Xc'], decimals=3)

  print ' plot_chisq.grid_param_vs_chisq(): Parameters of the best model:'
  print ' Chisq={0}; based on: {1}'.format(min_chisq, by)
  print ' M={0}, fov={1}, Z={2}, logD={3}, Xc={4} \n'.format(best_mass, best_fov, best_Z, best_logD, best_Xc)

  # list_R     = np.array([ dic['R_star']/Rsun for dic in list_dic_chisq ])
  # list_L     = np.array([ dic['L_star']/Lsun for dic in list_dic_chisq ])
  # list_Teff  = np.array([ Tsun * np.power(list_L[i], 0.25)/np.power(list_R[i], 0.5) for i in range(n_dic) ])
  # list_logg  = np.array([ logg_sun + np.log10(list_mass[i]) -2*np.log10(list_R[i]) for i in range(n_dic) ])

  if 'M_ini' in names: list_mass = recarr['M_ini']
  if 'M'     in names: list_mass = recarr['M']
  list_fov   = recarr['ov']
  list_Z     = recarr['Z']
  list_logD  = recarr['logD']
  list_Xc    = recarr['Xc']
  list_R     = recarr['R_star']
  list_L     = recarr['L_star']
  list_Teff  = Tsun * np.power(list_L, 0.25)/np.power(list_R, 0.5)
  list_logg  = logg_sun + np.log10(list_mass) -2*np.log10(list_R)

  if add_f0_to_fov: list_fov += add_f0_to_fov
  
  #--------------------------------
  # Prepare the figure
  #--------------------------------
  import matplotlib
  from matplotlib import cm
  from matplotlib.colors import BoundaryNorm
  from matplotlib.ticker import MaxNLocator

  fig    = plt.figure(figsize=(9, 6), dpi=200)
  levels = MaxNLocator(nbins=999).tick_values(np.min(log_chisq), np.max(log_chisq))
  cmap   = plt.cm.get_cmap('gnuplot2')
  norm   = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

  #--------------------------------
  # Create 9 panels
  #--------------------------------
  import matplotlib.gridspec as gridspec
  gsp  = gridspec.GridSpec(2, 30)
  ax11 = plt.subplot(gsp[0, 1:9])
  ax12 = plt.subplot(gsp[0, 11:19])
  ax13 = plt.subplot(gsp[0, 21:29])
  ax21 = plt.subplot(gsp[1, 1:9])
  ax22 = plt.subplot(gsp[1, 11:19])
  ax23 = plt.subplot(gsp[1, 21:30])
  gsp.update(left=0.03, right=1.0, bottom=0.08, top=0.98, hspace=0.20, wspace=0.10)

  #--------------------------------
  # Loop over models, and put them on
  # different panels
  #--------------------------------
  ax11.scatter(list_mass, log_chisq, marker='o', s=9, color='navy')
  ax12.scatter(list_fov, log_chisq, marker='o', s=9, color='navy')
  ax13.scatter(list_Z, log_chisq, marker='o', s=9, color='navy')
  ax21.scatter(list_logD, log_chisq, marker='o', s=9, color='navy')
  ax22.scatter(list_Xc, log_chisq, marker='o', s=9, color='navy')
  kiel = ax23.scatter(np.log10(list_Teff), list_logg, marker='o', s=9, 
                      edgecolors='none', norm=norm, c=log_chisq, 
                      vmin=log_chisq[-1], vmax=log_chisq[0], 
                      cmap=cmap, zorder=10)
  ax23.scatter([np.log10(list_Teff[-1])], [list_logg[-1]], marker='o', s=80,
                      edgecolors='w', facecolors='none', zorder=11)

  #--------------------------------
  # Include The star on Kiel diagram
  # if info is available
  #--------------------------------
  if isinstance(dic_star, dict):
    Teff, Teff_err = dic_star['log_Teff'], dic_star['err_log_Teff_1s']
    logg, logg_err = dic_star['logg'], dic_star['err_logg_1s']

    ax23.fill_between([Teff-Teff_err, Teff+Teff_err], y1=logg-logg_err, y2=logg+logg_err,
                      facecolor='0.50', edgecolor='0.55', alpha=0.5, zorder=12)
    ax23.fill_between([Teff-2*Teff_err, Teff+2*Teff_err], y1=logg-2*logg_err, y2=logg+2*logg_err,
                      facecolor='0.65', edgecolor='0.70', alpha=0.5, zorder=12)
    ax23.fill_between([Teff-3*Teff_err, Teff+3*Teff_err], y1=logg-3*logg_err, y2=logg+3*logg_err,
                      facecolor='0.80', edgecolor='0.85', alpha=0.5, zorder=12)

  #--------------------------------
  # Rangers, Annotations
  #--------------------------------
  ax11.set_xlim(mass_from, mass_to)
  ax11.set_ylim(chisq_from, chisq_to)
  ax11.set_xlabel(r'Mass [M$_\odot$]')
  ax11.set_ylabel(r'$\log \chi^2$')

  ax12.set_xlim(ov_from, ov_to)
  ax12.set_ylim(chisq_from, chisq_to)
  if ov_to > 0.10:
    ax12.set_xlabel(r'$\alpha_{\rm ov}$')
  else:
    ax12.set_xlabel(r'$f_{\rm ov}$')
  ax12.set_ylabel(r'$\log \chi^2$')

  ax13.set_xlim(Z_from, Z_to)
  ax13.set_ylim(chisq_from, chisq_to)
  ax13.set_xlabel(r'$Z$')
  ax13.set_ylabel(r'$\log \chi^2$')
  for label in ax13.xaxis.get_ticklabels()[::2]:
    label.set_visible(False)

  ax21.set_xlim(logD_from, logD_to)
  ax21.set_ylim(chisq_from, chisq_to)
  ax21.set_xlabel(r'$\log D_{\rm ext}$ [cm$^2$ sec$^{-1}$]')
  ax21.set_ylabel(r'$\log \chi^2$')
  xticks = ax21.get_xticks()
  # n_xticks = len(xticks)
  ax21.set_xticks([0, 1, 2, 3, 4, 5])
  ax21.set_xticklabels(('None', '1', '2', '3', '4', '5'))  

  ax22.set_xlim(Xc_from, Xc_to)
  ax22.set_ylim(chisq_from, chisq_to)
  ax22.set_xlabel(r'$X_{\rm c}$')
  ax22.set_ylabel(r'$\log \chi^2$')

  ax23.set_xlim(logTeff_from, logTeff_to)
  ax23.set_ylim(logg_from, logg_to)
  ax23.set_xlabel(r'$\log$ T$_{\rm eff}$ [K]')
  ax23.set_ylabel(r'$\log g$ [cm sec$^{-1}$]')
  ax23.tick_params(axis='x', which='major', labelsize=10)
  # cb = fig.colorbar(ax=ax23, shrink=0.95)
  cb = plt.colorbar(kiel)
  # cb = matplotlib.colorbar.ColorbarBase(ax23, cmap=cmap, norm=norm)

  # Add running labels to all panels
  # plot_commons.add_roman_counter_to_fig_axes(fig, x=0.05, y=0.05, fontsize='medium')
  plot_commons.add_roman_counter_to_fig_axes(fig, x=0.88, y=0.05, fontsize='medium')

  #--------------------------------
  # Store the plot
  #--------------------------------
  # plt.savefig(file_out, dpi=200)
  plt.savefig(file_out, transparent=True)
  print ' - plot_chisq: grid_param_vs_chisq: saved {0}'.format(file_out)
  plt.close()

  return fig

#=================================================================================================================
def grid_param_vs_chisq_around_best_model(list_dic_chisq, by='reduced_chisq_seism_freq', file_out=None):
  """
  INCOMPLETE ...
  Throw all chisquare values as scatterd points on different panels, where the axes for each panel comes from grid
  parameters. The plotting is done first from those points with the highest chisquare values being in the background,
  and proceeds to the best model with the minimum chisquare value in the very front of the panel. This is somehow similar
  to Fig. 6 in Moravveji et al. (2014), but without interpolation.
  @param list_dic_chisq: list of dictionaries with the mode frequency and/or period spacing information, in
         addition to chisq information in its items(). We check for the presence of the required chisq key in
         advance. This list can be retrieved from e.g. mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type list_dic_chisq: list of dictionaries
  @param by: specify whcih chisq criteria to be used. The allowd names are:
        - reduced_chisq_evol/chisq_evol: returned by mesa_gyre.chisq.evol()
        - chisq_one_fit: returned by mesa_gyre.chisq.fit_one_freq()
        - reduced_chisq_seism_freq/chisq_seism_freq: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P/chisq_seism_P; returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_dP/chisq_seism/dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P_and_dP/chisq_seism_P_and_dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type by: string
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: matplotlib figure object
  @rtype: object
  """
  n_dic = len(list_dic_chisq)
  if n_dic == 0:
    logging.error('plot_chisq: grid_param_vs_chisq_around_best_model: Input list is empty')
    raise SystemExit
  if by not in list_dic_chisq[0].keys():
    logging.error('plot_chisq: grid_param_vs_chisq_around_best_model: "{0}" not found among available keys.'.format(by))
    raise SystemExit

  arr_chisq  = np.array([ dic[by] for dic in list_dic_chisq ])
  ind_sort   = np.argsort(arr_chisq)[::-1]     # from max to the min
  arr_chisq  = arr_chisq[ind_sort]
  list_dic_chisq = [list_dic_chisq[i] for i in ind_sort]

  max_chisq  = arr_chisq[0]
  min_chisq  = arr_chisq[-1]

  best_dic   = list_dic_chisq[0]
  best_fov   = best_dic['ov']
  best_Z     = best_dic['Z']
  best_logD  = best_dic['logD']
  best_Xc    = np.round(best_dic['Xc'], decimals=3)

  print ' plot_chisq.grid_param_vs_chisq(): Parameters of the best model:'
  print ' Chisq={0}'.format(min_chisq)
  print ' M={0}, fov={1}, Z={2}, logD={3}, Xc={4} \n'.format(best_mass, best_fov, best_Z, best_logD, best_Xc)

  list_mass  = np.array([ dic['M'] for dic in list_dic_chisq ])
  list_fov   = np.array([ dic['ov'] for dic in list_dic_chisq ])
  list_Z     = np.array([ dic['Z'] for dic in list_dic_chisq ])
  list_logD  = np.array([ dic['logD'] for dic in list_dic_chisq ])
  list_Xc    = np.array([ np.round(dic['Xc'], decimals=3) for dic in list_dic_chisq ])

  #--------------------------------
  # Prepare the figure
  #--------------------------------
  import matplotlib.cm as cm
  fig = plt.figure(figsize=(10, 10), dpi=200)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.12, right=0.86, bottom=0.12, top=0.97)

  # Create 10 panels

  #--------------------------------
  # Define dummy lists for all panels
  #--------------------------------
  list_dic_for_panel = []

  #--------------------------------
  # Panel a) mass versus fov
  #--------------------------------
  ind_dics_for_panel     = np.where((list_Z == best_Z) & (list_logD == best_logD) & (list_Xc == best_Xc))[0]
  list_dic_for_panel     = [list_dic_chisq[i] for i in ind_dics_for_panel]
  list_chisq_for_panel   = arr_chisq[ind_dics_for_panel]
  list_mass_for_panel    = list_mass[ind_dics_for_panel]
  list_fov_for_panel     = list_fov[ind_dics_for_panel]
  set_mass               = list(set(list_mass_for_panel))
  set_mass.sort()
  set_fov                = list(set(list_fov_for_panel))
  set_fov.sort()


  #--------------------------------
  # Panel b) fov versus Xc
  #--------------------------------
  ind_dics_for_panel     = np.where((list_mass == best_mass) & (list_Z == best_Z) & (list_logD == best_logD))[0]
  list_dic_for_panel     = [list_dic_chisq[i] for i in ind_dics_for_panel]
  list_chisq_for_panel   = arr_chisq[ind_dics_for_panel]
  list_fov_for_panel     = list_fov[ind_dics_for_panel]
  list_Xc_for_panel      = list_Xc[ind_dics_for_panel]
  set_fov                = list(set(list_fov_for_panel))
  set_fov.sort()
  set_Xc                 = list(set(list_Xc_for_panel))
  set_Xc.sort()


  #--------------------------------
  # panel c) logD versus fov
  #--------------------------------
  ind_dics_for_panel     = np.where((list_mass == best_mass) & (list_Z == best_Z) & (list_Xc == best_Xc))[0]
  list_dic_for_panel     = [list_dic_chisq[i] for i in ind_dics_for_panel]
  list_chisq_for_panel   = arr_chisq[ind_dics_for_panel]
  list_fov_for_panel     = list_fov[ind_dics_for_panel]
  list_logD_for_panel    = list_logD[ind_dics_for_panel]
  set_fov                = list(set(list_fov_for_panel))
  set_fov.sort()
  set_logD               = list(set(list_logD_for_panel))
  set_logD.sort()

  print set_fov
  print set_logD
  print len(set_logD), len(set_fov), len(ind_dics_for_panel)





#=================================================================================================================
def mean_dP_vs_x(list_dic_chisq, xaxis=None, by='reduced_chisq_seism_freq',
               chisq_from=None, chisq_to=None, xlabel=None, xaxis_from=None, xaxis_to=None,
               dP_from=None, dP_to=None, file_out=None):
  """
  a single panel plot, with color highlighting corresponding to chisquare values, showing trends of x w.r.t y,
  e.g. how mean dP varies with Xc.
  @param list_dic_chisq: list of dictionaries with the mode frequency and/or period spacing information, in
         addition to chisq information in its items(). We check for the presence of the required chisq key in
         advance. This list can be retrieved from e.g. mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type list_dic_chisq: list of dictionaries
  @param by: specify whcih chisq criteria to be used. The allowd names are:
        - reduced_chisq_evol/chisq_evol: returned by mesa_gyre.chisq.evol()
        - chisq_one_fit: returned by mesa_gyre.chisq.fit_one_freq()
        - reduced_chisq_seism_freq/chisq_seism_freq: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P/chisq_seism_P; returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_dP/chisq_seism/dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P_and_dP/chisq_seism_P_and_dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type by: string
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: matplotlib figure object
  @rtype: object
  """
  yaxis = 'el_1_gm_dP_mean'
  n_dic = len(list_dic_chisq)
  if n_dic == 0:
    logging.error('plot_chisq: mean_dP_vs_x: Empty input list')
    raise SystemExit
  if xaxis is None or yaxis is None:
    logging.error('plot_chisq: mean_dP_vs_x: xaxis or yaxis not set')
    raise SystemExit
  a_dic = list_dic_chisq[0]
  avail_keys = a_dic.keys()
  if xaxis not in avail_keys:
    logging.error('plot_chisq: mean_dP_vs_x: {0} not availabe among dictionary keys'.format(xaxis))
  if yaxis not in avail_keys:
    logging.error('plot_chisq: mean_dP_vs_x: {0} not availabe among dictionary keys'.format(yaxis))
  if by not in avail_keys:
    logging.error('plot_chisq: mean_dP_vs_x: {0} not availabe among dictionary keys'.format(by))

  # exclude outlying models; they already have dic[by] == 1e99
  max_allowed_chisq = 1e90
  ind_trimmed = [i for i in range(n_dic) if list_dic_chisq[i][by] < max_allowed_chisq]
  list_dic_chisq = [list_dic_chisq[i] for i in ind_trimmed]
  n_dic       = len(list_dic_chisq)
  if n_dic == 0:
    logging.error('plot_chisq: mean_dP_vs_x: No modedl ramined after trimming for chisq<={0}'.format(max_allowed_chisq))
    raise SystemExit
  list_chisq  = np.array( [dic[by] for dic in list_dic_chisq] )
  if chisq_from is None: chisq_from = np.min(list_chisq)
  if chisq_to   is None:
    chisq_to   = np.max(list_chisq)
  else:
    ind_trimmed = [i for i in range(n_dic) if list_dic_chisq[i][by] <= chisq_to]
    list_dic_chisq = [list_dic_chisq[i] for i in ind_trimmed]
    list_chisq  = np.array( [dic[by] for dic in list_dic_chisq] )
    n_dic = len(list_dic_chisq)
  print ' - plot_chisq: mean_dP_vs_x: Considering {0} <= chisquare <= {1}'.format(chisq_from, chisq_to)

  # sort the dictionaries by their chisq value (i.e. dic[by]) in "decreasing" order
  ind_sort       = list_chisq.argsort()[::-1]
  list_dic_chisq = [list_dic_chisq[i] for i in ind_sort]
  list_chisq     = list_chisq[ind_sort]

  #--------------------------------
  # Prepare the figure
  #--------------------------------
  import matplotlib.cm as cm
  fig = plt.figure(figsize=(6, 4), dpi=200)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.12, right=0.86, bottom=0.12, top=0.97)
  normalized_chisq = list_chisq / chisq_to
  clr = np.array( [plt.cm.Spectral(j) for j in normalized_chisq] )

  #--------------------------------
  # Iterate over dictionaries, and plot them
  #--------------------------------
  list_xaxis = []
  list_yaxis = []
  for i_dic, dic in enumerate(list_dic_chisq):
    xval   = dic[xaxis]
    yval   = dic[yaxis]
    list_xaxis.append(xval)
    list_yaxis.append(yval)

    ax.scatter([xval], [yval], color=clr[i_dic], s=16, zorder=i_dic)

  #--------------------------------
  # Color Bar
  #--------------------------------
  pos     = plt.axes([0.88, 0.12, 0.05, 0.85]) #[left,bottom,width,height]
  cb_vals = np.linspace(0, 1, num=5, endpoint=True)
  cb_str  = [str(int(val)) for val in np.linspace(chisq_from, chisq_to, num=5, endpoint=True)]
  cb      = matplotlib.colorbar.ColorbarBase(pos, ticks=cb_vals, cmap=plt.cm.Spectral, orientation='vertical')
  cb.set_ticks(cb_vals)
  cb.set_ticklabels(cb_str)

  #--------------------------------
  # Ranges, Annotations
  #--------------------------------
  if xaxis_from is None: xaxis_from = min(list_xaxis) * 0.90
  if xaxis_to is None:   xaxis_to   = max(list_xaxis) * 1.10
  if dP_from is None:    dP_from    = min(list_yaxis) * 0.90
  if dP_to is None:      dP_to      = max(list_yaxis) * 1.10
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(dP_from, dP_to)
  if xlabel: ax.set_xlabel(xlabel)
  ax.set_ylabel(r'Mean $\Delta P$ [sec]')

  #--------------------------------
  # Background box highlighting
  #--------------------------------
  obs_mean_dP     = 5428  # sec
  obs_mean_dP_err = 211   # sec
  sigma           = 1.25
  ax.fill_between([xaxis_from, xaxis_to], y1=obs_mean_dP-sigma*obs_mean_dP_err,
                   y2=obs_mean_dP+sigma*obs_mean_dP_err, color='0.15', alpha=0.25, zorder=0)

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  if file_out:
    plt.savefig(file_out, dpi=200)
    print ' - plot_chisq: mean_dP_vs_x: saved {0}'.format(file_out)
    plt.close()

  return fig

#=================================================================================================================
def x_vs_y(list_dic_chisq, list_params=None, by='reduced_chisq_seism_P_and_dP', file_out=None):
  """
  just a simple output producer
  """
  n_dic = len(list_dic_chisq)
  xaxis = list_params[0]
  yaxis = list_params[1]
  a_dic = list_dic_chisq[0]
  if not a_dic.has_key(xaxis):
    logging.error('plot_chisq: x_vs_y: {0} not in the dictionary key'.format(xaxis))
    raise SystemExit
  if not a_dic.has_key(yaxis):
    logging.error('plot_chisq: x_vs_y: {0} not in the dictionary key'.format(yaxis))
    raise SystemExit
  if not a_dic.has_key(by):
    logging.error('plot_chisq: x_vs_y: {0} not in the dictionary key'.format(by))
    raise SystemExit

  chisq_vals = np.array( [dic[by] for dic in list_dic_chisq] )
  ind_sort   = np.argsort(chisq_vals)
  list_dic_chisq = [list_dic_chisq[i] for i in ind_sort]

  #--------------------------------
  # Prepare the figure
  #--------------------------------
  fig = plt.figure(figsize=(6, 4), dpi=200)
  ax = fig.add_subplot(1,1,1)
  plt.subplots_adjust(left=0.15, right=0.98, bottom=0.12, top=0.97)

  #--------------------------------
  # Loop over dictionaries
  #--------------------------------
  list_xaxis = []
  list_yaxis = []
  list_chisq = []
  for i_dic, dic in enumerate(list_dic_chisq):
    filename   = dic['filename']
    dic_params = param_tools.get_param_from_single_gyre_filename(filename)
    xval       = dic_params[xaxis]
    yval       = dic_params[yaxis]
    chisq      = dic[by]
    list_xaxis.append(xval)
    list_yaxis.append(yval)
    list_chisq.append(chisq)

  list_xaxis   = np.array(list_xaxis)
  list_yaxis   = np.array(list_yaxis)
  list_chisq   = np.array(list_chisq)

  ind_sort     = np.argsort(list_chisq)
  list_xaxis   = list_xaxis[ind_sort]
  list_yaxis   = list_yaxis[ind_sort]
  list_chisq   = list_chisq[ind_sort]
  min_chisq    = np.min(list_chisq)
  bad_points   = list_chisq >= 1e98
  max_chisq    = np.max(list_chisq[list_chisq<1e98])
  list_chisq[bad_points] = max_chisq
  n_chisq      = len(list_chisq)

  if xaxis == 'sc': list_xaxis = np.log10(list_xaxis)
  if yaxis == 'sc': list_yaxis = np.log10(list_yaxis)
  #--------------------------------
  # Set Up the Colors
  #--------------------------------
  import matplotlib.cm as cm
  cmap = plt.get_cmap('brg')
  norm = matplotlib.colors.Normalize(vmin=min_chisq, vmax=max_chisq)
  color = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)

  #--------------------------------
  # Plot
  #--------------------------------
  symsize = 100 * min_chisq / list_chisq
  #for i_pt in range(n_chisq):
    #sc_plot = ax.scatter([list_xaxis[i_pt]], [list_yaxis[i_pt]], marker='s', s=9, color=color.to_rgba(i_pt), alpha=0.5)
  ax.scatter([list_xaxis[0]], [list_yaxis[0]], marker='x', s=160, color='red', zorder=1)
  sc_plot = ax.scatter(list_xaxis, list_yaxis, marker='s', s=symsize, c=list_chisq,
                       vmin=min_chisq, vmax=max_chisq, cmap=cmap, alpha=0.5, zorder=2)

  #--------------------------------
  # Annotations, Legend, Finalize it
  #--------------------------------
  #plt.colorbar(sc_plot)
  x_range = np.abs(np.max(list_xaxis) - np.min(list_xaxis))
  y_range = np.abs(np.max(list_yaxis) - np.min(list_yaxis))
  x_extra = x_range * 0.05
  y_extra = y_range * 0.05
  ax.set_xlim(np.min(list_xaxis)-x_extra, np.max(list_xaxis)+x_extra)
  ax.set_ylim(np.min(list_yaxis)-y_extra, np.max(list_yaxis)+y_extra)
  if xaxis == 'sc':
    ax.set_xlabel(r'$\log\alpha_{\rm sc}$')
  else:
    ax.set_xlabel(r''+xaxis)
  if yaxis == 'sc':
    ax.set_ylabel(r'$\log\alpha_{\rm sc}$')
  else:
    ax.set_ylabel(r''+yaxis)

  if file_out:
    plt.savefig(file_out, dpi=200)
    print ' - plot_chisq: x_vs_y: {0} saved'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def scattered_points(list_dic_chisq, list_params=None, by='reduced_chisq_seism_P_and_dP', file_out=None):
  """
  Create a 3D scatter plot with size of each point weighted by the 1./chisquare value, so that the best point have
  larger sizes.
  @param list_dic_chisq: list of dictionaries with the mode frequency and/or period spacing information, in
         addition to chisq information in its items(). We check for the presence of the required chisq key in
         advance. This list can be retrieved from e.g. mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type list_dic_chisq: list of dictionaries
  @param list_params: specify which parameters to plot on the x-, y- and z-axis for the plotting.
         E.g. list_params = ['M', 'ov', 'Z'].
         Note: The x- y- and z-axis will be chosen by the same item order in list_params, so that e.g. x-axes
               is list_params[0] and so on.
         WARNING: list_params is restricted to only have 3 field names for 3 axes
  @type list_params: list of strings
  @param by: specify whcih chisq criteria to be used. The allowd names are:
        - reduced_chisq_evol/chisq_evol: returned by mesa_gyre.chisq.evol()
        - chisq_one_fit: returned by mesa_gyre.chisq.fit_one_freq()
        - reduced_chisq_seism_freq/chisq_seism_freq: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P/chisq_seism_P; returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_dP/chisq_seism/dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P_and_dP/chisq_seism_P_and_dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type by: string
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  n_dic = len(list_dic_chisq)
  if n_dic == 0:
    logging.error('plot_chisq.scattered_points: Input list is empty')
    raise SystemExit
  n_par = len(list_params)
  if n_par != 3:
    logging.error('plot_chisq: scattered_points: keyword list_params only accepts 3 members')
    raise SystemExit

  a_dic      = list_dic_chisq[0]
  a_filename = a_dic['filename']
  dic_params = param_tools.get_param_from_single_gyre_filename(a_filename)
  if not a_dic.has_key(by):
    logging.error('plot_chisq: scattered_points: {0} not in available dictionary keys!'.format(by))
    raise SystemExit
  for a_par in list_params:
    if not dic_params.has_key(a_par):
      logging.error('plot_chisq: scattered_points: {0} not in filename'.format(a_par))
      raise SystemExit

  #--------------------------------
  # Prepare the figure
  #--------------------------------
  from mpl_toolkits.mplot3d.axes3d import Axes3D
  import matplotlib.cm as cm
  fig = plt.figure(figsize=(6,6), dpi=200)
  ax = fig.add_subplot(1,1,1, projection='3d')
  plt.subplots_adjust()

  #--------------------------------
  # Read in all values from dictionaries
  #--------------------------------
  xaxis = list_params[0]
  yaxis = list_params[1]
  zaxis = list_params[2]
  list_files = []
  list_chisq = []
  list_xaxis = []
  list_yaxis = []
  list_zaxis = []
  for i_dic, dic in enumerate(list_dic_chisq):
    the_chisq = dic[by]
    filename   = dic['filename']
    dic_params = param_tools.get_param_from_single_gyre_filename(filename)
    list_chisq.append(the_chisq)
    list_files.append(filename)
    list_xaxis.append(dic_params[xaxis])
    list_yaxis.append(dic_params[yaxis])
    list_zaxis.append(dic_params[zaxis])

  #--------------------------------
  # convert the lists to np.ndarray,
  # sort them, extract unique values along x- and y-axis,
  #--------------------------------
  list_chisq = np.array(list_chisq)
  list_xaxis = np.array(list_xaxis)
  list_yaxis = np.array(list_yaxis)
  list_zaxis = np.array(list_zaxis)
  ind_sort   = np.argsort(list_chisq)
  list_chisq = list_chisq[ind_sort]
  list_xaxis = list_xaxis[ind_sort]
  list_yaxis = list_yaxis[ind_sort]
  list_zaxis = list_zaxis[ind_sort]
  if xaxis == 'sc': list_xaxis = np.log10(list_xaxis)
  if yaxis == 'sc': list_yaxis = np.log10(list_yaxis)
  if zaxis == 'sc': list_zaxis = np.log10(list_zaxis)

  #--------------------------------
  # Chisquare ranges
  #--------------------------------
  n_points   = len(list_chisq)
  min_chisq  = np.min(list_chisq)
  max_chisq  = np.max(list_chisq)
  for i_ch, chsq in enumerate(list_chisq):
    if chsq != min_chisq:
      continue
    else:
      x_val_for_min_chisq = list_xaxis[i_ch]
      y_val_for_min_chisq = list_yaxis[i_ch]
      z_val_for_min_chisq = list_zaxis[i_ch]
      break
  xvals      = set(list_xaxis)
  yvals      = set(list_yaxis)
  zvals      = set(list_zaxis)

  #--------------------------------
  # Plot
  #--------------------------------
  point_sizes = 25. *  min_chisq/list_chisq
  ax.scatter(list_xaxis, list_yaxis, zs=list_zaxis, zdir='z', s=point_sizes, c=list_chisq, alpha=0.5)
  ax.scatter([x_val_for_min_chisq], [y_val_for_min_chisq], zs=[z_val_for_min_chisq], s=50, c='red', marker='x')

  #--------------------------------
  # Annotations and Ranges
  #--------------------------------
  xname = xaxis
  yname = yaxis
  zname = zaxis
  if xaxis == 'M':   xname = r'Mass [M$_\odot$]'
  if xaxis == 'eta': xname = r'$\eta$'
  if xaxis == 'ov':  xname = r'$f_{\rm ov}$'
  if xaxis == 'sc':  xname = r'$\log \alpha_{\rm sc}$'
  if xaxis == 'Z':   xname = r'Z'
  if xaxis == 'Yc':  xname = r'Y$_{\rm c}$'
  if xaxis == 'Xc':  xname = r'X$_{\rm c}$'
  if yaxis == 'M':   yname = r'Mass [M$_\odot$]'
  if yaxis == 'eta': yname = r'$\eta$'
  if yaxis == 'ov':  yname = r'$f_{\rm ov}$'
  if yaxis == 'sc':  yname = r'$\log \alpha_{\rm sc}$'
  if yaxis == 'Z':   yname = r'Z'
  if yaxis == 'Yc':  yname = r'Y$_{\rm c}$'
  if yaxis == 'Xc':  yname = r'X$_{\rm c}$'
  if zaxis == 'M':   zname = r'Mass [M$_\odot$]'
  if zaxis == 'eta': zname = r'$\eta$'
  if zaxis == 'ov':  zname = r'$f_{\rm ov}$'
  if zaxis == 'sc':  zname = r'$\log \alpha_{\rm sc}$'
  if zaxis == 'Z':   zname = r'Z'
  if zaxis == 'Yc':  zname = r'Y$_{\rm c}$'
  if zaxis == 'Xc':  zname = r'X$_{\rm c}$'

  ax.set_xlabel(xname)
  ax.set_xlim(min(xvals), max(xvals))

  ax.set_ylabel(yname)
  ax.set_ylim(min(yvals), max(yvals))

  ax.set_zlabel(zname)
  ax.set_zlim(min(zvals), max(zvals))

  if file_out:
    fig.tight_layout()
    plt.savefig(file_out, dpi=200)
    print ' - plot_chisq: scattered_points: {0} saved'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def contour_with_projection(list_dic_chisq, list_params=None, by='reduced_chisq_seism_P_and_dP', file_out=None):
  """
  Create a 3D surface plot with projected contours on the constant x, y and z planes
  @param list_dic_chisq: list of dictionaries with the mode frequency and/or period spacing information, in
         addition to chisq information in its items(). We check for the presence of the required chisq key in
         advance. This list can be retrieved from e.g. mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type list_dic_chisq: list of dictionaries
  @param list_params: specify which parameters to plot on the x-, y- axis for the plotting.
         E.g. list_params = ['M', 'ov'].
         Note: The x- and y-axis will be chosen by the same item order in list_params, so that e.g. x-axes
               is list_params[0] and so on.
         WARNING: list_params is restricted to only have 2 field names for 2 axes; chisquare value goes on the the
         3rd axis
  @type list_params: list of strings
  @param by: specify whcih chisq criteria to be used. The allowd names are:
        - reduced_chisq_evol/chisq_evol: returned by mesa_gyre.chisq.evol()
        - chisq_one_fit: returned by mesa_gyre.chisq.fit_one_freq()
        - reduced_chisq_seism_freq/chisq_seism_freq: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P/chisq_seism_P; returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_dP/chisq_seism/dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P_and_dP/chisq_seism_P_and_dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type by: string
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  n_dic = len(list_dic_chisq)
  if n_dic == 0:
    logging.error('plot_chisq.contour_with_projection: Input list is empty')
    raise SystemExit
  n_par = len(list_params)
  if n_par != 2:
    logging.error('plot_chisq: contour_with_projection: keyword list_params only accepts 2 members')
    raise SystemExit

  a_dic      = list_dic_chisq[0]
  a_filename = a_dic['filename']
  dic_params = param_tools.get_param_from_single_gyre_filename(a_filename)
  if not a_dic.has_key(by):
    logging.error('plot_chisq: contour_with_projection: {0} not in available dictionary keys!'.format(by))
    raise SystemExit
  for a_par in list_params:
    if not dic_params.has_key(a_par):
      logging.error('plot_chisq: contour_with_projection: {0} not in filename'.format(a_par))
      raise SystemExit

  #--------------------------------
  # Prepare the figure
  #--------------------------------
  import matplotlib
  from mpl_toolkits.mplot3d.axes3d import Axes3D
  import matplotlib.cm as cm
  fig = plt.figure(figsize=(6,6), dpi=200)
  ax = fig.add_subplot(1,1,1, projection='3d')
  #plt.subplots_adjust(left=0.02, right=0.05, bottom=0.05, top=0.98)

  #--------------------------------
  # Read in all values from dictionaries
  #--------------------------------
  xaxis = list_params[0]
  yaxis = list_params[1]
  #zaxis = list_params[2]
  #list_files = []
  list_chisq = []
  list_xaxis = []
  list_yaxis = []
  for i_dic, dic in enumerate(list_dic_chisq):
    the_chisq = dic[by]
    filename   = dic['filename']
    dic_params = param_tools.get_param_from_single_gyre_filename(filename)
    list_chisq.append(the_chisq)
    list_xaxis.append(dic_params[xaxis])
    list_yaxis.append(dic_params[yaxis])

  #--------------------------------
  # convert the lists to np.ndarray,
  # sort them, extract unique values along x- and y-axis,
  #--------------------------------
  list_chisq = np.array(list_chisq)
  list_xaxis = np.array(list_xaxis)
  list_yaxis = np.array(list_yaxis)
  ind_sort   = np.argsort(list_chisq)
  list_chisq = list_chisq[ind_sort]
  list_xaxis = list_xaxis[ind_sort]
  list_yaxis = list_yaxis[ind_sort]
  if xaxis == 'sc': list_xaxis = np.log10(list_xaxis)
  if yaxis == 'sc': list_yaxis = np.log10(list_yaxis)

  #--------------------------------
  # Chisquare ranges
  #--------------------------------
  n_points   = len(list_chisq)
  min_chisq  = np.min(list_chisq)
  max_chisq  = np.max(list_chisq[list_chisq < 1e98])
  list_chisq[list_chisq == 1e99] = max_chisq

  #--------------------------------
  # Create x and y meshgrid
  #--------------------------------
  xvals = np.sort(np.array( list( Counter(list_xaxis) ) ))
  yvals = np.sort(np.array( list( Counter(list_yaxis) ) ))
  n_xaxis    = len(xvals)
  n_yaxis    = len(yvals)
  x_grid, y_grid = np.meshgrid(xvals, yvals)
  grid_shape = x_grid.shape

  #--------------------------------
  # Create a 2-D surface of chisq
  #--------------------------------
  surf_chiq  = np.ones((n_xaxis, n_yaxis)) * max_chisq
  i_count    = 0
  for i_pt in range(n_points):
    the_chisq    = list_chisq[i_pt]
    true_x_value = list_xaxis[i_pt]
    true_y_value = list_yaxis[i_pt]
    ind_x        = np.where((true_x_value == xvals))[0]
    ind_y        = np.where((true_y_value == yvals))[0]
    if surf_chiq[ind_x, ind_y] != max_chisq:
      print 'repeat chached: ', ind_x, ind_y, true_x_value, true_y_value, surf_chiq[ind_x, ind_y], the_chisq
    if true_x_value != xvals[ind_x] or true_y_value != yvals[ind_y]:
      logging.error('plot_chisq: contour_with_projection: Issue Indexing')
      raise SystemExit
    surf_chiq[ind_x, ind_y] = the_chisq
    if the_chisq == min_chisq:
      x_val_for_min_chisq = true_x_value
      y_val_for_min_chisq = true_y_value
  surf_chiq = surf_chiq.T

  #--------------------------------
  # Set Offsets and axis ranges
  #--------------------------------
  xleft = xvals[0] - 2. * (xvals[1] - xvals[0])
  ytop  = yvals[-1] + 2. * (yvals[-1] - yvals[-2])
  zbot  = -np.max(surf_chiq)/4.
  ax.set_xlim3d(xleft, np.max(xvals))
  ax.set_ylim3d(np.min(yvals), ytop)
  ax.set_zlim3d(zbot, np.max(surf_chiq))

  #--------------------------------
  # Annotations and Ranges
  #--------------------------------
  xname = xaxis
  yname = yaxis
  if xaxis == 'M':   xname = r'Mass [M$_\odot$]'
  if xaxis == 'eta': xname = r'$\eta$'
  if xaxis == 'ov':  xname = r'$f_{\rm ov}$'
  if xaxis == 'sc':  xname = r'$\log \alpha_{\rm sc}$'
  if xaxis == 'Z':   xname = r'Z'
  if xaxis == 'Yc':  xname = r'Y$_{\rm c}$'
  if xaxis == 'Xc':  xname = r'X$_{\rm c}$'
  if yaxis == 'M':   yname = r'Mass [M$_\odot$]'
  if yaxis == 'eta': yname = r'$\eta$'
  if yaxis == 'ov':  yname = r'$f_{\rm ov}$'
  if yaxis == 'sc':  yname = r'$\log \alpha_{\rm sc}$'
  if yaxis == 'Z':   yname = r'Z'
  if yaxis == 'Yc':  yname = r'Y$_{\rm c}$'
  if yaxis == 'Xc':  yname = r'X$_{\rm c}$'

  ax.set_xlabel(xname)
  ax.set_ylabel(yname)
  ax.set_zlabel(r'$\chi^2_{\rm red}$')

  #--------------------------------
  # Plot
  #--------------------------------
  ax.plot_surface(x_grid, y_grid, surf_chiq, rstride=4, cstride=4, alpha=0.50)
  cset = ax.contour(x_grid, y_grid, surf_chiq, zdir='x', offset=xleft, cmap=cm.coolwarm)
  cset = ax.contour(x_grid, y_grid, surf_chiq, zdir='y', offset=ytop, cmap=cm.coolwarm)
  cset = ax.contour(x_grid, y_grid, surf_chiq, zdir='z', offset=zbot, cmap=cm.coolwarm, zorder=1)
  cset = ax.scatter(list_xaxis, list_yaxis, [zbot]*len(list_xaxis), c='k', s=1, zorder=2)
  cset = ax.scatter([x_val_for_min_chisq], [y_val_for_min_chisq], [zbot], c='red', s=49, zorder=3)

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  if file_out:
    ind_point = file_out.rfind('.')
    file_out = file_out[:ind_point] + '-' + xaxis + '-vs-' + yaxis + file_out[ind_point:]
    plt.savefig(file_out, dpi=200)
    print ' - plot_chisq: contour_with_projection: {0} created'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def param_summary(list_dic_chisq, list_params=None, by='reduced_chisq_seism_freq', contour=True,
                  factor=None, file_out=None):
  """
  To generate a multi-panel plot that shows the value of chi-square with color highlighting, and iterates
  over all parameters on the x- and y-axis specified in list_params. Each panel is created by calling
  mesa_gyre.plot_chisq.append_chisq_contour_panel().
  @param list_dic_chisq: list of dictionaries with the mode frequency and/or period spacing information, in
         addition to chisq information in its items(). We check for the presence of the required chisq key in
         advance. This list can be retrieved from e.g. mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type list_dic_chisq: list of dictionaries
  @param list_params: specify which parameters to plot on the x- and y-axis for the plotting.
         E.g. list_params = ['M', 'ov', 'Z']
  @type list_params: list of strings
  @param xaxis/yaxis: specify which parameter goes on the xaxis/yaxis. They have to be the part of the filename.
        Therefore, the only allowed names are 'M', 'eta', 'ov', 'sc', 'Z', 'Yc' and 'Xc'.
  @type xaxis/yaxis: string
  @param factor: if not None, only those models with chisq between 1 and factor times the minimum chisquare are plotted
  @type factor: float
  @param by: specify whcih chisq criteria to be used. The allowd names are:
        - reduced_chisq_evol/chisq_evol: returned by mesa_gyre.chisq.evol()
        - chisq_one_fit: returned by mesa_gyre.chisq.fit_one_freq()
        - reduced_chisq_seism_freq/chisq_seism_freq: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P/chisq_seism_P; returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_dP/chisq_seism/dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P_and_dP/chisq_seism_P_and_dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type by: string
  @param contour: if set, the output is a contour plot, otherwise, it is just a "scatter" plot of xaxis versus
         yaxis color-coded with chisq values.
  @type contour: boolean; default=True
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  n_dic = len(list_dic_chisq)
  if n_dic == 0:
    logging.error('plot_chisq.param_summary: Input list_dic_chisq is empty')
    raise SystemExit
  n_par = len(list_params)
  if n_par < 1:
    logging.error('plot_chisq.param_summary: Input list_params is inadequate')
    raise SystemExit

  a_dic      = list_dic_chisq[0]
  a_filename = a_dic['filename']
  if not a_dic.has_key(by):
    logging.error('plot_chisq: param_summary: {0} not available in input dictionaries'.format(by))
    raise SystemExit
  dic_params = param_tools.get_param_from_single_gyre_filename(a_filename)
  for par in list_params:
    if not a_dic.has_key(par):
      logging.error('plot_chisq.param_summary: {0} not in filename'.format(par))
      raise SystemExit
  if file_out is None:
    logging.warning('plot_chisq: param_summary: file_out is None')
    return None

  #--------------------------------
  # Configure Panels
  #--------------------------------
  n_panels   = n_par * (n_par-1) / 2
  #n_col      = int(np.sqrt(n_panels)) + 1
  n_col      = n_par - 1
  #n_row      = int(np.sqrt(n_panels)) + 1
  n_row      = int( np.ceil(n_panels / n_col) )
  xaxis_lbls = set()
  yaxis_lbls = set()


  #--------------------------------
  # Configure Plots
  #--------------------------------
  if contour:
    if n_col == n_row == 1: figsize = (4, 4)
    else:                   figsize = (2*n_col, 2*n_row)
    fig = plt.figure(figsize=figsize, dpi=200)

    panel_names = set()
    for i_panel in range(n_panels):
      for i_x, x_col in enumerate(list_params):
        for i_y, y_col in enumerate(list_params):
          if x_col == y_col: continue
          if (x_col, y_col) in panel_names: continue
          if (y_col, x_col) in panel_names: continue

          ax = fig.add_subplot(n_col, n_row, i_panel+1)
          fig.subplots_adjust(left=0.15, right=0.98, bottom=0.15, top=0.98, wspace=0.05, hspace=0.05)

          ind_point = file_out.rfind('.')
          plot_name = file_out[:ind_point] + '-Contour-' + x_col + '-vs-' + y_col + file_out[ind_point:]
          append_chisq_contour_panel(list_dic_chisq, ax, xaxis=x_col, yaxis=y_col, by=by, factor=factor,
                                     contour=True, n_chisq=200, file_out=plot_name)
          panel_names.add( (x_col, y_col) )
          print ' - panel:{0}, {1}, {2}: {3} vs {4}'.format(n_col, n_row, i_panel+1, x_col, y_col)

    if file_out:
      plt.savefig(file_out, dpi=200)
      print ' - plot_chisq: param_summary: {0} created'.format(file_out)
      plt.close()


  if not contour:
    fig = plt.figure(figsize=(6,4), dpi=200)
    ax = fig.add_subplot(111)
    fig.subplots_adjust(left=0.16, right=0.97, bottom=0.17, top=0.97, wspace=0.05, hspace=0.05)
    for i_panel in range(n_par):
      x_col = list_params[i_panel]
      ind_point = file_out.rfind('.')
      plot_name = file_out[:ind_point] + '-Scatter-by-' + x_col + file_out[ind_point:]
      append_chisq_contour_panel(list_dic_chisq, ax, xaxis=x_col, yaxis=x_col, by=by, factor=factor,
                                 contour=False, file_out=plot_name)

    if file_out:
      plt.savefig(file_out, dpi=200)
      print ' - plot_chisq: param_summary: {0} created'.format(file_out)
      plt.close()

  print ' - plot_chisq: param_summary: Created {0} by {1} panels from {2} fields'.format(n_row, n_col, n_par)

  #fig.tight_layout()

  return None

#=================================================================================================================
def append_chisq_contour_panel(list_dic_chisq, axis, xaxis='M', yaxis='ov', by='reduced_chisq_seism_freq',
                               contour=True, factor=None, n_chisq=None, file_out=None):
  """
  To plot where the minima in chisq occurs for two parameters that are positioned on the xaxis and yaxis.
  The value of the chisq is shown by color highlighting
  @param list_dic_chisq: list of dictionaries with the mode frequency and/or period spacing information, in
         addition to chisq information in its items(). We check for the presence of the required chisq key in
         advance. This list can be retrieved from e.g. mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type list_dic_chisq: list of dictionaries
  @param axis: a matplotlib.axis object created by a third-party, and fullly updated here.
  @type axis:
  @param xaxis/yaxis: specify which parameter goes on the xaxis/yaxis. They have to be the part of the filename.
        Therefore, the only allowed names are 'M', 'eta', 'ov', 'sc', 'Z', 'Yc' and 'Xc'.
  @type xaxis/yaxis: string
  @param by: specify whcih chisq criteria to be used. The allowd names are:
        - reduced_chisq_evol/chisq_evol: returned by mesa_gyre.chisq.evol()
        - chisq_one_fit: returned by mesa_gyre.chisq.fit_one_freq()
        - reduced_chisq_seism_freq/chisq_seism_freq: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P/chisq_seism_P; returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_dP/chisq_seism/dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
        - reduced_chisq_seism_P_and_dP/chisq_seism_P_and_dP: returned by mesa_gyre.chisq.do_chisq_freq_P_dP()
  @type by: string
  @param contour: if set, the output is a contour plot, otherwise, it is just a "scatter" plot of xaxis versus
         chisquare color-coded with chisquare values.
  @type contour: boolean; default=True
  @param factor: if not None, only those models with chisq between 1 and factor times the minimum chisquare are plotted
  @type factor: float
  @param n_chisq: if set, consider the first n_chisq points for plotting. this is useful to avoid smearing out any
        feature from mixing with too many chisquare valuees in contour plots. default=None.
        WARNING: Use with a lot of experiments to end up with an optimal value.
  @type n_chisq: integer
  @param file_out: full path to the output plot file. if provided, then this function can be used stand-alone
  @type file_out: string
  @return: updatd axis with being plotted
  @rtype: matplotlib axis
  """
  n_dic = len(list_dic_chisq)
  if n_dic == 0:
    logging.error('plot_chisq.append_chisq_contour_panel: Input list is empty')
    raise SystemExit

  a_dic      = list_dic_chisq[0]
  a_filename = a_dic['filename']
  dic_params = param_tools.get_param_from_single_gyre_filename(a_filename)
  if not dic_params.has_key(xaxis):
    logging.error('plot_chisq: append_chisq_contour_panel: {0} not in filename'.format(xaxis))
    raise SystemExit
  if not dic_params.has_key(yaxis):
    logging.error('plot_chisq: append_chisq_contour_panel: {0} not in filename'.format(yaxis))
    raise SystemExit
  if not a_dic.has_key(by):
    logging.error('plot_chisq: append_chisq_contour_panel: {0} not in available dictionary keys!'.format(by))
    raise SystemExit

  #--------------------------------
  # Only use the first n_chisq values
  #--------------------------------
  if n_chisq is not None:
    if n_chisq > n_dic:
      logging.warning('plot_chisq: append_chisq_contour_panel: n_chisq exceeded n_dic. Ignore it')
      n_chisq = n_dic
    list_dic_chisq = [dic for dic in list_dic_chisq[0 : n_chisq+1]]

  list_files = []
  list_chisq = []
  list_xaxis = []
  list_yaxis = []
  for i_dic, dic in enumerate(list_dic_chisq):
    the_chisq = dic[by]
    filename   = dic['filename']
    dic_params = param_tools.get_param_from_single_gyre_filename(filename)
    list_chisq.append(the_chisq)
    list_files.append(filename)
    list_xaxis.append(dic_params[xaxis])
    list_yaxis.append(dic_params[yaxis])

  #--------------------------------
  # convert the lists to np.ndarray,
  # sort them, extract unique values along x- and y-axis,
  #--------------------------------
  list_chisq = np.array(list_chisq)
  list_xaxis = np.array(list_xaxis)
  list_yaxis = np.array(list_yaxis)
  ind_sort   = np.argsort(list_chisq)
  list_chisq = list_chisq[ind_sort]
  list_xaxis = list_xaxis[ind_sort]
  list_yaxis = list_yaxis[ind_sort]
  if xaxis == 'sc': list_xaxis = np.log10(list_xaxis)
  if yaxis == 'sc': list_yaxis = np.log10(list_yaxis)

  #--------------------------------
  # Chisquare ranges
  #--------------------------------
  n_points   = len(list_chisq)
  min_chisq  = np.min(list_chisq)
  max_chisq  = np.max(list_chisq)
  #list_chisq[np.isnan(list_chisq)] = max_chisq
  list_chisq[list_chisq == 1e99] = max_chisq

  #--------------------------------
  # if factor is set, clip off some models
  #--------------------------------
  if factor is not None:
    lower_chisq = min_chisq
    upper_chisq = factor * min_chisq
    ind_factor  = np.where((lower_chisq <= list_chisq) & (upper_chisq >= list_chisq))[0]
    list_chisq  = list_chisq[ind_factor]
    list_xaxis  = list_xaxis[ind_factor]
    list_yaxis  = list_yaxis[ind_factor]
    n_points    = len(list_chisq)
    min_chisq   = np.min(list_chisq)
    max_chisq   = np.max(list_chisq)

  #--------------------------------
  # Create x and y meshgrid
  #--------------------------------
  xvals = np.sort(np.array( list( Counter(list_xaxis) ) ))
  yvals = np.sort(np.array( list( Counter(list_yaxis) ) ))
  n_xaxis    = len(xvals)
  n_yaxis    = len(yvals)
  x_grid = np.array([xvals] * n_yaxis).transpose()
  y_grid = np.array([yvals] * n_xaxis)
  grid_shape = x_grid.shape

  #--------------------------------
  # Create a 2-D surface of chisq
  # setup meshgrid
  #--------------------------------
  surf_chiq  = np.zeros((n_xaxis, n_yaxis))
  i_count    = 0
  for i_pt in range(n_points):
    the_chisq    = list_chisq[i_pt]
    true_x_value = list_xaxis[i_pt]
    true_y_value = list_yaxis[i_pt]
    ind_x        = np.where((true_x_value == xvals))[0]
    ind_y        = np.where((true_y_value == yvals))[0]
    if contour and surf_chiq[ind_x, ind_y] != 0:
      min_val    = min([surf_chiq[ind_x, ind_y], the_chisq])
      surf_chiq[ind_x, ind_y] = min_val
      print 'Using chisq={0} instead of {1} at ind_x={2}, ind_y={3}'.format(min_val, max([surf_chiq[ind_x, ind_y], the_chisq]),
                                                                            ind_x, ind_y)
    else:
      surf_chiq[ind_x, ind_y] = the_chisq
    if true_x_value != xvals[ind_x] or true_y_value != yvals[ind_y]:
      logging.error('plot_chisq: append_chisq_contour_panel: Issue Indexing')
      raise SystemExit
    if the_chisq == min_chisq:
      x_val_for_min_chisq = true_x_value
      y_val_for_min_chisq = true_y_value

  #--------------------------------
  # setup the color range and levels
  #--------------------------------
  import matplotlib as mpl
  from matplotlib.colors import BoundaryNorm
  from matplotlib.ticker import MaxNLocator

  levels = MaxNLocator(nbins=100).tick_values(surf_chiq.min(), surf_chiq.max())

  cmap = plt.get_cmap('brg')
  norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

  #--------------------------------
  # Do the plotting
  #--------------------------------
  if contour:
    logging.info('plot_chisq: append_chisq_contour_panel: Create a Contour Plot')
    #im = axis.imshow(surf_chiq, aspect='auto', interpolation='bicubic', vmin=list_chisq.min(), vmax=list_chisq.max(),
               #origin='lower', norm=norm, alpha=0.7,
               #extent=[list_xaxis.min(), list_xaxis.max(), list_yaxis.min(), list_yaxis.max()], zorder=1)
    #plt.colorbar(im, ax=axis)
    symsize = 1000 * min(list_chisq)/list_chisq
    axis.scatter(list_xaxis, list_yaxis, c=list_chisq, s=symsize, alpha=0.50, marker='o', zorder=2)
    #axis.scatter([x_val_for_min_chisq], [y_val_for_min_chisq], s=49, c='red', marker='o', zorder=2)

  if not contour:
    logging.info('plot_chisq: append_chisq_contour_panel: Create a Scatter Plot')
    axis.scatter(list_xaxis, list_chisq, c=list_chisq, s=25, alpha=0.5)

  #--------------------------------
  # Ranges, Annotations
  #--------------------------------
  xname = xaxis
  yname = yaxis
  if xaxis == 'M':   xname = r'Mass [M$_\odot$]'
  if xaxis == 'eta': xname = r'$\eta$'
  if xaxis == 'ov':  xname = r'$f_{\rm ov}$'
  if xaxis == 'sc':  xname = r'$\log \alpha_{\rm sc}$'
  if xaxis == 'Z':   xname = r'Z'
  if xaxis == 'Yc':  xname = r'Y$_{\rm c}$'
  if xaxis == 'Xc':  xname = r'X$_{\rm c}$'
  if yaxis == 'M':   yname = r'Mass [M$_\odot$]'
  if yaxis == 'eta': yname = r'$\eta$'
  if yaxis == 'ov':  yname = r'$f_{\rm ov}$'
  if yaxis == 'sc':  yname = r'$\log \alpha_{\rm sc}$'
  if yaxis == 'Z':   yname = r'Z'
  if yaxis == 'Yc':  yname = r'Y$_{\rm c}$'
  if yaxis == 'Xc':  yname = r'X$_{\rm c}$'

  if contour:
    axis.set_xlabel(xname, fontsize='x-small')
    axis.set_xlim(min(xvals), max(xvals))
    if len(xvals) <=7: axis.set_xticks(tuple(xvals))
    axis.set_ylabel(yname, fontsize='x-small')
    axis.set_ylim(min(yvals), max(yvals))
    if len(yvals) <=5: axis.set_yticks(tuple(yvals))
  else:
    axis.set_xlabel(xname, fontsize='x-small')
    axis.set_xlim(min(xvals)*0.9, max(xvals)*1.1)
    if len(xvals) <=7: axis.set_xticks(tuple(xvals))
    axis.set_ylabel(r'$\chi^2_{\rm red}$', fontsize='x-small')
    axis.set_ylim(min(list_chisq)*0.8, min(list_chisq)*factor)

  #--------------------------------
  # Write the plot to a file
  #--------------------------------
  #if file_out:
    #plt.savefig(file_out, dpi=200)
    #print ' - plot_chisq: append_chisq_contour_panel: %s created' % (file_out, )
    #plt.close()

  logging.info('plot_chisq: append_chisq_contour_panel: Plot {0} vs. {1}'.format(xaxis, yaxis))

  return axis

#=================================================================================================================
def comp_obs_th_freq(dic_obs, dic_best, file_out=None):
  """
  Compare the whole observed frequencies with those of the best-fit model after chisq analysis
  @param dic_obs: the measured properties of the target star is passed through this item.
         list of dictionaries returned by any of objects in mesa_gyre.stars, containing a dictionary
         per each mode in a star.
  @type dic_obs: dictionary
  @param dic_best: dictionary of the best GYRE model that minimized the chi-square. It is typically returned from
         mesa_gyre.read.read_multiple_gyre_files()
  @type dic_best: dictionary
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  if file_out is None: return

  #--------------------------------
  # Prepare the figure
  #--------------------------------
  fig, (top, bottom) = plt.subplots(2, 1, figsize=(8,4), dpi=200)
  plt.subplots_adjust(left=0.05, right=0.99, bottom=0.115, top=0.875, hspace=0.04, wspace=0.04)

  #--------------------------------
  # Prepare data
  #--------------------------------
  dic_conv      = commons.conversions()
  Hz_to_cd      = dic_conv['Hz_to_cd']

  list_dic_freq = dic_obs['list_dic_freq']
  n_modes       = len(list_dic_freq)
  obs_freq      = np.array([ dic['freq'] for dic in list_dic_freq ])
  obs_freq_err  = np.array([ dic['freq_err'] for dic in list_dic_freq ])
  obs_el        = []
  obs_el_err    = []
  for dic in list_dic_freq:
    if dic.has_key('l'): 
      obs_el.append(dic['l'])
      obs_el_err.append(0)
    if not dic.has_key('l'): 
      obs_el.append(None)
      obs_el_err.append(2)
  obs_el_err    = np.array(obs_el_err)

  obs_P         = 1.0 / obs_freq
  obs_P_err     = obs_freq_err / obs_freq**2

  th_freq       = np.real(dic_best['freq']) * Hz_to_cd
  th_el         = dic_best['l']
  th_n_pg       = dic_best['n_pg']
  list_match_freq = []
  list_match_el   = []
  list_match_n_pg = []

  #--------------------------------
  # Only choose the closest frequency match
  #--------------------------------
  closest_match = False

  if closest_match:
    for i_dic, dic in enumerate(list_dic_freq):
      f         = dic['freq'] 
      if dic.has_key('l'):
        l         = dic['l']
        ind_el    = np.where(th_el == l)[0]
        some_th_f = th_freq[ind_el]
        some_th_n = th_n_pg[ind_el]
        ind_f     = np.argmin(np.abs(f - some_th_f))
        match_f   = some_th_f[ind_f]
        match_n   = some_th_n[ind_f]
        list_match_el.append(l)
        list_match_freq.append(match_f)
        list_match_n_pg.append(match_n)

      else:
        ind_f     = np.argmin(np.abs(f - th_freq))
        match_f   = th_freq[ind_f]
        list_match_el.append(th_el[ind_f])
        list_match_freq.append(match_f)
        list_match_n_pg.append(th_n_pg[ind_f])

  list_match_freq = np.array(list_match_freq)
  list_match_el   = np.array(list_match_el)
  list_match_n_pg = np.array(list_match_n_pg)
  list_match_P    = 1.0 / list_match_freq

  #--------------------------------
  # Or show all modes within the observed range
  #--------------------------------
  if not closest_match:
    which_el      = 1
    ind_el        = np.where(th_el == which_el)[0]
    th_freq       = th_freq[ind_el]
    min_freq      = obs_freq[0] - 10 * obs_freq_err[0]
    max_freq      = obs_freq[-1] + 500 * obs_freq_err[-1]
    ind_choose    = np.where((th_freq >= min_freq) & (th_freq <= max_freq))[0]

    list_match_freq = th_freq[ind_choose]
    list_match_el   = which_el * np.ones(len(ind_choose))
    list_match_n_pg = th_n_pg[ind_choose]
    list_match_P    = 1.0 / list_match_freq

  #--------------------------------
  # Do the plotting
  #--------------------------------
  top.errorbar(obs_freq, np.ones(n_modes), xerr=obs_freq_err, yerr=obs_el_err, color='white',
              ecolor='grey', elinewidth=3, zorder=1)
  top.scatter(obs_freq, np.ones(n_modes), s=25, facecolor='red', edgecolor='black', zorder=2, label='HD 50230')
  top.scatter(list_match_freq, list_match_el+0.15, s=25, facecolor='blue', marker='s', zorder=3, label='Model')

  bottom.errorbar(obs_P, np.ones(n_modes), xerr=obs_P_err, yerr=obs_el_err, color='white', 
              ecolor='grey', elinewidth=3, zorder=1)
  bottom.scatter(obs_P, np.ones(n_modes), s=25, facecolor='red', edgecolor='black', zorder=2, label='HD 50230')
  bottom.scatter(list_match_P, list_match_el+0.15, s=25, facecolor='blue', marker='s', zorder=3, label='Model')


  #--------------------------------
  # Annotations, Ranges and Legend
  #--------------------------------
  top.set_xlim(min(obs_freq)*.80, max(obs_freq)*1.25)
  top.set_xlabel(r'Frequency [d$^{-1}$]')
  top.xaxis.set_ticks_position('top')
  top.xaxis.set_label_position('top')
  top.set_ylim(-.4, 2.4)
  top.set_yticks((0,1,2))
  top.set_ylabel(r'Degree $\ell$')
  top_leg = top.legend(loc=1, fontsize='small', scatterpoints=1)

  bottom.set_xlim(min(obs_P)*0.8, max(obs_P)*1.25)
  bottom.set_xlabel(r'Period [d]')
  bottom.set_ylim(-0.4, 2.4)
  bottom.set_yticks((0,1,2))
  bottom.set_ylabel(r'Degree $\ell$')
  bot_leg = bottom.legend(loc=1, fontsize='small', scatterpoints=1)

  filename = dic_best['filename']
  filename = filename[filename.rfind('/')+1 : filename.rfind('.')]
  filename = filename.split('-')
  filename = [filename[i] for i in [2, 4, 6, 8, 9]]
  star_name= dic_obs['name'][0]
  txt      = ';  '.join(filename)
  # bottom.annotate(star_name+': '+txt, xy=(0.65, 0.10), xycoords='axes fraction',
  #                 ha='center', va='center', fontsize='medium')

  for k, f in enumerate(obs_freq):
    if f < 2.0: continue
    txt    = '{0}'.format(list_match_n_pg[k])
    top.annotate(txt, xy=(list_match_freq[k], list_match_el[k]-0.15), rotation=45, 
                 color='blue', ha='center', va='center')

  for k, P in enumerate(obs_P):
    if P < 0.6: continue
    txt    = '{0}'.format(list_match_n_pg[k])
    bottom.annotate(txt, xy=(1./list_match_freq[k], list_match_el[k]+0.40), rotation=45, 
                    color='blue', ha='center', va='center')

  #--------------------------------
  # Store the plot
  #--------------------------------
  # plt.tight_layout()
  plt.savefig(file_out, dpi=200)
  print ' - %s created' % (file_out, )
  plt.close()

  return None

#=================================================================================================================
def results_for_M_eta_ov_Z_Xc(list_dic, which_chisq='', file_out=None):
  """
  @param file_out: full path to the output plot
  @tyep file_out: string
  """
  a_dic = list_dic[0]
  if which_chisq not in a_dic.keys():
    logging.error('results_by_param:')

  return None

#=================================================================================================================
def relative_freq_deviation(dic_obs, dic_best, dic_next=None, dic_last=None, el=1, sigma=3, 
			    xaxis_from=0.5, xaxis_to=2.5, yaxis_from=-1, yaxis_to=+1, file_out=None):
  """
  To plot the relative frequency deviation between observation set and the best model, here,
  we plot the following quantity
             (f_th - f_obs) / f_obs
  which shows how ready we are to start Inversion Technique for a given star.
  @param dic_obs: dictionary of observed quantities of the star including the "list_dic_freq" key
      to fetch observed frequencies and their respective error estimates
  @type dic_obs: dictionary
  @param dic_best: dictionary with GYRE frequency output information, representing the best seismic
      model of the star, it can or cannot have additional keys associated with chisq. 
  @type dic_best: dictionary
  @param dic_next: optional; a dictionary identical to dic_best, but for the second best model.
  @type dic_next: dictionary
  @param dic_last: optimal; a dictionary identical to dic_best, but for the third best model.
  @type dic_last: dictionary
  @param el: harmonic degree el to filter the GYRE frequency list; default=1
  @type el: integer
  @return: None
  @rtype: None
  """
  if 'list_dic_freq' not in dic_obs.keys():
    logging.error('plot_chisq: relative_freq_deviation: "list_dic_freq" not available')
    raise SystemExit
  if file_out is None:
    print 'Warning: plot_chisq: relative_freq_deviation: You should specify the plot name'
    return None

  Hz_to_cd = commons.conversions()['Hz_to_cd']
  sec_to_d = commons.conversions()['sec_to_d']

  list_obs_freq = dic_obs['list_dic_freq']
  list_obs_freq = stars.check_obs_vs_model_freq_unit('Hz', list_obs_freq)
  dic_obs['list_dic_freq'] = list_obs_freq
  # dic_obs       = stars.list_freq_to_list_dP(dic_obs, freq_unit='Hz')

  list_obs_freq = dic_obs['list_dic_freq'] #[::-1]
  obs_freq      = np.array([ dic['freq'] for dic in list_obs_freq ])
  obs_freq_err  = np.array([ dic['freq_err'] for dic in list_obs_freq ])
  # obs_P         = np.array([ dic['P'] for dic in list_obs_freq ])
  obs_P         = 1.0 / obs_freq #[::-1]
  #obs_P_err     = np.array([ dic['P_err'] for dic in list_obs_freq ])
  #obs_dP        = np.array([ dic['dP'] for dic in list_obs_freq ])
  #obs_dP_err    = np.array([ dic['dP_err'] for dic in list_obs_freq ])
  n_obs_freq    = len(obs_freq)
  min_freq      = obs_freq[0]  - sigma * obs_freq_err[0]  # 3-sigma?
  max_freq      = obs_freq[-1] + sigma * obs_freq_err[-1] # 3-sigma?

  degree  = dic_best['l']
  best_f  = np.real(dic_best['freq'])
  ind     = np.where((min_freq <= best_f) & (max_freq >= best_f))[0]

  if not any(ind):
    logging.error('plot_chisq: relative_freq_deviation: Mismatch between el, and frequency range!')
    print 'skipping'
    return None

  degree   = degree[ind]
  n_th     = len(degree)
  best_f   = best_f[ind]

  if len(best_f) != len(obs_freq):
    print '   Warning :::'
    print '   plot_chisq: relative_freq_deviation: Warning: len(obs_freq) != len(theo_frerq): {0} vs {1}'.format(len(obs_freq), len(best_f))
    new_best_f = np.zeros(len(obs_freq))
    for i, f in enumerate(obs_freq):
      k = np.argmin(np.abs(best_f - f))
      new_best_f[i] = best_f[k]
    best_f = new_best_f

  delta_f  = best_f - obs_freq
  rel_f    = delta_f / obs_freq * 100
  obs_P_d  = obs_P * sec_to_d
  
  fig, ((al, ar), (bl, br), (cl, cr)) = plt.subplots(3, 2, figsize=(6,4), sharex='col', sharey='row')
  bottom   = 0.12
  l_start  = 0.13
  r_start  = 0.80
  l_width  = 0.66
  l_height = 0.285 
  r_width  = 0.19
  r_height = 0.285
  al.set_position([l_start, bottom+2*(l_height+0.01), l_width, l_height])
  ar.set_position([r_start, bottom+2*(r_height+0.01), r_width, r_height])
  bl.set_position([l_start, bottom+l_height+0.01, l_width, l_height])
  br.set_position([r_start, bottom+r_height+0.01, r_width, r_height])
  cl.set_position([l_start, bottom, l_width, l_height])
  cr.set_position([r_start, bottom, r_width, r_height])

  #al.scatter(obs_P_d, rel_f, marker='o', s=50, color='blue', zorder=2, label=r'Model $\#$4')
  
  if xaxis_from is None: xaxis_from = np.min(obs_P_d)
  if xaxis_to   is None: xaxis_to   = np.max(obs_P_d)
  if yaxis_from is None: yaxis_from = np.min(rel_f) * 1.10
  if yaxis_to   is None: yaxis_to   = np.max(rel_f) * 1.10
  al.set_xlim(xaxis_from, xaxis_to)
  al.set_ylim(yaxis_from, yaxis_to)
  bl.set_ylim(yaxis_from, yaxis_to)
  cl.set_ylim(yaxis_from, yaxis_to)
  #al.set_yticklabels(['-0.4', '-0.2', '0.0', '0.2', '0.4'])
  bl.set_ylabel(r'$\delta f_i / f^{\rm (obs)}_i \,[\%]$')
  cl.set_xlabel(r'Period [day]')
  
  #ar.hist(rel_f, 7, range=(yaxis_from, yaxis_to), orientation='horizontal', 
	     #histtype='step', stacked=False, fill=True, fc='blue', ec='white', zorder=2, alpha=0.5)
  ar.set_xlim(0, 11)
  ar.set_ylim(yaxis_from, yaxis_to)
  ar.set_xlabel(r'Num. of modes')
  #ar.set_yticklabels(())

  if isinstance(dic_best, dict):
    degree  = dic_best['l']
    best_f  = np.real(dic_best['freq'])
    ind     = np.where((min_freq <= best_f) & (max_freq >= best_f))[0]
    degree   = degree[ind]
    n_th     = len(degree)
    best_f   = best_f[ind]
    if len(best_f) != len(obs_freq):
      new_best_f = np.zeros(len(obs_freq))
      for i, f in enumerate(obs_freq):
        k = np.argmin(np.abs(best_f - f))
        new_best_f[i] = best_f[k]
      best_f = new_best_f
    delta_f  = (best_f - obs_freq) 
    rel_f    = delta_f / obs_freq * 100
    obs_P_d  = obs_P * sec_to_d

    al.axhline(y=0, linestyle='dotted', lw=1, color='black', zorder=0)
    al.scatter(obs_P_d, rel_f, marker='o', s=50, color='red', label=r'Model $\#$4')
    ar.hist(rel_f, 7, range=(yaxis_from, yaxis_to), orientation='horizontal', 
	      histtype='step', stacked=False, fill=True, fc='red', ec='black', zorder=1)
    al.annotate(r'Model 4', xy=(0.05, 0.10), xycoords='axes fraction', fontsize=10)
  
  if isinstance(dic_next, dict):
    degree  = dic_next['l']
    best_f  = np.real(dic_next['freq'])
    ind     = np.where((min_freq <= best_f) & (max_freq >= best_f))[0]
    degree   = degree[ind]
    n_th     = len(degree)
    best_f   = best_f[ind]
    if len(best_f) != len(obs_freq):
      new_best_f = np.zeros(len(obs_freq))
      for i, f in enumerate(obs_freq):
        k = np.argmin(np.abs(best_f - f))
        new_best_f[i] = best_f[k]
      best_f = new_best_f
    delta_f  = (best_f - obs_freq) 
    rel_f    = delta_f / obs_freq * 100
    obs_P_d  = obs_P * sec_to_d

    bl.axhline(y=0, linestyle='dotted', lw=1, color='black', zorder=0)
    bl.scatter(obs_P_d, rel_f, marker='s', s=50, color='blue', label=r'Model $\#$8')
    br.hist(rel_f, 7, range=(yaxis_from, yaxis_to), orientation='horizontal', 
	      histtype='step', stacked=False, fill=True, fc='blue', ec='black')
    bl.annotate(r'Model 8', xy=(0.05, 0.10), xycoords='axes fraction', fontsize=10)

  if isinstance(dic_last, dict):
    degree  = dic_last['l']
    best_f  = np.real(dic_last['freq'])
    ind     = np.where((min_freq <= best_f) & (max_freq >= best_f))[0]
    degree   = degree[ind]
    n_th     = len(degree)
    best_f   = best_f[ind]
    if len(best_f) != len(obs_freq):
      new_best_f = np.zeros(len(obs_freq))
      for i, f in enumerate(obs_freq):
        k = np.argmin(np.abs(best_f - f))
        new_best_f[i] = best_f[k]
      best_f = new_best_f
    delta_f  = (best_f - obs_freq) 
    rel_f    = delta_f / obs_freq * 100
    obs_P_d  = obs_P * sec_to_d

    cl.axhline(y=0, linestyle='dotted', lw=1, color='black', zorder=0)
    cl.scatter(obs_P_d, rel_f, marker='p', s=50, color='purple')
    cr.hist(rel_f, 7, range=(yaxis_from, yaxis_to), orientation='horizontal', 
	      histtype='step', stacked=False, fill=True, fc='purple', ec='black')
    cl.annotate(r'Model 11', xy=(0.05, 0.10), xycoords='axes fraction', fontsize=10)

  
  plt.savefig(file_out)
  print ' - plot_chisq: relative_freq_deviation: saved {0}'.format(file_out)
  plt.close()
  
  return None
  
#=================================================================================================================
def relative_freq_deviation_vs_amplitude(dic_obs, dic_best, sigma=3, file_out=None):
  """
  To verify if there is any relation between the relative frequency deviation, i.e. delta f/f, and the 
  mode amplitude for the best model.
  @param dic_obs: dictionary of observed quantities of the star including the "list_dic_freq" key
      to fetch observed frequencies and their respective error estimates
  @type dic_obs: dictionary
  @param dic_best: dictionary with GYRE frequency output information, representing the best seismic
      model of the star, it can or cannot have additional keys associated with chisq. 
  @type dic_best: dictionary
  @return: None 
  @rtype: None
  """
  if 'list_dic_freq' not in dic_obs.keys():
    logging.error('plot_chisq: relative_freq_deviation_vs_amplitude: "list_dic_freq" not available')
    raise SystemExit
  if file_out is None:
    print 'Warning: plot_chisq: relative_freq_deviation_vs_amplitude: You should specify the plot name'
    return None

  Hz_to_cd = commons.conversions()['Hz_to_cd']
  sec_to_d = commons.conversions()['sec_to_d']

  list_obs_freq = dic_obs['list_dic_freq']
  n_obs_freq = len(list_obs_freq)
  list_obs_freq = stars.check_obs_vs_model_freq_unit('Hz', list_obs_freq)
  dic_obs['list_dic_freq'] = list_obs_freq
  # dic_obs       = stars.list_freq_to_list_dP(dic_obs, freq_unit='Hz')

  list_obs_freq = dic_obs['list_dic_freq'] #[::-1]
  obs_freq      = np.array([ dic['freq'] for dic in list_obs_freq ])

  obs_freq      = np.array([ dic['freq'] for dic in list_obs_freq ])
  obs_freq_err  = np.array([ dic['freq_err'] for dic in list_obs_freq ])
  obs_P         = 1.0 / obs_freq[::-1]
  n_obs_freq    = len(obs_freq)
  min_freq      = obs_freq[0]  - sigma * obs_freq_err[0]  # 3-sigma?
  max_freq      = obs_freq[-1] + sigma * obs_freq_err[-1] # 3-sigma?
  obs_amp       = np.array([ dic['amp'] for dic in list_obs_freq ]) # ppm

  degree  = dic_best['l']
  best_f  = np.real(dic_best['freq'])
  ind     = np.where((min_freq <= best_f) & (max_freq >= best_f))[0]

  if not any(ind):
    logging.error('plot_chisq: relative_freq_deviation_vs_amplitude: Mismatch between el, and frequency range!')
    print 'skipping'
    return None

  degree   = degree[ind]
  n_th     = len(degree)
  best_f   = best_f[ind]

  if len(best_f) != len(obs_freq):
    print '   Warning :::'
    print '   plot_chisq: relative_freq_deviation_vs_amplitude: Warning: len(obs_freq) != len(theo_frerq): {0} vs {1}'.format(len(obs_freq), len(best_f))
    new_best_f = np.zeros(len(obs_freq))
    for i, f in enumerate(obs_freq):
      k = np.argmin(np.abs(best_f - f))
      new_best_f[i] = best_f[k]
    best_f = new_best_f

  delta_f  = best_f - obs_freq
  rel_f    = delta_f / obs_freq * 100

  fig, ax = plt.subplots(1, figsize=(6,4))
  fig.subplots_adjust(left=0.13, right=0.97, bottom=0.12, top=0.97)

  ax.scatter(np.log10(obs_amp), np.abs(rel_f), marker='o', facecolor='white', edgecolor='black', s=200)
  ax.set_ylim(0, 0.4)
  ax.set_ylabel(r'$|\delta f_i / f^{\rm (obs)}_i| \,[\%]$')
  ax.set_xlim(1.75, 4.25)
  ax.set_xlabel('Logarithm Amplitude [ppm]')

  for i, dic in enumerate(list_obs_freq):
    integ = '{0}'.format(i+1)
    txt = r'$f_{0}$'.format(integ)
    ax.annotate(txt, xy=(np.log10(obs_amp[i]), np.abs(rel_f[i])), fontsize='x-small', color='red', va='top', ha='center')

  plt.savefig(file_out)
  print ' - plot_chisq: relative_freq_deviation_vs_amplitude: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def dP_match(dic_obs, list_dic, list_lbls, el=1, best_chisq=None, sigma=3., list_P_shift=[],
             xaxis_from=0.5, xaxis_to=2.5, dP_from=5000, dP_to=7000, df_from=-1, df_to=1, 
             black_canvas=False, file_out=None):
  """
  To compare the observed period spacing with the best-matching period spacing from the MESA-GYRE gird.
  @param dic_obs: dictionary of observed quantities of the star including the "list_dic_freq" key
      to fetch observed frequencies and their respective error estimates
  @type dic_obs: dictionary
  @param list_dic: list of dictionaries with GYRE frequency output information, it can or cannot have additional
      keys associated with chisq. Thus, the list of dictionaries can be plugged in right after
      read.read_multiple_gyre_files() and period_spacing.gen_dP(), or after chisq.do_chisq_freq_P_dP()
  @type list_dic: list of dictionaries
  @param el: harmonic degree el to filter the GYRE frequency list; default=1
  @type el: integer
  @param list_lbls: list of labels, one corresponding to one dictionary in list_dic
  @param best_chisq: if provided, the best chisquare value will be printed on the output plot
  @type best_chisq: float; default=None
  @param file_out: full path to the output plot
  @param sigma: the width of the frequency/period uncertainty considered around the first and last mode to
      accommodate and compare with GYRE frequencies. defalt=3.
  @type sigma: float
  @param black_canvas: to set the figure background and the side edges to black. Mainly useful for presentations.
      If you want to try, you have to update it to fix both panels.
  @param list_P_shift: If Chisquare calculation is performed with a non-zero P_shift, then can also plot their
      correspongind period spacing applying the same P_shift. If the list is non-empty, the size of the list must
      match the size of list_dic.
      Note: the unit for the P_shift is in "seconds" here.
  @type list_P_shift: list of floats.
  @type black_canvas: boolean
  @tyep file_out: string
  @return: None
  @rtype: None
  """
  if 'list_dic_freq' not in dic_obs.keys():
    logging.error('plot_chisq: dP_match: "list_dic_freq" not available')
    raise SystemExit
  star_name = dic_obs['name'][0]

  if list_P_shift:
      if len(list_P_shift) != len(list_dic):
          logging.error('plot_chisq: dP_match: "list_P_shift" has a different size than "list_dic".')
          raise SystemExit

  fontsize = 'x-small' # 'x-small'  # or 'medium'
  Hz_to_cd = commons.conversions()['Hz_to_cd']
  sec_to_d = commons.conversions()['sec_to_d']

  list_obs_freq = dic_obs['list_dic_freq']
  n_obs_freq = len(list_obs_freq)
  list_obs_freq = stars.check_obs_vs_model_freq_unit('Hz', list_obs_freq)
  dic_obs['list_dic_freq'] = list_obs_freq
  # update the list of frequencies to include the periods, period spacings and their associated errors
  dic_obs       = stars.list_freq_to_list_dP(dic_obs, freq_unit='Hz')
  list_obs_freq = dic_obs['list_dic_freq']
  obs_freq      = np.array([ dic['freq'] for dic in list_obs_freq ])
  obs_freq_err  = np.array([ dic['freq_err'] for dic in list_obs_freq ])
  obs_P         = np.array([ dic['P'] for dic in list_obs_freq ])
  obs_P_err     = np.array([ dic['P_err'] for dic in list_obs_freq ])
  obs_dP        = np.array([ dic['dP'] for dic in list_obs_freq ])
  obs_dP_err    = np.array([ dic['dP_err'] for dic in list_obs_freq ])
  n_obs_freq    = len(obs_freq)
  n_obs_dP      = len(obs_dP)
  min_P         = obs_P[0]  - sigma * obs_P_err[0]  # 3-sigma?
  max_P         = obs_P[-1] + sigma * obs_P_err[-1] # 3-sigma?

  str_el = str(el)
  str_P  = 'el_' + str_el + '_gm_P'
  str_dP = 'el_' + str_el + '_gm_dP'

  if file_out:
    import matplotlib.gridspec as gridspec
    fig = plt.figure(figsize=(6,4), dpi=200)
    gs = gridspec.GridSpec(100, 1)
    top = plt.subplot(gs[0:65, 0])
    bot = plt.subplot(gs[68:94, 0])
    gs.update(left=0.13, right=0.98, bottom=0.06, top=0.98, hspace=0.20, wspace=0.20)
    # plt.subplots_adjust(left=0.12, right=0.97, bottom=0.22, top=0.96)

    if black_canvas:
      colors  = itertools.cycle(['yellow', 'cyan', 'red', 'blue', 'purple', 'green', 'grey'])
    else:
      colors  = itertools.cycle(['black', 'blue', 'red', 'green', 'purple', 'yellow', 'grey', 'cyan', 'pink'])
    markers = itertools.cycle(['o', 's', '+', '^'])

    if black_canvas:
      top.errorbar(obs_P[:-1]*sec_to_d, obs_dP[:-1], xerr=obs_P_err[:-1]*sec_to_d, yerr=obs_dP_err[:-1], fmt=None,
                  ecolor='white', elinewidth=2, capsize=3, barsabove=True, capthick=2, marker='o',
                  label=star_name, zorder=1)
      print obs_P
      #top.scatter(obs_P[:-1]*sec_to_d, obs_dP[:-1], edgecolor='black', facecolor='white', s=25, label=r'Observed')
      top.plot(obs_P[:-1]*sec_to_d, obs_dP[:-1], linestyle='solid', color='white', lw=1)
    else:
      top.errorbar(obs_P[:-1]*sec_to_d, obs_dP[:-1], xerr=obs_P_err[:-1]*sec_to_d, yerr=obs_dP_err[:-1], fmt='none',
                  ecolor='grey', elinewidth=2, capsize=3, barsabove=True, capthick=2, marker='o', s = 50,
                  label=star_name, zorder=1)
      #top.scatter(obs_P[:-1]*sec_to_d, obs_dP[:-1], edgecolor='black', facecolor='grey', s=25, label=r'Observed')
      top.plot(obs_P[:-1]*sec_to_d, obs_dP[:-1], linestyle='solid', color='grey', lw=1)
    bot.axhline(0, color='black', lw=0.5, linestyle='dotted', zorder=1)

    if list_dic:
      delta_f = []
      for i_dic, dic in enumerate(list_dic):
        degree  = dic['l']
        best_P  = dic[str_P]
        best_dP = dic[str_dP]
        if list_P_shift: best_P += list_P_shift[i_dic]   # the unit for list_P_shift is currently only in sec, otherwise it is wrong
        ind     = np.where((min_P <= best_P) & (max_P >= best_P))[0]

        if not any(ind):
          logging.error('plot_chisq: dP_match: Mismatch between el, and frequency range!')
          print 'skipping'
          continue

        degree   = degree[ind]
        n_th     = len(degree)
        best_P   = best_P[ind]
        best_f   = np.array( [1./best_P[i] for i in np.arange(n_th)] )
        best_dP  = best_dP[ind[:-1]]

        if len(best_f) != len(obs_freq[0:n_th]):
          print '   plot_chisq: dP_match: Warning: len(obs_freq) != len(theo_frerq): {0} vs {1}'.format(len(obs_freq[0:n_th]), len(best_f))
          continue
        delta_f  = (best_f - obs_freq[0 : n_th]) * Hz_to_cd * 1e3 # now freq in d^{-1}; 1e3 is just to factor out by 0.001
        #delta_f  = (best_f - obs_freq[0 : n_th]) * 1e9 # now freq in nHz

        clr = colors.next()
        sym = markers.next()
        if i_dic % 2 == 0:
          fc = clr
        else:
          fc = 'white'

        top.scatter(best_P[:-1]*sec_to_d, best_dP, marker=sym, s=36, edgecolor=clr, facecolor=fc, # 'white',
                   label=list_lbls[i_dic], zorder=1)
        top.plot(best_P[:-1]*sec_to_d, best_dP, linestyle='dashed', color=clr, lw=1, zorder=1)

        bot.scatter(obs_P[0:n_th]*sec_to_d, delta_f, marker=sym, s=36, edgecolor=clr, facecolor=fc,
                    zorder=1)


        if False:
          print ' - plot_chisq: dP_match: Mean(delta f)={0:12.8f} [nHz]'.format(np.mean(np.abs(delta_f)))
          print '                         Min(Abs(delta f))={0:12.8f} [nHz]'.format(np.min(np.abs(delta_f)))
          print '                         Max(Abs(delta f))={0:12.8f} [nHz]'.format(np.max(np.abs(delta_f)))
          print '                         STD(delta f)     ={0:12.8f} [nHz]'.format(np.std(delta_f))

    yerr_cd = obs_freq_err[0:n_obs_freq] * Hz_to_cd * 1e3  # 1e3 is just to factor out by 0.001
    #yerr_cd = obs_freq_err[0:n_obs_freq] * 1e9 # now errors are in muHz
    bot.fill_between(obs_P[0:n_obs_freq]*sec_to_d, y1=-yerr_cd, y2=yerr_cd, color='grey', alpha=0.50, zorder=0)
    if False:
      print '                         Min(Q*sigma_i)_obs  ={0:12.8f} [nHz]'.format(np.min(yerr_cd))
      print '                         Max(Q*sigma_i)_obs  ={0:12.8f} [nHz]'.format(np.max(yerr_cd))
      print '                         Mean(Q*sigma_i)_obs ={0:12.8f} [nHz]'.format(np.mean(yerr_cd))
      print '                         STD(Q*sigma_i)_obs  ={0:12.8f} [nHz] \n'.format(np.std(yerr_cd))
    
    #top.set_xlim(min(np.abs(n_pg))-1, max(np.abs(n_pg))+1)
    top.set_xlim(xaxis_from, xaxis_to)
    # for tick in top.xaxis.get_major_ticks():
    #   # tick.label.set_fontsize(10)
    #   if black_canvas:     tick.label.set_color('black')
    #   if not black_canvas: tick.label.set_color('white')
    top.set_xticklabels(())
    # top.set_ylim(500, 3000)      # For KIC 7760680
    # top.set_ylim(5000, 6050)   # For KIC 10625204
    # top.set_ylim(9000, 9800)   # For HD 50230
    top.set_ylim(dP_from, dP_to)
    top.set_ylabel(r'$\Delta P$ [sec]', fontsize=fontsize)
    bot.set_xlabel(r'Period [day]', fontsize=fontsize)
    bot.set_xlim(xaxis_from, xaxis_to)
    bot.set_ylim(df_from, df_to)
    bot.set_ylabel(r'$\delta f_i \,[ 10^{-3} \,{\rm d^{-1}}]$', fontsize=fontsize)
    #bot.set_ylabel(r'$\sigma_i \,[ 10^{-3} \,{\rm d^{-1}}]$', fontsize=fontsize)
    if list_dic:
      txt = dic['filename']
      txt = txt[txt.rfind('/')+1:txt.rfind('.')].split('-')
      if 'ad' in txt: txt.pop(txt.index('ad'))
      if 'sum' in txt: txt.pop(txt.index('sum'))
      if 'MS' in txt: txt.pop(txt.index('MS'))
      txt = '  '.join(txt[:-1])
      #top.annotate(r''+txt, xy=(0.02, 0.90), xycoords='axes fraction', fontsize='x-small')
    #if best_chisq:
      #txt = r'$\chi^2_{\rm red}$=%0.2f' % (best_chisq, )
      #top.annotate(txt, xy=(0.04, 0.12), xycoords='axes fraction', fontsize='x-small')

    top.annotate(r'(a)', xy=(0.92, 0.05), xycoords='axes fraction', fontsize='medium')
    bot.annotate(r'(b)', xy=(0.92, 0.10), xycoords='axes fraction', fontsize='medium')

    leg = top.legend(loc=1, scatterpoints=1, fontsize=fontsize)
    leg.get_frame().set_alpha(0.5)

    for tick in top.xaxis.get_major_ticks(): tick.label.set_fontsize(fontsize) 
    for tick in top.yaxis.get_major_ticks(): tick.label.set_fontsize(fontsize) 
    for tick in bot.xaxis.get_major_ticks(): tick.label.set_fontsize(fontsize) 
    for tick in bot.yaxis.get_major_ticks(): tick.label.set_fontsize(fontsize) 
            
    if black_canvas:
      plot_commons.set_canvas_to_black(fig)
      plt.savefig(file_out, facecolor=fig.get_facecolor(), transparent=True)
    else:
      plt.savefig(file_out, transparent=True)
    print ' - plot_chisq: dP_match: %s created.' % (file_out, )
    plt.close()

  return None

#=================================================================================================================
def frequency_match(dic_obs, dic_best, el=1, best_chisq=None, file_out=None):
  """
  To compare the observed frequencies with the best-matching frequencies from the MESA-GYRE gird.
  @param dic_obs: dictionary of observed quantities of the star including the "list_dic_freq" key
      to fetch observed frequencies and their respective error estimates
  @type dic_obs: dictionary
  @param dic_best: dictionary of the GYRE frequencies and global model parameters for the best-matching
      model, i.e. with smallest chi-square value
  @type dic_best: dictionary
  @param el: harmonic degree el to filter the GYRE frequency list; default=1
  @type el: integer
  @param best_chisq: if provided, the best chisquare value will be printed on the output plot
  @type best_chisq: float; default=None
  @param file_out: full path to the output plot
  @tyep file_out: string
  @return: None
  @rtype: None
  """
  if 'list_dic_freq' not in dic_obs.keys():
    message = 'Error: plot_chisq: frequency_match: "list_dic_freq" not available'
    raise SystemExit, message

  dic_conv      = commons.conversions()
  Hz_to_cd      = dic_conv['Hz_to_cd']

  list_obs_freq = dic_obs['list_dic_freq']
  n_obs_freq    = len(list_obs_freq)
  list_obs_freq = stars.check_obs_vs_model_freq_unit('cd', list_obs_freq)

  all_freq     = np.array([dic['freq'] for dic in list_obs_freq])
  all_freq_err = np.array([dic['freq_err'] for dic in list_obs_freq])
  min_freq     = min(all_freq)
  max_freq     = max(all_freq)

  degree  = dic_best['l']
  freq    = np.real(dic_best['freq']) * Hz_to_cd
  n_pg    = dic_best['n_pg']
  n_modes = len(degree)
  if el is not None:
    ind   = (degree == el) & (freq >= min_freq) & (freq <= max_freq)
  else:
    ind   = (freq >= min_freq) & (freq <= max_freq)

  if not any(ind):
    message = 'Error: plot_chisq: frequency_match: Mismatch between el, and frequency range!'
    raise SystemExit, message

  degree   = degree[ind]
  min_el   = np.min(degree)
  max_el   = np.max(degree)
  freq     = freq[ind]
  n_pg     = n_pg[ind]
  n_modes  = len(degree)

  if file_out:
    fig = plt.figure(figsize=(6, 2), dpi=200)
    ax  = fig.add_subplot(111)
    plt.subplots_adjust(left=0.01, right=0.99, bottom=0.25, top=0.99)

    # ax.errorbar(all_freq, np.zeros(n_obs_freq)-1, yerr=all_freq_err, fmt=None,
    #             ecolor='black', elinewidth=2, capsize=3, barsabove=True, capthick=2, marker='s', label=r'Errors')
    point_sizes = all_freq_err * 1e5
    ax.scatter(all_freq, np.zeros(n_obs_freq)-1, marker='+', edgecolor='black', facecolor='white', s=point_sizes, 
      label=r'HD 50230')
    if el is None:
      markers = itertools.cycle(['o', 's', '*'])
      colors  = itertools.cycle(['blue', 'red', 'green'])
      for i_el in range(min_el, max_el+1):
        ind_el = np.where((degree == i_el))[0]
        n_el   = len(ind_el)
        xvals  = freq[ind_el]
        yvals  = np.zeros(n_el) + i_el

        mrk    = markers.next()
        clr    = colors.next()
        ax.scatter(xvals, yvals, s=16, color=clr, marker=mrk, label=r'$\ell={0}$'.format(i_el)) 
    else:
      ax.scatter(freq, np.ones(n_modes), s=16, color='red', marker='o', label=r'GYRE')

    #ax.set_xlim(min(np.abs(n_pg))-1, max(np.abs(n_pg))+1)
    ax.set_xlabel(r'Frequency [d$^{-1}$]')
    ax.set_yticks(())
    ax.set_xlim(min_freq*0.80, max_freq*1.20)
    txt = dic_best['filename']
    txt = txt[txt.rfind('/')+1:txt.rfind('.')].split('-')
    if 'ad' in txt: txt.pop(txt.index('ad'))
    if 'sum' in txt: txt.pop(txt.index('sum'))
    if 'MS' in txt: txt.pop(txt.index('MS'))
    txt = ' '.join(txt[:-1])
    # ax.annotate(r''+txt, xy=(0.02, 0.90), xycoords='axes fraction', fontsize='x-small')
    if best_chisq:
      txt = r'$\chi^2_{\rm red}$=%0.2f' % (best_chisq, )
      # ax.annotate(txt, xy=(0.04, 0.12), xycoords='axes fraction', fontsize='x-small')
    leg = ax.legend(loc=1, scatterpoints=1, fontsize='x-small')

    plt.savefig(file_out)
    print ' - plot_chisq: frequency_match: %s created.' % (file_out, )
    plt.close()

  return None

#=================================================================================================================


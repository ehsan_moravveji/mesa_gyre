
import sys
import numpy as np
import read
import matplotlib
import matplotlib.pylab as plt

#=================================================================================================================
def compare_eta(list_dic_hist, list_lbls=None, ref_indx=0, from_pre_ms=True, from_zams=False,
                eta_surface=False, eta_center=False, eta_core_bndry=False, v_surf=False,
                by_L=False, by_t=False, by_n=False, by_Xc=False,
                x_from=0, x_to=100, y_from=0, y_to=1.1, file_out=None):
  """
  This function compares the evolution of the surface/center/core_bndry of eta (=Omega/Omega_crit).
  There are various choices for the x-axis, and also for the y-axis
  """
  import itertools

  n_hist = len(list_dic_hist)
  if n_hist == 0:
    message = 'Error: plot_hist: compare_eta: Input list is empty.'
    raise SystemExit, message
  if not any([eta_center, eta_core_bndry, eta_surface, v_surf]):
    print 'Error: plot_hist: compare_eta: Set ONE of these to True:'
    message = '    eta_center, eta_core_bndry or eta_surface, v_center, v_core_bdry, v_surf'
    raise SystemExit, message
  if not any([by_L, by_t, by_n, by_Xc]):
    message = 'Error: plot_hist: compare_eta: Set ONE of by_L, by_t, by_n, by_Xc to True.'
    raise SystemExit, message
  if all([from_pre_ms, from_zams]) or not any([from_pre_ms, from_zams]):
    raise SystemExit, 'Error: plot_hist, compare_eta: Set either from_pre_ms OR from_zams to True.'

  required_keys = ['surf_avg_omega', 'surf_avg_omega_crit', 'surf_avg_omega_div_omega_crit',
                   'surf_avg_v_rot', 'surf_avg_v_crit', 'surf_avg_v_div_v_crit',
                   'log_L', 'log_Lnuc', 'log_LH', 'log_LHe', 'star_age', 'model_number',
                   'center_h1']
  ref_hist = list_dic_hist[ref_indx]['hist']
  dic_avail_keys = {}
  hist_names = ref_hist.dtype.names
  for name in hist_names: dic_avail_keys[name] = 0.0
  dic_keys = read.check_key_exists(dic_avail_keys, required_keys)

  # Terminate if essential keys are missing
  if not dic_keys['surf_avg_omega_div_omega_crit'] or not all([dic_keys['surf_avg_omega'],
                                                               dic_keys['surf_avg_omega_crit']]):
    raise SystemExit, 'Error: plot_hist: compare_eta: code 1'
  if v_surf:
    if not dic_keys['surf_avg_v_rot']: raise SystemExit, 'Error: plot_hist: compare_eta: code 2'

  #---------------------------
  # Define the Plot
  #---------------------------
  fig = plt.figure(figsize=(6,4.5))
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.105, right=0.99, bottom=0.10, top=0.98, hspace=0.02, wspace=0.02)

  colormap = plt.get_cmap('brg')
  norm = matplotlib.colors.Normalize(vmin=0, vmax=n_hist-1)
  color = matplotlib.cm.ScalarMappable(norm=norm, cmap=colormap)

  ls = itertools.cycle(['dotted', 'dashed', 'dashdot'])

  #---------------------------
  # Iterate over all hist data
  #---------------------------
  for i_hist, dic_hist in enumerate(list_dic_hist):
    hist = dic_hist['hist']
    header  = dic_hist['header']

    log_g = hist['log_g']
    mxg = np.argmax(log_g)
    if from_pre_ms: start_indx = 0
    if from_zams: start_indx = mxg

    # fix x-axis and its range
    if by_L:
      if not dic_keys['log_L']: raise SystemExit, 'Error: plot_hist: compare_eta: Missing log_L'
      log_L = hist['log_L']
      if not dic_keys['log_Lnuc'] and (not dic_keys['log_LH'] and not dic_keys['log_LHe']):
        raise SystemExit, 'Error: plot_hist: compare_eta: Missing log_Lnuc and/or log_LH, log_LHe.'
      if dic_keys['log_Lnuc']:
        log_Lnuc = hist['log_Lnuc']
      elif (dic_keys['log_LH'] and dic_keys['log_LHe']):
        Lnuc = np.power(10.0, hist['log_LH']) + np.power(10.0, hist['log_LHe'])
        log_Lnuc = np.log10(Lnuc)
      log_Lnuc_div_L = log_Lnuc - log_L
      xaxis = log_Lnuc_div_L[start_indx : -1]
      xtitle = r'Nuclear to Total Luminosity $\log(L_{\rm nuc}/L_{\rm total})$'

    if by_t:
      if not dic_keys['star_age']: raise SystemExit, 'Error: plot_hist: compare_eta: Missing star_age.'
      star_age = hist['star_age']
      xaxis = star_age[start_indx : -1] / 1e6
      xtitle = r'Star Age [Myr]'

    if by_n:
      if not dic_keys['model_number']: raise SystemExit, 'Error: plot_hist: compare_eta: Missing model_number.'
      model_number = hist['model_number']
      xaxis = model_number[start_indx : -1]
      xtitle = r'Model Number'

    if by_Xc:
      if not dic_keys['center_h1']: raise SystemExit, 'Error: plot_hist: compare_eta: Missing center_h1'
      center_h1 = hist['center_h1']
      xaxis = center_h1[start_indx : -1]
      xtitle = r'Center Hydrogen Mass Fraction [$X_c$]'

    if eta_surface:
      if not dic_keys['surf_avg_omega_div_omega_crit'] or (not dic_keys['surf_avg_omega'] and not dic_keys['surf_avg_omega_crit']):
        raise SystemExit, 'Error: plot_hist: compare_eta: Missing surf_avg_... omega.'
      if dic_keys['surf_avg_omega'] and dic_keys['surf_avg_omega_crit']:
        surf_avg_omega_div_omega_crit = hist['surf_avg_omega']/hist['surf_avg_omega_crit']
      if dic_keys['surf_avg_omega_div_omega_crit']:
        surf_avg_omega_div_omega_crit = hist['surf_avg_omega_div_omega_crit']
      yaxis = surf_avg_omega_div_omega_crit[start_indx : -1]
      ytitle = r'Surface $\eta_{\rm rot}=\Omega_{\rm eq}/\Omega_{\rm crit}$'

    if eta_center:
      if not dic_keys['center_omega_div_omega_crit']:
        raise SystemExit, 'Error: plot_hist: compare_eta: center_omega_div_omega_crit missing'
      yaxis = hist['center_omega_div_omega_crit'][start_indx : -1]
      ytitle = r'Center $\eta_{\rm rot}=\Omega_{\rm eq}/\Omega_{\rm crit}$'

    if eta_core_bndry:
      if not dic_keys['cz_omega_div_omega_crit']:
        raise SystemExit, 'Error: plot_hist: compare_eta: missing cz_omega_div_omega_crit'
      else:
        yaxis = hist['cz_omega_div_omega_crit'][start_indx : -1]
      ytitle = r'$\eta_{\rm rot}$ at Conv. Core Boundary'

    if v_surf:
      if not dic_keys['surf_avg_v_rot']: raise SystemExit, 'Error: plot_hist: compare_eta: surf_avg_v_crit missing.'
      yaxis = hist['surf_avg_v_rot'][start_indx : -1]
      ytitle = r'Surface Rotation Velocity $v_{\rm rot}$ [km sec$^{-1}$]'


    #---------------------------
    # Iteratively, Overplot
    #---------------------------
    if i_hist == ref_indx:
      clr = 'black'
      lstyl = 'solid'
      lbl = r'Ref: ' + list_lbls[i_hist]
    else:
      clr = color.to_rgba(i_hist)
      lstyl = ls.next()
      lbl = list_lbls[i_hist]
    ax.plot(xaxis, yaxis, linestyle=lstyl, color=clr, lw=2, label=lbl)

    # Add the ZAMS point
    if from_pre_ms: ax.scatter([xaxis[mxg]], [yaxis[mxg]], marker='o', color='red', s=25)

  ax.set_xlabel(xtitle, fontsize='medium')
  ax.set_xlim(x_from, x_to)
  ax.set_ylabel(ytitle, fontsize='medium')
  ax.set_ylim(y_from, y_to)

  #---------------------------
  # Determine the location of Legend
  #---------------------------
  leg_loc = 0    # default
  if by_L and eta_surface: leg_loc = 2
  if by_Xc and eta_surface: leg_loc = 2
  if by_Xc and v_surf: leg_loc = 1
  if list_lbls:
    leg = ax.legend(loc=leg_loc, shadow=True, fancybox=True, fontsize='small')


  if not file_out: file_out = 'Compare-evol-eta.pdf'
  plt.savefig(file_out, dpi=200)
  print '\n - compare_eta: %s saved. \n' % (file_out, )
  plt.close()

  return None

#=================================================================================================================
def diffusion_and_radiative_levitation(dic_hist, rec_abund, list_elem=[], xaxis='center_h1',
                     xaxis_from=None, xaxis_to=None, xtitle=None, file_out=None):
  """
  This function plots the evolution of the surface abundances of selected species, and compares them with
  the observed values in spectroscopic notation
  @param dic_hist: dictionary with the MESA history output data.
  @type dic_hist: dictionary
  @param rec_abund: array with the full mixture information.
  """
  try:
    import abund_lib
  except:
    print 'Error: mesa_gyre: plot_hist: diffusion_and_radiative_levitation: could not import abund_lib!'

  header = dic_hist['header']
  hist = dic_hist['hist']
  hist_names = hist.dtype.names

  if xaxis not in hist_names:
    print 'Error: plot_hist: diffusion_and_radiative_levitation: {0} not in available hist keys'.format(xaxis)
    raise SystemExit
  xvals = hist[xaxis]

  list_avail_surf_abund = []
  for name in hist_names:
    if 'surface_' in name: list_avail_surf_abund.append(name)

  n_avail_abund = len(list_avail_surf_abund)
  n_list_elem = len(list_elem)
  list_elem_plot = []
  if n_list_elem > 0:
    for i_elem, elem_name in enumerate(list_elem):
      if 'surface_' + elem_name in list_avail_surf_abund:
        list_elem_plot.append('surface_' + elem_name)
  n_plots = len(list_elem_plot)

  # Reference Observed Abundances
  obs_elems = rec_abund['elem']
  obs_elem_A = rec_abund['A']
  obs_elem_Z = rec_abund['Z']
  n_obs_elem = len(obs_elems)
  obs_elem_names = ['surface_' + obs_elems[i] + str(obs_elem_A[i]) for i in range(n_obs_elem)]

  #---------------------------
  # Create the Plot
  #---------------------------
  fig, axarr = plt.subplots(n_plots, 1, figsize=(6, n_plots*1.5))
  fig.subplots_adjust(left=0.12, right=0.95, bottom=0.06, top=0.97, hspace=0.12, wspace=.10)

  #---------------------------
  # Loop over elements
  #---------------------------
  for i_p, ax in enumerate(axarr.ravel()):
    i_elem = i_p
    elem_name = list_elem_plot[i_p]
    # if elem_name == 'surface_h1' or elem_name == 'surface_h2': continue
    # if elem_name == 'surface_he3' or elem_name == 'surface_he4': continue
    if 'surface_h1' in hist_names:
      X_h1 = hist['surface_h1']
    else:
      X_h1 = 0.0
    if 'surface_h2' in hist_names:
      X_h2 = hist['surface_h2']
    else:
      X_h2 = 0.0
    X_h = X_h1 + X_h2
    if 'surface_he3' in hist_names:
      X_he3 = hist['surface_he3']
    else:
      X_he3 = 0.0
    if 'surface_he4' in hist_names:
      X_he4 = hist['surface_he4']
    else:
      X_he4 = 0.0
    X_he = X_he3 + X_he4

    if elem_name == 'surface_h2': continue
    if elem_name == 'surface_he3': continue
    if elem_name not in obs_elem_names:
      print 'Warning: plot_hist: diffusion_and_radiative_levitation: {0} not in observed input record array!'.format(elem_name)
      add_obs = False
      continue
    else:
      ind = [i for i in range(n_obs_elem) if elem_name == obs_elem_names[i]][0]
      elem_recarr = rec_abund[ind]
      elem_A = elem_recarr['A']
      add_obs = True

    if elem_name == 'surface_h1':
      X_elem = X_h
    elif elem_name == 'surface_he4':
      X_elem = X_he
    else:
      X_elem = hist[elem_name]
    eps_elem = np.log10(X_elem / (X_h * elem_A)) + 12.0

    ax.plot(xvals, eps_elem, color='black', lw=2, zorder=2)
    len_elem = len(elem_name)
    str_part = ''.join( [''+elem_name[i] for i in range(len_elem) if not elem_name[i].isdigit()] )
    str_part = str_part[ str_part.rfind('_')+1 : ].title()
    int_part = ''.join( [''+str(elem_name[i]) for i in range(len_elem) if elem_name[i].isdigit()] )
    ytitle = r'$\epsilon(^{0}{1}{2}${3})'.format('{', int_part, '}', str_part)
    ax.set_ylabel(ytitle)
    ax.ticklabel_format(style='plain', useOffset=False)

    if add_obs:
      obs_abund = elem_recarr['abund']
      obs_abund_err = elem_recarr['abund_err']
      ax.fill_between(xvals, y1=obs_abund-obs_abund_err, y2=obs_abund+obs_abund_err,
        color='grey', alpha=0.5, zorder=1)

    if i_p == n_plots - 1:
      if xtitle is None: xtitle = xaxis.replace('_', ' ')
      ax.set_xlabel(xtitle)
    else:
      ax.set_xticklabels(())

    if xaxis_from is not None and xaxis_to is not None:
      ax.set_xlim(xaxis_from, xaxis_to)

  #---------------------------
  # Save the plot
  #---------------------------
  if file_out:
    # plt.tight_layout()
    plt.savefig(file_out)
    print ' - plot_hist: diffusion_and_radiative_levitation: saved {0}'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def check_job_complete_by_hist(list_dic_hist, ascii_out=None, file_out=None):
  """
  This function receives a list of dictionaries containing history information for a grid. Then, it checks the final 
  value for center_h1, center_he4 and Teff to make sure that the tracks did not terminate before the core he depletion.
  If an uncomplete track is detected, then it will store the file name in a list, and returns the list.
  A job is considerd failed/incomplete if any of these conditions come true:
  - center_h1 > 0.0
  - center_he4 > 0.0
  - Teff > 10,000 K
  
  @param list_dic_hist: list of dictionaries containing history information. each dictionary has at least these keys:
      - filename: gives the source ascii filename, full physical model parameters can be extracted from this
      - header:   ascii file header in the form of record array
      - hist:     numpy record array containing the full history information
  @type list_dic_hist: list of dictionaries.  
  @param ascii_out: full path to the ascii file (compatible with the VSC worker frame) containing the list of physical 
      parameters of the failed jobs per each line. (default=None)
  @type ascii_out: string
  @param file_out: default=None, if provided, then a plot with multiple panels will be stored at the full path given by file_out.
      different plot panels show the end values of Teff, Xc and Yc against one another.
  @type file_out: string
  @return: a list which contains the "filename" of those jobs that are identified as failed/incomplete
  @rtype: list of strings
  """
  
  n_hist = len(list_dic_hist)
  if n_hist == 0:
    message = 'Error: file_dir_stat: check_job_complete_by_hist: Empty input list!'
    raise SystemExit, message
  
  try: 
    sample_hist = list_dic_hist[0]['hist']
  except:
    message = 'Error: file_dir_stat: check_job_complete_by_hist: "hist" field is missing from input dictionaries.'
    raise SystemExit, message
  
  sample_hist_names = sample_hist.dtype.names
  dic_avail_keys = {}
  for key in sample_hist_names:
    dic_avail_keys[key] = 0.0
  required_keys = ['log_Teff', 'effective_T', 'center_h1', 'center_he4']
  dic_keys = read.check_key_exists(dic_avail_keys, required_keys)
  flag_Teff = dic_keys['effective_T']
  flag_logTeff = dic_keys['log_Teff']
  if not flag_logTeff and not flag_Teff:
    message = 'Error: file_dir_stat: check_job_complete_by_hist: effective_T and log_Teff are both missing from history information.'
    raise SystemExit, message
  
  print '\n - file_dir_stat: check_job_complete_by_hist: Testing %s MESA tracks ...' % (n_hist, )
  
  list_filenames = []
  list_failed = []
  n_fail = 0
  list_Teff = []
  list_Xc   = []
  list_Yc   = []
  for i_hist, dic in enumerate(list_dic_hist):
    hist = dic['hist']
    list_filenames.append(dic['filename'])
    
    if flag_Teff and not flag_logTeff: list_Teff.append(hist['effective_T'][-1])
    if flag_logTeff and not flag_Teff: list_Teff.append(np.power(10.0, hist['log_Teff'][-1]))
    if flag_Teff and flag_logTeff: list_Teff.append(hist['effective_T'][-1])
    list_Xc.append(hist['center_h1'][-1])
    list_Yc.append(hist['center_he4'][-1])
    
  # convert lists to numpy array
  list_Teff = np.asarray(list_Teff)
  list_Xc   = np.asarray(list_Xc)
  list_Yc   = np.asarray(list_Yc)
  
  bool_bad_Xc   = list_Xc > 1e-12 
  n_bool = len(bool_bad_Xc)
  bool_bad_Teff = list_Teff > 1e4
  bool_bad_Yc = (list_Xc < 1e-12) & (list_Yc > 1e-3)
  #bool_bad_Yc = []
  #for i_bool in range(n_bool):
    #if all([ (list_Xc[i_bool] < 1e-12),  (list_Yc[i_bool] > 1e-3) ]):
      #bool_bad_Yc.append(True)
    #else: bool_bad_Yc.append(False)
  
  bool_final = []
  #for i_bool in range(n_bool):
  for i_bool in range(n_hist):
    #if any([ bool_bad_Xc[i_bool], bool_bad_Yc[i_bool], bool_bad_Teff[i_bool] ]): 
    if (bool_bad_Xc[i_bool]) or ((bool_bad_Teff[i_bool]) & (bool_bad_Yc[i_bool])):
      bool_final.append(True)
      d = pt.get_param_from_single_hist_filename(list_filenames[i_bool])
      line = '%05.2f,%4.2f,%5.3f,%4.2f,%5.3f %s' % (d['m_ini'],d['eta'],d['ov'],d['sc'],d['z'],' \n') 
      list_failed.append(line)
      n_fail += 1
      
  if n_fail>0: 
    print '   We detected %s failed/incomplete jobs.' % (n_fail, )
  else:
    print '   Safe: All jobs are complete.'
  
  if ascii_out and n_fail>0:
    print '   Writing the failed information to %s' % (ascii_out, )
    w = open(ascii_out, 'w')
    line = 'm_ini,eta_ini,ov_ini,sc_ini,z_ini \n'
    w.writelines(line)
    w.writelines(list_failed)
    w.close()
    
  if file_out:   # enter plotting 
    fig = plt.figure()
    plt.subplots_adjust(left=0.09, right=0.99, bottom=0.10, top=0.99, hspace=0.15, wspace=0.15)
    ax0 = fig.add_subplot(221)
    ax1 = fig.add_subplot(222)
    ax2 = fig.add_subplot(223)
    
    ax0.scatter(list_Teff, list_Xc)
    ax0.set_xlim(max(list_Teff), min(list_Teff))
    ax0.set_xlabel(r'T$_{\rm eff}$')
    ax0.set_ylim(-0.01, 0.72)
    ax0.set_ylabel(r'X$_{\rm c}$')
    
    ax1.scatter(list_Teff, list_Yc)
    ax1.set_xlim(max(list_Teff), min(list_Teff))
    ax1.set_xlabel(r'T$_{\rm eff}$')
    ax1.set_ylim(-0.01, 1.0)
    ax1.set_ylabel(r'Y$_{\rm c}$')

    ax2.scatter(list_Xc, list_Yc)
    ax2.set_xlim(0.72, -0.01)
    ax2.set_xlabel(r'X$_{\rm c}$')
    ax2.set_ylim(-0.01, 1.0)
    ax2.set_ylabel(r'Y$_{\rm c}$')
    
    plt.savefig(file_out)
    print ' - %s saved.' % (file_out, ) 
    plt.close()
  
  return list_failed 

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================




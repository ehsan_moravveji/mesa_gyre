"""
This script installs the mesa_gyre package.

Example usage: without pip:

$:> python setup.py build
$:> sudo python setup.py install

With pip:

$:> python setup.py sdist
$:> sudo pip install dist/mesa_gyre_0.0.tar.gz

And with pip you can uninstall:

$:> sudo pip uninstall mesa_gyre

On *buntu systems, the installation directory is

/usr/local/lib/python2.7/dist-packages/mesa_gyre

"""


from numpy.distutils.core import setup, Extension
import glob
import sys
from numpy.distutils.command.build import build as _build    
        
setup(
    
    name="mesa_gyre",
    version="0.0",
    description="Tools to Use/Interpret the MESA-GYRE Evolutionary and Seismic Grid of Massive Star Models.",
    long_description="reStructuredText format",
    author="Ehsan Moravveji",
    author_email="Ehsan.Moravveji@ster.kuleuven.be",

    packages=['mesa_gyre'],
    
    entry_points = {
        "console_scripts": ["ascii2h5 = mesa_gyre.ascii2h5:main"]}

)


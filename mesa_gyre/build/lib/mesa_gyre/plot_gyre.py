
"""
This module provides various routines for interpretation of single
GYRE output file or an archive of GYRE files, and provides several
plotting scripts, and data column calculation, respectively.

created: 9 August 2013
author: Ehsan Moravveji
"""

import sys, os
import logging
import numpy as np
import itertools
import matplotlib as mpl
import pylab as plt
#import matplotlib as mpl
#mpl.use('Agg')

import gyre
import commons as cm
import param_tools as pt
import plot_commons as pcm
import plot_hrd as hrd
import period_spacing as per
import frequency_spacing as fresp
import read as rd

Msun = 1.9892e33    # g
Rsun = 6.9598e10    # cm
Lsun = 3.8418e33    # cgs
G    = 6.67428e-8   # cm^3 g^-1 s^-2

#=================================================================================================================
def freq_or_period_vs_Xc(list_dic_ref, list_dic_comp=[], which='per', by='Xc', ell=1, xaxis_from=0.72, xaxis_to=-0.01, 
                 yaxis_from=0.1, yaxis_to=5, freq_unit='Hz', file_out=None):
  """
  Plot the evolution of the modes period (ordinate) as a function of time represented by Xc (or maybe
  Teff and/or age after soon implementing them if needed). The excited modes are highlighted with larger
  marker size.
  @param list_dic_ref: The list of GYRE_AD or GYRE_NAD output files that contain mode frequency. For GYRE_NAD 
         output files where the imaginary part of the eigenfrequency is non-zero, we change the size of the 
         plotting symbols to represent excited modes. 
  @type list_dic_ref: list of dictionaries
  @param list_dic_comp: optional; similar to list_dic_ref for another track is one likes to compare the mode evolution.
         Caution: The plot can get very busy since the mode bumping for one track only will already be busy;
         So, add a comparison set if you're sure about it.
         The comparison set will be shown in grey.
  @type list_dic_comp: list of dictionaries
  @param which: specify which of the period, "per", or frequency, "freq" to plot on the y-axis.
  @type which: string
  @param ell: mode degree; ell. 
  @type ell: integer
  @param xaxis_from, xaxis_to: the range of x-axis specified by passing the "by" argument
  @type xaxis_from, xaxis_to: float
  @param yaxis_from, yaxis_to: the period range on the ordinate
  @type yaxis_from, yaxis_to: float
  @return: None
  @rtype: None
  """
  if file_out is None: return
  if which not in ('per', 'freq'):
    logging.error('plot_gyre: freq_or_period_vs_Xc: "which" can only be "per" or "freq", not: "{0}"'.format(which))
    raise SystemExit, 'Error: plot_gyre: freq_or_period_vs_Xc: "which" can only be "per" or "freq", not: "{0}"'.format(which)
  use_freq    = which == 'freq'
    
  dic_conv    = cm.conversions()
  Hz_to_cd    = dic_conv['Hz_to_cd']
  sec_to_d    = dic_conv['sec_to_d']

  ref_n       = len(list_dic_ref)
  ref_names   = list_dic_ref[0].keys()
  ref_xvals   = []
  ref_yvals   = []
  ref_freq_im = []
  for i, dic in enumerate(list_dic_ref):
    degree    = dic['l']
    ind       = np.where(degree == ell)[0]
    freq_re   = np.real(dic['freq'])[ind] * Hz_to_cd
    freq_im   = np.imag(dic['freq'])[ind] * Hz_to_cd
    per       = 1.0 / freq_re #* sec_to_d
    n_freq    = len(freq_re)
    if by == 'Xc' and by in ref_names:
      xval    = dic['Xc']
    ref_xvals.append( np.ones(n_freq) * xval )
    if use_freq:
      ref_yvals.append(freq_re)
    else:
      ref_yvals.append(per)
    ref_freq_im.append(freq_im)

  if list_dic_comp:
    comp_n       = len(list_dic_comp)
    comp_names   = list_dic_comp[0].keys()
    comp_xvals   = []
    comp_yvals   = []
    comp_freq_im = []
    for i, dic in enumerate(list_dic_comp):
      degree    = dic['l']
      ind       = np.where(degree == ell)[0]
      freq_re   = np.real(dic['freq'])[ind] * Hz_to_cd
      freq_im   = np.imag(dic['freq'])[ind] * Hz_to_cd
      per       = 1.0 / freq_re #* sec_to_d
      n_freq    = len(freq_re)
      if by == 'Xc' and by in comp_names:
        xval    = dic['Xc']
      comp_xvals.append( np.ones(n_freq) * xval )
      if use_freq:
        comp_yvals.append(freq_re)
      else:
        comp_yvals.append(per)
      comp_freq_im.append(freq_im)

  ########################################
  # Initiate the plot
  ########################################
  fig, ax = plt.subplots(1, figsize=(6,4), dpi=200)
  plt.subplots_adjust(left=0.09, right=0.985, bottom=0.11, top=0.97)

  ########################################
  # Do the plot
  ########################################
  size_if_damped  = 1
  size_if_excited = 5
  # First, put the reference track in the background with a grey color
  for i in range(ref_n):
    plot_xvals = ref_xvals[i]
    plot_yvals = ref_yvals[i]
    freq_im    = ref_freq_im[i]
    m          = len(plot_xvals)
    excited    = np.where(freq_im < 0, True, False)
    plot_sizes = np.where(freq_im >=0, size_if_damped, size_if_excited)

    ax.scatter(ref_xvals[i], ref_yvals[i], marker='.', color='black', s=1, zorder=1)
    ax.scatter(ref_xvals[i][excited], ref_yvals[i][excited], marker='o', color='black', 
               s=size_if_excited, zorder=1)

  # Second, overplot with the comparison track, if present, with black color in front of the reference
  if list_dic_comp:
    for i in range(comp_n):
      plot_xvals = comp_xvals[i]
      plot_yvals = comp_yvals[i]
      freq_im    = comp_freq_im[i]
      m          = len(plot_xvals)
      excited    = np.where(freq_im < 0, True, False)
      plot_sizes = np.where(freq_im >=0, size_if_damped, size_if_excited)

      ax.scatter(comp_xvals[i], comp_yvals[i], marker='.', color='blue', s=1, alpha=0.25, zorder=2)
      ax.scatter(comp_xvals[i][excited], comp_yvals[i][excited], marker='o', color='blue', alpha=0.25, 
                 s=size_if_excited, zorder=2)

  # Third, add the n_pg for the first models based on the reference track
  offset     = 0.025
  first_dic  = list_dic_ref[0]
  first_ell  = first_dic['l']
  first_ind  = np.where(first_ell == ell)[0]
  first_Xc   = first_dic[by]
  first_n_pg = first_dic['n_pg'][first_ind]
  first_freq = np.real(first_dic['freq'])[first_ind] * Hz_to_cd
  first_per  = 1.0 / first_freq #* sec_to_d
  n_first    = len(first_n_pg)
  first_arr  = np.zeros(n_first) + first_Xc + offset

  take_g1    = True
  for i in range(0, n_first):
    if first_n_pg[i] > 0:
      mode = 'p'
    elif first_n_pg[i] == 0:
      mode = 'f'
    else :
      mode = 'g'
      if take_g1: 
        i_g1 = i
        take_g1   = False
      if (i - i_g1) % 2 == 0: continue

    if use_freq:
      pos = (first_arr[i], first_freq[i])
    else:
      pos = (first_arr[i], first_per[i])

    txt = r'$' + mode + '_{' + '{0}'.format(np.abs(first_n_pg[i])) + r'}$'  
    ax.annotate(txt, xy=pos, xycoords='data', color='grey', 
                ha='left', va='center', fontsize=8)
    # print i, txt, pos

  ########################################
  # Finalize the plot
  ########################################
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)
  ax.set_xlabel(r'Center Hydrogen X$_{\rm c}$')
  if use_freq:
    ax.set_ylabel(r'Frequency [d$^{-1}$]')
  else:
    ax.set_ylabel(r'Period [d]')

  plt.savefig(file_out)
  print ' - plot_gyre: freq_or_period_vs_Xc: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def mode_vs_Xini(list_dic, dic_obs, by='freq', ell=1, xaxis_from=0.5, xaxis_to=2, 
                 freq_unit='Hz', file_out=None):
  """
  Create a plot of mode frequencies/periods as a function of initial hydrogen abundance, Xini.
  This shows the possible sensitivity of one's Analysis to the assumed initial composition.
  @param list_dic: list of GYRE output data, passed by e.g. read.read_multiple_gyre_files()
  @type list_dic: list of dictionaries
  @param dic_obs: full data information about a specific star. The frequencies or periods from 
         observations will be plotted as vertical solid lines.
  @type dic_obs: dict 
  @param by: specify wether to put 'freq' or 'per' of each mode on the x-axis
  @type by: string
  @param file_out: full path to the output plot 
  @type file_out: string
  @return: None
  @rtype: None
  """
  import stars
  n_dic = len(list_dic)
  if n_dic == 0:
    logging.error('plot_gyre: mode_vs_Xini: Input list is empty')
    raise SystemExit, 'Error: plot_gyre: periods_vs_Xini: Input list is empty'
  by = by.lower()
  if by != 'freq' and by != 'per':
    logging.error('plot_gyre: mode_vs_Xini: the "by" keyword accepts only "freq" OR "per". ')
    raise SystemExit, 'Error: plot_gyre: mode_vs_Xini: the "by" keyword accepts only "freq" OR "per".'

  list_dic_freq = dic_obs['list_dic_freq']
  list_dic_freq = stars.check_obs_vs_model_freq_unit(freq_unit, list_dic_freq)
  dic_obs['list_dic_freq'] = list_dic_freq

  list_obs_freq = np.array([dic['freq'] for dic in list_dic_freq])
  list_obs_per  = 1./list_obs_freq[::-1]  # increasing order  

  ########################################
  # Initiate the plot
  ########################################
  fig, ax = plt.subplots(1, figsize=(6,4), dpi=200)
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97)

  for i, freq in enumerate(list_obs_freq):
    if by == 'freq': val = freq 
    if by == 'per' : val = 1. / freq
    ax.axvline(val, ymin=0.05, ymax=0.95, linestyle='solid', color='black', lw=2, zorder=1)

  ########################################
  # Iterate over dictionaries, and plot them
  ########################################
  dic_conv = cm.conversions()
  Hz_to_cd = dic_conv['Hz_to_cd']

  for i_dic, dic in enumerate(list_dic):
    el   = dic['l']
    el_1 = np.where((el == ell))[0]
    freq = np.real(dic['freq'][el_1]) * Hz_to_cd
    n_freq = len(freq)
    if by == 'freq': xvals = freq 
    if by == 'per':  xvals = 1./freq
    filename  = dic['filename']
    dic_param = pt.get_param_from_single_gyre_filename(filename)
    Xini      = dic_param['Xini']
    yvals     = np.ones(n_freq) * Xini

    ax.scatter(xvals, yvals, marker='o', color='grey', s=50)

  ########################################
  # Finalize and store the plot as a file
  ########################################
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(0.67, 0.74)
  if by == 'freq': ax.set_xlabel(r'Observed Frequency [d$^{-1}$]')
  if by == 'per':  ax.set_xlabel(r'Observed Period [d]')
  ax.set_ylabel(r'Initial Hydrogen Abundance $X_{\rm ini}$')

  if file_out:
    plt.savefig(file_out)
    print ' - plot_gyre: mode_vs_Xini: saved {0}'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def growth_rates(list_list_dic_eig, by='freq', dic_star=None, list_lbls=[],
                 P_from=0.5, P_to=2.3, W_from=-1.05, W_to=1.05, 
                 list_dic_annot=None, file_out=None):
  """
  Growth rate, eta, is the total work divided by the integral of the absolute value of the 
  work derivative dW/dr over radius dr. In other words,
       eta = W / int_0^\infty | dW/dr | dr
  as a result of this, -1 <= eta <= +1. For a clear definition, see e.g. 
   - Dziembowski & Pamyatnykh (2008, MNRAS), their Fig. 8
  Therefore, we only work with the eigenfunctions
  """
  if file_out is None: return

  if by not in ['freq', 'per']:
    logging.error('plot_gyre: growth_rates: by={0} is not supported'.format(by))
    raise SystemExit, 'Error: plot_gyre: growth_rates: by={0} is not supported'.format(by)

  if 'W' not in list_list_dic_eig[0][0].keys():
    logging.error('plot_gyre: growth_rates: You should provide eigenfunction data')
    raise SystemExit, 'plot_gyre: growth_rates: You should provide eigenfunction data'

  dic_conv = cm.conversions()
  Hz_to_cd = dic_conv['Hz_to_cd']
  sec_to_d = dic_conv['sec_to_d']
  list_dic_freq = dic_star['list_dic_freq']
  n_nad    = len(list_list_dic_eig)

  ########################################
  # Prepare the plot
  ########################################
  fig, ax = plt.subplots(1, figsize=(6, 4), dpi=200)
  plt.subplots_adjust(left=0.13, right=0.98, bottom=0.12, top=0.97, hspace=0.2, wspace=0.25)
  symbols = itertools.cycle(['o', 's', '^'])
  colors  = itertools.cycle(['0.75', '0.50', '0.25'])
  ax.axhline(y=0, xmin=0, xmax=100, linestyle='dotted', lw=1, color='black', zorder=1)

  ########################################
  # Add observed periods
  ########################################
  for i_dic, dic in enumerate(list_dic_freq):
    obs_per = 1./dic['freq'] #* sec_to_d
    ax.axvline(x=obs_per, ymin=0.5, ymax=0.65, ls='-', lw=1, color='0.50', 
               alpha=0.9, zorder=1)

  list_min = []
  list_max = []
  el_max   = 2
  for i_list, list_dic_eig in enumerate(list_list_dic_eig):

    clr     = colors.next()

    for i_dic, dic in enumerate(list_dic_eig):

      M_star  = dic['M_star']
      R_star  = dic['R_star']
      L_star  = dic['L_star']
      factor  = G * M_star**2 / R_star

      el      = dic['l']
      n_pg    = dic['n_pg']
      freq_re = np.real(dic['freq']) * Hz_to_cd
      per     = 1./freq_re
      freq_im = np.real(dic['freq']) * Hz_to_cd
      W       = np.real(dic['W']) 
      x       = dic['x']
      dW_dx   = np.abs(dic['dW_dx'])
      n       = len(x)
      dx      = np.zeros(n)
      dx[1:]  = x[1:] - x[:n-1]
      num     = W
      denum   = np.sum(dW_dx * dx)
      eta     = num / denum

      if el == 0: sym = 'o'
      if el == 1: sym = 's'
      if el == 2: sym = '*'

      excited = eta >= 0
      damped  = eta < 0

      if excited: facecolor = clr
      if damped:  facecolor = 'white'

      if by  == 'freq': xval = freq_re
      if by  == 'per':  xval = 1. / freq_re

      ax.scatter([xval], [eta], marker=sym, facecolor=facecolor, edgecolor=clr, s=25)

      # ax.axhline(y=0, xmin=0, xmax=100, linestyle='dotted', color='black', lw=0.5, zorder=0)

  ########################################
  # Cosmetics
  ########################################
  if by == 'freq': xlabel = r'Frequency [d$^{-1}$]'
  if by == 'per':  xlabel = r'Period [d]'
  ax.set_xlabel(xlabel)
  ax.set_ylabel(r'Growth Rate $\,\eta$')
  ax.set_xlim(P_from, P_to)
  ax.set_ylim(W_from, W_to)

  if list_dic_annot: pcm.add_annotation(ax, list_dic_annot)

  ax.scatter([], [], marker='o', s=100, color='black', label=r'$\ell=0$')
  ax.scatter([], [], marker='s', s=100, color='black', label=r'$\ell=1$')
  ax.scatter([], [], marker='*', s=100, color='black', label=r'$\ell=2$')
  ax.scatter([], [], marker='s', s=100, color='0.75',  label=list_lbls[0])
  ax.scatter([], [], marker='s', s=100, color='0.50',  label=list_lbls[1])
  ax.scatter([], [], marker='s', s=100, color='0.25',  label=list_lbls[2])
  leg = ax.legend(loc=1, fontsize=10, fancybox=False, shadow=False, 
                  frameon=False, framealpha=0, scatterpoints=1)

  plt.savefig(file_out, transparent=True, dpi=300)
  print ' - plot_gyre: growth_rates: save {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def excited_or_damped_modes_vs_period(list_dic, dic_star, list_lbls, ell=1, black_canvas=False, P_from=0.5, P_to=2.3,
                      omega_im_from=-100, omega_im_to=100, leg_loc=2, list_dic_annot=None, file_out=None):
  """
  To plot the imaginary part of the eigenfrequency versus mode period. This distinguishes the damped from excited modes.
  @param list_dic: list of dictionaries with the data from output gyre files. See e.g. Fig 9 in Moravveji et al. (2014, A&A).
  @type list_dic: list of dictionaries
  @dic_star: dictionary containing information and mode frequencies about a specific star, returned e.g. by mesa_gyre.star
  @type dic_star: dictionary
  @param list_lbls: list of labels, one per each item in list_dic.
  @type list_lbls: list of strings
  @param ell: harmonic degree, ell. default=1
  @type ell: integer
  @param black_canvas: default=False, but if set True, will use opposite colors for contrast.
  @type black_canvas: boolean
  @return: None
  @rtype: None
  """

  dic_conv = cm.conversions()
  Hz_to_cd = dic_conv['Hz_to_cd']
  sec_to_d = dic_conv['sec_to_d']
  list_dic_freq = dic_star['list_dic_freq']

  the_el   = ell
  n_nad    = len(list_dic)

  if black_canvas:
    fig = plt.figure(figsize=(6, 4), dpi=200, facecolor='black')
  else:
    fig = plt.figure(figsize=(6, 4), dpi=200)
  ax  = fig.add_subplot(111)
  #fig.patch.set_facecolor('red')
  if black_canvas:
    ax.spines['bottom'].set_color('white')
    ax.spines['top'].set_color('white')
    ax.spines['left'].set_color('white')
    ax.spines['right'].set_color('white')
    ax.xaxis.label.set_color('white')
    ax.tick_params(axis='x', colors='white')
    ax.tick_params(axis='y', colors='white')
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.11, top=0.98, hspace=0.03, wspace=0.03)
  all_ls  = itertools.cycle(['solid', 'dashed', 'dashdot', 'dotted'])
  all_sym = itertools.cycle(['o', 's', '^', '*'])
  if black_canvas:
   all_clr = itertools.cycle(['cyan', 'yellow', 'purple', 'blue', 'red', 'green'])
  else:
   all_clr = itertools.cycle(['blue', 'red', 'green', 'cyan', 'yellow', 'purple'])

  # Add a box specifying the observed period ragne
  # Bbox object around which the fancy box will be drawn.
  import matplotlib.transforms as mtransforms
  from matplotlib.patches import FancyBboxPatch
  bb = mtransforms.Bbox([[1./1.01, -100], [1/.4722, 100]])

  def draw_bbox(ax, bb):
      # boxstyle=square with pad=0, i.e. bbox itself.
      p_bbox = FancyBboxPatch((bb.xmin, bb.ymin), abs(bb.width), abs(bb.height),
                              boxstyle="square,pad=0.", ec="grey", alpha=0.5, fc="none", zorder=0)
      ax.add_patch(p_bbox)

  def test1(ax):
      # a fancy box with round corners. pad=0.1
      p_fancy = FancyBboxPatch((bb.xmin, bb.ymin), abs(bb.width), abs(bb.height),
                               boxstyle="round,pad=0.", fc='grey', alpha=0.5, zorder=0)
      ax.add_patch(p_fancy)
      # draw the original bbox in black
      draw_bbox(ax, bb)

  #test1(ax)

  # Add observed periods
  for i_dic, dic in enumerate(list_dic_freq):
    obs_per = 1./dic['freq'] #* sec_to_d
    ax.axvline(x=obs_per, ls='-', lw=1, color='0.50', alpha=0.9, zorder=1)
    # print obs_per

  list_min = []
  list_max = []
  for i_dic, dic in enumerate(list_dic):
    filename= dic['filename']
    filename= filename[filename.rfind('/')+1 : filename.rfind('.')]
    pieces  = filename.split('-')
    #mass_str= pieces[0][1:]
    mass_str = pieces[0]

    el      = dic['l']
    ind_el  = el == the_el
    freq_re = np.real(dic['freq'][ind_el]) * Hz_to_cd
    freq_im = np.imag(dic['freq'][ind_el]) * Hz_to_cd * 1e6
    work    = dic['W'][ind_el]
    n_pg    = dic['n_pg'][ind_el]

    per     = 1./freq_re
    n_modes = len(freq_re)
    damped  = freq_im > 0
    excited = freq_im <= 0

    list_min.append(np.min(freq_im))
    list_max.append(np.max(freq_im))

    the_ls  = all_ls.next()
    the_clr = all_clr.next()
    the_sym = all_sym.next()
    s       = 70
    if i_dic == 2: s = 25
    label   = list_lbls[i_dic]

    if black_canvas:
      ax.scatter(per[damped], freq_im[damped], facecolor='white', s=s, marker=the_sym, zorder=2)
    else:
      ax.scatter(per[damped], freq_im[damped], facecolor='white', edgecolor=the_clr, s=s, marker=the_sym, zorder=2)
    ax.scatter(per[excited], freq_im[excited], color=the_clr, s=s, marker=the_sym, zorder=2, label=r''+label)

  if black_canvas:
    ax.fill_between([-10, 10], y1=1e6, y2=-1e6, color='black', zorder=0)
    ax.axhline(y=0, xmin=0, xmax=100, linestyle='dotted', color='white', lw=0.5, zorder=0)
  else:
    ax.axhline(y=0, xmin=0, xmax=100, linestyle='dotted', color='black', lw=0.5, zorder=0)

  ax.set_xlabel(r'Period [d]')
  ax.set_xlim(P_from, P_to)
  if black_canvas:
    ax.set_ylabel(r'$\omega_{\rm Im}/2\pi$ [$\times10^{-6}$ d$^{-1}$]', color='white')
  else:
    ax.set_ylabel(r'$\omega_{\rm Im}/2\pi$ [$\times10^{-6}$ d$^{-1}$]')

  ax.set_ylim(omega_im_from, omega_im_to)

  if list_dic_annot: pcm.add_annotation(ax, list_dic_annot)

  # plt.rcParams['legend.numpoints'] = 1
  # matplotlib.rcParams['legend.scatterpoints'] = 1
  leg = plt.legend(loc=leg_loc, fontsize='large', numpoints=1, scatterpoints=1)
  leg.get_frame().set_alpha(0.5)

  if file_out is None: return

  if black_canvas:
    plt.savefig(file_out, facecolor='0.0')
  else:
    plt.savefig(file_out)
  print ' - plot_gyre: excited_or_damped_modes_vs_period: save {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def mode_trapping(list_dic_eig, file_out=None):
  """
  Plot <x> = \frac{\int_0^1 x |\delta r|^2 dm}{\int_0^1 |\delta r|^2 dm}; See Eq (6) in Miglio et al. (2008, MNRAS).
  """
  n_eig = len(list_dic_eig)
  if n_eig == 0:
    logging.error('plot_gyre: mode_trapping: Input list is empty')
    raise SystemExit

  ########################################
  # Prepare the plot
  ########################################
  fig = plt.figure(figsize=(6,6), dpi=200)
  plt.subplots_adjust(left=0.13, right=0.98, bottom=0.10, top=0.97, hspace=0.2, wspace=0.25)
  ax1  = fig.add_subplot(211)
  ax2  = fig.add_subplot(212)

  ########################################
  # Loop over eigenfunctions and evaluate <x>
  ########################################
  list_x    = []
  list_n_pg = []
  for i_dic, dic in enumerate(list_dic_eig):
    M_star = dic['M_star']
    R_star = dic['R_star']
    L_star = dic['L_star']
    el     = dic['l']
    n_pg   = dic['n_pg']
    freq_re= np.real(dic['freq'])
    freq_im= np.imag(dic['freq'])
    xi_r   = dic['xi_r']
    xi_h   = dic['xi_h']
    delta_r= xi_r**2.0 + el * (el + 1) * xi_h**2.0
    x      = dic['x']       # relative radius, 0 <= x <= 1
    m      = dic['m']       # mass in cgs
    g      = G * m / (x * R_star)**2.0
    g[0]   = 0.0
    prop   = dic['prop_type']
    evanes = prop == 0
    A      = dic['As']
    N2     = g * A / (x * R_star)
    N2[0]  = 0.0
    N2[N2<0] = 1e-99
    N      = np.sqrt(N2)
    n      = len(x)
    dx     = np.zeros(n)
    dm     = np.zeros(n)
    for i in range(n-1): dm[i] = m[i+1] - m[i]
    for i in range(n-1): dx[i] = x[i+1] - x[i]

    num    = np.sum(x * delta_r * dm)
    denum  = np.sum(delta_r * dm)
    list_x.append(num / denum)
    list_n_pg.append(n_pg)

    P0_inv = np.sum(np.abs(N[1:])*dx[1:]/x[1:])
    Px_inv = np.zeros(n)
    Pmu_inv= np.zeros(n)
    for i in range(1, n-1): Px_inv[i] = np.sum(np.abs(N[1:i+1])/x[1:i+1] * dx[1:i+1])

  list_n_pg = np.array(list_n_pg)
  list_x    = np.array(list_x)

  ind_n_pg = [i for i in range(len(list_n_pg)) if list_n_pg[i]<=-15 and list_n_pg[i]>=-34]
  ax1.scatter(list_n_pg, list_x, s=16, marker='o', color='black', label=r'Damped')
  ax1.scatter(list_n_pg[ind_n_pg], list_x[ind_n_pg], s=16, marker='s', color='red', label=r'Observed')
  ax1.set_xlabel(r'Mode Order $n_{\rm pg}$')
  ax1.set_ylabel(r'$<x>$ [R$_\star$]')
  leg1 = ax1.legend(loc=2, fontsize='large')
  #txt = r'$<x>=\frac{\int_0^1 x|\delta r|^2 dm}{\int_0^1 x|\delta r|^2 dm}$'
  #ax1.annotate(txt, xy=(0.05, 0.80), xycoords='axes fraction', color='black', fontsize='large')

  ax2.plot(Px_inv/P0_inv, N, linestyle='solid', color='black', lw=1.5)
  ax2.set_xlabel(r'$\Pi_0/\Pi_x$')
  ax2.set_ylabel(r'N [Hz]')

  if file_out:
    plt.savefig(file_out, dpi=200)
    print ' - plot_gyre: mode_trapping: {0} saved'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def propagation_diagram(dic, xaxis_from=None, xaxis_to=None, file_out=None):
  """
  In two panels, show the eigenfunctions and the brunt vaisala and lamb frequencies
  @param dic: dictionary with the gyre_out column information, as returned e.g. from read.read_multiple_gyre_files()
  @type dic: dictinary
  @param xaxis_from/xaxis_to: the range of x-axis
  @type xaxis_from/xaxis_to: float
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  Hz_to_cd= cm.conversions()['Hz_to_cd']
  G_grav  = 6.674e-8
  m       = dic['m']
  m_rel   = m/dic['M_star']
  x_rel   = dic['x']
  x       = dic['x'] * dic['R_star']
  el      = dic['l']
  xi_r    = np.real(dic['xi_r'])
  xi_h    = np.real(dic['xi_h'])
  As      = dic['As']
  prop    = dic['prop_type']

  c_s_sq  = dic['Gamma_1'] * dic['p'] / dic['rho']
  S_l_sq  = el * (el+1) * c_s_sq / x**2.0
  S_l     = np.sqrt(S_l_sq)
  log_S_l = np.log10(S_l * Hz_to_cd)
  grav    = G_grav * m/x**2.0
  N_sq    = grav * As / x
  N_sq[N_sq<=0] = 1e-99
  N       = np.sqrt(N_sq)
  log_N   = np.log10(N * Hz_to_cd)
  log_freq= np.log10(np.real(dic['freq']) * Hz_to_cd)

  ########################################
  # Prepare the plot
  ########################################
  fig = plt.figure(figsize=(6,4), dpi=200)
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97, hspace=0.02, wspace=0.02)
  ax1  = fig.add_subplot(211)
  ax2  = fig.add_subplot(212)

  ax1.fill_between(m_rel, y1=-100, y2=100, where=(prop==0), color='blue', alpha=0.25)
  ax1.fill_between(m_rel, y1=-100, y2=100, where=(prop==1), color='green', alpha=0.25)
  ax1.plot(m_rel, xi_r, lw=1.5, color='black', linestyle='solid', label=r'$\xi_r$')
  ax1.plot(m_rel, xi_h, lw=1.5, color='black', linestyle='dashed', label=r'$\xi_h$')

  ax2.fill_between(m_rel, y1=-100, y2=100, where=(prop==0), color='blue', alpha=0.25, label=r'Conv.')
  ax2.fill_between(m_rel, y1=-100, y2=100, where=(prop==1), linestyle='dashed', color='green', alpha=0.25)
  ax2.plot(m_rel, log_N, lw=1.5, label=r'$\log N^2$')
  ax2.plot(m_rel, log_S_l, lw=1.5, linestyle='dashed', label=r'$\log S_\ell^2$')
  ax2.fill_between(m_rel, y1=log_freq*0.99, y2=log_freq*1.01, where=(prop==-1), color='purple',
                   linestyle='dashed')
  ax2.plot([], [], linestyle='dashed', lw=2, color='purple', label=r'$\log f$') # label for fill_between()

  ########################################
  # Annotations
  ########################################
  if not xaxis_from: xaxis_from = 0
  if not xaxis_to:   xaxis_to = 1
  ax1.set_xticklabels(())
  ax1.set_xlim(xaxis_from, xaxis_to)
  ax1.set_ylabel(r'Displacement')
  ax1.set_ylim(-2.5, 2.5)

  ax2.set_xlim(xaxis_from, xaxis_to)
  ax2.set_xlabel(r'Enclosed Mass [M$_\odot$]')
  ax2.set_ylabel(r'$\log N_{\rm BV}$ [d$^{-1}$]')
  ax2.set_ylim(-1,3)

  txt = r'$f$='+'{0:0.3f} '.format(np.real(dic['freq'])*Hz_to_cd) + r'd$^{-1}$'
  ax1.annotate(txt, xy=(0.04, 0.85), xycoords='axes fraction', fontsize='large')
  txt = r'$n$={0}'.format(dic['n_pg'])
  ax1.annotate(txt, xy=(0.04, 0.70), xycoords='axes fraction', fontsize='large')

  leg1 = ax1.legend(loc=3)
  leg2 = ax2.legend(loc=4)

  ########################################
  # Finalize the plot
  ########################################
  if file_out:
    plt.savefig(file_out, dpi=200)
    print ' - plot_gyre: propagation_diagram: {0} saved'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def rotation_kernels(dic, xaxis_from=None, xaxis_to=None, file_out=None):
  """
  In two panels, show the kernels constructed from eigenfunctions and the brunt vaisala and lamb frequencies
  @param dic: dictionary with the gyre_out column information, as returned e.g. from read.read_multiple_gyre_files()
  @type dic: dictinary
  @param xaxis_from/xaxis_to: the range of x-axis
  @type xaxis_from/xaxis_to: float
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  Hz_to_cd= cm.conversions()['Hz_to_cd']
  G_grav  = 6.674e-8
  m       = dic['m']
  m_rel   = m/dic['M_star']
  mass    = m / Msun
  x_rel   = dic['x']
  x       = dic['x'] * dic['R_star']
  el      = dic['l']
  #xi_r    = np.real(dic['xi_r'])
  #xi_h    = np.real(dic['xi_h'])
  Knl     = np.real(dic['K'])
  As      = dic['As']
  prop    = dic['prop_type']

  c_s_sq  = dic['Gamma_1'] * dic['p'] / dic['rho']
  S_l_sq  = el * (el+1) * c_s_sq / x**2.0
  S_l     = np.sqrt(S_l_sq)
  log_S_l = np.log10(S_l * Hz_to_cd)
  grav    = G_grav * m/x**2.0
  N_sq    = grav * As / x
  N_sq[N_sq<=0] = 1e-99
  N       = np.sqrt(N_sq)
  log_N   = np.log10(N * Hz_to_cd)
  log_freq= np.log10(np.real(dic['freq']) * Hz_to_cd)

  ########################################
  # Prepare the plot
  ########################################
  fig = plt.figure(figsize=(6,4), dpi=200)
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97, hspace=0.02, wspace=0.02)
  ax1  = fig.add_subplot(211)
  ax2  = fig.add_subplot(212)

  ax1.fill_between(mass, y1=-100, y2=100, where=(prop==0), color='blue', alpha=0.25)
  ax1.fill_between(mass, y1=-100, y2=100, where=(prop==1), color='green', alpha=0.25)
  ax1.plot(mass, Knl, lw=1.5, color='black', linestyle='solid', label=r'$K_{n,\ell}$')
  #ax1.plot(mass, xi_r, lw=1.5, color='black', linestyle='solid', label=r'$\xi_r$')
  #ax1.plot(mass, xi_h, lw=1.5, color='black', linestyle='dashed', label=r'$\xi_h$')

  ax2.fill_between(mass, y1=-100, y2=100, where=(prop==0), color='blue', alpha=0.25, label=r'Conv.')
  ax2.fill_between(mass, y1=-100, y2=100, where=(prop==1), linestyle='dashed', color='green', alpha=0.25)
  ax2.plot(mass, log_N, lw=1.5, label=r'$\log N^2$')
  ax2.plot(mass, log_S_l, lw=1.5, linestyle='dashed', label=r'$\log S_\ell^2$')
  ax2.fill_between(mass, y1=log_freq*0.99, y2=log_freq*1.01, where=(prop==-1), color='purple',
                   linestyle='dashed')
  ax2.plot([], [], linestyle='dashed', lw=2, color='purple', label=r'$\log f$') # label for fill_between()

  ########################################
  # Annotations
  ########################################
  if not xaxis_from: xaxis_from = 0
  if not xaxis_to:   xaxis_to = 1
  ax1.set_xticklabels(())
  ax1.set_xlim(xaxis_from, xaxis_to)
  ax1.set_ylabel(r'Kernels')
  ax1.set_ylim(-0.01, np.max(Knl)*1.05)

  ax2.set_xlim(xaxis_from, xaxis_to)
  ax2.set_xlabel(r'Enclosed Mass [M$_\odot$]')
  ax2.set_ylabel(r'$\log N_{\rm BV}$ [d$^{-1}$]')
  ax2.set_ylim(-1,3)

  txt = r'$f$='+'{0:0.3f} '.format(np.real(dic['freq'])*Hz_to_cd) + r'd$^{-1}$'
  ax1.annotate(txt, xy=(0.04, 0.85), xycoords='axes fraction', fontsize='large')
  txt = r'$n$={0}'.format(dic['n_pg'])
  ax1.annotate(txt, xy=(0.04, 0.70), xycoords='axes fraction', fontsize='large')

  leg1 = ax1.legend(loc=3)
  leg2 = ax2.legend(loc=4)

  ########################################
  # Finalize the plot
  ########################################
  if file_out:
    plt.savefig(file_out, dpi=200)
    print ' - plot_gyre: rotation_kernels: {0} saved'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def mark_dP_with_features(dic_dP, el=1, P_from=0.9, P_to=2.3, file_out=None):
  """
  You can highlight a given period spacing profile with the mean, max, min, dips, peaks, and distance between dips, etc.
  This is useful for journal plots
  @param dic_dP: dictionary returned by e.g. mesa_gyre.period_spacing.gen_dP(), or mesa_gyre.period_spacing.update_dP_dip()
  @type dic_dP: dictionary
  @param el: the spherical harmonic degree; default: el=1
  @type el: integer
  @param P_from, P_to: range of period [d] to plot
  @type P_from, P_to: float
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  from scipy.signal import argrelmin, argrelmax, argrelextrema
  dic_conv = cm.conversions()
  sec_to_d = dic_conv['sec_to_d']

  ########################################
  # Prepare the plot
  ########################################
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=.98)

  if el<1 or el>2:
    message = 'Error: plot_gyre: mark_dP_with_features: el=%s not supported yet' % (el, )
    raise SystemExit, message

  str_el = str(el)
  key_gm_P       = 'el_' + str_el + '_gm_P'
  key_gm_dP      = 'el_' + str_el + '_gm_dP'
  key_gm_dip_P   = 'el_' + str_el + '_gm_dip_P'
  key_gm_dip_dP  = 'el_' + str_el + '_gm_dip_dP'
  key_gm_dP_mean = 'el_' + str_el + '_gm_dP_mean'
  key_gm_peak_P  = 'el_' + str_el + '_gm_peak_P'
  key_gm_peak_dP = 'el_' + str_el + '_gm_peak_dP'
  key_gm_peak_dP_mean = 'el_' + str_el + '_gm_peak_dP_mean'

  gm_P  = dic_dP[key_gm_P] * sec_to_d
  gm_dP = dic_dP[key_gm_dP]
  gm_dip_P = dic_dP[key_gm_dip_P] * sec_to_d
  gm_dip_dP = dic_dP[key_gm_dip_dP]
  gm_dP_mean = dic_dP[key_gm_dP_mean]
  gm_peak_P  = dic_dP[key_gm_peak_P] * sec_to_d
  gm_peak_dP = dic_dP[key_gm_peak_dP]
  gm_peak_dP_mean = dic_dP[key_gm_peak_dP_mean]

  ind_P = [i for i in range(len(gm_P)) if gm_P[i] >= P_from and gm_P[i] <= P_to]
  if ind_P[-1] == len(gm_dP): del_last_ind = ind_P.pop(-1)
  gm_P  = gm_P[ind_P]
  gm_dP = gm_dP[ind_P]

  ax.plot(gm_P, gm_dP, linestyle='solid', lw=2, color='black')
  ax.scatter(gm_P, gm_dP, s=40, marker='o', facecolor='black', edgecolor='black', label=r'$\Delta P$')
  ax.scatter(gm_dip_P, gm_dip_dP, s=55,   marker='o', facecolor='white', edgecolor='red', label=r'Local Dip')
  ax.scatter(gm_peak_P, gm_peak_dP, s=55, marker='s', facecolor='white', edgecolor='blue', label=r'Local Peak')
  ax.axhline(gm_dP_mean, linestyle='solid', lw=2, color='red', label=r'Mean $\Delta P$')
  ax.axhline(gm_peak_dP_mean, linestyle='dashed', color='blue', label=r'Mean Peak')
  #leg = ax.legend(loc=1, fontsize='x-small', fancybox=True, shadow=True)
  #leg.get_frame().set_alpha(0.5)

  # add arrows to signify the distance between dips
  if len(gm_dip_P) >= 3:
    for i_arrow in range(len(gm_dip_P)-1):
      arrow_P_width = gm_dip_P[i_arrow+1] - gm_dip_P[i_arrow]
      ax.arrow(gm_dip_P[i_arrow], gm_dip_dP[i_arrow], arrow_P_width, 0, head_width=40, head_length=0.05,
               fc='purple', ec='purple', length_includes_head=True)
      ax.arrow(gm_dip_P[i_arrow+1], gm_dip_dP[i_arrow], -arrow_P_width, 0, head_width=40, head_length=0.05,
               fc='purple', ec='purple', length_includes_head=True)
  # add arrows to signify the depth of one dip
  if len(gm_dip_P) > 0:
    for i_arrow in range(len(gm_peak_P)):
      depth = gm_dip_dP[i_arrow]-gm_peak_dP_mean
      ax.arrow(gm_dip_P[i_arrow], gm_peak_dP_mean, 0, depth, head_width=0.02, head_length=100,
             fc='green', ec='green', length_includes_head=True)
      ax.arrow(gm_dip_P[i_arrow], gm_dip_dP[i_arrow], 0, -depth, head_width=0.02, head_length=100,
             fc='green', ec='green', length_includes_head=True)


    #ax.arrow(gm_dip_P[0], gm_peak_dP_mean, 0, depth, head_width=40, head_length=0.05,
             #fc='green', ec='green', length_includes_head=True)
    #ax.arrow(gm_dip_P[1], gm_dip_dP[1], 0, -depth, head_width=40, head_length=0.05,
             #fc='green', ec='green', length_includes_head=True)

  ax.set_xlabel(r'Period [d]')
  ax.set_xlim(P_from, P_to)
  ax.set_ylabel(r'Period Spacing $\Delta P$ [sec]')
  ax.set_ylim(min(gm_dP)-500, max(gm_dP)+500)

  if file_out:
    plt.savefig(file_out, dpi=200)
    print '\n - plot_gyre: mark_dP_with_features: %s created \n' % (file_out, )
    plt.close()


  return None

#=================================================================================================================
def check_dP_correlates(recarr, only_for=None, file_out=None):
  """
  To check "graphically" if different quantities do correlate with one another, so that we would perform a Principal Component Analysis
  or Multivariate Linear Regression.
  @param recarr: record array that contains all period spacing and evolutionary columns.
  @type recarr: numpy record array
  @param only_for: check the dP correlation only for a limited number of keys passed through this argument (default=None).
  @type only_for: list of strings
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  names = recarr.dtype.names
  if only_for is not None: names = only_for
  n_col = len(names)
  n_plots = n_col * (n_col-1)/2
  n_panels = int(np.ceil( np.sqrt(n_plots) ))  # there will be n_panels by n_panels subplots


  ########################################
  # Prepare the plot
  ########################################
  fig = plt.figure(figsize=(8,8), dpi=600)
  plt.subplots_adjust()
  cmap = plt.get_cmap('brg')
  norm = mpl.colors.Normalize(vmin=0, vmax=n_plots)
  color = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)

  i_plot = 1
  for i_x, x_col in enumerate(names):
    x_item = recarr[x_col]

    for i_y in range(i_x+1, n_col):
      y_col = names[i_y]
      y_item = recarr[y_col]

      ax = fig.add_subplot(n_panels, n_panels, i_plot)
      xname = pt.get_name_for_axis(x_col)
      yname = pt.get_name_for_axis(y_col)
      ax.annotate(xname, xy=(0.7, 0.1), xycoords='axes fraction', fontsize='xx-small', color='black')
      ax.annotate(yname, xy=(0.1, 0.7), xycoords='axes fraction', fontsize='xx-small', color='black')
      ax.scatter(x_item, y_item, s=1, color=color.to_rgba(i_plot))
      ax.set_xticks(())
      ax.set_yticks(())

      i_plot += 1


  if file_out:
    plt.savefig(file_out, dpi=600)
    print '\n - plot_gyre: check_dP_correlates: %s created' % (file_out, )
    print '   It has %s panels in a shape of %s-by-%s window' % (n_plots, n_panels, n_panels )
    plt.close()

  return None

#=================================================================================================================
def compare_dP_with_shift(list_dic_dP, list_lbls=None, el=1, P_from=1.0, P_to=2.2, shift=0., file_out=None):
  """
  """
  from scipy.signal import argrelmin, argrelmax, argrelextrema
  dic_conv = cm.conversions()
  sec_to_d = dic_conv['sec_to_d']

  n_dic = len(list_dic_dP)
  if n_dic == 0:
    message = 'Error: plot_gyre: compare_dP_with_shift: Input list is empty'
    raise SystemExit, message

  ########################################
  # Prepare the plot
  ########################################
  import itertools
  fig = plt.figure(figsize=(5,6), dpi=200)
  ax = fig.add_subplot(111)
  left = 0.14
  if shift > 0: left = 0.05
  plt.subplots_adjust(left=left, right=0.98, bottom=0.09, top=0.94)
  cmap = plt.get_cmap('brg')
  norm = mpl.colors.Normalize(vmin=0, vmax=n_dic)
  color = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)
  markers = itertools.cycle('osp>*1HD^,d')
  linestyles = itertools.cycle(['solid', 'dashed', 'dotted', 'dashdot'])

  # Add a patch associated with the observed period range
  if P_from < 0.9 and P_to > 2.2:
    obs_box = plt.Rectangle((0.9, 0), 1.3, 3000000, facecolor="0.90", edgecolor=None, lw=0.5, alpha=0.5)
    plt.gca().add_patch(obs_box)

  ########################################
  # Iteratively do the plotting
  ########################################
  list_min_dP = []; list_max_dP = []
  list_m_ini = []; list_eta = []; list_ov = []; list_Z = []; list_Xc = []
  list_loc_left_point = []
  for i_dic, dic_dP in enumerate(list_dic_dP):
    # check first, if the period spacing for the desired el is already available
    str_el = str(el)
    this_key = 'el_' + str_el + '_gm_P'
    if this_key not in dic_dP.keys():
      message = 'Error: plot_gyre: compare_dP_with_shift: the key: %s not available among input keys in the dictionary' % (this_key, )
      raise SystemExit, message

    key_gm_P  = 'el_' + str_el + '_gm_P'
    key_gm_dP = 'el_' + str_el + '_gm_dP'
    key_gm_dip_P = 'el_' + str_el + '_gm_dip_P'
    key_gm_dip_dP = 'el_' + str_el + '_gm_dip_dP'
    key_gm_dP_mean = 'el_' + str_el + '_gm_dP_mean'
    key_gm_peak_P  = 'el_' + str_el + '_gm_peak_P'
    key_gm_peak_dP = 'el_' + str_el + '_gm_peak_dP'
    key_gm_peak_dP_mean = 'el_' + str_el + '_gm_peak_dP_mean'

    gm_P  = dic_dP[key_gm_P] * sec_to_d
    gm_dP = dic_dP[key_gm_dP]
    gm_dip_P = dic_dP[key_gm_dip_P] * sec_to_d
    gm_dip_dP = dic_dP[key_gm_dip_dP]
    gm_dP_mean = dic_dP[key_gm_dP_mean]
    gm_peak_P  = dic_dP[key_gm_peak_P] * sec_to_d
    gm_peak_dP = dic_dP[key_gm_peak_dP]
    gm_peak_dP_mean = dic_dP[key_gm_peak_dP_mean]

    ind_P = [i for i in range(len(gm_P)) if gm_P[i] >= P_from and gm_P[i] <= P_to]
    if ind_P[-1] == len(gm_dP): del_last_ind = ind_P.pop(-1)
    gm_P  = gm_P[ind_P]
    gm_dP = gm_dP[ind_P]
    if shift > 0:
      gm_dP += i_dic * shift
      gm_dip_dP += i_dic * shift
      gm_dP_mean += i_dic * shift
      gm_peak_dP += i_dic * shift
      gm_peak_dP_mean += i_dic * shift

    lns = linestyles.next()
    mrk = markers.next()
    clr = color.to_rgba(i_dic)

    ax.axhline(y=gm_dP_mean, xmin=min(gm_P), xmax=max(gm_P), linestyle='dotted', lw=0.5, color=clr)
    ax.axhline(y=gm_peak_dP_mean, xmin=min(gm_P), xmax=max(gm_P), linestyle='dashed', lw=1, color=clr)
    if list_lbls is None:
      ax.plot(gm_P, gm_dP, color=clr, linestyle='solid', lw=1)
      ax.scatter(gm_P, gm_dP, facecolor=clr, marker=mrk, s=36, edgecolor='black')
    else:
      ax.plot(gm_P, gm_dP, color=clr, linestyle='solid', lw=2)
      ax.scatter(gm_P, gm_dP, facecolor=clr, marker=mrk, s=36, edgecolor='black',
                 label=r''+list_lbls[i_dic])
    ax.scatter(gm_dip_P, gm_dip_dP, marker=mrk, facecolor='white', edgecolor='black', s=50)
    ax.scatter(gm_peak_P, gm_peak_dP, marker=mrk, facecolor='white', edgecolor='black', s=50)

    # Save these values for plotting annotations
    list_min_dP.append(np.min(gm_dP))
    list_max_dP.append(np.max(gm_dP))
    list_m_ini.append(dic_dP['m_ini'])
    list_eta.append(dic_dP['eta'])
    list_ov.append(dic_dP['ov'])
    list_Z.append(dic_dP['Z'])
    list_Xc.append(dic_dP['Xc'])
    list_loc_left_point.append((gm_P[0]-0.1, gm_dP[0]*1.02))

  ########################################
  # Annotations, Legend, and Ranges
  ########################################
  ax.set_xlabel(r'Period [d]')
  ax.set_xlim(P_from, P_to)
  ax.set_ylabel(r'Period Spacing $\Delta P$ [sec]')
  ax.set_ylim(max([min(list_min_dP)*0.99,0]), max(list_max_dP)*1.01)
  if shift > 0: ax.set_yticks(())

  min_m_ini = min(list_m_ini)
  max_m_ini = max(list_m_ini)
  min_eta   = min(list_eta)
  max_eta   = max(list_eta)
  min_ov    = min(list_ov)
  max_ov    = max(list_ov)
  min_Z     = min(list_Z)
  max_Z     = max(list_Z)
  min_Xc    = round(min(list_Xc), 3)
  max_Xc    = round(max(list_Xc), 3)

  if min_m_ini == max_m_ini:
    txt_m_ini = r'M=%05.2fM$_\odot$' % (min_m_ini,)
  else:
    txt_m_ini = r'%05.2f$\leq M/M_\odot\leq$%05.2f' % (min_m_ini, max_m_ini)
  if min_eta == max_eta:
    txt_eta = r'$\eta_{\rm rot}=$%0.2f' % (min_eta,)
  else:
    txt_eta = r'%0.2f$\leq \eta_{\rm rot}\leq$%0.2f' % (min_eta, max_eta)
  if min_ov == max_ov:
    txt_ov = r'$f_{\rm ov}=$%0.3f' % (min_ov, )
  else:
    txt_ov = r'%0.3f$\leq f_{\rm ov}\leq$%0.3f' % (min_ov, max_ov)
  if min_Xc == max_Xc:
    txt_Xc = r'$X_{\rm c}=$%0.3f' % (min_Xc, )
  else:
    txt_Xc = r'%0.3f$\leq X_{\rm c}\leq$%0.3f' % (min_Xc, max_Xc)
  if min_Z == max_Z:
    txt_Z = r'$Z=$%0.3f' % (min_Z, )
  else:
    txt_Z = r'%0.3f$\leq Z\leq$%0.3f' % (min_Z, max_Z)

  txt_el = r'$\ell$='+str_el

  txt_title = txt_el + ', ' + txt_m_ini + ', ' + txt_eta + ', ' + txt_ov + ', ' + txt_Z + ', ' + txt_Xc
  ax.set_title(txt_title, fontsize='small')

  for i, loc in enumerate(list_loc_left_point):
    ax.annotate(list_lbls[i], xy=loc, xycoords='data', color=color.to_rgba(i), fontsize='x-small')
  #if list_lbls:
    #leg = ax.legend(loc=4, fontsize='x-small')
    #leg.get_frame().set_alpha(0.5)

  if file_out:
    plt.savefig(file_out, dpi=200)
    print '\n - plot_gyre: compare_dP_with_shift: %s created' % (file_out, )
    plt.close()

  return ax

#=================================================================================================================
def compare_mode_evolution(rec_arr, el=1, n_pg=-1, compare_by='eta', list_comp_vals=[0.,0.1],
                           comp_freq=False, comp_P=False, comp_omega=False,
                           xaxis_by_Xc=False, xaxis_by_Teff=False, file_out=None):
  """
  Trace and compare the mode frequency (or omega) or period. The tracer is either age (drop in Xc) or Teff (not implemented yet),
  and the comparison item is chosen by compare_by keyword, which searches to find canonical values in the list provided
  by list_comp_vals.
  @param rec_arr: Input array which contains the whole seismic and evolutionary information per each mode
  @type rec_arr: numpy record array
  @param el: (default=1); spherical polar degree, only searches for modes with this given el
  @type el: integer
  @param n_pg: (default=-1) the mode order, only searches for modes with this order
  @type n_pg: signed integer
  @param compare_by: comparison is based on this. It has to be already provided and available in the input record array.
      Appropriate choices are m_ini, eta, ov, Z and Xc
  @type compare_by: string
  @param list_comp_vals: the comparison is performed for the values provided in this list. If one or all of them are not
      available in the input record array, then a warning flag is raised, and printed on the terminal
  @type list_comp_vals: list or numpy array
  @param comp_freq, comp_omega, comp_P: choose one of these to put on the yaxis. E.g. you trace and compare mode period for
      a fixed m_ini, but across different overshooting parameter.
  @type comp_freq, comp_omega, comp_P: boolean
  @param xaxis_by_Xc, xaxis_by_Teff: To trace the evolution by age (i.e. drop in Xc) or by drop in Teff (not implemented yet)
  @type xaxis_by_Xc, xaxis_by_Teff: boolean
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: the plot created
  @rtype: matplotlib axes object
  """
  n_rec_orig = len(rec_arr)
  if n_rec_orig == 0:
    message = 'Error: plot_gyre: compare_mode_evolution: Input record array is empty'
    raise SystemExit, message
  if not any([comp_freq, comp_P, comp_omega]) or all([comp_freq, comp_P, comp_omega]):
    message = 'Error: plot_gyre: compare_mode_evolution: Choose either of comp_freq OR comp_P'
    raise SystemExit, message
  if not any([xaxis_by_Teff, xaxis_by_Xc]) or all([xaxis_by_Teff, xaxis_by_Xc]):
    message = 'Error: plot_gyre: compare_mode_evolution: Choose either of xaxis_by_Teff OR xaxis_by_Xc'
    raise SystemExit, message
  if xaxis_by_Teff:
    message = 'Error: plot_gyre: compare_mode_evolution: xaxis_by_Teff not implemented yet. Just do it :-D'
    raise SystemExit, message
  if el != 0 and n_pg == 0:
    print 'Warning: plot_gyre: compare_mode_evolution: n_pg = 0 for el=%s is not supported' % (el, )
    return None

  rec_names  = rec_arr.dtype.names
  if compare_by not in rec_names:
    message = 'Error: plot_gyre: compare_mode_evolution: %s not found in the available numpy record array fields' % (compare_by, )
    raise SystemExit, message

  # start reducing the large sample table into a small chunks so that each chuck iteratively meets one of the external
  # constraints, e.g. for_eta = 0.10
  rec_arr = rec_arr[rec_arr['l']==el]
  if type(rec_arr) is type(None):
    message = 'Error: plot_gyre: trace_mode_evolution: no field with el = %s' % (el, )
    raise SystemExit, message
  #
  rec_arr = rec_arr[rec_arr['n_pg']==n_pg]
  if type(rec_arr) is type(None):
    message = 'Error: plot_gyre: trace_mode_evolution: no field with n_pg = %s' % (n_pg, )
    raise SystemExit, message
  #
  n_rec = len(rec_arr)
  print ' - Applying mode geometry constraints: el = %s, n_pg = %s' % (el, n_pg)
  print '   Input record survived shrinking from %s to %s lines' % (n_rec_orig, n_rec)

  # iteratively copy the desired part of the rec_arr with a selected lines if available (i.e. if desired value present)
  list_comp_rec = []
  list_avail_comp_vals = []
  pack_xaxis = []
  pack_yaxis = []
  for i_val, comp_val in enumerate(list_comp_vals):
    ind_comp = [i for i in range(n_rec) if rec_arr[compare_by][i]==comp_val]
    if len(ind_comp) == 0:
      print ' - Warning: plot_gyre: trace_mode_evolution: no field with %s = %s' % (compare_by, comp_val)
      continue
    #list_comp_rec.append(rec_arr[ind_comp].copy())       # Generate the first record array assosiated to first comparison value
    list_avail_comp_vals.append(comp_val)
    if xaxis_by_Xc:
      pack_xaxis.append(rec_arr['Xc'][ind_comp].copy())
    if comp_freq:
      pack_yaxis.append(rec_arr['freq_cd'][ind_comp].copy())
    if comp_P:
      pack_yaxis.append(rec_arr['period_d'][ind_comp].copy())
    if comp_omega:
      pack_yaxis.append(rec_arr['omega_re'][ind_comp].copy())

  n_comp = len(list_avail_comp_vals)
  if n_comp == 0:
    message = 'Error: plot_gyre: trace_mode_evolution: Comparison list ended up empty for %s' % (compare_by, )
    raise SystemExit, message

  ########################################
  # Prepare the plot
  ########################################
  import itertools
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax = plt.axes([0.11,0.11,0.87,0.87])

  markers = itertools.cycle('osp>*+xD^,8')
  cmap = plt.get_cmap('brg')
  norm = mpl.colors.Normalize(vmin=0, vmax=n_comp)
  color = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)

  for i_comp, comp_val in enumerate(list_avail_comp_vals):
    lbl_txt = compare_by + '=' + str(comp_val)
    ax.plot(pack_xaxis[i_comp], pack_yaxis[i_comp], lw=1, color='grey', linestyle='solid')
    ax.scatter(pack_xaxis[i_comp], pack_yaxis[i_comp], marker=markers.next(), color=color.to_rgba(i_comp),
               label=lbl_txt)

  if xaxis_by_Xc:
    #from collections import Counter
    #vec_x_vals = np.sort(np.array( list(Counter(rec_arr['Xc']) ))
    ax.set_xlabel(r'Central Hydrogen $X_{\rm c}$')
    ax.set_xlim(0.70, 0.0)
  if comp_freq:
    ax.set_ylabel(r'Frequency [d$^{-1}$]')
  if comp_P:
    ax.set_ylabel(r'Period [d]')
  if comp_omega:
    ax.set_ylabel(r'Frequency $\omega_r$ [$(GM/R^3)^{1/2}$]')

  ########################################
  # Annotations and Legend
  ########################################
  list_annot = []
  if comp_omega or comp_freq:
    left = 0.85; top = 0.95
  if comp_P:
    left = 0.85; top = 0.15
  shift = 0.055
  list_annot.append(r'$\ell=$'+str(el))
  list_annot.append(r'$n_{\rm pg}=$'+str(n_pg))
  n_annot = len(list_annot)
  for i_annot, txt_annot in enumerate(list_annot):
    ax.annotate(txt_annot, xy=(left,top-i_annot*shift), xycoords='axes fraction', color='black', fontsize='medium')

  if comp_omega or comp_freq: leg_loc = 2
  if comp_P: leg_loc = 3
  leg = ax.legend(loc=leg_loc, fontsize='x-small', shadow=True, fancybox=True)
  leg.get_frame().set_alpha(0.5)

  if file_out:
    plt.savefig(file_out, dpi=200)
    print '- plot_gyre: compare_mode_evolution: %s created \n' % (file_out, )
    plt.close()

  return ax

#=================================================================================================================
def trace_mode_evolution(rec_arr, xaxis_by_Xc=False, xaxis_by_Teff=False, yaxis_by_freq=False, yaxis_by_P=False, yaxis_by_omega=False,
                    for_m_ini=5.0, for_eta=0.00, for_ov=0.006, for_Z=0.014,
                    el=1, n_pg_from=-10, n_pg_to=5, file_out=None):
  """
  This function shows a plot with the evolution of the mode frequency/period/omega as a function of age (i.e. Xc drop).
  You can specify the desired el and the range of n_pg to accommodate all in the same plot.
  WARNING: xaxis_by_Teff is not supported yet, since the GYRE output does not provide Teff.
  @param rec_arr: Input numpy record array which contains the whole seismic and evolutionary information of every individual
     mode. For the full list of available fields see mesa_gyre.write.modes_to_recarray
  @type rec_arr: numpy record array
  @param xaxis_by_Xc: plot the xaxis by drop in Xc
  @type xaxis_by_Xc: boolean
  @param xaxis_by_Teff: WARNING: Nont supported yet. plot the xaxis by drop in Teff
  @type xaxis_by_Teff: boolean
  @param yaxis_by_freq, yaxis_by_omega, yaxis_by_P: plot yaxis by frequency, omega (i.e. adimensional frequency) or period, respectively.
  @type yaxis_by_freq, yaxis_by_omega, yaxis_by_P: boolean
  @param for_m_ini: Specify the specific initial mass of the models to select
  @type for_m_ini: float
  @param for_eta: Specify the specific initial rotation rate, eta
  @type for_eta: float
  @param for_ov: Specify the specific initial overshooting ov
  @type for_ov: float
  @param for_Z: Specify the specific initial metalicity Z
  @type for_Z: float
  @param el: Specify which harmonic degree to trace; default = 1
  @type el: integer
  @param n_pg_from: Specify the lower mode order n_pg = np - n_g (sometimes +1) to trace. it is inclusive, default = -10
  @type n_pg_from: signed integer
  @param n_pg_to: Specify the the upper inclusive mode order to trace.
  @type n_pg_to: integer
  @param file_out: Specify the full path to store the plot file. By default no plot file is stored.
  @type file_out: string
  @return: plot axis used to create this plot
  @rtype: numpy axes
  """
  n_rec = len(rec_arr)
  if n_rec == 0:
    message = 'Error: plot_gyre: trace_mode_evolution: Input record array is empty'
    raise SystemExit, message
  if not any([xaxis_by_Teff, xaxis_by_Xc]) or all([xaxis_by_Teff, xaxis_by_Xc]):
    message = 'Error: plot_gyre: trace_mode_evolution: Choose either of xaxis_by_Teff OR xaxis_by_Xc.'
    raise SystemExit, message
  if not any([yaxis_by_freq, yaxis_by_P, yaxis_by_omega]) or all([yaxis_by_freq, yaxis_by_P, yaxis_by_omega]):
    message = 'Error: plot_gyre: trace_mode_evolution: Choose either of yaxis_by_freq, yaxis_by_omega OR yaxis_by_P.'
    raise SystemExit, message

  rec_names  = rec_arr.dtype.names
  n_len_orig = len(rec_arr)
  # start reducing the large sample table into a small chunks so that each chuck iteratively meets one of the external
  # constraints, e.g. for_eta = 0.10
  rec_arr = rec_arr[rec_arr['m_ini']==for_m_ini]
  if type(rec_arr) is type(None):
    message = 'Error: plot_gyre: trace_mode_evolution: no field with m_ini = %s' % (for_m_ini, )
    raise SystemExit, message
  #
  rec_arr = rec_arr[rec_arr['eta']==for_eta]
  if type(rec_arr) is type(None):
    message = 'Error: plot_gyre: trace_mode_evolution: no field with eta = %s' % (for_eta, )
    raise SystemExit, message
  #
  rec_arr = rec_arr[rec_arr['ov']==for_ov]
  if type(rec_arr) is type(None):
    message = 'Error: plot_gyre: trace_mode_evolution: no field with ov = %s' % (for_ov, )
    raise SystemExit, message
  #
  rec_arr = rec_arr[rec_arr['Z']==for_Z]
  if type(rec_arr) is type(None):
    message = 'Error: plot_gyre: trace_mode_evolution: no field with Z = %s' % (for_Z, )
    raise SystemExit, message
  #
  n_len = len(rec_arr)
  print '\n - plot_gyre: trace_mode_evolution: Applying physical parameter constraints:'
  print '   Input record survived shrinking from %s to %s lines' % (n_len_orig, n_len)

  #
  rec_arr = rec_arr[rec_arr['l']==el]
  if type(rec_arr) is type(None):
    message = 'Error: plot_gyre: trace_mode_evolution: no field with l = %s' % (el, )
    raise SystemExit, message
  #
  ind_n_pg = [i for i in range(len(rec_arr)) if rec_arr['n_pg'][i]>=n_pg_from and rec_arr['n_pg'][i]<=n_pg_to]
  if len(ind_n_pg) == 0:
    message = 'Error: plot_gyre: trace_mode_evolution: Input record array did not survive the constraint on n_pg'
    raise SystemExit, message
  rec_arr = rec_arr[ind_n_pg]

  print ' - Applying mode geometry constraints: el = %s, n_pg = %s to %s' % (el, n_pg_from, n_pg_to)
  print '   Input record survived shrinking from %s to %s lines' % (n_len_orig, n_len)
  n_len = len(rec_arr)

  if xaxis_by_Teff:
    message = 'Error: plot_gyre: trace_mode_evolution: xaxis_by_Teff not supported yet.'
    raise SystemExit, message

  from collections import Counter
  vec_x_vals = np.sort(np.array( list(Counter(rec_arr['Xc'])) ))

  pack_n_pg  = []
  pack_xaxis = []
  pack_yaxis = []
  min_y_arr = []; max_y_arr = []
  for n_pg in range(n_pg_from, n_pg_to+1, +1):
    if n_pg == 0: continue
    ind_n_pg = [i for i in range(n_len) if rec_arr['n_pg'][i]==n_pg]
    n_ind = len(ind_n_pg)
    if n_ind == 0: continue

    pack_n_pg.append(n_pg)
    pack_xaxis.append(rec_arr['Xc'][ind_n_pg])
    if yaxis_by_freq:
      pack_yaxis.append(rec_arr['freq_cd'][ind_n_pg])
      min_y_arr.append(min(rec_arr['freq_cd'][ind_n_pg]))
      max_y_arr.append(max(rec_arr['freq_cd'][ind_n_pg]))
    if yaxis_by_P:
      pack_yaxis.append(rec_arr['period_d'][ind_n_pg])
      min_y_arr.append(min(rec_arr['period_d'][ind_n_pg]))
      max_y_arr.append(max(rec_arr['period_d'][ind_n_pg]))
    if yaxis_by_omega:
      pack_yaxis.append(rec_arr['omega_re'][ind_n_pg])
      min_y_arr.append(min(rec_arr['omega_re'][ind_n_pg]))
      max_y_arr.append(max(rec_arr['omega_re'][ind_n_pg]))

  ########################################
  # Prepare the plot
  ########################################
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax = plt.axes([0.11,0.11,0.87,0.875])

  if xaxis_by_Xc:
    ax.set_xlim(max(vec_x_vals), min(vec_x_vals))
    ax.set_xlabel(r'Central Hydrogen $X_{\rm c}$')
  if yaxis_by_freq:
    ax.set_ylabel(r'Frequency [d$^{-1}$]')
    ax.set_ylim(min(min_y_arr), max(max_y_arr))
  if yaxis_by_omega:
    ax.set_ylabel(r'Frequency $\omega_{\rm r}$ [$(GM/R^3)^{1/2}$]')
    ax.set_ylim(min(min_y_arr), max(max_y_arr))
  if yaxis_by_P:
    ax.set_ylabel(r'Period [d]')
    ax.set_ylim(min(min_y_arr), max(max_y_arr))

  ########################################
  # Iteratively plot each mode
  ########################################
  for i_n, n_pg in enumerate(pack_n_pg):
    if n_pg < 0:
      marker = 'o'
      color  = 'red'
    else:
      marker = 's'
      color  = 'blue'
    ax.plot(pack_xaxis[i_n], pack_yaxis[i_n], lw=1, linestyle='solid', color='grey')
    ax.scatter(pack_xaxis[i_n], pack_yaxis[i_n], s=10, marker=marker, color=color)

  ########################################
  # Annotations
  ########################################
  list_annot = []
  left = 0.80; top = 0.93; shift = 0.055
  list_annot.append(r'$\ell$=' + str(el))
  list_annot.append(r''+str(n_pg_from) + r'$\leq n_{\rm pg}\leq$' + r''+str(n_pg_to))
  list_annot.append(r'M='+str(for_m_ini)+r' [M$_\odot$]')
  list_annot.append(r'$\eta_{\rm rot}$='+str(for_eta))
  list_annot.append(r'$f_{\rm ov}$='+str(for_ov))
  list_annot.append(r'Z='+str(for_Z))

  for i_annot, txt_annot in enumerate(list_annot):
    ax.annotate(txt_annot, xy=(left, top-shift*i_annot), xycoords='axes fraction', color='black', fontsize='medium')

  ########################################
  # Save the plot
  ########################################
  if file_out:
    plt.savefig(file_out, dpi=200)
    print '\n - plot_gyre: trace_mode_evolution: %s created' % (file_out, )
    plt.close()

  return ax

#=================================================================================================================
def contour_mode_properties(rec_arr, xaxis, yaxis, el=1, n_pg=-1, by_freq=False, by_P=False,
                            n_levels=11, file_out=None):
  """
  This function provides a contour presentatin of the frequency or period of a mode on a plane specifed with xaxis and
  yaxis. The choices of the latter two comes from the grid free parameters like Xc, m_ini, ov, eta, and Z.
  WARNING: THERE ARE MANY CASES WHERE MORE THAN ONE MATCH IS FOUND FOR A SET OF GIVEN XAXIS AND YAXIS IN THE SUBSET.
  THIS OCCURS FOR XC<0.004. CURRENTLY, I ONLY USE THOSE CASES WHERE THERE IS EXACTLY ONE MATCH FOUND
  @param rec_arr: Input record array that contains the whole asteroseismic and evolutionary information per each mode.
  @type rec_arr: numpy record array
  @param xaxis, yaxis: one of the available fields in the record array to be place on xaxis/yaxis.
     Best examples are 'Xc', 'ov', 'Z' or 'eta'
  @type xaxis, yaxis: string
  @param el: (default = 1), specifying the harmonic degree of the mode
  @type el: integer
  @param n_pg: (default=-1) specifying the mode order
  @type n_pg: integer
  @param by_freq: plot the frequency distribution of the mode as a contour depending on xaxis and yaxis
  @type by_freq: boolean
  @param by_P: plot the period distribution of the mode depending on xaxis and yaxis as a contour
  @type by_P: boolean
  @param n_levels: Number of levels that the contour divides the values into color bins (default = 11). Odd numbers are preferred
  @type n_levels: integer
  @param file_out: full path to the output contour plot file to store, default is to skip creating the plot
  @type file_out: string
  @return: matplotlib axis used to create and store the plot
  @rtype: matplotlib axes
  """
  n_rec = len(rec_arr)
  if n_rec == 0:
    message = 'Error: plot_gyre: contour_mode_properties: Input record array is empty'
    raise SystemExit, message
  if not any([by_freq, by_P]) or all([by_freq, by_P]):
    message = 'Error: plot_gyre: contour_mode_properties: Choose one of by_freq OR by_P'
    raise SystemExit, message
  if xaxis == yaxis:
    message = 'Error: plot_gyre: contour_mode_properties: xaxis:%s cannot be the same as yaxis:%s' % (xaxis, yaxis)
    raise SystemExit, message
  if n_pg == 0:
    print ' - Warning: plot_gyre: contour_mode_properties: n_pg = 0 is not possible. Exiting'
    return None

  from collections import Counter

  rec_names  = rec_arr.dtype.names
  if xaxis not in rec_names or yaxis not in rec_names:
    message = 'Error: plot_gyre: contour_mode_properties: xaxis:%s OR yaxis:%s not in the record array available fields' % (xaxis, yaxis)
    raise SystemExit, message
  vec_x_vals = np.sort(np.array( list( Counter(rec_arr[xaxis]) ) ))
  vec_y_vals = np.sort(np.array( list( Counter(rec_arr[yaxis]) ) ))
  n_x = len(vec_x_vals)
  n_y = len(vec_y_vals)
  x_mesh = np.array([vec_x_vals] * n_y).transpose()
  y_mesh = np.array([vec_y_vals] * n_x)
  grid_shape = x_mesh.shape
  z = np.zeros((n_x, n_y))

  ########################################
  # Prepare the plot
  ########################################
  from matplotlib.colors import BoundaryNorm
  from matplotlib.ticker import MaxNLocator
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax = plt.axes([0.125,0.105,0.91,0.85])

  if xaxis=='ov' or xaxis=='eta' or xaxis=='m_ini' or xaxis=='Z':
    ax.set_xlim(min(vec_x_vals), max(vec_x_vals))
  elif xaxis=='Xc' or xaxis=='Yc':
    ax.set_xlim(max(vec_x_vals), min(vec_x_vals))
  else:
    raise SystemExit, 'Error: plot_gyre: contour_mode_properties: sth wrong with xaxis'

  if yaxis=='ov' or yaxis=='eta' or yaxis=='m_ini' or yaxis=='Z':
    ax.set_ylim(min(vec_y_vals), max(vec_y_vals))
  elif yaxis=='Xc' or yaxis=='Yc':
    ax.set_ylim(max(vec_y_vals), min(vec_y_vals))
  else:
    raise SystemExit, 'Error: plot_gyre: contour_mode_properties: sth wrong with yaxis'

  # force xticks and yticks to the grid values in each parameter
  if xaxis != 'Xc': ax.set_xticks(vec_x_vals)
  else: ax.set_xticks(np.arange(0.0, 0.80, 0.10))
  if yaxis != 'Xc': ax.set_yticks(vec_y_vals)
  else: ax.set_yticks(np.linspace(min(vec_y_vals), max(vec_y_vals), 0.10))

  if xaxis == 'm_ini': ax.set_xlabel(r'Initial Mass [M$_\odot$]')
  if xaxis == 'ov':  ax.set_xlabel(r'Overshoot $f_{\rm ov}$')
  if xaxis == 'eta': ax.set_xlabel(r'Rotation Rate $\eta_{\rm rot}$')
  if xaxis == 'Z':   ax.set_xlabel(r'Metalicity $Z$')
  if xaxis == 'Xc':  ax.set_xlabel(r'Central Hydrogen $X_{\rm c}$')
  if yaxis == 'm_ini': ax.set_ylabel(r'Initial Mass [M$_\odot$]')
  if yaxis == 'ov':  ax.set_ylabel(r'Overshoot $f_{\rm ov}$')
  if yaxis == 'eta': ax.set_ylabel(r'Rotation Rate $\eta_{\rm rot}$')
  if yaxis == 'Z':   ax.set_ylabel(r'Metalicity $Z$')
  if yaxis == 'Xc':  ax.set_ylabel(r'Central Hydrogen $X_{\rm c}$')

  min_m_ini_arr = []; max_m_ini_arr = []
  min_eta_arr = []  ; max_eta_arr = []
  min_ov_arr = []   ; max_ov_arr = []
  min_Z_arr = []    ; max_Z_arr = []
  min_Xc_arr = []   ; max_Xc_arr = []

  ind_el_n_pg = [i for i in range(n_rec) if rec_arr['l'][i]==el and rec_arr['n_pg'][i]==n_pg]
  n_ind_el_n_pg = len(ind_el_n_pg)
  if n_ind_el_n_pg == 0:
    message = 'Error: plot_gyre: contour_mode_properties: Input record array did not survive constraints by el:%s and n_pg:%s' % (el, n_pg)
    raise SystemExit, message

  subset = rec_arr[ind_el_n_pg]

  x = subset[xaxis]
  y = subset[yaxis]
  if by_freq: plot_arr = subset['freq_cd']
  if by_P:    plot_arr = subset['period_d']

  for i_x, x_mesh_val in enumerate(vec_x_vals):
    for i_y, y_mesh_val in enumerate(vec_y_vals):
      ind_match_x_y = [i for i in range(n_ind_el_n_pg) if x[i]==x_mesh_val and y[i]==y_mesh_val]
      n_ind_match_x_y = len(ind_match_x_y)
      if n_ind_match_x_y == 1:
        ind_match_x_y = ind_match_x_y[0]
        z[i_x, i_y] = plot_arr[ind_match_x_y]
      #if n_ind_match_x_y > 1:
        #i = ind_match_x_y

  # collect the min and max of different physical parameters
  min_m_ini_arr.append(min(subset['m_ini']))
  max_m_ini_arr.append(max(subset['m_ini']))
  min_eta_arr.append(min(subset['eta']))
  max_eta_arr.append(max(subset['eta']))
  min_ov_arr.append(min(subset['ov']))
  max_ov_arr.append(max(subset['ov']))
  min_Z_arr.append(min(subset['Z']))
  max_Z_arr.append(max(subset['Z']))
  min_Xc_arr.append(min(subset['Xc']))
  max_Xc_arr.append(max(subset['Xc']))

  # x and y are bounds, so z should be the value *inside* those bounds.
  # Therefore, remove the last value from the z array.
  levels = MaxNLocator(nbins=n_levels).tick_values(z[z>0.0].min(), z.max())

  # pick the desired colormap, sensible levels, and define a normalization
  # instance which takes data values and translates those into levels.
  cmap = plt.get_cmap('seismic')
  norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

  #plt.contourf(x_mesh, y_mesh, z, levels=levels, cmap=cmap)
  im = plt.pcolormesh(x_mesh, y_mesh, z, cmap=cmap, norm=norm)
  plt.colorbar()

  ########################################
  # Annotations
  ########################################
  #if by_freq: txt_z = r'Frequency [Hz]'
  #if by_P:    txt_z = r'Period [d$^{-1}$]'
  #ax.annotate(txt_z, xy=(0.04, 0.05), xycoords='axes fraction', fontsize='medium', color='yellow')
  list_annot = []
  left = 0.04; top = 0.95; shift = 0.055
  list_annot.append(r'$\ell$=' + str(el))
  list_annot.append(r'$n_{\rm pg}$=' + str(n_pg))

  m_ini_from = min(min_m_ini_arr); m_ini_to = max(max_m_ini_arr)
  eta_from = min(min_eta_arr);     eta_to = max(max_eta_arr)
  ov_from = min(min_ov_arr);       ov_to = max(max_ov_arr)
  Z_from = min(min_Z_arr);         Z_to = max(max_Z_arr)
  Xc_from = min(min_Xc_arr);       Xc_to = max(max_Xc_arr)

  if xaxis != 'm_ini' and yaxis != 'm_ini':
    if m_ini_from < m_ini_to: list_annot.append(r''+str(round(m_ini_from,2))+'$\leq M/M_\odot\leq$'+str(round(m_ini_to,2)) )
    if m_ini_from == m_ini_to:list_annot.append(r'$M/M_\odot=$'+str(round(m_ini_to,2)) )
  if xaxis != 'eta' and yaxis != 'eta':
    if eta_from < eta_to: list_annot.append(r''+str(round(eta_from,2))+r'$\leq \eta_{\rm rot}\leq$'+str(round(eta_to,2)) )
    if eta_from == eta_to:list_annot.append(r'$\eta_{\rm rot}=$'+str(round(eta_to,2)) )
  if xaxis != 'ov' and yaxis != 'ov':
    if ov_from < ov_to: list_annot.append(r''+str(round(ov_from,3))+r'$\leq f_{\rm ov}\leq$'+str(round(ov_to,3)) )
    if ov_from == ov_to:list_annot.append(r'$f_{\rm ov}=$'+str(round(ov_to,3)) )
  if xaxis != 'Z' and yaxis != 'Z':
    if Z_from < Z_to: list_annot.append(r''+str(round(Z_from,3))+'$\leq Z\leq$'+str(round(Z_to,3)) )
    if Z_from == Z_to:list_annot.append(r'$Z=$'+str(round(Z_to,3)) )
  if xaxis != 'Xc' and yaxis != 'Xc':
    if Xc_from < Xc_to: list_annot.append(r''+str(round(Xc_from,4))+r'$\leq X_{\rm c}\leq$'+str(round(Xc_to,4)) )
    if Xc_from == Xc_to:list_annot.append(r'$X_{\rm c}=$'+str(round(Xc_to,4)) )

  n_annot = len(list_annot)
  for i_annot, txt_annot in enumerate(list_annot):
    ax.annotate(txt_annot, xy=(left, top-i_annot*shift), xycoords='axes fraction', fontsize='medium', color='yellow')


  ########################################
  # Saving the plot
  ########################################
  if file_out:
    plt.savefig(file_out, dpi=200)
    print '\n - plot_gyre: contour_mode_properties: %s saved.' % (file_out, )
    plt.close

  return ax

#=================================================================================================================
def contour_instability_strips(list_dic, list_dic_comp=[], list_hist=[], el=1, style='Kiel', 
                               logT_from=4.6, logT_to=4, yaxis_from=4.5, yaxis_to=3.2, 
                               list_dic_annot=[], file_out=None):
  """
  Contour plot of instability strips showing the number of excited modes and the boundary of instability
  regions at every point that the GYRE output is available. This function can:
  - put a point at the position of each model
  - separate the number of excited p and g modes for each model 
  - make a contour of the n_p and n_g excited modes
  - add few representative tracks

  @param list_dic: list of GYRE output dictionaries for the whole grid 
  @type list_dic: list of dictionaries
  @param list_dic_comp: optional; similar to list_dic, but for another set of tracks, that are computed with 
        a different physical input, e.g. different Z, opacity table, Iron-enhancement factor, etc.
  @type list_dic_comp: list of dictionaries
  @param style: the style of the figure. The only three accepted styles are :
        - kiel: where the ordinate is log_g  
        - hrd: where the ordinate is log_L 
        - sHRD: where the ordinate is the spectroscopic HR diagram
  @tpe style: string 
  @return: None
  @rtype: None
  """
  if file_out is None: return
  # n_dic  = len(list_dic)
  n_comp   = len(list_dic_comp)
  add_comp = n_comp > 0
  n_hist   = len(list_hist)
  add_hist = n_hist > 0

  style = style.lower()
  if style not in ['kiel', 'hrd', 'shrd']:
    logging.error('plot_gyre: contour_instability_strips: style={0} is not supported'.format(style.lower()))
    raise SystemExit, 'Error: plot_gyre: contour_instability_strips: style={0} is not supported'.format(style.lower())

  def give_what_is_needed(dics, style):
    n_dic        = len(dics)
    Xc_arr       = np.zeros(n_dic)
    M_arr        = np.zeros(n_dic)
    R_arr        = np.zeros(n_dic)
    L_arr        = np.zeros(n_dic)
    log_Teff_arr = np.zeros(n_dic)
    log_g_arr    = np.zeros(n_dic)
    log_L_arr    = np.zeros(n_dic)
    n_excited_p  = np.zeros(n_dic, dtype=int)
    n_excited_g  = np.zeros(n_dic, dtype=int)  

    for i_dic, dic in enumerate(dics):
      Xc                  = dic['Xc']
      Xc_arr[i_dic]       = Xc
      M_star              = dic['M_star'] / Msun
      R_star              = dic['R_star'] / Rsun
      L_star              = dic['L_star'] / Lsun
      M_arr[i_dic]        = M_star
      R_arr[i_dic]        = R_star
      L_arr[i_dic]        = L_star
      log_L_arr[i_dic]    = np.log10(L_star)
      log_Teff_arr[i_dic] = np.log10(5777.) + np.log10(L_star)/4 - np.log10(R_star)/2
      log_g_arr[i_dic]    = np.log10(G*Msun/np.power(Rsun, 2)) + np.log10(M_star) - 2 * np.log10(R_star)

      all_el              = dic['l']
      ind_el              = np.where(all_el == el)[0]
      n_pg                = dic['n_pg'][ind_el]
      freq_re             = np.real(dic['freq'])[ind_el]
      freq_im             = np.imag(dic['freq'])[ind_el]

      ind_p               = np.where(n_pg >= 0)[0]
      ind_g               = np.where(n_pg < 0)[0]
      ind_excited_p       = np.where(freq_im[ind_p] <= 0)[0]
      ind_excited_g       = np.where(freq_im[ind_g] <= 0)[0]
      if Xc < 0.01: continue
      n_excited_p[i_dic]  = len(ind_excited_p)
      n_excited_g[i_dic]  = len(ind_excited_g)

      ########################################
      # Setup your reference x- and y-axis values
      ########################################
      xvals  = log_Teff_arr
      if style == 'kiel':
        yvals = log_g_arr
      elif style == 'hrd':
        yvals = log_L_arr
      elif style == 'shrd':
        yvals = 4 * log_Teff_arr - log_g_arr
      else:
        logging.error('plot_gyre: contour_instability_strips: there is another issue with style') 
        raise SystemExit, 'Error: plot_gyre: contour_instability_strips: there is another issue with style'

    return (xvals, yvals, n_excited_p, n_excited_g)


  ref_tuple     = give_what_is_needed(dics=list_dic, style=style)
  ref_xvals     = ref_tuple[0]
  ref_yvals     = ref_tuple[1]
  ref_excited_p = ref_tuple[2]
  ref_excited_g = ref_tuple[3]
  ref_max_p     = np.max(ref_excited_p)
  ref_max_g     = np.max(ref_excited_g)
  print ' - contour_instability_strips: max num excited modes: p={0}, g={1}'.format(ref_max_p, ref_max_g)

  if add_comp:
    comp_tuple     = give_what_is_needed(dics=list_dic_comp, style=style)
    comp_xvals     = comp_tuple[0]
    comp_yvals     = comp_tuple[1]
    comp_excited_p = comp_tuple[2]
    comp_excited_g = comp_tuple[3]
    comp_max_p     = np.max(comp_excited_p)
    comp_max_g     = np.max(comp_excited_g)
    print ' - contour_instability_strips: max num excited modes: p={0}, g={1}'.format(comp_max_p, comp_max_g)

  ########################################
  # Mask bad values to show them transparently
  ########################################
  ind_pure_p    = np.where((ref_excited_p > 0) & (ref_excited_g == 0))[0]
  if el > 0:
    ind_pure_g  = np.where((ref_excited_g > 0) & (ref_excited_p == 0))[0]
    ind_hybrid  = np.where((ref_excited_p > 0) & (ref_excited_g > 0))[0]

  ref_pure_p    = ref_excited_p[ind_pure_p]
  ref_max_h     = 0   # initialize to zero
  if el > 0:
    ref_pure_g  = ref_excited_g[ind_pure_g]
    ref_hybrid  = ref_excited_p[ind_hybrid] + ref_excited_g[ind_hybrid]
    ref_max_h   = np.max(ref_hybrid)

  ref_pure_p_x  = ref_xvals[ind_pure_p]
  ref_pure_p_y  = ref_yvals[ind_pure_p]
  if el > 0:
    ref_pure_g_x  = ref_xvals[ind_pure_g]
    ref_pure_g_y  = ref_yvals[ind_pure_g]
    ref_hybrid_x  = ref_xvals[ind_hybrid]
    ref_hybrid_y  = ref_yvals[ind_hybrid]

  # ref_excited_p = np.ma.masked_where(ref_excited_p==0, ref_excited_p)
  # ref_excited_g = np.ma.masked_where(ref_excited_g==0, ref_excited_g)

  ########################################
  # Create the plot
  ########################################
  from matplotlib.colors import BoundaryNorm
  from matplotlib.ticker import MaxNLocator
  # fig, ax = plt.subplots(1, figsize=(6,4), dpi=300)
  fig = plt.figure(figsize=(6,4), dpi=600)
  ax  = fig.add_axes([0.09, 0.11, 0.81, 0.86])
  # plt.subplots_adjust(left=0.11, right=0.98, bottom=0.11, top=0.97)

  # lower_level = -5
  lev_factor  = 0.33
  low_lev_p   = -lev_factor * ref_max_p
  levels_p    = MaxNLocator(nbins=ref_max_p).tick_values(low_lev_p, ref_max_p)
  if el > 0:
    low_lev_g = -lev_factor * ref_max_g
    low_lev_h = -lev_factor * ref_max_h
    levels_g  = MaxNLocator(nbins=ref_max_g).tick_values(low_lev_g, ref_max_g)
    levels_h  = MaxNLocator(nbins=ref_max_h).tick_values(low_lev_h, ref_max_h)

  cmap_p      = plt.get_cmap('Reds') 
  norm_p      = BoundaryNorm(levels_p, ncolors=cmap_p.N, clip=True)
  if el > 0:
    cmap_g    = plt.get_cmap('Blues')
    norm_g    = BoundaryNorm(levels_g, ncolors=cmap_g.N, clip=True)
    cmap_h    = plt.get_cmap('Greys')
    norm_h    = BoundaryNorm(levels_h, ncolors=cmap_h.N, clip=True)

  # cmap_p.set_under('w', alpha=0)
  # cmap_g.set_under('w', alpha=0)
  fig.patch.set_facecolor('none')
  fig.patch.set_alpha(0.0)
  ax.patch.set_facecolor('none')
  ax.patch.set_alpha(0.0)

  ########################################
  # ax.scatter(ref_xvals, ref_yvals, marker='.', s=1, color='0.9', zorder=1) 
  # Pure p-modes
  sc_p  = ax.scatter(ref_pure_p_x, ref_pure_p_y, c=ref_pure_p, s=6, 
                     cmap=cmap_p, vmin=low_lev_p, vmax=ref_max_p, 
                     marker='s', edgecolor='none')
  ax_p  = fig.add_axes([0.91, 0.71, 0.02, 0.26])
  tvl_p = range(1, ref_max_p + 1)   # ticks, values and ticklabels for p-modes
  cb_p  = plt.colorbar(sc_p, cax=ax_p, format='%d', values=tvl_p) #, ticks=tvl_p)
  cb_p.set_ticks(tvl_p)
  cb_p.set_ticklabels(tvl_p, update_ticks=True)
  cb_p.set_label(r'Pure Excited p-modes', fontsize=8)
  cb_p.ax.tick_params(labelsize=10) 
  cb_p.update_ticks()


  if el > 0 and ref_max_g > 0:
    # Pure g-modes
    sc_g  = ax.scatter(ref_pure_g_x, ref_pure_g_y, c=ref_pure_g, s=6, 
                       cmap=cmap_g, vmin=1, vmax=ref_max_g, 
                       marker='s', edgecolor='none')
    ax_g  = fig.add_axes([0.91, 0.11, 0.02, 0.26])
    tvl_g = range(1, ref_max_g + 1)   # ticks, values and ticklabels for g-modes
    cb_g  = plt.colorbar(sc_g, cax=ax_g, format='%d', values=tvl_g)
    cb_g.set_ticks(tvl_g)
    cb_g.set_ticklabels(tvl_g, update_ticks=True)
    cb_g.set_label(r'Pure Excited g-modes', fontsize=8)
    cb_g.ax.tick_params(labelsize=10) 


  if el > 0 and ref_max_h > 0:
    # Hybrids
    sc_h  = ax.scatter(ref_hybrid_x, ref_hybrid_y, c=ref_hybrid, s=6, 
                       cmap=cmap_h, vmin=1, vmax=ref_max_h, 
                       marker='s', edgecolor='none', alpha=0.5)
    ax_h  = fig.add_axes([0.91, 0.41, 0.02, 0.26])
    tvl_h = range(1, ref_max_h + 1)   # ticks, values and ticklabels for hybrids
    cb_h  = plt.colorbar(sc_h, cax=ax_h, format='%d', values=tvl_h)
    cb_h.set_ticks(tvl_h)
    cb_h.set_ticklabels(tvl_h, update_ticks=True)
    cb_h.set_label(r'Excited Hybrids', fontsize=8)
    cb_h.ax.tick_params(labelsize=10) 

  ########################################
  from matplotlib.mlab import griddata
  n_pts = 201 
  if False:
    x_p   = np.linspace(np.min(ref_xvals), np.max(ref_xvals), n_pts, endpoint=True)
    y_p   = np.linspace(np.min(ref_yvals), np.max(ref_yvals), n_pts, endpoint=True)
    z_p   = griddata(ref_xvals, ref_yvals, ref_excited_p, x_p, y_p, interp='nn')
    c_p   = ax.contour(x_p, y_p, z_p, [1], linewidths=0.5, colors='red', linestyles='dashed')
    cf_p  = ax.contourf(x_p, y_p, z_p, range(1,ref_max_p+1), cmap=cmap_p, norm=norm_p, alpha=1)
    ax_p  = fig.add_axes([0.91, 0.56, 0.02, 0.41])
    cb_p  = plt.colorbar(cf_p, cax=ax_p)
    cb_p.set_label(r'Num. excited p-modes', fontsize=8)

    if el > 0:
      x_g   = np.linspace(np.min(ref_xvals), np.max(ref_xvals), n_pts, endpoint=True)
      y_g   = np.linspace(np.min(ref_yvals), np.max(ref_yvals), n_pts, endpoint=True)
      z_g   = griddata(ref_xvals, ref_yvals, ref_excited_g, x_g, y_g, interp='nn')
      c_g   = ax.contour(x_g, y_g, z_g, [1], linewidths=0.5, colors='blue', linestyles='solid')
      cf_g  = ax.contourf(x_g, y_g, z_g, range(1,ref_max_g+1), cmap=cmap_g, norm=norm_g, alpha=0.5)
      ax_g  = fig.add_axes([0.91, 0.11, 0.02, 0.41])
      cb_g  = plt.colorbar(cf_g, cax=ax_g)
      cb_g.set_label(r'Num. excited g-modes', fontsize=8)

  ########################################
  # Annotations, and Legend
  ########################################
  if list_dic_annot: pcm.add_annotation(ax, list_dic_annot)

  # Fake labels
  ax.plot([], [], linestyle='solid', color='blue', lw=2, label='g-modes')
  ax.plot([], [], linestyle='dashed', color='red', lw=2, label='p-modes')
  # leg = ax.legend(loc=1, fontsize=10, fancybox=False, shadow=False, frameon=False, framealpha=0)

  ########################################
  # Optionally add few selected tracks on 
  # top of the instability strips
  ########################################
  if add_hist:
    for i_dic, dic in enumerate(list_hist):
      header = dic['header']
      M_ini  = header['initial_mass'][0]
      hist   = dic['hist']
      mass   = hist['star_mass']
      log_g  = hist['log_g']
      mxg    = np.argmax(log_g)
      x_hist = hist['log_Teff']
      if style == 'kiel': y_hist = hist['log_g']
      if style == 'hrd':  y_hist = hist['log_L']
      if style == 'shrd': y_hist = 4*hist['log_Teff'] - hist['log_g']
      x_hist = x_hist[mxg : ]
      y_hist = y_hist[mxg : ]

      ax.plot(x_hist, y_hist, linestyle='solid', color='black', lw=0.5, zorder=2)
      txt    = r'{0:0.1f}'.format(M_ini) + r'\,M$_\odot$'
      ax.annotate(txt, xy=(x_hist[0], y_hist[0]+0.01), xycoords='data', color='black', 
                  ha='left', va='top', fontsize=8)

  ########################################
  # Finalize and save the plot
  ########################################
  ax.set_xlim(logT_from, logT_to)
  ax.set_ylim(yaxis_from, yaxis_to)
  ax.set_xlabel(r'Effective Temperature $\log T_{\rm eff}$ [K]')
  if style == 'kiel': ytitle = r'Surface gravity  $\,\log g$ [cm$^2$ sec$^{-1}$]'
  if style == 'hrd':  ytitle = r'Surface Luminosity  $\,\log(L/L_\odot)$'
  if style == 'shrd': ytitle = r'$\log \mathcal{ L/L}_\odot$; $\mathcal{L}=T_{\rm eff}^4/g$'
  ax.set_ylabel(ytitle)

  plt.savefig(file_out, dpi=600, transparent=True, facecolor=fig.get_facecolor(), edgecolor='none')
  # plt.savefig(file_out, dpi=600)
  print ' - plot_gyre: contour_instability_strips: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def plot_compare_freq_diff(list_files, ref_indx=0, only_el=None, list_lbls=None, by_ng=False, by_P=False, by_freq=False,
                      ng_from=0, ng_to=-100, P_from=0.1, P_to=10, freq_from=0, freq_to=100, diff_f_from=-5,
                      diff_f_to=+5, dP_from=6000, dP_to=16000, diff_dP_from=-0.5, diff_dP_to=0.5, leg_loc=None, freq_unit='Hz',
                      file_out=None):
  """
  This is a wrapper for plot_compare_g_freq_diff, so that ancient routines/scripts still function well.
  For the meaning of different parameters and their meanings see plot_compare_g_freq_diff()
  """
  plot_compare_g_freq_diff(list_files=list_files, ref_indx=ref_indx, only_el=only_el, list_lbls=list_lbls,
                           by_ng=by_ng, by_P=by_P, by_freq=by_freq, ng_from=ng_from, ng_to=ng_to, P_from=P_from,
                           P_to=P_to, freq_from=freq_from, freq_to=freq_to, diff_f_from=diff_f_from, diff_f_to=diff_f_to,
                           dP_from=dP_from, dP_to=dP_to, diff_dP_from=diff_dP_from, diff_dP_to=diff_dP_to, leg_loc=leg_loc,
                           freq_unit=freq_unit, file_out=file_out)
  return None
#=================================================================================================================
def plot_compare_g_freq_diff(list_files, ref_indx=0, only_el=None, list_lbls=None, dic_star=None, by_ng=False, by_P=False,
                             by_freq=False, ng_from=0, ng_to=-100, P_from=0.1, P_to=10, freq_from=0, freq_to=100,
                             diff_f_from=-5, diff_f_to=+5, dP_from=6000, dP_to=16000, diff_dP_from=-0.5, diff_dP_to=0.5,
                             leg_loc=None, freq_unit='Hz', file_out=None):
  """
  This function receive a list of gyre h5 files, and makes a multi-panel plot, which compares the frequency of "g-modes" of
  every individidual file with respect to the reference file. The reference file is the one specified with ref_indx
  @param list_files: list of string, giving full path to the GYRE output files
  @type list_files: list of strings
  @param ref_indx: index of the reference file in the input list_files; default=0
  @type ref_indx: integer
  @param only_el: restrict this study only to this el value, otherwise, all available el will be considered
  @type only_el: integer
  @param list_lbls: list of LaTeX compatible plotting labels for every individual item in the input list_files
  @type list_lbls: list of strings
  @param dic_star: dictionary containing observed parameters of the star, as returned e.g. by mesa_gyre.stars()
  @type dic_star: dictionary
  @param by_ng, by_P, by_freq: The choice of x-axis
  @type by_ng, by_P, by_freq: boolean
  @param dP_from, dP_to: dP range in the top panel
  @type dP_from, dP_to: float
  @param leg_loc: location of the legend in the file as defined by matplotlib.pyplot.legend() documentation
       if None, then it is set to 3 (i.e. bottom left) by default
  @type leg_loc: integer
  @param freq_unit: default=Hz; You better specify exactly the unit of the frequencies listed in the GYRE output file
  @type freq_unit: string
  @param file_out: full path to the output figure file in pdf format
  @type file_out: string
  @return: None
  @rtype: None
  """
  list_flags = [by_ng, by_freq, by_P]
  if not any(list_flags):
    message = 'Error: plot_gyre: plot_compare_g_freq_diff: at least one of x-axis flags must be True.'
    raise SystemExit, message

  n_h5 = len(list_files)

  fig = plt.figure(figsize=(6,4), dpi=200)
  plt.subplots_adjust(left=0.13, right=0.98, top=0.98, bottom=0.11, wspace=0.04, hspace=0.04)

  dic_conv = cm.conversions()
  Hz_to_cd = dic_conv['Hz_to_cd']
  uHz_to_Hz = dic_conv['uHz_to_Hz']
  d_to_sec  = dic_conv['d_to_sec']
  sec_to_d  = dic_conv['sec_to_d']

  if freq_unit == 'Hz': factor = Hz_to_cd
  elif freq_unit == 'uHz': factor = uHz_to_Hz * Hz_to_cd

  if by_ng: xlim = (ng_from, ng_to); xtitle = r'Radial Order $n_g$'
  elif by_P: xlim = (P_from, P_to);  xtitle = r'Period [d]'
  elif by_freq: xlim = (freq_from, freq_to); xtitle = r'Frequency [d$^{-1}$]'
  else: raise SystemExit, 'Error: plot_gyre: plot_compare_g_freq_diff: set one of by_ng, by_P or by_freq to True.'

  if not only_el: only_el = 1   # default to dipole modes
  str_el = str(only_el)

  key_el_n_gm = 'n_el_' + str_el + '_gm'
  key_el_freq_re = 'el_' + str_el + '_gm_freq_re'
  key_el_n_pg = 'el_' + str_el + '_gm_npg'
  key_el_dP = 'el_' + str_el + '_gm_dP'
  key_el_P  = 'el_' + str_el + '_gm_P'

  ref_file = list_files[ref_indx]
  ref_dic, ref_ad = gyre.read_output(ref_file)
  for name in ref_ad.dtype.names: ref_dic[name] = ref_ad[name]
  ref_dic_dP = per.gen_dP(ref_dic, freq_unit=freq_unit)

  # check if el = 1 is found in dic_dP:
  if not ref_dic_dP.has_key('n_el_1'):
    message = ' - Error: plot_gyre: plot_compare_g_freq_diff: el=1 is not in dic_dP. \n'
    raise SystemExit, message

  ref_n_gm = ref_dic_dP[key_el_n_gm]
  ref_freq = ref_dic_dP[key_el_freq_re]
  ref_n_pg = ref_dic_dP[key_el_n_pg]
  ref_dP   = ref_dic_dP[key_el_dP]
  ref_P    = ref_dic_dP[key_el_P]

  # Put the dP vs. n_pg plot for the reference file on the top panel
  if by_ng: ref_xaxis = ref_n_pg
  if by_freq: ref_xaxis = ref_freq
  if by_P: ref_xaxis = ref_P * sec_to_d
  ax0 = fig.add_subplot(3,1,1)
  ax1 = fig.add_subplot(3,1,2)
  ax2 = fig.add_subplot(3,1,3)

  min_col = 0; max_col = n_h5
  cmap = plt.get_cmap('autumn')
  norm = mpl.colors.Normalize(vmin=min_col, vmax=max_col)
  # colr = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)
  colr = pcm.get_major_colors_iterator()

  import itertools
  markers = itertools.cycle('s*>p1HD^,d')
  linestyles = itertools.cycle(['dashed', 'dotted', 'dashdot'])
  list_mk = []; list_ls = []; list_cl = []

  # Optionally, add the observed perod spacing pattern of a star in the background of panel a
  if dic_star is not None:
    from mesa_gyre import stars
    dic_star       = stars.list_freq_to_list_dP(dic_star, freq_unit=freq_unit)
    list_dic_freq  = dic_star['list_dic_freq']
    obs_P          = np.array( [dic['P'] * sec_to_d for dic in list_dic_freq] )
    obs_P_err      = np.array( [dic['P_err'] * sec_to_d for dic in list_dic_freq] )
    obs_dP         = np.array( [dic['dP'] for dic in list_dic_freq] )
    obs_dP_err     = np.array( [dic['dP_err'] for dic in list_dic_freq] )
    ax0.errorbar(obs_P[:-1], obs_dP[:-1], xerr=obs_P_err[:-1], yerr=obs_dP_err[:-1], fmt=None,
                ecolor='grey', elinewidth=1, capsize=3, barsabove=True, capthick=1, marker='s',
                label=r'Observed', zorder=0)
    #ax0.scatter(obs_P[:-1]/1e3, obs_dP[:-1], edgecolor='black', facecolor='white', s=25, label=r'Observed')
    ax0.plot(obs_P[:-1], obs_dP[:-1], linestyle='solid', color='grey', lw=1)


  # Optionally, add the expected relative frequency precision from Kepler, K2, CoRoT and BRITE.
  if True:
    dT_Kepler = 1400. * d_to_sec
    dT_K2     = 80.   * d_to_sec
    dT_CoRoT  = 150.  * d_to_sec
    dT_BRITE  = 180.  * d_to_sec
    df_Kepler = 1.5/dT_Kepler   # Based on  Loumos, G. L. & Deeming, T. J. 1978, Ap&SS, 56, 285
    df_K2     = 1.5/dT_K2
    df_CoRoT  = 1.5/dT_CoRoT
    df_BRITE  = 1.5/dT_BRITE
    rel_df_Kepler = df_Kepler / ref_freq * 100.
    rel_df_K2     = df_K2     / ref_freq * 100.
    rel_df_CoRoT  = df_CoRoT  / ref_freq * 100.
    rel_df_BRITE  = df_BRITE  / ref_freq * 100.

    #print ' - df for Kepler={0}, CoRoT={1} and K2={2}'.format(df_Kepler, df_CoRoT, df_K2)

    ax1.fill_between(ref_xaxis, rel_df_Kepler, -rel_df_Kepler, color='blue', alpha=1.0, zorder=1)
    #ax1.fill_between(ref_xaxis, rel_df_BRITE, -rel_df_BRITE, color='0.5', alpha=0.4, zorder=1)
    ax1.fill_between(ref_xaxis, rel_df_CoRoT, -rel_df_CoRoT, color='0.5', alpha=0.4, zorder=1)
    ax1.fill_between(ref_xaxis, rel_df_K2, -rel_df_K2, color='0.8', alpha=0.6, zorder=1)

    ax1.annotate(r'K2', xy=(0.90, 0.08), xycoords='axes fraction', color='black', fontsize='small')
    ax1.annotate(r'CoRoT', xy=(0.90, 0.30), xycoords='axes fraction', color='black', fontsize='x-small')

  ax0.set_xlabel(xtitle)
  ax0.set_ylabel(r'$\Delta P$ [sec]')
  ax0.set_xlim(xlim)
  ax0.set_xticklabels(())
  ax0.set_ylim(dP_from, dP_to)

  list_dic = []  # collect dP dictionaries
  list_dP  = []; list_P = []; list_freq = []; list_n_pg = []; list_num_modes = []
  list_delta_freq = []
  for i in range(n_h5):
     file = list_files[i]
     if not os.path.isfile(file):
       print ' - Warning: input file %s does not exist!' % (file, )
       continue

     dic, ad = gyre.read_output(file)
     for name in ad.dtype.names: dic[name] = ad[name]

     dic_dP = per.gen_dP(dic, freq_unit=freq_unit)
     list_dic.append(dic_dP)

     freq = dic_dP[key_el_freq_re]
     list_freq.append(freq)
     #
     num_ad_modes = dic_dP[key_el_n_gm]
     list_num_modes.append(num_ad_modes)
     #
     dP = dic_dP[key_el_dP]
     list_dP.append(dP)
     #
     n_pg = dic_dP[key_el_n_pg]
     list_n_pg.append(n_pg)
     #
     Per = dic_dP[key_el_P]
     list_P.append(Per)

     # to store delta_freq for this file w.r.t. to the reference, define this numpy array:
     delta_freq = np.ones(num_ad_modes)
     delta_dP   = np.ones(num_ad_modes-1)
     #list_delta_freq.append(delta_freq)

     # Plot the whole things that are gonna be compared to the reference
     if by_ng: xaxis = n_pg
     if by_freq: xaxis = freq
     if by_P: xaxis = Per * sec_to_d

     if i == ref_indx:
       list_ls.append('solid')
       list_mk.append('o')
       list_cl.append('black')
       #ax0.plot(ref_xaxis[:ref_n_gm-1], ref_dP, color='black', label=r'Ref: '+list_lbls[ref_indx])
       ax0.plot(ref_xaxis[:ref_n_gm-1], ref_dP, color='black', linestyle='solid')
       ax0.scatter(ref_xaxis[:ref_n_gm-1], ref_dP, color='black', s=16, marker='o', label=list_lbls[ref_indx])
     else:
       ls = linestyles.next(); list_ls.append(ls)
       mk = markers.next(); list_mk.append(mk)
       # cl = colr.to_rgba(i); list_cl.append(cl)
       cl = colr.next()
       if cl == 'black': cl = colr.next()
       list_cl.append(cl)
       ax0.plot(xaxis[:num_ad_modes-1], dP, color=cl, linestyle=ls)
       ax0.scatter(xaxis[:num_ad_modes-1], dP, color=cl, s=9, marker=mk, label=list_lbls[i])

     # Walk through all ref_n_pg, and compare one by one with ref
     list_absent_n_pg = []
     for i_ref, ref_one_n_pg in enumerate(ref_n_pg):
       ref_one_freq = ref_freq[i_ref]
       if ref_one_n_pg in n_pg:
         ind_match_n_pg = [ind for ind in range(num_ad_modes) if ref_one_n_pg == n_pg[ind]][0]
         match_freq = freq[ind_match_n_pg]
         delta_freq[ind_match_n_pg] = (match_freq - ref_one_freq) / ref_one_freq
         condition_1 = i_ref <= ref_n_gm - 2
         condition_2 = i_ref <= num_ad_modes - 1
         condition_3 = ind_match_n_pg <= num_ad_modes - 2
         if all([condition_1, condition_2, condition_3]):
           ref_one_dP   = ref_dP[i_ref]
           match_dP = dP[ind_match_n_pg]
           delta_dP[ind_match_n_pg]   = (match_dP - ref_one_dP) / ref_one_dP
       else:
         list_absent_n_pg.append({'n_pg':ref_one_n_pg, 'filename':file})

     #if len(list_absent_n_pg) <= 5:
       #for i, dic_absent in enumerate(list_absent_n_pg):
         #print 'Warning: plot_gyre: plot_compare_g_freq_diff: n_pg: %s in file: %s' % (dic_absent['n_pg'], dic_absent['filename'])
     #else:
       #print 'Warning: plot_gyre: plot_compare_g_freq_diff: %s orders(i.e. n_pg) not found.' % (len(dic_absent),)

     # now append the delta_freq to a list
     list_delta_freq.append(delta_freq)

     ls = list_ls[i]
     mk = list_mk[i]
     cl = list_cl[i]

     if i == ref_indx:
       ax1.plot(ref_xaxis, delta_freq * 100., color=cl, linestyle=ls)
       ax1.scatter(ref_xaxis, delta_freq * 100., marker=mk, s=16, color=cl)
       ax1.set_xlim(xlim)
       ax1.set_ylabel(r'$\delta f$ [$\%$]')
       ax1.set_ylim(diff_f_from, diff_f_to)
       txt = r'$\delta f = (f_i - f_{\rm ref})/f_{\rm ref}$'
       #ax1.annotate(txt, xy=(0.05, 0.80), xycoords='axes fraction')
       ax1.set_xticklabels(())
     else:
       ax1.plot(xaxis, delta_freq * 100., color=cl, linestyle=ls)
       ax1.scatter(xaxis, delta_freq * 100., color=cl, linestyle=ls, marker=mk, s=9)

     if i == ref_indx:
       ax2.plot(xaxis[:len(dP)], delta_dP * 100., color=cl, linestyle=ls)
       ax2.scatter(xaxis[:len(dP)], delta_dP * 100., color=cl, marker=mk, s=16)
       ax2.set_xlabel(xtitle)
       ax2.set_xlim(xlim)
       ax2.set_ylabel(r'$\delta(\Delta P) \, [\%]$')
       ax2.set_ylim(diff_dP_from, diff_dP_to)
       txt = r'$\delta (\Delta P) = (\Delta P_i - \Delta P_{\rm ref})/\Delta P_{\rm ref}$'
       #ax2.annotate(txt, xy=(0.05, 0.05), xycoords='axes fraction')
     else:
       ax2.plot(xaxis[:len(dP)], delta_dP * 100., color=cl, linestyle=ls)
       ax2.scatter(xaxis[:len(dP)], delta_dP * 100., color=cl, marker=mk, linestyle=ls, s=9)

  if not leg_loc: leg_loc=3
  leg0 = ax0.legend(loc=leg_loc, shadow=True, fontsize='x-small', scatterpoints=1)
  leg0.get_frame().set_alpha(0.50)

  if file_out:
    pdf_file = file_out
  else:
    if not os.path.exists('plots/'): os.path.makedirs('plots/')
    pdf_file = 'plots/Comp-g-freq-diff.pdf'
  plt.savefig(pdf_file)
  print ' - plot_compare_g_freq_diff: %s created!' % (pdf_file, )
  plt.close()

  return None

#=================================================================================================================
def plot_compare_p_freq_diff(list_files, ref_indx=0, only_el=None, list_lbls=None, by_ng=False, by_P=False, by_freq=False,
                      ng_from=0, ng_to=-100, P_from=0.1, P_to=10, freq_from=0, freq_to=100, diff_df_from=0, diff_df_to=1000,
                      diff_f_from=-5, diff_f_to=+5, df_from=6000, df_to=16000, leg_loc=None, freq_unit='Hz', file_out=None):
  """
  This function receive a list of gyre h5 files, and makes a multi-panel plot, which compares the frequency of "p-modes" of
  every individidual file with respect to the reference file. The reference file is the one specified with ref_indx
  @param list_files: list of string, giving full path to the GYRE output files
  @type list_files: list of strings
  @param ref_indx: index of the reference file in the input list_files; default=0
  @type ref_indx: integer
  @param only_el: restrict this study only to this el value, otherwise, all available el will be considered
  @type only_el: integer
  @param list_lbls: list of LaTeX compatible plotting labels for every individual item in the input list_files
  @type list_lbls: list of strings
  @param by_ng, by_P, by_freq: The choice of x-axis
  @type by_ng, by_P, by_freq: boolean
  @param df_from, df_to: df range in the top panel in muHz
  @type df_from, df_to: float
  @param leg_loc: location of the legend in the file as defined by matplotlib.pyplot.legend() documentation
       if None, then it is set to 3 (i.e. bottom left) by default
  @type leg_loc: integer
  @param freq_unit: default=Hz; You better specify exactly the unit of the frequencies listed in the GYRE output file
  @type freq_unit: string
  @param file_out: full path to the output figure file in pdf format
  @type file_out: string
  @return: None
  @rtype: None
  """
  list_flags = [by_ng, by_freq, by_P]
  if not any(list_flags):
    message = 'Error: plot_gyre: plot_compare_g_freq_diff: at least one of x-axis flags must be True.'
    raise SystemExit, message

  n_h5 = len(list_files)

  fig = plt.figure(figsize=(6,4), dpi=200)
  plt.subplots_adjust(left=0.13, right=0.98, top=0.98, bottom=0.11, wspace=0.04, hspace=0.04)

  dic_conv = cm.conversions()
  Hz_to_cd = dic_conv['Hz_to_cd']
  uHz_to_Hz = dic_conv['uHz_to_Hz']
  d_to_sec  = dic_conv['d_to_sec']
  sec_to_d  = dic_conv['sec_to_d']

  if freq_unit == 'Hz': factor = Hz_to_cd
  elif freq_unit == 'uHz': factor = uHz_to_Hz * Hz_to_cd

  if by_ng: xlim = (ng_from, ng_to); xtitle = r'Radial Order $n_g$'
  elif by_P: xlim = (P_from, P_to);  xtitle = r'Period [d$^{-1}$]'
  elif by_freq: xlim = (freq_from, freq_to); xtitle = r'Frequency [d$^{-1}$]'
  else: raise SystemExit, 'Error: plot_gyre: plot_compare_g_freq_diff: set one of by_ng, by_P or by_freq to True.'

  if not only_el: only_el = 1   # default to dipole modes
  str_el = str(only_el)

  key_el_n_pm = 'n_el_' + str_el + '_pm'
  key_el_freq_re = 'el_' + str_el + '_pm_freq_re'
  key_el_n_pg = 'el_' + str_el + '_pm_npg'
  key_el_df = 'el_' + str_el + '_pm_df'

  ref_file = list_files[ref_indx]
  ref_dic, ref_ad = gyre.read_output(ref_file)
  for name in ref_ad.dtype.names: ref_dic[name] = ref_ad[name]
  ref_dic_df = fresp.gen_df(ref_dic, freq_unit=freq_unit)

  # check if el = 1 is found in dic_df:
  if not ref_dic_df.has_key('n_el_1'):
    message = ' - Error: plot_gyre: plot_compare_g_freq_diff: el=1 is not in dic_df. \n'
    raise SystemExit, message

  ref_n_pm = ref_dic_df[key_el_n_pm]
  ref_freq = ref_dic_df[key_el_freq_re]
  ref_n_pg = ref_dic_df[key_el_n_pg]
  ref_df   = ref_dic_df[key_el_df]
  ref_P    = 1./ref_freq

  # Put the df vs. n_pg plot for the reference file on the top panel
  if by_ng: ref_xaxis = ref_n_pg
  if by_freq: ref_xaxis = ref_freq
  if by_P: ref_xaxis = ref_P * sec_to_d
  ax0 = fig.add_subplot(3,1,1)
  ax1 = fig.add_subplot(3,1,2)
  ax2 = fig.add_subplot(3,1,3)

  min_col = 0; max_col = n_h5
  cmap = plt.get_cmap('gnuplot2')
  norm = mpl.colors.Normalize(vmin=min_col, vmax=max_col)
  colr = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)

  import itertools
  markers = itertools.cycle('s*>p1HD^,d')
  linestyles = itertools.cycle(['dashed', 'dotted', 'dashdot'])
  list_mk = []; list_ls = []; list_cl = []

  # Optionally, add the expected relative frequency precision from Kepler, K2, CoRoT and BRITE.
  if True:
    dT_Kepler = 1400. * d_to_sec
    dT_K2     = 80.   * d_to_sec
    dT_CoRoT  = 150.  * d_to_sec
    dT_BRITE  = 180.  * d_to_sec
    df_Kepler = 1.5/dT_Kepler   # Based on  Loumos, G. L. & Deeming, T. J. 1978, Ap&SS, 56, 285
    df_K2     = 1.5/dT_K2
    df_CoRoT  = 1.5/dT_CoRoT
    df_BRITE  = 1.5/dT_BRITE
    rel_df_Kepler = df_Kepler / ref_freq * 100.
    rel_df_K2     = df_K2     / ref_freq * 100.
    rel_df_CoRoT  = df_CoRoT  / ref_freq * 100.
    rel_df_BRITE  = df_BRITE  / ref_freq * 100.

    #print ' - df for Kepler={0}, CoRoT={1} and K2={2}'.format(df_Kepler, df_CoRoT, df_K2)

    ax1.fill_between(ref_xaxis, rel_df_Kepler, -rel_df_Kepler, color='blue', alpha=1.0, zorder=1)
    #ax1.fill_between(ref_xaxis, rel_df_BRITE, -rel_df_BRITE, color='0.5', alpha=0.4, zorder=1)
    ax1.fill_between(ref_xaxis, rel_df_CoRoT, -rel_df_CoRoT, color='0.5', alpha=0.4, zorder=1)
    ax1.fill_between(ref_xaxis, rel_df_K2, -rel_df_K2, color='0.8', alpha=0.6, zorder=1)

    ax1.annotate(r'K2', xy=(0.90, 0.80), xycoords='axes fraction', color='black', fontsize='small')
    ax1.annotate(r'CoRoT', xy=(0.90, 0.60), xycoords='axes fraction', color='black', fontsize='x-small')

  ax0.set_xlabel(xtitle)
  ax0.set_ylabel(r'$\Delta f$ [$\mu$Hz]')
  ax0.set_xlim(xlim)
  ax0.set_xticklabels(())
  ax0.set_ylim(df_from, df_to)

  list_dic = []  # collect df dictionaries
  list_df  = []; list_P = []; list_freq = []; list_n_pg = []; list_num_modes = []
  list_delta_freq = []
  for i in range(n_h5):
     file = list_files[i]
     if not os.path.isfile(file):
       print ' - Warning: input file %s does not exist!' % (file, )
       continue

     dic, ad = gyre.read_output(file)
     for name in ad.dtype.names: dic[name] = ad[name]

     dic_df = fresp.gen_df(dic, freq_unit=freq_unit)
     list_dic.append(dic_df)

     freq = dic_df[key_el_freq_re]
     list_freq.append(freq)
     #
     num_ad_modes = dic_df[key_el_n_pm]
     list_num_modes.append(num_ad_modes)
     #
     df = dic_df[key_el_df]
     list_df.append(df)
     #
     n_pg = dic_df[key_el_n_pg]
     list_n_pg.append(n_pg)
     #
     Per = 1./freq
     list_P.append(Per)

     # to store delta_freq for this file w.r.t. to the reference, define this numpy array:
     delta_freq = np.ones(num_ad_modes)
     delta_df   = np.ones(num_ad_modes-1)
     #list_delta_freq.append(delta_freq)

     # Plot the whole things that are gonna be compared to the reference
     if by_ng: xaxis = n_pg
     if by_freq: xaxis = freq
     if by_P: xaxis = Per * sec_to_d

     if i == ref_indx:
       list_ls.append('solid')
       list_mk.append('o')
       list_cl.append('black')
       #ax0.plot(ref_xaxis[:ref_n_pm-1], ref_df*1e6, color='black', label=r'Ref: '+list_lbls[ref_indx])
       ax0.plot(ref_xaxis[:ref_n_pm-1], ref_df*1e6, color='black', linestyle='solid')
       ax0.scatter(ref_xaxis[:ref_n_pm-1], ref_df*1e6, color='black', s=16, marker='o', label=list_lbls[ref_indx])
     else:
       ls = linestyles.next(); list_ls.append(ls)
       mk = markers.next(); list_mk.append(mk)
       cl = colr.to_rgba(i); list_cl.append(cl)
       ax0.plot(xaxis[:num_ad_modes-1], df*1e6, color=cl, linestyle=ls)
       ax0.scatter(xaxis[:num_ad_modes-1], df*1e6, color=cl, s=9, marker=mk, label=list_lbls[i])

     # Walk through all ref_n_pg, and compare one by one with ref
     list_absent_n_pg = []
     for i_ref, ref_one_n_pg in enumerate(ref_n_pg):
       ref_one_freq = ref_freq[i_ref]
       if ref_one_n_pg in n_pg:
         ind_match_n_pg = [ind for ind in range(num_ad_modes) if ref_one_n_pg == n_pg[ind]][0]
         match_freq = freq[ind_match_n_pg]
         delta_freq[ind_match_n_pg] = (match_freq - ref_one_freq) / ref_one_freq
         condition_1 = i_ref <= ref_n_pm - 2
         #condition_2 = i_ref <= num_ad_modes - 1
         condition_3 = ind_match_n_pg <= num_ad_modes - 2
         #if all([condition_1, condition_2, condition_3]):
         if all([condition_1, condition_3]):
           ref_one_df   = ref_df[i_ref]
           match_df     = df[ind_match_n_pg]
           delta_df[ind_match_n_pg]   = (match_df - ref_one_df) / ref_one_df
       else:
         list_absent_n_pg.append({'n_pg':ref_one_n_pg, 'filename':file})

     #if len(list_absent_n_pg) <= 5:
       #for i, dic_absent in enumerate(list_absent_n_pg):
         #print 'Warning: plot_gyre: plot_compare_g_freq_diff: n_pg: %s in file: %s' % (dic_absent['n_pg'], dic_absent['filename'])
     #else:
       #print 'Warning: plot_gyre: plot_compare_g_freq_diff: %s orders(i.e. n_pg) not found.' % (len(dic_absent),)

     # now append the delta_freq to a list
     list_delta_freq.append(delta_freq)

     ls = list_ls[i]
     mk = list_mk[i]
     cl = list_cl[i]

     if i == ref_indx:
       ax1.plot(ref_xaxis, delta_freq * 100., color=cl, linestyle=ls)
       ax1.scatter(ref_xaxis, delta_freq * 100., marker=mk, s=16, color=cl)
       ax1.set_xlim(xlim)
       ax1.set_ylabel(r'$\delta f$ [$\%$]')
       ax1.set_ylim(diff_f_from, diff_f_to)
       txt = r'$\delta f = (f_i - f_{\rm ref})/f_{\rm ref}$'
       ax1.annotate(txt, xy=(0.05, 0.80), xycoords='axes fraction')
       ax1.set_xticklabels(())
     else:
       ax1.plot(xaxis, delta_freq * 100., color=cl, linestyle=ls)
       ax1.scatter(xaxis, delta_freq * 100., color=cl, linestyle=ls, marker=mk, s=9)

     if i == ref_indx:
       ax2.plot(xaxis[:len(df)], delta_df * 100., color=cl, linestyle=ls)
       ax2.scatter(xaxis[:len(df)], delta_df * 100., color=cl, marker=mk, s=16)
       ax2.set_xlabel(xtitle)
       ax2.set_xlim(xlim)
       ax2.set_ylabel(r'$\delta(\Delta f) \, [\%]$')
       ax2.set_ylim(diff_df_from, diff_df_to)
       txt = r'$\delta (\Delta f) = (\Delta f_i - \Delta f_{\rm ref})/\Delta f_{\rm ref}$'
       ax2.annotate(txt, xy=(0.05, 0.05), xycoords='axes fraction')
     else:
       ax2.plot(xaxis[:len(df)], delta_df * 100., color=cl, linestyle=ls)
       ax2.scatter(xaxis[:len(df)], delta_df * 100., color=cl, marker=mk, linestyle=ls, s=9)

  if not leg_loc: leg_loc=3
  leg0 = ax0.legend(loc=leg_loc, shadow=True, fontsize='x-small', framealpha=0.5, numpoints=1, scatterpoints=1)

  if file_out:
    pdf_file = file_out
  else:
    if not os.path.exists('plots/'): os.path.makedirs('plots/')
    pdf_file = 'plots/Comp-p-freq-diff.pdf'
  plt.savefig(pdf_file)
  print ' - plot_compare_p_freq_diff: %s created!' % (pdf_file, )
  plt.close()

  return None

#=================================================================================================================
def plot_compare_inertia(list_files, only_el=None, list_lbls=None, by_ng=False, by_P=False, by_freq=False,
                    ng_from=0, ng_to=-100, P_from=0, P_to=100, freq_from=0, freq_to=100,
                    E_from=6000, E_to=16000, freq_unit='Hz'):
  """
  This function receives a list of gyre h5 files, and makes a multi-panel plot, which compares
  the mode interia between all of them within the same period range, or frequency range or mode order range.
  @param list_files: list of gyre input files
  @type list_files: list of strings
  @param only_el: set it to an integer (default=None), if you want to restrict the el to a certain value.
  @type only_el: integer
  @param lbl: list of strings (default=None), giving labels to appear in the plot legend for each gyre file
  @type lbl: list of strings
  @param by_ng, by_P, by_freq: boolean, determining the choice of x-axis. Accurdingly, the ranges for the x-axis can
        be fixed by e.g. ng_from and ng_to, etc. See the source argument list.
  @param E_from, E_to: specify the y-axis range; it is your responsibility to fix it if either E or E_norm is available
  @type E_from, E_to: float
  @param freq_unit: string specify the frequency unit of the source GYRE output file (HDF5 file). This will be then
        passed to period_spacing.gen_dP() to decently convert all possibilities into cgs units.
  @type freq_unit: string
  @type by_ng, by_P, by_freq: boolean
  @return: None
  @rtype: None
  """
  list_flags = [by_ng, by_freq, by_P]
  if not any(list_flags):
    message = 'Error: plot_gyre: plot_compare_inertia: at least one of x-axis flags must be True.'
    raise SystemExit, message

  n_h5 = len(list_files)
  if n_h5 == 0:
    message = 'Error: plot_gyre: plot_compare_E: input file list is empty'
    raise SystemExit, message

  sample_file = list_files[0]
  dic, loc = gyre.read_output(sample_file)
  names = loc.dtype.names
  dic_sample = {}
  for key in names:
    dic_sample[key] = 0.0
  requested_keys = ['E', 'E_norm']
  dic_keys = rd.check_key_exists(dic_sample, requested_keys)
  flag_E = dic_keys['E']
  flag_E_norm = dic_keys['E_norm']

  if not flag_E_norm and not flag_E:
    message = 'Error: plot_gyre: plot_compare_E: E and E_norm fields are essential, but missing in %s ' % (sample_file, )
    raise SystemExit, message


  #fig, axis = plt.subplots(nrows=n_h5, sharex=True, squeeze=True)
  fig = plt.figure()
  plt.subplots_adjust(left=0.085, right=0.98, top=0.985, bottom=0.08, wspace=0.05, hspace=0.07)

  dic_conv = cm.conversions()
  Hz_to_cd = dic_conv['Hz_to_cd']
  uHz_to_Hz = dic_conv['uHz_to_Hz']

  if freq_unit == 'Hz': factor = Hz_to_cd
  elif freq_unit == 'uHz': factor = uHz_to_Hz * Hz_to_cd

  if by_ng: xlim = (ng_from, ng_to); xtitle = r'Radial Order $n_g$'
  elif by_P: xlim = (P_from, P_to);  xtitle = r'Period [sec]'
  elif by_freq:
    xlim = (freq_from, freq_to)
    xtitle = r'Frequency [d$^{-1}$]'
    #if freq_unit == 'Hz': xtitle = r'Frequency [Hz]'
    #if freq_unit == 'uHz': xtitle = r'Frequency [$\mu$Hz]'
  else: raise SystemExit, 'Error: plot_gyre: plot_compare_inertia: set one of by_ng, by_P or by_freq to True.'

  for i in range(n_h5):
    file = list_files[i]
    if not os.path.isfile(file):
      print 'Warning: input file %s does not exist!' % (file, )
      continue

    ax = fig.add_subplot(n_h5, 1, i+1)
    ax.set_xlim(*xlim)
    ax.set_ylim(E_from, E_to)

    dic, loc = gyre.read_output(file)

    dic_dP= per.gen_dP(loc, freq_unit=freq_unit)
    dic_dP['file'] = file
    el_avail = dic_dP['el_avail']
    n_el_avail = len(el_avail)

    for i_el in range(n_el_avail):
      el = el_avail[i_el]
      if only_el is not None:
        if el != only_el: continue
      str_el = str(el)
      if el == 1: gm_clr = 'blue'; sym = 'o'
      if el == 2: gm_clr = 'red'; sym = 's'
      if el == 3: gm_clr = 'purple'; sym = '*'
      if el > 3:  gm_clr = 'pink'; sym = '^'

      key_n_gm = 'n_el_' + str_el + '_gm'
      n_gm = dic_dP[key_n_gm]
      key_el_npg = 'el_' + str_el + '_gm_npg'
      gm_npg = dic_dP[key_el_npg]
      key_el_P = 'el_' + str_el + '_gm_P'
      gm_P = dic_dP[key_el_P]
      key_el_dP = 'el_' + str_el + '_gm_dP'
      gm_dP = dic_dP[key_el_dP]
      key_el_freq_re = 'el_' + str_el + '_gm_freq_re'
      gm_freq_re = dic_dP[key_el_freq_re]
      key_el_E = 'el_' + str_el + '_gm_E'
      gm_E = dic_dP[key_el_E]
      key_el_E_norm = 'el_' + str_el + '_gm_E_norm'
      gm_E_norm = dic_dP[key_el_E_norm]

      if flag_E_norm and not flag_E:
        yaxis = np.log10(gm_E_norm)
        ax.set_ylabel(r'Normalized Inertia', fontsize='small')
      if not flag_E_norm and flag_E:
        yaxis = np.log10(gm_E)
        ax.set_ylabel(r'Inertia', fontsize='small')
      if flag_E and flag_E_norm:
        yaxis = np.log10(gm_E_norm)
        ax.set_ylabel(r'Normalized Inertia', fontsize='small')

      if by_ng: xaxis = gm_npg
      if by_P:  xaxis = gm_P
      if by_freq: xaxis = gm_freq_re * factor
      if i != n_h5 - 1:
        ax.set_xticklabels(())
      else:
        ax.set_xlabel(xtitle)

      ax.plot(xaxis, yaxis, linestyle='-', color=gm_clr, label=r'$\ell=$'+str_el)
      if list_lbls is not None: ax.annotate(list_lbls[i], [0.02, 0.05], xycoords='axes fraction')
      for j in range(n_gm):
        ax.scatter(xaxis[j], yaxis[j], color=gm_clr, marker=sym)
      if i == 0:
        if by_ng: leg_loc = 2
        if by_P: leg_loc = 1
        if by_freq: leg_loc = 1
        leg = ax.legend(loc=leg_loc, fontsize='small')


  plt.savefig('Inertia.pdf')
  print ' - Plot: Inertia.pdf Created'
  plt.close()


#=================================================================================================================
def plot_compare_dP(list_files, only_el=None, list_lbls=None, by_ng=False, by_P=False, by_freq=False,
                    ng_from=0, ng_to=-100, P_from = 0, P_to=10, freq_from = 0, freq_to = 100,
                    dP_from=6000, dP_to=16000, freq_unit='Hz', file_out=None):
  """
  This function receives a list of gyre h5 files, and makes a multi-panel plot, which compares
  the period spacing between all of them within the same period range, or frequency range or mode order range.
  @param list_files: list of gyre input files
  @type list_files: list of strings
  @param only_el: set it to an integer (default=None), if you want to restrict the el to a certain value.
  @type only_el: integer
  @param lbl: list of strings (default=None), giving labels to appear in the plot legend for each gyre file
  @type lbl: list of strings
  @param by_ng, by_P, by_freq: boolean, determining the choice of x-axis. Accurdingly, the ranges for the x-axis can
        be fixed by e.g. ng_from and ng_to, etc. See the source argument list.
  @param dP_from, dP_to: specify the y-axis range [sec]
  @type dP_from, dP_to: float
  @param freq_unit: string specify the frequency unit of the source GYRE output file (HDF5 file). This will be then
        passed to period_spacing.gen_dP() to decently convert all possibilities into cgs units.
  @type freq_unit: string
  @type by_ng, by_P, by_freq: boolean
  @param file_out: default=None; otherwise, gives the full path and name of the output plot file to be stored.
  @type file_out: string
  @return: None
  @rtype: None
  """
  list_flags = [by_ng, by_freq, by_P]
  if not any(list_flags):
    message = 'Error: plot_gyre: plot_compare_dP: at least one of x-axis flags must be True.'
    raise SystemExit, message

  n_h5 = len(list_files)

  #fig, axis = plt.subplots(nrows=n_h5, sharex=True, squeeze=True)
  fig = plt.figure(figsize=(6,4), dpi=200)
  #plt.subplots_adjust(left=0.085, right=0.98, top=0.985, bottom=0.08, wspace=0.05, hspace=0.07)
  plt.subplots_adjust(left=0.13, right=0.98, top=0.98, bottom=0.115, wspace=0.05, hspace=0.07)

  dic_conv = cm.conversions()
  Hz_to_cd = dic_conv['Hz_to_cd']
  uHz_to_Hz = dic_conv['uHz_to_Hz']
  sec_to_d  = dic_conv['sec_to_d']

  if freq_unit == 'Hz': factor = Hz_to_cd
  elif freq_unit == 'uHz': factor = uHz_to_Hz * Hz_to_cd

  if by_ng: xlim = (ng_from, ng_to); xtitle = r'Radial Order $n_g$'
  elif by_P: xlim = (P_from, P_to);  xtitle = r'Period [d]'
  elif by_freq:
    xlim = (freq_from, freq_to)
    xtitle = r'Frequency [d$^{-1}$]'
    #if freq_unit == 'Hz': xtitle = r'Frequency [Hz]'
    #if freq_unit == 'uHz': xtitle = r'Frequency [$\mu$Hz]'
  else: raise SystemExit, 'Error: plot_gyre: plot_compare_dP: set one of by_ng, by_P or by_freq to True.'

  for i in range(n_h5):
    file = list_files[i]
    if not os.path.isfile(file):
      print 'Warning: input file %s does not exist!' % (file, )
      continue

    ax = fig.add_subplot(n_h5, 1, i+1)
    ax.set_xlim(*xlim)
    ax.set_ylim(dP_from, dP_to)
    ax.set_ylabel(r'$\Delta P$ [sec]', fontsize='small')

    dic, loc = gyre.read_output(file)
    for i_name, name in enumerate(loc.dtype.names): dic[name] = loc[name]

    #dic_dP= per.gen_dP(loc, freq_unit=freq_unit)
    dic_dP= per.gen_dP(dic, freq_unit=freq_unit)
    dic_dP['file'] = file
    el_avail = dic_dP['el_avail']
    n_el_avail = len(el_avail)

    for i_el in range(n_el_avail):
      el = el_avail[i_el]
      if only_el is not None:
        if el != only_el: continue
      str_el = str(el)
      if el == 1: gm_clr = 'blue'; sym = 'o'
      if el == 2: gm_clr = 'red'; sym = 's'
      if el == 3: gm_clr = 'purple'; sym = '*'
      if el > 3:  gm_clr = 'pink'; sym = '^'

      key_n_gm = 'n_el_' + str_el + '_gm'
      n_gm = dic_dP[key_n_gm]
      key_el_npg = 'el_' + str_el + '_gm_npg'
      gm_npg = dic_dP[key_el_npg]
      key_el_P = 'el_' + str_el + '_gm_P'
      gm_P = dic_dP[key_el_P]
      key_el_dP = 'el_' + str_el + '_gm_dP'
      gm_dP = dic_dP[key_el_dP]
      key_el_freq_re = 'el_' + str_el + '_gm_freq_re'
      gm_freq_re = dic_dP[key_el_freq_re]

      if by_ng: xaxis = gm_npg
      if by_P:  xaxis = gm_P * sec_to_d
      if by_freq: xaxis = gm_freq_re * factor
      if i != n_h5 - 1:
        ax.set_xticklabels(())
      else:
        ax.set_xlabel(xtitle)

      ax.plot(xaxis[:n_gm-1], gm_dP, linestyle='-', color=gm_clr, label=r'$\ell=$'+str_el)
      if list_lbls is not None: ax.annotate(list_lbls[i], [0.02, 0.05], xycoords='axes fraction')
      for j in range(n_gm-1):
        ax.scatter(xaxis[j], gm_dP[j], color=gm_clr, marker=sym)
      if i == 0:
        if by_ng: leg_loc = 2
        if by_P: leg_loc = 1
        if by_freq: leg_loc = 1
        leg = ax.legend(loc=leg_loc, fontsize='small')

  if file_out:
    pdf_full_path = file_out
  else:
    if not os.path.exists('plots/'): os.makedirs('plots/')
    pdf_full_path = 'plots/Comp-dP.pdf'

  plt.savefig(pdf_full_path)
  print ' - Plot: %s Created' % (pdf_full_path, )
  plt.close()


#=================================================================================================================
def plot_evol_dP_Xc(filenames):
  """
  Provided a list of GYRE output filenames, this function produces a plot of the evolution of
  period spacing Delta P_n as a function of the change in Xc. Variation as a function of Teff,
  log_g, etc are in prospect.
  @param filenames: list of filenames giving the full path to more than one GYRE output file
  @type filenames: list of strings
  @return: None
  @rtype: None
  """

  n_files = len(filenames)
  if n_files == 1:
    print 'plot_evol_dP_Xc: Only one filename detected!'
    file = filenames[0]
    plot_single_dP(file)
    return None

  # extract all information and store them as a list of dictionaries
  if n_files > 1:
    list_dic = get_dP_multiple_gyre_file(filenames)

  Xc_arr = []
  for i_file in range(n_files):
    Xc = list_dic[i_file]['Xc']
    Xc_arr.append(Xc)

  Xc_arr = np.asarray(Xc_arr)
  Xc_sort_indx = np.argsort(Xc_arr)
  Xc_arr = Xc_arr[Xc_sort_indx]
  #list_dic = list_dic[Xc_sort_indx]

  # sort the list w.r.t the sorted indices of Xc
  list_dic_new = []
  for i_file in range(n_files):
    list_dic_new.append(list_dic[Xc_sort_indx[i_file]])
  list_dic = list_dic_new[:]
  list_dic_new = 0

  #---------------------------
  # Define the Plot
  #---------------------------
  fig = plt.figure(figsize=(8,6), dpi=200)
  font = hrd.get_font_dic(font = 'serif', clr = 'Black', sz = 16)

  #---------------------------
  # Plot a List of Period Spacing
  #---------------------------
  for i_file in range(n_files):
    which_dic = list_dic[i_file]
    gmodes_subseq_n_g = which_dic['subseq_n_g']
    gmodes_subseq_per = which_dic['subseq_P']
    gmodes_subseq_delta_per = which_dic['subseq_dP']
    mean_per = np.average(gmodes_subseq_per)
    gmodes_subseq_per = gmodes_subseq_per - mean_per + Xc_arr[i_file]

    plt.plot(gmodes_subseq_n_g, gmodes_subseq_delta_per)

  gyre_srch_str = pt.gen_gyre_srch_str()['gyre_srch_str']
  plot_file = pt.gen_path_plot() + 'dP-' + gyre_srch_str + '.pdf'
  fig.savefig(plot_file, dpi=200)
  print 'Multiple Period Spacing Plot Stored at: '
  print '  ', plot_file
  plt.close()

  return None

#=================================================================================================================
def plot_single_dP(file, file_out):
  """
  For a single GYRE HDF5 output file, it calls get_dP_single_gyre_file() function to get the period spacings,
  and param_tools.get_param_from_single_gyre_filename() to convert filename string to a dictionary.
  It produces a plot
  @param file: full path to the GYRE output file
  @type file: string
  @return: None
  @rtype: None
  """
  dic_param_from_single_gyre_filename = pt.get_param_from_single_gyre_filename(file)
  dic = dic_param_from_single_gyre_filename
  dic_param = pt.param_to_str()

  dic_dP = get_dP_single_gyre_file(file)
  gmodes_subseq_n_g = dic_dP['subseq_n_g']
  gmodes_subseq_delta_per = dic_dP['subseq_dP']

  #---------------------------
  # Define the Plot
  #---------------------------
  fig = plt.figure(figsize=(8,6), dpi=200)
  font = hrd.get_font_dic(font = 'serif', clr = 'Black', sz = 16)

  #---------------------------
  # Plot Period Spacing
  #---------------------------
  plt.plot(gmodes_subseq_n_g, gmodes_subseq_delta_per, linestyle = '-', color = 'black')
  plt.scatter(gmodes_subseq_n_g, gmodes_subseq_delta_per, color = 'blue')
  plt.xlim(0,gmodes_subseq_n_g.max()+1)
  plt.ylim(gmodes_subseq_delta_per.min()-200,gmodes_subseq_delta_per.max()+200)
  plt.xlabel(r'${\rm Radial\, Nodes\, } n_g$')
  plt.ylabel(r'${\rm Period\, Spacing\, } \Delta P_n = P_{n+1} - P_n$')

  #---------------------------
  # Add Text
  #---------------------------
  if False:
    left = 0.70; top = 0.92; shift = 0.04
    plt.text(0.5, 0.5, 'hi')
    plt.annotate('hi',(.5,.5), xycoords='figure fraction' )
    txt_mass = r'$M = $' + r'$' + dic_param['mass_fmt'].format(dic['mass']) + ' M_\odot$'
    plt.text(left, top, txt_mass, ha='left', va='center', color='Navy', alpha=1.0, clip_on=True, fontdict=font)
    txt_eta = r'$\eta_{\rm rot} = $' + r'$' + dic_param['eta_fmt'].format(dic['eta']) + '$'
    plt.text(left, top-shift, txt_eta, ha='left', va='center', color='Navy', alpha=1.0, clip_on=True, fontdict=font)
    txt_ov = r'$f_{\rm ov} = $' + r'$' + dic_param['ov_fmt'].format(dic['ov']) + '$'
    plt.text(left, top-2*shift, txt_ov, ha='left', va='center', color='Navy', alpha=1.0, clip_on=True, fontdict=font)
    txt_sc = r'$\alpha_{\rm sc} = $' + r'$' + dic_param['sc_fmt'].format(dic['sc']) + '$'
    plt.text(left, top-3*shift, txt_sc, ha='left', va='center', color='Navy', alpha=1.0, clip_on=True, fontdict=font)
    txt_z = r'$Z = $' + r'$' + dic_param['z_fmt'].format(dic['Z']) + '$'
    plt.text(left, top-4*shift, txt_z, ha='left', va='center', color='Navy', alpha=1.0, clip_on=True, fontdict=font)
    txt_Xc = r'$X_c = $' + r'$' + dic_param['Xc_fmt'].format(dic['Xc']) + '$'
    plt.text(left, top-5*shift, txt_Xc, ha='left', va='center', color='Navy', alpha=1.0, clip_on=True, fontdict=font)
    txt_Yc = r'$Y_c = $' + r'$' + dic_param['Yc_fmt'].format(dic['Yc']) + '$'
    plt.text(left, top-6*shift, txt_Yc, ha='left', va='center', color='Navy', alpha=1.0, clip_on=True, fontdict=font)

  #---------------------------
  # Plot Location
  #---------------------------
  fig.savefig(file_out, dpi=200)
  print ' - plot_gyre: plot_single_dP: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def get_dP_multiple_gyre_file(filenames):
  """
  Provide a list of GYRE output filenames, and it returns a list of dictionaries.
  It calls get_dP_single_gyre_file to generate a dictionary of g-mode spectrum, and
  stacks them together as a list of dictionaries.
  @param filenames: list of strings specifying the path to an archive of GYRE output files.
  @type filenames: list of strings
  @return: list of dictionaries. Check the dictionary keys and values with the output of
           get_dP_single_gyre_file.
  @rtype: list of dictionaries
  """

  list_dic = []

  n_files = len(filenames)
  if n_files == 1:
    print 'get_dP_multiple_gyre_file detected ONLY one input file!'
    file = filenames[0]
    dic_out = get_dP_single_gyre_file(file)
    list_dic.append(dic_out)

  if n_files > 1:
    for i_file in range(n_files):
      file = filenames[i_file]
      dic_out = get_dP_single_gyre_file(file)
      list_dic.append(dic_out)

  return list_dic

#=================================================================================================================
def get_dP_single_gyre_file(file):
  """
  For a given GYRE output HDF5 file, gmodes are sorted in increasing order n_g, and
  the subsequent modes are identified. Only for these modes, it returns their order,
  their period, and their period spacing
  @param file: the full path to the GYRE output file
  @type file: string
  @return: a dictionary with these keys: 'subseq_n_g', 'subseq_P', 'subseq_dP'.
        The values are numpy ndarray.
  @rtype: dictionary
  """

  dic_param_from_single_gyre_filename = pt.get_param_from_single_gyre_filename(file)

  dic_one_gyre, recarr_one_gyre = gyre.read_output(file)
  num_modes = len(recarr_one_gyre)
  n_g = recarr_one_gyre['n_g']
  freq  = recarr_one_gyre['freq'].real

  gmodes_indx = np.where((n_g > 0))[0]
  gmodes_num = len(gmodes_indx)
  gmodes_n_g  = n_g[gmodes_indx]
  gmodes_freq = freq[gmodes_indx]

  gmodes_sort_n_g_indx = np.argsort(gmodes_n_g)
  gmodes_n_g = gmodes_n_g[gmodes_sort_n_g_indx]
  gmodes_freq = gmodes_freq[gmodes_sort_n_g_indx]

  gmodes_per  = 1.0/gmodes_freq
  gmodes_delta_n_g = np.zeros(gmodes_num-1)
  gmodes_delta_per = np.zeros(gmodes_num-1)

  for i_n in range(gmodes_num-1):
    gmodes_delta_n_g[i_n] = gmodes_n_g[i_n+1] - gmodes_n_g[i_n]
    gmodes_delta_per[i_n] = gmodes_per[i_n+1] - gmodes_per[i_n]
  gmodes_subseq_indx = np.where((gmodes_delta_n_g == 1.0))[0]
  gmodes_subseq_num  = len(gmodes_subseq_indx)
  gmodes_subseq_n_g  = gmodes_n_g[gmodes_subseq_indx]
  gmodes_subseq_per  = gmodes_per[gmodes_subseq_indx]
  gmodes_subseq_delta_per = gmodes_delta_per[gmodes_subseq_indx]

  dic_dP = {}
  dic_dP['subseq_n_g'] = gmodes_subseq_n_g
  dic_dP['subseq_P']   = gmodes_subseq_per
  dic_dP['subseq_dP']  = gmodes_subseq_delta_per

  dic_dP.update(dic_dP.items() + dic_param_from_single_gyre_filename.items())

  return dic_dP

#=================================================================================================================
def hist_obs_minus_model_freq(dics_modes, dic_star, el=1, sigma=10, bins=11, file_out=None):
  """
  For all modes in the list_dic_freq of dic_star, show the histograms of the deviations 
  of the observed frequencies from the model frequencies. There is a selection rule here:
  We only accept those models that have exactly n_obs_freq modes between the observed 
  lowest and highest frequencies (to also be consistent with our chi-square computations, 
  which enforces the same).
  @param dics_modes: list of dictionaries of gyre output frequencies
  @type dics_modes: list of dictionaries
  @param dic_star: star information as returned by calling one the star-functions in mesa_gyre.stars
  @type dic_star: dictionary
  @param el: mode degree el for mode selection and comparison
  @type el: integer 
  @param bins: the number of bins for creating the histogram.
  @type bins: integer
  @param file_out: full path to the output plot 
  @type file_out: string 
  @return: Histogram data matrix of "n_obs_freq x bins" size. Each row of the matrix corresponds to 
        a the histogram of one mode.
  @rtype: ndarray
  """
  if file_out is None: return 
  n_dics   = len(dics_modes)
  dic_conv = cm.conversions()
  Hz_to_cd = dic_conv['Hz_to_cd']

  list_dic_freq = dic_star['list_dic_freq']
  n_obs_freq    = len(list_dic_freq)
  obs_freqs     = np.array([ dic['freq'] for dic in list_dic_freq ])
  obs_freqs_err = np.array([ dic['freq_err'] for dic in list_dic_freq ])
  ind_sort      = np.argsort(obs_freqs)
  obs_freqs     = obs_freqs[ind_sort]
  obs_freqs_err = obs_freqs_err[ind_sort]          

  min_freq      = obs_freqs[0] - sigma * obs_freqs_err[0]
  max_freq      = obs_freqs[-1] + sigma * obs_freqs_err[-1]

  df_mtrx       = np.zeros((n_dics, n_obs_freq))

  i_row         = 0
  for i_dic, dic in enumerate(dics_modes):
    ind_el      = np.where(dic['l'] == el)[0]
    mod_freqs   = np.real(dic['freq'][ind_el]) * Hz_to_cd
    ind_modes   = np.where( (mod_freqs <= max_freq) & (mod_freqs >= min_freq) )[0]
    n_modes     = len(ind_modes)
    if n_modes != n_obs_freq: continue

    mod_freqs   = mod_freqs[ind_modes]
    df_mtrx[i_row, 0:n_obs_freq] = obs_freqs - mod_freqs   # obs - theory
    i_row       += 1

  num_rows      = i_row
  df_mtrx       = df_mtrx[0:num_rows, :]

  #---------------------------
  # Now, show histograms of the
  # matrix of differences
  #---------------------------
  n_rows      = 4
  n_cols      = 5
  fig, tup_ax = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(8, 8), squeeze=False, sharex=True, sharey=True)
  plt.subplots_adjust(left=0.06, right=0.98, bottom=0.06, top=0.98, wspace=0.06, hspace=0.06)

  mx    = 0
  my    = 0
  data  = np.zeros((n_obs_freq, bins))
  for i in range(n_obs_freq):
    col = df_mtrx[:, i]
    i_row = i / n_cols
    i_col = i % n_cols
    ax  = tup_ax[i_row, i_col]

    ax.axvline(x=0, linestyle='dotted', lw=1, color='grey', zorder=1)
    ax.annotate(r'$f_{'+str(i+1)+r'}$', (0.1, 0.85), xycoords='axes fraction')
    res = ax.hist(col, bins=bins, color='black', zorder=2)

    for tick in ax.get_xticklabels():
        tick.set_rotation(45)

    n   = res[0]
    x   = res[1]
    mx  = min([mx, n.min()])
    my  = max([my, n.max()])

    data[i, 0:bins] = n

  fig.delaxes(tup_ax[-1, -1])

  plt.locator_params(axis='x', nbins=3)
  ax.set_ylim(0, my*1.1)
  # plt.xlabel(r'$\delta f_i=f_i^{\rm (obs)}-f_i^{\rm (mod)}$')

  plt.savefig(file_out, transparent=True)
  print ' - plot_gyre: hist_obs_minus_model_freq: saved {0}'.format(file_out)
  print '   With sigma={0}, {1} survived our strict condition out of {2} \n'.format(sigma, num_rows, n_dics)
  plt.close()

  return data

#=================================================================================================================
def plot_comp_ad_nad(ad_sum, nad_sum, freq_unit='Hz', tag=''):
  """
  This function receives full path to two GYRE short summary files, and makes a multi-panel comparison plot.
  @param ad_sum: full path to adiabatic short summary file
  @type ad_sum: string
  @param nad_sum: full path to Non-adiabatic short summary file
  @type nad_sum: string
  @param freq_unit: the output frequency unit of gyre file. It is specified in the GYRE input inlist below the &output
      namelist; default = 'Hz'
      It is passed to gen_dP() to make sure that the periods and period spacings returned are in units of sec.
  @type freq_unit: string
  @param tag: to append extra name at the end of the output PDF file, before the .pdf extension.
  @type tag: string
  @return: None
  @rtype: None
  """

  dic_conv = cm.conversions()
  Hz_to_cd = dic_conv['Hz_to_cd']
  uHz_to_Hz = dic_conv['uHz_to_Hz']
  sec_to_d  = dic_conv['sec_to_d']

  ind_slash = ad_sum.rfind('/')
  if ind_slash > 10:
    main_path = ad_sum[:ind_slash+1]
    plot_path = main_path.replace('gyre_out', 'plots')
    prof_name = ad_sum[ind_slash+1:]
  else:
    plot_path = 'plots/'
    prof_name = ad_sum[ind_slash+1:]

  if tag:
    #pdf_name  = 'Comp-ad-nad-' + prof_name.replace('.h5', '.pdf')
    #pdf_name.replace('.pdf', '-' + tag + '.pdf')
    pdf_name = 'Comp-ad-nad-' + tag + '.pdf'
  else:
    pdf_name  = 'Comp-ad-nad-' + prof_name.replace('.h5', '.pdf')
  pdf_full_path = plot_path + pdf_name
  if not os.path.exists(plot_path): os.makedirs(plot_path)

  loc, ad = gyre.read_output(ad_sum)
  loc, nad = gyre.read_output(nad_sum)

  dic_ad = per.gen_dP(ad, freq_unit=freq_unit)
  dic_nad = per.gen_dP(nad, freq_unit=freq_unit)

  ad_has_el_1  = dic_ad.has_key('n_el_1')
  nad_has_el_1 = dic_nad.has_key('n_el_1')

  if not ad_has_el_1 and not nad_has_el_1:
    message = 'Error: plot_gyre: plot_comp_ad_nad: ad and nad have no el=1 mode!'
    raise SystemExit, message

  ad_freq = dic_ad['el_1_gm_freq_re']
  ad_n_pg = dic_ad['el_1_gm_npg']
  ad_per  = dic_ad['el_1_gm_P']
  ad_dP   = dic_ad['el_1_gm_dP']
  num_ad_modes = dic_ad['n_el_1_gm']
  num_ad_modes_initially = num_ad_modes

  nad_freq = dic_nad['el_1_gm_freq_re']
  nad_n_pg = dic_nad['el_1_gm_npg']
  nad_per  = dic_nad['el_1_gm_P']
  nad_dP   = dic_nad['el_1_gm_dP']
  num_nad_modes = dic_nad['n_el_1_gm']
  num_nad_modes_initially = num_nad_modes

  if freq_unit == 'Hz':
    ad_freq[:]  = ad_freq[:] * Hz_to_cd
    nad_freq[:] = nad_freq[:] * Hz_to_cd
  elif freq_unit == 'uHz':
    ad_freq[:]  = ad_freq[:] * uHz_to_Hz * Hz_to_cd
    nad_freq[:] = nad_freq[:] * uHz_to_Hz * Hz_to_cd
  else:
    message = 'Error: plot_gyre: plot_comp_ad_nad: freq_unit = %s is not supported!' % (freq_unit, )
    raise SystemExit, message

  delta_freq = []
  n_pg_for_delta = []
  val = 0.0

  if num_ad_modes != num_nad_modes:
    ad_nad_disagree = True
    print ' - plot_gyre: plot_comp_ad_nad: Warning:'
    print '   num_ad_modes != num_nad_modes: %s and %s' % (num_ad_modes, num_nad_modes)
    print '   min(ad_n_pg)= %s, max(ad_n_pg)= %s' % (min(ad_n_pg), max(ad_n_pg))
    print '   min(nad_n_pg)= %s, max(nad_n_pg)= %s' % (min(nad_n_pg), max(nad_n_pg))
    print '   Walk through the ad_freq, and JUST pick the closest nad_freq.\n'
  else: ad_nad_disagree = False

  if ad_nad_disagree:
    min_num_modes = min([num_ad_modes, num_nad_modes])
    # Store the nad frequency into a new array, and pick from it iteratively, and empty it.
    # This makes sure we do not find the same nad frequency for more than one ad frequency!
    nad_freq_initially = list(nad_freq); nad_n_pg_initially = list(nad_n_pg)
    nad_per_initially = list(nad_per)
    # Artificially add one item at the end of nad_dP_initially to make it the same size as others.
    nad_dP_initially = list(nad_dP); nad_dP_initially.insert(-1, -1)

    new_ad_freq = [];  new_ad_npg = [];  new_ad_per = [];  new_ad_dP = []
    new_nad_freq = []; new_nad_npg = []; new_nad_per = []; new_nad_dP = []

    # walk through the ad_freq, and pick the closest nad_freq
    for ind_ad, one_ad_freq in enumerate(ad_freq):
      ind_close_nad_freq = np.argmin( np.abs( nad_freq_initially[:] - one_ad_freq ) )
      one_nad_freq = nad_freq_initially[ind_close_nad_freq]
      one_nad_n_pg = nad_n_pg_initially[ind_close_nad_freq]
      one_nad_per  = nad_per_initially[ind_close_nad_freq]
      try:
        one_nad_dP = nad_dP_initially[ind_close_nad_freq]
      except IndexError:
        one_nad_dP = 0.0

      delta_freq.append( (one_ad_freq - one_nad_freq)/one_ad_freq )

      # Pop out this closest match from the nad list
      del nad_freq_initially[ind_close_nad_freq]
      del nad_n_pg_initially[ind_close_nad_freq]
      del nad_per_initially[ind_close_nad_freq]
      del nad_dP_initially[ind_close_nad_freq]

      # Append them all to the list to keep their ordering
      new_ad_freq.append(one_ad_freq)
      new_ad_npg.append(ad_n_pg[ind_ad])
      new_ad_per.append(ad_per[ind_ad])
      if ind_ad == min_num_modes-1:
        new_ad_dP.append(0.0)
      else:
        new_ad_dP.append(ad_dP[ind_ad])

      new_nad_freq.append(one_nad_freq)
      new_nad_npg.append(one_nad_n_pg)
      new_nad_per.append(one_nad_per)
      new_nad_dP.append(one_nad_dP)





  #if ad_nad_disagree:
    ## Store the ad and nad freq into new lists, to pick up from the list iteratively, and empty it.
    #nad_freq_initially = []
    #for i in range(num_nad_modes): nad_freq_initially.append(nad_freq[i])
    #new_ad_freq = [];  new_ad_npg = [];  new_ad_per = [];  new_ad_dP = []
    #new_nad_freq = []; new_nad_npg = []; new_nad_per = []; new_nad_dP = []
    ## walk through the ad_freq, and pick the closest nad_freq
    #for i_m, one_ad_freq in enumerate(ad_freq):
      #new_ad_freq.append(one_ad_freq)
      #new_ad_npg.append(ad_n_pg[i_m])
      #new_ad_per.append(ad_per[i_m])
      #if i_m == min_num_modes-1:
        #new_ad_dP.append(0.0)
      #else:
        #new_ad_dP.append(ad_dP[i_m])
      #ind_close_nad_freq = np.argmin(np.abs(nad_freq_initially - one_ad_freq))
      #new_nad_freq.append(nad_freq[ind_close_nad_freq])
      #new_nad_npg.append(nad_n_pg[ind_close_nad_freq])
      #new_nad_per.append(nad_per[ind_close_nad_freq])
      #if i_m == min_num_modes-1:
        #new_nad_dP.append(0.0)
      #else:
        #new_nad_dP.append(nad_dP[i_m])

      ## Take this closest-matching nad_freq out from the list to avoid douplicated matching!
      #del nad_freq_initially[ind_close_nad_freq]
    # convert the new freq and n_pg back to numpy arrrays
    ad_freq = np.asarray(new_ad_freq)
    ad_n_pg = np.asarray(new_ad_npg)
    ad_per  = np.asarray(new_ad_per)
    ad_dP   = np.asarray(new_ad_dP)
    num_ad_modes = len(ad_freq)
    nad_freq = np.asarray(new_nad_freq)
    nad_n_pg = np.asarray(new_nad_npg)
    nad_per  = np.asarray(new_nad_per)
    nad_dP   = np.asarray(new_nad_dP)
    num_nad_modes = len(nad_freq)

    #for ad_ind, one_ad_freq in enumerate(ad_freq):
      #one_nad_freq = nad_freq[ad_ind]
      #val = (one_ad_freq - one_nad_freq) / one_ad_freq
      #delta_freq.insert(-1, val)
    delta_freq = np.asarray(delta_freq)

  #for i in range(min_num_modes):
    #print 'ad_npg: %s, nad_npg: %s, ad_freq: %6e, nad_freq: %6e, delta_freq: %s' %  (ad_n_pg[i], nad_n_pg[i],
                                                                                     #ad_freq[i], nad_freq[i], delta_freq[i]*100)

  min_npg = min(ad_n_pg); max_npg = max(ad_n_pg)
  min_ad_freq = min(ad_freq); max_ad_freq = max(ad_freq)
  min_ad_per  = min(ad_per * sec_to_d); max_ad_per = max(ad_per * sec_to_d)

  # get the fundamental radial mode out
  found_radial = 0 in ad.l and 1 in ad.n_pg
  if found_radial:
    ind_rad = [ind for ind in range(num_ad_modes) if (ad_n_pg[ind] == 1)]

  fig = plt.figure(figsize=(8, 8))
  ax1 = fig.add_subplot(221)
  plt.subplots_adjust(left=0.06, right=0.99, bottom=0.06, top=0.99, wspace=0.16, hspace=0.18)

  ax1.scatter(ad_freq, ad_freq, color='blue', marker='s', s=12.0, label=r'adiab')
  ax1.scatter(ad_freq, nad_freq, color='red', marker='o', s=5.0, label=r'None-adiab')
  #ax1.set_xlim(min_npg-2, max_npg+2)
  ax1.set_xlim(min_ad_freq*0.98, max_ad_freq*1.02)
  ax1.set_ylabel(r'Frequency [d$^{-1}$]')
  ax1.set_xlabel(r'Frequency $f_{\rm ad}$ [d$^{-1}$]')
  ax1.set_ylim(min(ad_freq)-1e-5, max(ad_freq)+1e-4)
  txt_ad  = r'N$_{\rm ad}$ =  ' + str(num_ad_modes_initially)
  txt_nad = r'N$_{\rm nad}$ = ' + str(num_nad_modes_initially)
  ax1.annotate(txt_ad, xy=(0.75, 0.15), xycoords='axes fraction')
  ax1.annotate(txt_nad, xy=(0.75, 0.07), xycoords='axes fraction')
  leg1 = ax1.legend(loc='upper left')

  ax2 = fig.add_subplot(223)
  ax2.scatter(ad_per * sec_to_d, ad_per * sec_to_d, color='blue', marker='s', s=12.0, label=r'adiab')
  ax2.scatter(ad_per * sec_to_d, nad_per * sec_to_d, color='red', marker='o', s=5.0, label=r'None-adiab')
  #ax2.set_xlim(min_npg-2, max_npg+2)
  ax2.set_xlim(min_ad_per*0.98, max_ad_per*1.02)
  ax2.set_ylabel(r'Period [d]')
  ax2.set_xlabel(r'Period $f_{\rm ad}^{-1}$ [d]')
  ax2.set_ylim(min(ad_per*sec_to_d)*0.95, max(ad_per*sec_to_d)*1.05)
  leg2 = ax2.legend(loc=2)

  ax3 = fig.add_subplot(222)
  #ax3.scatter(n_pg_for_delta, delta_freq * 100.0, color='Maroon')
  ax3.scatter(ad_freq, delta_freq * 100.0, color='Maroon')
  ax3.set_ylabel(r'$\delta f = (f_{\rm ad} - f_{\rm nad})/f_{\rm ad}$  [$\%$]')
  #ax3.set_xlim(min_npg-2, max_npg+2)
  ax3.set_xlim(min_ad_freq*0.98, max_ad_freq*1.02)
  ax3.set_xlabel(r'Frequency $f_{\rm ad}$ [d$^{-1}$]')
  #ax3.set_xticklabels(())
  ax3.set_ylim(-5, 5)
  txt = r'Max($\delta f$)=' + "{:.2f}".format(max(delta_freq*100.)) + r'$\%$'
  ax3.annotate(txt, xy=(0.60, 0.89), xycoords='axes fraction')
  txt = r'Min($\delta f$)=' + "{:.2f}".format(min(delta_freq*100.)) + r'$\%$'
  ax3.annotate(txt, xy=(0.60, 0.81), xycoords='axes fraction')

  ax4 = fig.add_subplot(224)
  ax4.scatter(ad_n_pg[:num_ad_modes-1], ad_dP[:num_ad_modes-1]/1000, marker='o', color='blue', label=r'$\ell=1$, Adiab')
  ax4.plot(ad_n_pg[:num_ad_modes-1], ad_dP[:num_ad_modes-1]/1000, color='blue')
  #ax4.scatter(nad_n_pg, nad_dP, marker='s', color='red')
  #ax4.plot(nad_n_pg, nad_dP, color='red')
  ax4.set_ylabel(r'Period Spacing $\Delta P$ [kilo sec]')
  ax4.set_xlim(min_npg-2, max_npg+2)
  #ax4.set_xlim(min_ad_freq*0.98, max_ad_freq*1.02)
  ax4.set_xlabel(r'Total Mode Order $n_{pg}$')
  leg4 = ax4.legend(loc=4)

  plt.savefig(pdf_full_path)
  print ' - plot_comp_ad_nad: ', pdf_full_path, ' generated!'
  plt.close()

  return None

############################## EigenFunction Study
#ad_eig_files = glob.glob('h5/' + 'ad-eig-*.h5')
#nad_eig_files = glob.glob('h5/' + 'nad-eig-*.h5')
#num_ad_eig_files = len(ad_eig_files)
#num_nad_eig_files = len(nad_eig_files)
#print ' - Found %s ad and %s nad eigen files' % (num_ad_eig_files, num_nad_eig_files)

#ad_eig_data = []
#ad_eig_n_pg = []
#for i_eig in range(num_ad_eig_files):
  #eig_file = ad_eig_files[i_eig]
  #loc, ad = gyre.read_output(eig_file)
  #ad_eig_data.append((loc, ad))
  #ad_eig_n_pg.append(loc['n_pg'])


#nad_eig_data = []
#nad_eig_n_pg = []
#for i_eig in range(num_nad_eig_files):
  #eig_file = nad_eig_files[i_eig]
  #loc, nad = gyre.read_output(eig_file)
  #nad_eig_data.append((loc, nad))
  #nad_eig_n_pg.append(loc['n_pg'])


#for ad_ind, ad_tup in enumerate(ad_eig_data):
  #ad_loc, ad_data = ad_tup
  #one_ad_freq = ad_loc['freq'].real * Hz_to_cd
  #one_ad_n_pg = ad_loc['n_pg']

  ##for nad_ind, nad_tup in enumerate(nad_eig_data):
  #nad_ind = [ind for ind in range(num_nad_eig_files-1) if nad_eig_data[ind][0]['n_pg'] == one_ad_n_pg]
  #if len(nad_ind) == 0: continue
  #nad_ind = nad_ind[0]
  #nad_tup = nad_eig_data[nad_ind]
  #nad_loc, nad_data = nad_tup
  #one_nad_freq = nad_loc['freq'].real * Hz_to_cd
  #one_nad_n_pg = nad_loc['n_pg']

  ## open a new plot, and put the ad and nad eigenfunctions for each mode.
  #fig = plt.figure()
  #ax1 = fig.add_subplot(411)
  #plt.subplots_adjust(left=0.10, right=0.99, bottom=0.09, top=0.99, wspace=0.02, hspace=0.05)

  #ad_xi_r_r = ad_data['xi_r'].real
  #ad_xi_r_i = ad_data['xi_r'].imag
  #ad_xi_h_r = ad_data['xi_h'].real
  #ad_xi_h_i = ad_data['xi_h'].imag
  #ad_T_coor = np.log10(ad_data['T'])
  #nad_xi_r_r = nad_data['xi_r'].real
  #nad_xi_r_i = nad_data['xi_r'].imag
  #nad_xi_h_r = nad_data['xi_h'].real
  #nad_xi_h_i = nad_data['xi_h'].imag
  #nad_delp_r = nad_data['delp'].real
  #nad_delp_i = nad_data['delp'].imag
  #nad_delS_r = nad_data['delS'].real
  #nad_delS_i = nad_data['delS'].imag
  #nad_delL_r = nad_data['delL'].real
  #nad_delL_i = nad_data['delL'].imag
  #nad_dE_dx  = nad_data['dE_dx']
  #nad_dW_dx  = nad_data['dW_dx']
  #nad_T_coor = np.log10(nad_data['T'])

  #ax1.plot(ad_T_coor, ad_xi_r_r, '-b', alpha=0.6, label=r'Adiab, Real')
  #ax1.plot(ad_T_coor, ad_xi_r_i, '--b', alpha=0.6, label=r'Adiab, Imag')
  #ax1.plot(nad_T_coor, nad_xi_r_r, '-r', alpha=0.6, label=r'Non-Adiab, Real')
  #ax1.plot(nad_T_coor, nad_xi_r_i, '--r', alpha=0.6, label=r'Non-Adiab, Imag')
  #ax1.set_ylabel(r'Radial $\xi_r$', fontsize = 'small')
  #ax1.set_xlim(max(ad_T_coor)+0.1, min(ad_T_coor)+0.1)
  #ax1.set_xticklabels( () )
  #txt = r'$(n_{pg})_{\rm ad}=$' + str(one_ad_n_pg)
  #ax1.annotate(txt, xy=(0.01, 0.40), fontsize='small', xycoords='axes fraction')
  #txt = r'$f_{\rm ad}=$' + "{:.4f}".format(one_ad_freq)+' [d$^{-1}$]'
  #ax1.annotate(txt, xy=(0.15, 0.40), fontsize='small', xycoords='axes fraction')
  #txt = r'$P_{\rm ad}=$' + "{:.4f}".format(1./one_ad_freq)+' [d]'
  #ax1.annotate(txt, xy=(0.33, 0.40), fontsize='small', xycoords='axes fraction')
  #txt = r'$(n_{pg})_{\rm nad}=$' + str(one_nad_n_pg)
  #ax1.annotate(txt, xy=(0.01, 0.20), fontsize='small', xycoords='axes fraction')
  #txt = r'$f_{\rm nad}=$' + "{:.4f}".format(one_nad_freq)+' [d$^{-1}$]'
  #ax1.annotate(txt, xy=(0.15, 0.20), fontsize='small', xycoords='axes fraction')
  #txt = r'$P_{\rm nad}=$' + "{:.4f}".format(1./one_nad_freq)+' [d]'
  #ax1.annotate(txt, xy=(0.33, 0.20), fontsize='small', xycoords='axes fraction')
  ##leg = ax1.legend(loc='upper left', fontsize='x-small')

  #ax2 = fig.add_subplot(412)
  #ax2.plot(ad_T_coor, ad_xi_h_r, '-b', alpha=0.6, label=r'Adiab, Real')
  #ax2.plot(ad_T_coor, ad_xi_h_i, '--b', alpha=0.6, label=r'Adiab, Imag')
  #ax2.plot(nad_T_coor, nad_xi_h_r, '-r', alpha=0.6, label=r'Non-Adiab, Real')
  #ax2.plot(nad_T_coor, nad_xi_h_i, '--r', alpha=0.6, label=r'Non-Adiab, Imag')
  #ax2.set_ylabel(r'Horizontal $\xi_h$', fontsize = 'small')
  #ax2.set_xlim(max(ad_T_coor)+0.1, min(ad_T_coor)+0.1)
  #ax2.set_xticklabels( () )
  #leg = ax2.legend(loc='upper left', fontsize = 'x-small')

  #ax3 = fig.add_subplot(413)
  #ax3.plot(ad_T_coor, nad_delp_r/max(nad_delp_r), '-b', alpha=0.6, label=r'Real$(\delta p)$')
  #ax3.plot(ad_T_coor, nad_delp_i/max(nad_delp_i), '--b', alpha=0.6, label=r'Imag$(\delta p)$')
  #ax3.plot(ad_T_coor, nad_delS_r/max(nad_delS_r), '-r', alpha=0.6, label=r'Real$(\delta S)$')
  #ax3.plot(ad_T_coor, nad_delS_i/max(nad_delS_i), '--r', alpha=0.6, label=r'Imag$(\delta S)$')
  #ax3.plot(ad_T_coor, nad_delL_r/max(nad_delL_r), '-c', alpha=0.6, label=r'Real$(\delta L)$')
  #ax3.plot(ad_T_coor, nad_delL_i/max(nad_delL_i), '-c', alpha=0.6, label=r'Imag$(\delta L)$')
  #ax3.set_xlim(max(ad_T_coor)+0.1, min(ad_T_coor)+0.1)
  #ax3.set_ylabel(r'Relative Change')
  #ax3.set_xticklabels( () )
  #leg = ax3.legend(loc='upper left', fontsize = 'x-small')

  #nad_x = nad_data['x']
  #int_dW = 0.0
  #dx = np.zeros(len(nad_x))
  #int_dW_arr = np.zeros(len(nad_x))
  #for i in range(1, len(nad_x)):
    #dx = nad_x[i] - nad_x[i-1]
    #int_dW += nad_dW_dx[i] * dx
    #int_dW_arr[i] = int_dW

  #ax4 = fig.add_subplot(414)
  #ax4.plot(ad_T_coor, nad_dW_dx/max(np.abs(nad_dW_dx)), '-b', label=r'$dW$/Max($dW$)')
  #ax4.plot(ad_T_coor, int_dW_arr/max(np.abs(int_dW_arr)), '-r', label=r'$W$/Max($W$)')
  #ax4.set_xlim(max(ad_T_coor)+0.1, min(ad_T_coor)+0.1)
  #ax4.set_xlabel(r'Temperature $\log T$ [K]')
  #ax4.set_ylim(-1.05, 1.05)
  #ax4.set_ylabel(r'Relative $W, dW/dx$')
  #txt = r'$W=\int dx \, (dW/dx)=$' + "{:.3f}".format(nad_loc['W']) + r' [$L(R_\star^3/GM_\star)^{1/2}$]'
  #ax4.annotate(txt, xy=(0.01, 0.80), xycoords='axes fraction', fontsize='small')
  #ax4.legend(loc='upper right', fontsize = 'small')

  #pdf_file = ad_eig_files[ad_ind].replace('.h5', '.pdf')
  #plt.savefig(pdf_file)
  #plt.close

#=================================================================================================================
def check_Hydr_Equil(dic, Lagr=True, Euler=False, file_out=None):
  """
  Plot and ensure that the input GYRE model is in hydrostatic equilibrium point by point
  @param dic: dictinary provided by mesa_gyre.read.read_gyre_in, containing data from the file.
  @type dic: dictionary
  @param file_out: full path to the output plot
  @type file_out: string
  @param Lagr, Euler: to check the hydrostatic equilibrium in Lagrangian vs. Eulerian coordinate
  @type Lagr, Euler: boolean
  @return: None
  @rtype: None
  """
  if type(dic) is not type({}):
    message = 'Error: plot_gyre: check_Hydr_Equil: First argument is not a dictionary!'
    raise SystemExit, message

  c_grav = 6.67428e-8 # cgs; as defined in mesa/const/const_def.f

  nn     = dic['nn']
  M_star = dic['M_star']
  R_star = dic['R_star']
  L_star = dic['L_star']
  ncol   = dic['ncol']
  rec    = dic['gyre_in']

  r_cm   = rec['radius']
  P      = rec['pressure']
  Rho    = rec['density']
  logT   = np.log10(rec['temperature'])
  q_div_xq = rec['q_div_xq']
  w      = q_div_xq

  m_gr   = np.zeros(nn)
  for i in range(0, nn-1): m_gr[i] = M_star * w[i] / (1.0 + w[i])
  m_gr[nn-1] = M_star
  dm = np.zeros(nn)
  dm[1:] = m_gr[1:] - m_gr[0:nn-1]
  dm[0]  = m_gr[0]
  #dm[0:nn-1] = m_gr[1:nn] - m_gr[0:nn-1]

  gravity= - c_grav * m_gr[:] / r_cm[:]**2.0

  dr = np.zeros(nn)
  dr[1:nn] = r_cm[1:nn] - r_cm[0:nn-1]
  dr[0] = r_cm[0]

  lhs = np.zeros(nn)
  rhs = np.zeros(nn)
  for i in range(1, nn):
    if Lagr:
      lhs[i] = (P[i-1] - P[i]) / ((dm[i-1] + dm[i]) / 2.0)
      rhs[i] = -c_grav * m_gr[i] / (4.0 * np.pi * r_cm[i]**4.0)
    if Euler:
      lhs[i] = (P[i-1] - P[i]) / ((dr[i-1] + dr[i]) / 2.0)
      rhs[i] = -Rho[i] * gravity[i]
  lhs[0] = lhs[1]
  rhs[0] = rhs[1]

  departure = np.abs(1.0 - lhs[:]/rhs[:])
  if Lagr: txt = 'Lagrangian'
  else: txt = 'Eulerian'
  print ' - plot_gyre: check_Hydr_Equil: ' + txt + ' frame'
  print '   Median of departure = %s' % (np.median(departure), )
  print '   Min = %s, Max= %s' % (min(np.abs(departure)), max(departure))

  if file_out:
    fig = plt.figure(figsize=(6,4), dpi=200)
    ax  = fig.add_subplot(111)
    plt.subplots_adjust(left=0.12, bottom=0.12)
    ax.set_xlim(logT.max(), logT.min())
    ax.set_ylim(-20, 1)
    ax.set_xlabel(r'log T')
    ax.set_ylabel(r'$\log Q = \log|1.0 - \frac{\rm LHS}{\rm RHS}|$')
    ax.plot(logT, np.log10(departure), lw=1, color='black', label=r'$\log Q$')
    ax.plot(logT, np.log10(np.abs(lhs)), lw=1, color='blue', label=r'$\log$(LHS)')
    ax.plot(logT, np.log10(np.abs(rhs)), lw=1, color='red', label=r'$\log$(RHS)')
    leg = ax.legend(loc=0, fontsize='x-small')
    plt.savefig(file_out, dpi=200)
    print ' - plot_gyre: check_Hydr_Equil: %s saved' % (file_out, )
    plt.close()

  return None

#=================================================================================================================
def gyre_in_columns(list_prof, xaxis=None, yaxis=None, list_lbls=None, xaxis_from=None, xaxis_to=None,
                    yaxis_from=None, yaxis_to=None, xlog=False, ylog=False, loc_leg=0, file_out=None):
  """
  Compare x vs. y for a list of input profiles read from gyre_in files.
  @param list_prof: list of gyre_in data as dictionaries, where each dictionary is returned by e.g. read.read_gyre_in()
  @type list_prof: list of dictionaries
  @param xaxis/yaxis: the quantities to put on the xaxis/yaxis
  @type xaxis/yaxis: string
  @return: None
  @rtype: None
  """
  n_prof = len(list_prof)
  if n_prof == 0:
    logging.error('plot_gyre: gyre_in_columns: Input list is empty')
    raise SystemExit

  #---------------------------
  # Define the Plot
  #---------------------------
  import itertools
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.18, right=0.98, bottom=0.12, top=0.97)
  colors = itertools.cycle(['black', 'blue', 'red', 'green', 'purple', 'yellow', 'cyan', 'grey'])
  linestyles = itertools.cycle(['solid', 'dashed', 'dashdot', 'dotted'])

  #---------------------------
  # Loop over dictionaries and Plot x vs y
  #---------------------------
  gyre_in    = list_prof[0]['gyre_in']
  names      = gyre_in.dtype.names + ('mass', )
  list_x_min = []; list_x_max = []
  list_y_min = []; list_y_max = []
  for i_dic, dic in enumerate(list_prof):
    nn      = dic['nn']
    ncol    = dic['ncol']
    M_star  = dic['M_star']
    R_star  = dic['R_star']
    L_star  = dic['L_star']
    gyre_in = dic['gyre_in']
    if xaxis not in names:
      logging.error('plot_gyre: gyre_in_columns: {0} not in columns names'.format(xaxis))
      raise SystemExit
    if yaxis not in names:
      logging.error('plot_gyre: gyre_in_columns: {0} not in columns names'.format(yaxis))
      raise SystemExit

    if xaxis == 'mass':
      w = gyre_in['q_div_xq']
      x = w * M_star / (1 + w) / Msun
    else:
      x       = gyre_in[xaxis]
    y       = gyre_in[yaxis]

    if xaxis == 'radius': x = x / R_star
    if yaxis == 'brunt_N2': y[y<=0] = 1e-99

    if xlog: x = np.log10(x)
    if ylog: y = np.log10(y)

    list_x_min.append(np.min(x))
    list_x_max.append(np.max(x))
    list_y_min.append(np.min(y))
    list_y_max.append(np.max(y))
    clr = colors.next()
    lst = linestyles.next()
    if list_lbls: lbl = list_lbls[i_dic]
    else: lbl = r''

    ax.plot(x, y, color=clr, linestyle=lst, label=lbl)

  #---------------------------
  # Annotations, Legend, Ranges
  #---------------------------
  if xaxis_from is None: xaxis_from = min(list_x_min)
  if xaxis_to   is None: xaxis_to   = max(list_x_max)
  if yaxis_from is None: yaxis_from = min(list_y_min)
  if yaxis_to   is None: yaxis_to   = max(list_y_max)
  ax.set_xlabel(r'{0}'.format(xaxis.replace('_', ' ')))
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylabel(r'{0}'.format(yaxis.replace('_', ' ')))
  ax.set_ylim(yaxis_from, yaxis_to)

  if list_lbls is not None: leg = ax.legend(loc=loc_leg, fontsize='small', framealpha=0.5)
  #plt.tight_layout()

  #---------------------------
  # Finalize the plot
  #---------------------------
  if file_out:
    plt.savefig(file_out, transparent=True, dpi=200)
    print ' - plot_gyre: gyre_in_columns: {0} saved'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def nablas_at_core_boundary(dic, file_out=None):
  """
  Plot the three nablas at the core boundary: nabla_ad, nabla_rad and nabla_T.
  This allows to test if nabla_ad and nabla_rad are equal at the very boundary.
  """
  if file_out is None: return
  nn      = dic['nn']
  ncol    = dic['ncol']
  M_star  = dic['M_star']
  R_star  = dic['R_star']
  L_star  = dic['L_star']
  gyre_in = dic['gyre_in']

  grada   = gyre_in['grada']
  gradT   = gyre_in['gradT']
  gradr   = get_gradr(dic)

  w       = gyre_in['q_div_xq']
  mass    = w * M_star / (1 + w) / Msun

  fig, ax = plt.subplots(1, figsize=(6,4))
  inset   = fig.add_axes([0.45, 0.60, 0.4, 0.3])
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97)

  ind_bdry = 0
  for i in range(len(mass)):
    str_ad = '{0:0.8f}'.format(grada[i])
    str_T  = '{0:0.8f}'.format(gradT[i])
    if str_ad == str_T: ind_bdry = i
  mass_core = mass[ind_bdry]
  grada_core = grada[ind_bdry]
  gradT_core = gradT[ind_bdry]
  gradr_core = gradr[ind_bdry]
  print '   M_cc = {0}'.format(mass_core)
  print '   gradT={0}, grada={1}'.format(gradT_core, grada_core)

  ax.fill_between([0, mass_core], y1=0, y2=2, color='blue', alpha=0.25, zorder=0)
  ax.plot(mass, gradT, lw=4, color='black', linestyle='solid', zorder=1, label=r'$\nabla$')
  ax.plot(mass, gradr, lw=1, color='red', linestyle='dashed', zorder=2, label=r'$\nabla_{\rm rad}$')
  ax.plot(mass, grada, lw=1, color='blue', linestyle='dashed', zorder=2, label=r'$\nabla_{\rm ad}$')

  inset.fill_between([0, mass_core], y1=0, y2=2, color='blue', alpha=0.25, zorder=0)
  inset.plot(mass, gradT, lw=4, color='black', linestyle='solid', zorder=1)
  inset.plot(mass, gradr, lw=1, color='red', linestyle='dashed', zorder=2)
  inset.plot(mass, grada, lw=1, color='blue', linestyle='dashed', zorder=2)

  ax.set_xlim(0, mass.max()*1.02)
  ax.set_ylim(0, 1)
  ax.set_xlabel(r'Enclosed Mass [M$_\odot$]')
  ax.set_ylabel(r'Temperature Gradients')
  ax.legend(loc=3)

  inset.set_xlim(mass_core*0.95, mass_core*1.05)
  inset.set_ylim(0.38, 0.4)

  plt.savefig(file_out, transparent=True)
  print ' - plot_gyre: nablas_at_core_boundary: saved {0}'.format(file_out)
  plt.close()

  return


#=================================================================================================================
def eig_columns(list_dic_eig, xaxis=None, yaxis=None, filter_eig_by='n_pg',
                freq_val=None, freq_err=None, P_val=None, P_err=None, n_pg_val=None,
                xaxis_from=None, xaxis_to=None, yaxis_from=None, yaxis_to=None,
                xtitle=None, ytitle=None, leg_loc=1, freq_unit='Hz', file_out=None):
  """
  Plot and compare one eigenfunction column for a list of eigenfunctions.
  @param list_dic_eig: eigenfunction data as read e.g. by read.read_multiple_gyre_files()
  @type list_dic_eig: list of dictionaries
  @param xaxis, yaxis: name of the variable to put on the x- and/or y-axis. It should be already availbe, except for
       xaxis='mass', which will be calculated internally from M_star and q_div_xq.
  @type xaxis, yaxis: string
  @param filter_eig_by: you want to compare a specific eigenfunction among different models. Then, you choose
       e.g. all n_pg=-20 modes and compare them. this filter sets the choice.
       available choices are: 'n_pg', 'freq', or 'P'
  @type filter_eig_by: string
  @param freq_val, freq_err, P_val, P_err, n_pg_val: parameters that set the range for the choice of filter.
  @type freq_val, freq_err, P_val, P_err: float
  @type n_pg_val: string
  @param leg_loc: specify the location of the legend
  @type leg_loc: integer
  @param freq_unit: the Frequency unit of the GYRE output calculations as written in the eigenfunction file attributes
  @type freq_unit: string
  @param file_out: the full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  possible_filters = ['n_pg', 'freq', 'P']
  if filter_eig_by not in possible_filters:
    raise SystemExit, 'Error: plot_gyre: eig_columns: Impossible choice of filter!'

  avail_cols = list_dic_eig[0].keys()
  extra_cols = ['logT']
  avail_cols = avail_cols + extra_cols
  if xaxis not in avail_cols:
    raise SystemExit, 'Error: plot_gyre: eig_columns: {0} not in available columns'.format(xaxis)
  if yaxis not in avail_cols:
    raise SystemExit, 'Error: plot_gyre: eig_columns: {0} not in available columns'.format(yaxis)

  if freq_unit == 'Hz':
    freq_conv_factor = cm.conversions()['Hz_to_cd']
  else:
    raise SystemExit, 'Error: plot_gyre: eig_columns: Conversion of {0} to c/d not supported yet'.format(freq_unit)

  #---------------------------
  # Prepare Plot
  #---------------------------
  import itertools
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax  = fig.add_subplot(111)
  plt.subplots_adjust(left=0.13, right=0.98, bottom=0.12, top=0.97)
  list_clr = itertools.cycle(['black', 'blue', 'red', 'green', 'purple', 'yellow', 'cyan', 'grey'])
  list_ls  = itertools.cycle(['solid', 'dashed', 'dashdot', 'dotted'])
  ax.axhline(y=0, color='black', lw=1, linestyle='solid')

  #---------------------------
  # Loop over dictionaries
  #---------------------------
  for i_dic, dic in enumerate(list_dic_eig):
      filename = dic['filename']
      if filter_eig_by == 'n_pg':
        fltr_val = dic[filter_eig_by]
      if filter_eig_by == 'freq':
        fltr_val = np.real(dic[filter_eig_by]) * freq_conv_factor # so, fltr_val is in c/d
      if filter_eig_by == 'P':
        fltr_val = 1.0/(np.real(dic['freq']) * freq_conv_factor)

      if filter_eig_by == 'n_pg' and fltr_val != n_pg_val: continue
      if filter_eig_by == 'freq' and ((fltr_val>freq_val+freq_err) or (fltr_val<freq_val-freq_err)): continue
      if filter_eig_by == 'P'    and ((fltr_val>P_val+P_err)       or  (fltr_val<P_val-P_err)):      continue

      print '   plot_gyre.eig_columns: Using {0}: W={1}'.format(filename, dic['W'])

      if xaxis == 'logT':
        xvals = np.log10(dic['T'])
      else:
        xvals = dic[xaxis]
      yvals = dic[yaxis]

      clr = list_clr.next()
      ls  = list_ls.next()

      ax.plot(xvals, yvals, color=clr, lw=2, linestyle=ls)

  #---------------------------
  # Annotations, Ranges, Legend
  #---------------------------
  if xaxis_from is None: xaxis_from = np.min(xvals)
  if xaxis_to   is None: xaxis_to   = np.max(xvals)
  if yaxis_from is None: yaxis_from = np.min(yvals)
  if yaxis_to   is None: yaxis_to   = np.max(yvals)
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)

  if xtitle is not None: ax.set_xlabel(xtitle)
  if ytitle is not None: ax.set_ylabel(ytitle)

  #---------------------------
  # Finalize the Plot
  #---------------------------
  if file_out:
    plt.savefig(file_out, dpi=200)
    print '   plot_gyre.eig_columns: save {0}'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def eig_nad_driving(list_dic_eig, xaxis=None, yaxis=None, filter_eig_by='n_pg',
                freq_val=None, freq_err=None, P_val=None, P_err=None, n_pg_val=None,
                xaxis_from=None, xaxis_to=None, yaxis_from=None, yaxis_to=None,
                xtitle=None, ytitle=None, list_lbls=None, leg_loc=1, freq_unit='Hz',
                file_out=None):
  """
  Plot and compare one eigenfunction column for a list of eigenfunctions.
  @param list_dic_eig: eigenfunction data as read e.g. by read.read_multiple_gyre_files()
  @type list_dic_eig: list of dictionaries
  @param xaxis, yaxis: name of the variable to put on the x- and/or y-axis. It should be already availbe, except for
       xaxis='mass', which will be calculated internally from M_star and q_div_xq.
  @type xaxis, yaxis: string
  @param filter_eig_by: you want to compare a specific eigenfunction among different models. Then, you choose
       e.g. all n_pg=-20 modes and compare them. this filter sets the choice.
       available choices are: 'n_pg', 'freq', or 'P'
  @type filter_eig_by: string
  @param freq_val, freq_err, P_val, P_err, n_pg_val: parameters that set the range for the choice of filter.
  @type freq_val, freq_err, P_val, P_err: float
  @type n_pg_val: string
  @param leg_loc: specify the location of the legend
  @type leg_loc: integer
  @param freq_unit: the Frequency unit of the GYRE output calculations as written in the eigenfunction file attributes
  @type freq_unit: string
  @param file_out: the full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  import copy
  n_eig = len(list_dic_eig)
  possible_filters = ['n_pg', 'freq', 'P']
  if filter_eig_by not in possible_filters:
    raise SystemExit, 'Error: plot_gyre: eig_nad_driving: Impossible choice of filter!'

  avail_cols = list_dic_eig[0].keys()
  extra_cols = ['logT']
  avail_cols = avail_cols + extra_cols
  if xaxis not in avail_cols:
    raise SystemExit, 'Error: plot_gyre: eig_nad_driving: {0} not in available columns'.format(xaxis)
  if yaxis not in avail_cols:
    raise SystemExit, 'Error: plot_gyre: eig_nad_driving: {0} not in available columns'.format(yaxis)

  if freq_unit == 'Hz':
    freq_conv_factor = cm.conversions()['Hz_to_cd']
  else:
    raise SystemExit, 'Error: plot_gyre: eig_nad_driving: Conversion of {0} to c/d not supported yet'.format(freq_unit)

  #---------------------------
  # Prepare Plot
  #---------------------------
  import itertools
  fig = plt.figure(figsize=(6,6), dpi=200)
  top  = fig.add_subplot(211)
  bot  = fig.add_subplot(212)
  plt.subplots_adjust(left=0.13, right=0.98, bottom=0.10, top=0.97, hspace=0.02, wspace=0.02)
  list_clr = itertools.cycle(['black', 'blue', 'red', 'green', 'purple', 'yellow', 'cyan', 'grey'])
  list_ls  = itertools.cycle(['solid', 'dashed', 'dashdot', 'dotted'])
  top.axhline(y=0, color='black', lw=1, linestyle='solid')
  bot.axhline(y=0, color='black', lw=1, linestyle='solid')

  #---------------------------
  # Loop over dictionaries
  #---------------------------
  min_W_arr = np.zeros(n_eig)
  max_W_arr = np.zeros(n_eig)
  for i_dic, dic in enumerate(list_dic_eig):
      if xaxis == 'logT':
        xvals = np.log10(copy.deepcopy(dic['T']))
      else:
        xvals = copy.deepcopy(dic[xaxis])
      nz = len(xvals)
      yvals = copy.deepcopy(dic[yaxis])
      if yaxis == 'dW_dx':
        positive = yvals > 0
        zero     = yvals == 0
        negative = yvals < 0
        yvals[positive] = np.log10(yvals[positive])
        yvals[zero]     = 0.0
        yvals[negative] = -np.log10(np.abs(yvals[negative]))

      dx       = dic['x'][1 : ] - dic['x'][ : -1]
      integrand= (dic['dW_dx'][1 : ] + dic['dW_dx'][ : -1]) / 2.
      W_cumul  = np.zeros(nz-1)
      W_net    = 0.0
      for i in range(nz-1):
        W_cumul[i] = np.sum(integrand[0:i+1] * dx[0:i+1])
        W_net      += integrand[i] * dx[i]
      log_W_cumul      = np.sign(W_cumul) * np.log10(np.abs(W_cumul))
      min_W_arr[i_dic] = np.min(log_W_cumul)
      max_W_arr[i_dic] = np.max(log_W_cumul)

      filename = dic['filename']
      if filter_eig_by == 'n_pg':
        fltr_val = dic[filter_eig_by]
      if filter_eig_by == 'freq':
        fltr_val = np.real(dic[filter_eig_by]) * freq_conv_factor # so, fltr_val is in c/d
      if filter_eig_by == 'P':
        fltr_val = 1.0/(np.real(dic['freq']) * freq_conv_factor)

      if filter_eig_by == 'n_pg' and fltr_val != n_pg_val: continue
      if filter_eig_by == 'freq' and ((fltr_val>freq_val+freq_err) or (fltr_val<freq_val-freq_err)): continue
      if filter_eig_by == 'P'    and ((fltr_val>P_val+P_err)       or  (fltr_val<P_val-P_err)):      continue

      print '   plot_gyre.eig_nad_driving: Using {0}: W={1}'.format(filename, dic['W'])

      clr = list_clr.next()
      ls  = list_ls.next()
      if list_lbls:
        lbl = list_lbls[i_dic]
      else:
        lbl = r''

      top.plot(xvals, yvals, color=clr, lw=2, linestyle=ls, label=lbl)

      bot.plot(xvals[:-1], log_W_cumul, color=clr, lw=2, linestyle=ls)
      bot.axhline(y=np.sign(dic['W'])*np.log10(np.abs(dic['W'])), linestyle='dashed', color='grey', lw=1.5, alpha=0.5)

  #---------------------------
  # Annotations, Ranges, Legend
  #---------------------------
  if xaxis_from is None: xaxis_from = np.min(xvals)
  if xaxis_to   is None: xaxis_to   = np.max(xvals)
  if yaxis_from is None: yaxis_from = np.min(yvals)
  if yaxis_to   is None: yaxis_to   = np.max(yvals)
  top.set_xlim(xaxis_from, xaxis_to)
  top.set_ylim(yaxis_from, yaxis_to)
  top.set_xticklabels(())
  if ytitle is not None: top.set_ylabel(ytitle)

  if xtitle is not None: bot.set_xlabel(xtitle)
  bot.set_ylabel(r'sign$(W)\,\times\,\log_{10}(W)$ ')
  bot.set_xlim(xaxis_from, xaxis_to)
  bot.set_ylim(np.min(min_W_arr)*1.10, np.max(max_W_arr)*1.10)

  if list_lbls:
    leg = bot.legend(loc=leg_loc, fontsize='medium')
    # leg.get_frame().set_alpha(0.5)

  #---------------------------
  # Finalize the Plot
  #---------------------------
  if file_out:
    plt.savefig(file_out, dpi=200)
    print '   plot_gyre.eig_nad_driving: save {0}'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def eig_rho_N_x_xi_r(list_dic_eig, xaxis=None, list_lbls=None, leg_loc=1, file_out=None):
  """
  Plot this quantity for each EigenFunction dictionary:
       sqrt(rho) . sqrt(N) . r^1.5 . xi_r

  @param list_dic_eig: eigenfunction data as read e.g. by read.read_multiple_gyre_files()
  @type list_dic_eig: list of dictionaries
  @param xaxis: name of the variable to put on the x-axis. It should be already availbe, except for
       xaxis='mass', which will be calculated internally from M_star and q_div_xq.<< double check
  @type xaxis: string
  @param leg_loc: specify the location of the legend
  @type leg_loc: integer
  @param file_out: the full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  n_eig = len(list_dic_eig)
  if n_eig == 0:
    logging.error('plot_gyre: eig_rho_N_x_xi_r: empty input list!')
    raise SystemExit

  if xaxis not in list_dic_eig[0].keys():
    logging.error('plot_gyre: eig_rho_N_x_xi_r: {0} not availabe in eigenfunction output file'.format(xaxis))
    raise SystemExit

  #---------------------------
  # Prepare Plot
  #---------------------------
  import itertools
  fig = plt.figure(figsize=(6,6), dpi=200)
  ax  = fig.add_subplot(111)
  plt.subplots_adjust(left=0.13, right=0.98, bottom=0.10, top=0.97, hspace=0.02, wspace=0.02)
  list_clr = itertools.cycle(['black', 'blue', 'red', 'green', 'purple', 'yellow', 'cyan', 'grey'])
  list_ls  = itertools.cycle(['solid', 'dashed', 'dashdot', 'dotted'])

  #---------------------------
  # Iterate over dics and make plots
  #---------------------------
  for i_eig, eig in enumerate(list_dic_eig):
    xi_r    = np.real(eig['xi_r'])
    As      = eig['As']
    m       = eig['m']
    x       = eig['x']
    rho     = eig['rho']
    g       = m/x**2
    N2      = g/x * As
    neg     = N2 <= 0
    N2[neg] = 1e-99
    N       = np.sqrt(N2)
    coeff   = np.sqrt(rho) * np.sqrt(N) * np.power(x, 1.5)
    var     = coeff * xi_r

    xvals   = eig[xaxis]
    if xaxis == 'm': xvals = xvals / eig['M_star']

    ax.plot(xvals, var, linestyle=list_ls.next(), color=list_clr.next())

  #---------------------------
  # Finalize the plot
  #---------------------------
  if file_out:
    plt.savefig(file_out, dpi=200)
    print ' - plot_gyre: eig_rho_N_x_xi_r: save {0}'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def show_grad_Rho(dic, xrange=[], yrange=[], file_out=None):

  gyre_in = dic['gyre_in']
  R_star  = dic['R_star']
  M_star  = dic['M_star']
  radius  = gyre_in['radius'] 
  x       = radius / R_star
  gradr   = get_gradr(dic)
  grada   = gyre_in['grada']

  tup_grad_Rho = eval_Rho_gradient(dic)
  dr      = tup_grad_Rho[0]
  dRho    = tup_grad_Rho[1]
  dRho_dr = tup_grad_Rho[2]
  adim    = dRho_dr * R_star**4 / M_star


  fig, (top, bot) = plt.subplots(2, figsize=(6,6), sharex=True)
  lft = bot.twinx()
  plt.subplots_adjust(left=0.13, right=0.98, bottom=0.08, top=0.98)

  top.plot(x, dRho, 'k.-')
  bot.plot(x, adim, 'k.-')
  lft.plot(x, gradr, 'r--', label=r'$\nabla_{\rm rad}$')
  lft.plot(x, grada, 'b-', label=r'$\nabla_{\rm ad}$')

  if xrange: top.set_xlim(xrange[0], xrange[1])
  if yrange: top.set_ylim(yrange[0], yrange[1])
  top.set_ylabel(r'$d\rho$ [g cm$^{-3}$]')
  bot.set_ylabel(r'$(R_\star^4/M_\star)\,d\rho/dr$')
  bot.set_xlabel(r'Scaled Radius $x=r/R_\star$')
  # bot.set_yscale('log')
  leg = lft.legend(loc=0, fontsize=12)

  plt.savefig(file_out)
  print ' -show_grad_Rho: saved: {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def eval_Rho_gradient(dic):
  """
  Evaluate the first derivative of the density by a finite differences
  @param dic: gyre input data 
  @type dic: dict 
  @return: dr, d_Rho and d_Rho_dr 
  @rtype: tuple
  """
  gyre_in = dic['gyre_in']
  R_star  = dic['R_star']
  radius  = gyre_in['radius'] #/ R_star
  Rho     = gyre_in['density']
  n       = len(radius)
  dr      = np.zeros(n, dtype=np.float64)
  dRho    = np.zeros(n, dtype=np.float64)
  dr[0:n-1]   = radius[1:n] - radius[:n-1]
  dRho[0:n-1] = Rho[1:n] - Rho[0:n-1]
  dRho_dr = np.zeros(n, dtype=np.float64)
  dRho_dr[:n-1] = dRho[:n-1] / dr[:n-1]

  return (dr, dRho, dRho_dr)

#=================================================================================================================
def get_gradr(dic):
  """
  Calculate \nabla_{\rm rad} based on different input GYRE columns. Note that
  \nabla_{\rm rad} = \frac{3}{16 \pi a c G}\frac{\kappa_r L P}{m_r T^4}
  all the required columns are available in dic['gyre_in'] numpy record array
  @param dic: dictionary with all data read from gyre_in file
  @type dic: dictionary
  @return: vector giving \nabla_{\rm rad}
  @rtype: numpy array
  """
  c       = 2.99792458e10
  Boltz   = 5.670400e-5
  a       = 4. * Boltz / c
  nn      = dic['nn']
  ncol    = dic['ncol']
  M_star  = dic['M_star']
  R_star  = dic['R_star']
  L_star  = dic['L_star']
  gyre_in = dic['gyre_in']
  kappa_r = gyre_in['kappa']
  Lum     = gyre_in['luminosity']
  P       = gyre_in['pressure']
  T       = gyre_in['temperature']
  q_div_xq= gyre_in['q_div_xq']
  m_r     = q_div_xq * M_star / (1 + q_div_xq)
  const   = 3.0 / (16.0 * np.pi * a * c * G)
  gradr   = const * (kappa_r * Lum * P) / (m_r * np.power(T, 4.0))

  return gradr

#=================================================================================================================
def correct_gyre_in_N2_column_in_conv_zones(dic):
  """
  It happens in MESA that the Brunt-Vaisala frequency is non-zero inside convective regions (gradr > gradL).
  Indeed, this is wrong by definition. Thus, such regions should be identified and replaced with reasonable
  values in the brunt_N2 column.
  Once we identify such regions, if they exist in the input model, then they are replaced with the minimum
  value of brunt_N2 across the whole model (which could be zero or a negative number).
  @param dic: GYRE input model as returned by e.g. read.read_gyre_in().
  @type dic: dictionary
  @return: the same format as input, with the brunt_N2 column fixed, if it had an inconsistency.
  @rtype: dict
  """
  dic     = dic.copy()
  gyre_in = dic['gyre_in']

  gradr   = get_gradr(dic)
  grada   = gyre_in['grada']
  N2      = gyre_in['brunt_N2']
  conv    = np.where(gradr - grada >= 0, 1, 0)
  pos_N2  = np.where(N2 > 0, 1, 0)
  product = conv * pos_N2
  ind_bad = np.where(product == 1)[0]
  n_bads  = len(ind_bad)
  if n_bads == 0: return dic

  print ' - correct_gyre_in_N2_column_in_conv_zones: Found {0} Inconsistent mesh points'.format(n_bads)

  min_N2  = np.min(N2)
  new_N2  = N2
  new_N2[ind_bad] = min_N2
  dic['brunt_N2'] = new_N2

  print '   Returning the Fixed GYRE input data ...'

  return dic

#=================================================================================================================

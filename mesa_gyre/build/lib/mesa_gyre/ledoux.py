
import sys, os, glob
import logging
import numpy as np 
import commons, tar

#=========================================================================================
#=========================================================================================
#=========================================================================================
#=========================================================================================
def get_radial_order_in_P_range(dic, el=0, em=0, P_from=None, P_to=None):
  """
  Since the rotation rates brings in modes of different radial order, "n_pg" into the 
  observed range (specified by P_from and P_to), this routine gives the lower and upper 
  n_pg of modes in the observed range, for the inertial modes specified by their degree 
  "el" and azimuthal order "em".
  """
  keys = dic.keys()
  the_key = 'P_inertial_l{0:1d}_m{1:+1d}'.format(el, em)
  if the_key not in keys:
    logging.error('ledoux: get_radial_order_in_P_range: call dP_for_inertial_modes() first')
    print 'Error: ledoux: get_radial_order_in_P_range: key={0} not found'.format(the_key)
    raise SystemExit, '    call dP_for_inertial_modes() first'

  if P_from is None or P_to is None:
    logging.error('ledoux: get_radial_order_in_P_range: P_from and P_to should be both set')
    raise SystemExit, 'Error: ledoux: get_radial_order_in_P_range: P_from and P_to should be both set'

  n_pg_key = 'n_pg_l{0:1d}_m{1:+1d}'.format(el, em)
  f_key    = the_key.replace('P_inertial_', 'freq_inertial_')

  n_pg     = dic['n_pg']
  per      = dic[the_key]
  freq     = dic[f_key]

  # print len(n_pg), len(per), len(freq)
  # for i in range(10):
  #   print i, freq[i], per[i], n_pg[i]

  return None

#=========================================================================================
def count_num_modes_in_period_range(dic, el=0, em=0, P_from=None, P_to=None):
  """
  Enumerate the number of modes (using their periods), within (and including) the lower and
  upper period ranges, P_from and P_to. They should be already splitted by calling 
  ledoux.from_norot_to_corot_frame().
  Therefore, the input "dic" should already have few fields like "P_corot_l2_m-1".
  """
  keys = dic.keys()
  the_key = 'P_inertial_l{0:1d}_m{1:+1d}'.format(el, em)
  if the_key not in keys:
    logging.error('ledoux: count_num_modes_in_period_range: call dP_for_inertial_modes() first')
    print 'Error: ledoux: count_num_modes_in_period_range: key={0} not found'.format(the_key)
    raise SystemExit, '    call dP_for_inertial_modes() first'

  if P_from is None or P_to is None:
    logging.error('ledoux: count_num_modes_in_period_range: You should specify both P_from and P_to.')
    raise SystemExit, 'Error: ledoux: count_num_modes_in_period_range: You should specify both P_from and P_to.'

  P_l_m = dic[the_key]
  ind   = np.where((P_l_m >= P_from) & (P_l_m <= P_to))[0]

  return len(ind)

#=========================================================================================
def dP_for_inertial_modes(dic):
  """
  Compute period spacing, dP, for those modes that are splitted due to the effect of rotation
  by Ledoux constant. These set of modes are returned by calling ledoux.from_norot_to_corot_frame()
  and have a key names like "freq_l2_m+2" in "dic".
  """
  keys = dic.keys()
  n_inert = 0
  inert_keys = []
  for key in keys:
    if 'freq_inertial_' in key:
      n_inert += 1
      inert_keys.append(key)
  
  if n_inert == 0:
    print ' - dP_for_inertial_modes: No splitting computed yet!'
    print '   You should call from_norot_to_corot_frame() first'
    return dic 

  # Calculate dP for inertial modes
  for key in inert_keys:
    P_key  = key.replace('freq_inertial_', 'P_inertial_')
    n_key  = key.replace('freq_inertial_', 'n_pg_')
    dP_key = 'd' + P_key # P_key.replace('P_inertial_', 'dP_inertial_')
    n_pg   = dic[n_key]
    ind    = np.where(n_pg < 0)[0]
    f_l_m  = dic[key][ind]
    n_f    = len(f_l_m)
    P_l_m  = 1.0 / f_l_m
    dP_l_m = np.zeros(n_f-1, dtype=np.float64)
    dP_l_m[0:n_f-1] = P_l_m[:-1] - P_l_m[1:]

    dic[P_key] = P_l_m
    dic[dP_key] = dP_l_m

  return dic

#=========================================================================================
def convert_gyre_freq_unit(dic, freq_unit):
  """
  Convert and adapt the GYRE output frequencies to the rotation frequency unit, so that 
  both would be in the same unit. 
  @param dic: all GYRE short-summary output data
  @type dic: dictionary
  @param freq_unit: the final frequency unit for the "freq" array in "dic". Valid options are:
        - Hz: The SI unit 
        - uHz: micro-Hz
        - cd: cycles per day 
  @type freq_unit: string
  @return: a copy of the input dictionary, with the real and imaginary parts of the "freq" 
        column multiplied by a suitable conversion factor (from mesa_gyre.commons.conversions())
        is returned to adapt to the requested unit. Other items in the input dictionary are kept 
        intact.
  @rtype: dictionary
  """
  dic_conv  = commons.conversions()
  freq_unit = freq_unit.lower()
  supported_units = ['hz', 'uhz', 'cd']

  if freq_unit not in supported_units:
    logging.error('ledoux: convert_gyre_freq_unit: freq_unit:{0} not supported'.format(freq_unit))
    raise SystemExit, 'Error: ledoux: convert_gyre_freq_unit: freq_unit:{0} not supported'.format(freq_unit)

  orig_freq_unit = dic['freq_units'].lower()
  if orig_freq_unit not in supported_units:
    logging.error('ledoux: convert_gyre_freq_unit: ORIGINAL freq_unit:{0} not supported'.format(orig_freq_unit))
    raise SystemExit, 'Error: ledoux: convert_gyre_freq_unit: ORIGINAL freq_unit:{0} not supported'.format(orig_freq_unit)

  if freq_unit == orig_freq_unit: return dic
  
  import copy
  out = copy.deepcopy(dic)
  if orig_freq_unit   == 'hz' and freq_unit == 'cd':  
    conv_factor       = dic_conv['Hz_to_cd']
    new_unit          = 'cd'
  elif orig_freq_unit == 'hz' and freq_unit == 'uhz': 
    conv_factor       = dic_conv['Hz_to_uHz']
    new_unit          = 'uHz'
  elif orig_freq_unit == 'uhz' and freq_unit == 'hz': 
    conv_factor       = dic_conv['uHz_to_Hz']
    new_unit          = 'Hz' 
  elif orig_freq_unit == 'uhz' and freq_unit == 'cd': 
    conv_factor       = dic_conv['uHz_to_cd']
    new_unit          = 'cd'
  elif orig_freq_unit == 'cd' and freq_unit == 'hz':  
    conv_factor       = dic_conv['cd_to_Hz']
    new_unit          = 'Hz'
  elif orig_freq_unit == 'cd' and freq_unit == 'uhz': 
    conv_factor       = dic_conv['cd_to_uHz']
    new_unit          = 'uHz'
  else:
    logging.error('ledoux: convert_gyre_freq_unit: Unanticipated error occured !!!')
    raise SystemExit, 'Error: ledoux: convert_gyre_freq_unit: Unanticipated error occured !!!'

  out['freq']       *= conv_factor
  out['freq_units'] = new_unit

  return out 

#=========================================================================================
def from_corot_to_inertial_frame(dic):
  """
  By calling ledoux.get_freq_interial() routine for all items in "dic" that contain 
  the "_corot_" in their key name, we translate the frequencies from the co-rotating 
  frame to the inertial frame by 
        freq_inertial = freq_corot + em * freq_rot
  @return: extented "dic" with new frequency key/value pairs for all el and em, with
        the _corot_ replaced by _inertial_
  @rtype: dict
  """
  keys = dic.keys()
  corot_keys = []
  for key in keys:
    if 'freq_corot_' in key:
      corot_keys.append(key)  
  n_corot    = len(corot_keys)
  if n_corot == 0:
    logging.error('ledoux: from_corot_to_inertial_frame: no key found with freq_corot_ in it!')
    raise SystemExit, 'Error: ledoux: from_corot_to_inertial_frame: no key found with freq_corot_ in it!'

  for key in corot_keys:
    inert_key = key.replace('freq_corot_', 'freq_inertial_')
    ind_m     = key.rfind('_m') + 3
    em        = int(key[ind_m])
    freq_rot  = dic['Led_freq_rot']
    freq_corot= dic[key]
    freq_inert= tar.get_freq_interial(em, freq_rot, freq_corot)

    dic[inert_key] = freq_inert

  return dic 

#=========================================================================================
def from_norot_to_corot_frame(dic, freq_rot, min_el=None, max_el=None, freq_unit='cd'):
  """
  Starting from non-rotating frequencies, freq_norot, the frequencies in the co-rotating frame, 
  called freq_corot, come from the first linear perturbation of rotation, using the Ledoux 
  constants:
      freq_corot = freq_norot + m * beta * freq_rot
  Indeed, the radial (el=0) modes exhibit no splittings. 
  @param freq_rot: rotation frequency. The physical unit of freq_rot is passed through 
      "freq_unit" argument. Then, the non-rotating pulsation frequencies in "dic" are adapted
      to "freq_unit" too, for consistency.
      Note that freq_rot is free of the 2pi factor, and is pure frequency, NOT angular frequency.
  @type freq_rot: float 
  @param freq_unit: The physical unit of "freq_rot". The non-rotating pulsation frequencies in 
      "dic" are adapted to "freq_unit", for consistency. Allowed values are: Hz, uHz and cd
  @type freq_unit: string
  """
  names = dic.keys()
  if 'beta' not in names:
    logging.error('ledoux: from_norot_to_corot_frame: field name "beta" unavailable in GYRE output!')
    raise SystemExit, 'Error: ledoux: from_norot_to_corot_frame: field name "beta" unavailable in GYRE output!'

  if dic['freq_units'].lower() != freq_unit.lower():
    dic   = convert_gyre_freq_unit(dic, freq_unit)

  el      = dic['l']
  n       = len(el)
  f_norot = np.real(dic['freq'])
  beta    = np.real(dic['beta'])
  if min_el is None: min_el = np.min(el)
  if max_el is None: max_el = np.max(el)

  for l in range(min_el, max_el+1):
    # if l == 0: continue
    ind   = np.where(el == l)[0]
    m     = len(ind)
    if m == 0:
      logging.error('ledoux: from_norot_to_corot_frame: len(ind) cannot be zoro for el={0}'.format(l))
      raise SystemExit, 'Error: ledoux: from_norot_to_corot_frame: len(ind) cannot be zoro for el={0}'.format(l)

    for em in range(-l, l+1):
      # if em == 0: continue
      str_l_m = 'freq_corot_l{0:1d}_m{1:+1d}'.format(l, em)
      str_npg = 'n_pg_l{0:1d}_m{1:+1d}'.format(l, em)
      f_corot = np.zeros(m, dtype=np.float64)
      n_pg_new= np.zeros(m, dtype=np.int8)
      # We revert the frequency list, so that the resulting periods be in increasing order
      # This is more consistent for getting period spacing
      # f_corot = f_norot[ind][::-1] + em * beta[ind][::-1] * freq_rot
      f_corot = f_norot[ind] + em * beta[ind] * freq_rot
      n_pg_new= dic['n_pg'][ind]
      dic[str_l_m] = f_corot
      dic[str_npg] = n_pg_new

  dic['Led_freq_rot'] = freq_rot

  return dic 

#=========================================================================================

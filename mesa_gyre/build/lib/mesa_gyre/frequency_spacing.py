
import os, glob
import numpy as np
import commons, read

#=================================================================================================================
#=================================================================================================================
def gen_list_dic_df(list_dic, freq_unit='Hz'):
  """
  This function receives a list of dictionaries retured from e.g. read.read_multiple_gyre_files, and returns a 
  dictionary of frequency spacing information per each of the input record arrays by calling gen_df.
  @param list_dic: the list of dictionaries, where each record array is associated to one .h5 file.
  @type list_dic: list of dictionaries
  @param freq_unit: this specifies the frequency unit of the GYRE output. It must be specified to make the values 
     in the right unit. It is passed later to gen_dP as an argument
  @type freq_unit: string
  @return: list of dictionaries, one dictionary per input record array. For the valid key-value pairs returned by 
     each dictionary, see the help from gen_df
  @rtype: list of dictionaries
  """
  n_rec = len(list_dic)
  if n_rec == 0:
    message = 'Error: frequency_spacing: gen_list_dic_df: input list is empty!'
    raise SystemExit, message
  
  list_dic_df = []
  for i_dic, dic in enumerate(list_dic):
    dic_df = gen_df(dic, freq_unit=freq_unit)
    
    # update dic with period spacing information, and return the updated dictionary
    for key, value in dic_df.items(): dic[key] = value
    list_dic_df.append(dic)
    
  return list_dic_df

#=================================================================================================================
def gen_df(dic, freq_unit='Hz'):
  """
  """
  
  names = dic.keys()
  dic_names = {}
  for key in names:
    dic_names[key] = 0.0
  requested_keys = ['l', 'n_p', 'n_g', 'n_pg', 'freq', 'omega', 'E', 'E_norm', 'W', 'M_star', 'R_star', 'L_star']
  dic_keys = read.check_key_exists(dic_names, requested_keys)
  flag_l     = dic_keys['l']
  flag_np    = dic_keys['n_p']
  flag_ng    = dic_keys['n_g']
  flag_npg   = dic_keys['n_pg']
  flag_freq  = dic_keys['freq']
  flag_E     = dic_keys['E']
  flag_E_norm= dic_keys['E_norm']
  flag_W     = dic_keys['W']
  flag_M_star= dic_keys['M_star']
  flag_R_star= dic_keys['R_star']
  flag_L_star= dic_keys['L_star']
  
  if not flag_l or not flag_npg or not flag_freq:
    message = 'Error: frequency_spacing: gen_df: Essenstial fields (l, n_pg, freq) are missing!'
    raise SystemExit, message
  
  if freq_unit != 'Hz':
    dic_convert = commons.conversions()
    uHz_to_Hz = dic_convert['uHz_to_Hz']
    cd_to_Hz  = dic_convert['cd_to_Hz']
    if freq_unit == 'None':
      message = 'Error: period_spacing: gen_dP: this freq_unit not supported yet.'
      raise SystemExit, message
    if freq_unit == 'uHz':
      freq_re_all = dic['freq'].real * uHz_to_Hz
      freq_im_all = dic['freq'].imag * uHz_to_Hz
    if freq_unit == 'per_day' or freq_unit == 'cd':
      freq_re_all = dic['freq'].real * cd_to_Hz
      freq_im_all = dic['freq'].imag * cd_to_Hz
  elif freq_unit == 'Hz':    
    freq_re_all = dic['freq'].real
    freq_im_all = dic['freq'].imag
  else:
    message = 'Error: period_spacing: gen_dP: Sth wrong with freq_unit: %s' % (freq_unit)
    raise SystemExit, message
  
  dic_df = {}

  el_all = dic['l']
  el_0_indx = np.where((el_all == 0))[0]
  n_el_0 = len(el_0_indx)
  el_1_indx = np.where((el_all == 1))[0]
  n_el_1 = len(el_1_indx)
  el_2_indx = np.where((el_all == 2))[0]
  n_el_2 = len(el_2_indx)
  el_3_indx = np.where((el_all == 3))[0]
  n_el_3 = len(el_3_indx)
  el_high   = np.where((el_all > 3))[0]
  n_el_high = len(el_high)
  has_el_0 = False; has_el_1 = False; has_el_2 = False; has_el_3 = False; has_el_high = False
  el_avail = list( set(el_all) )
  num_el_avail = len(el_avail)

  if n_el_0 > 0:    
    has_el_0 = True
    #num_el_avail += 1
    #el_avail.append(0)
  if n_el_1 > 0:
    has_el_1 = True
    #num_el_avail += 1
    #el_avail.append(1)
  if n_el_2 > 0:
    has_el_2 = True
    #num_el_avail += 1
    #el_avail.append(2)
  if n_el_3 > 0:
    has_el_3 = True
    #num_el_avail += 1
    #el_avail.append(3)
  if n_el_high > 0:
    has_el_high = True
    #num_el_avail += 10
    #el_avail.append([k for k in range(4, max(el_all)+1)])
    #num_el_avail += len(range(4, max(el_all)+1))
  dic_df['el_avail'] = el_avail
    
  np_all  = dic['n_p']
  ng_all  = dic['n_g']
  npg_all = dic['n_pg']
  per_all = 1.0/freq_re_all              # sec
  if flag_W:  W_all = dic['W']
  if flag_E: E_all  = dic['E']
  if flag_E_norm: E_norm_all = dic['E_norm']
 
  for which in range(num_el_avail):
    which_el = el_avail[which]
    which_el_indx = np.where((el_all == which_el))[0]  # fetch only for one available el
    n_which_el = len(which_el_indx)
    if n_which_el < 1: continue
    
    which_el_freq_re = freq_re_all[which_el_indx]
    which_el_freq_im = freq_im_all[which_el_indx]
    which_el_np   = np_all[which_el_indx]
    which_el_ng   = ng_all[which_el_indx]
    which_el_npg  = npg_all[which_el_indx]
    which_el_per  = per_all[which_el_indx]
    if flag_W: which_el_W  = W_all[which_el_indx]
    else: which_el_W = np.zeros(n_which_el) - 99.0
    if flag_E: which_el_E  = E_all[which_el_indx]
    else: which_el_E = np.zeros(n_which_el) - 99.0
    if flag_E_norm: which_el_E_norm  = E_norm_all[which_el_indx]
    else: which_el_E_norm = np.zeros(n_which_el) - 99.0
    
    which_pm_indx = np.where((which_el_npg >= 0))[0] # for this availale el, fetch only the relevant modes
    n_which_pm = len(which_pm_indx)
    if n_which_pm < 1: continue
    which_pm_freq_re = which_el_freq_re[which_pm_indx]
    which_pm_freq_im = which_el_freq_im[which_pm_indx]
    which_pm_ng   = which_el_ng[which_pm_indx]
    which_pm_np   = which_el_np[which_pm_indx]
    which_pm_npg  = which_el_npg[which_pm_indx]
    which_pm_per  = which_el_per[which_pm_indx]
    which_pm_W    = which_el_W[which_pm_indx]
    which_pm_E    = which_el_E[which_pm_indx]
    which_pm_E_norm = which_el_E_norm[which_pm_indx]
    
    pm_sort_indx  = np.argsort(which_pm_npg) # sort in increasing order
    which_pm_freq_re[:] = which_pm_freq_re[pm_sort_indx]    
    which_pm_freq_im[:] = which_pm_freq_im[pm_sort_indx]
    which_pm_ng[:]      = which_pm_ng[pm_sort_indx]
    which_pm_np[:]      = which_pm_np[pm_sort_indx]
    which_pm_npg[:]     = which_pm_npg[pm_sort_indx]
    which_pm_per[:]     = which_pm_per[pm_sort_indx]
    which_pm_W[:]       = which_pm_W[pm_sort_indx]
    which_pm_E[:]       = which_pm_E[pm_sort_indx]
    which_pm_E_norm[:]  = which_pm_E_norm[pm_sort_indx]
    
    which_pm_df         = which_pm_freq_re[1:] - which_pm_freq_re[0:-1]  # df_n = f_{n+1} - f_{n}
    
    which_pm_stbl_indx = np.where((which_pm_W < 0))[0]
    which_pm_unst_indx = np.where((which_pm_W >= 0))[0]
    
    which_el_str = str(which_el)
    key_n_which_el = 'n_el_' + which_el_str
    dic_df[key_n_which_el] = n_which_el
    key_which_pm_el = 'el_' + which_el_str
    dic_df[key_which_pm_el] = el_all[which_el_indx]
    key_n_which_pm = key_n_which_el + '_pm'
    dic_df[key_n_which_pm] = n_which_pm

    key_which_pm_freq_re = 'el_' + which_el_str + '_pm_freq_re'
    dic_df[key_which_pm_freq_re] = which_pm_freq_re
    key_which_pm_freq_im = 'el_' + which_el_str + '_pm_freq_im'
    dic_df[key_which_pm_freq_im] = which_pm_freq_im

    key_which_pm_np = 'el_' + which_el_str + '_pm_np'
    dic_df[key_which_pm_np] = which_pm_np
    key_which_pm_ng = 'el_' + which_el_str + '_pm_ng'
    dic_df[key_which_pm_ng] = which_pm_ng
    key_which_pm_npg = 'el_' + which_el_str + '_pm_npg'
    dic_df[key_which_pm_npg] = which_pm_npg

    key_which_pm_E = 'el_' + which_el_str + '_pm_E'
    dic_df[key_which_pm_E] = which_pm_E
    key_which_pm_E_norm = 'el_' + which_el_str + '_pm_E_norm'
    dic_df[key_which_pm_E_norm] = which_pm_E_norm
    
    key_which_pm_df= 'el_' + which_el_str + '_pm_df'
    dic_df[key_which_pm_df] = which_pm_df
    
  #n_old = n_pm
  #fixed_dic = exclude_repeat(dic_df)
  #dic_df = fixed_dic
  #n_new = dic_df['n_pm']
  #if n_old != n_new:
    #print 'gen_dP: n_pm reduced from ', n_old, ' to ', n_new
    
  return dic_df

#=================================================================================================================
def df_features_to_recarr(list_dic, el=0, n_pg=1):
  """
  Convert the list of dictionaries to a record array
  """
  n_dic = len(list_dic)
  if n_dic == 0:
    message = 'Error: frequency_spacing: df_features_to_recarr: The input list is empty'
    raise SystemExit, message

  str_el = str(el)
  key_el          = 'el_avail'
  key_n_pg        = 'el_' + str_el + '_pm_npg'
  key_freq        = 'el_' + str_el + '_pm_freq_re'
  
  list_records = []
  for i_dic, dic in enumerate(list_dic):
    m_ini      = dic['m_ini']
    eta        = dic['eta']
    ov         = dic['ov']
    sc         = dic['sc']
    Z          = dic['Z']
    Xc         = dic['Xc']
    Yc         = dic['Yc']
    
    M_star     = dic['M_star'] / Msun
    R_star     = dic['R_star'] / Rsun
    L_star     = dic['L_star'] / Lsun
    Teff       = Tsun * np.power((L_star/R_star**2.), 1./4.)
    logg       = logg_sun + np.log10(M_star) - 2.*np.log10(R_star)
    
    freq       = dic[key_freq]
    n_modes    = len(freq)
    #ind_el     = [i for i in range(n_modes) if dic['l'][i] == el]
    #ind_n_pg   = [i for i in range(n_modes) if dic['n_pg'][i] == n_pg]
    ind_el_n_pg= (dic['l'] == el) & (dic['n_pg'] == n_pg)
    if not any(ind_el_n_pg):
      message = 'Error: frequency_spacing: df_features_to_recarr: No index matching el=%s and n_pg=%s. Check again!' % (el, n_pg)
      raise SystemExit, message
    freq       = freq[ind_el_n_pg]  # only the frequency for the mode with el and n_pg 
    
    records = [ov, Z, Xc, M_star, R_star, L_star, Teff, logg, 
               freq]
    
    list_records.append(records)
  
  # Collect the list of columns in the same was as they are going to be stored in the record array
  list_names = ['ov', 'Z', 'Xc', 'M_star', 'R_star', 'L_star', 'Teff', 'logg', key_freq]
  names = ','.join(list_names)
  # convert to a numpy record array
  arr = np.array(list_records)
  recarr = np.core.records.fromarrays(arr.transpose(), names=names)
  
  return recarr, arr

#=================================================================================================================
#=================================================================================================================

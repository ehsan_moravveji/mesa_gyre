#! /usr/bin/python 


#import numpy as np

import stars, read, commons, sample_grid, plot_hrd
import param_tools as pt
import slurm_commons as slc

#=================================================================================================================

dic_star = stars.KIC_10526294()
Teff = dic_star['Teff']
err_Teff = dic_star['err_Teff_1s']
logg = dic_star['logg']
err_logg = dic_star['err_logg_1s']

path_repo = commons.set_paths()['path_base']
dic_param = commons.set_srch_param()
mass_from = dic_param['mass_from']
mass_to   = dic_param['mass_to']
mass_step = dic_param['mass_step']
mass_fmt  = dic_param['mass_fmt']

dic_grid = sample_grid.scan_grid(Teff, err_Teff, logg, err_logg)
print 'How many entries in dic_grid? %s' % (len(dic_grid), )

#for j in range(0, len(dic_grid), 100):
  #print 'j:%s, fn:%s' % (j, dic_grid[j]['filename'])

print
print dic_grid[0]['filename']
print dic_grid[-1]['filename']

list_dic_files = slc.get_list_dic_files(path_repo, mass_from, mass_to, mass_step, mass_fmt)
hist_files = []
for dic_files in list_dic_files:
  which_hist_files = dic_files['hist_files']
  for hf in which_hist_files: hist_files.append(hf)
n_hist = len(hist_files)  
print 'How many hist files? %s' % (n_hist, )

#plot_hrd.plot_kiel_for_mesa_gyre_grid(dic_grid, list_dic_stars = [dic_star])

#=================================================================================================================




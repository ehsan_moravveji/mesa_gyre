#! /usr/bin/python

"""
Public interface to the MESA-GYRE grid of models and pulsation properties
Contact ehsan@ster.kuleuven.be for enquiries/questions.

Usage:
This module can either be imported, or called directly from the command line.
For instance
   >>> python --query_grid
   >>> query_grid.fetch_tar()

Pleaes take your time to read the help for each function, e.g. 
   >>> import query_grid
   >>> dir(query_grid)
   >>> help(extract_tarball)
   
The whole idea is to provide a single tarball per each value of metalicity Z, and
initial rotation rate, eta. In this way, we optimize the number, size and content 
of each tarfile; instead, each tarfile is treated as a directory where one can fetch
the desired files based on a limit of their parameters.
Therefore, in the near future, we will provide extra tarball archives.

A recommended way to interact with this module is through few easy steps:
- Step 1. Download the "query.input" file, and place it beside this module.
          If one of the key-value pairs is missing, they will be substituted by their
          default - which is defined on the top of the module.
- Step 2. Open and edit the ranges of parameters therein.
- Step 3. In the command-line, call python and fetch the whole tarballs only once
          >>> import query_grid 
          >>> query_grid.fetch_tar()
- Step 4. You are now ready to extract a subset of the archive into a local directory
          >>> query_grid.main()
- Step 5. Practice some patience. Reading and extracting each tarball may take up to 10 minutes.
          Check your preferred "path_save" directory often to make sure it is being filled up by
          HDF5 files. If so, you're done and ready to GO!  
Do not be surprised if you find a query.log file in your working directory, which is meant for 
issue tracking.
"""

import sys, os, glob
import logging
import re 
import tarfile
import numpy as np

#=================================================================================================================
# Constants

# Paths
path_grid = ''   
path_ftp  = ''
path_bz2  = path_ftp + 'gyre_out.tar.bz2'
url_bz2   = 'ftp://ftp.ster.kuleuven.be/dist/ehsan/ASAMBA/gyre_out.tar.bz2'

default_path_save = './'

# Physical Parameters
# If the values for the following fields are not provided in the query.input file, then these are used as the defaults
default_mass_from = 4.10
default_mass_to   = 4.40
default_mass_step = 0.10

default_eta_from  = 0.00
default_eta_to    = 0.03 
default_eta_step  = 0.003

default_ov_from   = 0.000
default_ov_to     = 0.030
default_ov_step   = 0.003

default_Z_from    = 0.014
default_Z_to      = 0.014
default_Z_step    = 0.002

default_sc        = 0.01 

#=================================================================================================================
def read_query_input(query_input='query.input'):
  """
  Read the input setting file, and use the specified parameters for further processing of the query
  @param query_input: full path to the input file containing the parameter ranges and steps
  @type query_input: string
  @return: dictionary with the key-value pairs taken from each line that contains "=" sign.
  @rtype: dictionary
  """
  if not os.path.isfile(query_input):
    logging.error('read_query_input: {0} does not exist'.format(query_input))
  
  float_names = ['mass_from', 'mass_to', 'mass_step', 'eta_from', 'eta_to', 'eta_step', 
                 'ov_from', 'ov_to', 'ov_step', 'Z_from', 'Z_to', 'Z_step', 'sc']
  
  with open(query_input, 'r') as r: lines = r.readlines()
  dic = {}
  for i_line, line in enumerate(lines):
    if line:
      pieces = line.rstrip('\r\n').split()
      if pieces:                               # skip empty lines
        if len(pieces) < 3: continue           # skip unknown lines
        if pieces[0] == '#': continue          # skip comment lines 
        if pieces[0] in float_names:
          dic[pieces[0]] = float(pieces[2])
        else:
          dic[pieces[0]] = pieces[2]
  
  logging.info('read_query_input: {0} fully read and converted to a dictionary'.format(query_input))
  
  return dic
  
#=================================================================================================================
def gen_param_range(query_input='query.input'):
  """
  Generate range of mass, overshooting, and metalicity range to query from the archive.
  @param query_input: full path to the input file containing the parameter ranges and steps
  @type query_input: string
  @return: dictionary 
  @rtype: dictionary
  """
  dic = read_query_input(query_input)
  
  if not dic.has_key('path_save'): dic['path_save'] = default_path_save
  
  if not dic.has_key('mass_from'): dic['mass_from'] = default_mass_from
  if not dic.has_key('mass_to'):   dic['mass_to']   = default_mass_to 
  if not dic.has_key('mass_step'): dic['mass_step'] = default_mass_step 
  
  if not dic.has_key('eta_from'):  dic['eta_from']  = default_eta_from 
  if not dic.has_key('eta_to'):    dic['eta_to']    = default_eta_to 
  if not dic.has_key('eta_step'):  dic['eta_step']  = default_eta_step
   
  if not dic.has_key('ov_from'):   dic['ov_from']   = default_ov_from 
  if not dic.has_key('ov_to'):     dic['ov_to']     = default_ov_to 
  if not dic.has_key('ov_step'):   dic['ov_step']   = default_ov_step
   
  if not dic.has_key('Z_from'):    dic['Z_from']    = default_Z_from 
  if not dic.has_key('Z_to'):      dic['Z_to']      = default_Z_to 
  if not dic.has_key('Z_step'):    dic['Z_step']    = default_Z_step
   
  if not dic.has_key('sc'):        dic['sc']        = default_sc 
  

  mass_from = dic['mass_from']
  mass_to   = dic['mass_to']
  mass_step = dic['mass_step']

  eta_from  = dic['eta_from']
  eta_to    = dic['eta_to']
  eta_step  = dic['eta_step']
  
  ov_from   = dic['ov_from']
  ov_to     = dic['ov_to']
  ov_step   = dic['ov_step']
  
  Z_from    = dic['Z_from']
  Z_to      = dic['Z_to']
  Z_step    = dic['Z_step']
  
  sc        = dic['sc']

  n_mass    = int(np.round((mass_to-mass_from)/mass_step, decimals=3)+1)
  mass_vals = np.linspace(mass_from, mass_to, num=n_mass, endpoint=True)
  mass_string = ['M{0:05.2f}'.format(val) for val in mass_vals]

  n_eta     = int(np.round((eta_to-eta_from)/eta_step, decimals=3)+1)
  eta_vals  = np.linspace(eta_from, eta_to, num=n_eta, endpoint=True)
  eta_string= ['eta{0:04.2f}'.format(val) for val in eta_vals]

  n_ov      = int(np.round((ov_to-ov_from)/ov_step, decimals=3)+1)
  ov_vals   = np.linspace(ov_from, ov_to, num=n_ov, endpoint=True)
  ov_string = ['ov{0:05.3f}'.format(val) for val in ov_vals]
  
  n_Z       = int(np.round((Z_to-Z_from)/Z_step, decimals=4)+1)
  Z_vals    = np.linspace(Z_from, Z_to, num=n_Z, endpoint=True)
  Z_string  = ['Z{0:5.3f}'.format(val) for val in Z_vals]
  
  n_sc      = 1
  sc_string = 'sc{0:04.2f}'.format(sc)
  
  dic['n_mass']    = n_mass
  dic['mass_vals'] = mass_vals
  dic['mass_string'] = mass_string
  dic['n_eta']     = n_eta
  dic['eta_vals']  = eta_vals
  dic['eta_string'] = eta_string
  dic['n_ov']      = n_ov
  dic['ov_vals']   = ov_vals
  dic['ov_string'] = ov_string
  dic['n_Z']       = n_Z
  dic['Z_vals']    = Z_vals
  dic['Z_string']  = Z_string
  dic['n_sc']      = n_sc
  dic['sc_string'] = sc_string
  
  return dic

#=================================================================================================================
def extract_tarball(dic_param, path_tarball=None, path_save=None):
  """
  Extract a limited range of files from the tarball, based on the limited range for mass, overshooting and metalicity
  @param dic_param: The dictionary contains the range of mass, overshooting and metalicity as float, and also as a list 
     of strings as required to deal with file names
  @type dic_param: dictionary
  @param path_tarball: full path to an existing tarball which is already downloaded from FTP.
     default=None, which takes the file 'gyre_out.tar.bz2' from local directory
  @type path_tarball: string
  @param path_save: full path to the directory that extracted files will sit. If it does not exist, it will be created.
  @type path_save: string
  @return: None
  @rtype: None
  """
  if path_tarball is None: path_tarball = 'gyre_out.tar.bz2'
  if not os.path.isfile(path_tarball):
    logging.error('extract_tarball: {0} does not exist!'.format(path_tarball))
  if path_save[-1] != '/': path_save += '/'
  if not os.path.exists(path_save): 
    logging.warning('extract_tarball: Creating {0}'.format(path_save))
    os.makedirs(path_save)
  
  handle    = tarfile.open(path_tarball, 'r:bz2')
  filenames = handle.getnames() 
  n_filenames = len(filenames)
  logging.info('extract_tarball: {0} has {1} files'.format(path_tarball, n_filenames))
  print filenames[0]
  
  n_mass      = dic_param['n_mass']
  mass_vals   = dic_param['mass_vals']
  mass_string = dic_param['mass_string']
  n_eta       = dic_param['n_eta']
  eta_vals    = dic_param['eta_vals']
  eta_string  = dic_param['eta_string'] 
  n_ov        = dic_param['n_ov']
  ov_vals     = dic_param['ov_vals']
  ov_string   = dic_param['ov_string']
  n_Z         = dic_param['n_Z']
  Z_vals      = dic_param['Z_vals']
  Z_string    = dic_param['Z_string']
  sc_string   = dic_param['sc_string']
  
  if n_Z > 1:
    logging.warning('extract_tarball: Currently, we only support solar metalicity Z=0.014')

  n_match = 0 
  for i_file, a_file in enumerate(filenames):
    pieces     = a_file.rstrip('\r\n').split('-')
    if float(pieces[2][1:]) < np.min(mass_vals): continue
    #if float(pieces[2][1:]) > np.max(mass_vals): 
      #print i_file, float(pieces[2][1:])
      #break
    right_mass = (pieces[2] in mass_string)
    right_eta  = (pieces[3] in eta_string)
    right_ov   = (pieces[4] in ov_string)
    right_sc   = (pieces[5] == sc_string)
    right_Z    = (pieces[6] in Z_string)
    if all([right_mass, right_eta, right_ov, right_sc, right_Z]):
      n_match += 1
      handle.extract(a_file, path=path_save)
     
  print 'Extracted {0} Files out of {1}'.format(n_match, n_filenames)
  logging.info('extract_tarball: Extracted {0} Files out of {1}'.format(n_match, n_filenames))
  logging.info('extract_tarball: Done')
  
  return 0

#=================================================================================================================
def gen_tarball(path_save=None):
  """
  Create a bzip2 bundle from the whole archive
  @param path_save: the full path to the location where the output .tar.bz2 file will rest.
      default=None, which means save in the current location
  @type path_save: string
  @return: None
  @rtype: None
  """
  if path_save is not None:
    if path_save[-1] != '/': path_save += '/'
    if not os.path.exists(path_save): os.makedirs(path_save)
  else:
    path_save = os.getcwd()
  tarball_name = path_save + 'gyre_out.tar.bz2'
  logging.info('gen_tarball: saves to {0}'.format(tarball_name))
  
  list_files = glob.glob(path_grid + 'M*/gyre_out/*.h5')
  n_files    = len(list_files)
  print 'gen_tarball: Found {0} files'.format(n_files)
  logging.info('gen_tarball: Found {0} files'.format(n_files))
  
  ind_slash = list_files[0].rfind('/')
  
  with tarfile.open(tarball_name, 'w:bz2') as handle:
    for a_file in list_files: 
      short_name = a_file[ind_slash+1 : ]
      handle.add(a_file, arcname=short_name)
  
  logging.info('gen_tarball: Done')
    
  return None
  
#=================================================================================================================
def fetch_tar():
  """
  Simply downloads the gyre_out.tar.bz2 from the public ftp 
  """
  import urllib
  def progress_bar(a,b,c):
    """
    Show a progress bar similar to what wget does
    """
    sys.stdout.write( '\r% 3.1f%% of %d bytes' % (min(100, float(a * b) / c * 100), c) )
    sys.stdout.flush()
  logging.info('fetch_tar: Download started.')
  urllib.urlretrieve(url_bz2, 'gyre_out.tar.bz2', progress_bar)
  logging.info('fetch_tar: {0} downloaded.'.format(url_bz2))
  
  return 0
#=================================================================================================================
def main():
  """
  The main caller
  """
  logging.info('main: Start')
  dic_param = gen_param_range()
  
  if True:
    print 'Extract a subset of the archive'
    extract_tarball(dic_param, path_tarball=path_bz2, path_save=dic_param['path_save']) 
  
  
  if False:
    print 'Create a tarball from all GYRE outputs'
    gen_tarball(path_save=path_ftp)

  logging.info('main: End')
  
  return 0
#=================================================================================================================
if __name__ == '__main__':
  # Logging settings
  logging.basicConfig(filename='query.log', format='%(levelname)s: %(asctime)s: %(message)s', level=logging.DEBUG)

  status = main()
  sys.exit(status)
#=================================================================================================================


import glob, os, sys, shutil
import logging
import time
import tarfile
import param_tools as pt
from scipy import interpolate
import numpy as np
import subprocess
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import commons, debug, read, param_tools

#=================================================================================================================
#=================================================================================================================
def create_bz2_from_dir(path, save_dir='/STER/ehsan', prefix='Archive', filter_by='*'):
  """
  
  """
  if not os.path.exists(path):
    message = 'Error: file_dir_stat: create_bz2_from_dir: path=%s does not exist' % (path, )
    raise SystemExit, message
  if path[-1] != '/': path += '/'

  if not os.path.exists(save_dir):
    message = 'Error: file_dir_stat: create_bz2_from_dir: save_dir=%s does not exist' % (save_dir, )
    raise SystemExit, message
  if save_dir[-1] != '/': save_dir += '/'
  
  os.chdir(path)     # change directory to the repository 
  list_files = glob.glob(filter_by)
  n_files = len(list_files)
  if n_files == 0:
    message = 'Error: file_dir_stat: create_bz2_from_dir: %s is empty' % (path, )
    raise SystemExit, message
  print ' - file_dir_stat: create_bz2_from_dir: %s files detected for Archiving' % (n_files, )
  
  list_write = []
  nl = '\n'
  for i_f, f in enumerate(list_files): list_write.append(f + nl)
  
  temp_file = save_dir + 'Tar-FileList.tmp'
  w = open(temp_file, 'w')
  w.writelines(list_write)  
  w.close()
  
  tarball = save_dir + prefix + '.tar.bz2'
  cmnd = 'tar -jc ' + '-T ' + temp_file + ' -f ' + tarball  
  print '   ', cmnd
  tar_output = os.popen(cmnd)
  
    #if os.path.isfile(temp_file): os.unlink(temp_file)
  
  return None

#=================================================================================================================
def update_file_timestamp(path='/scratch/leuven/307/vsc30745/Grid', regex='*'):
  """
  On VSC, files on /scratch disk are erased after 20 days. To update the timestamp to avoid such deletion, this function
  updates the file timestamps in the input given directory
  @param path: full path to the directory where the file timestamps will be updated.
  @type path: string
  @param regex: the regular expression to filter files; default='*', meaning update all files in path
  @type regex: string
  @return: None
  @rtype: None
  """
  if not os.path.exists(path): 
    message = 'Error: file_dir_stat: update_file_timestamp: %s does not exist' % (path, )
    raise SystemExit, message
  
  if path[-1] != '/': path += '/'
  files = glob.glob(path + regex)
  n_files = len(files)
  if n_files == 0: 
    logging.warning('update_file_timestamp: no files found in {0}'.format(path))
    print ' - file_dir_stat: update_file_timestamp: Found No files in {0}'.format(path)
    print '   The regex = {1}'.format(regex)
    return None
  
  for a_file in files: os.utime(a_file, None)
  
  return None

#=================================================================================================================
def identify_corrupted_ascii(list_filenames, decision=None, path_move='/scratch/leuven/307/vsc30745/corrupted/'):
  """
  WARNING: This function needs libmagic, file and python-magic be installed.
  When wriging MESA output files to disk - mainly on the VSCentrum - some of the ascii files are corrupted and cannot 
  be read again. This function goes through all files passed by list_filenames, and identifies the corrupted files. If 
  the number of currupted files is greater than 1, then it asks the user to decide whehter "delete" or "keep" them
  >>> bad_files, good_files = identify_corrupted_ascii(list_of_all_my_files)
  @param list_filenames: full path to all files identified on the disk for which we want to investigate their file type
  @type list_filenames: list of strings
  @param decision: One of the "keep", "move" or "delete" to deal with corrupted files. default=None, so will be prompted
     on the command line
  @type decision: string
  @param path_move: full path to the directory where corrupted files are moved into. default is matched to VSC path
  @type path_move: string
  @return: list of bad files and list of good files, wether or not they are deleted
  @rtype: tuple
  """
  #try:
    #import magic
  #except:
    #message = 'Error: file_dir_stat: identify_corrupted_ascii: Failed to import magic!'
    #raise SystemExit, message
  
  if path_move[-1] != '/': path_move += '/'
  if not os.path.exists(path_move): 
    ind = [i for i in range(len(path_move)) if path_move[i] == '/']
    ind = ind[-2]
    mkdir_at = path_move[:ind]
    write_aceess = os.access(mkdir_at, os.W_OK) # W_OK is for writing, R_OK for reading, etc.
    if write_aceess: 
      os.makedirs(path_move)    
    else:
      message = 'Error: file_dir_stat: identify_corrupted_ascii: No Permission to create: %s: %s' % (path_move, write_aceess)
      raise SystemExit, message
  
  good = 'ASCII text'
  bad  = 'data'
  
  good_files = []
  bad_files = []
  for i, fn in enumerate(list_filenames):
    #file_type = magic.from_file(fn)
    cmd = 'file ' + fn
    file_type = os.popen(cmd).readlines()[0]
    if good in file_type: good_files.append(fn)
    if bad  in file_type: bad_files.append(fn)
  
  n_bad = len(bad_files)
  if n_bad == 0:
    print ' - identify_corrupted_ascii: No currupted files identified'
  if n_bad > 0:
    print ' - identify_corrupted_ascii: Num. corrupt files = %s' % (n_bad, )
    print '   Decide to "delete", "move" or "keep".'

    if decision is not None: flag = decision
    else: flag = raw_input('How to proceed? "keep"/"move"/"delete"? ')

    if flag != 'delete' and flag != 'keep' and flag != 'move':
      raise SystemExit, 'Error: Wrong decision: %s is not delete, move or keep.' % (flag, )

    if flag == 'delete':
      print '   Starting to delete %s files ...' % (n_bad, ) 
      for i, fn in enumerate(bad_files): os.unlink(fn)

    if flag == 'move':
      for i, fn in enumerate(bad_files):
        shutil.move(fn, path_move)
        print '   %s moved to %s' % (fn, path_move)
    if flag == 'keep':
      print '   Returns the list of the full path to the corrupted files.'

  return bad_files, good_files
  
#=================================================================================================================
def check_job_complete_by_hist(list_dic_hist, ascii_out=None, file_out=None):
  """
  This function receives a list of dictionaries containing history information for a grid. Then, it checks the final 
  value for center_h1, center_he4 and Teff to make sure that the tracks did not terminate before the core he depletion.
  If an uncomplete track is detected, then it will store the file name in a list, and returns the list.
  A job is considerd failed/incomplete if any of these conditions come true:
  - center_h1 > 0.0
  - center_he4 > 0.0
  - Teff > 10,000 K
  
  @param list_dic_hist: list of dictionaries containing history information. each dictionary has at least these keys:
      - filename: gives the source ascii filename, full physical model parameters can be extracted from this
      - header:   ascii file header in the form of record array
      - hist:     numpy record array containing the full history information
  @type list_dic_hist: list of dictionaries.  
  @param ascii_out: full path to the ascii file (compatible with the VSC worker frame) containing the list of physical 
      parameters of the failed jobs per each line. (default=None)
  @type ascii_out: string
  @param file_out: default=None, if provided, then a plot with multiple panels will be stored at the full path given by file_out.
      different plot panels show the end values of Teff, Xc and Yc against one another.
  @type file_out: string
  @return: a list which contains the "filename" of those jobs that are identified as failed/incomplete
  @rtype: list of strings
  """
  
  n_hist = len(list_dic_hist)
  if n_hist == 0:
    message = 'Error: file_dir_stat: check_job_complete_by_hist: Empty input list!'
    raise SystemExit, message
  
  try: 
    sample_hist = list_dic_hist[0]['hist']
  except:
    message = 'Error: file_dir_stat: check_job_complete_by_hist: "hist" field is missing from input dictionaries.'
    raise SystemExit, message
  
  sample_hist_names = sample_hist.dtype.names
  dic_avail_keys = {}
  for key in sample_hist_names:
    dic_avail_keys[key] = 0.0
  required_keys = ['log_Teff', 'effective_T', 'center_h1', 'center_he4']
  dic_keys = read.check_key_exists(dic_avail_keys, required_keys)
  flag_Teff = dic_keys['effective_T']
  flag_logTeff = dic_keys['log_Teff']
  if not flag_logTeff and not flag_Teff:
    message = 'Error: file_dir_stat: check_job_complete_by_hist: effective_T and log_Teff are both missing from history information.'
    raise SystemExit, message
  
  print '\n - file_dir_stat: check_job_complete_by_hist: Testing %s MESA tracks ...' % (n_hist, )
  
  list_filenames = []
  list_failed = []
  n_fail = 0
  list_Teff = []
  list_Xc   = []
  list_Yc   = []
  for i_hist, dic in enumerate(list_dic_hist):
    hist = dic['hist']
    list_filenames.append(dic['filename'])
    
    if flag_Teff and not flag_logTeff: list_Teff.append(hist['effective_T'][-1])
    if flag_logTeff and not flag_Teff: list_Teff.append(np.power(10.0, hist['log_Teff'][-1]))
    if flag_Teff and flag_logTeff: list_Teff.append(hist['effective_T'][-1])
    list_Xc.append(hist['center_h1'][-1])
    list_Yc.append(hist['center_he4'][-1])
    
  # convert lists to numpy array
  list_Teff = np.asarray(list_Teff)
  list_Xc   = np.asarray(list_Xc)
  list_Yc   = np.asarray(list_Yc)
  
  bool_bad_Xc   = list_Xc > 1e-12 
  n_bool = len(bool_bad_Xc)
  bool_bad_Teff = list_Teff > 1e4
  bool_bad_Yc = (list_Xc < 1e-12) & (list_Yc > 1e-3)
  #bool_bad_Yc = []
  #for i_bool in range(n_bool):
    #if all([ (list_Xc[i_bool] < 1e-12),  (list_Yc[i_bool] > 1e-3) ]):
      #bool_bad_Yc.append(True)
    #else: bool_bad_Yc.append(False)
  
  bool_final = []
  #for i_bool in range(n_bool):
  for i_bool in range(n_hist):
    #if any([ bool_bad_Xc[i_bool], bool_bad_Yc[i_bool], bool_bad_Teff[i_bool] ]): 
    if (bool_bad_Xc[i_bool]) or ((bool_bad_Teff[i_bool]) & (bool_bad_Yc[i_bool])):
      bool_final.append(True)
      d = pt.get_param_from_single_hist_filename(list_filenames[i_bool])
      line = '%05.2f,%4.2f,%5.3f,%4.2f,%5.3f %s' % (d['m_ini'],d['eta'],d['ov'],d['sc'],d['z'],' \n') 
      list_failed.append(line)
      n_fail += 1
      
  if n_fail>0: 
    print '   We detected %s failed/incomplete jobs.' % (n_fail, )
  else:
    print '   Safe: All jobs are complete.'
  
  if ascii_out and n_fail>0:
    print '   Writing the failed information to %s' % (ascii_out, )
    w = open(ascii_out, 'w')
    line = 'm_ini,eta_ini,ov_ini,sc_ini,z_ini \n'
    w.writelines(line)
    w.writelines(list_failed)
    w.close()
    
  if file_out:   # enter plotting 
    fig = plt.figure()
    plt.subplots_adjust(left=0.09, right=0.99, bottom=0.10, top=0.99, hspace=0.15, wspace=0.15)
    ax0 = fig.add_subplot(221)
    ax1 = fig.add_subplot(222)
    ax2 = fig.add_subplot(223)
    
    ax0.scatter(list_Teff, list_Xc)
    ax0.set_xlim(max(list_Teff), min(list_Teff))
    ax0.set_xlabel(r'T$_{\rm eff}$')
    ax0.set_ylim(-0.01, 0.72)
    ax0.set_ylabel(r'X$_{\rm c}$')
    
    ax1.scatter(list_Teff, list_Yc)
    ax1.set_xlim(max(list_Teff), min(list_Teff))
    ax1.set_xlabel(r'T$_{\rm eff}$')
    ax1.set_ylim(-0.01, 1.0)
    ax1.set_ylabel(r'Y$_{\rm c}$')

    ax2.scatter(list_Xc, list_Yc)
    ax2.set_xlim(0.72, -0.01)
    ax2.set_xlabel(r'X$_{\rm c}$')
    ax2.set_ylim(-0.01, 1.0)
    ax2.set_ylabel(r'Y$_{\rm c}$')
    
    plt.savefig(file_out)
    print ' - %s saved.' % (file_out, ) 
    plt.close()
  
  return list_failed 

#=================================================================================================================
def organize_VSC_tar(path_tar='/STER/mesa-gyre/From-VSC', path_dest='/STER/mesa-gyre/Grid', 
                     filter_prof='*', filter_hist='*'):
  """
  The VSC computations produce a large number of tarballs (for profile and gyre_in files) in addition to the history
  HDF5 files. They are all stored in $VSC_SCRATCH and are retrieved routinely to /STER/mesa-gyre/From-VSC. The files
  are not sorted into relevant directories based on their filenames.
  This function iteratively takes one file, identifies the file, creates an appropriate directory where it must sit,
  and copies/extracts them to that right destination.
  @param path_tar: (default='/STER/mesa-gyre/From-VSC') is the default location of all tarballs.
  @type path_tar: string
  @param path_dest: (default='/STER/mesa-gyre/Grid') is the default location of all tarballs.
  @type path_dest: string
  @param filter_hist: default = '*'; to limit the organization of history files (i.e. rsync between path_tar and path_dest)
      to a specific range in parameters.
  @param filter_prof: default = '*'; to limit the tar uncompression to a range in parameters for limited files/filenames. 
      The same filter applies to the gyre_in files, since they exactly have the same filenames to profiles, except 
      their extension.
      E.g. to organize only 5Msun files at solar metalicity, one can use: filter_prof='M05.00*-Z0.014*MS*'
  @type filter_prof: string
  @return: list of failed/corrupt/empty profile tarballs that could not manage to extract. empty list means success!
  @rtype: list of strings
  """
  if not os.path.exists(path_tar):
    message = 'Error: file_dir_stat: organize_VSC_tar: Main repository: %s does not exist!' % (path_tar,)
    raise SystemExit, message
  if not os.path.exists(path_dest):
    print 'Warning: file_dir_stat: organize_VSC_tar: Destination directory: %s is just created!' % (path_dest, )
  
  # drop trailing '/' from path_dest and path_tar and put them explicitly later
  if path_dest[-1] == '/': path_dest = path_dest[:-1]
  if path_tar[-1] == '/': path_tar = path_tar[:-1]

  list_hist_files = glob.glob(path_tar + '/' + filter_hist + '.h5')
  list_hist_files.sort()
  n_hist = len(list_hist_files)
  list_prof_files = glob.glob(path_tar + '/' + filter_prof + '.prof.tar.bz2')
  list_prof_files.sort()
  n_prof = len(list_prof_files)
  list_gyre_in_files = glob.glob(path_tar + '/' + filter_prof + '.gyre_in.tar.bz2')
  list_gyre_in_files.sort()
  n_gyre_in = len(list_gyre_in_files)
  
  print '\n - Status Report:'
  print '- Total number of hist files      : %s' % (n_hist)
  print '- Total number of profile tarballs: %s' % (n_prof)
  print '- Total number of gyre_in tarballs: %s' % (n_gyre_in)
  
  # Start by hist files. Identify the mass, create relevant subdirectory, and move them there
  if n_hist > 0:
    for i_hist, hist_file in enumerate(list_hist_files):
      ind_slash = hist_file.rfind('/')
      hist_core = hist_file[ind_slash+1:]
      str_mass = hist_core[0:6]
      hist_dir = path_dest + '/' + str_mass + '/hist'
      if not os.path.exists(hist_dir): os.makedirs(hist_dir)
    
    #copy(hist_file, hist_dir)
    cmnd = 'rsync -av ' + hist_file + ' ' + hist_dir
    rsync_output = os.popen(cmnd)

  if n_gyre_in > 0:
    print ' - Extract the gyre_in tar files'
    extract_bz2(path_tar=path_tar, path_dest=path_dest, is_gyre_in=True, list_files=list_gyre_in_files)  
  if n_prof > 0:
    print ' - Extract the prof tar files'
    extract_bz2(path_tar=path_tar, path_dest=path_dest, is_prof=True, list_files=list_prof_files)
    
  list_failed = []
  return list_failed
    
#=================================================================================================================
def rsync_gyre_out(path_target='/scratch/leuven/307/vsc30745/Grid/', path_dest='/STER/mesa-gyre/Grid/'):
  """
  The outcome of GYRE are files that are sorted out in different subdirectories of the grid under "gyre_out" name.
  This function recursively does fetch the gyre_out HDF5 files, and places them into the right directory in the 
  desitination folder.
  WARNING: This would only be run from pleiads to rsync with VSC.
  @param path_target: the path where the files normally sit. default is the location of the grid on the VSC
  @type path_target: string
  @param path_dest: the path where the files will be copied to using rsync. default is the location of the grid at IvS.
  @return: None
  @rtype: None
  """
  if path_target[-1] != '/': path_target += '/'
  if path_dest[-1]   != '/': path_dest   += '/'
  if not os.path.exists(path_dest): 
    logging.error('rsync_gyre_out: {0} does not exist'.format(path_target))
    raise SystemExit
  
  # first ensure the ssh is allowed to tunnel to VSC
  step1 = os.popen('start-ssh-agent')
  step2 = os.popen('ssh-add')
  

  return None

#=================================================================================================================
def extract_bz2(path_tar='/STER/mesa-gyre/From-VSC', list_files=[], path_dest='/STER/mesa-gyre/Grid',
                is_prof=False, is_gyre_in=False):
  """
  Extract each tarball from the source at path_tar to the destination path_dest, making sure that we are not duplicating
  """
  n_files = len(list_files)
  if n_files == 0:
    logging.error('file_dir_stat: extract_bz2: Empty input list.')
    raise SystemExit
  
  if not is_prof and not is_gyre_in:
    logging.error('file_dir_stat: extract_bz2: Set either of is_prof:%s OR is_gyre_in:%s to True.' % (is_prof, is_gyre_in))
    raise SystemExit
  
  if all([is_gyre_in, is_prof]):
    logging.error('file_dir_stat: extract_bz2: Set either of is_prof:%s OR is_gyre_in:%s to True.' % (is_prof, is_gyre_in))
    raise SystemExit
  
  if is_prof:    
    final_dir = '/profiles'
    end_str   = '.prof.tar.bz2'
  if is_gyre_in: 
    final_dir = '/gyre_in'
    end_str   = '.gyre_in.tar.bz2'
  
  if path_dest[-1] != '/': path_dest += '/'
  
  for i_file, tar_file in enumerate(list_files):
    ind_slash = tar_file.rfind('/')
    file_core = tar_file[ind_slash+1:]
    str_mass  = file_core.split('-')[0]  #file_core[0:6]
    target_dir = path_dest + str_mass + final_dir
    if not os.path.exists(target_dir): os.makedirs(target_dir)
    
    TarBall = tarfile.open(tar_file, 'r:bz2')
    try:
      TarBall_name = TarBall.getnames()[0]
    except IOError:
      print '   IOError: {0}'.format(tar_file)
      TarBall_name = handle_partially_corrupt_tar_bz2(tar_file, path_tar, path_dest)
      debug.warning('file_dir_stat: extract_bz2: IOError: getnames failed: {0}'.format(tar_file), email=True)
    except EOFError:
      print '   EOFError: {0}'.format(tar_file)
      TarBall_name = handle_partially_corrupt_tar_bz2(tar_file, path_tar, path_dest)
      debug.warning('file_dir_stat: extract_bz2: EOFError: getnames failed: {0}'.format(tar_file), email=True)
      
    # excluding leading dir names and trailing extension
    TarBall_core = TarBall_name[TarBall_name.rfind('/')+1 : TarBall_name.rfind('.tar.bz2')] 
    # excluding anything after e.g. Z0.014 in the filename
    TarBall_core = TarBall_core[:TarBall_core.rfind('-Z0.0')+7] 
    
    # skip Untar if already done
    avail_files = os.listdir(target_dir)
    n_avail_files = len(avail_files)
    if n_avail_files >0:
      list_avail_file_cores = set()  # set is way faster than list, since it is unordered
      for i_avail, avail_file in enumerate(avail_files):
        # excluding leading dir names and trailing extension
        avail_file_core = avail_file[avail_file.rfind('/')+1 : avail_file.rfind(end_str)] 
        list_avail_file_cores.add(avail_file_core)
      if TarBall_core in list_avail_file_cores: 
        print '   {0} already extracted'.format(tar_file)
        continue  # i.e. forget Untaring this file and proceed to the next

    # Fix full path to files in the tarball by identifying and avoiding the leading directory levels    
    if '/' in TarBall_name:
      strip_components = TarBall_name.count('/') - 1
      cmnd = 'tar jxPf ' + tar_file + ' --strip-components=' + str(strip_components) + ' -m -C ' + target_dir
    else:
      cmnd = 'tar jxPf ' + tar_file + ' -m -C ' + target_dir
    
    print '   %s' % (cmnd, )
    untar_output = os.popen(cmnd)
    logging.info('file_dir_stat: extract_bz2: {0}'.format(tar_file))
    
  return None

#=================================================================================================================
def handle_partially_corrupt_tar_bz2(tar_file, path_tar, path_dest):
  """
  Sometimes a tar.bz2 file is "partially" corrupt due, maybe, to archiving process or the rsync file transfer.
  Some files can still be retrieved from these bundles. We copy the identified
  
  """
  temp_dir = '/tmp/working/'
  if not os.path.exists(temp_dir): os.makedirs(temp_dir)
  os.chdir(temp_dir)
  shutil.copy2(tar_file, temp_dir)
  cmnd = 'tar jxPf {0}'.format(tar_file)
  output = subprocess.Popen(cmnd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
  print ' - file_dir_stat: handle_partially_corrupt_tar_bz2: sleep for 5 sec'
  time.sleep(5)
  h5_files = glob.glob('*.h5')
  n_h5 = len(h5_files)
  if n_h5 == 0: 
    return []

  # Re-Create the bundle from only the safe files
  for f in h5_files: shutil.copy2(f, path_dest)
  new_tar_file = glob.glob('*.tar.bz2')[0]
  if os.path.isfile(new_tar_file): os.remove(new_tar_file)
  cmnd = 'tar -jcPf {0} *.h5'.format(new_tar_file)
  #os.popen(cmnd)
  output = subprocess.Popen(cmnd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
  print ' - file_dir_stat: handle_partially_corrupt_tar_bz2: sleep for 5 sec'
  time.sleep(5)
  
  # Get the filenames
  TarBall = tarfile.open(new_tar_file, 'r:bz2', ignore_zeros=True)
  TarBall_name = TarBall.getnames()[0]
  
  # Change file write permission before and after the copying
  #os.chmod(path_tar, 755)
  os.popen('chmod 755 {0}'.format(path_tar))
  os.remove(tar_file)
  shutil.move(new_tar_file, path_tar)
  #os.chmod(path_tar, 555)
  os.popen('chmod 555 {0}'.format(path_tar))
  
  all_files = glob.glob('*')
  for f in all_files: os.remove(f)
  logging.info('file_dir_stat: handle_partially_corrupt_tar_bz2: {0}'.format(tar_file)) 
  
  return TarBall_name

#=================================================================================================================
def identify_corrupted_tar_bz2(path_repo, srch_str='*', ascii_out=None):
  """
  Skim the tar.bz2 repository for those files that are corrupt - during compression or transfer - and write their list
  as an ascii file
  @param path_repo: full path to where the archive sits
  @type path_repo: string
  @param srch_str: The string to search for .tar.bz2 files using glob; default='*'
  @type srch_str: string
  @param ascii_out: full path to the output ascii file, giving the list of corrupted files
  @type ascii_out: string
  @return: list of currupted files
  @rtype: list of strings
  """
  if not os.path.exists(path_repo):
    logging.error('file_dir_stat: identify_corrupted_tar_bz2: {0} does not exist'.format(path_repo))
    raise SystemExit
  if path_repo[-1] != '/': path_repo += '/'
  
  tar_files    = sorted( glob.glob(path_repo + srch_str + '.prof.tar.bz2') )
  n_tar        = len(tar_files)
  if n_tar     == 0: 
    logging.error('file_dir_stat: identify_corrupted_tar_bz2: No files found!')
    print '   search string: {0}'.format(srch_str)
    raise SystemExit
  print ' - file_dir_stat: identify_corrupted_tar_bz2: Found {0} .tar.bz2'.format(n_tar)
  list_corrupt = []
  for i_file, a_file in enumerate(tar_files):
    print '   bunzip2 -t {0}'.format(a_file)
    process   = subprocess.Popen('bunzip2 -t {0}'.format(a_file), stdout=subprocess.PIPE, 
                             stderr=subprocess.PIPE, shell=True)
    out, err  = process.communicate()
    n_out     = len(out)
    n_err     = len(err)
    if n_err  == 0: continue
  
    # otherwise, the output has message lines, so the file is corrupt!
    # but, let's make an extra check as well:
    if 'file ends unexpectedly' in err: list_corrupt.append(a_file)
      
  n_corrupt = len(list_corrupt)
  if n_corrupt > 0: 
    print ' - file_dir_stat: identify_corrupted_tar_bz2: Found {0} corrupted in {1} input .tar.bz2'.format(n_corrupt, n_tar)
    
  if ascii_out and n_corrupt > 0:
    list_corrupt = [a_file+'\n' for a_file in list_corrupt]
    with open(ascii_out, 'w') as w: w.writelines(list_corrupt)
    print ' - file_dir_stat: identify_corrupted_tar_bz2: {0} stored'.format(ascii_out)
  
  return list_corrupt
  
#=================================================================================================================
def find_prof_file_by_header_value(param_name, param_value, list_dic_prof, report=None):
  """
  This function searches in the header of every dictionary in list_dic_prof to find the value (i.e. param_value) of
  the parameter that matches the name param_name.
  It prints the best matching value, and the corresponding filename
  @param param_name: the key to the parameter we are going to match a good value for. It must be present in the MESA 
      profile header list. For instance, param_name could be "Teff", or "h1_mass", etc.
  @type param_name: string
  @param param_value: the value that we try to find the closest match.
  @type param_value: integer of float
  @param list_dic_prof: list of dictionaries, which each dictionary hast at least the following keys: 1. filename, 
      2. header, and 3. prof.
  @type list_dic_prof: list of dictionaries
  @return: If successful to locate it, the dictionary associated with the best matching value will be rturned
  @rtype: dictionary
  """
  n_dic = len(list_dic_prof)
  if n_dic == 0:
    message = 'Error: file_dir_stat: find_prof_file_by_header_value: Input list is empty!'
    raise SystemExit, message

  list_values = []  
  for i_dic, dic in enumerate(list_dic_prof):
    names = dic['header'].dtype.names
    if param_name not in names:
      print '   Warning: file_dir_stat: find_prof_file_by_header_value: %s not in header list' % (param_name, )
      continue
    
    found_value = dic['header'][param_name][0]
    list_values.append(found_value)
    
  list_values = np.asarray(list_values)
  ind_min = np.argmin(np.abs(list_values - param_value))
  dic_match = list_dic_prof[ind_min]
  val_match = dic_match['header'][param_name][0]
  
  if report:
    print '\n - file_dir_stat: find_prof_file_by_header_value:'
    print '   Closest match to %s = %s is %s.' % (param_name, param_value, val_match)
    print '   Refer to file: %s \n' % (dic_match['filename'], )
  
  return dic_match
  
#=================================================================================================================
def get_files(path, ext):
  """
  This function simply returns the list of specific files in a directory
  specified by the input path
  @param path: string, specifying the location of the history files, e.g. /Users/home/files/M05.6/hist/
  Make sure that the path ends with a slash "/".
  @type path: string
  @param ext: the file extension to look for, e.g. "hist" without * or .
  @type: string
  @return: list of the full path to the whole files found
  @rtype: list of strings
  """
  flag = os.path.exists(path)
  if not flag:
    message = 'Error: file_dir_stat: get_files: %s does not exist!' % (path, )
    raise SystemExit, message

  dic_params = commons.set_srch_param() 
  evol = dic_params['evol']
  
  if evol == '*': 
    srch_str = 'M*.' + ext
  else:
    srch_str = 'M*-' + evol + '-*.' + ext
  if ext == 'hist': srch_str = 'M*.' + ext
  
  files = glob.glob(path + srch_str)
  len_files = len(files)
  
  if len_files == 0:
    print 'Warning: file_dir_stat: get_files: No *.%s files found in %s ' % (ext, path)
    print 'Warning: file_dir_stat: get_files: srch_str = %s' % (srch_str, )
  
  return files


#=================================================================================================================
def get_hist_files():
  
  dic_hist = pt.gen_hist_srch_str()
  hist_full_srch_str = dic_hist['hist_full_srch_str']
  print hist_full_srch_str
  
  hist_files = glob.glob(hist_full_srch_str)
  n_hist = len(hist_files)
  
  dic_files = {'n_hist':n_hist, 'hist_files':hist_files}
  dic_hist.update(dic_files)
  
  return dic_files
 
#=================================================================================================================
def get_prof_files():
  
  dic_prof = pt.gen_prof_srch_str()
  prof_full_srch_str = dic_prof['prof_full_srch_str']
  print 'get_prof_files(): ', prof_full_srch_str
  
  prof_files = glob.glob(prof_full_srch_str)
  n_prof = len(prof_files)
  
  dic_files = {'n_prof':n_prof, 'prof_files':prof_files}
  dic_prof.update(dic_files)
  
  return dic_files

#=================================================================================================================
def get_gyre_files():
  
  dic_gyre = pt.gen_gyre_srch_str()
  gyre_full_srch_str = dic_gyre['gyre_full_srch_str']
  print 'get_gyre_file: GYRE Search String: ' 
  print '  ', gyre_full_srch_str
  
  gyre_files = glob.glob(gyre_full_srch_str)
  n_gyre = len(gyre_files)
  
  dic_files = {'n_gyre':n_gyre, 'gyre_files':gyre_files}
  dic_gyre.update(dic_files)
  
  return dic_gyre 
  
#=================================================================================================================
def remove_dir(dir_in):
  """
  This function empties the entire content of the input directory.
  WARNING: IT IS THE USER RESPONSIBILITY FOR THE LOSS OF VALUABLE DATA!
  @param dir_in: full path to the directory and the entire contents in its sub-directories to be removed
  @type dir_in: string
  @return: None
  @rtype: None
  """
  if not os.path.exists(dir_in):
    message = 'Error: file_dir_stat: remove_dir: %s does not exist' % (dir_in, )
    raise SystemExit, message

  shutil.rmtree(dir_in)
  return None
  
  os.chdir(dir_in)
  files = glob.glob('*')
  print ' - file_dir_stat: remove_dir: Ready to remove %s files in %s ' % (len(files), dir_in)
  
  password = 'U+ek5'
  user_pass = raw_input("  Type %s for file delete authentication: " % (password, ))
  if user_pass != password:
    print '   Wrong password: %s is not %s' % (user_pass, password)
    return None
  
  dir_to_remove = []
  for a_file in files: 
    try: os.unlink(a_file)
    except OSError: 
      print '  Failed to remove: %s' % (a_file, )
      dir_to_remove.append(a_file)
  dir_to_remove.append(dir_in)
  for dr in dir_to_remove: os.removedirs(dr)
  
  return None
  
#=================================================================================================================
def sort_list_files_by_model_number(list_in, extension='.prof'):
  """
  When using glob.glob to find *.prof files in a directory, they are not returned sorted.
  However, we like to sort the filenames before reading them by read.read_multiple_mesa_files, so that
  further plottings would make use of the ordered and sorted profile data.
  The model number is the last piece of information in the profile filename, e.g. 12345 in:
       "M12.3-eta0.12-ov0.021-sc0.01-Z0.016-PMS-Yc0.9987-Xc0.000-12345.prof"
  @param list_in: list of filenames giving the full path to every profile file
  @type list_in: list of strings
  @param extension: the filename filter to be sorted by model number. The possible options are:
       - .prof for the MESA ascii profile files
       - .h5 for the GYRE HDF5 output files
  @type extension: string
  @return: list of filenames, just sorted in increasing model number order
  @rtype: list of strings
  """
  n_files = len(list_in)
  if n_files == 0:
    message = 'Error: file_dir_stat: sort_list_files_by_model_number: Empty input list.'
    raise SystemExit, message
  
  len_extension = len(extension)
  list_model_numbers = []
  for i_file, filename in enumerate(list_in):
    ind_end = filename.rfind(extension)
    model_number = filename[ind_end-5 : ind_end]
    list_model_numbers.append(model_number)
  arr_model_number = np.asarray(list_model_numbers)
  arr_sort_index   = np.argsort(arr_model_number)

  list_out = []
  for ind in arr_sort_index: list_out.append(list_in[ind])  
  
  return list_out
  
  
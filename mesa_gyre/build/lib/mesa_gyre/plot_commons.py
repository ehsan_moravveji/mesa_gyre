
import sys
import itertools
import logging
import numpy as np
import pylab as plt


#=================================================================================================================
#=================================================================================================================
#=================================================================================================================
def get_contour_vertices(contour):
  """
  It is possible to extract the contour vertices of a matplotlib contour object.
  These can be used then to be written to file, and reused later on several times 
  for plotting purposes too. This routine extracts the vertices, and writes them to an 
  ASCII file.
  the contour value for the lowest level is extracted by using contour.collections[0],
  where n-1 is the number of items in contour.collections list.
  @param contour: a data object returned by a call to plt.contour()
  @type contour: no clue for now
  @return: the vertices of the contour as an (x,y)  pair
  @rtype: tuple
  """

  n = len(contour.collections)

  x_data = tuple()
  y_data = tuple()
  for i in range(n):
    try:
      p = contour.collections[i].get_paths()[0]
      v = p.vertices
      x = np.array( v[:,0] )
      y = np.array( v[:,1] )
      x_data = x_data + (x, )
      y_data = y_data + (y, )
    except IndexError:
      pass

  x_data = np.hstack(x_data)
  y_data = np.hstack(y_data)

  return (x_data, y_data)
  
#=================================================================================================================
def get_major_colors_iterator():
  """
  This function returns the major colors, like black, blue, red etc. as an iterator, using the itertools.cycle(), so that
  one can cycle through them, for cases that number of required colors exceeds the number of defined colors here.
  The returned colors are essentially the 8 build-in matplotlib colors (excluding white, and including grey instead).
  """

  return itertools.cycle( [ 'black', 'blue', 'red', 'grey', 'green', 'magenta', 'cyan', 'yellow' ] )

#=================================================================================================================
def save_panel_as_one_figure(fig, which_panel=None, file_out=None):
  """
  This function extracts one of the panels of the "fig" object specified by its index (which_panel), and stores
  that specific panel with all its properties as a single panel figure with a name "file_out".
  @param fig: matplotlib figure object, created by e.g. pylab.figure()
  @type fig: object
  @param which_panel: the index number of the specific panel to extract.
          Note: the index should be <= number of available panels, which start from 0
  @type which_panel: integer
  @param file_out: full path to the output file
  @type file_out: string
  @return: None
  @rtype: None
  """
  list_axes = fig.get_axes()
  n_axes    = len(list_axes)
  if which_panel is None:
    logging.error('plot_commons: save_panel_as_one_figure: No panle specified to extract! Why bother?')
    raise SystemExit
  if which_panel > n_axes:
    logging.error('plot_commons: save_panel_as_one_figure: which_panel > n_axes: {0} vs {1}'.format(which_panel, n_axes))
    raise SystemExit
  if file_out is None:
    logging.error('plot_commons: save_panel_as_one_figure: No output plot file specified! Why bother?')
    raise SystemExit

  the_axis = list_axes[which_panel]
  new_fig  = plt.figure()
  new_axis = new_fig.add_subplot(111)
  new_axis = the_axis

  # x = plt.getp(the_axis)
  # x = plt.getp(fig)
  # u = the_axis.get_lines().get_xydata()
  # print type(u)
  new_fig.axes.append(the_axis)

  plt.savefig(file_out)
  print ' - plot_commons: save_panel_as_one_figure: Panel {0} saved as {1}'.format(which_panel, file_out)
  plt.close()

  return None

#=================================================================================================================
def add_roman_counter_to_fig_axes(fig, x=0.90, y=0.90, fontsize='small'):
  """
  Append tags like (a), (b), etc. to every subplot in the fig object, at the given 'axes fraction' coordinates: x and y
  @param fig: matplotlib figure object created by e.g pylab.figure()
  @type fig: object
  @param x, y: coordinates of the text in every panel; we use 'axes fraction' for positioning
  @type x, y: float
  @param fontsize: the standard matplotlib fontsize, e.g. 'x-small', 'medium', etc.
  @type fontsize: string
  @return: None
  @rtype: None
  """
  import string
  list_axes = fig.get_axes()
  n_axes    = len(list_axes)
  letters   = string.ascii_lowercase
  n_letters = len(letters)

  if n_axes > n_letters:
    logging.error('add_roman_counter_to_fig_axes: number of axes > number of letters')
    raise SystemExit

  for i_ax, ax in enumerate(list_axes[:-1]):
    txt = '({0})'.format(letters[i_ax])
    ax.annotate(txt, xy=(x, y), xycoords='axes fraction', fontsize=fontsize)

  return None

#=================================================================================================================
def add_annotation(axis, list_dic_annot):
  """
  Add annotations to the axis
  @param axis: matplotlib axis, mainly created by matplotlib.figure.add_subplot()
  @type axis: matplotlib axis
  @param list_dic_annot: list of dictionaries with annotation key-value pairs. For possible keys see documentation
  @type list_dic_annot: list of dictionaries
  @return: axis updated with the annotation text
  @rtype: matplotlib axis
  """
  n_annot = len(list_dic_annot)
  if n_annot==0:
    raise SystemExit, 'Error: plot_commons: add_annotation: Input list is empty'

  for i_annot, dic_annot in enumerate(list_dic_annot):
      txt      = r'Annotation'
      xy       = (0.5, 0.90)
      rotation = 0
      xycoords = 'axes fraction'
      fontsize = 'small'
      alpha    = 1
      color    = 'black'
      family   = 'fantasy'
      style    = 'normal'

      if dic_annot.has_key('txt'):      txt      = dic_annot['txt']
      if dic_annot.has_key('xy'):       xy       = dic_annot['xy']
      if dic_annot.has_key('rotation'): rotation = dic_annot['rotation']
      if dic_annot.has_key('xycoords'): xycoords = dic_annot['xycoords']
      if dic_annot.has_key('fontsize'): fontsize = dic_annot['fontsize']
      if dic_annot.has_key('alpha'):    alpha    = dic_annot['alpha']
      if dic_annot.has_key('color'):    color    = dic_annot['color']
      if dic_annot.has_key('family'):   family   = dic_annot['family']
      if dic_annot.has_key('style'):    style    = dic_annot['style']

      axis.annotate(s=txt, xy=xy, xycoords=xycoords, rotation=rotation, fontsize=fontsize,
                  color=color,
                  family=family,
                  style=style,
                  alpha=alpha)
  return axis

#=================================================================================================================
def set_canvas_to_black(fig):
  """
  Set the figure canvas to black, and the textuals to white.
  Remember to update plt.savefig() with the following additional keywords:
  plt.savefig('figure.png', facecolor=fig.get_facecolor(), transparent=True)
  @param fig: matplotlib figure object
  @type fig: matplotlib figure object
  @return: axis updated with new colors
  @rtype: matplotlib axis
  """
  fig.patch.set_facecolor('black')
  # ax = fig.gca()
  # ax.spines['bottom'].set_color('white')
  # ax.spines['top'].set_color('white')
  # ax.spines['left'].set_color('white')
  # ax.spines['right'].set_color('white')
  # ax.xaxis.label.set_color('white')
  # ax.yaxis.label.set_color('white')
  # ax.tick_params(axis='x', colors='white')
  # ax.tick_params(axis='y', colors='white')

  all_axes = fig.get_axes()
  for i, ax in enumerate(all_axes):
    ax.spines['bottom'].set_color('white')
    ax.spines['top'].set_color('white')
    ax.spines['left'].set_color('white')
    ax.spines['right'].set_color('white')
    ax.xaxis.label.set_color('white')
    ax.yaxis.label.set_color('white')
    ax.tick_params(axis='x', colors='white')
    ax.tick_params(axis='y', colors='white')

  return fig

#=================================================================================================================
def smooth_1d(arr, m=5):
  """
  Smooth an array with a sliding box of m pixels. The starting and ending m pixels are set to their default values.
  @param arr: a numpy 1D array
  @type arr: np.array
  @param m: an odd integer giving the size of the sliding box
  @type m: integer
  @return: smoothed 1D array
  @rtype: np.array
  """
  n = len(arr)
  if n < m:
    logging.error('smooth_1d: n < m')
    raise SystemExit
  if n == m:
    return arr

  out = np.zeros(n)
  for i in range(n):
    if i < m: out[i] = arr[i]
    if i > n - m: out[i] = arr[i]
    if m <= i <= n - m:
      out[i] = np.mean( arr[i-m : i+m] )

  return out

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================


#! /usr/bin/python

import numpy as np
import glob
#import matplotlib as mpl
#mpl.use('Agg')
#import matplotlib.pyplot as plt

import commons, read
#import param_tools as pt
import file_dir_stat 
import stars, plot_hrd
#import plot_gyre as pg
#import gyre

#=================================================================================================================
def skim_grid():
  """
  Create a HR diagram of the whole grid
  """
  #dic_platform = commons.set_platform(commons.Pleiad)
  platform = commons.Pleiad
  path_base  = commons.set_paths(platform)['path_base']
  path_plots = commons.set_paths(platform)['path_plots']
  list_dic_all_stars = stars.all_stars()

  list_h5_hist_files = glob.glob(path_base + '*/hist/M03.10*.h5')
  n_hist = len(list_h5_hist_files)

  print '\n - skim_grid.py:'
  print ' - Grid path_base = %s' % (path_base, )
  print ' - Total number of hist *.h5 files = %s' % (n_hist, )

  list_dic_hist = read.read_multiple_h5(list_h5_hist_files[0:])
  
  # Check the end point of all hist files
  list_failed = file_dir_stat.check_job_complete_by_hist(list_dic_hist)
  
  # Plot the Kiel Diagram
  kiel_file = path_plots + 'Kiel-Diagram-M03.10.pdf'
  plot_hrd.plot_kiel_diagram(list_dic_hist, list_dic_stars=list_dic_all_stars, file_out=kiel_file, 
                             logTeff_from=4.15, logTeff_to=3.55, logg_from=4.4, logg_to=1.0)

  print '[Done]'


#=================================================================================================================
if __name__ == '__main__':
  skim_grid()
#=================================================================================================================


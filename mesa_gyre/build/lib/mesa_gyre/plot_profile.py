
"""
This module provides some post-processing plotting tools based on the input profile data from MESA.
The idea is that the data be read by the mesa_gyre.read.read_multiple_mesa_files, and a list of
dictionaries containing the whole data be passed to different functions in this module.
Any repeated reading must be avoided here.
"""

import numpy as np
import commons, read, param_tools, plot_commons
import sys, os
import matplotlib
from matplotlib.transforms import Bbox
import matplotlib.pyplot as plt
from scipy import interpolate
import itertools

Msun = 1.9892e33    # gr; as defined in mesa/const/const_def.f
Rsun = 6.9598e10    # cm; as defined in mesa/const/const_def.f
c_grav = 6.67428e-8 # cgs; as defined in mesa/const/const_def.f

#=================================================================================================================
def compare_column(list_dic_prof, xaxis=None, yaxis=None, list_lbls=None, xlog=False, xpower=False,
                   ylog=False, ypower=False, xaxis_from=None, xaxis_to=None, yaxis_from=None, yaxis_to=None,
                   multiply_yaxis_by=1, xtitle=None, ytitle=None, list_dic_annot=None, include_conv_zones=False, 
                   leg_loc=None, file_out=None):
  """
  Compare the profile of a quantity (from MESA profile columns) on the yaxis w.r.t. xaxis
  @param list_dic_prof: list of dictionaries containing profile information
  @type list_dic_prof
  @param xaxis/yaxis: the name of the column that represents the x/y-axis
  @type xaxis/yaxis: string
  @param list_lbls: list of labels per each dictionary in the input list_dic_prof. Mandatory!
  @type list_lbls: list of strings
  @param xlog/ylog: take a logarithm of the x- and/or y-values and them put them on axis
  @type xlog/ylog: boolean
  @param xpower/ypower: take the x- and/or y-values to the power 10, and them put them on axis
  @type xpower/ypower: boolean
  @param xaxis_from/xaxis_to/yaxis_from/yaxis_to: range in x- and y-axis
  @type xaxis_from/xaxis_to/yaxis_from/yaxis_to: float
  @param multiply_yaxis_by: multiply all y-axis columns by a fixed multiplier to get rid of small or large valued columns.
        Note that this happens before ylog and/or ypower!
  @type multiply_yaxis_by: float
  @param xtitle/ytitle: The title to put on the axis
  @type xtitle/ytitle: string
  @param include_conv_zones: flag to put the convectively unstable zones in the background with a blue shade
  @type include_conv_zones: boolean
  @param file_out: the full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  n_prof = len(list_dic_prof)
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax  = fig.add_subplot(111)
  plt.subplots_adjust(left=0.11, right=0.98, bottom=0.12, top=0.97)

  #--------------------------------
  # Prepare the plot and panels
  #--------------------------------
  import itertools
  ls = itertools.cycle(['solid', 'dotted', 'dashed', 'dashdot'])
  cmap = plt.get_cmap('brg')
  norm = matplotlib.colors.Normalize(vmin=0, vmax=n_prof)
  # color = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
  color = itertools.cycle(['black', 'blue', 'red', 'grey', 'green', 'purple', 'cyan', 'pink'])

  #--------------------------------
  # Loop over profiles, and Plot them!
  #--------------------------------
  list_min_x = []; list_max_x = []
  list_min_y = []; list_max_y = []
  for i_dic, dic in enumerate(list_dic_prof):
    header = dic['header']
    prof   = dic['prof']

    xvals  = prof[xaxis]
    yvals  = prof[yaxis]

    if multiply_yaxis_by != 1.0: yvals = yvals * multiply_yaxis_by
    if xlog: xvals = np.log10(xvals)
    if ylog: yvals = np.log10(yvals)
    if xpower: xvals = np.power(10.0, xvals)
    if ypower: yvals = np.power(10.0, yvals)
    list_min_x.append(np.min(xvals))
    list_max_x.append(np.max(xvals))
    list_min_y.append(np.min(yvals))
    list_max_y.append(np.max(yvals))

    # clr = color.to_rgba(i_dic)
    clr = color.next()
    if list_lbls:
      lbl = list_lbls[i_dic]
    else:
      lbl = ''
    ax.plot(xvals, yvals, linestyle=ls.next(), color=clr, lw=1.5, label=lbl)

  #--------------------------------
  # Annotations and Legend
  #--------------------------------
  if xaxis_from is None: xaxis_from = min(list_min_x)
  if xaxis_to is None:   xaxis_to   = max(list_max_x)
  if yaxis_from is None: yaxis_from = min(list_min_y)
  if yaxis_to is None:   yaxis_to   = max(list_max_y)
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)
  if xtitle: ax.set_xlabel(xtitle)
  if ytitle: ax.set_ylabel(ytitle)

  if list_dic_annot is not None: add_annotation(ax, list_dic_annot)
  if list_lbls and leg_loc:
    # leg = ax.legend(loc=leg_loc, fontsize='large')
    # leg.get_frame().set_alpha(0.5)
    leg = ax.legend(loc=leg_loc, fontsize=12, labelspacing=0.25, fancybox=False, shadow=False, 
                    frameon=False, framealpha=0, scatterpoints=1, scatteryoffsets=[0.5],
                    mode=None,    bbox_to_anchor=(0.07, 0.93))

  if include_conv_zones:
    ax.fill_between(xvals, y1=yaxis_from, y2=yaxis_to, where=(prof['mixing_type']==1), 
       color='blue', alpha=0.2)
    ax.annotate(r'Convective', (0.32, -5), color='black', ha='center', fontsize=12)
    ax.annotate(r'Zone', (0.32, -5.2), color='black', ha='center', fontsize=12)

  #--------------------------------
  # Finalize the Plot
  #--------------------------------
  if file_out:
    fig.patch.set_facecolor('none')
    fig.patch.set_alpha(0.0)
    ax.patch.set_facecolor('none')
    ax.patch.set_alpha(0.0)
    plt.savefig(file_out, dpi=300, transparent=True, facecolor=fig.get_facecolor(), edgecolor='none')
    print ' - plot_profile: compare_column: stored {0}'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def diagnose_convective_boundaries(dic_hist, dic_prof, xaxis_from=None, xaxis_to=None, file_out=None):
  """
  on multiple panels show how different quantities change at the edge of the convective core
  Interesting items would be: grada, gradr, gradL, gradT, v_conv, Gamma_conv, L_conv
  @param dic_prof: dictionary with the profile information
  @type dic_prof: dictionary
  @param file_out: full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  Hz_to_cd = commons.conversions()['Hz_to_cd']

  header = dic_prof['header']
  mod_num_prof = int(header['model_number'][0])
  prof   = dic_prof['prof']
  names  = prof.dtype.names
  fixed_keys = ['mass', 'grada', 'gradr', 'gradL', 'gradT', 'log_conv_vel', 
                'lum_conv_div_L', 'log_mlt_Gamma', 'gradT_sub_grada', 
                'brunt_N2', 'brunt_N2_composition_term', 'mixing_type',
                'logT', 'logP', 'logRho', 'opacity']
  other_keys = [] # ['luminosity', 'log_L', 'log_Lrad'] 
  required_fileds = fixed_keys + other_keys

  hist         = dic_hist['hist']
  Mcc_ar       = hist['mass_conv_core']
  mod_num_arr  = hist['model_number'].astype(int)
  ind_row      = np.where(mod_num_arr == mod_num_prof)[0] 
  if ind_row == -1:
    print 'Error: plot_profile: diagnose_convective_boundaries: failed to match "mod_num_prof"={0}'.format(mod_num_prof)
    raise SystemExit
  elif len(ind_row) == 0:
    print 'Error: plot_profile: diagnose_convective_boundaries: "ind_row" is empty'
    print mod_num_prof
    raise SystemExit
  else:
    ind_row    = ind_row[0]
  hist_row     = hist[ind_row]
  Mcc          =  Mcc_ar[ind_row]

  dic_avail_keys = {}
  for name in names: dic_avail_keys[name] = 0.0
  dic_keys = read.check_key_exists(dic_avail_keys, required_fileds)

  for a_name in fixed_keys:
    if not dic_keys[a_name]:
      print 'Error: plot_profile: diagnose_convective_boundaries: {0} not in profiles'.format(a_name)
      raise SystemExit

  #--------------------------------
  # Prepare the plot and panels
  #--------------------------------
  fig = plt.figure(figsize=(8,8), dpi=200)
  ax1 = fig.add_subplot(221)
  ax2 = fig.add_subplot(222)
  ax3 = fig.add_subplot(223)
  ax4 = fig.add_subplot(224)
  fig.subplots_adjust(left=0.11, right=0.98, bottom=0.06, top=0.98, wspace=0.25, hspace=0.05)

  # consider ylim() carefully
  if xaxis_from is not None or xaxis_to is not None:
    ax1.set_ylim(0.3, 0.5)
    ax3.set_ylim(-0.004, 0.004)
  if xaxis_from is None and xaxis_to is None: 
    ax1.set_ylim(0.36, 0.40)
    ax3.set_ylim(-.004, .004)
  if xaxis_from is None: xaxis_from = Mcc * 0.95 # 0.99
  if xaxis_to   is None: xaxis_to   = Mcc * 1.05 # 1.01
  left   = 0.05
  bottom = 0.05

  flag_y = 'y_reconst' in prof.dtype.names
  flag_v = 'log_conv_vel' in prof.dtype.names

  mass     = prof['mass']
  h1       = prof['h1']
  # he4      = prof['he4']
  # c12      = prof['c12']
  # n14      = prof['n14']
  # o16      = prof['o16']
  kappa    = prof['opacity']
  logT     = prof['logT']
  logP     = prof['logP']
  logRho   = prof['logRho']
  Rho      = np.power(10, logRho)
  grada    = prof['grada']
  gradL    = prof['gradL']
  gradT    = prof['gradT']
  gradr    = prof['gradr']
  super_ad = prof['gradT_sub_grada']
  d_grad_L = np.log10(np.abs(gradr-gradL)) * (gradr-gradL) / np.abs(gradr-gradL)
  d_grad_a = np.log10(np.abs(gradr-grada)) * (gradr-grada) / np.abs(gradr-grada)
  log_Gamma= prof['log_mlt_Gamma']
  log_v_c  = prof['log_conv_vel']
  log_Lc_L = np.log10(prof['lum_conv_div_L'])
  # kappa    = prof['opacity']
  mix      = prof['mixing_type']
  # mix      = prof['mlt_mixing_type']
  N2       = prof['brunt_N2']
  N2_mu    = prof['brunt_N2_composition_term']
  N2[N2<=0] = 1e-100
  N_Hz     = np.sqrt(N2)
  N_cd     = N_Hz * Hz_to_cd
  N2_mu[N2_mu<=0] = 1e-100
  N_mu_Hz  = np.sqrt(N2_mu)
  N_mu_cd  = N_mu_Hz * Hz_to_cd
  if flag_v: v_conv = np.power(10.0, prof['log_conv_vel'])
  if flag_y: y_rec  = prof['y_reconst']
  # if dic_keys['luminosity']: Ltot   = prof['luminosity']
  # if dic_keys['log_L']: Ltot = np.power(10, prof['log_L'])
  # if dic_keys['log_Lrad']: Lrad = np.power(10, prof['log_Lrad']) 
  # if dic_keys["luminosity_conv"]: Lconv  = prof['luminosity_conv']
  # if dic_keys['lum_conv']: Lconv = prof['lum_conv']

  a0       = 9. / 4.  # see Cox & Guili (1968), below Eq. 14.73
  mlt_gamma= np.power(10, log_Gamma)
  zeta     = a0 * mlt_gamma**2 / (1.0 + mlt_gamma*(1.0 + a0*mlt_gamma))

  #--------------------------------
  # Panel 1. Temperature Gradients
  #--------------------------------
  ax1.fill_between(mass, y1=-100, y2=+100, where=(mix==1), color='blue', alpha=0.25, zorder=0)
  ax1.fill_between(mass, y1=-100, y2=+100, where=(mix==2), color='yellow', alpha=0.15, zorder=0)
  ax1.fill_between(mass, y1=-100, y2=+100, where=(mix==3), color='0.25', alpha=0.15, zorder=0)
  ax1.plot(mass, gradT, linestyle='solid', lw=2, color='black', zorder=1, label=r'$\nabla$')
  ax1.plot(mass, gradL, linestyle='dashed', lw=1, color='red', zorder=2, label=r'$\nabla_{\rm Led}$')
  ax1.plot(mass, gradr, linestyle='dashdot', lw=1, color='blue', zorder=2, label=r'$\nabla_{\rm r}$')
  ax1.plot(mass, grada, linestyle='dotted', lw=1, color='green', zorder=2, label=r'$\nabla_{\rm ad}$')
  ax1.axvline(x=Mcc, linestyle='dotted', lw=1, color='red', zorder=2, label=r'M$_{\rm cc}$')
  leg1 = ax1.legend(loc=1, fontsize='small')
  ax1.set_xlim(xaxis_from, xaxis_to)
  ax1.set_xticklabels(())
  ax1.set_ylabel(r'Temperature Gradients')
  # ax1.set_ylim(0.25, 0.75)
  txt_mass = r'{0:.2f}'.format(mass[0]) + r' M$_\odot$'
  txt_Xc   = r'$X_{\rm c}$=' + r'{0:.4f}'.format(h1[-1])
  ax1.annotate(txt_mass, xy=(0.70, 0.16), xycoords='axes fraction', fontsize='medium')
  ax1.annotate(txt_Xc, xy=(0.70, 0.08), xycoords='axes fraction', fontsize='medium')
  ax1.annotate('(a)', xy=(left, bottom), xycoords='axes fraction', fontsize='medium', color='black')

  #--------------------------------
  # Panel 2: Luminosities
  #--------------------------------
  ax2.fill_between(mass, y1=-100, y2=+100, where=(mix==1), color='blue', alpha=0.25, zorder=0) # convective
  ax2.fill_between(mass, y1=-100, y2=+100, where=(mix==2), color='yellow', alpha=0.15, zorder=0)  # softening
  ax2.fill_between(mass, y1=-100, y2=+100, where=(mix==3), color='0.25', alpha=0.15, zorder=0) # overshoot
  ax2.plot(mass, log_Gamma, linestyle='solid', color='black', lw=1.5, zorder=1, label=r'$\log\Gamma_{\rm conv}$')
  ax2.plot(mass, log_v_c, linestyle='dashed', color='blue', lw=1.5, zorder=1, label=r'$\log v_{\rm conv}$')
  ax2.plot(mass, log_Lc_L, linestyle='dashdot', color='red', lw=1.5, label=r'$L_{\rm conv}/L_{\rm tot}$')
  ax2.axvline(x=Mcc, linestyle='dotted', lw=1, color='red', zorder=2)
  ax2.plot(mass, logP-18, 'purple', lw=1, zorder=1, label=r'$\log P - 18$')
  ax2.plot(mass, logT - 6, 'r-', lw=1, zorder=1, label=r'$\log T - 6$')
  ax2.plot(mass, logRho, 'g-', lw=1, zorder=1, label=r'$\log\rho$')
  ax2.plot(mass, kappa, 'b-', lw=1, zorder=1, label=r'$\kappa$')
  # ax2.plot(mass, np.log10(he4), linestyle='dashed', color='blue', lw=1, label=r'$^4$He')
  # ax2.plot(mass, np.log10(c12), linestyle='dashdot', color='red', lw=1, label=r'$^{\rm 12}$C')
  # ax2.plot(mass, np.log10(n14), linestyle='dotted', color='green', lw=1, label=r'$^{\rm 14}$N')
  # ax2.plot(mass, np.log10(o16), linestyle='solid', color='purple', lw=1, label=r'$^{\rm 16}$O')
  # leg2 = ax2.legend(loc=1, bbox_to_anchor=[0.99, 0.8], fontsize='small')
  leg2 = ax2.legend(loc=1, fontsize='small')
  ax2.set_xticklabels(())
  ax2.set_xlim(xaxis_from, xaxis_to)
  #ax2.set_ylabel(r'Fraction Carried by Convection')
  ax2.set_ylim(-4, 7)
  ax2.set_ylabel(r'Convection Properties')
  # ax2.set_ylim(-6, 0.1)
  ax2.annotate('(b)', xy=(left, bottom), xycoords='axes fraction', fontsize='medium', color='black')

  #--------------------------------
  # Panel 3. Convective Discriminant
  #--------------------------------
  ax3.fill_between(mass, y1=-100, y2=+100, where=(mix==1), color='blue', alpha=0.25, zorder=0) # convective
  ax3.fill_between(mass, y1=-100, y2=+100, where=(mix==2), color='yellow', alpha=0.15, zorder=0)  # softening
  ax3.fill_between(mass, y1=-100, y2=+100, where=(mix==3), color='0.25', alpha=0.15, zorder=0) # overshoot
  ax3.axhline(y=0, linestyle='dotted', lw=0.5, zorder=1)
  ax3.semilogy(mass, (gradr-gradL), 'k.-', lw=1, zorder=1, label=r'$\nabla_{\rm r}-\nabla_{\rm L}$')
  ax3.semilogy(mass, (gradr-grada), 'b.-.', lw=1, zorder=1, label=r'$\nabla_{\rm r}-\nabla_{\rm ad}$')
  ax3.semilogy(mass, super_ad, linestyle='dashdot', color='red', lw=1, zorder=1, label=r'$\nabla-\nabla_{\rm ad}$')
  ax3.semilogy(mass, np.log10(zeta), 'g.-', zorder=1, label=r'$\log\zeta_{\rm MLT}$')
  ax3.axvline(x=Mcc, linestyle='dotted', lw=1, color='red', zorder=2, label=r'M$_{\rm cc}$')
  if flag_y: ax3.plot(mass, y_rec, linestyle='dotted', color='black', lw=1, zorder=1, label=r'$y_{\rm reconst}$')
  # ax3.plot(mass, h1, linestyle='solid', color='black', lw=1, zorder=1, label=r'$^1$H')
  # leg3 = ax3.legend(loc=1, bbox_to_anchor=[0.99, 0.7], fontsize='small')
  leg3 = ax3.legend(loc=1, fontsize='small')
  ax3.set_xlabel(r'Enclosed Mass $m$ [M$_\odot$]')
  ax3.set_ylabel(r'$\nabla_{\rm r}-\nabla_{\rm L}$')
  ax3.set_xlim(xaxis_from, xaxis_to)
  ax3.set_yscale('symlog', linthreshy=1e-5)
  ax3.annotate('(c)', xy=(left, bottom), xycoords='axes fraction', fontsize='medium', color='black')

  #--------------------------------
  # Panel 4. Convective Velocity
  #--------------------------------
  ax4.fill_between(mass, y1=-10, y2=+10, where=(mix==1), color='blue', alpha=0.25, zorder=0) # convective
  ax4.fill_between(mass, y1=-10, y2=+10, where=(mix==2), color='yellow', alpha=0.15, zorder=0)  # softening
  ax4.fill_between(mass, y1=-10, y2=+10, where=(mix==3), color='0.25', alpha=0.15, zorder=0) # overshoot
  # if flag_v: ax4.plot(mass, v_conv/1e2, linestyle='solid', color='black', lw=2, zorder=1, label=r'$v_{\rm conv}$')
  ax4.plot(mass, np.log10(N_cd), 'k.-', lw=2, zorder=1, label=r'$\log N(r)$')
  ax4.plot(mass, np.log10(N_mu_cd), 'b.-', lw=1, zorder=1, label=r'$\log N_{\mu}$')
  ax4.axvline(x=Mcc, linestyle='dotted', lw=1, color='red', zorder=2, label=r'M$_{\rm cc}$')
  leg4 = ax4.legend(loc=4, fontsize='small')
  ax4.set_xlim(xaxis_from, xaxis_to)
  ax4.set_xlabel(r'Enclosed Mass $m$ [M$_\odot$]')
  ax4.set_ylim(-4, 3)
  # ax4.set_yscale('log')
  ax4.set_ylabel(r'Brunt Vaisala Frequency $\log N$ [d$^-1$]')
  ax4.annotate('(d)', xy=(left, bottom), xycoords='axes fraction', fontsize='medium', color='black')

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  if file_out:
    plt.savefig(file_out, transparent=True, dpi=400)
    print ' - plot_profile: diagnose_convective_boundaries: {0} created'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def gradT_for_quasi_adiabatic_overshoot(dic_prof, xrange=[0, 1], yrange=[0.3, 0.5], file_out=None):
  """
  show grada and gradL in addition to the mesh point where the boundary is
  """
  if file_out is None: return

  fig = plt.figure(figsize=(6,4), dpi=300)
  # fig.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97, wspace=0.04)
  left = 0.12
  width = 0.86
  top = fig.add_axes([left, 0.73, width, 0.24])
  bot = fig.add_axes([left, 0.11, width, 0.60])

  prof    = dic_prof['prof']
  mass    = prof['mass']
  grada   = prof['grada']
  gradr   = prof['gradr']
  gradL   = prof['gradL']
  gradT   = prof['gradT']
  d_grads = grada - gradr

  # factor   = get_qad_factor(dic_prof, A=1., f0=1e-3, f=0.03)
  # my_gradT = factor * grada + (1.0 - factor) * gradr
  
  if 'adjust_mlt_gradT_fraction' not in prof.dtype.names:
    logging.error('plot_profile: gradT_for_quasi_adiabatic_overshoot: adjust_mlt_gradT_fraction not available')
    raise SystemExit, 'plot_profile: gradT_for_quasi_adiabatic_overshoot: adjust_mlt_gradT_fraction not available'
  frac    = prof['adjust_mlt_gradT_fraction']

  top.axhline(y=0, linestyle='dotted')
  top.fill_between(mass, y1=-100, y2=100, where=(d_grads<=0), color='blue', alpha=0.25, zorder=1)
  top.plot(mass, d_grads, linestyle='solid', color='black', lw=2, zorder=2, 
           label=r'$\nabla_{\rm ad}-\nabla_{\rm rad}$')
  top.plot(mass, frac, linestyle='solid', color='red', marker='o', lw=2, zorder=2, 
           label=r'$\eta_{\rm ad}$')
  top.set_xticklabels(())
  top.set_ylabel(r'$\nabla_{\rm ad}-\nabla_{\rm rad}$')
  top.set_ylim(-0.1, 1.3)
  top.set_xlim(xrange[0], xrange[1])
  top.legend(loc=1)


  bot.fill_between(mass, y1=0, y2=10, where=(d_grads<=0), color='blue', alpha=0.25, zorder=0)
  bot.plot(mass, gradT, linestyle='solid', color='grey', lw=3, zorder=1, label=r'$\nabla$')
  bot.plot(mass, gradr, linestyle='solid', color='black', lw=1, zorder=2, label=r'$\nabla_{\rm rad}$')
  bot.plot(mass, grada, linestyle='dashed', color='red', lw=1, zorder=2, label=r'$\nabla_{\rm ad}$')

  leg = bot.legend(loc=1)
  bot.set_xlim(xrange[0], xrange[1])
  bot.set_ylim(yrange[0], yrange[1])
  bot.set_xlabel(r'Enclosed Mass [M$_\odot$]')
  bot.set_ylabel(r'Temperature Gradients')

  plt.savefig(file_out)
  print ' - gradT_for_quasi_adiabatic_overshoot: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def plot_multiple_columns(dic_prof, xaxis=None, list_yaxis=[], list_lbls=[], xaxis_from=None, xaxis_to=None,
                          multiply_yaxis_by=1.0, yaxis_from=None, yaxis_to=None, xtitle=None, ytitle=None, leg_loc=1,
                          file_out=None):
  """
  Plot multiple quantities on the y-axis at the same time. all requested quantities must be already available in the
  input dictionary.
  @param dic_prof: dictionary with 'prof' and 'header' keys, containing the whole information from the MESA profile outpt
  @type dic_prof: dictionary
  @param multiply_yaxis_by: multiply all y-axis columns by a fixed multiplier to get rid of small or large valued columns.
  @type multiply_yaxis_by: float
  """
  header = dic_prof['header']
  prof   = dic_prof['prof']
  names  = prof.dtype.names

  if xaxis not in names:
    print 'Error: plot_profile: plot_multiple_columns: {0} not in profile'.format(xaxis)
    raise SystemExit
  for a_name in list_yaxis:
    if a_name not in names:
      print 'Error: plot_profile: plot_multiple_columns: yaxis: {0} not in profile'.format(a_name)
      raise SystemExit
  n_y = len(list_yaxis)

  #--------------------------------
  # Prepare the Plot
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax  = fig.add_subplot(111)
  fig.subplots_adjust(left=0.13, right=0.97, bottom=0.13, top=0.97)

  if list_lbls is None: n_lbl = 0
  else: n_lbl = len(list_lbls)

  #--------------------------------
  # Set Up the Colors
  #--------------------------------
  import matplotlib.cm as cm
  import itertools
  cmap = plt.get_cmap('brg')
  norm = matplotlib.colors.Normalize(vmin=0, vmax=n_y)
  color = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
  linestyles = itertools.cycle(['solid', 'dashed', 'dashdot', 'dotted'])

  #--------------------------------
  # Loop over the y-axis and plot them
  #--------------------------------
  for i_y, yaxis in enumerate(list_yaxis):
    xvals = prof[xaxis]
    yvals = prof[yaxis]

    if multiply_yaxis_by != 1.0: yvals = yvals * multiply_yaxis_by

    cl = color.to_rgba(i_y)
    ls = linestyles.next()

    if n_lbl>0:
      ax.plot(xvals, yvals, linestyle=ls, color=cl, lw=2, label=r''+list_lbls[i_y])
    else:
      ax.plot(xvals, yvals, linestyle=ls, color=cl, lw=2)

  #--------------------------------
  # Legend and Annotations
  #--------------------------------
  x_from = np.min(xvals); x_to = np.max(xvals)
  y_from = np.min(yvals); y_to = np.max(yvals)
  if xaxis_from is not None: x_from = xaxis_from
  if xaxis_to   is not None: x_to   = xaxis_to
  if yaxis_from is not None: y_from = yaxis_from
  if yaxis_to   is not None: y_to   = yaxis_to
  ax.set_xlim(x_from, x_to)
  ax.set_ylim(y_from, y_to)
  if xtitle: ax.set_xlabel(xtitle)
  if ytitle: ax.set_ylabel(ytitle)

  leg = ax.legend(loc=leg_loc, fontsize='small')
  leg.get_frame().set_alpha(0.5)

  if file_out:
    plt.savefig(file_out, dpi=200)
    print ' - plot_profile: plot_multiple_columns: {0} saved'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def plot_hist_mesh(dic_prof, logT_step=None, mass_step=None, file_out=None):
  """
  This function makes a 3-panel plot, showing the mesh distribution in the input model (MESA profile) in terms
  of logT, mass and radius.
  @param
  """
  if not dic_prof.has_key('header') or not dic_prof.has_key('prof'):
    message = 'Error: plot_profile: plot_hist_mesh: "header" or "prof" keys are missing from input dictionary'
    raise SystemExit, message

  if not any([logT_step, mass_step]):
    message = 'Error: plot_profile: plot_hist_mesh: Choose one of logT_step, mass_step'
    raise SystemExit, message

  header = dic_prof['header']
  prof = dic_prof['prof']
  prof_names = prof.dtype.names
  dic_avail_names = {}
  for name in prof_names: dic_avail_names[name] = 0.0
  requested_keys = ['logT', 'mass', 'dq', 'logdq', 'logR', 'radius', 'mixing_type',
                    'brunt_N', 'brunt_N2']
  dic_keys = read.check_key_exists(dic_avail_names, requested_keys)

  if not dic_keys['logT'] or not dic_keys['mass']:
    print 'Warning: plot_profile: plot_hist_mesh: "logT" and "mass" unavailable from prof recarray '
    return None
  if not dic_keys['dq'] and not dic_keys['logdq']:
    print 'Warning: plot_profile: plot_hist_mesh: "dq" and "logdq" both unavailable from prof recarray '
    return None
  if not dic_keys['radius'] and not dic_keys['logR']:
    print 'Warning: plot_profile: plot_hist_mesh: "radius" and "logR" both unavailable from prof recarray '
    return None

  #--------------------------------
  # Prepare arrays
  #--------------------------------
  logT = prof['logT']
  mass = prof['mass']
  if dic_keys['dq'] and not dic_keys['logdq']: dq = prof['dq']
  if not dic_keys['dq'] and dic_keys['logdq']: dq = np.power(10.0, prof['logdq'])
  if dic_keys['dq'] and dic_keys['logdq']: dq = prof['dq']
  if dic_keys['radius'] and not dic_keys['logR']: radius = prof['radius']
  if not dic_keys['radius'] and dic_keys['logR']: radius = np.power(10.0, prof['logR'])
  if dic_keys['radius'] and dic_keys['logR']: radius = prof['radius']
  mixing_type = prof['mixing_type']

  nlines = len(dq)
  dr = np.zeros(nlines)
  for i in range(1, nlines-1): dr[i] = radius[i-1] - radius[i]

  #--------------------------------
  # Prepare figure
  #--------------------------------
  fig, (top, bot) = plt.subplots(2, figsize=(9, 5))
  plt.subplots_adjust(left=0.07, right=0.92, bottom=0.09, top=0.98, hspace=0.25)

  # highlight the background with boxes representing different mixing zones
  if dic_keys['mixing_type']:
    top.fill_between(logT, y1=0, y2=10000, where=(mixing_type==1), color='blue', alpha=0.5, zorder=1)
    top.fill_between(logT, y1=0, y2=10000, where=(mixing_type==3), color='grey', alpha=0.5, zorder=1)
    bot.fill_between(mass, y1=0, y2=10000, where=(mixing_type==1), color='blue', alpha=0.5, zorder=1)
    bot.fill_between(mass, y1=0, y2=10000, where=(mixing_type==3), color='grey', alpha=0.5, zorder=1)

  # Fix number of bins and ranges:
  #if not logT_step: logT_step = 0.01
  #if not mass_step: mass_step = 0.001

  logT_from = max(logT); logT_to = min(logT)
  mass_from = 0.0; mass_to = mass[0] * 0.99  # to avoid immense mesh below surface hiding the interior

  #--------------------------------
  # Histogram w.r.t. logT
  #--------------------------------
  n_logT = (logT[-1] - logT[0])/logT_step
  hist_arr, bins, patches = top.hist(logT, bins=n_logT, range=(logT_to, logT_from), 
                                     color='blue', zorder=1)
  hist_arr = np.asarray(hist_arr)
  top.set_xlim(logT_from, logT_to)
  top.set_xlabel(r'Temperature $\log T$ [K]')
  top.set_ylabel(r'Num. Mesh')
  top.set_ylim(0, max(hist_arr)*1.05)

  if 'gaussian_logT' in prof.dtype.names:
    gaussian_logT = prof['gaussian_logT'] 
    gaussian_norm = gaussian_logT / np.max(gaussian_logT)
    gaussian      = gaussian_norm * max(hist_arr) * 0.9
    # print max(prof['gaussian_logT']), max(hist_arr)/5
    top.plot(logT, gaussian_logT, 'g-')
    bot.plot(mass, gaussian_logT, 'g-')

  top.annotate(r'N$_{\rm mesh}=$'+str(nlines), xy=(0.3, 0.90), xycoords='axes fraction', fontsize='medium')
  str_Xc = r'%0.4f' % (dic_prof['header']['center_h1'], )
  top.annotate(r'$\delta \log T$='+str(logT_step), xy=(0.3, 0.80), xycoords='axes fraction')
  top.annotate(r'$X_{\rm c}=$'+str_Xc, xy=(0.3, 0.70), xycoords='axes fraction')

  #--------------------------------
  # Histogram w.r.t. mass
  #--------------------------------
  n_mass = mass[0]/mass_step
  hist_arr, bins, patches = bot.hist(mass, bins=n_mass, range=(mass_from, mass_to), 
                                     color='blue', zorder=1)
  hist_arr = np.asarray(hist_arr)
  bot.set_xlim(mass_from, mass_to)
  bot.set_xlabel(r'Enclosed Mass [M$_\odot$]')
  bot.set_ylabel(r'Num. Mesh')
  bot.set_ylim(0, max(hist_arr)*1.05)
  bot.annotate(r'$\delta M$='+str(mass_step), xy=(0.3, 0.90), xycoords='axes fraction', fontsize='medium')

  #--------------------------------
  # Add the Brunt-Vaisala Frequency, if available
  #--------------------------------
  if not dic_keys['brunt_N2'] and dic_keys['brunt_N']:
    brunt_N = prof['brunt_N']
    neg_indx = brunt_N <= 0.0
    #brunt_N[neg_indx] = 1e-20
    brunt_N2 = brunt_N**2.0
  if dic_keys['brunt_N2'] and not dic_keys['brunt_N']:
    brunt_N2 = prof['brunt_N2']
  log_brunt_N2 = np.log10(brunt_N2)

  # Add Buoyancy profile on the right axis
  ax0 = top.twinx()
  ax0.plot(logT, log_brunt_N2, color='Red', lw=1.5)
  ax0.set_xlim(logT_from, logT_to)
  ax0.set_ylim(-10, -4)
  ax0.set_ylabel(r'$\log N^2$ [Hz]', color='red')
  for tl in ax0.get_yticklabels():
    tl.set_color('r')

  ax1 = bot.twinx()
  ax1.plot(mass, log_brunt_N2, color='Red', lw=1.5)
  ax1.set_xlim(mass_from, mass_to)
  ax1.set_ylim(-10, -4)
  ax1.set_ylabel(r'$\log N^2$ [Hz]', color='red')
  for tl in ax1.get_yticklabels():
    tl.set_color('r')

  #--------------------------------
  # Save the figure
  #--------------------------------
  if not file_out: file_out = 'Hist-Mesh.pdf'
  plt.savefig(file_out)
  print ' - Plot %s Created.' % (file_out, )
  plt.close()

  return None


#=================================================================================================================
def identify_mixing_type_edges(indx_arr):
  """
  This function receives an array with indexes in the profile model, where mixing_type == certain_mixing_type. However,
  during the evolution the extent and the number of different mixing zones vary. This function determines the number of
  zones with certain_mixing_type, and also returns the index of their boundaries.
  @param indx_arr: array with indexes in the profile model where mixing_type == certain_mixing_type
  @type indx_arr: Numpy array
  @return: list of dictionaries with zone information. Each dictionary has the following three keys:
      - zone: integer, which is the zone label, starting from the surface inward
      - from: integer, index of the upper edge of the zone
      - to: integer, index of the inner edge of the zone
  @rtype: list of dictionaries
  """
  n_indx = len(indx_arr)
  if n_indx < 2: return None

  count_zones = 1  # at least one zone exists
  indx_start = indx_arr[0]; indx_end = -1; indx_next = -1
  list_dic_zones = []



  for i in range(n_indx-1):
    jump = False
    indx = indx_arr[i]
    indx_next = indx_arr[i+1]

    if (indx_next == indx + 1): continue
    else: jump = True

    if jump:
      indx_end = indx

      dic_zone = {}
      dic_zone['zone'] = count_zones
      dic_zone['from'] = indx_start
      dic_zone['to']   = indx_end
      list_dic_zones.append(dic_zone)

      indx_start = indx_next
      count_zones += 1

  # Add the last zone manually:
  dic_zone = {}
  dic_zone['zone'] = count_zones
  dic_zone['from'] = indx_start
  dic_zone['to']   = indx_arr[-1]
  list_dic_zones.append(dic_zone)

  return list_dic_zones


#=================================================================================================================
def plot_compare_atm(list_dic_prof, list_lbls=[]):
  """
  This function receives a list of MESA profiles, and checks if the optical depth tau or log_tau is stored in it.
  Then, it plots the run of temperature, pressure and density in the outer atmosphere from log_tau = 2 to log_tau = -7
  @param list_dic_prof: list of dictionaries, returned by e.g. read.read_multiple_mesa_files. Each dictionary has at
        least three keys: 1. filename, 2. header and 3. prof. The arrays returned by header and prof are numpy recarray.
  @type list_dic_prof: list of dictionaries
  @param list_lbls: list of strings giving the label for each of the profiles
  @type list_lbls: list of strings
  @return: None
  @rtype: None
  """
  n_prof = len(list_dic_prof)
  if n_prof == 0:
    message = 'Erro: plot_profile: plot_compare_atm: Input list is empty!'
    raise SystemExit, message

  requested_keys = ['tau', 'logtau', 'pressure', 'logP', 'logT', 'temperature', 'logRho']
  sample_prof = list_dic_prof[0]['prof']

  if not sample_prof.has_key('prof'):
    message = 'Error: plot_profile: plot_compare_atm: the key: "prof" is missing from input dictionaries'
    raise SystemExit, message

  #header, prof = read.read_mesa(filename=sample_prof)
  names = sample_prof.dtype.names
  dic_avail_names = {}
  for key in names: dic_avail_names[key] = 0.0
  dic_keys = read.check_key_exists(dic_avail_names, requested_keys)
  flag_tau = dic_keys['tau']
  flag_log_tau = dic_keys['logtau']
  flag_P = dic_keys['pressure']
  flag_logP = dic_keys['logP']
  flag_T = dic_keys['temperature']
  flag_logT = dic_keys['logT']
  flag_logRho = dic_keys['logRho']

  fig = plt.figure()
  plt.subplots_adjust(left=0.09, right=0.99, bottom=0.09, top=0.99)
  min_col = 0; max_col = n_prof
  colormap = plt.get_cmap('spectral')
  norm = matplotlib.colors.Normalize(vmin=min_col, vmax=max_col)
  color = matplotlib.cm.ScalarMappable(norm=norm, cmap=colormap)

  for i_prof in range(n_prof):
    file = list_dic_prof[i_prof]['filename']
    prof = list_dic_prof[i_prof]['prof']
    header = list_dic_prof[i_prof]['header']

    zones = prof['zone']
    nlins = len(zones)

    if flag_tau and not flag_log_tau: log_tau = np.log10(prof['tau'])
    if not flag_tau and flag_log_tau: log_tau = prof['logtau']
    if flag_tau and flag_log_tau:     log_tau = prof['logtau']
    if not flag_tau and not flag_log_tau:
      message = 'Error: plot_profile: plot_compare_atm: Cannot continue without tau and logtau!'
      raise SystemExit, message

    if flag_P and not flag_logP: logP = np.log10(prof['pressure'])
    if not flag_P and flag_logP: logP = prof['logP']
    if flag_P and flag_logP:     logP = prof['logP']
    if not flag_P and not flag_logP:
      message = 'Error: plot_profile: plot_compare_atm: Cannot continue without pressure and logP!'
      raise SystemExit, message

    if flag_T and not flag_logT: logT = np.log10(prof['temperature'])
    if not flag_T and flag_logT: logT = prof['logT']
    if flag_T and flag_logT:     logT = np.log10(prof['temperature'])
    if not flag_T and not flag_logT:
      message = 'Error: plot_profile: plot_compare_atm: Cannot continue without temperature and logT!'
      raise SystemExit, message

    if flag_logRho:
      logRho = prof['logRho']
    else:
      logRho = np.zeros(nlines)

    ax0 = fig.add_subplot(3,1,1)
    lbl = ''
    if list_lbls: lbl = list_lbls[i_prof]
    shift = i_prof*0.1
    shift = 0.0
    ax0.plot(log_tau, logT+shift, color=color.to_rgba(i_prof), label=r''+lbl)
    ax0.scatter(log_tau, logT+shift, color=color.to_rgba(i_prof), s=4)
    ax0.set_xlim(2, -3.1)
    ax0.set_xticklabels(())
    ax0.set_ylabel(r'Temperature $\log T$ [K]')
    ax0.set_ylim(4.2, 5)
    if list_lbls:
      leg0 = ax0.legend(loc=0, fontsize='small', shadow=True, frameon=True, fancybox=True)
      leg0.get_frame().set_alpha(0.8)

    ax1 = fig.add_subplot(3,1,2)
    ax1.plot(log_tau, logP, color=color.to_rgba(i_prof), label=r''+lbl)
    ax1.set_xlim(2, -3.1)
    ax1.set_xticklabels(())
    ax1.set_ylabel(r'Pressure $\log P$ [cgs]')
    ax1.set_ylim(2.5, 6)
    #if list_lbls: leg1 = ax1.legend(loc=0)

    ax2 = fig.add_subplot(3,1,3)
    ax2.plot(log_tau, logRho, color=color.to_rgba(i_prof), label=r''+lbl)
    ax2.set_xlim(2, -3.1)
    ax2.set_ylabel(r'Density $\log \rho$ [gr cm$^{-3}$]')
    ax2.set_xlabel(r'Optical Depth $\log \tau$')
    ax2.set_ylim(-11, -7.1)
    #if list_lbls: leg2 = ax2.legend(loc=0)

  pdf_full_path = 'Comp-Atm.pdf'
  plt.savefig(pdf_full_path)
  print ' - %s generated!' % (pdf_full_path, )
  plt.close()

  return None

#=================================================================================================================
def plot_compare_brunt(list_dic_prof, list_lbls=None, by_logT=False, by_mass=False, by_logR=False, obs_freq=None,
                       logT_from=None, logT_to=None, mass_from=None, mass_to=None, logN_from=None, logN_to=None,
                       file_out=None):
  """
  This function receives a list of full path to MESA profile files, reads them individually, and stores their Brunt-
  Vaisala (BV) frequency, and finally makes a plot comparing them.
  @param list_dic_prof: list of dictionaries, returned by e.g. read.read_multiple_mesa_files. Each dictionary has at
        least three keys: 1. filename, 2. header and 3. prof. The arrays returned by header and prof are numpy recarray.
  @type list_dic_prof: list of dictionaries
  @param list_lbls: list of strings giving the plotting label per each file; default = None
  @type list_lbls: list of strings
  @param by_logT: specifying the choice of x-axis (default)
  @type by_logT: boolean
  @param by_mass: specifying the choice of x-axis
  @type by_mass: boolean
  @param by_R: specifying the choice of x-axis
  @type by_R: boolean
  @param obs_freq: a list of observed frequencies in uHz unit
  @type obs_freq: list or tuple
  @param logT_from, mass_from: the inner part of the model in log_T, mass
  @type logT_from, mass_from: float
  @param logT_to, mass_to: the outer part of the model in log_T, mass
  @type logT_to, mass_to: float
  @param file_out: default=None; otherwise, gives the full path and name of the output plot file to be stored.
  @type file_out: string
  @return: None
  @rtype: None
  """
  list_flags = [by_logT, by_mass, by_logR]
  if all(list_flags):
    message = 'Error: plot_profile: plot_compare_brunt: All flags are set to True; Only one must be chosen!'
    raise SystemExit, message
  elif not any(list_flags):
    message = 'Error: plot_profile: plot_compare_brunt: At least one flag has to be chosen!'
    raise SystemExit, message
  else:
    pass

  n_prof = len(list_dic_prof)
  if n_prof == 0:
    message = 'Erro: plot_profile: plot_compare_N2: Input list is empty!'
    raise SystemExit, message

  sample_prof = list_dic_prof[0]['prof']
  avail_names = sample_prof.dtype.names
  dic_avail_names = {}
  for name in avail_names:
    dic_avail_names[name] = 0.0
  requested_keys = ['temperature', 'logT', 'mass', 'logR', 'radius', 'brunt_N', 'brunt_N2',
                    'brunt_N2_composition_term', 'lamb_S']
  dic_keys = read.check_key_exists(dic_avail_names, requested_keys)
  message = 'Error: plot_profile: plot_compare_brunt: Missing required filed! Cannot Continue ...'
  if (not dic_keys['brunt_N'] and not dic_keys['brunt_N2']): raise SystemExit, message
  two_panel = dic_keys['brunt_N2_composition_term']   # if avaiable, add extra panel for this brunt term
  if by_logT and (not dic_keys['temperature'] and not dic_keys['logT']): raise SystemExit, message
  if by_logR and (not dic_keys['radius'] and not dic_keys['logR']): raise SystemExit, message
  if by_mass and not dic_keys['mass']: raise SystemExit, message

  fig = plt.figure(figsize=(6,4), dpi=200)
  plt.subplots_adjust(left=0.09, right=0.98, bottom=0.106, top=0.98)
  cmap = plt.get_cmap('spectral')
  norm = matplotlib.colors.Normalize(vmin=0, vmax=n_prof)
  color = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
  linestyles = itertools.cycle(['solid', 'dashed', 'dotted', 'dashdot'])

  for i in range(n_prof):
    file = list_dic_prof[i]['filename']
    #header, prof = read.read_mesa(file)
    prof = list_dic_prof[i]['prof']
    if by_logT:
      if dic_keys['logT']: logT = prof['logT']
      if not dic_keys['logT'] and dic_keys['temperature']: logT = np.log10(prof['temperature'])
    if by_mass:
      mass = prof['mass']
    if by_logR:
      if dic_keys['logR']: logR = prof['logR']
      if not dic_keys['logR'] and dic_keys['radius']: logR = np.log10(dic_keys['radius'])
    if dic_keys['brunt_N'] and not dic_keys['brunt_N2']:  N = prof['brunt_N']
    if dic_keys['brunt_N2'] and not dic_keys['brunt_N']:
      N2 = prof['brunt_N2']
      N2[np.where((N2 <= 0.0))] = 1e-10
      N = np.sqrt(N2)
    N[:] = N[:] - 1e-5
    N_uHz = N[:] * 1e6
    logN = np.log10(N_uHz)
    if dic_keys['lamb_S']:
      Sl = prof['lamb_S']
      Sl_uHz = Sl * 1e6
      logS = np.log10(Sl_uHz)
    else: logS = 0.0

    if by_logT and not logT_from: logT_from = 7.6# logT[0]
    if by_logT and not logT_to:   logT_to   = 4.0 # logT[-1]
    if by_mass and not mass_from: mass_from = 0.0
    if by_mass and not mass_to:   mass_to   = mass[0]
    if not logN_from: logN_from = 0.95
    if not logN_to:   logN_to   = 4.0

    if by_logT: xaxis = logT; xr = (logT_from, logT_to); xtitle = r'Temperature $\log T$'
    if by_mass: xaxis = mass; xr = (mass_from, mass_to); xtitle = r'Star Mass [M$_\odot$]'
    if by_logR: xaxis = logR; xr = (0.0, logR[0]); xtitle = r'Star Radius [R$_\odot$]'

    core_name = file[file.rfind('/')+1:]
    fys_param = file.strip().split('-')
    if len(fys_param) != 9: fys_param = []
    if list_lbls:
      lbl = list_lbls[i]
    elif fys_param:
      lbl = r'' + str(fys_param[2])
    else: lbl = []
    if lbl:
      plt.plot(xaxis, logN, color=color.to_rgba(i), linestyle=linestyles.next(), label=lbl)
    else:
      plt.plot(xaxis, logN, color=color.to_rgba(i), linestyle=linestyles.next())

  if obs_freq is not None:
    n_obs = len(obs_freq)
    for i in range(n_obs):
      plt.axhline(y=np.log10(obs_freq[i]), linestyle='-', color='Navy')

  if dic_keys['lamb_S']: plt.plot(xaxis, logS, '--', color='black', label=r'$S_{\ell}$')
  plt.xlim(*xr)
  plt.xlabel(xtitle)
  plt.ylim(logN_from, logN_to)
  plt.ylabel(r'Buoyancy Frequency $\log N$ [$\mu$Hz]')
  leg = plt.legend(loc=4, fontsize='x-small')
  leg.get_frame().set_alpha(0.5)
  if file_out:
    pdf_full_path = file_out
  else:
    pdf_full_path = 'Comp-Brunt_N.pdf'

  plt.savefig(pdf_full_path)
  print ' - plot_compare_brunt: %s Created!' % (pdf_full_path, )
  plt.close()

  return None

#=================================================================================================================
def plot_kappa_components(dic_prof, xaxis='logT', file_out=None):
  """
  The radiative opacity comprises of different components:
    - bound-bound transitions
    - bound-free transitions (including H^- breakup)
    - free-free transitions
  and a constant background opacity due to electrons scattering.
  This routine plots the Rossland opacity as read from profile, and also compares that with the Kramers law (for bf transition),
  free-free transitions and the electron scattering.
  @param dic_prof: dictionary with profile information returned by e.g. read.read_mesa()
  @type dic_prof: dictionary
  @param xaxis: the choice of x-axis parameter. Note that it should be already available in the profile data structure. default='logT'
  @type xaxis: string
  @param file_out: full path to the output plot file
  @type file_out: string
  @return None
  @rtype None
  """
  header = dic_prof['header']
  prof     = dic_prof['prof']
  names  = prof.dtype.names
  required_fileds = [xaxis, 'logRho', 'logT', 'opacity', 'x', 'y', 'z']
  for a_name in required_fileds:
    if a_name not in names:
      raise SystemExit, 'Error: plot_profile: plot_kappa_components: "{0}" not available in profile columns'.format(a_name)

  logT     = prof['logT']
  kappa   = prof['opacity']
  logRho  = prof['logRho']
  x           = prof['x']
  y            = prof['y']
  z            = prof['z']
  nz          = len(x)

  kappa_es = 0.200 * (1. + x)
  log_kappa_es = np.log10(kappa_es)
  log_kappa_bf = np.log10(4.3e25) + np.log10(z) + np.log10(1 + x) + logRho -3.5 * logT
  log_kappa_ff  = np.log10(3.7e22) + np.log10(x + y) + np.log10(1 + x) + logRho -3.5 * logT

  ind_H             = np.where((logT >= np.log10(3e3)) & (logT <= np.log10(6e3)) & (logRho>=-10) & (logRho <= -5)
                                            & (z <= 0.02))[0]
  n_H                = len(ind_H)
  if n_H > 0:
    print ' - plot_profile: plot_kappa_components: Including H- opacity!'
    log_kappa_H_aux = np.log10(2.5e-31) + np.log10(z/0.02) + np.log10(1 + x) + 0.5*logRho + 9. * logT
    log_kappa_H        = np.ones(nz) - 100.0
    log_kappa_H[ind_H] =log_kappa_H_aux[ind_H]
    kappa_bf              = np.power(10, log_kappa_bf) + np.power(10, log_kappa_H)
    log_kappa_bf        = np.log10(kappa_bf)

  #--------------------------------
  # Prepare the Plot
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax  = fig.add_subplot(111)
  fig.subplots_adjust(left=0.13, right=0.97, bottom=0.13, top=0.97)

  ax.plot(logT, np.log10(kappa), lw=2, color='black', linestyle='solid', label=r'Rosseland Mean Opacity $\kappa_{\rm rad}$')
  ax.plot(logT, log_kappa_es, lw=1.5, color='blue', linestyle='dashed', label=r'Electron Scattering $\kappa_{\rm es}$')
  ax.plot(logT, log_kappa_bf, lw=1.5, color='red', linestyle = 'dashdot', label=r'Bound-Free Opacity $\kappa_{\rm bf}$')
  ax.plot(logT, log_kappa_ff, lw=1.5, color='grey', linestyle='dotted', label=r'Free-Free Opacity $\kappa_{\rm ff}$')

  ax.set_xlim(7.5, 3.5)
  if xaxis == 'logT':
    xlabel = r'Temperature $\log T$ [K]'
  else:
    xlabel = r'' + xaxis.replace('_', ' ') + ''
  ax.set_xlabel(xlabel)
  ax.set_ylim(-1,  6)
  ax.set_ylabel(r'Radiative Opacity $\,\log \kappa\,$ [cm$^2$ gr$^{-1}$]')

  leg1 = ax.legend(loc=2, fontsize='small')

  if file_out:
    plt.savefig(file_out)
    print ' - plot_profile: plot_kappa_components: saved {0}'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def plot_compare_kappa(list_dic_prof, list_lbls, ref_indx = 0, by_logT=True, by_mass=False, by_R=False,
                       file_out=None):
  """
  This function receives a list of MESA profile files, checks if the opacity-related quantities are present, and then
  returns a multi-panel plot comparing all opacities, the differences and their derivatives (if available).
  @param list_dic_prof: list of dictionaries, returned by e.g. read.read_multiple_mesa_files. Each dictionary has at
        least three keys: 1. filename, 2. header and 3. prof. The arrays returned by header and prof are numpy recarray.
  @type list_dic_prof: list of dictionaries
  @param list_lbls: list of strings, giving one-to-one labels for each profile file.
  @type list_lbls: list of strings
  @param file_out: full path to the plot file
  @type file_out: string
  @return: None, but saves a plot
  @rtype: None
  """
  import itertools

  n_prof = len(list_dic_prof)
  if n_prof == 0:
    message = 'Erro: plot_profile: plot_compare_kappa: Input list is empty!'
    raise SystemExit, message

  prof_file = list_dic_prof[ref_indx]['filename']  # take as a sample file
  ref_header = list_dic_prof[ref_indx]['header']
  ref_prof = list_dic_prof[ref_indx]['prof']
  names = ref_prof.dtype.names
  dic_avail_names = {}
  for ind, key in enumerate(names):
    dic_avail_names[key] = 0.0
  dic_keys = read.check_key_exists(dic_avail_names, ['opacity', 'log_opacity', 'dkap_dlnrho_at_face', 'dkap_dlnt_at_face'])
  flag_kap = dic_keys['opacity']
  flag_log_kap = dic_keys['log_opacity']
  flag_dkap_dlnrho = dic_keys['dkap_dlnrho_at_face']
  flag_dkap_dlnT   = dic_keys['dkap_dlnt_at_face']

  # Interpolate the reference kappa into a gird of n_ref size
  if flag_kap and not flag_log_kap: kappa = ref_prof['opacity']
  if not flag_kap and flag_log_kap: kappa = np.power(10.0, ref_prof['log_opacity'])
  if flag_kap and flag_log_kap:     kappa = ref_prof['opacity']
  if not flag_kap and not flag_log_kap:
    message = 'Error: plot_profile: plot_compare_kappa: both opacity and log_opacity are missing in %s' % (file, )
    raise SystemExit, message
  kappa_ref = kappa
  n_ref = 2500

  nline = len(ref_prof['zone'])
  logT = ref_prof['logT']
  mass = ref_prof['mass']
  if by_R:       radius = np.power(10., ref_prof['logR'])
  if by_logT:    xaxis = logT
  if by_mass:    xaxis = mass
  if by_R:       xaxis = radius
  tck = interpolate.splrep(xaxis, kappa_ref, s=0)
  xaxis_interp = np.linspace(xaxis[0], xaxis[nline-1], n_ref)
  kappa_ref_interp = interpolate.splev(xaxis_interp, tck, der=0)

  # Paths
  ind_slash = prof_file.rfind('/')
  if ind_slash > 0:
    main_path = prof_file[:ind_slash+1]
    plot_path = main_path.replace('profiles', 'plots')
    prof_name = prof_file[ind_slash+1:]
  else:
    plot_path = 'plots/'
    prof_name = prof_file[:]

  #--------------------------------
  # Prepare the plot and fix the number
  # of panels depending on columns in
  # the profile file
  #--------------------------------
  fig = plt.figure(figsize=(8,6))
  if flag_dkap_dlnrho and flag_dkap_dlnT:
    n_cols = 2; n_rows = 2
  else:
    n_cols = 1; n_rows = 2
  plt.subplots_adjust(left=0.07, right=0.99, bottom=0.09, top=0.985, hspace=0.04, wspace=0.20)
  list_ls = ['solid', 'dotted', 'dashed', 'dashdot']
  ls = itertools.cycle(list_ls)
  cmap = plt.get_cmap('brg')
  norm = matplotlib.colors.Normalize(vmin=0, vmax=n_prof)
  # color = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
  color = itertools.cycle(['black', 'blue', 'red', 'grey', 'green', 'purple', 'cyan', 'pink'])

  #--------------------------------
  # Iterate over profiles
  #--------------------------------
  for i in range(n_prof):
    file = list_dic_prof[i]['filename']
    prof = list_dic_prof[i]['prof']

    if flag_kap and not flag_log_kap: kappa = prof['opacity']
    if not flag_kap and flag_log_kap: kappa = np.power(10.0, prof['log_opacity'])
    if flag_kap and flag_log_kap:     kappa = prof['opacity']
    if not flag_kap and not flag_log_kap:
      message = 'Error: plot_profile: plot_compare_kappa: both opacity and log_opacity are missing in %s' % (file, )
      raise SystemExit, message
    if flag_dkap_dlnrho: dkap_dlnrho = prof['dkap_dlnrho_at_face']
    if flag_dkap_dlnT:   dkap_dlnT   = prof['dkap_dlnt_at_face']

    nline = len(prof['zone'])
    logT = prof['logT']
    mass = prof['mass']
    if by_R: radius = np.power(10., prof['logR'])
    if by_logT:
      xaxis = logT
      xlim = (max(logT)+0.05, min(logT)-0.05)
      xtitle = r'Temperature $\log T$ [K]'
    if by_mass:
      xaxis = mass
      xlim = (0.0, mass[0])
      xtitle = r'Star Mass [M$_\odot$]'
    if by_R:
      xaxis = radius
      xlim = (0.0, radius[0])
      xtitle = r'Star Radius [R$_\odot$]'

    linestyle = ls.next()
    # clr = color.to_rgba(i)
    clr = color.next()

    ax0 = fig.add_subplot(n_cols, n_rows, 1)
    ax0.plot(xaxis, kappa, linestyle=linestyle, lw=1.5, color=clr, label=r''+list_lbls[i]+r'')
    ax0.set_xticklabels(())
    ax0.set_xlim(*xlim)
    ax0.set_ylabel(r'Opacity $\kappa$ [cm$^2$ gr$^{-1}$]')
    ax0.set_ylim(-0.1, 21)
    ax0.annotate('(a)', xy=(0.90, 0.05), xycoords='axes fraction', fontsize='medium')
    leg0 = ax0.legend(loc=2, fontsize='medium')

    # interpolate the reference into a grid of size n_ref
    delta_kappa = np.ones(n_ref)
    xaxis_interp = np.linspace(xaxis[0], xaxis[nline-1], n_ref)
    tck = interpolate.splrep(xaxis, kappa, s=0)
    kappa_interp = interpolate.splev(xaxis_interp, tck, der=0)
    delta_kappa[:] = (kappa_interp[:] - kappa_ref_interp[:]) / kappa_interp[:]
    ax1 = fig.add_subplot(n_cols, n_rows, 3)
    ax1.plot(xaxis_interp, delta_kappa*100, linestyle=linestyle, lw=1.5, color=clr, label=list_lbls[i])
    ax1.set_xlabel(xtitle)
    ax1.set_xlim(*xlim)
    ax1.set_ylim(-9, 45)
    ax1.set_ylabel(r'$\delta\kappa=(\kappa_i - \kappa_{\rm Ref})/\kappa_i$ [$\%$]')
    ax1.annotate('(b)', xy=(0.90, 0.92), xycoords='axes fraction', fontsize='medium')
    ax1.annotate(r'Ref: '+list_lbls[ref_indx], xy=(0.04,0.90), fontsize='x-small', xycoords='axes fraction')
    #leg1 = ax1.legend(loc=0, fontsize='x-small')

    #print 'i=%s, file=%s, Max(kappa)=%s' % (i, file, max(delta_kappa*100.))

    if flag_dkap_dlnrho and flag_dkap_dlnT:
      #ax2 = axes[2]
      ax2 = fig.add_subplot(n_cols, n_rows, 2)
      ax2.plot(xaxis, dkap_dlnrho, linestyle=linestyle, lw=1.5, color=clr, label=r''+list_lbls[i]+r'')
      ax2.set_xticklabels(())
      ax2.set_xlim(*xlim)
      ax2.set_ylabel(r'$d\ln \kappa/d\ln\rho$')
      ax2.set_ylim(-0.1, 20.0)
      ax2.annotate('(c)', xy=(0.90, 0.05), xycoords='axes fraction', fontsize='medium')
      #leg2 = ax2.legend(loc=0, fontsize='x-small')

      #ax3 = axes[3]
      ax3 = fig.add_subplot(n_cols, n_rows, 4)
      ax3.plot(xaxis, dkap_dlnT, linestyle=linestyle, lw=1.5, color=clr, label=r''+list_lbls[i]+r'')
      ax3.set_xlabel(xtitle)
      ax3.set_xlim(*xlim)
      ax3.set_ylim(-45, 1)
      ax3.set_ylabel(r'$d\ln\kappa/d\ln T$')
      ax3.annotate('(d)', xy=(0.90, 0.05), xycoords='axes fraction', fontsize='medium')
      #leg3 = ax3.legend(loc=0, fontsize='x-small')

  #--------------------------------
  # Add Annotations
  #--------------------------------
  txt_mass = r'M$_{\rm ini}=$' + "{:4.1f}".format(ref_header['initial_mass'][0]) + r' M$_\odot$'
  txt_z    = r'Z$_{\rm ini}=$' + "{:05.3f}".format(ref_header['initial_z'][0]) + r''
  txt_Xc   = r'X$_{\rm c}=$'   + "{:06.4f}".format(ref_header['center_h1'][0])
  txt_Yc   = r'Y$_{\rm c}=$'   + "{:06.4f}".format(ref_header['center_he4'][0])
  txt_Teff = r'T$_{\rm eff}=$' + str(int(ref_header['Teff'][0])) + r' [K]'
  txt_logL = r'$\log (L/L_\odot)=$' + "{:4.2f}".format(np.log10(ref_header['photosphere_L'][0]))
  #txt_left = [txt_mass, txt_Xc, txt_Teff]
  #txt_right = [txt_z, txt_Yc, txt_logL]
  txt = [txt_mass, txt_Xc, txt_Teff, txt_z, txt_Yc, txt_logL]
  txt_top = 0.90; txt_shift = 0.08
  for i in range(6):
    #ax2.annotate(txt_left[i],  xy=(0.04, txt_top-i*txt_shift), xycoords='axes fraction', fontsize='x-small')
    #ax2.annotate(txt_right[i], xy=(0.32, txt_top-i*txt_shift), xycoords='axes fraction', fontsize='x-small')
    ax2.annotate(txt[i],  xy=(0.04, txt_top-i*txt_shift), xycoords='axes fraction', fontsize='small')

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  if file_out:
    pdf_full_path = file_out
  else:
    pdf_name  = 'Comp-Kappa.pdf'
    pdf_full_path = plot_path + pdf_name
    if not os.path.exists(plot_path): os.makedirs(plot_path)

  plt.savefig(pdf_full_path)
  plt.close()

  print ' - plot_compare_kappa: Created %s' % (pdf_full_path, )

  return None

#=================================================================================================================
def plot_hydr_equil(dic_prof, Euler=False, Lagr=True, file_out=None):
  """
  This function receives a dictionary with at least three keys: 1. 'filename', 2. 'header' and 3. 'prof'.
  Then it generates a multi-panel plot with the run of many quantities.
  @param dic_prof: dictionary with profile data. the value associated to the 'prof' key is a record array with the
      column names from the MESA profile file.
  @type dic_prof: dictionary
  @param Lagr, Euler: to check the hydrostatic equilibrium in Lagrangian vs. Eulerian coordinate
  @type Lagr, Euler: boolean
  @param file_out: default=None, the full path to the output pdf plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  if file_out:
    pdf_full_path = file_out
  else:
    plot_path = 'plots/'
    if not os.path.exists(plot_path): os.makedirs(plot_path)
    pdf_full_path = plot_path + 'HydrEq.pdf'

  if not dic_prof.has_key('header') or not dic_prof.has_key('prof'):
    message = 'Error: plot_profile: plot_hydr_equil: "header" or "prof" key is missing: ' + dic_prof.keys()
    raise SystemExit, message

  prof_file = dic_prof['filename']
  header = dic_prof['header']
  prof   = dic_prof['prof']

  prof_names = prof.dtype.names
  dic_avail_names = {}
  for name in prof_names: dic_avail_names[name] = 0.0
  requested_keys = ['logT', 'temperature', 'logdq', 'dq', 'mass', 'brunt_N2', 'lamb_S2', 'gradr', 'grada', 'gradL', 'gradT',
                    'gradT_sub_grada', 'log_D_conv', 'log_D_ovr', 'log_D_semi', 'log_D_mix', 'log_D_mix_non_rotation',
                    'omega', 'shear', 'i_rot', 'j_rot', 'v_rot', 'h1', 'he4', 'c12', 'n14', 'o16']
  dic_keys = read.check_key_exists(dic_avail_names, requested_keys)

  if dic_keys['temperature'] and not dic_keys['logT']:
    logT = np.log10(prof['temperature'])
    print 'using temperature'
  if dic_keys['logT'] and not dic_keys['temperature']:
    logT = prof['logT']
    print 'using logT'
  if dic_keys['logT'] and dic_keys['temperature']:
    logT = np.log10(prof['logT'])
    print 'using logT'
  if not dic_keys['logT'] and not dic_keys['temperature']:
    message = 'Error: plot_profile: plot_hydr_equil: Neither logT nor temperature are provided in input profile'
    raise SystemExit, message

  nlines  = len(logT)
  if dic_keys['logdq']:
    logdq   = prof['logdq']
    dq      = np.power(10.0, logdq)
  if dic_keys['dq']: dq = prof['dq']
  mass    = prof['mass']
  if dic_keys['brunt_N2']:
    N2 = prof['brunt_N2']
    N2_indx = np.where((N2 <= 0.0))
    N2[N2_indx] = 1e-20
  else:
    print 'Warning: plot_profile: plot_hydr_equil: "brunt_N2" not in profile data'
    N2 = np.zeros(nlines)
  if dic_keys['lamb_S2']:
    lamb_Sl2= prof['lamb_S2']
    lamb_Sl1= lamb_Sl2/np.sqrt(3)
  else:
    lamb_Sl1 = lamb_S2 = np.zeros(nlines)

  if dic_keys['gradr']: gradr   = prof['gradr']
  else: gradr = np.zeros(nlines)
  if dic_keys['grada']: grada   = prof['grada']
  else: grada = np.zeros(nlines)
  if dic_keys['gradL']: gradL   = prof['gradL']
  else: gradL = np.zeros(nlines)
  if dic_keys['gradT']: gradT   = prof['gradT']
  else: gradT = np.zeros(nlines)
  if dic_keys['gradT_sub_grada']: gradT_sub_grada = prof['gradT_sub_grada']
  else: gradT_sub_grada = np.zeros(nlines)

  if dic_keys['log_D_conv']: log_D_conv = prof['log_D_conv']
  else: log_D_conv = np.zeros(nlines)
  if dic_keys['log_D_ovr']: log_D_ovr  = prof['log_D_ovr']
  else: log_D_ovr = np.zeros(nlines)
  if dic_keys['log_D_semi']: log_D_semi = prof['log_D_semi']
  else: log_D_semi = np.zeros(nlines)
  if dic_keys['log_D_mix']: log_D_mix  = prof['log_D_mix']
  else: log_D_mix = np.zeros(nlines)
  if dic_keys['log_D_mix_non_rotation']: log_D_mix_non_rot = prof['log_D_mix_non_rotation']
  else: log_D_mix_non_rot = np.zeros(nlines)
  D_mix = np.power(10.0, log_D_mix)
  D_mix_non_rot = np.power(10.0, log_D_mix_non_rot)
  D_mix_rot = D_mix - D_mix_non_rot
  log_D_rot = np.log10(D_mix_rot)

  if dic_keys['omega']: omega      = prof['omega']
  else: omega = np.zeros(nlines)
  if dic_keys['shear']: shear      = prof['shear']
  else: shear = np.zeros(nlines)
  if dic_keys['i_rot']: i_rot      = prof['i_rot']
  else: i_rot = np.zeros(nlines)
  if dic_keys['j_rot']: j_rot      = prof['j_rot']
  else: j_rot = np.zeros(nlines)
  if dic_keys['v_rot']: v_rot      = prof['v_rot']
  else: v_rot = np.zeros(nlines)

  if dic_keys['h1']: h1  = prof['h1']
  else: h1 = np.zeros(nlines)
  if dic_keys['he4']: he4 = prof['he4']
  else: he4 = np.zeros(nlines)
  if dic_keys['c12']: c12 = prof['c12']
  else: c12 = np.zeros(nlines)
  if dic_keys['n14']: n14 = prof['n14']
  else: n14 = np.zeros(nlines)
  if dic_keys['o16']: o16 = prof['o16']
  else: o16 = np.zeros(nlines)

  hydr_depart, lhs, rhs = check_hydr_equil(prof, Euler=Euler, Lagr=Lagr)

  fig = plt.figure()
  plt.subplots_adjust(left=0.07, right=0.99, bottom=0.08, top=0.99, wspace=0.18, hspace=0.05)
  min_T = min(logT) - 0.1
  max_T = max(logT) + 0.1

  ax1 = fig.add_subplot(321)
  ax1.plot(logT, np.log10(N2), '-', color='black', label=r'$\log N^2$')
  ax1.plot(logT, np.log10(lamb_Sl1), '-', color='blue', label=r'$\log S_{\ell=1}^2$')
  if dic_keys['lamb_S2']: ax1.plot(logT, np.log10(lamb_Sl2), '--', color='blue', label=r'$\log S_{\ell=2}^2$')
  ax1.set_xlim(max_T, min_T)
  ax1.set_xticklabels(())
  ax1.set_ylabel(r'$\log N^2$', fontsize='x-small')
  ax1.set_ylim(-10, 0)
  leg1 = ax1.legend(loc='upper right', fontsize='x-small')
  leg1.get_frame().set_alpha(0.75)

  ax2 = fig.add_subplot(322)
  ax2.plot(logT, omega*1e6, '-', color='black', label=r'$\Omega(r)$')
  #ax2.plot(logT, shear, '-', color='blue', label=r'Shear')
  #ax2.plot(logT, np.log10(i_rot), '-', color='red', label=r'$i_{\rm rot}$')
  #ax2.plot(logT, np.log10(j_rot), '-', color='cyan', label=r'$j_{\rm rot}$')
  #ax2.plot(logT, v_rot, '-', color='green', label=r'$v_{\rm rot}$')
  ax2.set_xlim(max_T, min_T)
  ax2.set_ylim(min(omega*1e6)*0.95, max(omega*1e6)*1.01)
  ax2.set_xticklabels(())
  ax2.set_ylabel(r'$\Omega(r)$ [$\mu$Hz]', fontsize='small')
  try:
    dic_params = param_tools.get_param_from_single_prof_filename(prof_file)
    txt_m = r'$M_{\rm ini}$=' + "{:04.1f}".format(dic_params['m_ini']) + r' [$M_\odot$]'
    txt_eta = r'$\eta_{\rm rot}$=' + "{:04.2f}".format(dic_params['eta'])
    txt_ov  = r'$f_{\rm ov}$=' + "{:05.3f}".format(dic_params['ov'])
    txt_sc = r'$\alpha_{\rm sc}$=' + "{:04.2f}".format(dic_params['sc'])
    txt_z  = r'$Z$=' + "{:05.3f}".format(dic_params['z'])
    txt_Yc = r'$Y_{\rm c}$=' + "{:06.4f}".format(dic_params['Yc'])
    txt_Xc = r'$X_{\rm c}$=' + "{:06.4f}".format(dic_params['Xc'])
    txt_mn = r'Model Num. ' + r'' + str(dic_params['model_number']).zfill(5) + r''
    for n_txt, txt in enumerate([txt_m, txt_ov, txt_z, txt_Xc, txt_mn]):
      ax2.annotate(txt, xy=(0.50, 0.85-n_txt*0.12), xycoords='axes fraction', fontsize = 'x-small', color='Purple')
    for n_txt, txt in enumerate([txt_eta, txt_sc, txt_Yc]):
      ax2.annotate(txt, xy=(0.78, 0.85-n_txt*0.12), xycoords='axes fraction', fontsize = 'x-small', color='Purple')
  except ValueError:
    print '  - Warning: plot_profile: plot_hydr_equil: get_param_from_single_prof_filename failed!'
    print '    Skip writing information on the plot.'

  ax3 = fig.add_subplot(323)
  ax3.plot(logT, grada, '-', color='black', alpha=0.6, label=r'$\nabla_{\rm ad}$')
  ax3.plot(logT, gradr, '-', color='red', alpha=0.6, label=r'$\nabla_{\rm rad}$')
  ax3.plot(logT, gradT, '-', color='blue', alpha=0.6, label=r'$\nabla_{\rm T}$')
  ax3.plot(logT, gradL, '-.', color='green', alpha=0.6, label=r'$\nabla_{\rm Led}$')
  ax3.set_xlim(max_T, min_T)
  ax3.set_xticklabels(())
  ax3.set_ylim(0.15, 0.6)
  ax3.set_ylabel(r'Temperature Gradients', fontsize='small')
  leg3 = ax3.legend(loc=9, fontsize='x-small')
  leg3.get_frame().set_alpha(0.75)

  ax4 = fig.add_subplot(324)
  ax4.plot(logT, np.log10(h1), '-', color='black', label=r'$^1$H')
  ax4.plot(logT, np.log10(he4), '-', color='blue', label=r'$^4$He')
  ax4.plot(logT, np.log10(c12), '-', color='red', label=r'$^{12}$C')
  ax4.plot(logT, np.log10(n14), '-', color='green', label=r'$^{14}$N')
  ax4.plot(logT, np.log10(o16), '-', color='pink', label=r'$^{16}$O')
  ax4.set_xlim(max_T, min_T)
  ax4.set_xticklabels(())
  ax4.set_ylim(-5, 0.5)
  ax4.set_ylabel(r'Log Abundance Profiles', fontsize='small')
  leg4 = ax4.legend(loc='lower right', fontsize='x-small')
  leg4.get_frame().set_alpha(0.75)

  ax5 = fig.add_subplot(325)
  ax5.plot(logT, log_D_conv, '-', color='black', label=r'$\log D_{\rm conv}$')
  ax5.plot(logT, log_D_rot, '-', color='red', label=r'$\log D_{\rm rot}$')
  ax5.plot(logT, log_D_ovr, '-', color='blue', label=r'$\log D_{\rm over}$')
  ax5.plot(logT, log_D_semi, '-', color='green', label=r'$\log D_{\rm semi}$')
  ax5.set_xlim(max_T, min_T)
  ax5.set_xlabel(r'Temperature $\log T$ [K]')
  ax5.set_ylim(0, 20)
  ax5.set_ylabel(r'Mixing Diffusion Coeff.', fontsize='small')
  leg5 = ax5.legend(loc=0, fontsize='x-small')
  leg5.get_frame().set_alpha(0.75)

  ax6 = fig.add_subplot(326)
  ax6.plot(logT, np.log10(hydr_depart), '-', color='black', label=r'$\log Q$')
  #print min(lhs), max(lhs), min(rhs), max(rhs)
  #print lhs[500], rhs[500], lhs[500]/rhs[500]
  ax6.plot(logT, np.log10(np.abs(lhs)), lw=1, color='blue', label=r'$\log$(LHS)')
  ax6.plot(logT, np.log10(np.abs(rhs)), lw=1, color='red', label=r'$log$(RHS)')
  ax6.set_xlabel(r'Temperature $\log T$ [K]')
  ax6.set_xlim(max_T, min_T)
  max_dev = max(np.log10(hydr_depart))
  min_dev = min(np.log10(hydr_depart))
  print 'dev: ', min_dev, max_dev
  ax6.set_ylim(-20, 1)
  ax6.set_ylabel(r'Hydr. Equl. $\log Q$')
  txt = r'$Q=|1.0 - \frac{\rm LHS}{\rm RHS}|$'
  ax6.annotate(txt, xy=(0.20, 0.12), xycoords='axes fraction')
  leg6 = ax6.legend(loc=0, fontsize='x-small')

  plt.savefig(pdf_full_path)
  plt.close()

  print ' - plot_hydr_equil: Created %s' % (pdf_full_path, )

  return None


#=================================================================================================================
def check_hydr_equil(recarr, Lagr=True, Euler=False):
  """
  This function receives a record array containing the profile of various quantities as read by read.read_mesa.
  Then, it fetches dq, and P, and evaluates how the two sides of the equation of hydrostatic equilibrium do equate.
  For more info, see http://www.ster.kuleuven.be/~pieterd/mesa/run_star_extras.html
  @param recarr: record array containing the profile. It must have either dq or logdq and P in it.
  @type recarr: numpy record array
  @return: gives an array from surface to center of the model containing
           q = (LHS - RHS)/Max(LHS, RHS)
  @rtype: numpy 1-D array
  """
  if Lagr and Euler or (not Lagr and not Euler):
    message = 'plot_profile: check_hydr_equil: Both flags cannot be similar; Lagr:%s and Euler:%s' % (Lagr, Euler)
    raise SystemExit, message

  names = recarr.dtype.names
  dic_avail_names = {}
  for ind, key in enumerate(names):
    dic_avail_names[key] = 0.0

  dic_keys = read.check_key_exists(dic_avail_names, ['dq', 'logdq', 'logP', 'pressure', 'mass', 'logR', 'radius',
                                                     'gravity', 'log_g', 'logRho', 'density'])
  flag_dq = dic_keys['dq']
  flag_logdq = dic_keys['logdq']
  flag_logP = dic_keys['logP']
  flag_pressure = dic_keys['pressure']
  flag_mass     = dic_keys['mass']
  flag_radius   = dic_keys['radius']
  flag_logR     = dic_keys['logR']
  flag_gravity  = dic_keys['gravity']
  flag_log_g    = dic_keys['log_g']
  flag_logRho   = dic_keys['logRho']
  flag_density  = dic_keys['density']

  if not flag_mass:
    message = 'Error: plot_profile: check_hydr_equil: the mass profile is missing.'
    raise SystemExit, message

  if flag_dq and not flag_logdq: dq = recarr['dq']
  elif not flag_dq and flag_logdq: dq = np.power(10., recarr['logdq'])
  elif flag_dq and flag_logdq: dq = recarr['dq']
  elif not flag_dq and not flag_logdq:
    message = 'Error: plot_profile: check_hydr_equil: both dq and logdq flags are False!'
    raise SystemExit, message
  else:
    message = 'Error: plot_profile: check_hydr_equil: there is something wrong: 1'
    raise SystemExit, message

  if flag_pressure and not flag_logP:
    P = recarr['pressure']
    print 'using pressure 1'
  elif not flag_pressure and flag_logP:
    P = np.power(10., recarr['logP'])
    print 'using logP 1'
  elif flag_pressure and flag_logP:
    #P = recarr['pressure']
    #print 'using pressure 2'
    P = np.power(10.0, recarr['logP'])
    print 'using logP 2'
  elif not flag_pressure and not flag_logP:
    message = 'Error: plot_profile: check_hydr_equil: both pressure and logP flags are False!'
    raise SystemExit, message
  else:
    message = 'Error: plot_profile: check_hydr_equil: there is something wrong: 2'
    raise SystemExit, message

  if flag_radius and not flag_logR: radius = recarr['radius']
  elif not flag_radius and flag_logR: radius = np.power(10., recarr['logR'])
  elif flag_radius and flag_logR: radius = recarr['radius']
  elif not flag_radius and not flag_logR:
    message = 'Error: plot_profile: check_hydr_equil: both radius and logR flags are False!'
    raise SystemExit, message
  else:
    message = 'Error: plot_profile: check_hydr_equil: there is something wrong: 3'
    raise SystemExit, message

  if flag_gravity and not flag_log_g: gravity = recarr['gravity']
  elif not flag_gravity and flag_log_g: gravity = np.power(10., recarr['log_g'])
  elif flag_gravity and flag_log_g: gravity = recarr['gravity']
  elif not flag_gravity and not flag_log_g and Euler:
    message = 'Error: plot_profile: check_hydr_equil: both gravity and log_g flags are False!'
    raise SystemExit, message
  #else:
    #message = 'Error: plot_profile: check_hydr_equil: there is something wrong: 4'
    #raise SystemExit, message

  if flag_logRho and not flag_density:
    rho = np.power(10.0, recarr['logRho'])
    print 'using logRho 1'
  if flag_density and not flag_logRho:
    rho = recarr['density']
    print 'using density 1'
  if flag_density and flag_logRho:
    #rho = recarr['density']
    #print 'using density 2'
    rho = np.power(10.0, recarr['logRho'])
    print 'using logRho 2'
  if not flag_density and not flag_logRho:
    message = 'Error: plot_profile: check_hydr_equil: Neither logRho nor density are provided in input profile data'
    raise SystemExit, message

  len_prof = len(dq)
  mass = recarr['mass']      # in solar mass
  m_total = mass[0]          # in solar mass
  m_total_gr = m_total * Msun # in gr
  dm = np.zeros(len_prof)
  mass_gr = np.zeros(len_prof)
  r_cm = np.zeros(len_prof)

  for i in range(len_prof):
    dm[i] = m_total_gr * dq[i]       # gr
    mass_gr[i] = mass[i] * Msun
    r_cm[i] = radius[i] * Rsun

  dr = np.zeros(len_prof)    # cm
  for i in range(1, len_prof):
    dr[i] = r_cm[i-1] - r_cm[i]

  P_face = np.zeros(len_prof)
  P_face[0] = P[0]
  rho_face = np.zeros(len_prof)
  rho_face[0] = rho[0]
  alpha  = 0.0
  for i in range(1, len_prof):
    alpha = dq[i-1] / (dq[i-1]+dq[i])
    P_face[i]   = alpha*P[i] + (1.0 - alpha)*P[i-1]
    rho_face[i] = alpha*rho[i] + (1.0 - alpha)*rho[i-1]

  alpha = 0.0
  for i in range(1, len_prof):
    alpha = dq

  rhs_arr = []
  lhs_arr = []
  for i in range(1, len_prof):
    if Lagr:
      lhs = (P[i-1] - P[i]) / ((dm[i-1] + dm[i]) / 2.0)
      rhs = -c_grav * mass_gr[i] / (4.0 * np.pi * r_cm[i]**4.0)
    if Euler:
      lhs = (P_face[i-1] - P_face[i]) / ((dr[i-1] + dr[i]) / 2.0)
      rhs = -rho_face[i] * gravity[i]

    lhs_arr.append(lhs)
    rhs_arr.append(rhs)

  lhs_arr.insert(-1, lhs_arr[-1])
  rhs_arr.insert(-1, lhs_arr[-1])

  q = []
  for i in range(len_prof):
    #denom = max( [ np.abs(lhs_arr[i]), np.abs(rhs_arr[i]) ] )
    #q.append( (np.abs( lhs_arr[i] - rhs_arr[i] ) / denom) )
    #q.append(np.abs(lhs_arr[i]-rhs_arr[i])/P[i])
    q.append(np.abs(1.0 - lhs_arr[i] / rhs_arr[i]) )
  departure = np.array(q)

  if Lagr: txt = 'Lagrangian'
  else: txt = 'Eulerian'
  print ' - plot_profile: check_Hydr_Equil: ' + txt + ' frame'
  print '   Median of departure = %s' % (np.median(departure), )
  print '   Min = %s, Max= %s' % (min(np.abs(departure)), max(departure))

  return departure, np.array(lhs_arr), np.array(rhs_arr)

#=================================================================================================================
def local_adiabaticity(list_prof, xaxis='mass', list_lbls=None, xaxis_from=None, xaxis_to=None,
                       grad_from=None, grad_to=None,
                       R_from=None, R_to=None, L_from=None, L_to=None, T_from=None, T_to=None, P_form=None, P_to=None,
                       Rho_from=None, Rho_to=None, xlabel='', leg_loc=4, file_out=None):
  """
  By locally modifying the temperature gradient gradT in the input models, the profiles of pressure,
  temperature, and hence density will be affected. Since this is done consistently in MESA, the changes depend
  on the adiabaticity parameter used, e.g. 0<= eta <1.
  This routine creates a 4-panel plot showing nablaT, temperature, pressure and density profiles inside the
  model whitin the axis and axis ranges specified
  @param list_prof: list of profiels
  @type list_prof: list of dictionaries
  @param xaxis: assign the quantity which is used for plotting the x-axis. It can be e.g. radius, pressure, etc.
      If you choose 'mass', then the mass profile is calculated from q_div_xq column in the profiles.
  @type list_lbls: list of labels, one per each profile
  @type list_lbls: list of strings
  @param xaxis_from, xaxis_to: the rage of x-axsis. For the ranges of y-axis for each panel, do the modifications
      manually
  @type xaxis_from, xaxis_to: string
  @param file_out: full path to the output plot.
  @type file_out: string
  @return: matplotlib figure object
  @rtype: figure class
  """
  n_prof = len(list_prof)
  if n_prof == 0:
    logging.error('plot_gyre: local_adiabaticity: input list is empty')
    raise SystemExit

  #---------------------------
  # Create the Figure and Plot
  #---------------------------
  import itertools
  fig = plt.figure(figsize=(9, 6), dpi=300)
  ax1 = fig.add_subplot(241)
  ax2 = fig.add_subplot(242)
  ax3 = fig.add_subplot(243)
  ax4 = fig.add_subplot(244)
  ax5 = fig.add_subplot(245)
  ax6 = fig.add_subplot(246)
  ax7 = fig.add_subplot(247)
  ax8 = fig.add_subplot(248)
  plt.subplots_adjust(left=0.08, right=0.99, bottom=0.10, top=0.97, hspace=0.05, wspace=0.35)
  colors = itertools.cycle(['black', 'blue', 'red', 'green', 'cyan', 'pink', 'orange', 'purple'])
  linestyles = itertools.cycle(['solid', 'dashed', 'dashdot', 'dotted'])

  #---------------------------
  # Loop over dictionaries and Plot x vs y
  #---------------------------
  list_x_min = []; list_x_max = []
  list_y_min = []; list_y_max = []
  for i_dic, dic in enumerate(list_prof):
    header = dic['header']
    prof   = dic['prof']

    columns = prof.dtype.names
    if xaxis not in columns:
      logging.error('plot_gyre: gyre_in_columns: {0} not in columns'.format(xaxis))
      raise SystemExit

    xvals  = prof[xaxis]
    gradT  = prof['gradT']
    gradr  = prof['gradr']
    grada  = prof['grada']
    mxt    = prof['mixing_type']
    if 'radius' in columns:      radius      = prof['radius']
    if 'logR' in columns:        radius      = np.power(10.0, prof['logR'])
    if 'temperature' in columns: temperature = prof['temperature'] / 1e6
    if 'logT' in columns:        temperature = np.power(10.0, prof['logT']) / 1e6
    if 'pressure' in columns:    pressure    = prof['pressure'] / 1e15
    if 'logP' in columns:        pressure    = np.power(10.0, prof['logP']) / 1e15
    if 'density' in columns:     density     = prof['density']
    if 'logRho' in columns:      density     = np.power(10.0, prof['logRho'])
    # if 'luminosity' in columns:  logL        = np.log10(prof['luminosity'])
    # if 'logL' in columns:        logL        = prof['logL']
    if 'luminosity' in columns:  luminosity  = prof['luminosity'] / 1e3
    if 'logL' in columns:        luminosity  = np.power(10.0, prof['logL']) / 1e3

    clr = colors.next()
    ls  = linestyles.next()
    if list_lbls:
      lbl = list_lbls[i_dic]
    else:
      lbl = ''

    ax1.plot(xvals, gradT, color=clr, linestyle=ls, lw=1.5, zorder=2, label=lbl)
    ax2.plot(xvals, grada, color=clr, linestyle=ls, lw=1.5, zorder=2, label=lbl)
    ax3.plot(xvals, gradr, color=clr, linestyle=ls, lw=1.5, zorder=2, label=lbl)
    ax4.plot(xvals, radius, color=clr, linestyle=ls, lw=1.5, zorder=2, label=lbl)
    ax5.plot(xvals, luminosity, color=clr, linestyle=ls, lw=1.5, zorder=2, label=lbl)
    ax6.plot(xvals, temperature, color=clr, linestyle=ls, lw=1.5, zorder=2)
    ax7.plot(xvals, pressure, color=clr, linestyle=ls, lw=1.5, zorder=2)
    ax8.plot(xvals, density, color=clr, linestyle=ls, lw=1.5, zorder=2)

  ax1.fill_between(xvals, y1=0, y2=100, where=(mxt==1), color='0.50', alpha=0.50, zorder=1)
  ax2.fill_between(xvals, y1=0, y2=100, where=(mxt==1), color='0.50', alpha=0.50, zorder=1)
  ax3.fill_between(xvals, y1=0, y2=100, where=(mxt==1), color='0.50', alpha=0.50, zorder=1)
  ax4.fill_between(xvals, y1=0, y2=1e4, where=(mxt==1), color='0.50', alpha=0.50, zorder=1)
  ax5.fill_between(xvals, y1=0, y2=1e8, where=(mxt==1), color='0.50', alpha=0.50, zorder=1)
  ax6.fill_between(xvals, y1=0, y2=100, where=(mxt==1), color='0.50', alpha=0.50, zorder=1)
  ax7.fill_between(xvals, y1=0, y2=100, where=(mxt==1), color='0.50', alpha=0.50, zorder=1)
  ax8.fill_between(xvals, y1=0, y2=100, where=(mxt==1), color='0.50', alpha=0.50, zorder=1)
  ax1.fill_between(xvals, y1=0, y2=100, where=(mxt==2), color='0.50', alpha=0.25, zorder=1)
  ax2.fill_between(xvals, y1=0, y2=100, where=(mxt==2), color='0.50', alpha=0.25, zorder=1)
  ax3.fill_between(xvals, y1=0, y2=100, where=(mxt==2), color='0.50', alpha=0.25, zorder=1)
  ax4.fill_between(xvals, y1=0, y2=100, where=(mxt==2), color='0.50', alpha=0.25, zorder=1)
  ax5.fill_between(xvals, y1=0, y2=100, where=(mxt==2), color='0.50', alpha=0.25, zorder=1)
  ax6.fill_between(xvals, y1=0, y2=100, where=(mxt==2), color='0.50', alpha=0.25, zorder=1)
  ax7.fill_between(xvals, y1=0, y2=100, where=(mxt==2), color='0.50', alpha=0.25, zorder=1)
  ax8.fill_between(xvals, y1=0, y2=100, where=(mxt==2), color='0.50', alpha=0.25, zorder=1)

  #---------------------------
  # Ranges, Annotations, Lengend
  #---------------------------
  if not xaxis_from: xaxis_from = np.min(xvals)
  if not xaxis_to:   xaxis_to   = np.max(xvals)
  ax1.set_xlim(xaxis_from, xaxis_to)
  ax2.set_xlim(xaxis_from, xaxis_to)
  ax3.set_xlim(xaxis_from, xaxis_to)
  ax4.set_xlim(xaxis_from, xaxis_to)
  ax5.set_xlim(xaxis_from, xaxis_to)
  ax6.set_xlim(xaxis_from, xaxis_to)
  ax7.set_xlim(xaxis_from, xaxis_to)
  ax8.set_xlim(xaxis_from, xaxis_to)
  ax1.set_xticks( np.arange(xaxis_from, xaxis_to+1, 1.0) )
  ax2.set_xticks( np.arange(xaxis_from, xaxis_to+1, 1.0) )
  ax3.set_xticks( np.arange(xaxis_from, xaxis_to+1, 1.0) )
  ax4.set_xticks( np.arange(xaxis_from, xaxis_to+1, 1.0) )
  ax5.set_xticks( np.arange(xaxis_from, xaxis_to+1, 1.0) )
  ax6.set_xticks( np.arange(xaxis_from, xaxis_to+1, 1.0) )
  ax7.set_xticks( np.arange(xaxis_from, xaxis_to+1, 1.0) )
  ax8.set_xticks( np.arange(xaxis_from, xaxis_to+1, 1.0) )

  if not grad_from:  grad_from  = 0.18
  if not grad_to:    grad_to    = 0.45
  if not R_from:     R_from     = 0.00
  if not R_to:       R_to       = 5.00
  if not L_from:     L_from     = 0.00
  if not L_to:       L_to       = 100
  if not T_from:     T_from     = np.min(temperature)
  if not T_to:       T_to       = np.max(temperature)
  if not P_form:     P_form     = np.min(pressure)
  if not P_to:       P_to       = np.max(pressure)
  if not Rho_from:   Rho_from   = np.min(density)
  if not Rho_to:     Rho_to     = np.max(density)
  ax1.set_ylim(grad_from, grad_to)
  ax2.set_ylim(grad_from, grad_to)
  ax3.set_ylim(grad_from, grad_to)
  ax4.set_ylim(R_from, R_to)
  ax5.set_ylim(L_from, L_to)
  ax6.set_ylim(T_from, T_to)
  ax7.set_ylim(P_form, P_to)
  ax8.set_ylim(Rho_from, Rho_to)

  ax1.set_xlabel(()) ; ax1.set_xticklabels(())
  ax2.set_xlabel(()) ; ax2.set_xticklabels(())
  ax3.set_xlabel(()) ; ax3.set_xticklabels(())
  ax4.set_xlabel(()) ; ax4.set_xticklabels(())
  ax5.set_xlabel(xlabel)
  ax6.set_xlabel(xlabel)
  ax7.set_xlabel(xlabel)
  ax8.set_xlabel(xlabel)
  ax1.set_ylabel(r'$\nabla_{\star}$')
  ax2.set_ylabel(r'$\nabla_{\rm ad}$', fontsize='small')
  ax3.set_ylabel(r'$\nabla_{\rm r}$')
  ax4.set_ylabel(r'Radius [R$_\odot$]')
  ax5.set_ylabel(r'Luminosity [$10^3\,$L$_\odot$]')
  ax6.set_ylabel(r'Temperature [$10^6$ K]')
  ax7.set_ylabel(r'Pressure [$10^{15}$g cm$^{-1}$ sec$^{-2}$]')
  ax8.set_ylabel(r'Density [g cm$^{-3}$]')

  plot_commons.add_roman_counter_to_fig_axes(fig, x=0.86, y=0.90, fontsize='small')

  if list_lbls: leg1 = ax1.legend(loc=leg_loc, fontsize='x-small')

  if file_out:
    plt.savefig(file_out, dpi=300)
    print ' - plot_profile: local_adiabaticity: {0} saved'.format(file_out)
    plt.close()


  return fig

#=================================================================================================================
def Rho_T(list_dic_prof, xaxis_from=-8, xaxis_to=10, yaxis_from=3, yaxis_to=10,
          list_dic_annot=None, path_mesa_dir='/Users/ehsan/programs/mesa-5548', file_out=None):
  """
  Create the logT versus logRho plot for a list in input models, similar to Figs. 1 - 3 etc
  in MESA first instrument paper.
  @param list_dic_prof: list of dictionaries containing profile information
  @type list_dic_prof
  @param list_lbls: list of labels per each dictionary in the input list_dic_prof. Mandatory!
  @type xaxis_from/xaxis_to/yaxis_from/yaxis_to: float
  @param leg_loc: location of the plot legend
  @type leg_loc: integer
  @param file_out: the full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  from copy import deepcopy

  n_prof = len(list_dic_prof)

  #--------------------------------
  # Prepare the plot and panels
  #--------------------------------
  import itertools
  fig = plt.figure(figsize=(5,5), dpi=200)
  ax  = fig.add_subplot(111)
  plt.subplots_adjust(left=0.11, right=0.97, bottom=0.11, top=0.97)
  ls = itertools.cycle(['solid', 'dashdot', 'dashed', 'dotted'])
  cmap = plt.get_cmap('brg')
  norm = matplotlib.colors.Normalize(vmin=0, vmax=n_prof)
  color = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)

  #--------------------------------
  # Loop over profiles, and Plot them!
  #--------------------------------
  list_min_x = []; list_max_x = []
  list_min_y = []; list_max_y = []
  for i_dic, dic in enumerate(list_dic_prof):
    header = dic['header']
    prof   = dic['prof']

    xvals  = prof['logRho']
    yvals  = prof['logT']
    nz     = len(xvals)
    nan_arr= np.array([np.nan for i in range(nz)])

    mxt    = prof['mixing_type']

    x_conv = deepcopy(xvals); y_conv = deepcopy(yvals)
    x_conv[mxt != 1] = np.nan
    y_conv[mxt != 1] = np.nan

    x_over = deepcopy(xvals); y_over = deepcopy(yvals)
    x_over[mxt != 2] = np.nan
    y_over[mxt != 2] = np.nan

    x_sc   = deepcopy(xvals); y_sc = deepcopy(yvals)
    x_sc[mxt != 3]   = np.nan
    y_sc[mxt != 3]   = np.nan

    eps_nuc= prof['eps_nuc']
    x_burn1 = deepcopy(xvals); y_burn1 = deepcopy(yvals)
    x_burn2 = deepcopy(xvals); y_burn2 = deepcopy(yvals)
    x_burn3 = deepcopy(xvals); y_burn3 = deepcopy(yvals)
    x_burn1[eps_nuc < 1000] = np.nan
    y_burn1[eps_nuc < 1000] = np.nan
    x_burn2[eps_nuc < 100] = np.nan
    y_burn2[eps_nuc < 100] = np.nan
    x_burn3[eps_nuc < 1] = np.nan
    y_burn3[eps_nuc < 1] = np.nan

    list_min_x.append(np.min(xvals))
    list_max_x.append(np.max(xvals))
    list_min_y.append(np.min(yvals))
    list_max_y.append(np.max(yvals))

    ax.plot(x_burn1, y_burn1, lw=5.5, color='red', zorder=2)
    ax.plot(x_burn2, y_burn2, lw=4.5, color='orange', zorder=2)
    ax.plot(x_burn3, y_burn3, lw=4.0, color='yellow', zorder=2)
    ax.plot(x_conv, y_conv, lw=3.5, color='blue', zorder=3)
    ax.plot(x_over, y_over, lw=3.5, color='green', zorder=3)
    ax.plot(x_sc, y_sc, lw=3.5, color='grey', zorder=3)
    ax.plot(xvals, yvals, linestyle='solid', color='black', lw=1.5, zorder=4)

  ax.plot([], [], lw=1.5, color='black', label='The Star')
  ax.plot([], [], lw=3.5, color='blue', label='Convective')
  ax.plot([], [], lw=3.5, color='green', label='Conv. Overshoot')
  ax.plot([], [], lw=3.5, color='grey', label='Semi-Convection')
  ax.plot([], [], lw=5.0, color='red', label=r'Burning $\epsilon_{\rm nuc}\geq1000$')
  ax.plot([], [], lw=4.5, color='orange', label=r'Burning $\epsilon_{\rm nuc}\geq100$')
  ax.plot([], [], lw=4.0, color='yellow', label=r'Burning $\epsilon_{\rm nuc}\geq1$')

  #--------------------------------
  # Add additional, optinal lines from
  # MESA data directory
  #--------------------------------
  if path_mesa_dir[-1] != '/': path_mesa_dir += '/'
  data_dir = path_mesa_dir + 'data/star_data/plot_info/'
  flag = os.path.exists(data_dir)
  if (not flag):
    print '   plot_profile: Rho_T: mesa data directory not found: {0}'.format(data_dir)
    print '                        Skip additional lines.'

  data_files = ['hydrogen_burn.data', 'helium_burn.data', 'carbon_burn.data', 'oxygen_burn.data',
                'gamma_4_thirds.data', 'kap_rad_cond_eq.data']
  data_names = [r'$^1$H Burn', r'$^4$He Burn', r'$^{12}$C Burn', r'$^{16}$O Burn',
                r'$\Gamma<4/3$', r'$e^-$ Degen.']
  data_pos   = [(-5.0, 7.3), (-2.45, 8.2), (6.0, 8.35), (7.5, 9.0),
                (-0.4, 9.5), (0.5, 3.5)]
  data_angle = [0, 0, -10, -10, 0, 0]
  for f in data_files:
    flag = os.path.exists(data_dir + f)
    if not flag: break

  if flag:
    for i, f in enumerate(data_files):
      fp = data_dir + f
      data = np.loadtxt(fp, dtype={'names':('logRho', 'logT'), 'formats':('f4', 'f4')})
      ax.plot(data['logRho'], data['logT'], linestyle='dashed', color='magenta', alpha=0.5+i*0.05, lw=1)
      ax.annotate(data_names[i], xy=data_pos[i], color='grey', rotation=data_angle[i], fontsize='small')

    print '   plot_profile: Rho_T: Additional burning/EOS lines added'

  #--------------------------------
  # Annotations and Legend
  #--------------------------------
  if xaxis_from is None: xaxis_from = min(list_min_x)
  if xaxis_to is None:   xaxis_to   = max(list_max_x)
  if yaxis_from is None: yaxis_from = min(list_min_y)
  if yaxis_to is None:   yaxis_to   = max(list_max_y)
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)
  ax.set_xlabel(r'Density $\log \rho$ [g cm$^{}$]')
  ax.set_ylabel(r'Temperature $\log T$ [K]')

  if list_dic_annot is not None: add_annotation(ax, list_dic_annot)
  leg = ax.legend(loc=4, fontsize='x-small')

  #--------------------------------
  # Finalize the Plot
  #--------------------------------
  if file_out:
    plt.savefig(file_out, dpi=200)
    print ' - plot_profile: Rho_T: stored {0}'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def ionization(dic_prof, xaxis='logT', xaxis_from=7.6, xaxis_to=4, file_out=None):
  """
  Use two sets of columns in MESA outputs that start with the names 
  - avg_charge_
  - neutral_fraction_
  and they are proceeded by the name of the following element species:
  H, He, C, N, O, Ne, Mg, Si, and Fe.
  """
  if file_out is None: return 
  header = dic_prof['header']
  prof   = dic_prof['prof']
  names  = prof.dtype.names
  list_names_charge = []
  list_names_neutr  = []
  for name in names:
    if 'avg_charge_' in name: list_names_charge.append(name)
    if 'neutral_fraction_' in name: list_names_charge.append(name)
  n_charge = len(list_names_charge)
  n_neutr  = len(list_names_neutr)




#=================================================================================================================
def actual_model_logR_OPAL(dic_prof, file_out=None):
  """
  OPAL Type I tables are expressed in logT versus logR, where logR is defined as 
        logR = logRho - 3 * logT + 18
  This routine takes a profile, and shows the change of logR across the model from center to core.
  This allows to understand the range of logR being used in the Type I tables.
  @param dic_prof: MESA profile information passed by e.g. read.read_multiple_mesa_files()
  @type dic_prof: dictionary
  @param file_out: full path to the output plot file.
  @type file_out: string 
  @return: None 
  @rtype: None
  """
  if file_out is None: return
  header = dic_prof['header']
  prof   = dic_prof['prof']
  mass   = prof['mass']
  logT   = prof['logT']
  logRho = prof['logRho']
  logR   = logRho - 3 * logT + 18

  #--------------------------------
  # Prepare the plot
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(6,4))
  plt.subplots_adjust(left=0.12, right=0.88, bottom=0.11, top=0.97)

  #--------------------------------
  # make the plot
  #--------------------------------
  ax.plot(mass, logR, linestyle='solid', color='black', lw=2, label=r'$\log R$')

  left = ax.twinx()
  left.plot(mass, logT, linestyle='dashed', color='blue', lw=2, label=r'$\log T$')

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  for tm in left.get_yticklabels(): tm.set_color('blue')

  ax.set_xlim(0, mass[0])
  ax.set_ylim(-8, 1)
  ax.set_xlabel(r'Mass [M$_\odot$]')
  ax.set_ylabel(r'$\log R=\log\rho -3\log T+18$ [cgs]')

  left.set_xlim(0, mass[0])
  left.set_ylim(8, 3.75)
  left.set_ylabel(r'$\log T$ [K]', color='blue')

  ax.plot([], [], linestyle='dashed', color='blue', lw=2, label=r'$\log T$')
  leg = ax.legend(loc=0)

  plt.savefig(file_out)
  print ' - plot_profile: actual_model_logR_OPAL: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
#=================================================================================================================




#! /usr/bin/python 


#import numpy as np

import stars, read, commons, sample_grid, plot_hrd
import param_tools as pt
import slurm_commons as slc
import time
import datetime

#=================================================================================================================

time_start_script = time.clock()
print 
print ' - Start Time: %s ' % (datetime.datetime.now(),)

dic_star = stars.Nu_Eri()
Teff = dic_star['Teff']
err_Teff = dic_star['err_Teff_1s']
logg = dic_star['logg']
err_logg = dic_star['err_logg_1s']

freq_unit = dic_star['freq_unit']
f_obs_radial_cd = dic_star['f1_l0']
f_obs_radial_err_cd = dic_star['f1_l0_err']

cd_to_Hz = commons.conversions()['cd_to_Hz']

path_repo = commons.set_paths()['path_base']
dic_param = commons.set_srch_param()
mass_from = dic_param['mass_from']
mass_to   = dic_param['mass_to']
mass_step = dic_param['mass_step']
mass_fmt  = dic_param['mass_fmt']
srch_hist_regex = dic_param['srch_hist_regex']

time_start_scan_grid = time.clock()
#dic_grid = sample_grid.scan_grid(Teff, err_Teff, logg, err_logg)
dic_grid = sample_grid.scan_grid_multiproc(Teff, err_Teff, logg, err_logg)
time_end_scan_grid = time.clock()
time_scan_grid_sec = time_end_scan_grid - time_start_scan_grid
time_scan_grid_min = time_scan_grid_sec / 60.
time_scan_grid_hr  = time_scan_grid_min / 60. 
print ' - How many entries in dic_grid? %s' % (len(dic_grid), )
if time_scan_grid_hr > 1: print ' - Time Lapse for scan_grid: %s [hr]' % (time_scan_grid_hr, )
else: print ' - Time Lapse for scan_grid: %s [min]' % (time_scan_grid_min, )

time_start_list_files = time.clock()
list_dic_files = slc.get_list_dic_files(path_repo, mass_from, mass_to, mass_step, mass_fmt)
time_end_list_files = time.clock()
time_list_files_sec = time_end_list_files - time_start_list_files
time_list_files_min = time_list_files_sec / 60.
time_list_files_hr  = time_list_files_min / 60.
if time_list_files_hr > 1: print ' - Time Lapse for get_list_dic_files: %s [hr]' % (time_list_files_hr, )
else: print ' - Time Lapse for get_list_dic_files: %s [min]' % (time_list_files_min, )

hist_files = []
for dic_files in list_dic_files:
  which_hist_files = dic_files['hist_files']
  for hf in which_hist_files: hist_files.append(hf)
n_hist = len(hist_files)  
print 'How many hist files? %s' % (n_hist, )

time_start_Kiel = time.clock()
#plot_hrd.plot_kiel_for_mesa_gyre_grid(dic_grid, list_dic_stars = [dic_star])
time_end_Kiel   = time.clock()
time_Kiel_sec = time_end_Kiel - time_start_Kiel
time_Kiel_min = time_Kiel_sec / 60.
time_Kiel_hr  = time_Kiel_min / 60.
if time_Kiel_hr > 1: print ' - Time Lapse Plot Kiel Diagram: %s [hr]' % (time_Kiel_hr, )
elif time_Kiel_hr<1 and time_Kiel_min>1: print ' - Time Lapse Plot Kiel Diagram: %s [min]' % (time_Kiel_min, )
elif time_Kiel_min<1 and time_Kiel_sec>1: print ' - Time Lapse Plot Kiel Diagram: %s [sec]' % (time_Kiel_sec, )
else: print '   Error: Something is wrong here!'

time_start_fit_one_freq = time.clock()
f_obs_radial_Hz = f_obs_radial_cd * cd_to_Hz
f_obs_radial_err_Hz = f_obs_radial_err_cd * cd_to_Hz
el_obs = 0
radial_mode = tuple([el_obs, f_obs_radial_Hz, f_obs_radial_err_Hz])
list_dic_radial = sample_grid.fit_one_freq(dic_grid, radial_mode)
time_end_fit_one_freq   = time.clock()
time_fit_one_freq_sec = time_end_fit_one_freq - time_start_fit_one_freq
time_fit_one_freq_min = time_fit_one_freq_sec / 60.
time_fit_one_freq_hr  = time_fit_one_freq_min / 60.
if time_fit_one_freq_hr > 1: print ' - Time Lapse to fit_one_freq: %s [hr]' % (time_fit_one_freq_hr, )
elif time_fit_one_freq_hr<1 and time_fit_one_freq_min>1: print ' - Time Lapse to fit_one_freq: %s [min]' % (time_fit_one_freq_min, )
elif time_fit_one_freq_min<1: print ' - Time Lapse to fit_one_freq: %s [sec]' % (time_fit_one_freq_sec, )

time_end_script = time.clock()
time_script_sec = time_end_script - time_start_script
time_script_min = time_script_sec / 60.
time_script_hr  = time_script_min / 60.
if time_script_hr > 1: print ' - Time Lapse to Run Nu_Eri Script: %s [hr]' % (time_script_hr, )
elif time_script_hr<1 and time_script_min>1: print ' - Time Lapse to Run Nu_Eri Script: %s [min]' % (time_script_min, )
elif time_script_min<1: print ' - Time Lapse to Run Nu_Eri Script: %s [sec]' % (time_script_sec, )
else: print 'Error: Something is Wrong heere!'

print ' - End Time: %s ' % (datetime.datetime.now(),)
print ' - Done '
print

#=================================================================================================================




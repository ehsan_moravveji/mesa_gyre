
import os, sys, glob
import logging
import numpy as np 
import pylab as plt 
from mesa_gyre import commons

dic_conversions = commons.conversions()

#=========================================================================================
#=========================================================================================
#=========================================================================================
#=========================================================================================
def per_vs_rot_freq(list_dic_h5, dic_star, per_from=0.5, per_to=3, file_out=None):
  """
  This routine shows the evolution of the mode periods with respect to rotation rate,
  here eta_rot, quite similar to Figs. 1 and 2 in Bouabid et al. (2013).
  @param list_dic_h5: list of frequencies for every step/iteration (of a given eta_rot). 
        In addition to the default fields returned by read.read_multiple_gyre_files(), the
        following additional keys are also present:
        - omega_crit: critical angular rotation velocity, 2*pi*freq_crit in rad/sec
        - eta_rot: dimensionless quantity, equal to eta_rot above
  @type list_dic_h5: list of dictionaries
  @param file_out: full path to the output plot file
  @type file_out: string 
  @return: None
  @rtype: None
  """
  if file_out is None: return 
  sec_to_d = dic_conversions['sec_to_d']
  n_dic    = len(list_dic_h5)
  arr_eta_rot  = np.array([ dic['eta_rot'] for dic in list_dic_h5 ])
  arr_num_freq = np.array([ len(dic['l']) for dic in list_dic_h5 ])
  arr_freq     = [ np.real(dic['freq']) for dic in list_dic_h5 ]

  #--------------------------------
  # Prepare the plot
  #--------------------------------
  # fig, (top, bot) = plt.subplots(2, figsize=(4, 6), sharex=True)
  fig = plt.figure(figsize=(4, 6))
  l   = 0.135
  w   = 0.830
  b   = 0.075
  h1  = 0.60
  h2  = 0.30
  bot = fig.add_axes([l, b, w, h1])
  top = fig.add_axes([l, b+h1+0.01, w, h2])
  # plt.subplots_adjust(left=0.15, right=0.96, bottom=0.075, top=0.98)

  #--------------------------------
  # Do the plot
  #--------------------------------
  dics_obs   = dic_star['list_dic_freq']
  N_obs_freq = len(dics_obs)
  obs_freqs  = np.array([ dic['freq'] for dic in dics_obs ])
  min_obs_freq = np.min(obs_freqs)
  max_obs_freq = np.max(obs_freqs)
  min_per    = 1.0 / max_obs_freq
  max_per    = 1.0 / min_obs_freq

  min_n_pg   = 1000
  max_n_pg   = -1000
  list_n_pg  = []
  list_per   = []
  list_eta_rot = []
  list_N_mod = []

  for i_dic, dic in enumerate(list_dic_h5):
    eta_rot  = dic['eta_rot'] 
    freq     = np.real(dic['freq']) 
    n_freq   = len(freq)
    per      = 1.0 / freq * sec_to_d
    n_pg     = dic['n_pg']
    min_n_pg = min([min_n_pg, np.min(n_pg)])
    max_n_pg = max([max_n_pg, np.max(n_pg)])
    eta_arr  = np.ones(n_freq) * eta_rot

    ind_per  = np.where((per >= min_per) & (per <= max_per))[0]
    N_mod    = len(ind_per)

    list_eta_rot.append(eta_rot)
    list_n_pg.append(n_pg)
    list_per.append(per)
    list_N_mod.append(N_mod)

    # bot.scatter(eta_arr * 100, per, s=1, marker='.', color='black', zorder=2)

  range_n_pg = range(min_n_pg, max_n_pg+1)
  keep_eta   = []
  keep_per   = []
  for i, n in enumerate(range_n_pg):
    vals_eta = []
    vals_per = []
    for i_eta, eta_rot in enumerate(list_eta_rot):
      n_pg = list_n_pg[i_eta]
      ind  = np.where(n == n_pg)[0]
      if ind:
        vals_eta.append(eta_rot * 100)
        vals_per.append(list_per[i_eta][ind])
    keep_eta.append(vals_eta)
    keep_per.append(vals_per)

    bot.plot(vals_eta, vals_per, linestyle='solid', color='black', lw=0.5, zorder=3)

  list_eta_rot = np.array(list_eta_rot) * 100
  #--------------------------------
  # Add observed box as a background patch
  #--------------------------------
  bot.fill_between(list_eta_rot, y1=min_per, y2=max_per, color='blue', alpha=0.10, zorder=1)

  #--------------------------------
  # Add the top panel now
  #--------------------------------
  top.axhline(y=N_obs_freq, linestyle='dashed', color='grey', lw=2, zorder=1)
  top.scatter(list_eta_rot, list_N_mod, facecolor='blue', edgecolor='red', 
              marker='o', s=25, zorder=2)

  #--------------------------------
  # Legend, Annotations and Ranges
  #--------------------------------
  dx = (max(list_eta_rot) - min(list_eta_rot)) * 0.02
  top.set_xlim(min(list_eta_rot)-dx, max(list_eta_rot)+dx)
  top.set_ylim(min(list_N_mod)*0.90, max(list_N_mod)*1.10)
  top.set_ylabel(r'$N^{\rm (mod)}$')
  top.set_xticklabels(())
  top.xaxis.set_tick_params(labeltop='on')

  dy = (max(list_N_mod) - min(list_N_mod)) * 0.05
  txt = dic_star['name'][0]
  top.annotate(txt, xy=(min(list_eta_rot)+2*dx, N_obs_freq+dy), color='black', fontsize=10)

  bot.set_xlim(min(list_eta_rot)-dx, max(list_eta_rot)+dx)
  bot.set_ylim(per_from, per_to)
  bot.set_xlabel(r'$\eta_{\rm rot}=\Omega_{\rm rot} / \Omega_{\rm crit} \,[\%]$')
  bot.set_ylabel(r'Period in Inertial Frame [d]')

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  plt.savefig(file_out, transparent=True)
  print ' - plot_tar: per_vs_rot_freq: saved {0}'.format(file_out)
  plt.close()

  return

#=========================================================================================
def freq_vs_rot_freq(list_dic_h5, file_out=None):
  """
  This routine shows the evolution of the mode frequencies with respect to rotation rate,
  here eta_rot.
  @param list_dic_h5: list of frequencies for every step/iteration (of a given eta_rot). 
        In addition to the default fields returned by read.read_multiple_gyre_files(), the
        following additional keys are also present:
        - omega_crit: critical angular rotation velocity, 2*pi*freq_crit in rad/sec
        - eta_rot: dimensionless quantity, equal to eta_rot above
  @type list_dic_h5: list of dictionaries
  @param file_out: full path to the output plot file
  @type file_out: string 
  @return: None
  @rtype: None
  """
  if file_out is None: return 
  n_dic = len(list_dic_h5)
  arr_eta_rot  = np.array([ dic['eta_rot'] for dic in list_dic_h5 ])
  arr_num_freq = np.array([ len(dic['l']) for dic in list_dic_h5 ])
  arr_freq     = [ np.real(dic['freq']) for dic in list_dic_h5 ]

  #--------------------------------
  # Prepare the plot
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(6,4))
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.11, top=0.97)

  #--------------------------------
  # Do the plot
  #--------------------------------
  N_obs_freq = 36     # From Papics et al. (2015)
  for i_dic, dic in enumerate(list_dic_h5):
    eta_rot  = dic['eta_rot'] 
    freq     = np.real(dic['freq']) * 1e6
    n_freq   = len(freq)
    eta_arr  = np.ones(n_freq) * eta_rot

    ax.scatter(eta_arr, freq, s=9, marker='o', color='grey')

  ax.set_xlim(min(arr_eta_rot)-0.05, max(arr_eta_rot)+0.05)
  # ax.set_ylim(min(arr_num_freq)-1, max(N_obs_freq, max(arr_num_freq))+1)
  ax.set_xlabel(r'$\Omega_{\rm rot} / \Omega_{\rm crit}$')
  ax.set_ylabel(r'Frequency [$\mu$Hz]')

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  plt.savefig(file_out)
  print ' - plot_tar: freq_vs_rot_freq: saved {0}'.format(file_out)
  plt.close()

  return

#=========================================================================================
def num_freq_vs_rot_freq(list_dic_h5, file_out=None):
  """
  The frequency density of prograde (m=+1) modes in a fixed frequency interval, say f_a to f_b, 
  increases with the increase in rotation frequency, and then it declines. This routine 
  shows the number of frequencies as a function of dimentional rotation rate, 
        eta_rot = Omega_rot/Omega_crit
  @param list_dic_h5: list of frequencies for every step/iteration (of a given eta_rot). 
        In addition to the default fields returned by read.read_multiple_gyre_files(), the
        following additional keys are also present:
        - omega_crit: critical angular rotation velocity, 2*pi*freq_crit in rad/sec
        - eta_rot: dimensionless quantity, equal to eta_rot above
  @type list_dic_h5: list of dictionaries
  @param file_out: full path to the output plot file
  @type file_out: string 
  @return: None
  @rtype: None
  """
  if file_out is None: return 
  n_dic = len(list_dic_h5)
  arr_eta_rot  = np.array([ dic['eta_rot'] for dic in list_dic_h5 ])
  arr_num_freq = np.array([ len(dic['l']) for dic in list_dic_h5 ])

  #--------------------------------
  # Prepare the plot
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(6,4))
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.11, top=0.97)

  #--------------------------------
  # Do the plot
  #--------------------------------
  N_obs_freq = 36     # From Papics et al. (2015)
  ax.scatter(arr_eta_rot, arr_num_freq, s=50, marker='o', facecolor='red', edgecolor='black', zorder=1)
  ax.axhline(y=N_obs_freq, linestyle='dashed', color='grey', lw=2, zorder=2)
  ax.annotate('KIC 7760680', xy=(0.0, N_obs_freq-2), fontsize=10, color='grey')

  ax.set_xlim(min(arr_eta_rot)-0.05, max(arr_eta_rot)+0.05)
  ax.set_ylim(min(arr_num_freq)-1, max(N_obs_freq, max(arr_num_freq))+1)
  ax.set_xlabel(r'$\Omega_{\rm rot} / \Omega_{\rm crit}$')
  ax.set_ylabel('Num. Computed Model Freequencies')

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  plt.savefig(file_out)
  print ' - plot_tar: num_freq_vs_rot_freq: saved {0}'.format(file_out)
  plt.close()

  return

#=========================================================================================

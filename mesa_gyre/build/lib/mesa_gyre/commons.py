
"""
This module defines some functions that return some initializations for the analyis of 
the MESA-GYRE grid.
"""

import os
import logging
from collections import OrderedDict
from math import ceil

MacBookPro = 1
Pleiad = 2
VSC = 3
  
#=================================================================================================================
def set_platform():
  """
  Set the platform
  """
  Platform = MacBookPro

  dic = OrderedDict({})
  dic['MacBookPro'] = MacBookPro
  dic['Pleiad'] = Pleiad
  dic['VSC'] = VSC
  dic['Platform'] = Platform
 
  return dic
  
#=================================================================================================================
def set_paths():
  """
  Set the paths
  """
  
  dic = set_platform()
  MacBookPro = dic['MacBookPro']
  Pleiad = dic['Pleiad']
  Platform = dic['Platform']
   
  if Platform == MacBookPro:
   path_base = '/Users/ehsan/programs/models/interactive/LOGS/'
     
  if Platform == Pleiad:
   path_base = '/STER/mesa-gyre/Grid/'
   
  if Platform == VSC:
   path_base = '/scratch/leuven/307/vsc30745/'

  path_plots = path_base + 'plots/'
  if not os.path.exists(path_plots): os.makedirs(path_plots)
         
  dic['path_base'] = path_base
  dic['path_plots'] = path_plots
  
  return dic


#=================================================================================================================
def set_srch_param():
 """
 This function sets the physical parameters, like mass, eta_rot, overshoot, semi-convection, metalicity, center 
 helium Yc and center hydrogen Xc. For each parameter, we give _from, _to, _step, _all, n_, _arr and _fmt. These
 are stored in an OrderedDict.
 @param None:
 @type None:
 @return: an OrderedDict with these key:val pairs:
  - evol: string giving evolutionary phase of interest; whether *, MS or PMS are allowed
  - mass: the initial mass of the model. All mentioned entries like mass_from etc. are given.
  - eta: Omega_eq/Omega_crit.
  - ov: the f_ov free parameter in Herwig (2000) prescription.
  - sc: semiconvection 
  - z: metalicity in the range 0.010 to 0.018
  - Xc: center hydrogen mass fraction
  - Yc: center helium mass fraction
  - el: spherical degree el 
 @rtype: OrderedDict
 """

 evol    = 'MS'
 
 use_hist_regex = True
 srch_hist_regex = 'M*-eta0.10-ov0.012-sc0.01-Z*'
 use_prof_regex = False
 srch_prof_regex = '*'
 use_h5_regex = True
 srch_h5_regex = srch_hist_regex + '*' + evol + '*' + '-Yc*' + 'Xc*'
 
 mass_from = 04.1
 mass_to   = 20.0
 mass_step = 0.10
 mass_all  = False
 n_mass    = get_num_srch_param(mass_from, mass_to, mass_step, mass_all)['n']
 mass_arr  = get_num_srch_param(mass_from, mass_to, mass_step, mass_all)['arr']
 mass_fmt  = "{:04.1f}"
  
 eta_from  = 0.00
 eta_to    = 0.00
 eta_step  = 0.02
 eta_all   = False
 n_eta     = get_num_srch_param(eta_from, eta_to, eta_step, eta_all)['n']
 eta_arr   = get_num_srch_param(eta_from, eta_to, eta_step, eta_all)['arr']
 eta_fmt   = "{:04.2f}"

 ov_from   = 0.000
 ov_to     = 0.030
 ov_step   = 0.003
 ov_all    = False
 n_ov      = get_num_srch_param(ov_from, ov_to, ov_step, ov_all)['n']
 ov_arr    = get_num_srch_param(ov_from, ov_to, ov_step, ov_all)['arr']
 ov_fmt    = "{:05.3f}"
  
 sc_from   = 0.01
 sc_to     = 0.01
 sc_all    = False
 n_sc      = 1
 sc_fmt    = "{:04.2f}"
  
 z_from    = 0.014
 z_to      = 0.014
 z_step    = 0.002
 z_all     = False
 n_z       = get_num_srch_param(z_from, z_to, z_step, z_all)['n']
 z_arr     = get_num_srch_param(z_from, z_to, z_step, z_all)['arr']
 z_fmt     = "{:05.3f}"
 
 Xc_from   = 0.70
 Xc_to     = 0.00
 Xc_step   = 0.01
 Xc_all    = True
 n_Xc      = get_num_srch_param(Xc_from, Xc_to, Xc_step, Xc_all)['n']
 Xc_arr    = get_num_srch_param(Xc_from, Xc_to, Xc_step, Xc_all)['arr']
 Xc_fmt    = "{:06.4f}"
 
 Yc_from   = 0.80
 Yc_to     = 0.90
 Yc_step   = 0.02
 Yc_all    = True
 n_Yc      = get_num_srch_param(Yc_from, Yc_to, Yc_step, Yc_all)['n']
 Yc_arr    = get_num_srch_param(Yc_from, Yc_to, Yc_step, Yc_all)['arr']
 Yc_fmt    = "{:06.4f}"

 ell_from  = 0
 ell_to    = 2
 ell_step  = 1
 ell_all   = False
 n_ell     = get_num_srch_param(ell_from, ell_to, ell_step, ell_all)['n']
 ell_arr   = get_num_srch_param(ell_from, ell_to, ell_step, ell_all)['arr']
 ell_fmt   = "{:1i}"

 dic = OrderedDict({})
 dic['evol'] = evol 
 dic['mass_from'] = mass_from
 dic['mass_to'] = mass_to
 dic['mass_step'] = mass_step
 dic['mass_all'] = mass_all
 dic['mass_fmt'] =  mass_fmt
 dic['n_mass'] = n_mass
 dic['mass_arr'] = mass_arr
 dic['eta_from'] = eta_from
 dic['eta_to'] = eta_to
 dic['eta_step'] = eta_step
 dic['eta_all'] = eta_all
 dic['eta_fmt'] = eta_fmt
 dic['n_eta'] = n_eta
 dic['eta_arr'] = eta_arr
 dic['ov_from'] = ov_from
 dic['ov_to'] = ov_to
 dic['ov_step'] = ov_step
 dic['ov_all'] = ov_all
 dic['ov_fmt'] = ov_fmt
 dic['n_ov'] = n_ov
 dic['ov_arr'] = ov_arr 
 dic['sc_from'] = sc_from
 dic['sc_to'] = sc_to
 dic['sc_all'] = sc_all
 dic['sc_fmt'] = sc_fmt
 dic['n_sc'] = n_sc 
 dic['z_from'] = z_from
 dic['z_to'] = z_to
 dic['z_step'] = z_step
 dic['z_all'] = z_all 
 dic['z_fmt'] = z_fmt
 dic['n_z'] = n_z
 dic['z_arr'] = z_arr 
 dic['Xc_from'] = Xc_from
 dic['Xc_to'] = Xc_to
 dic['Xc_step'] = Xc_step
 dic['Xc_all'] = Xc_all 
 dic['Xc_fmt'] = Xc_fmt
 dic['n_Xc'] = n_Xc
 dic['Xc_arr'] = Xc_arr 
 dic['Yc_from'] = Yc_from
 dic['Yc_to'] = Yc_to
 dic['Yc_step'] = Yc_step
 dic['Yc_all'] = Yc_all 
 dic['Yc_fmt'] = Yc_fmt
 dic['n_Yc'] = n_Yc
 dic['Yc_arr'] = Yc_arr 
 dic['ell_from'] = ell_from
 dic['ell_to'] = ell_to
 dic['ell_step'] = ell_step
 dic['ell_all'] = ell_all
 dic['ell_fmt'] = ell_fmt
 dic['n_ell'] = n_ell
 dic['ell_arr'] = ell_arr 
 dic['use_hist_regex'] = use_hist_regex
 dic['srch_hist_regex'] = srch_hist_regex
 dic['use_prof_regex'] = use_hist_regex
 dic['srch_prof_regex'] = srch_prof_regex
 dic['use_h5_regex'] = use_h5_regex
 dic['srch_h5_regex'] = srch_h5_regex
 
 return dic


#=================================================================================================================
def settings():
  
  hist_prefix = '.hist'
  prof_prefix = '.prof'
  gyre_prefix = '.h5'
  
  dic = OrderedDict({'hist_prefix':hist_prefix, 'prof_prefix':prof_prefix, 'gyre_prefix':gyre_prefix})
  
  return dic
      
#=================================================================================================================
def get_num_srch_param(frm, to, step, flag = True):
  """
  For the parameter of interest, find the number of requested value.
  Returns a dictionary.
  """
  import numpy as np
  
  #if flag:
    #arr = np.arange(frm, to+step, step)
    #n = len(arr)
  #else:
    #arr = np.zeros(1) + frm
    #n = 1
  n = int(ceil( (to-frm)/step )) + 1
  arr = np.arange(frm, to+step, step)
  
  dic = OrderedDict({'n':n, 'arr':arr})
  
  return dic

#=================================================================================================================
def conversions():
  """
  This function returns a dictionary with basic frequency conversion factors between cycles per day (cd), Hz and 
  micro Hz (uHz).
  @return: dictionary with converson factors
  @rtype: OrderedDict
  """
 
  d_to_sec = 24.0 * 3600.
  sec_to_d = 1.0 / d_to_sec

  Hz_to_uHz = 1e6
  uHz_to_Hz = 1e-6

  cd_to_Hz = 1.0 / d_to_sec
  cd_to_uHz = cd_to_Hz * Hz_to_uHz
  Hz_to_cd = 1.0 / cd_to_Hz
  uHz_to_cd = 1.0 / cd_to_uHz
  
  dic_conv = OrderedDict({})
  dic_conv['d_to_sec'] = d_to_sec 
  dic_conv['sec_to_d'] = sec_to_d
  dic_conv['Hz_to_uHz'] = Hz_to_uHz
  dic_conv['uHz_to_Hz'] = uHz_to_Hz
  dic_conv['cd_to_Hz'] = cd_to_Hz
  dic_conv['cd_to_uHz'] = cd_to_uHz
  dic_conv['Hz_to_cd'] = Hz_to_cd 
  dic_conv['uHz_to_cd'] = uHz_to_cd 

  return dic_conv


#=================================================================================================================
def get_pp_job_server():
  try:
    import pp
  except ImportError:
    print 'Error: mesa_gyre: commons: get_pp_job_server: Failed to import "pp".'
    raise SystemExit
  import subprocess
  
  pp_dir = os.path.dirname(pp.__file__)
  path_ppserver = os.path.join(pp_dir, 'ppserver.py')
  
  pid = os.spawnl(os.P_NOWAIT, path_ppserver)
  
  process = subprocess.Popen('hostname', stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
  hostname, err = process.communicate()
  
  ppservers = (hostname, )

  job_server = pp.Server(ppservers=ppservers)
  
  return job_server
  
#=================================================================================================================
def get_slurm_jobid():
  """
  Get the $SLURM_JOBID environment variable for each task for each job running on SLURM.
  """
  job_id = os.getenv('SLURM_JOBID')
  if job_id is None:
    logging.error('commons: get_slurm_job_id: failed to get $SLURM_JOBID')
    raise SystemExit, 'Error: commons: get_slurm_job_id: failed to get $SLURM_JOBID'

  return int(job_id)

#=================================================================================================================

  
  
"""
This module provides methods to plot a single or multiple MESA evolutionary tracks
as an Hertzsprung-Russel diagram (HRD), or as a Kiel diagram (i.e. logTeff vs. log_g)
"""
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from pylab import *
import math
import numpy as np

import commons as cm
import read as rd
import param_tools as param
import plot_commons as pcm

logg_sun  = np.log10(27542.29)  
L_sun     = 3.8418e33
Msun      = 1.9892e33    # gr; as defined in mesa/const/const_def.f
Rsun      = 6.9598e10    # cm; as defined in mesa/const/const_def.f
c_grav    = 6.67428e-8   # cgs; as defined in mesa/const/const_def.f
logg_sun  = np.log10(c_grav*Msun/Rsun**2)   # g_sun = 27542.29 cgs  


#=================================================================================================================
def plot_kiel_for_mesa_gyre_grid(list_dic, list_dic_stars=None, log_Teff_from=None, log_Teff_to=None,
                                 log_g_from=None, log_g_to=None, file_out=None):
  """
  Fix: This module should not read the whole input files. Instead, it must receive a list of dictionaries with all
  history info!
  change it
  @param file_out: full path to store the Kiel diagram plot as a pdf file
  @type file_out: string
  @param list_dic_stars: list of dictionaries with stars loaded from mesa_gyre.stars
  @type list_dic_stars: list of dictionaries
  """

  n_points = len(list_dic)
  if n_points == 0:
    print 'plot_hrd: plot_kiel_for_mesa_gyre_grid: empty input list.'
    return None

  dic_param = cm.set_srch_param()
  mass_from = dic_param['mass_from']
  mass_to   = dic_param['mass_to']
  mass_fmt  = dic_param['mass_fmt']

  fig = plt.figure()
  fig, ax = plt.subplots()
  plt.subplots_adjust(left=0.08, right=0.99, bottom=0.10, top=0.99, hspace=0.03, wspace=0.03)

  list_avail_tracks = []
  list_zams_logg = []
  list_zams_log_Teff = []
  list_tams_log_Teff = []

  for i_pt, dic_pt in enumerate(list_dic):
    track_name = dic_pt['filename']
    flag_track_is_avail = track_name in list_avail_tracks
    if not flag_track_is_avail:
      list_avail_tracks.append(track_name)
      #header, data = rd.read_mesa(filename=track_name)
      data = dic_pt['hist']
      track_log_Teff = data['log_Teff']
      track_logg = data['log_g']
      track_Xc   = data['center_h1']
      ind_tams   = np.where((track_Xc < 1e-6))[0]
      mxg = np.argmax(track_logg)
      list_zams_logg.append(track_logg[mxg])
      list_zams_log_Teff.append(track_log_Teff[mxg])
      if len(ind_tams) == 0:
        list_tams_log_Teff.append(track_log_Teff[-1])
      elif ind_tams[0] != -1: list_tams_log_Teff.append(track_log_Teff[ind_tams[0]])
      # Now, plot the new track
      ax.plot(track_log_Teff[mxg:], track_logg[mxg:], color = '0.60', zorder=1)

    pt_log_Teff = data['log_Teff']
    pt_logg = data['log_g']
    #ax.scatter([pt_log_Teff, ], [pt_logg, ], s=4, c='black', marker="o", zorder=0)
    if 'has_h5' in data.dtype.names:
      has_h5 = data['has_h5']
      if has_h5: ax.scatter([pt_log_Teff, ], [pt_logg, ], s=16, c='red', marker="o")

  #--------------------------------
  # Include Observed Stars
  #--------------------------------
  if list_dic_stars is not None:
    n_dic_stars = len(list_dic_stars)
    for i_dic, dic_star in enumerate(list_dic_stars):
      names = dic_star.keys()
      star_log_Teff = dic_star['log_Teff']
      star_log_Teff_err = dic_star['err_log_Teff_1s']
      star_logg = dic_star['logg']
      star_logg_err = dic_star['err_logg_1s']

      if 'marker' in names: marker = dic_star['marker']
      else: marker = 's'
      if 'ms' in names: ms = dic_star['ms']
      else: ms = 10
      if 'mfc' in names: mfc = dic_star['mfc']
      else: mfc = 'navy'
      if 'ecolor' in names: ecolor = dic_star['ecolor']
      else: ecolor = mfc
      if 'mec' in names: mec = dic_star['mec']
      else: mec = 'purple'

      ax.errorbar(star_log_Teff, star_logg, xerr=star_log_Teff_err, yerr=star_logg_err, ecolor=ecolor,
                  elinewidth=2, capsize=5, marker=marker, mfc=mfc, mec=mec, ms=ms, zorder=2,
                  label=r''+dic_star['name'][0])
    if n_dic_stars <=10: leg_star = ax.legend(loc=4, fontsize='small')

  #--------------------------------
  # Finalize the Plot
  #--------------------------------
  if log_Teff_from is None: log_Teff_from = max(list_zams_log_Teff) + 0.005
  if log_Teff_to   is None: log_Teff_to   = min(list_tams_log_Teff) - 0.005
  if log_g_from    is None: log_g_from    = max(list_zams_logg) + 0.05
  if log_g_to      is None: log_g_to      = 3.0
  ax.set_xlim(log_Teff_from, log_Teff_to)
  ax.set_ylim(log_g_from, log_g_to)
  ax.set_xlabel(r'Effective Temperature $\log$T$_{\rm eff}$ [K]')
  ax.set_ylabel(r'Surface Gravity $\log g$ [cm sec$^{-2}$]')

  txt1 = r'Z=Z$_\odot$'
  txt2 = r'1.30$\leq$M/M$_\odot\leq$3.75'
  txt3 = r'$\Omega_{\rm surf}/\Omega_{\rm crit}$=45\%'
  #ax.annotate(txt1, xy=(0.04, 0.90), xycoords='axes fraction', fontsize='large')
  #ax.annotate(txt2, xy=(0.04, 0.83), xycoords='axes fraction', fontsize='large')
  #ax.annotate(txt3, xy=(0.04, 0.76), xycoords='axes fraction', fontsize='large')

  if not file_out:
    path_plots = cm.set_paths()['path_plots']
    plot_name = 'Kiel.pdf'
    file_out = path_plots + plot_name #'Kiel-M' + mass_fmt.format(mass_from) + '-M' + mass_fmt.format(mass_to) + '.pdf'
  plt.savefig(file_out, dpi = 600)
  print
  print ' - Kiel diagram %s saved.' % (file_out, )
  plt.close()

  return None


#=================================================================================================================
def plot_kiel_diagram(list_hist, list_list_dic_prof=None, list_lbls=None, ref_indx=0,
                      list_dic_stars=None, list_dic_mesa_gyre = None,
                      file_out=None, logTeff_from=None, logTeff_to=None,
                      logg_from=None, logg_to=None, list_dic_annot=None, *args, **kwargs):
  """
  Kiel Diagram -i.e. log_Teff vs. log_g - for a grid of MESA tracks.
  @param list_hist: list of dictionaries, where each dictionary contains all *.hist information
              Values for each of dictionary key is either a single integer/float/string or
              a 1D numpy array.
  @type list_hist: list of dictionaries
  @param list_list_dic_prof: defalt=None; if provided, it is complicated! It gives a list of list of dictionaries.
      per each history file in list_hist, there are several profile files stored. These profile files are
      identified and read into a list of dictionaries by read.read_multiple_mesa_files. each dictionary has
      these keys: filename, header, and prof. Here, we mainly use the header only to get logTeff and logg for
      each profile file, and put them on the track, to highlight each point along the track where a profile is
      stored.
  @type list_list_dic_prof: list of list of dictionaries.
  @param list_lbls: list of labels, where each label corresponds to one track in list_hist
  @type list_lbls: list of strings
  @param list_dic_stars: list of dictionaries with stars loaded from mesa_gyre.stars
  @type list_dic_stars: list of dictionaries
  @param list_dic_mesa_gyre: list of MESA profile data, to flag the position of available profile on the HRD track
  @type list_dic_mesa_gyre: list of dictionaries
  @param file_out: full path to store the Kiel diagram plot as a pdf file
  @type file_out: string
  @param logTeff_from, logTeff_to: the range of logTeff on the x-axis; defalt = None
  @type logTeff_from, logTeff_to: float
  @param logg_from, logg_to: the range of log_g on the y-axis; defalt = None
  @type logg_from, logg_to: float
  @param list_dic_annot: list of dictionaries giving the annotation key/values to be put on the plot.
  @type list_dic_annot: list of dictionaries
  @return: None
  @rtype:  None
  """
  import itertools

  n_tracks = len(list_hist)
  if n_tracks == 0:
    message = 'Error: plot_hrd: plot_kiel_diagram: Input list is empty'
    raise SystemExit, message

  if list_list_dic_prof: n_list_list_dic_prof = len(list_list_dic_prof)

  dic_sample = list_hist[0]
  if not dic_sample.has_key('hist'):
    message = 'Error: plot_hrd: plot_kiel_diagram: sample dictionary has no .hist field (which is numpy record array itself)'
  hist_sample = dic_sample['hist']
  dic_avail_keys = {}
  for name in hist_sample.dtype.names: dic_avail_keys[name] = 0.0
  required_keys = ['initial_mass', 'initial_z', 'Teff', 'log_Teff', 'log_g', 'gravity',
                   'surf_avg_v_rot', 'surf_avg_v_crit']
  dic_keys = rd.check_key_exists(dic_avail_keys, required_keys)
  flag_initial_mass = dic_keys['initial_mass']
  flag_initial_z    = dic_keys['initial_z']
  flag_log_Teff = dic_keys['log_Teff']
  flag_log_g    = dic_keys['log_g']
  flag_surf_v_rot  = dic_keys['surf_avg_v_rot']
  flag_surf_v_crit = dic_keys['surf_avg_v_crit']

  if not flag_log_Teff or not flag_log_g:
    print 'Kiel requires log_g and log_Teff. '
    print 'log_Teff:', flag_log_Teff, ' and log_g:', flag_log_g
    message = 'Error: plot_hrd: plot_kiel_diagram: Exit'
    raise SystemExit, message

  #--------------------------------
  # Prepare For Plotting; Load colors
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=600)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.09, right=0.97, bottom=0.105, top=0.97, hspace=0.01, wspace=0.01)
  ax.set_xlabel(r'$\log$' +r'$\, T_{\rm eff}$', fontsize='medium')
  ax.set_ylabel(r'$\log \, g \, {\rm [cgs]}$', fontsize='medium')

  cmap = plt.get_cmap('brg')
  norm = mpl.colors.Normalize(vmin=0, vmax=n_tracks)
  color = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)

  ls = itertools.cycle(['dotted', 'dashed', 'dashdot'])

  #--------------------------------
  # Read the grid iteratively
  #--------------------------------
  tracks_min_log_Teff = np.zeros(n_tracks)
  tracks_max_log_Teff = np.zeros(n_tracks)
  tracks_min_log_g    = np.zeros(n_tracks)
  tracks_max_log_g    = np.zeros(n_tracks)

  dic_param = param.param_to_str()
  flag_mass_all = dic_param['mass_all']
  flag_eta_all  = dic_param['eta_all']
  flag_ov_all   = dic_param['ov_all']
  flag_z_all    = dic_param['z_all']

  for i_track in range(n_tracks):
    hist = list_hist[i_track]['hist']
    mxg = 0
    if flag_log_g:
      log_g = hist['log_g']
      max_g = log_g.max()
      mxg = np.argmax(log_g)
      log_g = log_g[mxg:]
    log_Teff = hist['log_Teff'][mxg:]
    n_lines = len(log_Teff)
    #if flag_phot_L: log_L = np.log10(hist['photosphere_L'])[mxg:]
    #if flag_log_L: log_L = hist['log_L'][mxg:]
    if flag_initial_mass: m_ini = hist['initial_mass']
    if flag_initial_z:    z_ini = hist['initial_z']
    if flag_surf_v_rot:  v_rot = hist['surf_avg_v_rot'][mxg:]
    if flag_surf_v_crit: v_cri = hist['surf_avg_v_crit'][mxg:]

    tracks_min_log_Teff[i_track] = log_Teff.min()
    tracks_max_log_Teff[i_track] = log_Teff.max()
    tracks_min_log_g[i_track] = log_g.min()
    tracks_max_log_g[i_track] = log_g.max()

    # if list of profiles are also provided, flag each profile file along the track with a dot
    if list_list_dic_prof:
      which_list_dic = list_list_dic_prof[i_track]
      n_dic_in_list = len(which_list_dic)
      list_prof_logTeff = np.zeros(n_dic_in_list)
      list_prof_logg    = np.zeros(n_dic_in_list)
      for i_dic_prof, dic_prof in enumerate(which_list_dic):
        dic_prof_header = dic_prof['header']
        one_prof_Teff = dic_prof_header['Teff']
        one_prof_mass = dic_prof_header['star_mass']
        one_prof_radi = dic_prof_header['photosphere_r']
        logg_sun      = np.log10(27542.29)
        one_prof_logg = np.log10(one_prof_mass/(one_prof_radi**2.0)) + logg_sun
        # Put profile points if available
        list_prof_logTeff[i_dic_prof] = np.log10(one_prof_Teff[0])
        list_prof_logg[i_dic_prof]    = one_prof_logg[0]
      #list_prof_logTeff = np.asarray(list_prof_logTeff)
      #list_prof_logg    = np.asarray(list_prof_logg)

    # Find appropriate on-screen text based on search param
    if list_lbls:
      #if i_track == ref_indx: list_lbls[ref_indx] = r'Ref: ' + list_lbls[ref_indx]
      leg_label = r'' + list_lbls[i_track]
    else:
      leg_label = ''

    lw = 1
    if list_list_dic_prof: lw = 1
    if i_track == ref_indx:
      lstyle = 'solid'
      clr = 'black'
    else:
      lstyle = ls.next()
      clr = color.to_rgba(i_track)
    #lstyle = 'solid'

    # Now, plot the track
    if list_lbls:
      ax.plot(log_Teff, log_g, linewidth=lw, linestyle=lstyle, color=clr, label = leg_label)
    else:
      ax.plot(log_Teff, log_g, linewidth=lw, linestyle=lstyle, color='gray')
    # Highlight those models along the track for which a profile file is stored
    if list_list_dic_prof:
      ax.scatter(list_prof_logTeff, list_prof_logg, s=25, color=color.clr)
    # Highlight the ZAMS
    ax.scatter([log_Teff[0],], [log_g[0],], 50, color='purple')

  #--------------------------------
  # Final Plot Specifications
  #--------------------------------
  if list_lbls:
    leg = ax.legend(loc = 4, shadow=True, framealpha=0.5, fontsize='small')
    #leg = ax.legend(loc = 2, shadow=True, framealpha=0.5)
  max_log_Teff = tracks_max_log_Teff.max()
  min_log_Teff = tracks_min_log_Teff.min()
  max_log_g    = tracks_max_log_g.max()
  min_log_g    = tracks_min_log_g.min()
  #xlim(max_log_Teff+0.02, min_log_Teff-0.02)
  #ylim(max_log_g+0.2, min_log_g-0.1)
  if logTeff_from is not None: xlim_1 = logTeff_from
  else: xlim_1 = max_log_Teff+0.02
  if logTeff_to is not None: xlim_2 = logTeff_to
  else: xlim_2 = min_log_Teff-0.02
  if logg_from is not None: ylim_1 = logg_from
  else: ylim_1 = max_log_g + 0.05
  if logg_to is not None: ylim_2 = logg_to
  else: ylim_2 = min_log_g-0.05
  #
  ax.set_xlim(xlim_1, xlim_2)
  ax.set_ylim(ylim_1, ylim_2)

  #--------------------------------
  # Add Observed Stars with Asymmetric
  # Errorbars
  #--------------------------------
  if False and list_dic_stars:
    conny_stars = rd.add_stars('star_lists/aerts.list')
    for col_names in conny_stars.dtype.names:
      if col_names == 'log_g':
        obs_log_g = conny_stars[col_names]
      if col_names == 'e_log_g':
        obs_log_g_err = conny_stars[col_names]
      if col_names == 'Teff':
        Teff_col = col_names
        obs_log_Teff = np.log10(conny_stars[col_names])
      if col_names == 'e_Teff':
        if conny_stars[col_names].any > 0:
          err_Teff_col = col_names
          obs_log_Teff_err_hi = np.log10(conny_stars[Teff_col]+conny_stars[err_Teff_col]) - np.log10(conny_stars[Teff_col])
          obs_log_Teff_err_lo = np.log10(conny_stars[Teff_col]) - np.log10(conny_stars[Teff_col]-conny_stars[err_Teff_col])
        obs_log_Teff_err = [obs_log_Teff_err_lo, obs_log_Teff_err_hi]

    ax.errorbar(obs_log_Teff, obs_log_g, xerr = obs_log_Teff_err, yerr = obs_log_g_err,
                 color = 'Red', fmt = 'o')
    for i in range(len(conny_stars)):
      ax.text(obs_log_Teff[i], obs_log_g[i], conny_stars['Name'][i], ha='left', va='center', color='Red',
              transform=gca().transAxes, fontsize = 10, clip_on = True)

  #--------------------------------
  # Add Stars from a list
  #--------------------------------
  if list_dic_stars:
    n_dic_stars = len(list_dic_stars)
    print '\n - plot_hrd: plot_kiel_diagram: Including %s stars from the input list' % (n_dic_stars, )
    for i_star, dic_star in enumerate(list_dic_stars):
      star_name = dic_star['name'][0]
      star_Teff = dic_star['Teff']
      star_log_Teff = dic_star['log_Teff']
      star_Teff_err = dic_star['err_Teff_1s']
      star_log_Teff_err = dic_star['err_log_Teff_1s']
      star_logg = dic_star['logg']
      star_logg_err = dic_star['err_logg_1s']
      ax.errorbar(star_log_Teff, star_logg, xerr = star_log_Teff_err, yerr = star_logg_err, color = 'Orange', fmt='s')

  #--------------------------------
  # Add points from MESA GYRE grid to show the track with
  # available seismic information
  #--------------------------------
  if list_dic_mesa_gyre:
    n_list_dic_mesa_gyre = len(list_dic_mesa_gyre)
    for i_dic in range(n_list_dic_mesa_gyre):
      which_dic = list_dic_mesa_gyre[i_dic]
      model_Teff = which_dic['Teff']
      model_logg = which_dic['log_g']
      ax.scatter([model_Teff,], [model_logg,], 10, color='red')

  #--------------------------------
  # Annotations, Texts and Legend
  #--------------------------------
  if list_dic_annot is not None:
    pcm.add_annotation(ax, list_dic_annot)

  #--------------------------------
  # Set Plot Name, and Finalize the Plot
  #--------------------------------
  if not file_out:
    track_srch_str = param.gen_track_srch_str()['track_srch_str']
    file_out = param.gen_path_plot() + 'Kiel-' + track_srch_str + '.pdf'
  fig.savefig(file_out, dpi = 600)
  print ' - Kiel Diagram stored at: %s \n' % (file_out, )
  plt.close()

  return None


#=================================================================================================================
def plot_grid_hrd(list_dic, list_lbls=None, list_dic_stars=None, log_Teff_from=None, log_Teff_to=None,
                  log_L_from=None, log_L_to=None, file_out=None):
  """
  HR Diagram for a grid of MESA tracks.
  @param list_dic: list of dictionaries, where each dictionary contains all *.hist information
         Values for each of dictionary key is either a single integer/float/string or
         a 1D numpy array.
  @type list_dic: list
  @param list_lbls: list of labels, where each label corresponds to one track in list_hist
  @type list_lbls: list of strings
  @param list_dic_stars: list of dictionaries with stars loaded from mesa_gyre.stars
  @type list_dic_stars: list of dictionaries
  @param log_Teff_from, log_Teff_to: left and right values for log_Teff on the xaxis.
  @type log_Teff_from, log_Teff_to: float
  @param log_L_from, log_g_to: lower and upper limits on log luminosity to display on the yaxis.
  @type log_L_from, log_g_to: float
  @param file_out: full path to the output HR diagram pdf file
  @type file_out: string
  @return: None
  @rtype: None
  """

  n_tracks = len(list_dic)
  if n_tracks == 0:
    print 'Error: plot_grid_hrd: plot_hrd: Input list is empty'
    raise SystemExit, 0
  if n_tracks ==1:
    plot_single_hrd(list_dic[0]) # passes the only dictionary to plot a single HRD

  hist_sample = list_dic[0]['hist']
  hist_sample_names = hist_sample.dtype.names
  dic_avail_keys = {}
  for key in hist_sample_names:
    dic_avail_keys[key] = 0.0
  required_keys = ['initial_mass', 'initial_z', 'log_Teff', 'photosphere_L', 'log_L',
                   'surf_avg_v_rot', 'surf_avg_v_crit', 'log_g']
  dic_keys = rd.check_key_exists(dic_avail_keys, required_keys)
  flag_initial_mass = dic_keys['initial_mass']
  flag_initial_z    = dic_keys['initial_z']
  flag_log_Teff     = dic_keys['log_Teff']
  flag_phot_L       = dic_keys['photosphere_L']
  flag_log_L        = dic_keys['log_L']
  flag_log_g        = dic_keys['log_g']
  flag_surf_v_rot   = dic_keys['surf_avg_v_rot']
  flag_surf_v_crit  = dic_keys['surf_avg_v_crit']

  if not flag_log_Teff or (not flag_log_L and not flag_phot_L):
    print 'HRD requires L and Teff. '
    print 'log_Teff:', flag_log_Teff, 'photosphere_L:', flag_phot_L, ' and log_L: ', flag_log_L
    message = 'Error: plot_hrd: plot_single_hrd: Exit'
    raise SystemExit, message

  #--------------------------------
  # Prepare For Plotting
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=160)
  axes([0.11, 0.12, 0.87, 0.87])
  plt.xlabel(r'$\log$' +r'$\, T_{\rm eff}$')
  plt.ylabel(r'$\log$' +r'$\, (L/L_\odot)$')

  #--------------------------------
  # Read the grid iteratively
  #--------------------------------
  i_track = 0
  tracks_min_log_Teff = np.zeros(n_tracks)
  tracks_max_log_Teff = np.zeros(n_tracks)
  tracks_min_log_L    = np.zeros(n_tracks)
  tracks_max_log_L    = np.zeros(n_tracks)

  dic_param = param.param_to_str()
  flag_mass_all = dic_param['mass_all']
  flag_eta_all  = dic_param['eta_all']
  flag_ov_all   = dic_param['ov_all']
  flag_z_all    = dic_param['z_all']

  for dic in list_dic:
    dic_track = dic['hist']
    mxg = 0
    if flag_log_g:
      log_g = dic_track['log_g']
      max_g = log_g.max()
      mxg = np.argmax(log_g)
    log_Teff = dic_track['log_Teff'][mxg:]
    n_lines = len(log_Teff)
    if flag_phot_L: log_L = np.log10(dic_track['photosphere_L'])[mxg:]
    if flag_log_L: log_L = dic_track['log_L'][mxg:]
    if flag_initial_mass: m_ini = dic_track['initial_mass']
    if flag_initial_z:    z_ini = dic_track['initial_z']
    if flag_surf_v_rot:  v_rot = dic_track['surf_avg_v_rot'][mxg:]
    if flag_surf_v_crit: v_cri = dic_track['surf_avg_v_crit'][mxg:]

    tracks_min_log_Teff[i_track] = log_Teff.min()
    tracks_max_log_Teff[i_track] = log_Teff.max()
    tracks_min_log_L[i_track] = log_L.min()
    tracks_max_log_L[i_track] = log_L.max()

    # Find appropriate on-screen text based on search param
    leg_label = ''
    if flag_initial_z and flag_z_all and not flag_mass_all:
      leg_label = r'$Z=$' + r'$' + '{:.3f}'.format(z_ini) + '$'
    #if flag_eta_all and not flag_mass_all:
      #leg_label = r'$\eta_{rot}$' + r'$' + '{:.2f}'.format()

    # Now, plot the track
    if list_lbls:
      plt.plot(log_Teff, log_L, linewidth = 2, linestyle='-', color='gray', label = list_lbls[i_track], zorder=1)
    else:
      plt.plot(log_Teff, log_L, linewidth = 2, linestyle='-', color='gray', zorder=1)
    plt.scatter([log_Teff[0],], [log_L[0],], 50, color='purple', zorder=1)

    i_track += 1

  #--------------------------------
  # Final Plot Specifications
  #--------------------------------
  max_log_Teff = tracks_max_log_Teff.max()
  min_log_Teff = tracks_min_log_Teff.min()
  max_log_L    = tracks_max_log_L.max()
  min_log_L    = tracks_min_log_L.min()
  if log_Teff_from: max_log_Teff = log_Teff_from
  if log_Teff_to:   min_log_Teff = log_Teff_to
  if log_L_from:    min_log_L    = log_L_from
  if log_L_to:      max_log_L    = log_L_to
  xlim(max_log_Teff, min_log_Teff)
  ylim(min_log_L, max_log_L)

  #--------------------------------
  # Include Observed Stars
  #--------------------------------
  if list_dic_stars is not None:
    n_dic_stars = len(list_dic_stars)
    for i_dic, dic_star in enumerate(list_dic_stars):
      names = dic_star.keys()
      star_log_Teff = dic_star['log_Teff']
      star_log_Teff_err = dic_star['err_log_Teff_1s']
      star_log_L = dic_star['log_L']
      star_log_L_err = dic_star['err_log_L_1s']

      if 'marker' in names: marker = dic_star['marker']
      else: marker = 's'
      if 'ms' in names: ms = dic_star['ms']
      else: ms = 10
      if 'mfc' in names: mfc = dic_star['mfc']
      else: mfc = 'navy'
      if 'ecolor' in names: ecolor = dic_star['ecolor']
      else: ecolor = mfc
      if 'mec' in names: mec = dic_star['mec']
      else: mec = 'purple'

      #plt.errorbar(star_log_Teff, star_log_L, xerr=star_log_Teff_err, yerr=star_log_L_err, ecolor=ecolor,
                  #elinewidth=2, capsize=5, marker=marker, mfc=mfc, mec=mec, ms=ms, zorder=2,
                  #label=r''+dic_star['name'][0])
      plt.errorbar(star_log_Teff, star_log_L, xerr=3.*star_log_Teff_err,
                   yerr=[[3.*dic_star['er_L_l']], [3.*dic_star['er_L_u']]], ecolor=ecolor,
                  elinewidth=2, capsize=5, marker=marker, mfc=mfc, mec=mec, ms=ms, zorder=2,
                  label=r''+dic_star['name'][0])
    if n_dic_stars <=10: leg_star = plt.legend(loc=4, fontsize='small')

  #--------------------------------
  # Annotations, Texts and Legend
  #--------------------------------
  if list_lbls:
    leg = plt.legend(loc=2, shadow=True)

  left = 0.06; top = 0.94; lower = 0.045
  #txt_mass = gen_plot_srch_param_text('mass_arr', r'$M/M_\odot$', '{:.1f}')
  #text(left, top-0*lower, txt_mass, ha='left', va='center', color="Black", alpha=1.0,
         #transform=gca().transAxes, fontsize=12, clip_on=True)
  #txt_eta  = gen_plot_srch_param_text('eta_arr', r'$\eta_{\rm rot}$', '{:.2f}')
  #text(left, top-1*lower, txt_eta, ha='left', va='center', color="Black", alpha=1.0,
         #transform=gca().transAxes, fontsize=12, clip_on=True)
  #txt_ov   = gen_plot_srch_param_text('ov_arr', r'$f_{\rm ov}$', '{:.3f}')
  #text(left, top-2*lower, txt_ov, ha='left', va='center', color="Black", alpha=1.0,
         #transform=gca().transAxes, fontsize=12, clip_on=True)
  #txt_z    = gen_plot_srch_param_text('z_arr', r'$Z$', '{:.3f}')
  #text(left, top-3*lower, txt_z, ha='left', va='center', color="Black", alpha=1.0,
         #transform=gca().transAxes, fontsize=12, clip_on=True)

  #legend(loc='upper left')


  #--------------------------------
  # Set Plot Name, and Finalize the Plot
  #--------------------------------
  if not file_out:
    track_srch_str = param.gen_track_srch_str()['track_srch_str']
    file_out = param.gen_path_plot() + 'HRD-' + track_srch_str + '.pdf'
  print '   HRD Grid plot stored at: ', file_out
  fig.savefig(file_out, dpi = 160)
  plt.close()

  return

#=================================================================================================================
def plot_single_hrd(recarr):
  """
  HR Diagram for a single track with on-plot details of the choices of
  initial mass, rotation rate w.r.t critical, overshooting, semi-convection
  and metalicity.
  @param recarr: record array with keys taken from the MESA hist file header and columns
  @type recarr: numpy record array
  @return: None
  @rtype: None
  """

  required_keys = ['initial_mass', 'initial_z', 'log_Teff', 'photosphere_L', 'log_L',
                   'surf_avg_v_rot', 'surf_avg_v_crit', 'log_g']
  avail_keys = recarr.dtype.names
  dic_avail_keys = {}
  for key in avail_keys:
    dic_avail_keys[key] = 0.0
  dic_keys = rd.check_key_exists(dic_avail_keys, required_keys)
  flag_initial_mass = dic_keys['initial_mass']
  flag_log_Teff = dic_keys['log_Teff']
  flag_phot_L   = dic_keys['photosphere_L']
  flag_log_L    = dic_keys['log_L']
  flag_log_g    = dic_keys['log_g']
  flag_surf_v_rot  = dic_keys['surf_avg_v_rot']
  flag_surf_v_crit = dic_keys['surf_avg_v_crit']

  if not flag_log_Teff or (not flag_log_L and not flag_phot_L):
    print 'HRD requires L and Teff. '
    print 'log_Teff:', flag_log_Teff, 'photosphere_L:', flag_phot_L, ' and log_L: ', flag_log_L
    print 'Error: plot_hrd: plot_single_hrd: Exit'
    raise SystemExit, 0

  mxg = 0                      # the index of maximum surface gravity. Marks the ZAMS
  if flag_log_g:
    log_g = recarr['log_g']
    max_g = log_g.max()
    mxg   = np.argmax(log_g)
  log_Teff = recarr['log_Teff'][mxg:]
  n_lines = len(log_Teff)
  if flag_phot_L: log_L = np.log10(recarr['photosphere_L'])[mxg:]
  if flag_log_L: log_L = recarr['log_L'][mxg:]
  if flag_initial_mass: m_ini = recarr['initial_mass']
  if flag_surf_v_rot: v_rot = recarr['surf_avg_v_rot'][mxg:]
  if flag_surf_v_crit: v_cri = recarr['surf_avg_v_crit'][mxg:]

  min_log_Teff = log_Teff.min()
  max_log_Teff = log_Teff.max()
  min_log_L    = log_L.min()
  max_log_L    = log_L.max()

  fig = plt.figure(figsize=(8,6), dpi=160)

  axes([0.10, 0.10, 0.88, 0.88])
  xlim(max_log_Teff+0.02, min_log_Teff-0.02)
  ylim(min_log_L-0.1, max_log_L+0.1)
  #xticks(np.arange(max_log_Teff, min_log_Teff, 0.05))
  #yticks(np.arange(min_log_L, max_log_L, 0.5))
  #ax.xaxis.set_major_locator()
  ylabel(r'Luminosity $\log(L/L_\odot)$')
  xlabel(r'Effective Temperature $\log T_{\rm eff}$')

  plt.plot(log_Teff, log_L, color='black', linewidth = 2, linestyle='-')
  plt.scatter([log_Teff[0],], [log_L[0],], 50, color='purple')
  #annot_txt = str(m_ini) + r'$M_{\odot}$'
  #plt.annotate(annot_txt, xy=(log_Teff[0],log_L[0]), xycoords='data', xytext=(-10,-20),
               #textcoords='offset points', fontsize = 10)
  #left = 0.08; top = 0.94; lower = 0.06
  #txt_rot = r'$\eta_{\rm rot} = $' + r'$'+'{:.2f}'.format(dic['eta_from'])+'$'
  #text(left, top, txt_rot, ha='left', va='center', color="Navy", alpha=1.0,
         #transform=gca().transAxes, fontsize=16, clip_on=True)
  #txt_ov  = r'$f_{\rm ov} = $' + r'$'+'{:.2f}'.format(dic['ov_from'])+'$'
  #text(left, top-lower, txt_ov, ha='left', va='center', color="Navy", alpha=1.0,
         #transform=gca().transAxes, fontsize=16, clip_on=True)
  #txt_sc  = r'$\alpha_{\rm sc} = $' + r'$'+'{:.2f}'.format(dic['sc_from'])+'$'
  #text(left, top-2*lower, txt_sc, ha='left', va='center', color="Navy", alpha=1.0,
         #transform=gca().transAxes, fontsize=16, clip_on=True)
  #txt_z   = r'$Z = $' + r'$'+'{:.3f}'.format(dic['z_from'])+'$'
  #text(left, top-3*lower, txt_z, ha='left', va='center', color="Navy", alpha=1.0,
         #transform=gca().transAxes, fontsize=16, clip_on=True)

  track_srch_str = param.gen_track_srch_str()['track_srch_str']
  #file_out = param.gen_path_plot() + 'HRD-' + track_srch_str + '.pdf'
  file_out = 'HRD-' + track_srch_str + '.pdf'
  print 'HRD plot stored at: ', file_out
  fig.savefig(file_out, dpi = 160)
  plt.close()

  return

#=================================================================================================================
def ekstrom(list_dic_rot, list_dic_nonrot, list_dic_stars=[], file_out=None):
  """
  Plot Evolutionary tracks based on Ekstrom et al. 2012, A&A
  """
  n_rot = len(list_dic_rot)
  n_nonrot = len(list_dic_nonrot)

  if n_rot == n_nonrot == 0:
    print 'Error: plot_hrd: ekstrom: Input lists are both empty'
    return None

  #--------------------------------
  # Prepare The Plot
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.12, right=0.98, bottom=0.12, top=0.97)

  if n_rot>0:
    for i_dic, dic in enumerate(list_dic_rot):
      fname   = dic['filename']
      fname   = fname[fname.rfind('/')+1 : fname.rfind('.')]
      fname   = fname[1:4]
      if 'p' in fname: fname = fname.replace('p', '.')
      M_ini   = np.float(fname)
      if not 1.2 <= M_ini < 3: continue
      hist    = dic['hist']
      log_Teff= hist['lg(Teff)']
      log_R   = hist['lg(L)']/2. - 2.*log_Teff + 2.*np.log10(5777.)
      log_g   = np.log10(hist['mass']) - 2. * log_R + np.log10(27542.29)

      Teff     = np.power(10, log_Teff)

      # ax.plot(Teff, log_g, linestyle='solid', lw=1.5, color='black')
      # ax.annotate('{0}'.format(M_ini), xy=(Teff[0]+0.01, log_g[0]+0.05), rotation=0, fontsize='small')

  if n_nonrot>0:
    for i_dic, dic in enumerate(list_dic_nonrot):
      fname   = dic['filename']
      fname   = fname[fname.rfind('/')+1 : fname.rfind('.')]
      fname   = fname[1:4]
      if 'p' in fname: fname = fname.replace('p', '.')
      M_ini   = np.float(fname)
      if not 1.2 <= M_ini < 3: continue
      hist    = dic['hist']
      log_Teff= hist['lg(Teff)']
      log_R   = hist['lg(L)']/2. - 2.*log_Teff + 2.*np.log10(5777.)
      log_g   = np.log10(hist['mass']) - 2. * log_R + np.log10(27542.29)

      Teff     = np.power(10, log_Teff)

      # ax.plot(Teff, log_g, linestyle='dashed', lw=1.5, color='green')

  #--------------------------------
  # Adding Instability Strips
  #--------------------------------
  if True:
    ax.plot([7520, 8600], [3.20, 4.30], linestyle='solid', color='blue', lw=2, label=r'$\delta$ Scuti Strip')
    ax.plot([7380, 6370], [4.32, 3.25], linestyle='solid', color='blue', lw=2)
    ax.plot([7534, 7400], [4.32, 3.90], linestyle='dashed', color='red', lw=2, label=r'$\gamma$ Dor Strip')
    ax.plot([7200, 6880], [4.32, 4.00], linestyle='dashed', color='red', lw=2)

  #--------------------------------
  # Annotations, Legend
  #--------------------------------
  ax.set_xlabel(r'Effective Temperature $T_{\rm eff}$ [K]')
  ax.set_xlim(9000, 6000)
  ax.set_ylabel(r'Surface Gravity $\log g$ [cgs]')
  ax.set_ylim(5.0, 2.5)

  if list_dic_stars:
    n_stars = len(list_dic_stars)
    for i_star, dic_star in enumerate(list_dic_stars):
      star_Teff         = dic_star['Teff']
      star_Teff_err   = dic_star['Teff_err']
      star_logg        = dic_star['log_g']
      star_logg_err  = dic_star['log_g_err']
      star_log_Teff_err = np.log10(star_Teff + star_Teff_err) - np.log10(star_Teff)

      ax.errorbar(star_Teff, star_logg, xerr=star_Teff_err, yerr=star_logg_err,
                  elinewidth=2, capsize=5, zorder=2, ecolor='grey'
                  # ecolor=ecolor, marker=marker, mfc=mfc, mec=mec, ms=ms,
                  # label=r''+dic_star['name'][0]
                  )
      num_lbl = str(i_star+1)
      ax.scatter([star_Teff], star_logg, s=100, color='grey', marker='o', zorder=3) #, label=num_lbl + ': KIC'+dic_star['name'])
      ax.annotate(num_lbl, xy=(star_Teff+25, star_logg+0.03), color='black', fontsize='small')

  # for legend:
  # ax.plot([-1], [-1], linestyle='solid', lw=3, color='black', label='Rotating')
  # ax.plot([-1], [-1], linestyle='dashed', lw=3, color='green', label='Non-Rotating')
  leg = ax.legend(loc=2, fontsize='small', scatterpoints=1)


  #--------------------------------
  # Finalize the Plot
  #--------------------------------
  if file_out:
    plt.savefig(file_out, dpi=200)
    print ' - plot_hrd: ekstrom: {0} saved'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def gen_plot_srch_param_text(str_arr, str_core, fmt):
  """
  For the matplotlib text, we generate appropriate and accurate information
  to be put on the plot, for these parameters: mass, eta_rot, f_ov, a_sc, Z.
  This depends on the requested querry which is set in commons.py
  @param str_arr: is the key name of the dictionary produced by set_srch_param();
                  e.g. to get the requested mass range, str_arr must be "mass_arr".
  @type str_arr: string
  @param str_core: is a LaTeX-type string to add to the text, e.g. it can be:
                 r"$M/M_\odot$"
  @type str_core: string
  @param fmt: is the format to output the string, e.g. '{:.3f}'.
  @type fmt: string
  @return:
  @rtype: string
  """

  dic = cm.set_srch_param()

  arr = dic[str_arr]
  n_arr = len(arr)

  if n_arr == 1:
    str_val = r'$' + fmt.format(arr[0]) + '$'
    txt = str_core + r'$ = $' + str_val

  if n_arr == 2:
    str_from = r'$' + fmt.format(arr[0]) + '$'
    str_to   = r'$' + fmt.format(arr[1]) + '$'
    str_leq  = r'$ \leq $'
    txt = str_core + r'$ = $' + str_from + ' and ' + str_to

  if n_arr > 1:
    str_from = r'$' + fmt.format(arr[0]) + '$'
    str_to   = r'$' + fmt.format(arr[n_arr-1]) + '$'
    str_leq  = r'$ \leq $'
    txt = str_from + str_leq + str_core + str_leq + str_to

  return txt

#=================================================================================================================
def plot_Kiel_comp_two_tracks(ref_tracks=[], comp_tracks=[], ref_names=[], comp_names=[],
                              log_Teff_from=None, log_Teff_to=None, log_g_from=None, log_g_to=None, file_out=None):
  """
  Compare two (sets) of tracks on the Kiel diagram. E.g. one set can be with exponential overshooting, and the other
  with the step overshooting.
  @param ref_tracks: list of dictionaries of "Reference" history file(s). They are plotted with solid linestyle
  @type ref_tracks: list of dictionaries
  @param comp_tracks: list of dictionaries of "Comparison" history file(s). They are plotted with dashed linestyle
  @type comp_tracks: list of dictionaries
  @param logTeff_from, logTeff_to: the range of logTeff on the x-axis; defalt = None
  @type logTeff_from, logTeff_to: float
  @param logg_from, logg_to: the range of log_g on the y-axis; defalt = None
  @type logg_from, logg_to: float
  @param file_out: full path to the output plot file
  @type file_out: string
  @param ref_names, comp_names: per each input dictionary in ref_tracks/comp_tracks, one name is provided to later put
      on the plot as legend.
  @type ref_names, comp_names: list of strings
  @return: None
  @rtype: None
  """
  if not ref_tracks or not comp_tracks:
    message = 'Error: plot_hrd: plot_Kiel_comp_two_tracks: One of input lists is empty!'
    raise SystemExit, message

  if not file_out:
    message = 'Error: plot_hrd: plot_Kiel_comp_two_tracks: Input file_out is None'
    raise SystemExit, message

  #--------------------------------
  # Define th plot panels
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(bottom=0.12, right=0.98, top=0.98)
  ax.set_xlabel(r'Effective Temperature $\log T_{\rm eff}$')
  ax.set_ylabel(r'Surface Gravity $\log g$ [cgs]')

  min_log_Teff = 1e99; max_log_Teff = -1e99
  min_log_g    = 1e99; max_log_g    = -1e99
  for i_dic, ref_dic in enumerate(ref_tracks):
    hist = ref_dic['hist']
    log_Teff = hist['log_Teff']
    ref_log_Teff = log_Teff
    log_g    = hist['log_g']
    ref_logg = log_g
    Xc       = hist['center_h1']
    if ref_names:
      ax.plot(log_Teff, log_g, color='black', linestyle='solid', lw=6, label=ref_names[i_dic], zorder=1)
    else:
      ax.plot(log_Teff, log_g, color='black', linestyle='solid', lw=6, zorder=1)

    if np.min(log_Teff) < min_log_Teff: min_log_Teff = np.min(log_Teff)
    if np.max(log_Teff) > max_log_Teff: max_log_Teff = np.max(log_Teff)
    if np.min(log_g)    < min_log_g:    min_log_g    = np.min(log_g)
    if np.max(log_g)    > max_log_g:    max_log_g    = np.max(log_g)

  for i_dic, comp_dic in enumerate(comp_tracks):
    hist = comp_dic['hist']
    log_Teff = hist['log_Teff']
    log_g    = hist['log_g']
    if comp_names:
      ax.plot(log_Teff, log_g, color='gray', linestyle='dashed', lw=2, label=comp_names[i_dic], zorder=1)
    else:
      ax.plot(log_Teff, log_g, color='gray', linestyle='dashed', lw=2, zorder=1)

    if np.min(log_Teff) < min_log_Teff: min_log_Teff = np.min(log_Teff)
    if np.max(log_Teff) > max_log_Teff: max_log_Teff = np.max(log_Teff)
    if np.min(log_g)    < min_log_g:    min_log_g    = np.min(log_g)
    if np.max(log_g)    > max_log_g:    max_log_g    = np.max(log_g)

  #--------------------------------
  # Add three points at Xc=0.65, 0.35 and 0.05
  #--------------------------------
  if True:
    ind_Xc60 = np.argmin(np.abs(Xc - 0.65))
    ind_Xc35 = np.argmin(np.abs(Xc - 0.35))
    ind_Xc10 = np.argmin(np.abs(Xc - 0.05))
    ax.scatter([ref_log_Teff[ind_Xc60]], [ref_logg[ind_Xc60]], color='red', s=160, marker='o', edgecolors='white',
               label=r'$X_c=0.65$', zorder=2)
    # ax.scatter([ref_log_Teff[ind_Xc60]], [ref_logg[ind_Xc60]], color='white', s=10, marker='o', zorder=2)

    ax.scatter([ref_log_Teff[ind_Xc35]], [ref_logg[ind_Xc35]], color='red', edgecolors='white', s=160, marker='s',
               label=r'$X_c=0.35$', zorder=2)
    # ax.scatter([ref_log_Teff[ind_Xc35]], [ref_logg[ind_Xc35]], color='white', s=10, marker='o', zorder=2)

    ax.scatter([ref_log_Teff[ind_Xc10]], [ref_logg[ind_Xc10]], color='red', edgecolors='white', s=200, marker='*',
               label=r'$X_c=0.05$', zorder=2)
    # ax.scatter([ref_log_Teff[ind_Xc10]], [ref_logg[ind_Xc10]], color='white', s=10, marker='o', zorder=2)
    leg_sym = ax.legend(loc=4)

  #--------------------------------
  # Annotations
  #--------------------------------
  if ref_names and comp_names: leg = ax.legend(loc=4, fontsize='medium', scatterpoints=1)
  if not log_Teff_from: log_Teff_from = max_log_Teff + 0.05
  if not log_Teff_to:   log_Teff_to   = min_log_Teff - 0.05
  if not log_g_from:    log_g_from    = max_log_g + 0.05
  if not log_g_to:      log_g_to      = min_log_g - 0.05
  ax.set_xlim(log_Teff_from, log_Teff_to)
  ax.set_ylim(log_g_from, log_g_to)

  plt.savefig(file_out, dpi=200)
  print ' - plot_hrd: plot_Kiel_comp_two_tracks: {0} created. \n'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def get_font_dic(font = 'serif', clr = 'Black', sz = 16):
  """
  To provide a dictionary for your font of choice.
  @param font: specify the font name; available choices are
               'serif' and 'arial'. default = 'serif'
  @type font: string
  @param clr: color of the font. default = 'Black'
  @type clr: string
  @param sz: fontsize. default = 16
  @type sz: integer
  @return font_dic: a dictionary with the font family specifications
  @rtype font_dic: dictionary
  """

  if font == 'serif':
    font_dic = {'family':'serif', 'color':clr, 'weight':'normal', 'size':sz}
  if font == 'arial':
    font_dic = {'family':'arial', 'color':clr, 'weight':'normal', 'size':sz}

  return font_dic

#=================================================================================================================
def instability_strips(list_dic_hist, list_dic_gyre=[], list_lbls=None, list_dic_stars=None, ordinate='HRD',
                  log_Teff_from=None, log_Teff_to=None, log_L_from=None, log_L_to=None, file_out=None):
  """
  Instability Strips based on MESA and GYRE (non-adiabatic) models.
  @param list_dic_hist: list of dictionaries, where each dictionary contains all *.hist information
         Values for each of dictionary key is either a single integer/float/string or
         a 1D numpy array.
  @type list_dic_hist: list
  @param list_dic_gyre: list of dictionaries of GYRE output files from gyre_nad.
  @type list_dic_gyre: list of dictionaries
  @param list_lbls: list of labels, where each label corresponds to one track in list_hist
  @type list_lbls: list of strings
  @param list_dic_stars: list of dictionaries with stars loaded from mesa_gyre.stars
  @type list_dic_stars: list of dictionaries
  @param ordinate: The choice of the ordinate; default='HRD'. Possible options are:
         - HRD: The classical HR diagram, i.e. log_L on the ordinate
         - Kiel: The log_g sits on the ordinate
         - sHRD: The spectroscopic HR diagram (see Kudritzki and Langer, 2014)
  @param log_Teff_from, log_Teff_to: left and right values for log_Teff on the xaxis.
  @type log_Teff_from, log_Teff_to: float
  @param log_L_from, log_g_to: lower and upper limits on log luminosity to display on the yaxis.
  @type log_L_from, log_g_to: float
  @param file_out: full path to the output HR diagram pdf file
  @type file_out: string
  @return: None
  @rtype: None
  """
  n_tracks = len(list_dic_hist)
  if n_tracks == 0:
    print 'Error: instability_strips: plot_hrd: Input list is empty'
    raise SystemExit, 0
  n_gyre  = len(list_dic_gyre)
  if n_gyre == 0:
    print 'Error: plot_hrd: instability_strips: Input GYRE list is empty'
    raise SystemExit,  'Error: plot_hrd: instability_strips: Input GYRE list is empty'

  do_HRD  = ordinate == 'HRD'
  do_Kiel = ordinate == 'Kiel'
  do_sHRD = ordinate == 'sHRD'
  if not any([do_HRD, do_Kiel, do_sHRD]):
    print 'Error: plot_hrd: instability_strips: Wrong choice of ordinate'
    raise SystemExit, 0

  hist_sample = list_dic_hist[0]['hist']
  hist_sample_names = hist_sample.dtype.names
  dic_avail_keys = {}
  for key in hist_sample_names:
    dic_avail_keys[key] = 0.0
  required_keys = ['initial_mass', 'initial_z', 'log_Teff', 'photosphere_L', 'log_L',
                   'surf_avg_v_rot', 'surf_avg_v_crit', 'log_g']
  dic_keys = rd.check_key_exists(dic_avail_keys, required_keys)
  flag_initial_mass = dic_keys['initial_mass']
  flag_initial_z    = dic_keys['initial_z']
  flag_log_Teff     = dic_keys['log_Teff']
  flag_phot_L       = dic_keys['photosphere_L']
  flag_log_L        = dic_keys['log_L']
  flag_log_g        = dic_keys['log_g']
  flag_surf_v_rot   = dic_keys['surf_avg_v_rot']
  flag_surf_v_crit  = dic_keys['surf_avg_v_crit']

  if not flag_log_Teff or (not flag_log_L and not flag_phot_L):
    print 'HRD requires L and Teff. '
    print 'log_Teff:', flag_log_Teff, 'photosphere_L:', flag_phot_L, ' and log_L: ', flag_log_L
    message = 'Error: plot_hrd: instability_strips: Exit'
    raise SystemExit, message

  #--------------------------------
  # Prepare For Plotting
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(6,4), dpi=300)
  fig.subplots_adjust(left=0.13, right=0.98, bottom=0.12, top=0.97)
  ax.set_xlabel(r'$\log$' +r'$\, T_{\rm eff}$')
  if do_HRD:
    ax.set_ylabel(r'$\log$' +r'$\, (L/L_\odot)$')
  if do_sHRD:
    ax.set_ylabel(r'$\log (T_{\rm eff}^4/g)\, -\, \log (T_{\rm eff}^4/g)_\odot$')
  if do_Kiel:
    ax.set_ylabel(r'$\log g$ [cgs]')

  #--------------------------------
  # Read the history info iteratively
  #--------------------------------
  i_track = 0
  tracks_min_log_Teff = np.zeros(n_tracks)
  tracks_max_log_Teff = np.zeros(n_tracks)
  tracks_min_log_L    = np.zeros(n_tracks)
  tracks_max_log_L    = np.zeros(n_tracks)

  dic_param = param.param_to_str()
  flag_mass_all = dic_param['mass_all']
  flag_eta_all  = dic_param['eta_all']
  flag_ov_all   = dic_param['ov_all']
  flag_z_all    = dic_param['z_all']

  for dic in list_dic_hist:
    dic_track = dic['hist']
    mxg = 0
    if flag_log_g:
      log_g = dic_track['log_g']
      max_g = log_g.max()
      mxg   = np.argmax(log_g)
    log_g   = log_g[mxg:]
    log_Teff = dic_track['log_Teff'][mxg:]
    n_lines = len(log_Teff)
    log_T4_g= 4.0 * log_Teff - log_g - (4.0 * np.log10(5777) - logg_sun)
    if flag_phot_L: log_L = np.log10(dic_track['photosphere_L'])[mxg:]
    if flag_log_L: log_L = dic_track['log_L'][mxg:]
    if flag_initial_mass: m_ini = dic_track['initial_mass']
    if flag_initial_z:    z_ini = dic_track['initial_z']
    if flag_surf_v_rot:  v_rot = dic_track['surf_avg_v_rot'][mxg:]
    if flag_surf_v_crit: v_cri = dic_track['surf_avg_v_crit'][mxg:]

    tracks_min_log_Teff[i_track] = log_Teff.min()
    tracks_max_log_Teff[i_track] = log_Teff.max()
    tracks_min_log_L[i_track] = log_L.min()
    tracks_max_log_L[i_track] = log_L.max()

    # Find appropriate on-screen text based on search param
    leg_label = ''
    if flag_initial_z and flag_z_all and not flag_mass_all:
      leg_label = r'$Z=$' + r'$' + '{:.3f}'.format(z_ini) + '$'
    #if flag_eta_all and not flag_mass_all:
      #leg_label = r'$\eta_{rot}$' + r'$' + '{:.2f}'.format()

    # Now, plot the track
    if do_HRD:    yvals = log_L
    if do_sHRD:   yvals = log_T4_g
    if do_Kiel:   yvals = log_g
    lbl                 = ''
    if list_lbls: lbl   = list_lbls[i_track] 
    ax.plot(log_Teff, yvals, linewidth = 0.5, linestyle='-', color='gray', alpha=0.5, label=lbl, zorder=1)
    # ax.scatter([log_Teff[0],], [log_L[0],], 50, color='purple', zorder=1)

    i_track += 1

  #--------------------------------
  # Plot a point for each GYRE output
  # file, and assign color, symbol to
  # each point based on the number of 
  # excited modes.
  #--------------------------------
  list_log_Teff = np.zeros(n_gyre)
  list_log_L    = np.zeros(n_gyre)
  list_log_g    = np.zeros(n_gyre)
  list_log_T4_g = np.zeros(n_gyre)
  flag_l_0      = []
  flag_l_1_p    = []
  flag_l_1_g    = []
  flag_l_2_p    = []
  flag_l_2_g    = []
  l_0_size      = np.zeros(n_gyre)
  l_1_p_size    = np.zeros(n_gyre)
  l_1_g_size    = np.zeros(n_gyre)
  l_2_p_size    = np.zeros(n_gyre)
  l_2_g_size    = np.zeros(n_gyre)
  l_1_order     = np.zeros(n_gyre) + 4   # default to zorder=4
  l_2_order     = np.zeros(n_gyre) + 6   # default to zorder=6

  for i_gyre, dic in enumerate(list_dic_gyre):
    M_star  = dic['M_star']
    R_star  = dic['R_star']
    L_star  = dic['L_star']
    log_L   = np.log10(L_star/L_sun)
    logg    = np.log10(c_grav*M_star/R_star**2) #- logg_sun
    log_R   = np.log10(R_star/Rsun)
    logTeff = np.log10(5777.) + log_L/4 - log_R/2
    logT4g  = 4.0 * logTeff - logg - (4.0 * np.log10(5777.0) - logg_sun)

    list_log_Teff[i_gyre] = logTeff
    list_log_L[i_gyre]    = log_L
    list_log_g[i_gyre]    = logg
    list_log_T4_g[i_gyre] = logT4g

    ind_l_0   = np.where((dic['l'] == 0) & (dic['W']>0))[0]
    ind_l_1_p = np.where((dic['l'] == 1) & (dic['W']>0) & (dic['n_pg']>=0))[0]
    ind_l_1_g = np.where((dic['l'] == 1) & (dic['W']>0) & (dic['n_pg']<0))[0]
    ind_l_2_p = np.where((dic['l'] == 2) & (dic['W']>0) & (dic['n_pg']>=0))[0]
    ind_l_2_g = np.where((dic['l'] == 2) & (dic['W']>0) & (dic['n_pg']<0))[0]

    if len(ind_l_0) > 0: flag_l_0.append(i_gyre)
    if len(ind_l_1_p) > 0: flag_l_1_p.append(i_gyre)
    if len(ind_l_1_g) > 0: flag_l_1_g.append(i_gyre)
    if len(ind_l_2_p) > 0: flag_l_2_p.append(i_gyre)
    if len(ind_l_2_g) > 0: flag_l_2_g.append(i_gyre)
    
    n_l_0   = len(ind_l_0)
    n_l_1_p = len(ind_l_1_p)
    n_l_1_g = len(ind_l_1_g)
    n_l_2_p = len(ind_l_2_p)
    n_l_2_g = len(ind_l_2_g)

    if n_l_0 == 0: 
      l_0_size[i_gyre] = 0
    else:
      l_0_size[i_gyre] = 2 * n_l_0
    if n_l_1_p == 0: 
      l_1_p_size[i_gyre] = 0
    else:
      l_1_p_size[i_gyre] = 2 * n_l_1_p
    if n_l_1_g == 0: 
      l_1_g_size[i_gyre] = 0
    else:
      l_1_g_size[i_gyre] = 2 * n_l_1_g
    if n_l_2_p == 0: 
      l_2_p_size[i_gyre] = 0
    else:
      l_2_p_size[i_gyre] = 2 * n_l_2_p
    if n_l_2_g == 0: 
      l_2_g_size[i_gyre] = 0
    else:
      l_2_g_size[i_gyre] = 2 * n_l_2_g

  xvals = list_log_Teff
  if do_HRD:
    yvals = list_log_L
  elif do_Kiel:
    yvals = list_log_g
  elif do_sHRD:
    yvals = list_log_T4_g
  else:
    print 'Error: plot_hrd: instability_strips: something is wrong with the choice of ordinate'
    raise SystemExit, 0

  ax.scatter(xvals, yvals, alpha=0.5, s=1, marker='o', color='grey', zorder=1)
  ax.scatter(xvals[flag_l_0], yvals[flag_l_0], alpha=0.5, s=l_0_size, marker='o', facecolors='blue', edgecolors='blue', zorder=2)
  ax.scatter(xvals[flag_l_1_g], yvals[flag_l_1_g], alpha=0.5, s=l_1_g_size, marker='s', facecolors='white', edgecolors='red', zorder=3)
  ax.scatter(xvals[flag_l_1_p], yvals[flag_l_1_p], alpha=0.5, s=l_1_p_size, marker='s', facecolors='white', edgecolors='blue', zorder=3)
  # ax.scatter(xvals[flag_l_2_p], yvals[flag_l_2_p], alpha=0.5, s=l_2_p_size, marker='^', facecolors='yellow', edgecolors='yellow', zorder=4)
  # ax.scatter(xvals[flag_l_2_g], yvals[flag_l_2_g], alpha=0.5, s=l_2_g_size, marker='^', facecolors='yellow', edgecolors='yellow', zorder=4)


  #--------------------------------
  # Final Plot Specifications
  #--------------------------------
  max_log_Teff = tracks_max_log_Teff.max()
  min_log_Teff = tracks_min_log_Teff.min()
  max_log_L    = tracks_max_log_L.max()
  min_log_L    = tracks_min_log_L.min()
  if log_Teff_from: max_log_Teff = log_Teff_from
  if log_Teff_to:   min_log_Teff = log_Teff_to
  if log_L_from:    min_log_L    = log_L_from
  if log_L_to:      max_log_L    = log_L_to
  ax.set_xlim(max_log_Teff, min_log_Teff)
  ax.set_ylim(min_log_L, max_log_L)

  #--------------------------------
  # Include Observed Stars
  #--------------------------------
  if list_dic_stars is not None:
    n_dic_stars = len(list_dic_stars)
    for i_dic, dic_star in enumerate(list_dic_stars):
      names = dic_star.keys()
      star_log_Teff = dic_star['log_Teff']
      star_log_Teff_err = dic_star['err_log_Teff_1s']
      star_log_L = dic_star['log_L']
      star_log_L_err = dic_star['err_log_L_1s']

      if 'marker' in names: marker = dic_star['marker']
      else: marker = 's'
      if 'ms' in names: ms = dic_star['ms']
      else: ms = 10
      if 'mfc' in names: mfc = dic_star['mfc']
      else: mfc = 'navy'
      if 'ecolor' in names: ecolor = dic_star['ecolor']
      else: ecolor = mfc
      if 'mec' in names: mec = dic_star['mec']
      else: mec = 'purple'

      #ax.errorbar(star_log_Teff, star_log_L, xerr=star_log_Teff_err, yerr=star_log_L_err, ecolor=ecolor,
                  #elinewidth=2, capsize=5, marker=marker, mfc=mfc, mec=mec, ms=ms, zorder=2,
                  #label=r''+dic_star['name'][0])
      ax.errorbar(star_log_Teff, star_log_L, xerr=3.*star_log_Teff_err,
                   yerr=[[3.*dic_star['er_L_l']], [3.*dic_star['er_L_u']]], ecolor=ecolor,
                  elinewidth=2, capsize=5, marker=marker, mfc=mfc, mec=mec, ms=ms, zorder=2,
                  label=r''+dic_star['name'][0])
    if n_dic_stars <=10: leg_star = ax.legend(loc=4, fontsize='small')

  #--------------------------------
  # Annotations, Texts and Legend
  #--------------------------------
  if list_lbls:
    leg = ax.legend(loc=2, shadow=True)

  left = 0.06; top = 0.94; lower = 0.045
  #txt_mass = gen_plot_srch_param_text('mass_arr', r'$M/M_\odot$', '{:.1f}')
  #text(left, top-0*lower, txt_mass, ha='left', va='center', color="Black", alpha=1.0,
         #transform=gca().transAxes, fontsize=12, clip_on=True)
  #txt_eta  = gen_plot_srch_param_text('eta_arr', r'$\eta_{\rm rot}$', '{:.2f}')
  #text(left, top-1*lower, txt_eta, ha='left', va='center', color="Black", alpha=1.0,
         #transform=gca().transAxes, fontsize=12, clip_on=True)
  #txt_ov   = gen_plot_srch_param_text('ov_arr', r'$f_{\rm ov}$', '{:.3f}')
  #text(left, top-2*lower, txt_ov, ha='left', va='center', color="Black", alpha=1.0,
         #transform=gca().transAxes, fontsize=12, clip_on=True)
  #txt_z    = gen_plot_srch_param_text('z_arr', r'$Z$', '{:.3f}')
  #text(left, top-3*lower, txt_z, ha='left', va='center', color="Black", alpha=1.0,
         #transform=gca().transAxes, fontsize=12, clip_on=True)

  #legend(loc='upper left')

  # Show size scaling symbols
  ax.scatter([4.75], [4.40], s=2, marker='s', color='blue', facecolors='white')
  ax.scatter([4.75], [4.20], s=20, marker='s', color='blue', facecolors='white')
  ax.scatter([4.75], [4.00], s=40, marker='s', color='blue', facecolors='white')
  ax.annotate('1', xy=(4.72, 4.36), xycoords='data', fontsize='x-small')
  ax.annotate('10', xy=(4.72, 4.16), xycoords='data', fontsize='x-small')
  ax.annotate('20', xy=(4.72, 3.96), xycoords='data', fontsize='x-small')

  ax.scatter([], [], alpha=0.5, s=25, marker='o', color='grey', zorder=1, label=r'All Stable')
  ax.scatter([], [], alpha=0.5, s=25, marker='o', facecolors='blue', edgecolors='blue', zorder=2, label=r'p$_0$')
  ax.scatter([], [], alpha=0.5, s=25, marker='s', facecolors='white', edgecolors='blue', zorder=3, label=r'p$_1$')
  ax.scatter([], [], alpha=0.5, s=25, marker='s', facecolors='white', edgecolors='red', zorder=3, label=r'g$_1$')
  ax.scatter([], [], alpha=0.5, s=25, marker='^', facecolors='white', edgecolors='yellow', zorder=4, label=r'p$_2$')
  ax.scatter([], [], alpha=0.5, s=25, marker='^', facecolors='white', edgecolors='yellow', zorder=4, label=r'g$_2$')
  leg = ax.legend(loc=3, fontsize='medium', scatterpoints=1, shadow=True)



  #--------------------------------
  # Set Plot Name, and Finalize the Plot
  #--------------------------------
  fig.savefig(file_out, dpi = 160)
  print '   plot_hrd: instability_strips: plot saved in: {0}'.format(file_out)
  plt.close()

  return

#=================================================================================================================





"""
This module collects observational information for a subset of stars that we confront with
the MESA-GYRE grid.
For each star, we list all observed information with their 1sigma uncertainty if avaliable.
"""
# This Python file uses the following encoding: utf-8

import logging
import numpy as np

#=================================================================================================================
def all_stars():
  """
  This function returns a list of dictionary for all stars with carefully measured evolutionary and seismic properties.
  """

  list_stars = [theta_Oph(), nu_Eri(), V1449_Aqu(), Lac_12(), gamma_Peg(), 
                delta_Cet(), delta_Lup(), beta_Cen(), eps_Cen(),
                sigma_Sco_primary(), sigma_Sco_secondary(), Spica_primary(),
                Spica_secondary(), V380Cyg_primary(), V380Cyg_secondary(),
                HD43317(), HD50230(), HD160124(), HD48977(), HD170580(),
                HD129929(), HD16582(), HD46202(), HD44743(), HD163472(), 
                HD214993(), 
                KIC_10526294(), KIC_7760680(), EPIC202060092(),
                EPIC202061162(), EPIC202061197(), EPIC202061199(), EPIC202061205(),
                EPIC202061207(), EPIC202061208(), EPIC202061226()]

  return list_stars

#=================================================================================================================
def dic_default():

  dic = {}

  dic['name'] = [r'']
  dic['sp_type'] = ''
  dic['var_type'] = ''
  dic['binary'] = False
  dic['B_kG'] = 0   # magnetic field in kilo Gauss
  dic['err_B_kG'] = 0
  dic['PI'] = 'Ehsan Moravveji'
  dic['Ref'] = ['']
  dic['url'] = [''] # ADS or Simbad links
  dic['Teff'] = 1
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 1
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 0
  dic['err_logg_1s'] = 0
  dic['log_L'] = 0
  dic['err_log_L_1s'] = 0
  dic['parallax'] = 0
  dic['err_parallax'] = 0
  dic['vsini'] = 0
  dic['err_vsini_1s'] = 0
  dic['f_rot'] = 0  # cycles_per_day
  dic['v_micro'] = 0
  dic['v_macro'] = 0
  dic['Ys'] = 0
  dic['err_Ys'] = 0
  dic['Xc'] = 0
  dic['err_Xc'] = 0
  dic['mass'] = 0
  dic['err_mass'] = 0
  dic['f_ov'] = 0
  dic['err_f_ov'] = 0
  dic['alpha_ov'] = 0
  dic['err_alpha_ov'] = 0
  dic['list_dic_freq'] = [{'freq':0, 'freq_err':0, 'freq_unit':'cd', 'l':0, 'm':0, 'pg':''}] # see HD50230 for an example
  dic['marker'] = 's'     # for plotting; marker symbol
  dic['ms'] = 10          # for plotting; marker size
  dic['mfc'] = 'navy'     # for plotting; marker face color
  dic['mec'] = 'purple'   # for plotting; marker edge color
  dic['ecolor'] = 'navy'  # for plotting; error bar color

  return dic

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================
def gamma_Peg():

  dic = dic_default()

  dic['name'] = [r'$\gamma$\,Peg', r'HD\,886']
  dic['sp_type'] = 'B2IV'
  dic['var_type'] = '$\beta$ Cep - SPB Hybrid'
  dic['binary'] = True
  dic['PI'] = 'Gerald Handler'
  dic['Ref'] = ['Walczak P. et al. (2013, MNRAS)', 
                'Handler G. et al. (2009, ApJL)']
  dic['url'] = ['http://mnras.oxfordjournals.org/content/432/1/822.full.pdf'] 
  dic['Teff'] = np.power(10, 4.325)
  dic['log_Teff'] = 4.325   # Taken from Walczak+13; Aerts (2013, EAS) gives 4.210
  # dic['err_Teff_1s'] = 1
  dic['err_log_Teff_1s'] = 0.026
  dic['logg'] = 4.15        # from Aerts (2013, EAS)
  dic['err_logg_1s'] = 0.15 # I just choose it myself
  dic['log_L'] = 3.744
  dic['err_log_L_1s'] = 0.090
  dic['parallax'] = 8.33
  dic['err_parallax'] = 0.53
  dic['vsini'] = 0         # this is seriously zero, with v_rot~3 km/s
  dic['err_vsini_1s'] = 1  # I just chose it myself
  dic['alpha_ov'] = 0.05
  dic['err_alpha_ov'] = 0.05
  dic['list_dic_freq'] = [{'freq':0, 'freq_err':0, 'freq_unit':'cd', 'l':0, 'm':0, 'pg':''}] # see HD50230 for an example
  dic['marker'] = 's'     # for plotting; marker symbol
  dic['ms'] = 10          # for plotting; marker size
  dic['mfc'] = 'navy'     # for plotting; marker face color
  dic['mec'] = 'purple'   # for plotting; marker edge color
  dic['ecolor'] = 'navy'  # for plotting; error bar color

  list_freq = []
  list_freq.append({'freq':0.63551, 'freq_err':0.00010, 'freq_unit':'cd', 'pg':'g'}) 
  list_freq.append({'freq':0.68241, 'freq_err':0.00007, 'freq_unit':'cd', 'pg':'g'}) 
  list_freq.append({'freq':0.73940, 'freq_err':0.00010, 'freq_unit':'cd', 'pg':'g'}) 
  list_freq.append({'freq':0.8352,  'freq_err':0.00030, 'freq_unit':'cd', 'pg':'g'}) 
  list_freq.append({'freq':0.88550, 'freq_err':0.00007, 'freq_unit':'cd', 'pg':'g'}) 
  list_freq.append({'freq':0.91442, 'freq_err':0.00011, 'freq_unit':'cd', 'pg':'g'}) 
  list_freq.append({'freq':6.01616, 'freq_err':0.00014, 'freq_unit':'cd', 'pg':'p'}) 
  list_freq.append({'freq':6.0273,  'freq_err':0.00050, 'freq_unit':'cd', 'pg':'p'}) 
  list_freq.append({'freq':6.5150,  'freq_err':0.00080, 'freq_unit':'cd', 'pg':'p'}) 
  list_freq.append({'freq':6.58974, 'freq_err':0.00002, 'freq_unit':'cd', 'pg':'p'}) 
  list_freq.append({'freq':6.9776,  'freq_err':0.00050, 'freq_unit':'cd', 'pg':'p'}) 
  list_freq.append({'freq':8.1861,  'freq_err':0.00080, 'freq_unit':'cd', 'pg':'p'}) 
  list_freq.append({'freq':8.552,   'freq_err':0.00200, 'freq_unit':'cd', 'pg':'p'}) 
  list_freq.append({'freq':9.1092,  'freq_err':0.00012, 'freq_unit':'cd', 'pg':'p'}) 

  dic['list_dic_freq'] = list_freq

  return dic 

#=================================================================================================================
def HD214993():

  dic = dic_default()

  dic['name'] = [r'HD\,214993']
  dic['PI'] = 'Ehsan Moravveji'
  dic['Ref'] = [r'Aerts (2013)']
  dic['Teff'] = np.power(10., 4.389)
  dic['log_Teff'] = 4.389
  dic['err_Teff_1s'] = 1500.   # assumed
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.65
  dic['err_logg_1s'] = 0.3  # assumed
  dic['vsini'] = 36
  dic['f_rot'] = 0.120  # cycles_per_day
  dic['Xc'] = 0.28
  dic['err_Xc'] = 0.08
  dic['mass'] = 12.2
  dic['alpha_ov'] = 0.20   # < 0.40
  dic['err_alpha_ov'] = 0.2 # arbitrary

  return dic

#=================================================================================================================
def HD163472():
  """
  This star has a magnetic field
  """

  dic = dic_default()

  dic['name'] = [r'HD\,163472']
  dic['PI'] = 'Maryline Briquet'
  dic['Ref'] = ['Briquet et al. (2012)']
  dic['Teff'] = np.power(10.0, 4.352)
  dic['log_Teff'] = 4.352
  dic['err_Teff_1s'] = 0.3 # assumed
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.95
  dic['err_logg_1s'] = 0.3  # assumed
  dic['vsini'] = 63
  dic['f_rot'] = 0.275  # cycles_per_day
  dic['Xc'] = 0.29
  dic['mass'] = 8.9
  dic['alpha_ov'] = 0.075  # < 0.15  based on Table 1 in Aerts (2013)
  dic['err_alpha_ov'] = 0.075 # arbitrary

  return dic

#=================================================================================================================
def HD44743():

  dic = dic_default()

  dic['name'] = [r'$\beta$\,CMa', r'HD\,44743']
  dic['sp_type'] = 'B1 II/III'
  dic['var_type'] = r'$\beta$ Cep'
  dic['PI'] = 'Anwesh Mazumdar'
  dic['Ref'] = ['Mazumdar et al. (2006)', 'Aerts (2013)']
  dic['Teff'] = np.power(10.0, 4.380)
  dic['log_Teff'] = 4.380
  dic['err_Teff_1s'] = 1500   # assumed
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.50
  dic['err_logg_1s'] = 0.3 # assumed
  dic['parallax'] = 6.62
  dic['err_parallax'] = 0.22
  dic['vsini'] = 23
  dic['f_rot'] = 0.054  # cycles_per_day
  dic['Xc'] = 0.12
  dic['mass'] = 13.6
  dic['alpha_ov'] = 0.20
  dic['err_alpha_ov'] = 0.05

  return dic

#=================================================================================================================
def delta_Cet():

  dic = dic_default()

  dic['name'] = [r'$\delta$\,Cet', r'HD\,16582']
  dic['sp_type'] = 'B2 IV'
  dic['var_type'] = r'$\beta$ Cep - SPB Hybrid'
  dic['PI'] = 'Conny Aerts'
  dic['Ref'] = ['Aerts et al. (2006)'] #
  dic['url'] = ['http://cdsads.u-strasbg.fr/cgi-bin/nph-bib_query?2006ApJ...642..470A&db_key=AST&nosetcookie=1']
  dic['Teff'] = np.power(10.0, 4.327)
  dic['log_Teff'] = 4.327
  dic['err_Teff_1s'] = 1500  # assumed
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.80
  dic['err_logg_1s'] = 0.30  # assumed
  dic['parallax'] = 5.02
  dic['err_parallax'] = 0.15
  dic['vsini'] = 1
  dic['err_vsini_1s'] = 0  # assumed
  dic['f_rot'] = 0.075  # cycles_per_day
  dic['Xc'] = 0.25
  dic['mass'] = 10.2
  dic['alpha_ov'] = 0.20
  dic['err_alpha_ov'] = 0.05

  return dic

#=================================================================================================================
def delta_Lup():

  dic = dic_default()

  dic['name'] = [r'$\delta\,$Lup']
  dic['sp_type'] = ''
  dic['var_type'] = r'$\beta\,$Cep - SPB Hybrid'
  dic['binary'] = False
  dic['PI'] = 'Andzrej Pigulski (BRITE)'
  dic['Ref'] = ['']
  dic['url'] = [''] # ADS or Simbad links
  dic['Teff'] = np.power(10, 4.34)
  dic['log_Teff'] = 4.34  # mean of logTeff=4.36 and Teff=20900 both from Vizier
  dic['err_Teff_1s'] = 1000  # assumed
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.88  # mean of 3.86 and 3.90 both from Vizier
  dic['err_logg_1s'] = 0.10  # assumed

  return dic

#=================================================================================================================
def beta_Cen():

  dic = dic_default()

  dic['name'] = [r'$\beta\,$Cen']
  dic['sp_type'] = ''
  dic['var_type'] = r'$\beta\,$Cep - SPB Hybrid'
  dic['binary'] = True
  dic['PI'] = 'Mario Ausseloos; Andzrej Pigulski (BRITE)'
  dic['Ref'] = ['Ausseloos et al. (2006, A&S)']
  dic['url'] = [''] 
  dic['Teff'] = 25000.0
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 2000  # see the discussion in Ausseloos et al. 
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.5  # see the discussion in Ausseloos et al. 
  dic['err_logg_1s'] = 0.40  # see the discussion in Ausseloos et al. 

  return dic

#=================================================================================================================
def eps_Cen():

  dic = dic_default()

  dic['name'] = [r'$\epsilon\,$Cen']
  dic['sp_type'] = ''
  dic['var_type'] = r'$\beta\,$Cep - SPB Hybrid'
  dic['binary'] = False
  dic['PI'] = 'Andzrej Pigulski (BRITE)'
  dic['Ref'] = ['']
  dic['url'] = [''] 
  dic['Teff'] = np.power(10, 4.37)
  dic['log_Teff'] = 4.37
  dic['err_Teff_1s'] = 1000  # assumed
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.68 
  dic['err_logg_1s'] = 0.20  # assumed

  return dic

#=================================================================================================================
def HD163830():

  dic = dic_default()

  dic['name'] = [r'HD\,163830']
  dic['sp_type'] = 'B5 II/III'
  dic['var_type'] = r'SPB'
  dic['PI'] = 'Conny Aerts'
  dic['Ref'] = ['Aerts et al. (2006)'] #
  dic['url'] = ['http://iopscience.iop.org/1538-4357/642/2/L165/pdf/1538-4357_642_2_L165.pdf']
  dic['Teff'] = 13700.0
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 500
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.79
  dic['err_logg_1s'] = 0.14
  dic['mass'] = 4.5   # +/- 0.4

  return dic

#=================================================================================================================
def HD129929():

  dic = dic_default()

  dic['name'] = [r'V836\,Cen', r'HD\,129929']
  dic['sp_type'] = 'B3 V'
  dic['var_type'] = r'$\beta$ Cep'
  dic['PI'] = 'Conny Aerts'
  dic['Ref'] = ['Aerts et al. (2003), Dupret et al. (2004)']
  dic['url'] = ['http://cdsads.u-strasbg.fr/cgi-bin/nph-bib_query?2004A%26A...415..241A&db_key=AST&nosetcookie=1',
                'http://cdsads.u-strasbg.fr/cgi-bin/nph-bib_query?2004A%26A...415..251D&db_key=AST&nosetcookie=1']
  dic['Teff'] = np.power(10, 4.35) # Dupret+05 give 22392.
  dic['log_Teff'] = 4.35   # Aerts+04
  dic['err_Teff_1s'] = 1000.  # assumed
  dic['err_log_Teff_1s'] = 0.02  # Aerts+04
  dic['logg'] = 3.90             # Aerts+04
  dic['err_logg_1s'] = 0.15      # assumed; Aerts+04 give 0.05
  dic['log_L'] = 3.8568
  dic['err_log_L_1s'] = 0.5      # assumed
  dic['parallax'] = -0.04
  dic['err_parallax'] = 0.71
  dic['vsini'] = 2
  dic['err_vsini_1s'] = 0
  dic['f_rot'] = 0.012  # cycles_per_day
  dic['Xc'] = 0.35
  dic['mass'] = 9.4
  dic['alpha_ov'] = 0.10
  dic ['err_alpha_ov'] = 0.05

  return dic

#=================================================================================================================
def HD170580():

  dic = dic_default()

  dic['name'] = [r'HD\,170580']
  dic['sp_type'] = 'B2 V'
  dic['binary'] = True
  dic['var_type'] = r'$\beta$ Cep - SPB Hybrid'
  dic['binary'] = False
  dic['PI'] = 'Conny Aerts - Anne Thoul'
  dic['Ref'] = ['']
  dic['url'] = [''] # ADS or Simbad links
  dic['Teff'] = 20000.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 1000.
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 4.10
  dic['err_logg_1s'] = 0.15 

  return dic 

#=================================================================================================================
def theta_Oph():

  dic = dic_default()

  dic['name'] = [r'$\theta$\,Oph', r'HD\,157056']
  dic['sp_type'] = 'B2 IV' # Simbad
  dic['var_type'] = r'$\beta$ Cep'
  dic['binary'] = True
  dic['PI'] = 'Maryline Briquet'
  dic['Ref'] = ['Lovekin & Goupil (2010), Briquet et al. (2007)', 'Handler et al. (2005)', 'Briquet et al. (2005)']
  dic['url'] = ['http://adsabs.harvard.edu/abs/2010A%26A...515A..58L',
                'http://adsabs.harvard.edu/abs/2005MNRAS.362..612H',
                'http://adsabs.harvard.edu/abs/2007MNRAS.381.1482B',
                'http://adsabs.harvard.edu/abs/2005MNRAS.362..619B']
  dic['Teff'] = 22260.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 280.
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.950
  dic['err_logg_1s'] = 0.006
  dic['log_L'] = 3.7346
  dic['err_log_L_1s'] = 0.05
  dic['parallax'] = 7.48
  dic['err_parallax'] = 0.17
  dic['vsini'] = 29.
  dic['err_vsini_1s'] = 7.
  dic['f_rot'] = 0.107
  dic['Xc'] = 0.38
  dic['err_Xc'] = 0.2
  dic['mass'] = 8.2
  dic['err_mass'] = 0.3
  dic['alpha_ov'] = 0.44
  dic['err_alpha_ov'] = 0.07

  list_freq = []
  list_freq.append({'freq':7.1160, 'freq_err':8e-5, 'freq_unit':'cd', 'l':2, 'm':-1})
  list_freq.append({'freq':7.2881, 'freq_err':5e-4, 'freq_unit':'cd', 'l':2, 'm':1})
  list_freq.append({'freq':7.3697, 'freq_err':3e-4, 'freq_unit':'cd', 'l':2, 'm':2})
  list_freq.append({'freq':7.4677, 'freq_err':3e-4, 'freq_unit':'cd', 'l':0, 'm':0})
  #list_freq.append({'freq':7.7659, 'freq_err':3e-4, 'freq_unit':'cd', 'l':1, 'm':-1})
  list_freq.append({'freq':7.8742, 'freq_err':5e-4, 'freq_unit':'cd', 'l':1, 'm':0})
  #list_freq.append({'freq':7.9734, 'freq_err':5e-4, 'freq_unit':'cd', 'l':1, 'm':1 })
  dic['list_dic_freq'] = list_freq

  return dic

#=================================================================================================================
def HD46202():

  dic = dic_default()

  dic['name'] = [r'HD\,46202']
  dic['sp_type'] = 'O9 V'
  dic['var_type'] = ' '
  dic['PI'] = 'Maryline Briquet'
  dic['Ref'] = ['Briquet et al. (2011), Nieva (2013)']
  dic['url'] = ['http://adsabs.harvard.edu/abs/2011A%26A...527A.112B']
  dic['Teff'] = 34100.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 400.
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 4.18
  dic['err_logg_1s'] = 0.05
  dic['log_L'] = 4.71
  dic['err_log_L_1s'] = 0.10
  dic['vsini'] = 25
  dic['err_vsini_1s'] = 7
  dic['v_micro'] = 6   # +/- 2 km/s from Nieva (2013)
  dic['v_macro'] = 15  # +/- 7 km/s from Nieva (2013)
  dic['Ys'] = 0.271
  dic['err_Ys'] = 0.033
  dic['mass'] = 19.6   # reference gives 19.6 but conny gives 24.1
  dic['err_mass'] = 0.8
  dic['alpha_ov'] = 0.10
  dic['err_alpha_ov'] = 0.05

  return dic

#=================================================================================================================
def HD50230():

  dic = dic_default()

  dic['name'] = [r'HD\,50230']
  dic['sp_type'] = 'B3V' # Simbad
  dic['var_type'] = r'SPB'
  dic['binary'] = True
  dic['PI'] = 'Peter Degroot'
  dic['Ref'] = ['Degroot et al. (2010, 2012)']
  dic['Teff'] = 18000.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 1500.
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.8
  dic['err_logg_1s'] = 0.3
  dic['log_L'] = 0
  dic['err_log_L_1s'] = 0
  dic['parallax'] = 0
  dic['err_parallax'] = 0
  dic['vsini'] = 7
  dic['err_vsini_1s'] = 0
  dic['f_rot'] = 0.044
  dic['v_micro'] = 0
  dic['v_macro'] = 0
  dic['Ys'] = 0
  dic['err_Ys'] = 0
  dic['Xc'] = 0.15
  dic['err_Xc'] = 0.05
  dic['mass'] = 7.5
  dic['err_mass'] = 0.3
  dic['f_ov'] = 0.027
  dic['err_f_ov'] = 0.005
  dic['alpha_ov'] = 0.27
  dic['err_alpha_ov'] = 0.05

  dic['marker'] = 's'     # for plotting; marker symbol
  dic['ms'] = 10          # for plotting; marker size
  dic['mfc'] = 'navy'     # for plotting; marker face color
  dic['mec'] = 'purple'   # for plotting; marker edge color
  dic['ecolor'] = 'navy'  # for plotting; error bar color

  list_freq = []

  if False:
    # Using the frequency list from Degrote et al. (2012, A&A)
    # The following form the series published by Degroote et al. (2010, Nature)
    # list_freq.append({'freq':0.63683, 'freq_err':0.05e-3, 'freq_unit':'cd', 'l':1, 'pg':'g'})
    list_freq.append({'freq':0.68440, 'freq_err':0.02e-3, 'freq_unit':'cd', 'l':1, 'pg':'g'})
    list_freq.append({'freq':0.73841, 'freq_err':0.02e-3, 'freq_unit':'cd', 'l':1, 'pg':'g'})
    list_freq.append({'freq':0.80413, 'freq_err':0.02e-3, 'freq_unit':'cd', 'l':1, 'pg':'g'})
    list_freq.append({'freq':0.87931, 'freq_err':0.02e-3, 'freq_unit':'cd', 'l':1, 'pg':'g'})
    list_freq.append({'freq':0.97367, 'freq_err':0.02e-2, 'freq_unit':'cd', 'l':1, 'pg':'g'})
    # list_freq.append({'freq':1.09234, 'freq_err':0.09e-3, 'freq_unit':'cd', 'l':1, 'pg':'g'})
    # list_freq.append({'freq':1.23685, 'freq_err':0.04e-3, 'freq_unit':'cd', 'l':1, 'pg':'g'})

    # # # Possible low-orger g- or p- modes
    list_freq.append({'freq':2.478892, 'freq_err':4.3000e-5, 'freq_unit':'cd', 'pg':'pg'})
    list_freq.append({'freq':2.677581, 'freq_err':8.9000e-5, 'freq_unit':'cd', 'pg':'pg'})
    list_freq.append({'freq':2.838247, 'freq_err':8.1000e-5, 'freq_unit':'cd', 'pg':'pg'})
    list_freq.append({'freq':3.670321, 'freq_err':1.2500e-5, 'freq_unit':'cd', 'pg':'pg'})
    list_freq.append({'freq':3.999837, 'freq_err':1.1400e-4, 'freq_unit':'cd', 'pg':'pg'})

    # # Possible radial fundamental mode
    list_freq.append({'freq':4.921764, 'freq_err':5.2000e-5, 'freq_unit':'cd', 'pg':'pg'})

    # # Two isolated peaks in the very deserted part of the power spectrum
    list_freq.append({'freq':6.129384, 'freq_err':0.1, 'freq_unit':'cd', 'pg':'p'}) # freq_err=3.0700e-4
    list_freq.append({'freq':7.323919, 'freq_err':0.1, 'freq_unit':'cd', 'pg':'p'}) # freq_err=4.6600e-4

    # # Unidentified p-modes. Modes are splitted and crowded.
    # list_freq.append({'freq':8.477823, 'freq_err':3.9300e-4, 'freq_unit':'cd', 'pg':'p'}) 
    list_freq.append({'freq':8.821028, 'freq_err':0.343205, 'freq_unit':'cd', 'pg':'p'}) # freq_err=1.1950e-4
    list_freq.append({'freq':10.32814, 'freq_err':0.0354360, 'freq_unit':'cd', 'pg':'p'})
    # list_freq.append({'freq':11.00743, 'freq_err':4.5600e-4, 'freq_unit':'cd', 'pg':'p'})
    list_freq.append({'freq':11.60817, 'freq_err':0.1164616, 'freq_unit':'cd', 'pg':'p'})
    # list_freq.append({'freq':13.18485, 'freq_err':2.1600e-4, 'freq_unit':'cd', 'pg':'p'})
    list_freq.append({'freq':14.07403, 'freq_err':0.1157924, 'freq_unit':'cd', 'pg':'p'})

  else:
    # Using the frequency list derived from Conny's revision of the Degroote's periodogram!
    f_e = 1.0e-4  # Conny thinks so
    list_freq.append({'freq':0.48483, 'freq_err':f_e, 'freq_unit':'cd', 'pg':'g', 'l':1, 'm':0})
    list_freq.append({'freq':0.50992, 'freq_err':f_e, 'freq_unit':'cd', 'pg':'g', 'l':1, 'm':0})
    list_freq.append({'freq':0.56340, 'freq_err':f_e, 'freq_unit':'cd', 'pg':'g', 'l':1, 'm':0})
    list_freq.append({'freq':0.59342, 'freq_err':f_e, 'freq_unit':'cd', 'pg':'g', 'l':1, 'm':0})
    list_freq.append({'freq':0.63683, 'freq_err':f_e, 'freq_unit':'cd', 'pg':'g', 'l':1, 'm':0})
    list_freq.append({'freq':0.68440, 'freq_err':f_e, 'freq_unit':'cd', 'pg':'g', 'l':1, 'm':0})
    list_freq.append({'freq':0.73841, 'freq_err':f_e, 'freq_unit':'cd', 'pg':'g', 'l':1, 'm':0})
    list_freq.append({'freq':0.80413, 'freq_err':f_e, 'freq_unit':'cd', 'pg':'g', 'l':1, 'm':0})
    list_freq.append({'freq':0.87931, 'freq_err':f_e, 'freq_unit':'cd', 'pg':'g', 'l':1, 'm':0})
    list_freq.append({'freq':0.97367, 'freq_err':f_e, 'freq_unit':'cd', 'pg':'g', 'l':1, 'm':0})


  dic['list_dic_freq'] = list_freq

  return dic

#=================================================================================================================
def HD43317():

  dic = dic_default()

  dic['name'] = [r'HD\,43317']
  dic['sp_type'] = 'B3 IV' # Simbad
  dic['var_type'] = r'SPB-$\beta$ Cep'
  dic['PI'] = 'Peter Papics'
  dic['Ref'] = ['Papics et al. (2012)']
  dic['url'] = ['http://cdsads.u-strasbg.fr/cgi-bin/nph-bib_query?2012A%26A...542A..55P&db_key=AST&nosetcookie=1']
  dic['Teff'] = 16800.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 1000.
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.90
  dic['err_logg_1s'] = 0.1
  dic['parallax'] = 2.71
  dic['err_parallax'] = 0.45
  dic['vsini'] = 106
  dic['err_vsini_1s'] = 4

  return dic

#=================================================================================================================
def nu_Eri():

  dic = dic_default()

  dic['name'] = [r'$\nu$\,Eri', r'HD\,29248']
  dic['sp_type'] = r'B1.5 IV'  # Nieva (2013, A&A)
  dic['var_type'] = r'$\beta$ Cep'
  dic['PI'] = 'Ehsan Moravveji'
  dic['Ref'] = ['Ausseloos+(2004)', 'Dziembowski and Pamyatnykh (2008)', 'De Ridder+(2004)',
                'Daszynska-Daszkiewicz+(2009)', 'Aerts (2013)']
  dic['url'] = ['http://cdsads.u-strasbg.fr/cgi-bin/nph-bib_query?2004MNRAS.355..352A&db_key=AST&nosetcookie=1',
                'http://cdsads.u-strasbg.fr/cgi-bin/nph-bib_query?2008MNRAS.385.2061D&db_key=AST&nosetcookie=1',
                'http://cdsads.u-strasbg.fr/cgi-bin/nph-bib_query?2004MNRAS.351..324D&db_key=AST&nosetcookie=1',
                'http://mnras.oxfordjournals.org/content/403/1/496']
  dic['Teff'] = 23500.                      # Morel+06; Nieva (2013, A&A) give 22000
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 1000.                # Morel+06; Nieva (2013, A&A) give 250
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.75          # Morel+06
  dic['err_logg_1s'] = 0.15   # Morel+06
  dic['log_L'] = 3.89
  dic['err_log_L_1s'] = 0.29
  dic['parallax'] = 4.83
  dic['err_parallax'] = 0.19
  dic['vsini'] = 26                   # Nieva (2013, A&A)
  dic['err_vsini_1s'] = 2             # Nieva (2013, A&A)
  dic['Xc'] = 0.26
  dic['mass'] = 9.5
  dic['alpha_ov'] = 0.06   # <0.12 (Table 1 in Aerts 2013 proceeding)
  dic['err_alpha_ov'] = 0.06 # arbitrary
  dic['v_micro'] = 6                  # +/- 1 km/s; Nieva (2013, A&A)
  dic['v_macro'] = 15                 # +/- 5 km/s; Nieva (2013, A&A)

  list_freq = []
  f_e = 1e-4   # Taken as the worst value used in Daszynska-Daszkiewicz+2010
  # Whenever el or em is None, the photometric of spectroscopic mode identification were infeasible
  list_freq.append({'freq':0.4327860, 'freq_err':f_e, 'freq_unit':'cd', 'l':1, 'm':0})       # g-mode. em=0 is assumed
  list_freq.append({'freq':0.6144000, 'freq_err':f_e, 'freq_unit':'cd', 'l':None, 'm':None}) # g-mode. el=2,4,5
  list_freq.append({'freq':5.6200186, 'freq_err':f_e, 'freq_unit':'cd', 'l':1, 'm':-1})
  list_freq.append({'freq':5.6372470, 'freq_err':f_e, 'freq_unit':'cd', 'l':1, 'm':0})
  list_freq.append({'freq':5.6538767, 'freq_err':f_e, 'freq_unit':'cd', 'l':1, 'm':+1})
  list_freq.append({'freq':5.7632828, 'freq_err':f_e, 'freq_unit':'cd', 'l':0, 'm':0})  # radial fundamental mode
  list_freq.append({'freq':6.2236000, 'freq_err':f_e, 'freq_unit':'cd', 'l':None, 'm':None}) # el=1,2,3
  list_freq.append({'freq':6.2438470, 'freq_err':f_e, 'freq_unit':'cd', 'l':1, 'm':0})
  list_freq.append({'freq':6.2629170, 'freq_err':f_e, 'freq_unit':'cd', 'l':None, 'm':None}) # el=1,3
  list_freq.append({'freq':6.7322300, 'freq_err':f_e, 'freq_unit':'cd', 'l':None, 'm':None}) # el=(3)?
  list_freq.append({'freq':7.2009000, 'freq_err':f_e, 'freq_unit':'cd', 'l':None, 'm':None}) # el=(2)?
  list_freq.append({'freq':7.8982000, 'freq_err':f_e, 'freq_unit':'cd', 'l':1, 'm':None})
  list_freq.append({'freq':7.9138300, 'freq_err':f_e, 'freq_unit':'cd', 'l':None, 'm':None}) # el<=3
  list_freq.append({'freq':7.9299200, 'freq_err':f_e, 'freq_unit':'cd', 'l':None, 'm':None}) # el=?
  dic['list_dic_freq'] = list_freq

  return dic


#=================================================================================================================
def V1449_Aqu():

  dic = dic_default()

  dic['name'] = [r'V1449\,Aqu', r'HD\,180642']
  dic['sp_type'] = 'B1.5 II-III'
  dic['var_type'] = 'beta Cep'
  dic['PI'] = 'Ehsan Moravveji'
  dic['Ref'] = ['Aerts+(2011)']
  dic['url'] = ['http://cdsads.u-strasbg.fr/cgi-bin/nph-bib_query?2011A%26A...534A..98A&db_key=AST&nosetcookie=1']
  dic['Teff'] = np.power(10., 4.389)
  dic['log_Teff'] = 4.389
  dic['err_Teff_1s'] = 1500.  # assumed
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.83
  dic['err_logg_1s'] = 0.3 # assumed
  dic['log_L'] = 4.2
  dic['err_log_L_1s'] = 0.03
  dic['parallax'] = 1.73
  dic['err_parallax'] = 0.65
  dic['vsini'] = 25
  dic['f_rot'] = 0.075
  dic['Xc'] = 0.23
  dic['mass'] = 11.6
  dic['alpha_ov'] = 0.025  # <0.05 Table 1 in Aerts (2013)
  dic['err_alpha_ov'] = 0.025

  return dic

#=================================================================================================================
def Lac_12():

  dic = dic_default()

  dic['name'] = [r'12\,Lac']
  dic['sp_type'] = 'B2 III'
  dic['var_type'] = 'beta Cep'
  dic['PI'] = 'Ehsan Moravveji'
  dic['Ref'] = ['Desmet+(2009)', 'Dziembowski and Pamyatnykh (2008)', 'Daszynska-Daszkiewicz+(2013)']
  dic['url'] = ['http://adsabs.harvard.edu/abs/2009MNRAS.396.1460D',
                'http://mnras.oxfordjournals.org/content/431/4/3396']
  dic['Teff'] = 24500.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 1000.0
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.65          # Morel+06
  dic['err_logg_1s'] = 0.15   # Morel+06
  dic['parallax'] = 2.43
  dic['err_parallax'] = 0.29
  dic['vsini'] = 49.0
  dic['err_vsini_1s'] = 2.0
  dic['Xc'] = 0.17
  dic['err_Xc'] = 0.04
  dic['mass'] = 13.1
  dic['err_mass'] = 1.1
  dic['alpha_ov'] = 0.0

  list_freq = []
  f_e = 0  # For errors see Handler+06 and discuss with Conny too!
  list_freq.append({'freq':0.35529,  'freq_err':f_e, 'freq_unit':'cd', 'l':1}) # el is 1, 2 or 4
  list_freq.append({'freq':4.24062,  'freq_err':f_e, 'freq_unit':'cd', 'l':2})
  list_freq.append({'freq':5.066346, 'freq_err':f_e, 'freq_unit':'cd', 'l':1})
  list_freq.append({'freq':5.179034, 'freq_err':f_e, 'freq_unit':'cd', 'l':1})
  list_freq.append({'freq':5.2162,   'freq_err':f_e, 'freq_unit':'cd', 'l':4}) # el is 4 or 2
  list_freq.append({'freq':5.30912,  'freq_err':f_e, 'freq_unit':'cd', 'l':2}) # el is 2, 1 or 3
  list_freq.append({'freq':5.334357, 'freq_err':f_e, 'freq_unit':'cd', 'l':0})
  list_freq.append({'freq':5.490167, 'freq_err':f_e, 'freq_unit':'cd', 'l':2})
  list_freq.append({'freq':5.8341,   'freq_err':f_e, 'freq_unit':'cd', 'l':1}) # el is 1 or 2
  list_freq.append({'freq':5.84546,  'freq_err':f_e, 'freq_unit':'cd', 'l':2}) # el is 2 or 1
  list_freq.append({'freq':6.7023,   'freq_err':f_e, 'freq_unit':'cd', 'l':1})
  list_freq.append({'freq':7.40705,  'freq_err':f_e, 'freq_unit':'cd', 'l':1}) # el is 1 or 2
  list_freq.append({'freq':10.4324,  'freq_err':f_e, 'freq_unit':'cd', 'l':1}) # el is 1, 2 or 3
  dic['list_dic_freq'] = list_freq

  return dic

#=================================================================================================================
def Mon_16():

  dic = dic_default()

  dic['name'] = [r'16\,Mon', r'HD\,48977']
  dic['sp_type'] = 'B2.5 V'
  dic['var_type'] = r'$\beta$ Cep - SPB Hybrid'
  dic['binary'] = False
  dic['PI'] = 'Anne Thoul'
  dic['Ref'] = ['Thoul et al. (2013, A&A)']
  dic['url'] = ['http://adsabs.harvard.edu/abs/2013A%26A...551A..12T']
  dic['Teff'] = 20000.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 1000  # assumed; Thoul et al. give 1000
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 4.2
  dic['err_logg_1s'] = 0.1

  return dic

#=================================================================================================================
def KIC_10526294():

  dic = dic_default()

  dic['name'] = [r'KIC\,10526294']
  dic['sp_type'] = 'B8.3 V'
  dic['var_type'] = 'SPB'
  dic['PI'] = 'Ehsan Moravveji, Peter Papics'
  dic['Ref'] = ['not published yet']
  dic['Teff'] = 11550.0
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 500.0
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 4.1
  dic['err_logg_1s'] = 0.2
  dic['vsini'] = 18.0
  dic['err_vsini_1s'] = 4.0
  dic['v_micro'] = 2.0
  dic['mass'] = 3.19           # to boradly range from Model 4 and Model 11
  dic['err_mass'] = 0.06       # to make sure we cover from 3.13 to 3.25     
  dic['Xc'] = 0.627
  dic['err_Xc'] = 0.001
  dic['f_ov'] = 0.017
  dic['err_f_ov'] = 0.001
  dic['alpha_ov'] = 0.21       # Taken from best model from the Step Overshoot grid
  dic['err_alpha_ov'] = 0.01   # the grid stepsize

  list_freq = []
  #f_e       = 0.00171   # d^{-1} from Loumos and Deeming criteria
  f_e       = 0.000685   # d^{-1} from Rayleigh limit

  #list_freq.append({'id':'f00', 'freq':0.459223, 'freq_err':0.000007, 'freq_unit':'cd', 'l':1, 'pg':'g'})  # Not in P14 paper!
  
  list_freq.append({'id':'f01', 'amp':148.7 , 'freq':0.472220, 'freq_err':0.000019, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f02', 'amp':162.6 , 'freq':0.486192, 'freq_err':0.000019, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f03', 'amp':449.1 , 'freq':0.500926, 'freq_err':0.000012, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f04', 'amp':183.6 , 'freq':0.517303, 'freq_err':0.000017, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f05', 'amp':1429.6, 'freq':0.533426, 'freq_err':0.000006, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f06', 'amp':8738.6, 'freq':0.552608, 'freq_err':0.000002, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f07', 'amp':3017.3, 'freq':0.571964, 'freq_err':0.000004, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f08', 'amp':385.2 , 'freq':0.593598, 'freq_err':0.000013, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f09', 'amp':1342.3, 'freq':0.615472, 'freq_err':0.000006, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f10', 'amp':100   , 'freq':0.641202, 'freq_err':0.000023, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f11', 'amp':156.7 , 'freq':0.670600, 'freq_err':0.000019, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f12', 'amp':100   , 'freq':0.701246, 'freq_err':0.000025, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f13', 'amp':9640.6, 'freq':0.734708, 'freq_err':0.000003, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f14', 'amp':258.6 , 'freq':0.772399, 'freq_err':0.000015, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f15', 'amp':5601.8, 'freq':0.812940, 'freq_err':0.000003, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f16', 'amp':510.2 , 'freq':0.856351, 'freq_err':0.000010, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f17', 'amp':80    , 'freq':0.902834, 'freq_err':0.000023, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f18', 'amp':584.5 , 'freq':0.954107, 'freq_err':0.000010, 'freq_unit':'cd', 'l':1, 'pg':'g'})
  list_freq.append({'id':'f19', 'amp':376.9 , 'freq':1.013415, 'freq_err':0.000012, 'freq_unit':'cd', 'l':1, 'pg':'g'})

  dic['list_dic_freq'] = list_freq

  return dic

#=================================================================================================================
def KIC_6352430():

  dic = dic_default()

  dic['name'] = [r'KIC\,6352430']
  dic['var_type'] = r'SPB'
  dic['binary'] = True
  dic['PI'] = 'Peter Papics'
  dic['Ref'] = ['Papics+2013(A&A,553,127)']

  list_freq = []   # Based on Table 7 in the paper
  f_rayleigh = 0.000868   # The Rayleigh frequency limit (i.e. 1/T) of the dataset
  f_e = 2.5 * f_rayleigh
  list_freq.append({'id':'f01', 'freq':1.361690, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'id':'f02', 'freq':1.518726, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'id':'f03', 'freq':1.463729, 'freq_err':f_e, 'freq_unit':'cd'})
  #list_freq.append({'id':'f04', 'freq':1.047486, 'freq_err':f_e, 'freq_unit':'cd'}) # 3f1 - 2f2
  list_freq.append({'id':'f05', 'freq':1.173030, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'id':'f06', 'freq':1.284234, 'freq_err':f_e, 'freq_unit':'cd'})
  #list_freq.append({'id':'f07', 'freq':2.982459, 'freq_err':f_e, 'freq_unit':'cd'}) # f2+f3
  list_freq.append({'id':'f08', 'freq':0.465004, 'freq_err':f_e, 'freq_unit':'cd'})
  #list_freq.append({'id':'f09', 'freq':1.754439, 'freq_err':f_e, 'freq_unit':'cd'}) # 2f3-f5
  list_freq.append({'id':'f10', 'freq':0.997712, 'freq_err':f_e, 'freq_unit':'cd'})
  #list_freq.append({'id':'f11', 'freq':2.880422, 'freq_err':f_e, 'freq_unit':'cd'}) # f1+f2
  #list_freq.append({'id':'f12', 'freq':0.037698, 'freq_err':f_e, 'freq_unit':'cd'}) # f_SB2
  #list_freq.append({'id':'f13', 'freq':0.157032, 'freq_err':f_e, 'freq_unit':'cd'}) # f2-f1
  list_freq.append({'id':'f14', 'freq':3.821662, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'id':'f15', 'freq':3.086319, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'id':'f16', 'freq':3.246498, 'freq_err':f_e, 'freq_unit':'cd'})
  #list_freq.append({'id':'f17', 'freq':2.691767, 'freq_err':f_e, 'freq_unit':'cd'}) # f2+f5
  #list_freq.append({'id':'f18', 'freq':5.713156, 'freq_err':f_e, 'freq_unit':'cd'}) # 3f6+4f8
  #list_freq.append({'id':'f19', 'freq':2.723390, 'freq_err':f_e, 'freq_unit':'cd'}) # 2f1
  #list_freq.append({'id':'f20', 'freq':3.037473, 'freq_err':f_e, 'freq_unit':'cd'}) # 2f2
  list_freq.append({'id':'f21', 'freq':10.027681, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'id':'f22', 'freq':11.515490, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'id':'f23', 'freq':5.611949, 'freq_err':f_e, 'freq_unit':'cd'})
  #list_freq.append({'id':'f24', 'freq':5.446646, 'freq_err':f_e, 'freq_unit':'cd'}) # 4f1
  #list_freq.append({'id':'f25', 'freq':14.340933, 'freq_err':f_e, 'freq_unit':'cd'}) # 2f10+4f15
  list_freq.append({'id':'f26', 'freq':11.576811, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'id':'f27', 'freq':8.562751, 'freq_err':f_e, 'freq_unit':'cd'})
  #list_freq.append({'id':'f28', 'freq':13.034207, 'freq_err':f_e, 'freq_unit':'cd'}) # f2+f22
  list_freq.append({'id':'f29', 'freq':15.702644, 'freq_err':f_e, 'freq_unit':'cd'})

  all_freq = []
  for i_dic, dic in enumerate(list_freq): all_freq.append(dic['freq'])
  all_freq = np.array(all_freq)
  ind_sort = np.argsort(all_freq)
  ind_sort = [ind_sort[i] for i in range(len(all_freq))]
  #list_freq = list_freq[ind_sort]
  sorted_list = [list_freq[i] for i in ind_sort]

  #dic['list_dic_freq'] = list_freq   # sorted in ascending frequency order
  dic['list_dic_freq'] = sorted_list

  return dic

#=================================================================================================================
def sigma_Sco_primary():

  dic = dic_default()

  dic['name'] = [r'$\sigma$\,Sco Primary']
  dic['var_type'] = r'$\beta$ Cep'
  dic['PI'] = 'Andrew Tkachenko'
  dic['binary'] = True
  dic['Ref'] = 'Tkachenko et al. 2014, MNRAS'
  dic['Teff'] = 25200.
  dic['err_Teff_1s'] = 500.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.68
  dic['err_logg_1s'] = 0.05
  dic['log_L'] = 4.38
  dic['err_log_L_1s'] = 0.10    # actually, it is log_L = 4.38 + 0.05 - 0.10
  dic['vsini'] = 31.5
  dic['err_vsini_1s'] = 1.5
  dic['v_macro'] = 14.0

  list_dic_freq = []
  list_dic_freq.append({'id':'f1', 'el':0, 'n':1, 'freq':4.0515, 'freq_err':0.0019, 'freq_unit':'cd'})
  list_dic_freq.append({'id':'f2', 'el':1, 'n':1, 'freq':4.1726, 'freq_err':0.0019, 'freq_unit':'cd'})
  list_dic_freq.append({'id':'f3', 'el':0, 'n':3, 'freq':5.9706, 'freq_err':0.0019, 'freq_unit':'cd'})
  dic['list_dic_freq'] = list_dic_freq

  return dic

#=================================================================================================================
def sigma_Sco_secondary():

  dic = dic_default()
  dic['name'] = [r'$\sigma$\,Sco Secondary']
  #dic['var_type'] = r''
  dic['PI'] = 'Andrew Tkachenko'
  dic['binary'] = True
  dic['Ref'] = 'Tkachenko et al. 2014, MNRAS'
  dic['Teff'] = 25000.
  dic['err_Teff_1s'] = 800.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 4.16
  dic['err_logg_1s'] = 0.05
  dic['log_L'] = 3.73
  dic['err_log_L_1s'] = 0.15
  dic['vsini'] = 43.0
  dic['err_vsini_1s'] = 1.5
  dic['v_macro'] = 4.0

  return dic

#=================================================================================================================
def HD160124():

  dic = dic_default()

  dic['name'] = [r'HD\,160124']
  dic['sp_type'] = r'B'
  dic['var_type'] = 'SPB'
  dic['PI'] = 'Peter De Cat'
  dic['Ref'] = ['']
  dic['url'] = [''] # ADS or Simbad links
  f_e = 0.0001      # Assumed precision
  list_freq = []
  list_freq.append({'freq':0.52014, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'freq':0.52096, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'freq':0.52055, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'freq':0.52137, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'freq':0.52197, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'freq':0.70259, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'freq':1.0422, 'freq_err':f_e, 'freq_unit':'cd'})
  list_freq.append({'freq':0.31366, 'freq_err':f_e, 'freq_unit':'cd'})
  dic['list_dic_freq'] = list_freq # see HD50230 for an example

  return dic

#=================================================================================================================
def Spica_primary():

  dic = dic_default()

  dic['name'] = [r'Spica\,1', r'HD\,116658']
  dic['var_type'] = r'$\beta$ Cep'
  dic['binary'] = True
  dic['PI'] = 'Andrew Tkachenko'
  dic['Teff'] = 25300.0
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 500.0
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.71
  dic['err_logg_1s'] = 0.1
  dic['parallax'] = 13.06
  dic['err_parallax'] = 0.70
  dic['vsini'] = 166   # km/sec
  dic['err_vsini_1s'] = 0
  dic['v_eq'] = 147    # km/sec
  dic['v_crit'] = 540  # km/sec
  dic['eta_rot'] = 0.27
  dic['f_rot'] = 0  # cycles_per_day
  dic['v_micro'] = 5
  dic['v_macro'] = 0
  dic['mass'] = 11.43      # dynamic mass
  dic['err_mass'] = 1.15
  dic['marker'] = 's'     # for plotting; marker symbol
  dic['ms'] = 10          # for plotting; marker size
  dic['mfc'] = 'navy'     # for plotting; marker face color
  dic['mec'] = 'purple'   # for plotting; marker edge color
  dic['ecolor'] = 'navy'  # for plotting; error bar color

  return dic

#=================================================================================================================
def Spica_secondary():

  dic = dic_default()

  dic['name'] = [r'Spica\,2']
  dic['sp_type'] = ''
  dic['var_type'] = ''
  dic['binary'] = True
  dic['PI'] = 'Andrew Tkachenko'
  dic['Teff'] = 20900.0
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 800.0
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 4.15
  dic['err_logg_1s'] = 0.15
  dic['log_L'] = 0
  dic['err_log_L_1s'] = 0
  dic['parallax'] = 13.06
  dic['err_parallax'] = 0.70
  dic['vsini'] = 59    # km/sec
  dic['err_vsini_1s'] = 0
  dic['v_eq'] = 53     # km/sec
  dic['v_crit'] = 606  # km/sec
  dic['eta_rot'] = 0.08
  dic['v_micro'] = 0
  dic['v_macro'] = 2.5
  dic['Ys'] = 0
  dic['err_Ys'] = 0
  dic['Xc'] = 0
  dic['err_Xc'] = 0
  dic['mass'] = 7.21
  dic['err_mass'] = 0.75
  dic['list_dic_freq'] = [{'freq':0, 'freq_err':0, 'freq_unit':'cd'}] # see HD50230 for an example
  dic['marker'] = 's'     # for plotting; marker symbol
  dic['ms'] = 10          # for plotting; marker size
  dic['mfc'] = 'navy'     # for plotting; marker face color
  dic['mec'] = 'purple'   # for plotting; marker edge color
  dic['ecolor'] = 'navy'  # for plotting; error bar color

  return dic

#=================================================================================================================
def V380Cyg_primary():  

  dic = dic_default()

  dic['name'] = [r'V380\,Cyg\,A']
  dic['sp_type'] = 'B'
  dic['var_type'] = 'beta Cep'
  dic['binary'] = True
  dic['PI'] = 'Andrew Tkachenko'
  dic['Ref'] = ['Tkachenko et al. (2014)']
  dic['url'] = ['http://mnras.oxfordjournals.org/content/438/4/3093.full.pdf'] # ADS or Simbad links
  dic['Teff'] = 21700.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 300.0
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.104
  dic['err_logg_1s'] = 0.006
  dic['vsini'] = 98
  dic['err_vsini_1s'] = 2
  dic['mass'] = 11.43
  dic['err_mass'] = 0.19

  return dic

#=================================================================================================================
def V380Cyg_secondary():  

  dic = dic_default()

  dic['name'] = [r'V380\,Cyg\,B']
  dic['sp_type'] = 'B'
  dic['var_type'] = 'beta Cep'
  dic['binary'] = True
  dic['PI'] = 'Andrew Tkachenko'
  dic['Ref'] = ['Tkachenko et al. (2014)']
  dic['url'] = ['http://mnras.oxfordjournals.org/content/438/4/3093.full.pdf'] # ADS or Simbad links
  dic['Teff'] = 22700.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 1200.0
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 4.120
  dic['err_logg_1s'] = 0.011
  dic['vsini'] = 38
  dic['err_vsini_1s'] = 2
  dic['mass'] = 7.0
  dic['err_mass'] = 0.14

  return dic

#=================================================================================================================
def KIC_7760680():
  """
  """
  dic = dic_default()

  dic['name'] = [r'KIC\,7760680']
  dic['sp_type'] = 'B8 V'
  dic['var_type'] = 'SPB'
  dic['binary'] = False
  dic['PI'] = 'Ehsan Moravveji'
  dic['Ref'] = ['Papics et al. (2015)']
  dic['url'] = ['http://adsabs.harvard.edu/abs/2015ApJ...803L..25P'] 
  dic['Teff'] = 11650.0
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 210.0
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.97
  dic['err_logg_1s'] = 0.08
  dic['vsini'] = 62.0
  dic['err_vsini_1s'] = 5.0
  dic['v_macro'] = 0.0   # ^{+0.6}_{-0.0}
  dic['Xc'] = 0.44   # first rough estimate by Papics et al. (2015)
  dic['mass'] = 3.3  # first rough estimate by Papics et al. (2015)
  dic['err_mass'] = 0.1  # arbitrary
  dic['alpha_ov'] = 0.3  # first rough estimate by Papics et al. (2015)
  dic['err_alpha_ov'] = 0.05 # arbitrary

  list_dic_freq = []
  list_dic_freq.append({'freq':1.15035 , 'freq_err':0.00003 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':1.10930 , 'freq_err':0.00001 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':1.07485 , 'freq_err':0.00001 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':1.04521 , 'freq_err':0.00001 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':1.018650, 'freq_err':0.000006, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.992189, 'freq_err':0.000002, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.967849, 'freq_err':0.000004, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.94858 , 'freq_err':0.00001 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.931824, 'freq_err':0.000004, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.913381, 'freq_err':0.000003, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.894849, 'freq_err':0.000001, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.877208, 'freq_err':0.000006, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.862873, 'freq_err':0.000001, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.850603, 'freq_err':0.000005, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.838528, 'freq_err':0.000001, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.826317, 'freq_err':0.000005, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.814319, 'freq_err':0.000002, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.802928, 'freq_err':0.000009, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.794259, 'freq_err':0.000004, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.78394 , 'freq_err':0.00001 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.77553 , 'freq_err':0.00002 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.76716 , 'freq_err':0.00002 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.75993 , 'freq_err':0.00001 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.753179, 'freq_err':0.000008, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.745763, 'freq_err':0.000004, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.73831 , 'freq_err':0.00001 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.73129 , 'freq_err':0.00002 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.72496 , 'freq_err':0.00002 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.71937 , 'freq_err':0.00001 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.71379 , 'freq_err':0.00001 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.709133, 'freq_err':0.000009, 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.70342 , 'freq_err':0.00002 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.69765 , 'freq_err':0.00002 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.69287 , 'freq_err':0.00002 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.68842 , 'freq_err':0.00002 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  list_dic_freq.append({'freq':0.68472 , 'freq_err':0.00002 , 'freq_unit':'cd', 'l':1, 'm':0, 'pg':'g'})
  dic['list_dic_freq'] = list_dic_freq

  return dic

#=================================================================================================================
def EPIC202060092():

  dic = dic_default()

  dic['name'] = [r'EPIC\,202060092']
  dic['sp_type'] = 'O9V:p'
  dic['var_type'] = r'$\beta\,$Cep'
  dic['binary'] = True
  dic['PI'] = 'Bram Buysschaert'
  dic['Ref'] = ['Buysschaert et al. (2015, MNRAS)']
  dic['url'] = ['http://adsabs.harvard.edu/abs/2015arXiv150703091B']
  dic['Teff'] = 35000.0
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 2000.0   # I only guessed this, since it's not reporeted in the paper
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 4.3 #4.5
  dic['err_logg_1s'] = 0.20     # I only guessed this, since it's not reporeted in the paper
  dic['vsini'] = 270
  dic['err_vsini_1s'] = 0
  dic['Xc'] = 0.30              # see top axis in thei Fig. 5
  dic['err_Xc'] = 0.06          # see top axis in thei Fig. 5
  dic['mass'] = 32.5            # very roughly estimated in their paper
  dic['err_mass'] = 2.5         # I only guess this

  list_dic_freq = []
  list_dic_freq.append({'freq':3.4243, 'freq_err':0.0054, 'freq_unit':'cd', 'pg':'p'})
  list_dic_freq.append({'freq':3.6663, 'freq_err':0.0066, 'freq_unit':'cd', 'pg':'p'})
  list_dic_freq.append({'freq':6.9853, 'freq_err':0.0088, 'freq_unit':'cd', 'pg':'p'})
  list_dic_freq.append({'freq':3.3173, 'freq_err':0.0113, 'freq_unit':'cd', 'pg':'p'})
  list_dic_freq.append({'freq':3.5583, 'freq_err':0.0089, 'freq_unit':'cd', 'pg':'p'})
  list_dic_freq.append({'freq':0, 'freq_err':0, 'freq_unit':'cd', 'pg':'p'})

  dic['marker'] = 's'     # for plotting; marker symbol
  dic['ms'] = 10          # for plotting; marker size
  dic['mfc'] = 'navy'     # for plotting; marker face color
  dic['mec'] = 'purple'   # for plotting; marker edge color
  dic['ecolor'] = 'navy'  # for plotting; error bar color

  return dic

#=================================================================================================================
def EPIC202061162():
  """
  K2 Field 0. Teff and logg from Norberto Castro. vsini and v_macro from Sergio
  """

  dic = dic_default()

  dic['name'] = [r'HD\,253658', r'EPIC\,202061162']
  dic['sp_type'] = r'B8\,Ib' # Simbad
  dic['var_type'] = ''
  dic['binary'] = False
  dic['PI'] = 'Ehsan Moravveji'
  dic['Ref'] = ['']
  dic['url'] = [''] # ADS or Simbad links
  dic['Teff'] = 11000.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 1000.
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 2.26
  dic['err_logg_1s'] = 0.15
  dic['vsini'] = 24.
  dic['err_vsini_1s'] = 0
  dic['v_macro'] = 26

  dic['marker'] = 's'     # for plotting; marker symbol
  dic['ms'] = 10          # for plotting; marker size
  dic['mfc'] = 'blue'     # for plotting; marker face color
  dic['mec'] = 'blue'   # for plotting; marker edge color
  dic['ecolor'] = 'blue'  # for plotting; error bar color

  return dic

#=================================================================================================================
def EPIC202061197():
  """
  K2 Field 0. Teff and logg from Norberto Castro. vsini and v_macro from Sergio
  """

  dic = dic_default()

  dic['name'] = [r'HD\,251847', r'EPIC\,202061197']
  dic['sp_type'] = r'B1\,IV' # Simbad
  dic['var_type'] = ''
  dic['binary'] = False
  dic['PI'] = 'Ehsan Moravveji'
  dic['Ref'] = ['']
  dic['url'] = [''] # ADS or Simbad links
  dic['Teff'] = 28000.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 1000. 
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.7
  dic['err_logg_1s'] = 0.06
  dic['vsini'] = 20
  dic['err_vsini_1s'] = 0
  dic['v_macro'] = 23

  dic['marker'] = 'o'     # for plotting; marker symbol
  dic['ms'] = 10          # for plotting; marker size
  dic['mfc'] = 'white'     # for plotting; marker face color
  dic['mec'] = 'blue'   # for plotting; marker edge color
  dic['ecolor'] = 'blue'  # for plotting; error bar color

  return dic

#=================================================================================================================
def EPIC202061199():
  """
  K2 Field 0. Teff and logg from Norberto Castro. vsini and v_macro from Sergio
  """
  dic = dic_default()

  dic['name'] = [r'HD\,255134', r'EPIC\,202061199']
  dic['sp_type'] = r'B1\,IVp' # Simbad
  dic['var_type'] = ''
  dic['binary'] = False
  dic['PI'] = 'Ehsan Moravveji'
  dic['Ref'] = ['']
  dic['url'] = [''] # ADS or Simbad links
  dic['Teff'] = 27000.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 1000. 
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.7
  dic['err_logg_1s'] = 0.1
  dic['vsini'] = 13
  dic['err_vsini_1s'] = 0
  dic['v_macro'] = 34

  dic['marker'] = 'o'     # for plotting; marker symbol
  dic['ms'] = 10          # for plotting; marker size
  dic['mfc'] = 'white'     # for plotting; marker face color
  dic['mec'] = 'blue'   # for plotting; marker edge color
  dic['ecolor'] = 'blue'  # for plotting; error bar color

  return dic

#=================================================================================================================
def EPIC202061205():
  """
  K2 Field 0. Teff and logg from Norberto Castro. vsini and v_macro from Sergio
  """
  dic = dic_default()

  dic['name'] = [r'HD\,253049', r'EPIC\,202061205']
  dic['sp_type'] = r'B2\,IV' # Simbad
  dic['var_type'] = '' # could be variable
  dic['binary'] = False # could be binary or spotted 
  dic['PI'] = 'Ehsan Moravveji'
  dic['Ref'] = ['']
  dic['url'] = [''] # ADS or Simbad links
  dic['Teff'] = 21500.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 900.
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.8
  dic['err_logg_1s'] = 0.1
  dic['vsini'] = 12
  dic['err_vsini_1s'] = 0
  dic['v_macro'] = 15

  dic['marker'] = 'o'     # for plotting; marker symbol
  dic['ms'] = 10          # for plotting; marker size
  dic['mfc'] = 'grey'     # for plotting; marker face color
  dic['mec'] = 'grey'   # for plotting; marker edge color
  dic['ecolor'] = 'grey'  # for plotting; error bar color

  return dic

#=================================================================================================================
def EPIC202061207():
  """
  K2 Field 0. Teff and logg from Norberto Castro. vsini and v_macro from Sergio
  """
  dic = dic_default()

  dic['name'] = [r'HD\,252409', r'EPIC\,202061207']
  dic['sp_type'] = r'B3\,IV' # Simbad
  dic['var_type'] = ''
  dic['binary'] = False
  dic['PI'] = 'Ehsan Moravveji'
  dic['Ref'] = ['']
  dic['url'] = [''] # ADS or Simbad links
  dic['Teff'] = 14000.
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 1000. 
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.2
  dic['err_logg_1s'] = 0.1
  dic['vsini'] = 40
  dic['err_vsini_1s'] = 0
  dic['v_macro'] = 22

  dic['marker'] = 's'     # for plotting; marker symbol
  dic['ms'] = 10          # for plotting; marker size
  dic['mfc'] = 'white'     # for plotting; marker face color
  dic['mec'] = 'blue'   # for plotting; marker edge color
  dic['ecolor'] = 'blue'  # for plotting; error bar color

  return dic

#=================================================================================================================
def EPIC202061208():
  """
  K2 Field 0. Teff and logg from Norberto Castro. vsini and v_macro from Sergio
  """

  dic = dic_default()

  dic['name'] = [r'HD\,252357', r'EPIC\,202061208']
  dic['sp_type'] = r'B9\,III' # Simbad
  dic['var_type'] = ''
  dic['binary'] = False
  dic['PI'] = 'Ehsan Moravveji'
  dic['Ref'] = ['']
  dic['url'] = [''] # ADS or Simbad links
  dic['Teff'] = 12000.  # or cooler
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 0 # this star is cooler than Norberto's grid
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.6
  dic['err_logg_1s'] = 0.045
  dic['vsini'] = 244
  dic['err_vsini_1s'] = 0
  dic['v_macro'] = 0

  dic['marker'] = 's'     # for plotting; marker symbol
  dic['ms'] = 10          # for plotting; marker size
  dic['mfc'] = 'white'     # for plotting; marker face color
  dic['mec'] = 'blue'   # for plotting; marker edge color
  dic['ecolor'] = 'blue'  # for plotting; error bar color

  return dic

#=================================================================================================================
def EPIC202061226():
  """
  K2 Field 0. Teff and logg from Norberto Castro. vsini and v_macro from Sergio
  """

  dic = dic_default()

  dic['name'] = [r'EPIC\,202061226']
  dic['sp_type'] = r'B2.5\,IV' # Simbad
  dic['var_type'] = 'Hybrid'  # maybe
  dic['binary'] = False
  dic['PI'] = 'Ehsan Moravveji'
  dic['Ref'] = ['']
  dic['url'] = [''] # ADS or Simbad links
  dic['Teff'] = 20500
  dic['log_Teff'] = np.log10(dic['Teff'])
  dic['err_Teff_1s'] = 2000
  dic['err_log_Teff_1s'] = np.log10(1.0 + dic['err_Teff_1s']/dic['Teff'])
  dic['logg'] = 3.85
  dic['err_logg_1s'] = 0.3
  dic['vsini'] = 380
  dic['err_vsini_1s'] = 0
  dic['v_macro'] = 0

  dic['marker'] = 's'     # for plotting; marker symbol
  dic['ms'] = 10          # for plotting; marker size
  dic['mfc'] = 'blue'     # for plotting; marker face color
  dic['mec'] = 'blue'   # for plotting; marker edge color
  dic['ecolor'] = 'blue'  # for plotting; error bar color

  return dic

#=================================================================================================================
def substitute_freq_err(dic_star, new_err):
  """
  To assign a constant fixed value of frequency uncertainty to all frequencies of a star, you can use this function.
  Then, the whole frequencies will be sustituted by the new_err value.
  @param dic_star: dictionary with information from star. the key "list_dic_freq" MUST be present
  @type dic_star: dictionary
  @param new_err: new frequency uncertainty to be applied to all targets
  @type new_err: float
  @return: dic_star: the same input star dictionary, but with substituted frequency uncertainties
  @rtype dic_star: dictionary
  """
  list_dic_freq = dic_star['list_dic_freq']
  new_list = []
  for i, dic in enumerate(list_dic_freq):
    dic = dic.copy()
    dic['freq_err'] = new_err
    new_list.append(dic)
    
  dic_star['list_dic_freq'] = new_list
  
  return dic_star
    
#=================================================================================================================
def change_freq_err_by_factor_for_pg(dic_star, factor, pg):
  """
  In some cases, e.g. HD50230, you only need to increase the frequency uncertainties for a subset of modes, e.g. 
  g-modes, and keep the rest intact. This is where this subroutine comes to play.
  @param dic_star: dictionary with information from star. the key "list_dic_freq" MUST be present
  @type dic_star: dictionary
  @param factor: the multiplication factor by which you change the errors
  @type factor: float
  @return dic_star: the same input star dictionary, but with modified frequency uncertainties
  @rtype dic_star: dictionary
  """
  dic_star      = dic_star.copy()
  list_dic_freq = dic_star['list_dic_freq']
  new_list_dic  = []
  for i_dic, dic in enumerate(list_dic_freq):
      dic = dic.copy()
      if not dic.has_key('pg'): continue
      if dic['pg'] == pg: dic['freq_err'] *= factor 
      new_list_dic.append(dic)

  dic_star['list_dic_freq'] = new_list_dic

  return dic_star

#=================================================================================================================
def change_freq_err_by_factor(dic_star, factor):
  """
  If the frequency uncertainties in "list_dic_freq" is too optimistic or pessemistic, one can multiply all frequency
  errors by a constant factor.
  @param dic_star: dictionary with information from star. the key "list_dic_freq" MUST be present
  @type dic_star: dictionary
  @param factor: the multiplication factor by which you change the errors
  @type factor: float
  @return dic_star: the same input star dictionary, but with modified frequency uncertainties
  @rtype dic_star: dictionary
  """
  dic_star      = dic_star.copy()
  list_dic_freq = dic_star['list_dic_freq']
  orig_freq_err = [dic['freq_err'] for dic in list_dic_freq]
  new_freq_err  = [freq_err * factor for freq_err in orig_freq_err]
  new_list_dic  = []
  for i_dic, dic in enumerate(list_dic_freq):
      dic = dic.copy()
      dic['freq_err'] = new_freq_err[i_dic]
      new_list_dic.append(dic)

  dic_star['list_dic_freq'] = new_list_dic

  return dic_star

#=================================================================================================================
def MC_freq_to_per(freq, freq_err):
  """
  Calculate the period as the inverse of frequency, and evaluate the error in period based on a simple Monte Carlo
  simulation of a normally distributed frequencies around the input "freq" with the standard deviation of freq_err
  @param freq: Observed oscillation frequency of the mode, strictly in cycles per day
  @type freq: float
  @param: freq_err: 1-sigma uncertainty in frequency
  @type freq_err: float
  @return: period and a standard diviaiton of the period
  @rtype: tuple
  """
  n = 1000000   # number of realizations
  freq_norm = np.random.normal(loc=freq, scale=freq_err, size=(n, ))

  #return np.mean(1./freq_norm), np.std(1./freq_norm)
  return 1.0 / freq, np.std(1./freq_norm)

#=================================================================================================================
def MC_dP(per_hi, per_hi_err, per_lo, per_lo_err):
  """
  Evaluate the period spacing between two consecuitive modes and assess the error by a Monte Carlo simulation of the
  period difference
  @param per_hi: period of the mode with higher order, hence longer period
  @type per_hi: float
  @param per_lo: period of the mode with lower order, hence shorter period
  @type per_lo: float
  @return: period, error in period, period spacing between the two modes, and the error in period spacing. Each of these
     four outputs is a numpy array
  @rtype: tuple
  """
  n = 1000000   # number of realizations
  arr_per_hi = np.random.normal(loc=per_hi, scale=per_hi_err, size=(n, ))
  arr_per_lo = np.random.normal(loc=per_lo, scale=per_lo_err, size=(n, ))
  arr_dP = arr_per_hi - arr_per_lo

  return np.mean(arr_dP), np.std(arr_dP)

#=================================================================================================================
def list_freq_to_list_dP(dic_star, freq_unit='Hz'):
  """
  This function receives a list of dictionaries of frequencies and their errors, from the input dictionary for a star.
  It updates the 'list_dic_freq' with assigning a proper period, period error, period spacing and period spacing error
  to each individual observed mode. The value for the period spacing and period spacing error of the last mode in the
  ascending period error is set to NaN, as it cannot be assessed.
  @param dic_star: dictionary with information from star. the key "list_dic_freq" MUST be present
  @type dic_star: dictionary
  @param freq_unit: desired unit of the observed frequencies for conversion; default=Hz
  @type freq_unit: string
  @return: updated dictionary of the star information
  @rtype: dic_star
  """
  dic_star = dic_star.copy()
  if 'list_dic_freq' not in dic_star.keys():
    logging.error('list_freq_to_list_dP: list of frequencies not provided.')

  list_dic_freq = dic_star['list_dic_freq']
  n_freq        = len(list_dic_freq)
  list_dic_freq = check_obs_vs_model_freq_unit(freq_unit, list_dic_freq) # fix the unit
  list_dic_freq = filter_modes_by_pg(list_dic_freq, 'g')

  for i_dic, dic in enumerate(list_dic_freq):
    if not dic.has_key('pg'):
      print 'Error: stars: list_freq_to_list_dP: mode id:{0} has no pg defined'.format(dic['id'])
      raise SystemExit
    if dic['pg'] != 'g':
      print 'Error: stars: list_freq_to_list_dP: You should not send p-modes here'
      raise SystemExit

  # Avoid adding keys that are already available
  available_keys = list_dic_freq[0].keys()
  if 'P_err' in available_keys : return dic_star

  list_P        = np.zeros(n_freq)
  list_P_err    = np.zeros(n_freq)
  for i_dic, dic in enumerate(list_dic_freq):
    dic['P'], dic['P_err'] = MC_freq_to_per(dic['freq'], dic['freq_err'])
    list_P[i_dic]     = dic['P']
    list_P_err[i_dic] = dic['P_err']
  ind_sort   = np.argsort(list_P)  # to ensure the periods are sorted in ascending order
  list_P     = list_P[ind_sort]
  list_P_err = list_P_err[ind_sort]

  # Now, to each observed frequency, we append the associated period and the error in period
  update_list_dic_freq_with_P = [list_dic_freq[ind] for ind in ind_sort]

  list_dP = np.zeros(n_freq-1); list_dP_err = np.zeros(n_freq-1)
  for i_dic in range(n_freq-1):
    dic        = update_list_dic_freq_with_P[i_dic]
    dP, dP_err = MC_dP(per_hi=list_P[i_dic+1], per_hi_err=list_P_err[i_dic+1],
                       per_lo=list_P[i_dic], per_lo_err=list_P_err[i_dic])
    list_dP[i_dic]     = dP
    list_dP_err[i_dic] = dP_err
    dic['dP']          = dP
    dic['dP_err']      = dP_err

  # Finally, to each mode, add the assosicated period spacing with the next mode (which has higher period).
  # For the last mode, we assign np.NaN to dP and dP_err
  update_list_dic_freq_with_P[-1]['dP']     = np.nan
  update_list_dic_freq_with_P[-1]['dP_err'] = np.nan
  update_list_dic_freq_with_dP = [update_list_dic_freq_with_P[ind] for ind in range(n_freq)]
  dic_star['list_dic_freq']    = update_list_dic_freq_with_dP

  #return list_P, list_P_err, list_dP, list_dP_err
  return dic_star

#=================================================================================================================
def check_obs_vs_model_freq_unit(freq_unit, list_freq):
  """
  The frequency list for different stars are in different units, e.g. cycles_per_day, Hz, uHz etc.
  For chi-square minimization, the frequency units must match. This function takes care of the matching,
  in case any of the frequencies differ from the requested "freq_unit"
  @param freq_unit: Desired output frequency unit; one of these: cycles_per_day, Hz, uHz
  @type freq_unit: string
  @param list_freq: list of dictionaries with the frequency, frequency error and frequency unit. Read comments
      for dic_default
  @type list_freq: list of dictionaries
  @return: list of dictionaries with each frequency and error adapted to the requested "freq_unit"
  @rtype: list
  """
  import commons
  dic_conv = commons.conversions()

  if freq_unit not in ['cd', 'Hz', 'uHz']:
    message = 'Error: stars: check_obs_vs_model_freq_unit: the requested freq_unit=%s not supported' % (freq_unit,)
    raise SystemExit, message

  list_freq = list_freq[:]
  n_freq = len(list_freq)
  if n_freq == 0:
    message = 'Error: stars: check_obs_vs_model_freq_unit: Input frequency list is empty'
    raise SystemExit, message

  for i_dic, dic in enumerate(list_freq):
    if dic['freq_unit'] == freq_unit:
      continue
    else:

      if dic['freq_unit'] == 'Hz' and freq_unit == 'uHz':
        conv = dic_conv['Hz_to_uHz']
        dic['freq_unit'] = 'uHz'
      elif dic['freq_unit'] == 'uHz' and freq_unit == 'Hz':
        conv = dic_conv['uHz_to_Hz']
        dic['freq_unit'] = 'Hz'
      elif dic['freq_unit'] == 'cd' and freq_unit == 'Hz':
        conv = dic_conv['cd_to_Hz']
        dic['freq_unit'] = 'Hz'
      elif dic['freq_unit'] == 'cd' and freq_unit == 'uHz':
        conv = dic_conv['cd_to_uHz']
        dic['freq_unit'] = 'uHz'
      elif dic['freq_unit'] == 'Hz' and freq_unit == 'cd':
        conv = dic_conv['Hz_to_cd']
        dic['freq_unit'] = 'cd'
      elif dic['freq_unit'] == 'uHz' and freq_unit == 'cd':
        conv = dic_conv['uHz_to_cd']
        dic['freq_unit'] = 'cd'
      else:
        message = 'Error: stars: check_obs_vs_model_freq_unit: You have screwed up freq_unit'
        raise SystemExit, message

      dic['freq']     *= conv
      dic['freq_err'] *= conv

  return list_freq

#=================================================================================================================
def filter_modes_by_pg(list_dic_freq, pg):
  """
  This function receives a list of modes and only returns those that match their 'pg' key/value criteria.
  This is useful in chi-square compuations to e.g. filter out p-modes and only look at g-modes.
  Each mode dictionary should already contain the 'pg' key/value pair.
  @param list_dic_freq: list of full mode info. Each dictionary should contain the 'pg' key/value pair.
  @type list_dic_freq: list of dictionaries
  @param pg: filter either by pg='p' or pg='g' modes.
  @type pg: string
  @return: filtered list of modes, that could or could not be shorter than the original.
  @rtype: list of dictionaries
  """
  list_dic_freq = list_dic_freq[:]
  n_modes = len(list_dic_freq)
  if n_modes == 0:
    logging.error('stars: filter_modes_by_pg: Input list is empty')
    raise SystemExit, 'Error: stars: filter_modes_by_pg: Input list is empty'

  if pg != 'p' and pg != 'g' and pg != 'pg':
    logging.error('stars: filter_modes_by_pg: the pg argument accepts only "p", "g" or "pg" values.')
    raise SystemExit, 'Error: stars: filter_modes_by_pg: the pg argument accepts only "p", "g" or "pg" values.'

  output = [dic for dic in list_dic_freq if dic['pg'] == pg]
  n_out  = len(output)
  if n_out == 0:
    logging.error('stars: filter_modes_by_pg: Found no mode matching pg="{0}". Check again!'.format(pg))
    raise SystemExit, 'Error: stars: filter_modes_by_pg: Found no mode matching pg="{0}". Check again!'.format(pg)

  return output

#=================================================================================================================
def filter_modes_by_freq(dic_star, freq_from, freq_to):
  """
  This function receives a list of dictionaries containing mode information, and returns only those modes whose 
  frequencies lie within the range passed by freq_from and freq_to.
  @param dic_star: dictionary with information from star. the key "list_dic_freq" MUST be present
  @type dic_star: dictionary
  @param freq_from, freq_to: the lower and upper frequency range for clipping
  @type freq_from, freq_to: float
  @return: updated star dictionary with the shortened list of mode info 
  @rtype: dictionary
  """
  dic_star  = dic_star.copy()
  list_dic_freq = dic_star['list_dic_freq'][:]
  n_dic     = len(list_dic_freq)
  list_freq = np.array( [ dic['freq'] for dic in list_dic_freq ] )
  ind       = np.argsort(list_freq)
  list_freq = list_freq[ind]
  list_dic_freq = [list_dic_freq[i] for i in ind]

  ind       = np.where((list_freq >= freq_from) & (list_freq <= freq_to))[0]

  dic_star['list_dic_freq'] = [list_dic_freq[i] for i in ind]

  return dic_star

#=================================================================================================================
def filter_modes_by_number(dic_star, i_from, i_to):
  """
  This function receives a list of mode dictionaries, and returns only those modes which are between i_from to i_to
  in their order. We make sure that i_to is smaller than the number of available modes, otherwise, an exception is 
  raised.
  @param dic_star: dictionary with information from star. the key "list_dic_freq" MUST be present
  @type dic_star: dictionary
  @param i_from, i_to: the index to start and end mode clipping. This is e.g. to return 4th to 8th mode in a list 
         containing 13 modes. i_from and i_to are both inclusive.
  @type i_from, i_to: integer
  @return: updated star dictionary with the i_to - i_from + 1 modes in list_dic_freq
  @rtype: dictionary
  """
  if i_to < i_from:
    logging.error('stars: filter_modes_by_number: i_to < i_from: {0}<{1}'.format(i_to, i_from))
    raise SystemExit

  dic_star = dic_star.copy()
  list_dic_freq = dic_star['list_dic_freq'][:]
  n_modes  = len(list_dic_freq)
  if i_to >= n_modes:
    logging.error('stars: filter_modes_by_number: i_to={0} cannot exceed number of available \
      modes:{1}'.format(i_to, n_modes))
    raise SystemExit

  if i_to == 0:
    list_dic_freq = [list_dic_freq[i_to]]
  else:
    list_dic_freq = list_dic_freq[ i_from : i_to ]
  dic_star['list_dic_freq'] = list_dic_freq

  return dic_star

#=================================================================================================================


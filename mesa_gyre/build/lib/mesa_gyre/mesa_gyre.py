
#! /usr/bin/python

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import commons as cm
import param_tools as pt
import file_dir_stat as st
import read as read 
import plot_hrd as hrd
import plot_gyre as pg
import gyre

trk_srch_str = pt.give_track_srch_str()
print 'Track Search String: ', trk_srch_str

dic_hist_files = st.get_hist_files()
n_hist = dic_hist_files['n_hist']
hist_files = dic_hist_files['hist_files']
print 'n_hist = ', n_hist

#hist = read.read_single_mesa_file(hist_files, is_hist = True)
#print 'hist header: '
#print hist.keys()
#print hist['n_lines']
#hrd.plot_single_hrd(hist)

#hists = read.read_multiple_mesa_files(hist_files, is_hist = True)
#hrd.plot_grid_hrd(hists)
#hrd.plot_kiel_diagram(hists)

dic_gyre = st.get_gyre_files()
n_gyre = dic_gyre['n_gyre']
print 'N_gyre HDF5 files: ', n_gyre
gyre_file_list = dic_gyre['gyre_files']
#dic_all_gyre, recarr_all_gyre = read.read_multiple_gyre_files(gyre_file_list)
#pg.plot_single_dP(gyre_file_list[0])
#pg.plot_multiple_dP(gyre_file_list)

pt.find_evol_for_gyre_file(gyre_file_list[0])

#print gmodes_delta_n_g, gmodes_delta_per, num_modes, gmodes_num, gmodes_indx

print '[Done]'




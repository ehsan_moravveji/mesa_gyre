
import sys
import logging
import itertools
import numpy as np 
import pylab as plt
import commons, tar

dic_conv = commons.conversions()

#=========================================================================================
#=========================================================================================
def per_vs_rot_freq(list_dic_h5, dic_star, el=1, em=+1, per_from=0.5, per_to=3, file_out=None):
  """
  This routine is actually copied and adapted from the original one found in 
  plot_tar.per_vs_rot_freq().

  This routine shows the evolution of the mode periods with respect to rotation rate,
  here eta_rot, quite similar to Figs. 1 and 2 in Bouabid et al. (2013).
  @param list_dic_h5: list of frequencies for every step/iteration (of a given eta_rot). 
        In addition to the default fields returned by read.read_multiple_gyre_files(), the
        following additional keys are also present:
        - omega_crit: critical angular rotation velocity, 2*pi*freq_crit in rad/sec
        - eta_rot: dimensionless quantity, equal to eta_rot above
  @type list_dic_h5: list of dictionaries
  @param file_out: full path to the output plot file
  @type file_out: string 
  @return: None
  @rtype: None
  """
  if file_out is None: return 
  
  sec_to_d = dic_conv['sec_to_d']
  Hz_to_cd = dic_conv['Hz_to_cd']

  n_dic    = len(list_dic_h5)
  arr_freq_rot = np.array([ dic['Led_freq_rot'] for dic in list_dic_h5 ])
  arr_rot_crit = np.array([ tar.get_omega_crit(dic['M_star'], dic['R_star'], to_cgs=False) for dic in list_dic_h5 ])
  arr_rot_crit *= Hz_to_cd
  arr_eta_rot  = arr_freq_rot / arr_rot_crit
  # arr_num_freq = np.array([ len(dic['l']) for dic in list_dic_h5 ])
  # arr_freq     = [ np.real(dic['freq']) for dic in list_dic_h5 ]

  P_key    = 'P_inertial_l{0:1d}_m{1:+1d}'.format(el, em)
  n_key    = 'n_pg_l{0:1d}_m{1:+1d}'.format(el, em)
  if P_key not in list_dic_h5[0].keys():
    logging.error('plot_ledoux: per_vs_rot_freq: the key: "{0}" not available'.format(P_key))
    raise SystemExit, 'Error: plot_ledoux: per_vs_rot_freq: the key: "{0}" not available'.format(P_key)

  #--------------------------------
  # Prepare the plot
  #--------------------------------
  # fig, (top, bot) = plt.subplots(2, figsize=(4, 6), sharex=True)
  fig = plt.figure(figsize=(4, 6))
  l   = 0.135
  w   = 0.830
  b   = 0.075
  h1  = 0.60
  h2  = 0.30
  bot = fig.add_axes([l, b, w, h1])
  top = fig.add_axes([l, b+h1+0.01, w, h2])
  # plt.subplots_adjust(left=0.15, right=0.96, bottom=0.075, top=0.98)

  #--------------------------------
  # Do the plot
  #--------------------------------
  dics_obs   = dic_star['list_dic_freq']
  N_obs_freq = len(dics_obs)
  obs_freqs  = np.array([ dic['freq'] for dic in dics_obs ])
  min_obs_freq = np.min(obs_freqs)
  max_obs_freq = np.max(obs_freqs)
  min_per    = 1.0 / max_obs_freq
  max_per    = 1.0 / min_obs_freq

  min_n_pg   = 1000
  max_n_pg   = -1000
  list_n_pg  = []
  list_per   = []
  list_eta_rot = []
  list_N_mod = []

  for i_dic, dic in enumerate(list_dic_h5):
    eta_rot  = arr_eta_rot[i_dic]
    per      = dic[P_key]
    n_pg     = dic[n_key]
    n_per    = len(per)
    min_n_pg = min([min_n_pg, np.min(n_pg)])
    max_n_pg = max([max_n_pg, np.max(n_pg)])
    eta_arr  = np.ones(n_per) * eta_rot

    ind_per  = np.where((per >= min_per) & (per <= max_per))[0]
    N_mod    = len(ind_per)

    list_eta_rot.append(eta_rot)
    list_n_pg.append(n_pg)
    list_per.append(per)
    list_N_mod.append(N_mod)

    # bot.scatter(eta_arr * 100, per, s=1, marker='.', color='black', zorder=2)

  range_n_pg = range(min_n_pg, max_n_pg+1)
  # keep_eta   = []
  # keep_per   = []
  for i, n in enumerate(range_n_pg):
    if n >= 0: continue
    vals_eta = []
    vals_per = []
    for i_eta, eta_rot in enumerate(list_eta_rot):
      n_pg   = list_n_pg[i_eta]
      ind    = np.where(n == n_pg)[0]
      if len(ind) > 0:
        vals_eta.append(eta_rot * 100)
        vals_per.append(list_per[i_eta][ind])

    bot.plot(vals_eta, vals_per, linestyle='solid', color='black', lw=0.5, zorder=3)

  list_eta_rot = np.array(list_eta_rot) * 100
  #--------------------------------
  # Add observed box as a background patch
  #--------------------------------
  bot.fill_between(list_eta_rot, y1=min_per, y2=max_per, color='blue', alpha=0.10, zorder=1)

  #--------------------------------
  # Add the top panel now
  #--------------------------------
  top.axhline(y=N_obs_freq, linestyle='dashed', color='grey', lw=2, zorder=1)
  top.scatter(list_eta_rot, list_N_mod, facecolor='blue', edgecolor='red', 
              marker='o', s=25, zorder=2)

  #--------------------------------
  # Legend, Annotations and Ranges
  #--------------------------------
  dx = (max(list_eta_rot) - min(list_eta_rot)) * 0.02
  top.set_xlim(min(list_eta_rot)-dx, max(list_eta_rot)+dx)
  top.set_ylim(min(list_N_mod)*0.90, max(list_N_mod)*1.10)
  top.set_ylabel(r'$N^{\rm (mod)}$')
  top.set_xticklabels(())
  top.xaxis.set_tick_params(labeltop='on')

  dy = (max(list_N_mod) - min(list_N_mod)) * 0.05
  txt = dic_star['name'][0]
  top.annotate(txt, xy=(min(list_eta_rot)+2*dx, N_obs_freq+dy), color='black', fontsize=10)

  bot.set_xlim(min(list_eta_rot)-dx, max(list_eta_rot)+dx)
  bot.set_ylim(per_from, per_to)
  bot.set_xlabel(r'$\eta_{\rm rot}=\Omega_{\rm rot} / \Omega_{\rm crit} \,[\%]$')
  bot.set_ylabel(r'Period in Inertial Frame [d]')

  #--------------------------------
  # Finalize the plot
  #--------------------------------
  plt.savefig(file_out, transparent=True)
  print ' - plot_tar: per_vs_rot_freq: saved {0}'.format(file_out)
  plt.close()

  return

#=========================================================================================
def dP_for_one_l_m(list_dics_dP, frame='corot', dic_star=None, el=1, em=-1, obs_P_from=1, obs_P_to=3, 
                   P_from=0, P_to=5, dP_from=1000, dP_to=10000, file_out=None):
  """
  Plot the period spacing dP for a single combination of el and em, but for various files,
  each comming with different freq_rot.
  @param frame: The choice of the frame of reference, in which dP is plotted. It can only be 
         "corot" for the co-rotating frame, or "inertial" for the inertial frame.
  @type frame: string
  """
  if file_out is None: return 
  if frame not in ['corot', 'inertial']:
    logging.error('plot_ledoux: dP_for_one_l_m: Wrong "frame" selected: '.format(frame))
    raise SystemExit, 'Error: plot_ledoux: dP_for_one_l_m: Wrong "frame" selected: '.format(frame)

  n_dics     = len(list_dics_dP)
  the_key_P  = 'P_{0}_l{1:1d}_m{2:+1d}'.format(frame, el, em)
  the_key_dP = 'dP_{0}_l{1:1d}_m{2:+1d}'.format(frame, el, em)
  first      = list_dics_dP[0]
  keys       = first.keys()
  if the_key_P not in keys or the_key_dP not in keys:
    logging.error('dP_for_one_l_m: P or dP not computed yet. Call ledoux.dP_for_corot_and_inert_modes() first')
    raise SystemExit, 'Error: plot_ledoux: dP_for_one_l_m: P or dP not computed yet. Call ledoux.dP_for_corot_and_inert_modes() first'

  if 'Led_freq_rot' not in keys:
    logging.error('dP_for_one_l_m: P or dP not computed yet. "Led_freq_rot" not in keys()')
    raise SystemExit, 'Error: dP_for_one_l_m: P or dP not computed yet. "Led_freq_rot" not in keys()'

  fig, ax     = plt.subplots(1, figsize=(6, 4))
  markers     = itertools.cycle(['o', 'o', 's', 's', '*', '*', '^', '^', '+', 'x'])
  face_colors = itertools.cycle(['c', 'w', 'b', 'w', 'r', 'w', 'g', 'w', 'm', 'w'])
  edge_colors = itertools.cycle(['c', 'k', 'b', 'b', 'r', 'r', 'g', 'g', 'm', 'm'])

  ax.fill_between([obs_P_from, obs_P_to], y1=0, y2=1e6, color='purple', alpha=0.15, zorder=1)

  freq_units = first['freq_units']
  if freq_units == 'Hz':  conv_factor = 1.0
  if freq_units == 'cd':  conv_factor = dic_conv['cd_to_Hz']
  if freq_units == 'uHz': conv_factor = dic_conv['uHz_to_Hz']

  for i_dic, dic in enumerate(list_dics_dP):
    P        = dic[the_key_P]
    dP       = dic[the_key_dP] / conv_factor

    M_star   = dic['M_star']
    R_star   = dic['R_star']
    Omega_rot_crit = tar.get_omega_crit(M_star, R_star, to_cgs=False)
    eta_rot  = dic['Led_freq_rot'] / (Omega_rot_crit / conv_factor)

    f_clr    = face_colors.next()
    e_clr    = edge_colors.next()
    mkr      = markers.next()
    lbl      = '{0:.4f}'.format(eta_rot)

    ax.plot(P[:-1], dP, color=e_clr, mfc=f_clr, mec=e_clr, marker=mkr, ms=4, zorder=2, label=lbl)

  if dic_star is not None:
    d_to_sec = dic_conv['d_to_sec']
    list_dic_freq = dic_star['list_dic_freq']
    obs_freq = np.array([dic['freq'] for dic in list_dic_freq])
    obs_P    = 1. / obs_freq
    ind      = np.argsort(obs_P)
    obs_P    = obs_P[ind]
    obs_dP   = obs_P[1:] - obs_P[:-1]
    obs_dP   *= d_to_sec
    ax.plot(obs_P[:-1], obs_dP, color='k', mfc='k', mec='k', marker='o', ms=2, zorder=3, label='Observed')

  ax.set_xlim(P_from, P_to)
  ax.set_ylim(dP_from, dP_to)
  if frame == 'corot':
    ax.set_xlabel(r'Period in Co-Rotating Frame [d]')
    ax.set_ylabel(r'Co-Rotating Period Spacing [sec]')
  if frame == 'inertial':
    ax.set_xlabel(r'Period in Inertial Frame [d]')
    ax.set_ylabel(r'Inertial Period Spacing [sec]')

  if n_dics <= 21:
    leg = ax.legend(loc=0, fontsize=6, frameon=False, fancybox=False)
  else:
    print ' - plot_ledoux: dP_for_one_l_m: Skipped putting the Legend.'

  plt.savefig(file_out, transparent=True)
  print ' - plot_ledoux: dP_for_one_l_m: saved {0}'.format(file_out)
  plt.close()

  return None

#=========================================================================================
def all_dP_in_dic(dic, P_from=None, P_to=None, file_out=None):
  """
  Plot all splitted and non-splitted period spacings on a single figure
  """
  if file_out is None: return 
  freq_units = dic['freq_units']
  if freq_units == 'Hz':  conv_factor = 1.0
  if freq_units == 'cd':  conv_factor = dic_conv['cd_to_Hz']
  if freq_units == 'uHz': conv_factor = dic_conv['uHz_to_Hz']

  fig, ax = plt.subplots(1, figsize=(6, 4))

  keys = dic.keys()
  dP_keys = []
  for key in keys:
    if 'dP_inertial_' in key:
      dP_keys.append(key)

  n_dP_keys = len(dP_keys)
  if n_dP_keys == 0: 
    logging.warning('plot_ledoux: dP: You have not called ledoux.dP_for_corot_and_inert_modes() yet!')
    print 'Warning: plot_ledoux: dP: You have not called ledoux.dP_for_corot_and_inert_modes() yet!'
    return None 

  markers     = itertools.cycle(['o', 'o', 's', 's', '*', '*', '^', '^', '+', 'x'])
  face_colors = itertools.cycle(['k', 'w', 'b', 'w', 'r', 'w', 'g', 'w', 'm', 'w'])
  edge_colors = itertools.cycle(['k', 'k', 'b', 'b', 'r', 'r', 'g', 'g', 'm', 'm'])

  for dP_key in dP_keys:
    P_key    = dP_key[1:] # dP_key.replace('dP_inertial_', 'P_inertial_') 
    P_inert  = dic[P_key] 
    dP_inert = dic[dP_key] / conv_factor 

    f_clr    = face_colors.next()
    e_clr    = edge_colors.next()
    mkr      = markers.next()
    lbl      = dP_key.replace('dP_inertial_', '')
    lbl      = lbl.replace('l', 'l=')
    lbl      = lbl.replace('m', ', m=')
    lbl      = lbl.replace('_', ' ')

    ax.plot(P_inert[:-1], dP_inert, color=e_clr, mfc=f_clr, mec=e_clr, marker=mkr, lw=1, label=lbl)

  ax.set_xlabel(r'Period [d]')
  ax.set_ylabel(r'Period Spacing \, \, $dP_n=P_{n+1}-P_n$ [sec]')

  leg = ax.legend(loc=2)

  plt.tight_layout()
  plt.savefig(file_out, transparent=True)
  print ' - plot_ledoux: dP: saved {0}'.format(file_out)
  plt.close()

  return None

#=========================================================================================

"""
This module provides simple sets of tools to rewrite a MESA output file in the form of history or profile into an 
HDF5 file. For further detail refer to http://h5py.org
"""

try:
  import h5py
except:
  print 'h5py not installed! \n sudo pip install h5py'
#import tables
#from tables import Int32Col, Float64Col
import numpy as np
import sys, os, time
import argparse
import logging
import read


#=================================================================================================================
def replace_with_h5(mesa_file):
  """
  This function passes the input filename (of a MESA history/profile file) to write_h5 function, and after the file
  is converted to HDF5 format, deletes the original file.
  Warning: Use with caution, otherwise you entirely loose your source ascii file, and end up with an improper HDF5 
  file.
  @param mesa_file: full path to the valid MESA history/profile file.
  @type mesa_file: string
  @return: None
  @rtype: None
  """
  if not os.path.isfile(mesa_file):
    message = 'Error: ascii2h5: replace_with_h5: %s does not exist! ' % (mesa_file, )
    raise SystemExit, message
  
  write_h5(mesa_file)
  os.remove(mesa_file)
  
  return None
  
#=================================================================================================================
def write_h5(mesa_file):
  """
  This function receives a single MESA history or profile filename, and writes an HDF5 file side-by-side to it.
  The data are stored in the main group '/', and the headers of the MESA file are used as attributes the the .h5 file.
  @param mesa_file: full path to the valid MESA history/profile file.
  @type mesa_file: string
  @return: None
  @rtype: None
  """
  
  if not os.path.isfile(mesa_file):
    message = 'Error: ascii2h5: write_h5: %s does not exist! ' % (mesa_file, )
    raise SystemExit, message
  
  # Read the input MESA file, and store the output as a record array
  try:
    header, data = read.read_mesa_ascii(mesa_file)
    hdr_names  = header.dtype.names
    data_names = data.dtype.names
    data_dtype = read.get_dtype(data_names)
    data_size  = len(data[data_names[0]])
  except:
    print 'Error: ascii2h5: write_h5: %s is corrupted'.format(mesa_file)
    logging.error('ascii2h5: write_h5: %s is corrupted'.format(mesa_file))
    return None

  # Open the file
  out_h5 = convert_extension(mesa_file)
  f = h5py.File(out_h5, 'w')
  #f = tables.openFile(out_h5, mode='w')
  
  # Create the main data group
  main_group = f.create_group('profile')
  #main_group = f.createGroup(f.root, 'profile')

  # Add the filename and the profile header as the attributes to the main group
  source = mesa_file[mesa_file.rfind('/')+1:]
  main_group.attrs['source'] = source
  for i_hdr, hdr_name in enumerate(hdr_names):
    main_group.attrs[hdr_name] = header[hdr_name][0]
  #main_group._v_attrs['source'] = source
  #for i_hdr, hdr_name in enumerate(hdr_names):
    #main_group._v_attrs[hdr_name] = header[hdr_name][0]

  # Iteratively, create dataset arrays in the main_group of the file, and store each MESA profile column
  # with the same names as datasets.
  for i_name, data_name in enumerate(data_names):
    col_type = data_dtype[i_name]
    dset = main_group.create_dataset(data_name, (data_size, ), dtype=col_type)
    dset[...] = data[data_name]
    #f.createArray(main_group, name, data[name])

  # Close the file
  try:
    f.close()
  except IOError:
    print 'Warning: ascii2h5: write_h5: Failed to close: %s. Sleep for 5 sec.' % (out_h5, )
    time.sleep(5)
    f.close()
  
  return None

#=================================================================================================================
def convert_extension(filename):
  """
  This function converts the input filename into an *.h5 extension suitable for writing to disk as HDF5 file.
  It replaces the file extension with .h5
  @param filename: full path to the souce file
  @type filename: string
  @return: new filename
  @rtype: string
  """
  if len(filename) == 0:
    message = 'Error: ascii2h5: convert_extension: input filename is empty!'
    raise SystemExit, message
  
  ind_pt = filename.rfind('.')
  return filename.replace(filename[ind_pt:], '.h5')
  
#=================================================================================================================
def replace(list_files):
  """
  This function receives a list of MESA filenames from ascii2h5, and calls replace_with_h5 iteratively.
  Warning: This comes without any warranty. Use with caution, otherwise you entirely loose your source ascii file, 
  and end up with an improper HDF5 file.
  @param list_files: list of strings, where each item in the list gives the full path to a single or multiple MESA output
         ascii files that are either history or profiles.
  @type list_files: list of strings
  @return: None
  @rtype: None
  """
  # If a single file is input as a string, then put it in a list
  if type(list_files) is type('string'): list_files = [list_files]
  
  for i_file, file in enumerate(list_files):
    if not os.path.isfile(file):
      print ' - Warning: ascii2h5: replace: file: %s is missing' % (file, )
      logging.info('File: {0} is missing'.format(file))
    
    file_is_ascii = read.check_file_is_ascii(file)
    if not file_is_ascii: 
      print '{0} is not ascii'.format(file)
      logging.warning('File: {0} is not ascii!'.format(file))
      continue
    
    logging.info('Handling file {0}'.format(file))

    # Command to check whehter or not the file on the VSC is corrupted or not
    #cmd = 'file %s' % (file, )
    #result = os.popen(cmd).readlines()
    #if 'ASCII' in result:
      #replace_with_h5(file)
    #elif 'data' in result:
      #logging.info('{0} is corrupted'.format(file))
      #print 'Warning: ascii2h5: replace: %s is corrupted. file type: %s' % (file, result)
      #continue
    #else:
      #logging.info('{0} is weired'.format(file))
      #print result
      #raise SystemExit, 'Error: ascii2h5: replace: %s is strange file' % (file, )
    replace_with_h5(file)
  
  return None

#=================================================================================================================
def ascii2h5():
  """
  This can be called like this:
    - For a single file replacement
    $:> ascii2h5 history.data
    - For multiple file replacements
    $:> ascii2h5 /path/to/profiles/*.data
  """
  # Define the command line arguments
  parser = argparse.ArgumentParser(prog='ascii2h5', description='Replace MESA output ASCII files with HDF5 files.',
                      usage='%(prog)s Calling Syntax: $>ascii2h5 <full-path-to-MESA-file(s)> -v -extra=<full-path-to-namelist>',
                      epilog='For questions, contact e.moravveji@gmail.com', argument_default=None,
                      formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  # Create the mandatory positional argument
  parser.add_argument('ascii_in', metavar='ascii_in', type=str, nargs='*',
                      help='Single MESA history file or one/many profile files')
  # Create the optional arguments (they start with dash character, e.g. -v)
  parser.add_argument('-v', action="store_true", help="Verbose output")  
  
  # Parse the command line argument
  arg = vars(parser.parse_args())

  #if arg['-v']: logging.basicConfig(level=logging.INFO)
    
  # Call prepare_to_replace  
  ascii_in = arg['ascii_in']
  replace(ascii_in)
  
#=================================================================================================================
if __name__ == '__main__':
  ascii2h5()

#=================================================================================================================
  


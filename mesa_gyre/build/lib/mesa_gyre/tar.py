# coding: utf-8

"""
This module aims at translating the mode periods (frequencies) from a non-rotating frame to a co-rotating frame,
and later on to the observer's inertial frame following the formulae in
- B13: Bouabid M. P. et al. 2013, MNRAS, 429, 2500–2514
- T00: Townsend R. H. D. 2000, MNRAS
- T03: Townsend R. H. D. 2003, MNRAS, 340, 1020-1030
Here, we do such translation using the Traditional Approximation for Rotation, TAR.
Additional material can be found in 
- Eckart (1960)
- Lee and Saio (1993, 1997)
- Townsend (2000)

TO DO: 
  - this script is not tried even once; so, could have bugs
  - now we fit period spacing, but this is wrong; change everything to fit frequencies only
  - complete "freq_nonrot_to_freq_corot" subroutine
"""

import sys, os, glob, shutil
import logging, warnings
import time
import h5py
import subprocess
import socket
import numpy as np
from mesa_gyre import commons
import read
from submit_jobs import job_array_gyre as jag
from scipy.optimize import curve_fit
import trad, tradtable               # developed by Richard Townsend

warnings.filterwarnings('error')     # capture warnings like errors!

#=========================================================================================
dic_conv = commons.conversions()
Hz_to_cd = dic_conv['Hz_to_cd']
#=========================================================================================
#=========================================================================================
def gen_numbered_filename(dir_name, prefix, num_try, suffix):
    """
    For every iteration of the Simplex algorithm, a new file should be written to disk.
    The filename should carry the information about which try number it belongs to. So, the
    output filename has a fixed format:
          <dir_name>/<prefix>-NNN.<suffix>
    @param dir_name: directory name where the namelist will be stored.
           A trailing '/' character is added automatically if not present; this directory
           will be created if it does not exist
    @type dir_name: string 
    @param prefix, suffix: the prefix and suffix used to create the full filename
    @type prefix, suffix: string
    @param num_try: the counter for the attempt.
    @type num_try: integer
    @return: full path to where the namelist will be written to, by calling e.g. 
             write_nmlst_to_ascii()
    @rtype: string
    """
    if num_try >= 1000:
        logging.error('tar: gen_numbered_filename: Only 999 attempts are allowed')
        raise SystemExit, 'Error: tar: gen_numbered_filename: Only 999 attempts are allowed'

    if suffix[0] == '.': suffix = suffix[1:]
    if dir_name[-1] != '/': dir_name += '/'
    if not os.path.exists(dir_name): 
        print ' - tar: gen_numbered_filename: automatically creating {0}'.format(dir_name)
        os.makedirs(dir_name)

    return '{0}{1}-{2:03d}.{3}'.format(dir_name, prefix, num_try, suffix)

#=========================================================================================
def write_nmlst_to_ascii(nmlst, filename):
    """
    Flush the GYRE namelist data to an ASCII file. The namelist data, nmlst here, can be 
    read/produced by e.g. jag.read_sample_namelist_to_list_of_strings(nmlst_file), and could
    have been modified by tar.modify_nmlst() for the substitution of few selected fields.
    @param nmlst: GYRE-compatible namelist that is ready to be written to file 
    @type nmlst: list of strings
    @param filename: full path to the ASCII file where the data will be written to
    @type filename: string
    @return: None 
    @rtype: None
    """
    with open(filename, 'w') as w:
        w.writelines(nmlst)

    return None 

#=========================================================================================
def modify_nmlst(default_nmlst, list_dic_change):
    """
    This routine modifies the GYRE inlist information (passed as a list of strings by first 
    argument), and substitutes by the key, value pairs in the list of dictionaries. E.g.
    to change the name of the input file, one can call this routine as 
       new_nmlst = modify_nmlst(default_nmlst, [{'file':'model.gyre'}, {'summary_file':'output.h5'}])
    Note that the keys introduced in list_dic_change should be already available somewhere in 
    the default_nmlst as a whole or part of one of the strings
    @param default_nmlst: The GYRE namelist as read, and returned by 
          jag.read_sample_namelist_to_list_of_strings(namelist_file). Each line in the inlist 
          is a string in this list
    @type default_nmlst: list of strings
    @param list_dic_change: a collection of those fields that you want to modify. The key of 
          each dictionary should be a variable name in default_nmlst and should be positioned
          already on the left-hand-side of the "=" sign.
    @type list_dic_change: list of dictionaries
    @return: updated namelist with the same format as received, and with modified values
    @rtype: list of strings
    """
    if not isinstance(default_nmlst, list):
        logging.error('tar: modify_nmlst: input default_nmlst is not a list')
        raise SystemExit, 'Error: tar: modify_nmlst: input default_nmlst is not a list'

    n_lines = len(default_nmlst)
    if n_lines == 0:
        logging.error('tar: modify_nmlst: input default_nmlst is empty')
        raise SystemExit, 'Error: tar: modify_nmlst: input default_nmlst empty'

    n_change = len(list_dic_change)
    if n_change == 0: 
        return default_nmlst

    variables = []
    line_numbers = []
    for i, line in enumerate(default_nmlst):
        if '=' not in line: continue
        variable = line.split('=')[0].strip()    # remove leading and trailing white spaces
        variables.append(variable)
        line_numbers.append(i)

    keys_to_change = [dic.keys()[0] for dic in list_dic_change]
    new_nmlst       = default_nmlst[:]
    for i, key in enumerate(keys_to_change):
        try:
            ind = variables.index(key)
            num = line_numbers[ind]
        except ValueError:
            logging.error('tar: modify_nmlst: key:{0} not available'.format(key))
            raise SystemExit, 'Error: tar: modify_nmlst: key:{0} not available'.format(key)
        value   = list_dic_change[i][key]
        new_nmlst[num] = "  {0} = {1} \n".format(key, value)

    return new_nmlst

#=========================================================================================
def Secant_eta_rot_slope(eta_rot_a, eta_rot_b, N_dev_a, N_dev_b):
  """
  The slope of a line passing through two points, where the abscissa is associated with the trial
  rotation rate (w.r.t. the critical rotation rate), and the ordinate is the discreminant function
         slope = (Y_b - Y_a) / (X_b - X_a)
  @param eta_rot_a: first trial rotation rate 
  @type eta_rot_a: float 
  @param eta_rot_b: last trial rotation rate 
  @type eta_rot_b: float 
  @param N_dev_a: first value of the discreminant corresponding to eta_rot_a
  @type N_dev_a: integer
  @param N_dev_b: last value of the discreminant corresponding to eta_rot_b
  @type N_dev_b: integer
  @return: slope of the line connecting the two points 
  @rtype: float
  """

  return (N_dev_b - N_dev_a) / (eta_rot_b - eta_rot_a)

#=========================================================================================
def Brent_guess_next_eta_rot(list_eta_rot, list_N_dev):
  """
  Following Eqs. (9.3.2), (9.3.3) and (9.3.4) in Sect. 9.3 of the famous Numerical Recipes 
  (Press et al. 2007) for the Van Wijngaarden-Dekker-Brent scheme, this function tries to 
  update the guess for eta_rot around the middle point (which provides the closest point to 
  the zero). The three requird points are (a, f(a)), (b, f(b)) and (c, f(c)), where 
  a < b < c, and f(a) < f(b) < f(c).
  This is useful to use, specifically, after the first two initial attempts (by calling
  two_initial_eta_attempts()) followed by a Secant method (by calling Secant_guess_next_eta_rot())
  to guess the position of the midpoint (b).
  """
  if isinstance(list_eta_rot, list): list_eta_rot = np.array(list_eta_rot)
  if isinstance(list_N_dev, list):   list_N_dev   = np.array(list_N_dev)

  ind            = np.argsort(list_eta_rot)
  list_eta_rot   = list_eta_rot[ind]
  list_N_dev     = list_N_dev[ind]

  if list_N_dev[-1] == list_N_dev[-2] and len(list_N_dev) > 3:
    list_eta_rot    = np.delete(list_eta_rot, -2)
    list_N_dev      = np.delete(list_N_dev, -2)

  if len(list_eta_rot) < 3:
    logging.error('tar: Brent_guess_next_eta_rot: Input lists should have at least three values!')
    raise SystemExit, 'Error: tar: Brent_guess_next_eta_rot: Input lists should have at least three values!'

  if len(list_eta_rot) > 3:
    list_eta_rot = list_eta_rot[-3 : ]
    list_N_dev   = list_N_dev[-3 : ]

  a   = list_eta_rot[0]
  b   = list_eta_rot[1]
  c   = list_eta_rot[2]
  f_a = list_N_dev[0]
  f_b = list_N_dev[1]
  f_c = list_N_dev[2]

  R   = float(f_b) / f_c
  S   = float(f_b) / f_a
  T   = float(f_a) / f_c

  P   = S * ( T * (R-T) * (c-b) - (1.0-R) * (b-a) )
  Q   = (T-1.0) * (R-1.0) * (S-1.0)

  try: 
    x   = b + P / Q
  except:
    logging.error('tar: Brent_guess_next_eta_rot: Q=0')
    logging.error('   eta_rot = {0:.4f}, {1:.4f}, {2:.4f}'.format(list_eta_rot[0], list_eta_rot[1], list_eta_rot[2]))
    logging.error('   N_dev   = {0}, {1}, {2}'.format(list_N_dev[0], list_N_dev[1], list_N_dev[2]))
    logging.error('   ')
    raise SystemExit, '   eta_rot = {0:.4f}, {1:.4f}, {2:.4f}'.format(list_eta_rot[0], list_eta_rot[1], list_eta_rot[2])
    raise SystemExit, '   N_dev   = {0}, {1}, {2}'.format(list_N_dev[0], list_N_dev[1], list_N_dev[2])

  return x

#=========================================================================================
def Newton_Raphson_guess_next_eta_rot(list_eta_rot, list_N_dev, n_poly=1):
  """
  When more than three points are available for (eta_rot, N_dev), it is possible to fit a 
  low-order polynomial to these points, and analytically derive the function derivatives, 
  to fascilitate faster convergence to the root of the discreminant function. This is more
  useful for dipole prograde (m=-1, as defined by Bouabid+2013, Fig. 4, right panel) modes,
  where the Secant method can fall into weird issues in fine-tuning eta_rot. For the 
  Newton-Raphson algorithm to work progressively better and better, we need to use the history
  of former root-finding attempts (where in the first two steps is done with the Secant method).
  Therefore, we pass the list of eta_rot and deviations found in former steps for previous 
  attempts.
  """
  if isinstance(list_eta_rot, list): list_eta_rot = np.array(list_eta_rot)
  if isinstance(list_N_dev, list):   list_N_dev   = np.array(list_N_dev)
  ind_sort     = np.argsort(list_eta_rot)
  list_eta_rot = list_eta_rot[ind_sort]
  list_N_dev   = list_N_dev[ind_sort]

  #####################################
  # Curve Fitting; Function Definitions
  #####################################
  def cubic(x, a, b, c, d):
    """
    Fit function is f(x) = a*x^3 + b*x^2 + c*x + d 
    """
    return a*x*x*x + b*x*x + c*x + d

  def deriv_cubic(x, a, b, c, d):
    """
    derivative of a cubic spline is f'(x) = 3*a*x^2 + 2*b*x + c
    """
    return 3.0*a*x*x + 2*b*x + c

  def parabola(x, a, b, c):
    """
    Fit function is f(x) = a*x*x + b*x + c
    """
    return a * x * x + b * x + c

  def deriv_parabola(x, a, b, c):
    """
    derivative of a parabola is f'(x) = 2*a*x + b
    """
    return 2.0 * a * x + b

  def line(x, a, b):
    """
    Fit function is f(x) = a*x + b
    """
    return a * x + b

  def deriv_line(x, a, b):
    """
    derivative of a line is f'(x) = a
    """
    return a

  #####################################
  # Do Curve Fitting, and Compute the 
  # Next Rotation Rate
  #####################################
  eta_rot_n      = list_eta_rot[-1]
  n_data         = len(list_eta_rot)
  max_poly       = 3
  n_poly         = min([ n_poly, n_data - 1 ]) 
  n_poly         = min([ max_poly, n_poly])

  print 'n_data={0}, n_poly={1}'.format(n_data, n_poly)

  if n_poly == 3:
    fit = curve_fit(f=cubic, xdata=list_eta_rot, ydata=list_N_dev)
    optimal_params = fit[0]
    covar_matrix   = fit[1]
    a              = optimal_params[0]
    b              = optimal_params[1]
    c              = optimal_params[2]
    d              = optimal_params[3]
    f_n            = cubic(eta_rot_n, a, b, c, d)
    slope_n        = deriv_cubic(eta_rot_n, a, b, c, d)

  if n_poly == 2:
    fit = curve_fit(f=parabola, xdata=list_eta_rot, ydata=list_N_dev)
    optimal_params = fit[0]
    covar_matrix   = fit[1]
    a              = optimal_params[0]
    b              = optimal_params[1]
    c              = optimal_params[2]
    f_n            = parabola(eta_rot_n, a, b, c)
    slope_n        = deriv_parabola(eta_rot_n, a, b, c)

  if n_poly == 1:
    fit = curve_fit(f=line, xdata=list_eta_rot, ydata=list_N_dev)
    optimal_params = fit[0]
    covar_matrix   = fit[1]
    a              = optimal_params[0]
    b              = optimal_params[1]
    f_n            = line(eta_rot_n, a, b)
    slope_n        = deriv_line(eta_rot_n, a, b)

  eta_rot_n_plus_1 = eta_rot_n - f_n / slope_n

  return eta_rot_n_plus_1

#=========================================================================================
def Secant_guess_next_eta_rot(eta_rot_n, N_deviation_n, slope_n):
  """
  Use a Secant method to guess the zero of the discreminant function, i.e. 
  the zero of the number of model frequencies subtracting the number of observed frequencies
  (see two_initial_eta_attempts() for more info). The slope is assumed from a line passing the
  former two attempts. We are forced to use Secant method because the discreminant is not analytic
  and its derivative cannot be computed analytically.
  In all input variable naming, the extension "_n" means the input value at the iteration step "n".
  The Secant method is 
         x_{n+1} = x_n - f(x_n) / f'(x_n)
  where f(x_n) is the function (that we try to seek its zero) evaluated at x_n, and f'(x_n) is
  its slope at the same mesh point.
  @param eta_rot_n: the former trial rotation rate at step "n"-th.
  @type eta_rot_n: float
  @param N_deviation_n: deviations of the number of model frequencies from observed frequencies
         which serves as our discreminant function (to be minimized)
  @type N_deviation_n: integer
  @param slope_n: the derivative of the discreminant from the former two attempts, assuming a line
         passing through the two former points (hence Secant method)
  @type slope_n: float
  @return: the guess for the next eta_rot that can (hopefully) serve as the zero of the discreminant
         function. In case the input slope (from the initial two attempts) is zero, returns False. 
         In this case, the issue is handled by another function to resolve the case.
  @rtype: float
  """
  if slope_n == 0:
    logging.warning('tar: Secant_guess_next_eta_rot: slope_n == 0: Retrying with different eta_rot')
    # raise SystemExit, 'Error: tar: Secant_guess_next_eta_rot: slope_n == 0'
    return False

  eta_rot_n_plus_1 = eta_rot_n - N_deviation_n / slope_n
  if eta_rot_n_plus_1 < 0:
    logging.error('tar: Secant_guess_next_eta_rot: eta_rot_n_plus_1 < 0')
    raise SystemExit, 'tar: Secant_guess_next_eta_rot: eta_rot_n_plus_1 < 0'

  return eta_rot_n_plus_1

#=========================================================================================
def retry_first_three_attempts(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file,
                      dir_nmlst, dic_star, eta_rot_0, eta_rot_1, sigma):
    """
    This is a "rescue" function :-D
    It can happen that during the first two initial attempts plus the Secant attempt, the slope 
    found after the Secant method is zero. In such cases, the two_initial_eta_attempts() should
    be called again, but with "stretched" initial (and trial) eta_rot, to make sure we do not fall
    into this gory situations again.
    For the meaning of the input, refer to one_attempt() function.
    """
    list_dic_freq = dic_star['list_dic_freq']
    N_obs_modes = len(list_dic_freq)

    max_iter     = 21
    stretch_low  = 0.90
    stretch_high = 1.10

    rescued     = False
    for i in range(max_iter):
      eta_rot_0 = eta_rot_0 * stretch_high
      eta_rot_1 = eta_rot_1 * stretch_high

      tup_res   = two_initial_eta_attempts(which_gyre, dic_model, default_nmlst, gyre_in_file, 
                              gyre_out_file, dir_nmlst, dic_star, eta_rot_0, eta_rot_1, sigma)
      list_dev  = tup_res[0]
      slope     = tup_res[1]
      N_dev_0   = list_dev[0]
      N_dev_1   = list_dev[1]

      eta_rot_try = Secant_guess_next_eta_rot(eta_rot_n=eta_rot_1, N_deviation_n=N_dev_1, slope_n=slope)

      tup_try   = one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file, 
                                  dir_nmlst, dic_star, eta_rot_try, sigma)
      N_modes   = tup_try[0]
      N_dev     = N_modes - N_obs_modes

      rescued   = N_dev != N_dev_1
      if rescued:
        return eta_rot_try

    if not rescued:
      logging.error('tar: retry_first_three_attempts: Could not resolve the issue after {0} loops'.format(max_iter))
      raise SystemExit, 'Error: tar: retry_first_three_attempts: Could not resolve the issue after {0} loops'.format(max_iter)

#=========================================================================================
def two_initial_eta_attempts(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file,
                dir_nmlst, dic_star, eta_rot_0, eta_rot_1, sigma):
    """
    Make two attempts to call GYRE, compute the frequencies for the two proposed rotation rates 
    (eta_rot_0 and eta_rot_1) with the desired GYRE version/executable, and return the number of 
    frequencies N(eta_rot) within the observed range + the slope of a line connecting N(eta_rot_0)
    and N(eta_rot_1), for later Newton-Raphson root finder.
    @param which_gyre: The specific gyre ad nor nad and the version to call GYRE. You can 
           supply the full path to the GYRE executable, e.g. /home/me/gyre/bin/gyre_ad
           This is actually the recommended approach.
    @type which_gyre: string 
    @param dic_model: the input data as a dictionary, which is read directly from the gyre_in file.
           This is done by e.g. calling read.read_gyre_in(filename). 
    @type dic_model: dictionary
    @param dir_nmlst: full path to a directory where the namelists would be dumped. 
    @type dir_nmlst: string
    @param default_nmlst: The GYRE namelist as read, and returned by 
          jag.read_sample_namelist_to_list_of_strings(namelist_file). Each line in the inlist 
          is a string in this list
    @type default_nmlst: list of strings
    @param gyre_in_file: full path to the input GYRE file that contains the whole structure
          info, and is used to compute frequencies. 
    @type gyre_in_file: string
    @param gyre_out_file: the full path filename where the intermediate-step GYRE output files will 
          be stored. 
    @type gyre_out_file: string
    @param dir_nmlst: the directory where the intermediate-step GYRE inlist files will be stored.
          Note that it is created upon the first access if it does not exist.
    @type dir_nmlst: string 
    @param dic_star: dictionary containing information of the star; this is normally returned 
          from mesa_gyre.stars module
    @type dic_star: dict
    @param eta_rot_0, eta_rot_1: the first two initial trial rotation rates, w.r.t. the critical 
          rotation frequency.
    @type eta_rot_0, eta_rot_1: float
    @param sigma: multiplication factor for the frequency errors of the first and last mode, 
          around which the model frequencies are enumerated. One should make sure not to exceed
          the frequency difference between two consecutive modes, by allowing very large sigma.
          For KIC 7760680, sigma=9 would be appropriate, taking care of frequency difference between
          f_35 and f_36 in Papics et al. (2015, ApJL) Table 1.
    @type sigma: float
    @return: Two items are returned:
          1. a list of the deviations from observed number of modes for both attempts. 
             Thus, it looks like the following: [N_deviation_0, N_deviation_1] which is 
             a list of floats
          2. the slope of the line connecting (eta_rot_0, N_deviation_0) and 
             (eta_rot_1, N_deviation_1), which is a float
    @rtype: tuple
    """
    N_obs_freq      = len(dic_star['list_dic_freq'])

    stretch_high    = 1.10

    # Make one attempt for the first trial frequency
    res_0           = one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, 
                                  gyre_out_file, dir_nmlst, dic_star, eta_rot_0, sigma)
    if res_0 is False:
      while res_0 is False:
        eta_rot_0   = eta_rot_0 * stretch_high
        res_0       = one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, 
                                  gyre_out_file, dir_nmlst, dic_star, eta_rot_0, sigma)
      # logging.error('tar:two_initial_eta_attempts: first call to one_attempt() failed')
      # print 'Error: tar: two_initial_eta_attempts: first call to one_attempt() failed'
      # return False
    N_0             = res_0[0]
    dic_freq_0      = res_0[1]


    # Make a second attempt for the second trial frequency
    res_1           = one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, 
                                  gyre_out_file, dir_nmlst, dic_star, eta_rot_1, sigma)
    if res_1 is False:
      while res_1 is False:
        eta_rot_1   = eta_rot_1 * stretch_high
        res_1       = one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, 
                                  gyre_out_file, dir_nmlst, dic_star, eta_rot_1, sigma)
      # logging.error('tar:two_initial_eta_attempts: second call to one_attempt() failed')
      # print 'Error: tar: two_initial_eta_attempts: second call to one_attempt() failed'
      # return False
    N_1             = res_1[0]
    dic_freq_1      = res_1[1]

    # Find the deviations of the number of model frequencies from observations for the 
    # trial rotation rates
    N_deviation_0   = N_0 - N_obs_freq
    N_deviation_1   = N_1 - N_obs_freq

    slope           = Secant_eta_rot_slope(eta_rot_a=eta_rot_0, eta_rot_b=eta_rot_1, 
                                           N_dev_a=N_deviation_0, N_dev_b=N_deviation_1)

    return ([N_deviation_0, N_deviation_1], slope)

#=========================================================================================
def one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file, dir_nmlst,
                dic_star, eta_rot, sigma):
    """
    Make a single attempt to call GYRE, and compute the frequencies with the desired GYRE 
    version/executable, and the input namelist file, specified by its path.
    @param which_gyre: The specific gyre ad nor nad and the version to call GYRE. You can 
           supply the full path to the GYRE executable, e.g. /home/me/gyre/bin/gyre_ad
           This is actually the recommended approach.
    @type which_gyre: string 
    @param dic_model: the input data as a dictionary, which is read directly from the gyre_in file.
           This is done by e.g. calling read.read_gyre_in(filename). 
    @type dic_model: dictionary
    @param dir_nmlst: full path to a directory where the namelists would be dumped. 
    @type dir_nmlst: string
    @param default_nmlst: The GYRE namelist as read, and returned by 
          jag.read_sample_namelist_to_list_of_strings(namelist_file). Each line in the inlist 
          is a string in this list
    @type default_nmlst: list of strings
    @param gyre_in_file: full path to the input GYRE file that contains the whole structure
          info, and is used to compute frequencies. 
    @type gyre_in_file: string
    @param gyre_out_file: the full path filename where the intermediate-step GYRE output files will 
          be stored. Note that it is created upon the first access if it does not exist.
    @type gyre_out_file: string
    @param dir_nmlst: the directory where the intermediate-step GYRE inlist files will be stored.
          Note that it is created upon the first access if it does not exist.
    @type dir_nmlst: string 
    @param dic_star: dictionary containing information of the star; this is normally returned 
          from mesa_gyre.stars module
    @type dic_star: dict
    @param eta_rot: The trial rotation rate w.r.t. the breakup rotation rate; thus, 
          0 <= eta_rot <= 1. 
    @type eta_rot: float
    @param sigma: multiplication factor for the frequency errors of the first and last mode, 
          around which the model frequencies are enumerated. One should make sure not to exceed
          the frequency difference between two consecutive modes, by allowing very large sigma.
          For KIC 7760680, sigma=9 would be appropriate, taking care of frequency difference between
          f_35 and f_36 in Papics et al. (2015, ApJL) Table 1.
    @type sigma: float
    @return: number of model frequencies within the observed range (N_model_freq), and the full 
          list of resulting frequencies (dic_freq)
    @rtype: tuple
    """
    # Step 01) Substitute the gyre_in_file to default_nmlst to make an attempt
    #          Set the freq_rot and omega_rot
    #          Substitute a temporary file for the output for the first attempt
    freq_rot_crit = get_omega_crit(M_star=dic_model['M_star'], R_star=dic_model['R_star'], to_cgs=False)
    freq_rot      = eta_rot * freq_rot_crit
    omega_rot     = 2.0 * np.pi * freq_rot
    nmlst_step_01 = modify_nmlst( default_nmlst, list_dic_change=[
                    {'file':'"{0}"'.format(gyre_in_file)},
                    {'summary_file':'"{0}"'.format(gyre_out_file)}, 
                    {'Omega_uni':omega_rot} 
                    ] ) 

    # Step 02) Flush the nmlst from the last two steps into an ASCII file for the attempt
    path_nmlst    = gen_numbered_filename(dir_nmlst, 'attempt', 1, 'nmlst')
    write_nmlst_to_ascii(nmlst_step_01, path_nmlst)

    # Step 03) Find the two observed extreme frequency values that specify the observed range
    list_dic_freq  = dic_star['list_dic_freq']
    list_freq      = np.array([dic['freq'] for dic in list_dic_freq])
    list_freq_err  = np.array([dic['freq_err'] for dic in list_dic_freq])
    ind_sort       = np.argsort(list_freq)
    list_freq      = list_freq[ind_sort]
    list_freq_err  = list_freq_err[ind_sort]
    min_obs_freq   = list_freq[0]  - sigma * list_freq_err[0]
    max_obs_freq   = list_freq[-1] + sigma * list_freq_err[-1]

    # Step 04) Call GYRE, and store the frequencies in the file specified by gyre_out_file
    success = call_gyre(which_gyre, path_nmlst)
    if not success:
        logging.error('tar: one_attempt: call_gyre failed: {0}'.format(gyre_in_file))
        return False
        # raise SystemExit, 'Error: tar: one_attempt: call_gyre did not succeed.'

    # time.sleep(10)

    # Step 05) Add the rotation rate, eta_rot, to the file attribute
    add_extra_attributes_to_gyre_out_file(gyre_out_file, list_dic_attrs=[ {'eta_rot':eta_rot} ])

    # Step 06) Enum5rate the resulting number of model frequencies within observed range
    try:
      dic_freq      = read.read_multiple_gyre_files([gyre_out_file])[0]
    except ValueError:
      logging.error('tar one_attempt: failed to read: {0}'.format(gyre_out_file))
      raise SystemExit, 'Error: tar one_attempt: failed to read: {0}'.format(gyre_out_file)

    dic_freq['omega_crit']  = 2.0 * np.pi * freq_rot_crit
    dic_freq['eta_rot']     = eta_rot
    freq_mod      = np.real(dic_freq['freq']) * Hz_to_cd
    n_pg          = dic_freq['n_pg']

    ind           = np.where((freq_mod >= min_obs_freq) & (freq_mod <= max_obs_freq) )[0]
    N_model_freq  = len(ind)

    print '   one_attempt: eta_rot={0:0.4f}: n_freq={1}'.format(eta_rot, N_model_freq)

    return (N_model_freq, dic_freq)

#=========================================================================================
def call_gyre(which_gyre, path_nmlst):
    """
    This routine is copied from inversion.tools.py.
    This funtion creates an appropriate bash command to call and execute GYRE. Then, it waits until
    the run is over, and returns True if the execution succeeds. Otherwise, it returns False.
    @param which_gyre: full path to the GYRE executable. This should be decently provided by the calling
            script. It can be either gyre_ad or gyre_nad.
            I advice to provide the full path to the specific gyre executable that also specified the GYRE verison.
            E.g. which_gyre = '/home/user/gyre4.0/bin/gyre_nad'
            An additional space will be automatically added aftre which_gyre
    @type which_gyre: string
    @param path_nmlst: full path to where the input namelist for this specific job is stored, and GYRE will
            use the specified settings therein
    @return: True if succeeds, or False if fails
    @rtype: boolean
    """
    if not os.path.exists(path_nmlst):
        logging.error('tar: call_gyre: {0} does not exist'.format(path_nmlst))
        raise SystemExit, 'Error: tar: call_gyre: {0} does not exist'.format(path_nmlst)

    if not isinstance(which_gyre, str) or not isinstance(path_nmlst, str):
        logging.error('tar: call_gyre: which_gyre and path_nmlst should be string')
        raise SystemExit, 'Error: tar: call_gyre: which_gyre and path_nmlst should be string'

    cmnd = which_gyre + ' ' + path_nmlst

    exe = subprocess.Popen(cmnd, stdout=subprocess.PIPE, shell=True)
    output, error = exe.communicate()
    status = exe.wait()

    if error is not None: 
      logging.error('tar: call_gyre: returned error: {0}'.format(error))
      return False

    return status == 0

#=========================================================================================
def scan_eta_rot(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file, dir_nmlst,
                dic_star, sigma, eta_rot_from, eta_rot_to, eta_rot_num):
  """
  For only one input model, can one_attempt() iteratively, with trial rotation rates eta_rot 
  discretized between eta_rot_from and eta_rot_to in eta_rot_num number of steps, collect back
  the result as a list of tuple. Each item in the list (i.e. each tuple) contains the number 
  of modes between the observed frequency range (passed from inside dic_star), and the resulting 
  frequency list for later plotting. This is useful to show the evolution of the frequencies
  and/or periods as a function of trial 
  @return: if one_attempt() crashes, this function returns False just to capture the error
  """
  eta_rot_arr = np.linspace(eta_rot_from, eta_rot_to, eta_rot_num, endpoint=True)
  list_tup    = []

  for eta_rot in eta_rot_arr:
    tup_res   = one_attempt(which_gyre, dic_model, default_nmlst, gyre_in_file, gyre_out_file, dir_nmlst,
                dic_star, eta_rot, sigma)
    if tup_res is False: 
      logging.error('tar: scan_eta_rot: one_attempt failed')
      return False
    list_tup.append(tup_res)

  return list_tup

#=========================================================================================
def add_extra_attributes_to_gyre_out_file(gyre_out, list_dic_attrs):
  """
  The optimised rotation information should be stored along with the rest of the information
  that GYRE writes out by default. This is useful for reference to know later, what rotation
  rates gave the best match to the number of observed frequencies, and for later plottings.
  This routine adds any desired list of such information as file attributes to the originally
  existing GYRE output HDF5 file.
  @param gyre_out: full path to the GYRE output HDF5 file 
  @type gyre_out: string 
  @param list_dic_attrs: list of dictionaries that contain the attributes to be added. Each 
         attribute should be only passed as one key-value item in the dictionary. In other words,
         to pass N attributes, one should pass N dictionaries in this list.
  @type list_dic_attrs: list of dictionaries
  @return: None 
  @rtype: None
  """
  if not os.path.exists(gyre_out):
    txt = 'Error: tar: {0}: {1} does not exist; hostname={2}'.format(
           'add_extra_attributes_to_gyre_out_file', gyre_out, socket.gethostname())
    logging.error(txt)
    raise SystemExit, txt

  time.sleep(4)
  f = h5py.File(gyre_out, 'r+')
  for dic in list_dic_attrs:
    keys = dic.keys()
    if len(keys) != 1:
      logging.error('tar: add_extra_attributes_to_gyre_out_file: each attr dic should have one item()')
      raise SystemExit, 'tar: add_extra_attributes_to_gyre_out_file: each attr dic should have one item()'
    key = keys[0]
    f.attrs[key] = dic[key]
  f.close()

  return None

#=========================================================================================
def move_attempt_file_to_gyre_out_dir(attempt_out, gyre_out):
  """
  This function simply moves the temporary attempt_out file to a new location given by 
  gyre_out.
  @param attempt_out: full path to the temporary filename from previous attempts to 
         optimize eta_rot
  @type attempt_out: string 
  @param gyre_out: full path to the permanent filename
  @type gyre_out: string 
  @return: None 
  @rtype: None
  """
  # time.sleep(5)
  if not os.path.exists(attempt_out):
    logging.error('tar: move_attempt_file_to_gyre_out_dir: {0} does not exist!')
    raise SystemExit, 'Error: tar: move_attempt_file_to_gyre_out_dir: {0} does not exist!'

  # time.sleep(5)
  try:
    shutil.move(attempt_out, gyre_out)
    # os.rename(attempt_out, gyre_out) # this call fails miserably in many cases! weird !!!
  except OSError:
    raise SystemExit, 'move_attempt_file_to_gyre_out_dir: mv {0} -> {1}'.format(attempt_out, gyre_out)

  return None

#=========================================================================================
def gen_gyre_out_attempt_file(gyre_in, dir_attempt):
  """
  During the attempts for tuning the eta_rot, the intermediate-step outputs should be written
  out in a temporary directory. This ensures that if the job crashes for some reason, the 
  remaining file is not stored in the "gyre_out" sub-folders, and are not mistaken to be final
  files. Once the computations succeed to find the optimal eta_rot, then the attempt file 
  should be moved to the permanent "gyre_out" directory. The latter is achieved by a call 
  to move_attempt_file_to_gyre_out_dir().
  """
  if not os.path.exists(dir_attempt): os.makedirs(dir_attempt)
  if dir_attempt[-1] != '/': dir_attempt += '/'

  job_id       = commons.get_slurm_jobid()
  job_id_dir   = '{0:08d}/'.format(job_id)
  attempt_file = gen_gyre_out_from_gyre_in(gyre_in)

  ind          = attempt_file.rfind('/')
  attempt_file = attempt_file[ind + 1 : ]

  job_dir      = dir_attempt + job_id_dir
  if not os.path.exists(job_dir): os.makedirs(job_dir)

  return job_dir + attempt_file

#=========================================================================================
def gen_dir_gyre_out_from_gyre_in(file_in):
  """
  Generate the GYRE output directory from the file pattern in the input file, by removing
  the trailing file name, and substituting the "gyre_in" phrase with "gyre_out", instead. 
  """
  ind_slash = file_in.rfind('/')
  dir_gyre_out = file_in[0 : ind_slash + 1]
  dir_gyre_out = dir_gyre_out.replace('gyre_in', 'gyre_out')
  if not os.path.exists(dir_gyre_out): os.makedirs(dir_gyre_out)

  return dir_gyre_out

#=========================================================================================
def gen_gyre_out_from_gyre_in(file_in):
  """
  Generate the GYRE output file from the GYRE input file pattern, by substituting the "gyre_in" 
  phrase with "gyre_out", and ".gyre" with ".h5". Additionally, all GYRE output files start with
  "ad-sum-" to highlight the fact that they are adiabatic summary files.
  """
  if '-sc' in file_in:
    ind_sc  = file_in.rfind('-sc')
    file_in = file_in[0 : ind_sc] + file_in[ind_sc + 7 : ]
  file_out  = file_in.replace('gyre_in', 'gyre_out')
  file_out  = file_out.replace('.gyre', '.h5')
  ind_slash = file_out.rfind('/')
  file_out  = file_out[0:ind_slash+1] + 'ad-sum-' + file_out[ind_slash+1:]

  return file_out

#=========================================================================================
def freq_nonrot_to_freq_corot(el, em, nu, TradTable, freq_nonrot, freq_rot):
  """
  Using the tabulated coefficients for the Hough function (Townsend 2000, 2003, 2005), one can 
  just start from the unmodulated non-rotating frequencies, and correct for the effect of
  rotation within the Traditional Approximation (TAR). This function receives a list of 
  frequencies, and corrects them based on the input value of omega. 
  From Eq. (4) in B13:
     P_co(k) = sqrt(el(el+1))/sqrt(lambda) * P_nonrot
  """
  P_nonrot = 1.0 / freq_nonrot 
  lambdaa = get_lambda(el, em, nu, TradTable)

  return np.sqrt(el*(el+1.)) / np.sqrt(lambdaa) * P_nonrot

#=========================================================================================
def perturbative_omega_correction(freq, em, beta, omega, first_order=True):
  """
  First or higher-order perturbative rotation correction to the input frequencies, to take
  them from the initial rest frame to the observed non-inertial frame. if first_order=True,
      freq_pert = freq + em * beta * omega
  where freq, em, beta and omega are defined below. For higher order corrections see 
  Goupil & ... (201?)
  @param freq: a single or an array of frequencies in the inertial (e.g. non-rotating frame).
         they are normally computed by GYRE.
  @type freq: float or ndarray
  @param em: mode azimuthal degree; em can take the following discrete values: em=0 (zonal),
         em>0 (prograde) or em<0 (retrograde).
  @type em: integer
  @param beta: an integer or array of Ledoux constants for the modes; see Aerts et al. (2010,
         Sect. ...).
  @type beta: float or ndarray
  @param omega: rotation rate in Hz; it is asssumed that the rotation is rigid (solid body),
         therefore, we cannot account for differentially-rotating stars for now
  @type omega: float
  @param first_type: flag, specifying how many perturbative correction terms to include in 
         the final frequencies; if False, then, 3rd order correction of Goupil et al. (...) 
         will be considered. default=True
  @type first_order: boolean
  @return: perturbed frequency(ies) based on the input em, beta and omega
  @rtype: float or ndarray
  """
  if not isinstance(em, int):
    logging.error('tar: perturbative_omega_correction: em not integer')
    raise SystemExit, 'tar: perturbative_omega_correction: em not integer'
    
  if not first_order:
    logging.error('tar: perturbative_omega_correction: Higher-order corrections not supported')
    raise SystemExit, 'tar: perturbative_omega_correction: Higher-order corrections not supported'
    
  # if freq has to be modified due to the Hough function and/or the frequencies need to be
  # clipped off to match the observed range, it should all happen here  
    
  return freq + em * beta * omega

#=========================================================================================
def get_lambda(el, em, nu, TradTable):
    """
    This is a shortcut to TradTable.lambda(el, em, nu)
    The lambda is defined as 
        lambda = np.sqrt(l_e * (l_e + 1))
    @param el: mode degree in the non-rotating frame 
    @type el: integer
    @param em: mode azimuthal degree in the rotating frame 
    @type em: integer
    @param nu: spin parameter. You can get it, for any mode frequency freq_nonrot, 
          by calling get_spin(), knowing the star rotation frequency
    @type nu: float
    @param TradTable: Tabulated dataset containng data for TAR. It can be retrieved by calling 
          tar.get_TradTable(path_to_table)
    @type TradTable:
    @return: lambda
    @rtype: float
    """

    return TradTable.lamb(el, em, nu)

#=========================================================================================
def get_el_eff(el, em, nu, TradTable):
    """
    This is a shortcut to TradTable.l_e(el, em, nu).
    The eigenvalues of the Laplace tidal equation, lambda can be written as
         lambda = el_eff * (el_eff + 1.0)
    @param el: mode degree in the non-rotating frame 
    @type el: integer
    @param em: mode azimuthal degree in the rotating frame 
    @type em: integer
    @param nu: spin parameter. You can get it, for any mode frequency freq_nonrot, 
          by calling get_spin(), knowing the star rotation frequency
    @type nu: float
    @param TradTable: Tabulated dataset containng data for TAR. It can be retrieved by calling 
          tar.get_TradTable(path_to_table)
    @type TradTable:
    @return: effective degree
    @rtype: float
    """

    return TradTable.l_e(el, em, nu)

#=========================================================================================
def get_surface_v_rot(M_star, R_star, eta_rot, to_cgs=True):
  """
  Compute the surface rotation velocity [in km/sec] using the following relation 
     v_surf = 2 * pi * R_star * eta_rot * Omega_crit * cm_to_km
  Omega_crit is returned by a call to tar.get_omega_crit().
  @param M_star, R_star: mass and radius of the star with respect to the Sun, e.g. M_star=2.
  @type M_star, R_star: float
  param eta_rot: the rotation rate w.r.t. the critical rotation rate
  @type eta_rot: float
  @param to_cgs: flag to convert the input M_star and R_star to CGS units, if they are expressed
        w.r.t. the Solar mass and radius
  @type to_cgs: boolean
  @return: surface rotation velocity in km/sec 
  @type float 
  """
  cm_to_km   = 1e-5
  Omega_crit = get_omega_crit(M_star, R_star, to_cgs)
  Omega_rot  = eta_rot * Omega_crit
  if to_cgs:
    R_sun    = 6.9598e10
    R_star   = R_star * R_sun
  v_rot      = R_star * Omega_rot * cm_to_km

  return v_rot

#=========================================================================================
def get_freq_crit(M_star, R_star, to_cgs=True):
  """
  To handle the "implicit" 2pi difference between the omega_crit and freq_crit, this function
  just calls tar.get_omega_crit(), and divides the result by a factor 2pi.
  For the meaning of the arguments, refer to tar.get_omega_crit()
  """
  
  return get_omega_crit(M_star, R_star, to_cgs) / (2.0 * np.pi)

#=========================================================================================
def get_omega_crit(M_star, R_star, to_cgs=True):
  """
  Compute the critical angular rotation velocity at the surface based on the following formula:
     Omega_crit^2 = 8/27 * GM/R_p^3
  where we assume that the input radius, R_star is the polar radius R_p. This is the so-called
  Roche-model approximation. With this assumption, one neglects the centrifugal force, and 
  maintains the sphericity of the star.
  NOTE: There is a 2.0 pi factor implicit in this, that's why it is called "omega". 
  To get the critical rotation frequency call tar.get_freq_crit().
  @param M_star, R_star: mass and radius of the star with respect to the Sun, e.g. M_star=2.
  @type M_star, R_star: float
  @return: critical angular rotation frequency in Hertz
  @rtype: float
  """
  G     = 6.67428e-8
  M_sun = 1.9892e33
  R_sun = 6.9598e10

  # print M_star, M_star/M_sun
  # print R_star, R_star/R_sun
  # sys.exit()
  if to_cgs:
    M_gr  = M_star * M_sun
    R_cm  = R_star * R_sun
  else:
    M_gr  = M_star
    R_cm  = R_star
  
  return np.sqrt( (8.0 * G * M_gr) / (27.0 * R_cm * R_cm * R_cm) )
  
#=========================================================================================
def get_P_corot(el, el_eff, P_norot):
    """
    The period of a mode in the non-rotating frame is translated into that in the co-rotating frame, by using Eqs. (3)
    and (4) in B13. Assume
         L = sqrt(el * (el+1))
         and
         L_eff = sqrt(el_eff * (el_eff+1))
         then
         P_corot = L * P_norot / L_eff
    @param el: The harmonic polar degree of the mode 0<= el
    @type el: integer
    @el_eff: the effective harmonic degree in the TAR approximation; see T00
    @type el_eff: float
    @return: The mode period in the co-rotating frame P_corot
    @rtype: float
    """
    L = np.sqrt(el * (el+1.0))
    L_eff = np.sqrt(el_eff * (el_eff+1.0))
    if isinstance(P_norot, list):
      P_norot = np.array(P_norot)

    return L * P_norot / L_eff

#=========================================================================================
def get_freq_interial(em, freq_rot, freq_corot):
    """
    This routine calculates the mode's frequency in the inertial (observer) frame, 
    from that in the co-rotating frame freq_corot, knowing the rotation frequency freq_rot and the 
    azimuthal order of the mode em.
    The formula is 
           freq_inert = freq_corot + m * freq_rot
    Note that freq_corot and freq_rot must have the same units. This routine accepts a single valued freq_corot, 
    @param em: the azimuthal degree of the mode: |em| <= el
    @type em: integer
    @param freq_corot: the mode frequency in the co-rotating frame
    @type freq_corot: float or ndarray
    @param freq_rot: the star rotation period
    @type freq_rot: float 
    @return: the mode period in the inertial frame P_inert
    @rtype: float or ndarray
    """
    if isinstance(freq_corot, list):
      freq_corot = np.array(freq_corot)

    return freq_corot + em * freq_rot

#=========================================================================================
def get_P_inertial(em, P_corot, P_rot):
    """
    This routine calculates the period of a mode in the inertial (observer) frame, from that in the co-rotating frame P_corot,
    knowing the rotation period P_rot and the azimuthal order of the mode em.
    The formula is taken from Eq. 5 in B13:
           P_inert = P_corot / (1 - m * P_corot/P_rot)
    Note that P_corot and P_rot must have the same units.
    @param em: the azimuthal degree of the mode: |em| <= el
    @type em: integer
    @param P_corot: the mode frequency in the co-rotating frame
    @type P_corot: float
    @param P_rot: the star rotation period
    @type P_rot: float
    @return: the mode period in the inertial frame P_inert
    @rtype: float
    """
    if isinstance(P_corot, list):
      P_corot = np.array(P_corot)

    return P_corot / (1.0 - em * P_corot/P_rot)

#=========================================================================================
def get_spin(freq_rot, freq_corot):
    """
    Evaluate the spin parameter (B13, below Eq. 2)
          nu = 2 * omega_rot / omega_corot, 
    where omega_rot is the rotation frequency of the star
    and omega_corot is the mode frequency in co-rotating frame. This routine accepts a single frequency or a list 
    (or ndarray) of co-rotating frequencies.
    @param freq_rot: rotation frequency in Hz 
    @type freq_rot: float 
    @param freq_corot: mode frequency(ies) in co-rotating frame 
    @type freq_corot: float of ndarray
    @return: spin parameter for input mode(s)
    @rtype: float or ndarray
    """
    if isinstance(freq_corot, list):
      freq_corot = np.array(freq_corot)

    return 2 * freq_rot / freq_corot

#=========================================================================================
#=========================================================================================
def get_TradTable(path):
    """
    Specify the path to the tabulated eigenvalue HDF5 file, and load and return the table 
    @param path: full path to where the table file sits.
    @type path: string
    @return:
    @rtype:
    """
    if not os.path.exists(path):
        logging.error('tar: get_TradTable: {0} does not exist'.format(path))
        raise SystemExit, 'Error: tar: get_TradTable: {0} does not exist'.format(path)
    TradTable = tradtable.TradTable('trad_table.h5') # Change to the path of the HDF5 file
    print type(TradTable)

    return TradTable

#=========================================================================================

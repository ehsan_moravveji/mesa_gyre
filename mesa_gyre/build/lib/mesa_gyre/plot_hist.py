
import sys
import numpy as np
import read
import matplotlib
import matplotlib.pylab as plt

#=================================================================================================================
def compare_eta(list_dic_hist, list_lbls=None, ref_indx=0, from_pre_ms=True, from_zams=False,
                eta_surface=False, eta_center=False, eta_core_bndry=False, v_surf=False,
                by_L=False, by_t=False, by_n=False, by_Xc=False,
                x_from=0, x_to=100, y_from=0, y_to=1.1, file_out=None):
  """
  This function compares the evolution of the surface/center/core_bndry of eta (=Omega/Omega_crit).
  There are various choices for the x-axis, and also for the y-axis
  """
  import itertools

  n_hist = len(list_dic_hist)
  if n_hist == 0:
    message = 'Error: plot_hist: compare_eta: Input list is empty.'
    raise SystemExit, message
  if not any([eta_center, eta_core_bndry, eta_surface, v_surf]):
    print 'Error: plot_hist: compare_eta: Set ONE of these to True:'
    message = '    eta_center, eta_core_bndry or eta_surface, v_center, v_core_bdry, v_surf'
    raise SystemExit, message
  if not any([by_L, by_t, by_n, by_Xc]):
    message = 'Error: plot_hist: compare_eta: Set ONE of by_L, by_t, by_n, by_Xc to True.'
    raise SystemExit, message
  if all([from_pre_ms, from_zams]) or not any([from_pre_ms, from_zams]):
    raise SystemExit, 'Error: plot_hist, compare_eta: Set either from_pre_ms OR from_zams to True.'

  required_keys = ['surf_avg_omega', 'surf_avg_omega_crit', 'surf_avg_omega_div_omega_crit',
                   'surf_avg_v_rot', 'surf_avg_v_crit', 'surf_avg_v_div_v_crit',
                   'log_L', 'log_Lnuc', 'log_LH', 'log_LHe', 'star_age', 'model_number',
                   'center_h1']
  ref_hist = list_dic_hist[ref_indx]['hist']
  dic_avail_keys = {}
  hist_names = ref_hist.dtype.names
  for name in hist_names: dic_avail_keys[name] = 0.0
  dic_keys = read.check_key_exists(dic_avail_keys, required_keys)

  # Terminate if essential keys are missing
  if not dic_keys['surf_avg_omega_div_omega_crit'] or not all([dic_keys['surf_avg_omega'],
                                                               dic_keys['surf_avg_omega_crit']]):
    raise SystemExit, 'Error: plot_hist: compare_eta: code 1'
  if v_surf:
    if not dic_keys['surf_avg_v_rot']: raise SystemExit, 'Error: plot_hist: compare_eta: code 2'

  #---------------------------
  # Define the Plot
  #---------------------------
  fig = plt.figure(figsize=(6,4.5))
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.105, right=0.99, bottom=0.10, top=0.98, hspace=0.02, wspace=0.02)

  colormap = plt.get_cmap('brg')
  norm = matplotlib.colors.Normalize(vmin=0, vmax=n_hist-1)
  color = matplotlib.cm.ScalarMappable(norm=norm, cmap=colormap)

  ls = itertools.cycle(['dotted', 'dashed', 'dashdot'])

  #---------------------------
  # Iterate over all hist data
  #---------------------------
  for i_hist, dic_hist in enumerate(list_dic_hist):
    hist = dic_hist['hist']
    header  = dic_hist['header']

    log_g = hist['log_g']
    mxg = np.argmax(log_g)
    if from_pre_ms: start_indx = 0
    if from_zams: start_indx = mxg

    # fix x-axis and its range
    if by_L:
      if not dic_keys['log_L']: raise SystemExit, 'Error: plot_hist: compare_eta: Missing log_L'
      log_L = hist['log_L']
      if not dic_keys['log_Lnuc'] and (not dic_keys['log_LH'] and not dic_keys['log_LHe']):
        raise SystemExit, 'Error: plot_hist: compare_eta: Missing log_Lnuc and/or log_LH, log_LHe.'
      if dic_keys['log_Lnuc']:
        log_Lnuc = hist['log_Lnuc']
      elif (dic_keys['log_LH'] and dic_keys['log_LHe']):
        Lnuc = np.power(10.0, hist['log_LH']) + np.power(10.0, hist['log_LHe'])
        log_Lnuc = np.log10(Lnuc)
      log_Lnuc_div_L = log_Lnuc - log_L
      xaxis = log_Lnuc_div_L[start_indx : -1]
      xtitle = r'Nuclear to Total Luminosity $\log(L_{\rm nuc}/L_{\rm total})$'

    if by_t:
      if not dic_keys['star_age']: raise SystemExit, 'Error: plot_hist: compare_eta: Missing star_age.'
      star_age = hist['star_age']
      xaxis = star_age[start_indx : -1] / 1e6
      xtitle = r'Star Age [Myr]'

    if by_n:
      if not dic_keys['model_number']: raise SystemExit, 'Error: plot_hist: compare_eta: Missing model_number.'
      model_number = hist['model_number']
      xaxis = model_number[start_indx : -1]
      xtitle = r'Model Number'

    if by_Xc:
      if not dic_keys['center_h1']: raise SystemExit, 'Error: plot_hist: compare_eta: Missing center_h1'
      center_h1 = hist['center_h1']
      xaxis = center_h1[start_indx : -1]
      xtitle = r'Center Hydrogen Mass Fraction [$X_c$]'

    if eta_surface:
      if not dic_keys['surf_avg_omega_div_omega_crit'] or (not dic_keys['surf_avg_omega'] and not dic_keys['surf_avg_omega_crit']):
        raise SystemExit, 'Error: plot_hist: compare_eta: Missing surf_avg_... omega.'
      if dic_keys['surf_avg_omega'] and dic_keys['surf_avg_omega_crit']:
        surf_avg_omega_div_omega_crit = hist['surf_avg_omega']/hist['surf_avg_omega_crit']
      if dic_keys['surf_avg_omega_div_omega_crit']:
        surf_avg_omega_div_omega_crit = hist['surf_avg_omega_div_omega_crit']
      yaxis = surf_avg_omega_div_omega_crit[start_indx : -1]
      ytitle = r'Surface $\eta_{\rm rot}=\Omega_{\rm eq}/\Omega_{\rm crit}$'

    if eta_center:
      if not dic_keys['center_omega_div_omega_crit']:
        raise SystemExit, 'Error: plot_hist: compare_eta: center_omega_div_omega_crit missing'
      yaxis = hist['center_omega_div_omega_crit'][start_indx : -1]
      ytitle = r'Center $\eta_{\rm rot}=\Omega_{\rm eq}/\Omega_{\rm crit}$'

    if eta_core_bndry:
      if not dic_keys['cz_omega_div_omega_crit']:
        raise SystemExit, 'Error: plot_hist: compare_eta: missing cz_omega_div_omega_crit'
      else:
        yaxis = hist['cz_omega_div_omega_crit'][start_indx : -1]
      ytitle = r'$\eta_{\rm rot}$ at Conv. Core Boundary'

    if v_surf:
      if not dic_keys['surf_avg_v_rot']: raise SystemExit, 'Error: plot_hist: compare_eta: surf_avg_v_crit missing.'
      yaxis = hist['surf_avg_v_rot'][start_indx : -1]
      ytitle = r'Surface Rotation Velocity $v_{\rm rot}$ [km sec$^{-1}$]'


    #---------------------------
    # Iteratively, Overplot
    #---------------------------
    if i_hist == ref_indx:
      clr = 'black'
      lstyl = 'solid'
      lbl = r'Ref: ' + list_lbls[i_hist]
    else:
      clr = color.to_rgba(i_hist)
      lstyl = ls.next()
      lbl = list_lbls[i_hist]
    ax.plot(xaxis, yaxis, linestyle=lstyl, color=clr, lw=2, label=lbl)

    # Add the ZAMS point
    if from_pre_ms: ax.scatter([xaxis[mxg]], [yaxis[mxg]], marker='o', color='red', s=25)

  ax.set_xlabel(xtitle, fontsize='medium')
  ax.set_xlim(x_from, x_to)
  ax.set_ylabel(ytitle, fontsize='medium')
  ax.set_ylim(y_from, y_to)

  #---------------------------
  # Determine the location of Legend
  #---------------------------
  leg_loc = 0    # default
  if by_L and eta_surface: leg_loc = 2
  if by_Xc and eta_surface: leg_loc = 2
  if by_Xc and v_surf: leg_loc = 1
  if list_lbls:
    leg = ax.legend(loc=leg_loc, shadow=True, fancybox=True, fontsize='small')


  if not file_out: file_out = 'Compare-evol-eta.pdf'
  plt.savefig(file_out, dpi=200)
  print '\n - compare_eta: %s saved. \n' % (file_out, )
  plt.close()

  return None

#=================================================================================================================
def diffusion_and_radiative_levitation(dic_hist, rec_abund, list_elem=[], xaxis='center_h1',
                     xaxis_from=None, xaxis_to=None, xtitle=None, file_out=None):
  """
  This function plots the evolution of the surface abundances of selected species, and compares them with
  the observed values in spectroscopic notation
  @param dic_hist: dictionary with the MESA history output data.
  @type dic_hist: dictionary
  @param rec_abund: array with the full mixture information.
  """
  try:
    import abund_lib
  except:
    print 'Error: mesa_gyre: plot_hist: diffusion_and_radiative_levitation: could not import abund_lib!'

  header = dic_hist['header']
  hist = dic_hist['hist']
  hist_names = hist.dtype.names

  if xaxis not in hist_names:
    print 'Error: plot_hist: diffusion_and_radiative_levitation: {0} not in available hist keys'.format(xaxis)
    raise SystemExit
  xvals = hist[xaxis]

  list_avail_surf_abund = []
  for name in hist_names:
    if 'surface_' in name: list_avail_surf_abund.append(name)

  n_avail_abund = len(list_avail_surf_abund)
  n_list_elem = len(list_elem)
  list_elem_plot = []
  if n_list_elem > 0:
    for i_elem, elem_name in enumerate(list_elem):
      if 'surface_' + elem_name in list_avail_surf_abund:
        list_elem_plot.append('surface_' + elem_name)
  n_plots = len(list_elem_plot)

  # Reference Observed Abundances
  obs_elems = rec_abund['elem']
  obs_elem_A = rec_abund['A']
  obs_elem_Z = rec_abund['Z']
  n_obs_elem = len(obs_elems)
  obs_elem_names = ['surface_' + obs_elems[i] + str(obs_elem_A[i]) for i in range(n_obs_elem)]

  #---------------------------
  # Create the Plot
  #---------------------------
  fig, axarr = plt.subplots(n_plots, 1, figsize=(6, n_plots*1.5))
  fig.subplots_adjust(left=0.12, right=0.95, bottom=0.06, top=0.97, hspace=0.12, wspace=.10)

  #---------------------------
  # Loop over elements
  #---------------------------
  for i_p, ax in enumerate(axarr.ravel()):
    i_elem = i_p
    elem_name = list_elem_plot[i_p]
    # if elem_name == 'surface_h1' or elem_name == 'surface_h2': continue
    # if elem_name == 'surface_he3' or elem_name == 'surface_he4': continue
    if 'surface_h1' in hist_names:
      X_h1 = hist['surface_h1']
    else:
      X_h1 = 0.0
    if 'surface_h2' in hist_names:
      X_h2 = hist['surface_h2']
    else:
      X_h2 = 0.0
    X_h = X_h1 + X_h2
    if 'surface_he3' in hist_names:
      X_he3 = hist['surface_he3']
    else:
      X_he3 = 0.0
    if 'surface_he4' in hist_names:
      X_he4 = hist['surface_he4']
    else:
      X_he4 = 0.0
    X_he = X_he3 + X_he4

    if elem_name == 'surface_h2': continue
    if elem_name == 'surface_he3': continue
    if elem_name not in obs_elem_names:
      print 'Warning: plot_hist: diffusion_and_radiative_levitation: {0} not in observed input record array!'.format(elem_name)
      add_obs = False
      continue
    else:
      ind = [i for i in range(n_obs_elem) if elem_name == obs_elem_names[i]][0]
      elem_recarr = rec_abund[ind]
      elem_A = elem_recarr['A']
      add_obs = True

    if elem_name == 'surface_h1':
      X_elem = X_h
    elif elem_name == 'surface_he4':
      X_elem = X_he
    else:
      X_elem = hist[elem_name]
    eps_elem = np.log10(X_elem / (X_h * elem_A)) + 12.0

    ax.plot(xvals, eps_elem, color='black', lw=2, zorder=2)
    len_elem = len(elem_name)
    str_part = ''.join( [''+elem_name[i] for i in range(len_elem) if not elem_name[i].isdigit()] )
    str_part = str_part[ str_part.rfind('_')+1 : ].title()
    int_part = ''.join( [''+str(elem_name[i]) for i in range(len_elem) if elem_name[i].isdigit()] )
    ytitle = r'$\epsilon(^{0}{1}{2}${3})'.format('{', int_part, '}', str_part)
    ax.set_ylabel(ytitle)
    ax.ticklabel_format(style='plain', useOffset=False)

    if add_obs:
      obs_abund = elem_recarr['abund']
      obs_abund_err = elem_recarr['abund_err']
      ax.fill_between(xvals, y1=obs_abund-obs_abund_err, y2=obs_abund+obs_abund_err,
        color='grey', alpha=0.5, zorder=1)

    if i_p == n_plots - 1:
      if xtitle is None: xtitle = xaxis.replace('_', ' ')
      ax.set_xlabel(xtitle)
    else:
      ax.set_xticklabels(())

    if xaxis_from is not None and xaxis_to is not None:
      ax.set_xlim(xaxis_from, xaxis_to)

  #---------------------------
  # Save the plot
  #---------------------------
  if file_out:
    # plt.tight_layout()
    plt.savefig(file_out)
    print ' - plot_hist: diffusion_and_radiative_levitation: saved {0}'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================




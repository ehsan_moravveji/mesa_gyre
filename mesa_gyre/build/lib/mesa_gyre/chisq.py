
"""
This module contains different Chi-square Goodness-of-Fit functions to find best models given
input evolutionary and/or seismic information.
"""

import sys, glob, os
import logging
import time
import numpy as np
from multiprocessing import Pool, cpu_count
import param_tools, read, write, period_spacing, stars, plot_chisq


#=================================================================================================================
#=================================================================================================================
def fit_two_freq(list_dic, list_tup_obs):
  """
  This function receives a list of dictionaries, where each dictionary corresponds to a point on the Kiel diagram
  for a star within an observed uncertainties. It also receives a list of tuples; there is a tuple in this list per
  each observed mode. Each tuple contains an observed mode frequency and (1-sigma or more) error, and degree.
  ATTENTION: Make sure the frequency unit of the observed and the grid calculation perfectly match before calling
  this function, or you are comparing apples and bananas!

  @param list_dic: Input list of dictionaries. Each point has both evolutionary and seismic information in it.
  @type list_dic: list of dictionaries.
  @param list_tup_obs: list of two tuples, with the following items in each:
   - degree
   - frequency
   - frequency error: This is not essentially 1-sigma. This is decided in the higher-level script that calls fit_one_freq.
  @return: It returns a list of dictionaries for which the deisred observed frequency matches at least one frequency
     in the grid within the prescribed uncertainty range.
  @rtype: list of dictionaries
  """

  n_dic = len(list_dic)
  if n_dic == 0:
    message = 'Error: sample_grid: fit_two_freq: The input list is empty.'
    raise SystemExit, message

  if type(list_tup_obs) is not type([]):
    message = 'Error: sample_grid: fit_two_freq: 2nd input must be a list!'
    raise SystemExit, message

  n_tup = len(list_tup_obs)
  if n_tup != 2:
    message = 'Error: sample_grid: fit_two_freq: The input list must contain ONLY two tuples! %s' % (n_tup, )
    raise SystemExit, message

  tup_obs_1 = list_tup_obs[0]
  tup_obs_2 = list_tup_obs[1]

  list_dic_match_obs_1 = fit_one_freq(list_dic, tup_obs_1)
  n_match_obs_1 = len(list_dic_match_obs_1)
  if n_match_obs_1 == 0:
    print 'Warning: fit_two_freq: We could not match the first observed frequency!'
    return []
  if n_match_obs_1 > 0:
    print ' - fit_two_freq: Found %s points for the first frequency:' % (n_match_obs_1, )
    print '   el: %s, f_obs: %s +/- %s' % (tup_obs_1[0], tup_obs_1[1], tup_obs_1[2])

  list_dic_match_obs_2 = fit_one_freq(list_dic_match_obs_1, tup_obs_2)
  n_match_obs_2 = len(list_dic_match_obs_2)
  if n_match_obs_2 == 0:
    print 'Warning: fit_two_freq: We could not match the second observed frequency!'
    return []
  if n_match_obs_2 > 0:
    print ' - fit_two_freq: Found %s points for the second frequency: '
    print '   el: %s, f_obs: %s +/- %s' % (n_match_obs_2, tup_obs_2[0], tup_obs_2[1], tup_obs_2[2])
    return list_dic_match_obs_2


#=================================================================================================================
def fit_one_freq(list_dic, dic_freq, sigma=3):
  """
  This function receives a list of dictionaries, where each dictionary corresponds to a point on the Kiel diagram
  for a star within an observed uncertainties. It also receives a tuple with the observed mode frequency and (1-sigma
  or more) error, and degree.
  ATTENTION: Make sure the frequency unit of the observed and the grid calculation perfectly match before calling
  this function, or you are comparing apples and bananas!
  WARNING: the "sigma" is incorporated into the output chis-square value (in the denuminator)
  @param list_dic: Input list of dictionaries. Each point has both evolutionary and seismic information in it.
  @type list_dic: list of dictionaries.
  @param dic_freq: dictionary, with the following items in it:
   - 'freq': frequency
   - 'freq_err': 1-sigma uncertainty of the frequency
   - 'l': degree el
  @param sigma: the level of confidence around each each frequency for accepting/rejecting frequencies. default=3
  @type sigma: float
  @return: It returns a list of dictionaries for which the deisred observed frequency matches at least one frequency
     in the grid within the prescribed uncertainty range. One extra key-value item will be attached to each dictionary
     which gives the chisquare for matching that frequency with all those falling inside the uncertainty range.
     The new key is called "chisq_one_fit".
  @rtype: list of dictionaries
  """
  n_dic = len(list_dic)
  if n_dic == 0:
    message = 'Error: sample_grid: fit_one_freq: The input list is empty.'
    raise SystemExit, message

  if 'freq' not in dic_freq.keys() or 'freq_err' not in dic_freq.keys() or 'l' not in dic_freq.keys():
    message = 'fit_one_freq: inadequate information in dic_freq'
    logging.error(message)
    raise SystemExit, message

  f_obs     = dic_freq['freq']
  f_obs_err = dic_freq['freq_err']
  el_obs    = dic_freq['l']
  str_el    = 'el_' + str(el_obs) + '_freq'
  min_freq  = f_obs - sigma * f_obs_err
  max_freq  = f_obs + sigma * f_obs_err

  n_match = 0
  list_dic_match = []

  for i_dic, dic in enumerate(list_dic):
    freq  = np.real(dic['freq'])
    el    = dic['l']
    n_modes = len(el)

    ind   = [i for i in range(n_modes) if (el[i]==el_obs) & (freq[i] >= min_freq) & (freq[i] <= max_freq)]
    if len(ind) == 0: continue

    freq  = freq[ind]
    el    = el[ind]
    n_pg  = dic['n_pg'][ind]

    ind_close = np.argmin(np.abs(freq - f_obs))
    f_th  = freq[ind_close]
    chisq_one_fit = (f_obs - f_th)**2.00 / (sigma * f_obs_err)**2.0
    dic['chisq_one_fit'] = chisq_one_fit

    n_match += 1
    list_dic_match.append(dic)

  print ' - fit_one_freq: Matched %s frequencies for ell = %s and f_obs = %s +/- %s' % (n_match, el_obs, f_obs, sigma*f_obs_err)

  return list_dic_match

#=================================================================================================================
def multiproc_freq_P_dP(list_h5_files, dic_obs, chisq_by='reduced_chisq_seism_freq', el=1, freedom=4, sigma=10, 
			freq_unit='Hz', n_cpu=None, path_plots=None):
  """
  The parallel version of do_chisq_freq_P_dP(). Refer to that function to comprehend the meaning of arguments.
  This function makes use of multiprocessing module
  """
  n_files = len(list_h5_files)
  
  t_start = time.time()
  if path_plots is not None:
    if path_plots[-1] != '/': path_plots += '/'
    if not os.path.exists(path_plots): os.makedirs(path_plots)

  ###########################
  # Prepare the Pool
  ###########################
  if isinstance(n_cpu, int):
    pool = Pool(processes=n_cpu)
  elif n_cpu is None:
    n_cpu = cpu_count()
    print ' - chisq: multiproc_freq_P_dP: Automatically set n_cpu={0}'.format(n_cpu)
    pool = Pool(processes=n_cpu)
  else:
    logging.error('chisq: multiproc_freq_P_dP: n_cpu is assigned a wrong value')
    raise SystemExit, 'Error: chisq: multiproc_freq_P_dP: n_cpu is assigned a wrong value'
  
  ###########################
  # Serial Section
  ###########################
  logging.info('multiproc_freq_P_dP: Update dic_obs with period spacing of each mode')
  list_dic_freq = dic_obs['list_dic_freq']
  list_dic_freq = stars.check_obs_vs_model_freq_unit(freq_unit, list_dic_freq) # fix the unit
  dic_obs['list_dic_freq'] = list_dic_freq
  # update the list of frequencies to include the periods, period spacings and their associated errors
  dic_obs       = stars.list_freq_to_list_dP(dic_obs, freq_unit=freq_unit)

  ###########################
  # Parallel Section
  ###########################
  logging.info('multiproc_freq_P_dP: Parallel Read HDF5 files, Calculate peiod spacing.')

  list_dic_modes = read.multiproc_gyre_output(list_h5_files, n_cpu)
  # list_dic_modes = read.read_multiple_gyre_files(list_h5_files)
  list_h5_files  = None  # to free up some memory
  
  list_dic_dP = period_spacing.multiproc_gen_list_dic_dP(list_dic_modes, n_cpu, freq_unit=freq_unit)
  list_dic_modes = None  # to free up some memory
  
  list_dic_dP = period_spacing.multiproc_update_dP_dip(list_dic_dP, n_cpu, el=1, by_P=True, P_from=0.98, P_to=2.12)

  logging.info('parallel_freq_P_dP: Perform Chisquare based on Periods and Period Spacings')
  list_dic_modes_with_chisq = multiproc_seismic_dP(list_dic_dP, dic_obs, freedom=freedom, el=el, 
						   sigma=sigma, freq_unit=freq_unit, consecutive=True,
						   n_cpu=n_cpu)
  list_dic_dP = None  # to free up some memory

  ###########################
  # Serial Section
  ###########################
  logging.info('parallel_freq_P_dP: Perform Chisquare based on Frequencies')
  #list_dic_modes_with_chisq = seismic_freq(list_dic_dP, dic_obs, freedom=freedom, el=el,
                                           #freq_unit=freq_unit, consecutive=True, sigma=sigma)

  logging.info('parallel_freq_P_dP: Collect reduced Chisquare for frequencies')
  list_chisq = np.array([dic[chisq_by] for dic in list_dic_modes_with_chisq])

  logging.info('parallel_freq_P_dP: Sort Chisq lists in increasing order')
  ind_sorted      = np.argsort(list_chisq)
  list_chisq      = list_chisq[ind_sorted]
  #list_dic_modes = [list_dic_modes[i] for i in ind_sorted]
  #list_dic_dP    = [list_dic_dP[i] for i in ind_sorted]
  list_sorted     = [list_dic_modes_with_chisq[i] for i in ind_sorted]

  write.list_chisq_to_ascii(list_sorted, chisq_by=chisq_by, max_row=None)
  write.show_best_models(list_sorted[0], chisq_by=chisq_by)
  
  dt_sec = int(time.time() - t_start)
  
  print ' - chisq: multiproc_freq_P_dP:'
  print '   N_cpu={0}, N_files={1}, Time={2} [sec] \n'.format(n_cpu, n_files, dt_sec)
  
  return None

#=================================================================================================================
def parallel_freq_P_dP(list_h5_files, dic_obs, el=1, freedom=4, sigma=10, freq_unit='Hz', n_cpu=2, path_plots=None):
  """
  The parallel version of do_chisq_freq_P_dP(). Refer to that function to comprehend the meaning of arguments.
  """
  if path_plots is not None:
    if path_plots[-1] != '/': path_plots += '/'
    if not os.path.exists(path_plots): os.makedirs(path_plots)

  ###########################
  # Serial Section
  ###########################
  logging.info('parallel_freq_P_dP: Update dic_obs with period spacing of each mode')
  list_dic_freq = dic_obs['list_dic_freq']
  list_dic_freq = stars.check_obs_vs_model_freq_unit(freq_unit, list_dic_freq) # fix the unit
  dic_obs['list_dic_freq'] = list_dic_freq
  # update the list of frequencies to include the periods, period spacings and their associated errors
  dic_obs       = stars.list_freq_to_list_dP(dic_obs, freq_unit=freq_unit)

  ###########################
  # Parallel Section
  ###########################
  logging.info('parallel_freq_P_dP: Parallel Read HDF5 files, Calculate peiod spacing.')

  list_dic_modes = read.parallel_gyre_output(list_h5_files, n_cpu=n_cpu)

  list_dic_dP    = period_spacing.parallel_gen_list_dic_dP(list_dic_modes, n_cpu=n_cpu)

  list_dic_dP    = period_spacing.parallel_update_dP_dip(list_dic_dP, el=1, by_P=True, P_from=0.98, P_to=2.12, n_cpu=n_cpu)

  ###########################
  # Serial Section
  ###########################
  logging.info('parallel_freq_P_dP: Perform Chisquare based on Frequencies')
  #list_dic_modes_with_chisq = seismic_freq(list_dic_dP, dic_obs, freedom=freedom, el=el,
                                           #freq_unit=freq_unit, consecutive=True, sigma=sigma)

  logging.info('parallel_freq_P_dP: Perform Chisquare based on Periods and Period Spacings')
  list_dic_modes_with_chisq    = seismic_dP(list_dic_dP, dic_obs, freedom=freedom, el=el, sigma=sigma,
                                         consecutive=True)

  logging.info('parallel_freq_P_dP: Collect reduced Chisquare for frequencies')
  list_chisq_freq = np.array([dic['reduced_chisq_seism_f_and_dP']     for dic in list_dic_modes_with_chisq])

  logging.info('parallel_freq_P_dP: Sort Chisq lists in increasing order')
  ind_sorted_by_freq = np.argsort(list_chisq_freq)
  list_chisq_freq    = list_chisq_freq[ind_sorted_by_freq]
  list_dic_modes     = [list_dic_modes[i] for i in ind_sorted_by_freq]
  list_dic_dP        = [list_dic_dP[i] for i in ind_sorted_by_freq]
  list_sorted_by_freq= [list_dic_modes_with_chisq[i] for i in ind_sorted_by_freq]

  write.list_chisq_to_ascii(list_sorted_by_freq, chisq_by='reduced_chisq_seism_f_and_dP', max_row=None)
  
  return None

#=================================================================================================================
def do_chisq_freq_P_dP(list_h5_files, dic_obs, el=1, freedom=4, sigma=10, freq_unit='Hz', path_plots=None):
  """
  To carry out Chisquare analysis based on Frequency-Matching, Peirod-Matching and Period-Spacing-Matching for a star.
  This is like a very top-level script, and is a wrapper for multiple tools.
  @param list_h5_files: list of output GYRE files. They are normally stored in /gyre_out/ subdirectory
  @type list_h5_files: list of strings
  @param dic_obs: all observational properties of the stars are passed through this dictionary. Normally, it is retrieved
      from mesa_gyre.stars module.
  @type dic_obs: dictionary
  @param el: the desired harmonic degree
  @type el: integer
  @param freedom: The number of degrees of freedom, DOF, in the grid used to explain the frequencies. typically it is 4 or 5.
      this is normally passed to chisq.seismic_freq() or chisq.seismic_dP() routines.
  @type freedom: integer
  @param sigma: the frequency scanning range will be performed before the shortest frequency and after the largest frequency
      by sigma times the frequency uncertainty of that specific mode. This is passed to chisq.seismic_freq()
  @type sigma: integer
  @param freq_unit: The frequency unit of the GYRE output files. default='Hz'. The observed frequencies will be forced to
      adapt to this unit by internal conversion
  @type freq_unit: string
  @param path_plots: directory name where the plots will be saved. default=None, which means save here
  @return: None. But, some on-screen messages and several plots will be generated
  @rtype: None
  """
  if path_plots is not None:
    if path_plots[-1] != '/': path_plots += '/'
    if not os.path.exists(path_plots): os.makedirs(path_plots)

  logging.info('do_chisq_freq_P_dP: Update dic_obs with period spacing of each mode')
  list_dic_freq = dic_obs['list_dic_freq']
  list_dic_freq = stars.check_obs_vs_model_freq_unit(freq_unit, list_dic_freq) # fix the unit
  dic_obs['list_dic_freq'] = list_dic_freq
  # update the list of frequencies to include the periods, period spacings and their associated errors
  dic_obs       = stars.list_freq_to_list_dP(dic_obs, freq_unit=freq_unit)

  logging.info('do_chisq_freq_P_dP: Read HDF5 files, Calculate peiod spacing')
  list_dic_modes = read.read_multiple_gyre_files(list_h5_files)
  list_dic_dP    = period_spacing.gen_list_dic_dP(list_dic_modes)
  # list_dic_dP    = period_spacing.update_dP_dip(list_dic_dP, el=1, by_P=True, P_from=0.98, P_to=2.12)

  logging.info('do_chisq_freq_P_dP: Perform Chisquare based on Frequencies')
  list_dic_modes_with_chisq = seismic_freq(list_dic_dP, dic_obs, freedom=freedom, el=el,
                                           freq_unit=freq_unit, consecutive=True, sigma=sigma)
  list_chisq_freq = np.array([dic['reduced_chisq_seism_freq']     for dic in list_dic_modes_with_chisq])

  #logging.info('do_chisq_freq_P_dP: Perform Chisquare based on Periods and Period Spacings')
  #list_dic_modes_with_chisq    = seismic_dP(list_dic_dP, dic_obs, freedom=freedom, el=el,
                                         #consecutive=True, sigma=sigma)
  #list_chisq_freq = np.array([dic['reduced_chisq_seism_f_and_dP'] for dic in list_dic_modes_with_chisq])

  logging.info('do_chisq_freq_P_dP: Sort Chisq lists in increasing order')
  ind_sorted_by_freq = np.argsort(list_chisq_freq)
  list_chisq_freq    = list_chisq_freq[ind_sorted_by_freq]
  list_dic_modes     = [list_dic_modes[i] for i in ind_sorted_by_freq]
  list_dic_dP        = [list_dic_dP[i] for i in ind_sorted_by_freq]
  list_sorted_by_freq= [list_dic_modes_with_chisq[i] for i in ind_sorted_by_freq]
  print list_sorted_by_freq[0]['filename']
  print list_chisq_freq[0]

  # write.list_chisq_to_ascii(list_sorted_by_freq, chisq_by='reduced_chisq_seism_freq', max_row=None)

  if path_plots:
    logging.info('do_chisq_freq_P_dP: Optionally, do the plottings')
    list_lbls = [r'1st', r'2nd', r'3rd', r'4th', r'5$^{\rm th}$']
    # plot_chisq.dP_match(dic_obs, list_sorted_by_freq[0:5], list_lbls, el=1, best_chisq=None,
    #                     file_out=path_plots + 'Chisq-' + dic_obs['name'][0] + '-P-dP.png')

    plot_chisq.mean_dP_vs_x(list_dic_modes_with_chisq, xaxis='Xc',
                          by='reduced_chisq_seism_freq', chisq_from=0, chisq_to=100, xaxis_from=0.55,
                          xaxis_to=-0.02, xlabel=r'Central Hydrogen $X_c$', dP_from=5000, dP_to=6000,
                          file_out=path_plots+'Chisq-Mean-dP-vs-Xc.png')

    # plot_chisq.test(list_sorted_by_freq, list_params=['sc', 'Z'], by='reduced_chisq_seism_freq',
    #                 file_out=path_plots+'Chisq-sc-Z.png')


    #plot_chisq.param_summary(list_sorted_by_freq, list_params=['Z', 'Xc'], by='reduced_chisq_seism_freq',
                             #factor=2, contour=True, file_out=path_plots + 'Chisq-'+dic_obs['name'][0]+'-Contour-Summary.png')
    #plot_chisq.param_summary(list_sorted_by_freq, list_params=['sc', 'Xc', 'Z'], by='reduced_chisq_seism_freq',
                             #factor=5, contour=False, file_out=path_plots + 'Chisq-'+dic_obs['name'][0]+'-Scatter-Summary.png')

    # 3-D plots
    #plot_chisq.contour_with_projection(list_sorted_by_freq, list_params=['Xc', 'Z'],
                                       #by='reduced_chisq_seism_freq', file_out=path_plots+'Chisq-'+dic_obs['name'][0]+
                                       #'-3D-Contour.png')
    #plot_chisq.scattered_points(list_sorted_by_freq, list_params=['sc', 'Z', 'Xc'],
                                #by='reduced_chisq_seism_freq', file_out=path_plots+'Chisq-'+dic_obs['name'][0]+
                                #'-3D-Scatter.png')

  logging.info('do_chisq_freq_P_dP: Done')
  return None

#=================================================================================================================
def combine_chisq_p_g(list_final, list_other, chisq_by, average=False):
  """
  Combine two sets of chisquare computations, where each list has essentially different number of models in it 
  (due to strict model selection, which can behave differently for p- and g-modes). The list_final is generally 
  smaller than the list_other. After sorting list_final based on "chisq_by", we find mutula filenames between
  the two lists, and average their indentical chi-square score, and save it again in the same key "chisq_by"
  in the list_final dictionaries. The "chisq_by" values returned are the mean values plugged back into list_final.
  @param list_final, list_other: This is the final list of dictionaries, and commonly the smaller one, since has already gone 
         through more strict criteria. This list may contain results of chisquare computations for only p-modes. 
         The list_other is similar to the former, and perhaps larger, and contains identical fields as of list_final.
         However, the two lists have totally different sorting. So, we sort list_final again.
  @param average: specify to take an average of the two chi-square values, or just simply add them linearly,
         without any averaging. default=False.
  @type average: boolean
  @return: updated list of list_final dictionaries with average chisquare values of p- and g-modes.
  @rtype: list of dictionaries
  """
  n_final = len(list_final)
  if n_final == 0:
    logging.error('chisq: combine_chisq_p_g: list_final is empty!')
    raise SystemExit, 'Error: chisq: combine_chisq_p_g: list_final is empty!'
  n_other = len(list_other)
  if n_other == 0:
    logging.error('chisq: combine_chisq_p_g: list_other is empty!')
    raise SystemExit, 'Error: chisq: combine_chisq_p_g: list_other is empty!'

  chisq_final   = np.array([ dic[chisq_by] for dic in list_final ])
  ind_final     = np.argsort(chisq_final)
  chisq_final   = chisq_final[ind_final]
  list_final    = [list_final[i] for i in ind_final]
  list_fn_final = [dic['filename'] for dic in list_final]
  list_fn_other = [dic['filename'] for dic in list_final]

  list_output   = []
  for i_final, dic_final in enumerate(list_final):
    dic_final   = dic_final.copy()
    fn_final    = dic_final['filename']
    try:
      i_other   = list_fn_other.index(fn_final)
    except ValueError:
      print 'Error: chisq: combine_chisq_p_g: Think about a solutin here'
      raise ValueError
    dic_other   = list_other[i_other] #.copy()

    chisq_final = dic_final[chisq_by]
    chisq_other = dic_other[chisq_by]

    if average:
      chisq_mean  = ( chisq_final + chisq_other ) / 2.0
    else: 
      chisq_mean  = chisq_final + chisq_other
    dic_final[chisq_by] = chisq_mean

    list_output.append(dic_final)

  return list_output

#=================================================================================================================
def combine_chisq_three_lists(list_chisq_1, list_chisq_2, list_chisq_3, chisq_by_1, chisq_by_2, chisq_by_3,
                              normalize=True):
  """
  To combine three chi-square lists, list_chisq_1, list_chisq_2 and list_chisq_3, based on their individual list of chisquare 
  values passed by chisq_by_1, chisq_by_2 and chisq_by_3. This is found useful for treating three different 
  frequecy domains of the CoRoT B-type hybrid pulsator HD 50230.
  Note: The final sorting and writing of the chi-square values is based on an average of the three chi-square values 
        passed by chisq_by_1, chisq_by_2 and chisq_by_3. This option is set by the "normalize" argument.
  WARNING: This routine is currently super-slow, unless the index searching in the for loop is improved.
           The best way to improve this is by "pop"ing out the relevant row from chisq_vals_extra_1 and 
           chisq_vals_extra_2 using ind_in_extra_1 and ind_in_extra_2 once they are found. This will speed up the 
           index search as it goes on.
  @param list_chisq_1, list_chisq_2, list_chisq_3: the three lists of models with chi-square values that we use.
         Note: There is no priority difference between these lists. We finally sort all models based on their average chisquare.
  @type list_chisq_1, list_chisq_2, list_chisq_3: list of dictionaries
  @param chisq_by_1, chisq_by_2, chisq_by_3: which chi-square field to use for each of the three input lists. They should
         already exist in each of the lists/dictionaries.
  @type chisq_by_1, chisq_by_2, chisq_by_3: string 
  @param normalize: If set True, then the "frequency" chi-square list will be divided by the minimum chi-square value  
       from the list, and then, the chis-suqre for the frequency list will start from 1.00. This is later passed to 
       mesa_gyre.write.list_chisq_to_ascii(). This is achieved by calling chisq.normalize_list_dic_chisq(). default=True
  @type normalize: boolean
  @return: sorted list of models based on their average chisquare values. The new field is called 'chisq_mean'. If the 
        normalize is True, then the first item should be assigned to chisq_mean value of 1.00
  @rtype: list of dictionaries with a new key, called 'chisq_mean'
  """
  if chisq_by_1 not in list_chisq_1[0].keys():
    logging.error('chisq: combine_chisq_three_lists: {0} not in the first list: list_chisq_1'.format(chisq_by_1))
    raise SystemExit, 'Error: chisq: combine_chisq_three_lists: {0} not in the first list: list_chisq_1'.format(chisq_by_1)
  if chisq_by_2 not in list_chisq_2[0].keys():
    logging.error('chisq: combine_chisq_three_lists: {0} not in the second list: list_chisq_2'.format(chisq_by_2))
    raise SystemExit, 'Error: chisq: combine_chisq_three_lists: {0} not in the second list: list_chisq_2'.format(chisq_by_2)
  if chisq_by_3 not in list_chisq_3[0].keys():
    logging.error('chisq: combine_chisq_three_lists: {0} not in the third list: list_chisq_3'.format(chisq_by_3))
    raise SystemExit, 'Error: chisq: combine_chisq_three_lists: {0} not in the third list: list_chisq_3'.format(chisq_by_3)

  if normalize:
    list_chisq_1       = normalize_list_dic_chisq(list_chisq_1, chisq_by_1)[:]
    list_chisq_2       = normalize_list_dic_chisq(list_chisq_2, chisq_by_2)[:]
    list_chisq_3       = normalize_list_dic_chisq(list_chisq_3, chisq_by_3)[:]

  # It can happen that the three lists have different number of models. We only use those models that are common among both
  # lists, so, we choose the smallest list as the basis for the comparison, and then proceed
  n_1 = len(list_chisq_1)
  n_2 = len(list_chisq_2)
  n_3 = len(list_chisq_3)
  list_n  = np.array([n_1, n_2, n_3])
  ind_min = np.argmin(list_n)
  n_min   = np.min(list_n)
  list_of_lists   = [list_chisq_1[:], list_chisq_2[:], list_chisq_3[:]]
  list_base       = list_of_lists[ind_min]
  throw_away      = list_of_lists.pop(ind_min)

  # Retrieve chi-square values from each list
  list_chisq_keys    = [chisq_by_1, chisq_by_2, chisq_by_3]
  chisq_by_base      = list_chisq_keys[ind_min]
  throw_away         = list_chisq_keys.pop(ind_min)

  chisq_vals_base    = np.array( [dic[chisq_by_base] for dic in list_base])[:]
  chisq_vals_extra_1 = np.array( [dic[list_chisq_keys[0]] for dic in list_of_lists[0]] )[:]  #[0 : n_min]
  chisq_vals_extra_2 = np.array( [dic[list_chisq_keys[1]] for dic in list_of_lists[1]] )[:]  #[0 : n_min]

  # Retrieve the filenames from each list
  list_files_base    = [dic['filename'] for dic in list_base][:]
  list_files_extra_1 = [dic['filename'] for dic in list_of_lists[0]]  #[0 : n_min]
  list_files_extra_2 = [dic['filename'] for dic in list_of_lists[1]]  #[0 : n_min]
  arr_files_extra_1  = np.array(list_files_extra_1)
  arr_files_extra_2  = np.array(list_files_extra_2)

  print ' - chisq: combine_chisq_three_lists: Show progress of an exhaustive loop'
  n_base             = len(list_files_base)
  list_chisq_mean    = []
  for i, filename in enumerate(list_files_base):

    ind_in_extra_1   = np.where(arr_files_extra_1 == filename)[0]
    ind_in_extra_2   = np.where(arr_files_extra_2 == filename)[0]
    if len(ind_in_extra_1) == 0: continue
    if len(ind_in_extra_2) == 0: continue
    ind_in_extra_1   = ind_in_extra_1[0]
    ind_in_extra_2   = ind_in_extra_2[0]

    # Now, for sure, filename has counterparts in both extra lists.
    chisq_base       = chisq_vals_base[i]
    chisq_extra_1    = chisq_vals_extra_1[ind_in_extra_1]
    chisq_extra_2    = chisq_vals_extra_2[ind_in_extra_2]
    chisq_mean       = (chisq_base + chisq_extra_1 + chisq_extra_2) / 3.0
    list_chisq_mean.append(chisq_mean)

    # Include the new "chisq_mean" item to the dictionaries in list_base
    list_base[i][list_chisq_keys[0]] = chisq_extra_1
    list_base[i][list_chisq_keys[1]] = chisq_extra_2
    list_base[i]['chisq_mean']       = chisq_mean

    progress         = float(i) / float(n_base)
    param_tools.update_progress(progress)

  list_chisq_mean    = np.array(list_chisq_mean)
  ind_sort           = np.argsort(list_chisq_mean)
  # list_chisq_mean    = list_chisq_mean[ind_sort]
  list_output        = []
  for i in ind_sort:
    list_output.append(list_base[i])

  return list_output

#=================================================================================================================
def evol(list_dic_modes, dic_obs):
  """
  Evaluate chi-square Goodness-of-Fit to based on Teff, log_g and if available their errors.
  @param list_dic_modes: each dictionary in the list contains the contents of the GYRE short summary output file
  @type: list_dic_modes: list
  @param dic_obs: dictionary with observational physical parameters for a star, mainly returned from
      mesa_gyre.stars
  @type dic_obs: dictionary
  @return: updated list of dictionaries of frequencies with two extra key/value paris per dictionary:
     - chisq_evol: chi-square GOF
     - reduced_chisq_evol: reduced chi-square GOF; reduced_chisq_evol = chisq_evol / 2.0
  @rtype: list of dictionaries
  """
  c_grav = 6.67428e-8 # cgs; as defined in mesa/const/const_def.f
  Msun = 1.9892e33
  Rsun = 6.9598e10
  Lsun = 3.8418e33
  Tsun = 5777.0
  logg_sun = np.log10(c_grav * Msun / Rsun**2.0)

  n_dic = len(list_dic_modes)
  if n_dic == 0:
    message = 'Error: chisq: evol: Input list is empty!'
    raise SystemExit, message

  names = dic_obs.keys()
  if 'Teff' in names and 'log_Teff' not in names: Teff = dic_obs['Teff']
  if not 'Teff' in names and 'log_Teff' in names: Teff = np.power(10.0, dic['log_Teff'])
  if 'Teff' in names and 'log_Teff' in names:     Teff = dic_obs['Teff']
  if not 'Teff' in names and 'log_Teff' not in names:
    message = 'Error: chisq: evol: Neither "Teff" nor "log_Teff" in input dic_obs'
    raise SystemExit, message

  if 'logg' in names:
    log_g = dic_obs['logg']
  else:
    message = 'Error: evol: "logg" not available in dic_obs'
    raise SystemExit, message

  if 'err_logg_1s' in names and 'err_Teff_1s' in names:
    chisq_with_err = True
    Teff_err = dic_obs['err_Teff_1s']
    logg_err = dic_obs['err_logg_1s']
  else:
    print ' - Warning: chisq: evol: Teff/logg Uncertainties not available! Skipping that ...'
    chisq_with_err = False

  updated_list = []
  for i_dic, dic in enumerate(list_dic_modes):
    M_star = dic['M_star']
    R_star = dic['R_star']
    L_star = dic['L_star']
    theo_logg = np.log10(c_grav * M_star/R_star**2.0)
    theo_Teff = Tsun * np.power((L_star/Lsun)/(R_star/Rsun)**2.0, 0.25)
    dic['Teff'] = theo_Teff
    dic['logg'] = theo_logg

    if chisq_with_err:
      chisq_evol = (theo_Teff-Teff)**2.0/Teff_err**2.0 + (theo_logg-logg)**2.0/logg_err**2.0
      reduced_chisq_evol = chisq_evol/2.0
      dic['chisq_evol']  = chisq_evol
      dic['reduced_chisq_evol'] = reduced_chisq_evol
    else:
      chisq_evol = (theo_Teff-Teff)**2.0 + (theo_logg-logg)**2.0
      dic['chisq_evol'] = chisq_evol
      dic['reduced_chisq_evol'] = chisq_evol

    updated_list.append(dic)


  return updated_list

#=================================================================================================================
def seismic_freq(list_dic_modes, dic_obs, freedom, el, freq_unit='Hz', pg='g', consecutive=False, strict=True, sigma=3):
  """
  Evaluate chi-square for seismic frequencies, when degree "l" is identified.
  Note: If freedom >= n_obs_freq, it is not meaningful to compute reduced chisquares; in such cases, the value 
        of reduced_chisq_seism_freq = chisq_seism_freq
  @param list_dic_modes: each dictionary in the list contains the contents of the GYRE short summary output file
  @type: list_dic_modes: list
  @param dic_obs: dictionary with observational frequencies for a star. The frequency unit comes with dic_obs
  @type dic_obs: dictionary
  @param freedom: integer specifying the number of free parameters that we are matching. E.g. freedom=3 if we match
        mass, age and metallicity.
  @type freedom: integer
  @param el: the degree to match observed and modeled frequencies with.
        Warning: if el is set to None, no filtering is applied and any mode of any el will be fitted!
  @type el: integer
  @param freq_unit: the frequency unit of GYRE output; default='Hz'
  @type freq_unit: string
  @param pg: specify to compute chisquare for either 'p' or 'g' modes. In case of doubt about the nature of the mode, one can 
         pass 'pg', in which case, ... The list_dic_freq in dic_obs will be 
         searched to use only those mode dictionaries whose 'pg' key has the value of either 'p' or 'g'.
         default='g'.
  @type pg: string
  @param consecutive: flag to force evaluating chi square for N number of consecutive modes. This is essential
       to use e.g. in case of SPB stars like (HD50230); default=False. If False, a blind search is used to find
       the best match which for g-modes has a strong tendency to low-Xc and dense spectrum of modes (where the
       chances of finding a close match is high).
  @type consecutive: boolean
  @param strict: flag to limit the chi-square evaluation to those modes within the observed range (True), or be less 
       strict, and find the closest frequency, even if it lies well outside the observed frequency interval withing 
       "sigma" times the extrema of observed frequency list. default=True
  @type strict: boolean
  @param sigma: the level of confidence around each each frequency for accepting/rejecting frequencies. default=3
  @type sigma: float
  @return: updated list of modes with two extra keys per each dictionary:
    - chisq_seism_freq
    - reduced_chisq_seism_freq
  @rtype: list of dictionaries
  """
  n_dic = len(list_dic_modes)
  if n_dic == 0:
    message = 'Error: chisq: seismic_freq: Input list is empty!'
    raise SystemExit, message

  list_obs_freq = dic_obs['list_dic_freq']
  list_obs_freq = stars.check_obs_vs_model_freq_unit(freq_unit, list_obs_freq)
  list_obs_freq = stars.filter_modes_by_pg(list_obs_freq, pg)
  n_obs_freq = len(list_obs_freq)

  allow_red_chisq = True
  if freedom >= n_obs_freq:
    logging.debug('Warning: chisq: seismic_freq: freedom >= n_obs_freq. Just simple chisq')
    allow_red_chisq = False

  all_freq     = np.array([np.real(dic['freq']) for dic in list_obs_freq])
  all_freq_err = np.array([dic['freq_err'] for dic in list_obs_freq])
  ind_sort     = np.argsort(all_freq)
  all_freq     = all_freq[ind_sort]
  all_freq_err = all_freq_err[ind_sort]
  min_freq     = all_freq[0] - sigma * all_freq_err[0]
  max_freq     = all_freq[-1] + sigma * all_freq_err[-1]

  updated_list = []
  for i_dic, dic in enumerate(list_dic_modes):
    n_modes = len(dic['l'])
    dic['AIC_c'] = 1e99      # Just initialize the field value
    if el is not None:
      ind_el  = [i for i in range(n_modes) if dic['l'][i] == el]
      n_modes = len(ind_el)
    if n_modes == 0:
      dic['chisq_seism_freq'] = 1e99
      dic['reduced_chisq_seism_freq'] = 1e99
      continue

    # filter modes that match el
    if el is not None:
      freq   = np.real(dic['freq'][ind_el])
      n_p    = dic['n_p'][ind_el]
      n_g    = dic['n_g'][ind_el]
      n_pg   = dic['n_pg'][ind_el]
    else:
      freq   = np.real(dic['freq'])
      n_p    = dic['n_p']
      n_g    = dic['n_g']
      n_pg   = dic['n_pg']

    # be strict, and exclude those modes that lie beyond the observed range
    if strict:
      # filter modes with frequency less than min_freq and greater than max_freq
      ind_freq_range = (freq >= min_freq) & (freq <= max_freq)
      if not any(ind_freq_range):
        print ' - Warning: chisq: seismic_freq: GYRE frequency outside range: {0}'.format(dic['filename'])
        dic['chisq_seism_freq'] = 1e99
        dic['reduced_chisq_seism_freq'] = 1e99
        continue
      freq   = freq[ind_freq_range]
      n_p    = n_p[ind_freq_range]
      n_g    = n_g[ind_freq_range]
      n_pg   = n_pg[ind_freq_range]
      n_modes= len(freq)

    # if matching consecutive frequencies, then find the closest theoretical frequency to the
    # smallest observed frequency value, and chop off the theoretical frequencies with the same
    # length of observational frequencies
    if consecutive:
      ind_conseq = np.argmin(np.abs(freq[:] - all_freq[0]))

      freq   = freq[ind_conseq : ind_conseq + n_obs_freq]
      n_pg   = n_pg[ind_conseq : ind_conseq + n_obs_freq]
      n_modes= len(freq)
      if n_modes != n_obs_freq:   # apples with apples
        dic['chisq_seism_freq'] = 1e99
        dic['reduced_chisq_seism_freq'] = 1e99
        continue

    # iterate over observed frequencies, find closest GYRE frequency, and add to chisq_seism
    chisq_seism = 0.0
    for i_obs in range(n_obs_freq):
      obs_freq     = all_freq[i_obs]
      obs_freq_err = all_freq_err[i_obs]

      if consecutive:
        theo_freq = freq[i_obs]
      else:
        ind_match = np.argmin(np.abs(freq[:] - obs_freq))
        theo_freq = freq[ind_match]

      chisq_seism += (obs_freq - theo_freq)**2.0 / (obs_freq_err)**2.0
      
    dic['chisq_seism_freq'] = chisq_seism
    if allow_red_chisq:
      reduced_chisq_seism = chisq_seism / (n_obs_freq - freedom)
      dic['reduced_chisq_seism_freq'] = reduced_chisq_seism
      # dic['AIC_c'] = get_AICc(chisq_seism, n_obs_freq, freedom)
    else:
      dic['reduced_chisq_seism_freq'] = chisq_seism
  
    updated_list.append(dic)

  if False:
    all_chisq_f  = np.array( [dic['reduced_chisq_seism_freq'] for dic in updated_list] )

    lp           = dic['filename'].rfind('/') + 1
    if lp==-1: lp= 0
    ind_chisq_f  = np.argmin(all_chisq_f)

    # filenames for the best models
    f_chisq_f    = updated_list[ind_chisq_f]['filename'][lp:]
    ind_sort     = np.argsort(all_chisq_f)
    updated_list = [updated_list[i] for i in ind_sort]

    print ' - chisq: seismic_freq:'
    print '   From {0} input, only {1} survived as output'.format(n_dic, len(updated_list))
    print '   by freq: {0:06.2f} for {1}'.format(all_chisq_f[ind_chisq_f], f_chisq_f)
    print

  if n_dic ==1 and len(updated_list) == 0:
    # This is a problematic case (when using one_file_different_obs_freq), 
    # and we append the same dictionary again
    updated_list.append(dic)
  
  return updated_list

#=================================================================================================================
def seismic_P(list_dic_modes, dic_obs, freedom, el=None, freq_unit='Hz', consecutive=False, sigma=3, P_shift=0.0):
  """
  Evaluate chi-square for seismic frequencies, when degree "l" is identified. The computation is based only on periods.
  Generally, the formula for chisquare is [ sum_1^N (P_obs_i - (P_th_i + P_shift))^2 / (sigma_f_i)^2 ] /(N - freedom)
  This modification was suggested by Steven Kawaler (after a short visit to IvS on early Nov 2014), and the idea is to
  compensate for some possible unknown surface (or systematic) effects in the models when fitting the data.

  @param list_dic_modes: each dictionary in the list contains the contents of the GYRE short summary output file
  @type: list_dic_modes: list
  @param dic_obs: dictionary with observational frequencies for a star. The frequency unit comes with dic_obs
  @type dic_obs: dictionary
  @param freedom: integer specifying the number of free parameters that we are matching. E.g. freedom=3 if we match
        mass, age and metallicity.
  @type freedom: integer
  @param el: the degree to match observed and modeled frequencies with; default=None.
        Warning: if el is set to None, no filtering is applied and any mode of any el will be fitted!
  @type el: integer
  @param freq_unit: the frequency unit of GYRE output; default='Hz'
  @type freq_unit: string
  @param consecutive: flag to force evaluating chi square for N number of consecutive modes. This is essential
       to use e.g. in case of SPB stars like (HD50230); default=False. If False, a blind search is used to find
       the best match which for g-modes has a strong tendency to low-Xc and dense spectrum of modes (where the
       chances of finding a close match is high).
  @type consecutive: boolean
  @param sigma: the level of confidence around each each frequency for accepting/rejecting frequencies. default=3
  @type sigma: float
  @param P_shift: Apply a constant offset in theoretical periods before evaluating the chisquare. default=0.0.
       Note: P_shift must have the same units as the inverse of the keyword "freq_unit", e.g. sec.
  @type P_shift: float
  @return: updated list of modes with two extra keys per each dictionary:
    - chisq_seism_freq
    - reduced_chisq_seism_freq
  @rtype: list of dictionaries
  """
  n_dic = len(list_dic_modes)
  if n_dic == 0:
    message = 'Error: chisq: seismic_P: Input list is empty!'
    raise SystemExit, message

  list_obs_freq = dic_obs['list_dic_freq']
  n_obs_freq = len(list_obs_freq)
  list_obs_freq = stars.check_obs_vs_model_freq_unit(freq_unit, list_obs_freq)

  if freedom >= n_obs_freq:
    logging.error('Error: chisq: seismic_P: freedom >= n_obs_freq')
    raise SystemExit

  all_freq     = np.array([np.real(dic['freq']) for dic in list_obs_freq])
  all_freq_err = np.array([dic['freq_err'] for dic in list_obs_freq])
  all_per      = np.zeros(n_obs_freq)
  all_per_err  = np.zeros(n_obs_freq)
  for i in range(n_obs_freq):
    per, per_err = stars.MC_freq_to_per(all_freq[i], all_freq_err[i])
    all_per[i]   = per
    all_per_err[i] = per_err

  all_per      = all_per[::-1]        # from min per to max per
  all_per_err  = all_per_err[::-1]   # from min per to max per
  min_per      = all_per[0] - sigma * all_per_err[0]
  max_per      = all_per[-1] + sigma * all_per_err[-1]

  # ind_sort     = np.argsort(all_freq)
  # all_freq     = all_freq[ind_sort]
  # all_freq_err = all_freq_err[ind_sort]
  # min_freq     = all_freq[0] - sigma * all_freq_err[0]
  # max_freq     = all_freq[-1] + sigma * all_freq_err[-1]

  updated_list = []
  for i_dic, dic in enumerate(list_dic_modes):
    n_modes = len(dic['l'])
    if el is not None:
      ind_el  = [i for i in range(n_modes) if dic['l'][i] == el]
      n_modes = len(ind_el)
    if n_modes == 0:
      dic['chisq_seism_P'] = 1e99
      dic['reduced_chisq_seism_P'] = 1e99
      continue

    # filter modes that match el
    if el is not None:
      freq   = np.real(dic['freq'][ind_el])
      n_p    = dic['n_p'][ind_el]
      n_g    = dic['n_g'][ind_el]
      n_pg   = dic['n_pg'][ind_el]
    else:
      freq   = np.real(dic['freq'])
      n_p    = dic['n_p']
      n_g    = dic['n_g']
      n_pg   = dic['n_pg']
    per      = 1./freq
    per      = per[::-1]           # from min period to max period

    # filter modes with period less than max_per and greater than min_per
    ind_per_range = (per >= min_per) & (per <= max_per)
    if not any(ind_per_range):
      print ' - Warning: chisq: seismic_P: GYRE periods outside range: {0}'.format(dic['filename'])
      dic['chisq_seism_P'] = 1e99
      dic['reduced_chisq_seism_P'] = 1e99
      continue
    per     = per[ind_per_range]
    n_p     = n_p[ind_per_range]
    n_g     = n_g[ind_per_range]
    n_pg    = n_pg[ind_per_range]
    n_modes = len(per)

    # if matching consecutive periods, then find the closest theoretical period to the
    # shortest observed period value, and chop off the theoretical periods with the same
    # length of observational periods
    if consecutive:
      ind_conseq = np.argmin(np.abs(per[:] - all_per[0]))

      per  = per[ind_conseq : ind_conseq + n_obs_freq]
      n_pg = n_pg[ind_conseq : ind_conseq + n_obs_freq]
      n_modes= len(per)
      if n_modes != n_obs_freq:   # apples with apples
        dic['chisq_seism_P'] = 1e99
        dic['reduced_chisq_seism_P'] = 1e99
        continue

    # iterate over observed periods, find closest GYRE period, and add to chisq_seism
    chisq_seism = 0.0
    for i_obs in range(n_obs_freq):
      obs_per     = all_per[i_obs]
      obs_per_err = all_per_err[i_obs]

      if consecutive:
        theo_per  = per[i_obs]
      else:
        ind_match = np.argmin(np.abs(freq[:] - obs_freq))
        theo_freq = freq[ind_match]
        ind_match = np.argmin(np.abs(per[:] - obs_per))

      chisq_seism += (obs_per - (theo_per + P_shift))**2.0 / (obs_per_err)**2.0

    reduced_chisq_seism = chisq_seism / (n_obs_freq - freedom)
    dic['chisq_seism_P'] = chisq_seism
    dic['reduced_chisq_seism_P'] = reduced_chisq_seism

    updated_list.append(dic)

  all_chisq_P  = np.array( [dic['reduced_chisq_seism_P'] for dic in updated_list] )

  lp           = dic['filename'].rfind('/') + 1
  if lp==-1: lp= 0
  ind_chisq_P  = np.argmin(all_chisq_P)

  # filenames for the best models
  f_chisq_P    = updated_list[ind_chisq_P]['filename'][lp:]
  ind_sort     = np.argsort(all_chisq_P)
  updated_list = [updated_list[i] for i in ind_sort]

  print ' - chisq: seismic_P:'
  print '   From {0} input, only {1} survived as output'.format(n_dic, len(updated_list))
  print '   by Per: {0:06.2f} for {1} \n'.format(all_chisq_P[ind_chisq_P], f_chisq_P)

  return updated_list

#=================================================================================================================
def multiproc_seismic_dP(list_dic_dP, dic_obs, freedom, el=None, consecutive=False, freq_unit='Hz', sigma=3,
			 n_cpu=None):
  """
  Use "multiprocessing" to evaluate seismic_dP chisquare. For further information, read the documentation of 
  the seismic_dP() function.
  """
  n_dic = len(list_dic_dP)
  if n_dic == 0:
    logging.error('chisq: multiproc_seismic_dP: Input list is empty')
    raise SystemExit, 'Error: chisq: multiproc_seismic_dP: Input list is empty'
    
  if n_cpu is None: n_cpu = cpu_count()
  pool = Pool(processes=n_cpu)
  
  t_start = time.time()
  
  n_chops = n_dic / n_cpu + 1
  
  result = []
  for i_cpu in range(n_cpu):
    n_from = i_cpu * n_chops
    n_to   = n_from + n_chops
    if i_cpu == n_cpu - 1: n_to = n_dic
    dic_chop = list_dic_dP[n_from : n_to]
    result.append(pool.apply_async( seismic_dP, args=(dic_chop, dic_obs, freedom, el, 
						      consecutive, freq_unit, sigma) ))
  pool.close()
  pool.join()
  
  list_output = []
  for i_cpu in range(n_cpu): list_output = result[i_cpu].get()
  
  dt_sec = int(time.time() - t_start)
  
  print ' - chisq: multiproc_seismic_dP:'
  print '   N_cpu={0}, N_files={1}, Time={2} [sec] \n'.format(n_cpu, n_dic, dt_sec)
  
  return list_output
  
#=================================================================================================================
def seismic_dP(list_dic_dP, dic_obs, freedom, el=None, pg='g', consecutive=False, freq_unit='Hz', sigma=3, show=False):
  """
  Evaluate chi square based on comparing observed period spacing and the synthetic period spacing patterns.
  if el is not assigned, the period spacing pattern of modes of any degree (practically el=1, 2) will be fitted.
  Note: we first clip off theoretical frequencies based on the observed frequency range, and evaluate 
        reduced_chisq_seism. If some models have different number of modes than observation between the observed
        frequency range, they are thrown away. Based on this, we proceed to compute reduced_chisq_seism_P, 
        and reduced_chisq_seism_dP
  @param list_dic_dP: each dictionary in the list contains the contents of the GYRE short summary output file,
      in addition to the model period spacing.
  @type: list_dic_dP: list
  @param dic_obs: dictionary with observational frequencies for a star. The frequency unit comes with dic_obs.
      The input list of frequencies are then converted to the period spacing by calling stars.MC_freq_to_per()
  @type dic_obs: dictionary
  @param freedom: integer specifying the number of free parameters that we are matching. E.g. freedom=3 if we match
        mass, age and metallicity.
  @type freedom: integer
  @param el: the degree to match observed and modeled frequencies with; default=None.
        Warning: if el is set to None, no filtering is applied and any mode of any el will be fitted!
  @type el: integer
  @param sigma: the level of confidence around each each frequency for accepting/rejecting frequencies. default=3
  @type sigma: float
  @param pg: specify to compute chisquare for just 'g' modes. The list_dic_freq in dic_obs will be 
         searched to use only those mode dictionaries whose 'pg' key has the value of either 'g'; otherwise,
         an exception is raised. default='g'.
  @type pg: string
  @param consecutive: flag to force evaluating chi square for N number of consecutive modes. This is essential
       to use e.g. in case of SPB stars like (HD50230); default=False. If False, a blind search is used to find
       the best match which for g-modes has a strong tendency to low-Xc and dense spectrum of modes (where the
       chances of finding a close match is high).
  @type consecutive: boolean
  @param freq_unit: the frequency unit of GYRE output; default='Hz'
  @type freq_unit: string
  """
  n_dic = len(list_dic_dP)
  if n_dic == 0:
    logging.error('chisq: seismic_dP: Input list is empty')
    raise SystemExit

  if 'list_dic_freq' not in dic_obs.keys():
    logging.error('chisq: seismic_dP: "list_dic_freq" not in the input dic_obs!')
    raise SystemExit

  if el is None:
    logging.error('chisq: seismic_dP: For now, we only support cases where el is specified')
    raise SystemExit

  if pg != 'g':
    logging.error('chisq: seismic_dP: Only pg="g" is accepted here.')
    raise SystemExit, 'Error: chisq: seismic_dP: Only pg="g" is accepted here.'

  list_dic_freq = dic_obs['list_dic_freq']
  list_dic_freq = stars.check_obs_vs_model_freq_unit(freq_unit, list_dic_freq) # fix the unit
  list_dic_freq = stars.filter_modes_by_pg(list_dic_freq, 'g')
  dic_obs['list_dic_freq'] = list_dic_freq
  # update the list of frequencies to include the periods, period spacings and their associated errors
  dic_obs       = stars.list_freq_to_list_dP(dic_obs, freq_unit=freq_unit)
  list_dic_freq = dic_obs['list_dic_freq']
  n_freq        = len(list_dic_freq)
  list_freq     = np.array([ dic['freq'] for dic in list_dic_freq ])
  list_freq_err = np.array([ dic['freq_err'] for dic in list_dic_freq ])
  list_P        = np.array([ dic['P'] for dic in list_dic_freq])
  list_P_err    = np.array([ dic['P_err'] for dic in list_dic_freq ])
  list_dP       = np.array([ dic['dP'] for dic in list_dic_freq ])
  list_dP_err   = np.array([ dic['dP_err'] for dic in list_dic_freq ])
  ind_sort      = np.argsort(list_P)
  list_freq     = list_freq[ind_sort]
  list_freq_err = list_freq_err[ind_sort]
  list_P        = list_P[ind_sort]
  list_P_err    = list_P_err[ind_sort]
  list_dP       = list_dP[ind_sort]
  list_dP_err   = list_dP_err[ind_sort]
  min_P         = list_P[0]  - sigma * list_P_err[0]  # 3-sigma
  max_P         = list_P[-1] + sigma * list_P_err[-1] # 3-sigma
  min_f         = list_freq[-1] - sigma * list_freq_err[-1]
  max_f         = list_freq[0] + sigma * list_freq_err[0]

  n_obs_freq = len(list_freq)
  n_obs_P    = len(list_P)
  n_obs_dP   = len(list_dP) - 1 # since the dP for the highest period mode is set to NaN; thus, we do not include it
  if freedom >= n_obs_dP:
    logging.error('Error: chisq: seismic_dP: freedom >= n_obs_dP')
    raise SystemExit

  if False:
    print '\n - chisq: seismic_dP: List of Period Spacing to Match:'
    for i in range(n_freq):
      print '   f={0:16.8f} +/- {1:16.8f} uHz;   P={2:16.8f} +/- {3:16.8f} sec;   dP={4:16.8f} +/- {5:16.8f} sec'.format(
            list_freq[i]*1e6, list_freq_err[i]*1e6, list_P[i], list_P_err[i], list_dP[i], list_dP_err[i])
    print

  # Loop over all dP dictionaries, clip off those that fit into observed periods, and evaluate chis square
  updated_list = []
  str_el = 'el_' + str(el) + '_gm_'
  str_f  = str_el + 'freq_re'
  str_P  = str_el + 'P'
  str_dP = str_el + 'dP'
  for i_dic, dic in enumerate(list_dic_dP):
     freq   = dic[str_f]

     if consecutive:
       ind_conseq = np.argmin(np.abs(freq[:] - list_freq[0]))

       freq   = freq[ind_conseq : ind_conseq + n_obs_freq]
       n_modes= len(freq)
       if n_modes != n_obs_freq:   # apples with apples
         dic['chisq_seism_freq']         = 1e99
         dic['reduced_chisq_seism_freq'] = 1e99
         dic['chisq_seism_P']            = 1e99
         dic['reduced_chisq_seism_P']    = 1e99
         dic['chisq_seism_dP']           = 1e99
         dic['reduced_chisq_seism_dP']   = 1e99
         dic['chisq_seism_P_and_dP']     = 1e99
         dic['reduced_chisq_seism_P_and_dP'] = 1e99
         dic['chisq_seism_f_and_dP']     = 1e99
         dic['reduced_chisq_seism_f_and_dP'] = 1e99
         continue

     # iterate over observed frequencies, find closest GYRE frequency, and add to chisq_seism
     chisq_seism = 0.0
     for i_obs in range(n_obs_freq):
       obs_freq     = list_freq[i_obs]
       obs_freq_err = list_freq_err[i_obs]

       if consecutive:
         theo_freq = freq[i_obs]
       else:
         ind_match = np.argmin(np.abs(freq[:] - obs_freq))
         theo_freq = freq[ind_match]

       chisq_seism += (obs_freq - theo_freq)**2.0 / (obs_freq_err)**2.0
      
     reduced_chisq_seism = chisq_seism / (n_obs_freq - freedom)
     dic['chisq_seism_freq'] = chisq_seism
     dic['reduced_chisq_seism_freq'] = reduced_chisq_seism

     chisq_P  = 0.0
     per      = 1.0 / freq
     for i_obs in range(n_obs_P):
       P_theo     = per[i_obs]
       P_obs      = list_P[i_obs]
       P_err_obs  = list_P_err[i_obs]
       chisq_P    += (P_obs  - P_theo)**2.0  / (P_err_obs)**2.0
     dic['chisq_seism_P']          = chisq_P
     dic['reduced_chisq_seism_P']  = chisq_P / (n_obs_P - freedom)   # excluding one degree of freedom



     chisq_dP = 0.0
     dP       = per[1:] - per[:-1]
     for i_obs in range(n_obs_dP):
       dP_theo    = dP[i_obs]
       if np.isnan(dP_theo): continue
       dP_obs     = list_dP[i_obs]
       dP_err_obs = list_dP_err[i_obs]
       chisq_dP  += (dP_obs - dP_theo)**2.0 / (dP_err_obs)**2.0

     dic['chisq_seism_dP'] = chisq_dP
     dic['reduced_chisq_seism_dP'] = chisq_dP / (n_obs_dP - freedom)  # excluding one degree of freedom

     dic['chisq_seism_P_and_dP']   = (chisq_P + chisq_dP) / 2
     dic['reduced_chisq_seism_P_and_dP'] = (dic['reduced_chisq_seism_P'] +
                                              dic['reduced_chisq_seism_dP']) / 2 # excluding two degrees of freedom


     # Combinations of different Chisq terms
     dic['chisq_seism_P_and_dP']   = (chisq_P + chisq_dP) / 2
     dic['reduced_chisq_seism_P_and_dP'] = (dic['reduced_chisq_seism_P'] +
                                              dic['reduced_chisq_seism_dP']) / 2 # excluding two degrees of freedom

     dic['chisq_seism_f_and_dP'] = (chisq_seism + chisq_dP) / 2
     dic['reduced_chisq_seism_f_and_dP'] = (dic['reduced_chisq_seism_freq'] + dic['reduced_chisq_seism_dP']) / 2

     
     updated_list.append(dic)

  all_chisq_P  = np.array( [dic['reduced_chisq_seism_P'] for dic in updated_list] )
  all_chisq_dP = np.array( [dic['reduced_chisq_seism_dP'] for dic in updated_list] )
  all_chisq_P_dP = np.array( [dic['reduced_chisq_seism_P_and_dP'] for dic in updated_list] )
  all_chisq_f  = np.array( [dic['reduced_chisq_seism_freq'] for dic in updated_list] )
  all_chisq_f_dp = np.array( [dic['reduced_chisq_seism_f_and_dP'] for dic in updated_list] )

  lp           = dic['filename'].rfind('/') + 1
  if lp==-1: lp= 0
  ind_chisq_P  = np.argmin(all_chisq_P)
  ind_chisq_dP = np.argmin(all_chisq_dP)
  ind_chisq_P_dP = np.argmin(all_chisq_P_dP)
  ind_chisq_f  = np.argmin(all_chisq_f)
  ind_chisq_f_dP = np.argmin(all_chisq_f_dp)

  # filenames for the best models
  f_chisq_P    = updated_list[ind_chisq_P]['filename'][lp:]
  f_chisq_dP   = updated_list[ind_chisq_dP]['filename'][lp:]
  f_chisq_P_dP = updated_list[ind_chisq_P_dP]['filename'][lp:]
  f_chisq_f    = updated_list[ind_chisq_f]['filename'][lp:]
  f_chisq_b    = updated_list[ind_chisq_f_dP]['filename'][lp:]

  if show:
    print ' - chisq: seismic_dP:'
    print '   by freq: {0:06.2f} for {1}'.format(all_chisq_f[ind_chisq_f], f_chisq_f)
    print '   by P:    {0:06.2f} for {1}'.format(all_chisq_P[ind_chisq_P], f_chisq_P)
    print '   by dP:   {0:06.2f} for {1}'.format(all_chisq_dP[ind_chisq_dP], f_chisq_dP)
    print '   by P+dP: {0:06.2f} for {1}'.format(all_chisq_P_dP[ind_chisq_P_dP], f_chisq_P_dP)
    print '   by f+dP: {0:06.2f} for {1}'.format(all_chisq_f_dp[ind_chisq_f_dP], f_chisq_b)
    print

  return updated_list

#=================================================================================================================
def one_file_different_obs_freq(h5_file, chisq_by, freedom, freq_unit, 
                                list_dic_obs, list_sigma, list_el, list_pg, list_conseqcutive, list_strict,
                                list_chisq_names):
  """
  Compute chi-square scores for one file, one degree of freedom, one frequency unit, but a list of sigma,
  a list of observed frequencies, el, pg, for a star, and a list of consecutive and strict decisions.
  The list of chi-square output from each choice of dic_obs in list_dic_obs can receive a name which is passed
  by list_chisq_names.
  Note: If an input h5_file raises IOError (for any reason), the return value is None. So, make sure you 
        capture this when calling this routine.
  @param h5_file: full path to one GYRE output file. It will be read here
  @type h5_file: string
  @param chisq_by: Calculate chi-square based on this choice. Current supported values are 
        - reduced_chisq_seism_freq
        - chisq_seism_freq
        - reduced_chisq_seism_f_and_dP
  @type chisq_by: string
  @param freedom: the number of degrees of freedom
  @type freedom: integer
  @param freq_unit: frequency unit to adapt the models and observations
  @type freq_unit: string
  @param list_dic_obs: list of full star info that contain different chunks of list_dic_freq. 
         The lists should better be for one single star, but only differ in the list_dic_freq. A perfect 
         example of such a case/use is the CoRoT B3V star: HD 50230. The number of items in list_dic_obs
         determines how many times the chisquares have to be computed
  @type list_dic_obs: list of dictionaries
  @param list_sigma: list of sigma values around each mode to multiply the frequency error with.
  @type list_sigma: list of floats
  @param list_el: list of el values; for identified modes, it should be an integer, or None otherwise.
  @type list_el: list of integers of None
  @param list_pg: list of pg specification. This info is contained in list_dic_freq for each mode (i.e. dic)
        It can be either 'p' or 'g' for identified modes or 'pg' otherwise
  @param list_conseqcutive: list of consecutive decisions
  @type list_conseqcutive: list of boolean
  @param list_strict: list of strict decisions
  @type list_strict: list of boolean
  @param list_chisq_names: Once a chisquare is computed, it is returned as the key, value pair in a dictionary 
        that can be accessed by chisq_by. The key is then substituted by one of those in list_chisq_names, and 
        as a result all these new keys are availble in the returned result.
  @type list_chisq_names: list of strings
  @return: dictionary with chi-square information. It has additional keys as passed by list_chisq_names
  @rtype: dictionary
  """
  n_chisq = len(list_dic_obs)
  try:
    assert n_chisq == len(list_sigma)
    assert n_chisq == len(list_el)
    assert n_chisq == len(list_pg)
    assert n_chisq == len(list_conseqcutive)
    assert n_chisq == len(list_strict)
    assert n_chisq == len(list_chisq_names)
  except AssertionError:
    logging.error('one_file_different_obs_freq: The number of list elements should exactly match! Check again')
    raise SystemExit

  do_chi2_f     = chisq_by == 'reduced_chisq_seism_freq' or chisq_by == 'chisq_seism_freq'
  do_chi2_f_dP  = chisq_by == 'reduced_chisq_seism_f_and_dP'

  # Capture files with IO issues, and ignore them.
  try:
    dic_h5    = read.read_multiple_gyre_files([h5_file])[0]
  except IOError:
    print 'IOError: {0}'.format(h5_file)
    logging.debug('IOError: {0}'.format(h5_file))
    return None
  keys_param    = ['M_star', 'R_star', 'L_star']

  chisq_arr     = np.zeros(n_chisq)
  cumul         = 0.0
  dic           = {}
  for i in range(n_chisq):

    dic_obs     = list_dic_obs[i]
    sigma       = list_sigma[i]
    el          = list_el[i]
    pg          = list_pg[i]
    consecutive = list_conseqcutive[i]
    strict      = list_strict[i]
    chisq_name  = list_chisq_names[i]

    if do_chi2_f:
      a_list_ch = seismic_freq([dic_h5], dic_obs=dic_obs, freedom=freedom, el=el, freq_unit=freq_unit,
                            pg=pg, consecutive=consecutive, strict=strict, sigma=sigma)


    if do_chi2_f_dP:
      a_list_ch = seismic_dP([dic_h5], dic_obs=dic_obs, freedom=freedom, el=el, pg=pg, freq_unit=freq_unit,
                             consecutive=consecutive, sigma=sigma, show=False)

    dic_chisq   = a_list_ch[0] #.copy()
    chisq_val   = dic_chisq.pop(chisq_by, None)
    # dic_chisq[chisq_name] = chisq_val
    dic[chisq_name] = chisq_val
    cumul       += chisq_val

  # Get rid of those keys and values that contain chi-square values.
  # They are already properly renamed
  keys_remove = [key for key in dic_chisq.keys() if 'chisq' in key]
  for key in keys_remove: throw_away = dic_chisq.pop(key, None) 
  for key, val in dic.items(): dic_chisq[key] = val
  chisq_mean    = cumul / float(n_chisq)
  dic_chisq['chisq_mean'] = chisq_mean
  for key in keys_param: dic_chisq[key] = dic_h5[key]

  logging.info('chisq: one_file_different_obs_freq: {0}'.format(h5_file))

  return dic_chisq

#=================================================================================================================
def one_thread_different_obs_freq(list_h5_files, chisq_by, freedom, freq_unit, 
                                list_dic_obs, list_sigma, list_el, list_pg, list_conseqcutive, list_strict,
                                list_chisq_names):
  """
  Call one_file_different_obs_freq iteratively for a list of in put mode info, and collect the output as a list 
  of dictionaries.
  For the definition of the input, refer to the documentation below chisq.one_file_different_obs_freq().
  """
  n_h5 = len(list_h5_files)
  logging.info('chisq: one_thread_different_obs_freq: Receiving {0} jobs'.format(n_h5))
  list_output = []
  for i, f in enumerate(list_h5_files):
    dic_chisq = one_file_different_obs_freq(f, chisq_by=chisq_by, freedom=freedom, freq_unit=freq_unit,
                                list_dic_obs=list_dic_obs, list_sigma=list_sigma, list_el=list_el, 
                                list_pg=list_pg, list_conseqcutive=list_conseqcutive, list_strict=list_strict,
                                list_chisq_names=list_chisq_names)
    if dic_chisq is None: continue
    list_output.append(dic_chisq)
    progress         = float(i) / float(n_h5)
    param_tools.update_progress(progress)

  print
  return list_output

#=================================================================================================================
def get_AICc(chi_square, Nobs, dof):
  """
  Compute corrected Akaike Information Criterion AIC_c based on Akaike (1974), and Burnham and Anderson (2002, 2004)
  Let Nobs == N, and dof = m, then
                 AIC_c = 2(N-m) + chi_square + 2(N-m)(N-m-1)/(m-1)
  which rewards fitting by the (hopefully minimum) chi_square terma and penalizes the
  complexity by the first and the third terms.
  @param chi_square: The chisquare of the model
  @type chi_square: float
  @param Nobs: The number of observed quantity or data points
  @type Nobs: integer
  @param dof: The number of degrees of freedom in the grid or its dimensionality
  @type dof: integer
  @return: AIC_c score
  @rtype: float
  """
  
  x = Nobs - dof
  if x < 2:
    logging.error('chisq: get_AICc: Nobs - DOF < 2')
    print 'Error: chisq: get_AICc: Nobs - DOF < 2'
    raise SystemExit
  
  return 2. * x + chi_square + 2. * x * (x - 1.0) / (dof-1.0)

#=================================================================================================================
def sort_and_write_chisq(list_dic, chisq_by, normalize=False, file_out=None):
  """
  Sort the list of chisquares that are almost randomly collected and computed based on their chi-square values
  specified by the key "chisq_by". The sorting is in increasing order, so that the best model will be the one with
  index 0.
  @param list_dic: list of dictionaries with all mode information accumulated. In addition, every
         ditionary must have the additional key specified by the "chisq_by".
  @type list_dic: list of dictionaries
  @param chisq_by: Specify which scheme to call chisq. Possible options are:
          - reduced_chisq_seism_freq:
            This only calls chisq.seismic_freq(), and is much faster. The good model from this will automatically
            produce a good fit to the period spacing.
          - reduced_chisq_seism_f_and_dP: Then call
            This calls period_spacing.gen_list_dic_dP(), period_spacing.update_dP_dip() and chisq.seismic_dP().
            Therefore, it takes much longer to run
  @type chisq_by: string
  @param normalize: If set True, then the "frequency" chi-square list will be divided by the minimum chi-square value  
       from the list, and then, the chis-suqre for the frequency list will start from 1.00. This is later passed to 
       mesa_gyre.write.list_chisq_to_ascii(). This is achieved by calling chisq.normalize_list_dic_chisq(). default=True
  @type normalize: boolean
  @return:
  @rtype:
  """
  n_dic    = len(list_dic)
  if n_dic == 0:
    logging.error('chisq: sort_and_write_chisq: Input list is empty')
    raise SystemExit, 'Error: chisq: sort_and_write_chisq: Input list is empty'

  if not list_dic[0].has_key(chisq_by):
    logging.error('chisq: sort_and_write_chisq: "{0}" is not availble in the \
                     dictionaries'.format(chisq_by))
    raise SystemExit, 'Error: chisq: sort_and_write_chisq: "{0}" is not availble \
                       in the dictionaries'.format(chisq_by)

  if normalize: 
    list_dic  = normalize_list_dic_chisq(list_dic, chisq_by)

  list_chisq  = np.array([ dic[chisq_by] for dic in list_dic ])
  ind_sort    = np.argsort(list_chisq)
  list_chisq  = list_chisq[ind_sort]
  sorted_list = [list_dic[i] for i in ind_sort]

  if file_out is not None:
    write.list_chisq_to_ascii(sorted_list, chisq_by, max_row=None, file_out=file_out)

  return sorted_list

#=================================================================================================================
def evol_abund(dic_hist, recarr_abund, model_number):
  """
  """
  n_rec_elem = len(recarr_abund)
  elem_names = [recarr_abund[i]['elem'] for i in range(n_rec_elem)]

  # required columns from MESA history file
  required_fileds = ['surface_h1', 'surface_h2', 'surface_he3', 'surface_he4', 'surface_li7', 'surface_be9', 'surface_b11',
                               'surface_c12', 'surface_n14', 'surface_o16', 'surface_f19', 'surface_ne20', 'surface_na23',
                               'surface_mg24', 'surface_al27', 'surface_si28', 'surface_s32', 'surface_ar36', 'surface_ca40',
                               'surface_cr52', 'surface_mn55', 'surface_fe56', 'surface_ni58']

  hist = dic_hist['hist']
  names  = hist.dtype.names

  dic_avail_keys = {}
  for name in names: dic_avail_keys[name] = 0.0
  dic_keys = read.check_key_exists(dic_avail_keys, required_fileds)

  # clip off the relevant row from the whole hist matrix
  ind_model_number = np.where((hist['model_number'] == model_number))[0]
  hist = hist[ind_model_number]

  chisq = 0.0
  N_chisq = 0

  for i_elem, elem in enumerate(elem_names):

    if elem == 'h1': continue
    if elem == 'h2': continue
    if elem == 'he3': continue
    if elem == 'he4': continue

    if dic_keys['surface_h2']:
      X_h2 = hist['surface_h2'][0]
    else:
      X_h2 = 0.0
    X_H = hist['surface_h1'][0] + X_h2

    full_elem_name = 'surface_' + elem
    if not dic_keys[full_elem_name]: continue

    elem_A = recarr_abund[i_elem]['elem_A']
    Xa = hist[full_elem_name][0]

    Na_th   = np.log10(Xa /(X_H * elem_A)) + 12.0
    Na_obs = recarr_abund[i_elem]['abund']
    Na_obs_err = recarr_abund[i_elem]['abund_err']

    chisq += (Na_obs - Na_th)**2.0 / Na_obs_err**2.0
    N_chisq += 1

  return chisq / N_chisq

#=================================================================================================================
def get_a09_p13_abund():
  """
  Returns a numy record array with the element abundnaces in number fraction and their corresponding errors from
  - Nieva & Przybilla, 2012, A&A, 539, 143
  - Przybilla, et al. 2013, proceeding to appear in EAS Pub. Ser.
  - Asplund et al. 2009, ARA&A (for Be, B and Ni)
   @return: numpy record array containing the following fields:
      - elem: string, giving the name of the element, e.g. fe for ^{56}Fe, and so on
      - abund: float, giving the number fraction of the element [El/H], e.g. [Fe/H]=7.52
      - abund_err: float, giving the 1-sigma uncertainty in the number fraction of the element, e.g. [Fe/H]=7.52 +/- 0.03
  @rtype: numpy record array
  """
  # Element names
  elem_names = ['h1', 'he4', 'be9', 'b11', 'c12', 'n14', 'o16', 'ne20', 'na23',
                           'mg24', 'al27', 'si28', 's32', 'ar36', 'ca40', 'cr52', 'mn55', 'fe56', 'ni58']

  # Element atomic number, A = N + Z
  elem_A = [1, 4, 9, 11, 12, 14, 16, 20, 23,
                   24, 27, 28, 32, 36, 40, 52, 55, 56, 58]

  # Number density in Spectroscopic Notation
  elem_abund = [12.0, 10.99, 1.38, 2.70, 8.33, 7.79, 8.76, 8.09, 6.24,
                           7.56, 6.30, 7.50, 7.14, 6.50, 6.34, 5.64, 5.43, 7.52, 6.22]

  # 1-sigma uncertainty in number density
  elem_abund_err = [0.0, 0.01, 0.09, 0.20, 0.04, 0.04, 0.05, 0.05, 0.04,
                           0.05, 0.07, 0.05, 0.06, 0.08, 0.04, 0.04, 0.04, 0.03, 0.04]

  n_elem = len(elem_names)
  elem = [ [elem_names[i], elem_A[i], elem_abund[i], elem_abund_err[i]] for i in range(n_elem)]

  recarr = np.core.records.fromarrays(np.array(elem).transpose(), names=['elem', 'elem_A', 'abund', 'abund_err'], formats='a4, i4, f8, f8')

  return recarr

#=================================================================================================================
def delete_chisq_keys_from_dic(list_dic_modes_with_chisq):
  """
  One may use this function to remove all keys and their values carrying the results of computing chi-square
  and friends (e.g. AIC). This rejuvenates the individual mode dictionaries, and one can reuse them for extra
  chi-square computation without reading the whole files again!
  @param list_dic_modes_with_chisq: list of mode info. If still they do not have chi-square related keys, 
         popping out any key from these dictionaries does NOT raise any exception, and is quite safe.
  @type list_dic_modes_with_chisq: list of dictionaries
  @return: The same list of mode info with all chi-square-related items removed.
  @rtype: list of dictionaries
  """
  list_copy   = list_dic_modes_with_chisq[:]
  list_output = []
  for dic in list_copy:
    dic = dic.copy()
    dic.pop('chisq_seism_P', None)
    dic.pop('chisq_seism_dP', None)
    dic.pop('chisq_seism_freq', None)
    dic.pop('chisq_seism_P_and_dP', None)
    dic.pop('chisq_seism_f_and_dP', None)
    dic.pop('reduced_chisq_seism_P', None)
    dic.pop('reduced_chisq_seism_dP', None)
    dic.pop('reduced_chisq_seism_freq', None)
    dic.pop('reduced_chisq_seism_f_and_dP', None)
    dic.pop('reduced_chisq_seism_P_and_dP', None)
    dic.pop('reduced_chisq_evol', None)
    dic.pop('reduced_chisq_total', None)
    dic.pop('AIC', None)
    dic.pop('AIC_c', None) 

    list_output.append(dic)

  return list_output

#=================================================================================================================
def rename_chisq_key_from_dic(list_dic_modes_with_chisq, key_old, key_new):
  """
  It happens that you are doing multiple chisquare computations (of e.g. p- and g-modes) in different steps, and would
  like to keep track of former chisquare computations on the same model. Thus, to facilitate such bookkeepings, one may
  like to rename the chisquare key in a model dictinary and give it just a new name. As a result of this, the old key
  "key_old" will no lonber be accessible. This function receives a list of such dictionaries with chisquare information 
  in it, and renames the key_old into key_new, and stores the value from the former into the latter.
  @param list_dic_modes_with_chisq: list of mode info. If still they do not have chi-square related keys, 
         popping out any key from these dictionaries does NOT raise any exception, and is quite safe.
  @type list_dic_modes_with_chisq: list of dictionaries
  @param key_old: the name of the old key in the dictionary to pop out, and rename. Yet, the value of this key
         is preserved and is accessible under the new key name.
  @param key_new: the name of the new key to include in the dictionary. Its value is received from the former key.
  @type key_old, key_new: string
   @return: The same list of mode info with the old key substituted with the new one
  @rtype: list of dictionaries 
  """
  list_copy   = list_dic_modes_with_chisq[:]
  list_output = []
  for dic in list_copy:
    dic = dic.copy()
    val = dic.pop(key_old, None)
    dic[key_new] = val 
    list_output.append(dic)

  return list_output

#=================================================================================================================
def copy_chisq_key_to_dic(list_dic_modes_with_chisq, key_old, key_new):
  """
  It happens that you are doing multiple chisquare computations (of e.g. p- and g-modes) in different steps, and would
  like to keep track of former chisquare computations on the same model. Thus, to facilitate such bookkeepings, one may
  like to copy (append) the chisquare key in a model dictinary and give it just a new name. As a result of this, the old key
  "key_old" is still accessible. This function receives a list of such dictionaries with chisquare information 
  in it, and renames the key_old into key_new, and stores the value from the former into the latter.
  @param list_dic_modes_with_chisq: list of mode info. If still they do not have chi-square related keys, 
         popping out any key from these dictionaries does NOT raise any exception, and is quite safe.
  @type list_dic_modes_with_chisq: list of dictionaries
  @param key_old: the name of the old key in the dictionary to pop out, and rename. Yet, the value of this key
         is preserved and is accessible under the new key name.
  @param key_new: the name of the new key to include in the dictionary. Its value is received from the former key.
  @type key_old, key_new: string
   @return: The same list of mode info with the old key substituted with the new one
  @rtype: list of dictionaries 
  """
  list_copy = list_dic_modes_with_chisq[:]
  list_output = []
  for dic in list_copy:
    dic          = dic.copy()
    dic.update({key_new: dic[key_old]})
    list_output.append(dic)

  return list_output

#=================================================================================================================
def normalize_list_dic_chisq(list_dic_chisq, chisq_by):
  """
  Normalize the chi-square item in each dictionary by the minimum chi-square value among all.
  This is usefu when one likes to compare chi-square values regardless of their absolute value, like the case when
  they are (astronomically) large.
  @param list_dic_chisq: list of mode info. If still they do not have chi-square related keys, 
         popping out any key from these dictionaries does NOT raise any exception, and is quite safe.
  @type list_dic_chisq: list of dictionaries
  @param chisq_by: the key assigning the chi-square values, and the one which is used to retrieve the chi-square values. 
         The normalized values will be stored under the same key name.
  @type chisq_by: string
  @return: updated list of mode info with the chi-square values normalized to the minimum chi-square value in the list.
         Thus, the best model now has a normalized chi-square of 1.0.
  @rtype: list of dictionaries
  """
  n_dic = len(list_dic_chisq)
  if n_dic == 0:
    logging.error('chisq: normalize_list_dic_chisq: Input list is empty')
    raise SystemExit, 'Error: chisq: normalize_list_dic_chisq: Input list is empty'

  if not list_dic_chisq[0].has_key(chisq_by):
    logging.error('chisq: normalize_list_dic_chisq: Input dictionaries do not have "{0}" key'.format(chisq_by))
    raise SystemExit, 'Error: chisq: normalize_list_dic_chisq: Input dictionaries do not have "{0}" key'.format(chisq_by)

  list_dic_chisq = list_dic_chisq[:]
  list_chisq     = np.array( [ dic[chisq_by] for dic in list_dic_chisq ] )
  min_chisq      = np.min(list_chisq)
  norm_chisq     = list_chisq / min_chisq

  list_output = []
  for i_dic, dic in enumerate(list_dic_chisq):
    dic = dic.copy()
    dic[chisq_by] = norm_chisq[i_dic]
    list_output.append(dic)

  return list_output

#=================================================================================================================
def normalize_3_chisq_and_sort_again(inputs, chisq_by, extra_keys=[]):
  """
  To normalize the chi-square tables based on extra_keys provided, and then average and sort these several chi-square
  records into a single field called chisq_by, one can call this routine. Note that chisq_by and all keys provided by 
  extra_keys should already exist in the inputs. The input can be either a list of dictionaries, or a single numpy 
  recarray containing the whole information/table.
  The number of keys specified in extra_keys need not necessarily be 3; hence the routine name may be better adapted 
  later for the sake of generality.
  @param inputs: The chi-square table; the allowd formats are 
         - numpy record array with chisq_by and elements of extra_keys in the header; this format is preferred
         - list of dictionaries where each dictionary contains the whole information. In such a case, the chisq_by and
           elements of extra_keys should be available as keys of individual dictionaries in the list.
  @type inputs: numpy.recarray
  @param chisq_by: The chi-square field used to put the averages into, and finally sort with respect to that.
  @type chisq_by: string
  @param extra_keys: The list of extra chi-square keys that are used for averaging. They may (or not) be already
         averaged, so one element in each of these columns may be (or not) set to 1.00
         In any case, the field associated with the minimum chi-square value specified by chisq_by can hardly be 
         1.0, unless there exists a model in the table, where the chi-square values of all other fields be aleady 1.0, 
         so that the average is also 1.0!
         Note that the presence of extra_keys in the input table will be checked first hand.
  @type extra_keys: list of strings
  @return: the same table with the fields passed in extra_keys normalized to 1.0 (using the minimum chi-square) score 
         in that specific field, and the chi-square score of each model averaged and stored in the field specified by
         chisq_by
  @rtype: numpy record array
  """
  if not extra_keys: 
    logging.error('chisq: normalize_3_chisq_and_sort_again: extra_keys cannot be an empty list')
    raise SystemExit, 'Error: chisq: normalize_3_chisq_and_sort_again: extra_keys cannot be an empty list'

  input_is_list = False
  input_is_rec  = False 
  if isinstance(inputs, list):
    print ' - chisq: normalize_3_chisq_and_sort_again: Input is a list of dictionaries'
    input_is_list = True
    keys = inputs[0].keys()
  elif isinstance(inputs, np.core.records.recarray) or \
       isinstance(inputs, np.ndarray):
    print ' - chisq: normalize_3_chisq_and_sort_again: Input is a numpy record array'
    input_is_rec = True
    keys = inputs.dtype.names
  else:
    logging.error('chisq: normalize_3_chisq_and_sort_again: Invalid input!')
    raise SystemExit, 'Error: chisq: normalize_3_chisq_and_sort_again: Invalid input!'

  if chisq_by not in keys:
    logging.error('chisq: normalize_3_chisq_and_sort_again: {0} not among the input dictionary keys!'.format(chisq_by))
    raise SystemExit, 'Error: chisq: normalize_3_chisq_and_sort_again: {0} not among the input dictionary keys!'.format(chisq_by)

  for key in extra_keys:
    if input_is_list:
      if not key in inputs[0].keys():
        logging.error('chisq: normalize_3_chisq_and_sort_again: {0} not in the input fields'.format(key))
        raise SystemExit, 'Error: chisq: normalize_3_chisq_and_sort_again: {0} not in the input fields'.format(key)
    if input_is_rec:
      if not key in inputs.dtype.names:
        logging.error('chisq: normalize_3_chisq_and_sort_again: {0} not in the input fields'.format(key))
        raise SystemExit, 'Error: chisq: normalize_3_chisq_and_sort_again: {0} not in the input fields'.format(key)

  n_dic = len(inputs)
  n_key = len(extra_keys)

  list_min_chisq_vals = []

  for key in extra_keys:
    if input_is_list:
      arr_all_chisq_key = [dic[key] for dic in inputs]
    elif input_is_rec:
      arr_all_chisq_key = inputs[key]
    min_chisq_val     = min(arr_all_chisq_key)
    list_min_chisq_vals.append(min_chisq_val)

  list_output  = []
  list_normalized_chisq = np.zeros(n_dic)
  for i_dic, dic in enumerate(inputs):
    cumul      = 0.0
    for i_key, key in enumerate(extra_keys):  
      dic[key] = dic[key] / list_min_chisq_vals[i_key]
      cumul    += dic[key]
    dic[chisq_by] = cumul / n_key
    list_normalized_chisq[i_dic] = dic[chisq_by]
    list_output.append(dic)

  ind_sort = np.argsort(list_normalized_chisq)
  list_output = [list_output[i] for i in ind_sort]

  # convert the output to numpy recarray
  if input_is_rec:
    empty       = np.empty(n_dic, dtype=inputs.dtype)
    for i_row, row in enumerate(list_output): empty[i_row] = row
    list_output = empty

  logging.info('chisq: normalize_3_chisq_and_sort_again: done')

  return list_output

#=================================================================================================================
def multiply_chisq_and_sort_again(inputs, chisq_mult=[], save_chisq_as='chisq_mult'):
  """

  """
  n_rows = len(inputs)
  n_keys = len(chisq_mult)
  if n_rows == 0:
    logging.error('chisq: multiply_chisq_and_sort_again: Input recarray is empty')
    raise SystemExit, 'Error: chisq: multiply_chisq_and_sort_again: Input recarray is empty'

  if not isinstance(inputs, np.ndarray) or not isinstance(inputs, np.core.records.recarray):
    logging.error('chisq: multiply_chisq_and_sort_again: Only np.ndarray or np.recarray are alloed')
    raise SystemExit, 'Error: chisq: multiply_chisq_and_sort_again: Only np.ndarray or np.recarray are alloed'

  if n_keys == 0:
    logging.error('chisq: multiply_chisq_and_sort_again: Input chisq_mult list is empty')
    raise SystemExit, 'Error: chisq: multiply_chisq_and_sort_again: Input chisq_mult list is empty'

  names_orig = inputs.dtype.names
  dtype_orig = inputs.dtype 
  dtype_new  = [dt for dt in dtype_orig.descr] + [(save_chisq_as, 'f8')]
  output     = np.empty(n_rows, dtype=dtype_new)

  for key in chisq_mult:
    if key not in names_orig:
      logging.error('chisq: multiply_chisq_and_sort_again: {0} not available'.format(key))
      raise SystemExit, 'Error: chisq: multiply_chisq_and_sort_again: {0} not available'.format(key)

  for i_row, row in enumerate(inputs):
    mult     = 1.0
    for key in chisq_mult:
      mult   *= row[key]
    for field in names_orig: output[i_row][field] = row[field]
    output[save_chisq_as][i_row]     = mult

  chisq_mult_vals = output[save_chisq_as]
  ind_sort        = np.argsort(chisq_mult_vals)
  output          = output[ind_sort]

  return output

#=================================================================================================================


# README #

Welcome using mesa_gyre Python repository. This is intended for the use of the author, and comes without any warranty for the use of third-party. You are welcome to use and distribute it.

### What is this repository for? ###

* mesa_gyre repository provides a large user front-end functionalities to facilitate accurate and efficient interaction with the evolutionary and asteroseismic grid of models. The major requirement is to have h5py installed, and also the [GYRE Python Module](https://bitbucket.org/rhdtownsend/gyre/wiki/Home) be already installed. In addition, mesa_gyre makes substantial use of matplotlib and several of its wrappers, e.g. pylab.
* Version 1.0

### How do I get set up? ###

* Summary of set up: The set up is just trivial. Follow these command line steps to set up this repository:

- Let's assume this repository is planned to reside in /home/me/<path>, then in the command line execute

$> cd /home/me/<path>, 

$> git clone https://ehsan_moravveji@bitbucket.org/ehsan_moravveji/python.git

- In your .bash_profile, edit and add the following line

  export PYTHONPATH=$PYTHONPATH:/home/me/<path>

$> source ~/.bash_profile

$> python

$> import mesa_gyre

If an exception is raised, revise the above steps, and if that does not solve it, contact the author.

* Dependencies: [h5py](http://www.h5py.org), [GYRE Python Module](https://bitbucket.org/rhdtownsend/gyre/wiki/Home)  should be already installed.


#! /usr/bin/python

"""
This module defines classes and methods to construct members, their attributes, and their
relations.
Start date: Saturday night, 7 March 2015
Author: Ehsan Moravveji
"""

import sys, os, glob
import logging

#=========================================================================================
class member:
  """
  Define objects and methods relevant to build up the family members.
  """
  num_members = 0
  
  #=======================================================================================
  def __init__(self, 
    name='',  
    family='',
    dob_shamsi_d=0,
    dob_shamsi_m=0,
    dob_shamsi_y=0,
    marital='',
    spouse='',
    father='',
    mother='',
    children=[''],
    dir_pics='',
    phone=0,
    email=''):
    """ 
    Constructor.
    @param name: first name; default=''
    @type name: string
    @param family: family name; default=''
    @type family: string
    @param dob_shamsi_d: date of birth in Shamsi calendar; default=0
    @type dob_shamsi_d: integer 
    @param dob_shamsi_m: month of birth in Shamsi calendar; default=0
    @type dob_shamsi_m: integer 
    @param dob_shamsi_y: year of birth in Shamsi calendar; default=0
    @type dob_shamsi_y: integer 
    @param marital: marital status; default=''. Available options are:
           - single
           - married
           - devorced
    @param spouse: full name of the spouse; default=''. The person may or may not exist
           in this database.
    @type spouse: string
    @type marital: string
    @param father: father's name; default=''
    @type father: string
    @param mother: mother's name; default=''
    @type mother: string
    @param children: list of full name of the children, default=['']. There should be one
           string per each children. The children may or maynot exist in the database
    @type children: list of strings
    @param dir_pics: full path to a directory where pictures of the member are archived.
           default=''
    @type dir_pics: string
    @param phone: phone number; default=0
    @type phone: integer
    @param email: email address; default=0
    @type email: string
    """
    self.name = name
    self.family = family
    self.dob_shamsi_d = dob_shamsi_d
    self.dob_shamsi_m = dob_shamsi_m
    self.dob_shamsi_y = dob_shamsi_y
    self.marital = marital
    self.spouse = spouse
    self.father = father
    self.mother = mother
    self.children = children
    self.dir_pics = dir_pics
    self.phone = phone
    self.email = email

    member.num_members += 1
    
  #=======================================================================================
  def get_num_members(self):
    """
    Retrieve the updated number of family members
    """
    print ' - def_members: family: get_num_members: Total number of members = {0}'.format(
           member.num_members)
    
  #=======================================================================================
  def display_member(self):
    """
    Display basic information about any member on the terminal
    """
#     all_attributes    = dir(self)
#     native_attributes = [attr for attr in all_attributes if attr.startswith('__')]
#     given_attributes  = [attr for attr in all_attributes if not attr.startswith('__')]
    
    print ' - def_members: family: display_member: '    
    for key, value in self.__dict__.items():
      print '   {0}: {1}'.format(key.title(), value)
    
  #=======================================================================================
  def get_member_dic(self):
    """
    Retrieve the stored member information as a dictionary
    @param self: class object
    @type self: class
    @return: dictionary with the keys defined in def.__init__()
    @rtype: dict
    """
    print type(self)
    return self.__dict__
    
    
#=========================================================================================
class family(member):
  """
  Define objects and methods to build up a family.
  """
  num_families = 0
  
  #=======================================================================================
  def __init__(self, 
    father='',
    mother='',
    children=[]):
    """
    Constructor.
    @param father: full name of the father of the family, separated by a space; e.g.
           father='First Last'. The first name and family name of the father should already
           be available as the attributes of one of the members in the list. default=''
    @type father: string
    @param mother: full name of the mother of the family, similar to father; default=''
    @type mother: string
    @param children: list of children that belong to this family; default=[]
    @type children: list of class
    """
    self.father = father
    self.mother = mother
    self.children = children
    
    family.num_families += 1

  #=======================================================================================
  def display_family(self):
    """
    Display family information
    """
    print ' - def_members: family: display_family:'
    dic_family = self.__dict__
    for key, value in dic_family.items():
      if isinstance(value, list):
        for i, sth in enumerate(value):
          print '   {0}: {1} {2}'.format(key.title(), sth.name, sth.family)
      elif isinstance(value, member):
        print '   {0}: {1}'.format(key, self.name)
      else:
        print '   {0}: {1}'.format(key.title(), value), type(value)
    
#=========================================================================================

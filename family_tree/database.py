#! /usr/bin/python

"""
This module stores the data for all family members. Each member inherits pre-defined 
attributes as in def.__init__() constructor, and is initiated with default null values,
unless being changed/modified. 
For the full list of available attributes, see the documentation of def.__init__()
"""

import sys, os, glob
import logging
from def_members import member, family

#=========================================================================================

family_mohsen_moravveji = family()
mohsen = member(name='Mohsen', family='Moravveji')
ashraf = member(name='Ashraf', family='Nasr')
ehsan  = member(name='Ehsan', family='Moravveji')
farzad = member(name='Farzad', family='Moravveji')
farhad = member(name='Farhad', family='Moravveji')
farbod = member(name='Farbod', family='Moravveji')
mohsen.children = [ehsan, farzad, farhad, farbod]
ashraf.children = [ehsan, farzad, farhad, farbod]
family_mohsen_moravveji.members = [mohsen, ashraf, ehsan, farzad, farhad, farbod]

# mohsen.display_member()
family_mohsen_moravveji.display_family()

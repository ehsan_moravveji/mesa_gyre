
import sys, os, glob
import logging
import copy
import numpy as np

from brite.lib import do_scargle, do_pdm

###################################################################################################
__author__  = 'Ehsan Moravveji'
__version__ = '1'
__date__    = '27.07.2016'
###################################################################################################
###################################################################################################
class spectra:
  """
  Class: spectra

  Each spectral line profile is treated here as a class object, and is assigned several attributes
  and methods to better exploit the data, and facilitate the analysis in a much easier, and structured
  manner. This is the basic class, and constructs the smallest piece of objects. based on this class, 
  the "Line" class inherits the "spectra", when constructing list of spectra to read.
  """

  # spectra constructor
  def __init__(self, 
               filename):
    self.filename = filename
    
    self.name = ''
    self.wavelength = 0

    rec_spectra = read_one_spectra(filename)

    self.disp_v = rec_spectra['v']  # dispersion array across the line profile in km/sec
    self.flux   = rec_spectra['f']  # flux array across the line profile

    self.hjd    = 0
    self.snr    = 0
    self.weight = 0
    self.phase  = 0
    self.phase_freq = 0



  # class methods
  def set_name(self, name):
    self.name = name

  def set_wavelength(self, wavelength):
    self.wavelength = wavelength


###################################################################################################
class Line:
  """
  Class: Line
  
  Each line consists of a collection of spectral files which are assembled at a specific folder, 
  and are organized through a specific time.suffix file.
  Several attributes of the "Line" class are built upon the "spectrum" class objects.
  """

  # class variables

  # class constructor
  def __init__(self, 
               repos, 
               time_file, 
               search_string):
    """
    Initialize the Line object, by assigning the repository path for the spectra, and the relative
    filename of the time.suffix file.
    The optional attributes are
      - name: the line name, e.g. 'Halpha 6563'
      - wavelength: the wavelength (Angstrom) of the line core
      - ...
    """
    # data paths: mandatory arguments
    self.repos = repos
    self.time_file = time_file
    self.search_string = search_string

    self.initialized = True

    self.rec_times  = read_times_file(repos=repos, time_file=time_file)
    self.spec_files = gen_spectra_full_path_from_times_file(repos=repos, rec_times=self.rec_times) # sort_spectra(repos, search_string)
    self.list_spec  = [spectra(f) for f in self.spec_files]
    self.n_spec     = len(self.spec_files)

    # line properties
    self.name = ''
    self.wavelength = 0

    # observation properties
    self.hjd = self.rec_times['hjd']
    self.weight = self.rec_times['weight'] / np.sum(self.rec_times['weight'])
    
    self.snr = self.rec_times['snr']
    self.snr_filtered = False  # if filter_by_snr() already called
    self.snr_OK = 0            # indices of OK elements

    # pass the whole information from the time.suffix file into individual spectra
    update_spectra_with_line_info(self)

    self.timespan = self.hjd[-1] - self.hjd[0]

    # weighted average spectrum and the one-sigma standard deviation around the mean spectrum
    eval_mean_spectrum(self)
    eval_std_spectrum(self)

    # line moment information
    self.has_v0     = False
    self.v0_file    = ''
    self.v0         = 0
    self.v0_err     = 0
    self.has_v0_dft = False
    self.v0_dft_freq= 0
    self.v0_dft_amp = 0

    self.has_v1     = False
    self.v1_file    = ''
    self.v1         = 0
    self.v1_err     = 0
    self.has_v1_dft = False
    self.v1_dft_freq= 0
    self.v1_dft_amp = 0

    # store the phase information
    self.n_phase = 0
    self.n_phase_bins = 0
    self.phase_freq = 0
    self.phases  = 0
    self.list_phased = [] # with n_phase lists of "spectrum" objects, sorted by their phase
    self.list_phase_binned = []  # with n_phase_bins "spectrum" objects, already phase averaged
    self.list_phase_binned_residuals = [] # with n_phase_bins "spectrum" objects, mean subtracted
    self.matrix_phase_binned_residuals = 0 # shape: (n_phase_bins, n_dispersion), made for plotting

    # Pixel by Pixel 2D analysis
    self.pxbpx_freq   = 0
    self.pxbpx_disp_v = 0
    self.pxbpx        = 0
    self.has_pxbpx    = False
    self.pxbpx_method = ''

    return None
    
    ###########


  ###############
  # class methods
  ###############
  def set_name(self, name):
    self.name = name 
    if self.n_spec > 0:
      for sp in self.list_spec:
        sp.set_name(name)

  ###############
  def set_wavelength(self, wavelength):
    self.wavelength = wavelength
    if self.n_spec > 0:
      for sp in self.list_spec:
        sp.set_wavelength(wavelength)

  ###############
  def set_spec_files(self, files):
    self.spec_files = files

  ###############
  def set_hjd(self, hjd):
    self.hjd = hjd

  ###############
  def set_moment(self, filename, which):
    if self.snr_filtered:
      OK = self.snr_OK
    else:
      OK = np.arange(self.n_spec)

    if which == 0:
      v0           = read_moment(filename)
      self.has_v0  = True
      self.v0_file = filename
      self.v0      = v0['moment'][OK]
      self.v0_err  = v0['moment_err'][OK]
    elif which == 1:
      v1           = read_moment(filename)
      self.has_v1  = True
      self.v1_file = filename
      self.v1      = v1['moment'][OK]
      self.v1_err  = v1['moment_err'][OK]
    else:
      logging.error('lib: Line: set_moment: only which=0 (EW) or which=1 (RV) are supported now.')
      raise SystemExit, 'lib: Line: set_moment: only which=0 (EW) or which=1 (RV) are supported now.'

  ###############
  def set_moment_dft(self, which, f0=None, fn=None, df=None, norm='amplitude', weight=None):
    if which == 0:
      if not self.has_v0:
        logging.error('lib: Line: set_moment_dft: v0 moment not available yet. Call set_moment() first.')
        raise SystemExit, 'lib: Line: set_moment_dft: v0 moment not available yet. Call set_moment() first.'
      f, a = do_scargle(self.hjd, self.v0, f0=f0, fn=fn, df=df, norm='amplitude', weights=self.weight)
      self.has_v0_dft  = True
      self.v0_dft_freq = f 
      self.v0_dft_amp  = a

    elif which == 1:
      if not self.has_v1:
        logging.error('lib: Line: set_moment_dft: v1 moment not available yet. Call set_moment() first.')
        raise SystemExit, 'lib: Line: set_moment_dft: v1 moment not available yet. Call set_moment() first.'
      f, a = do_scargle(self.hjd, self.v1, f0=f0, fn=fn, df=df, norm=norm, weights=self.weight)
      self.has_v1_dft  = True
      self.v1_dft_freq = f 
      self.v1_dft_amp  = a

    else:
      logging.error('lib: Line: set_moment_dft: only 0th and 1st moments are supported: set which=0 or 1.')
      raise SystemExit, 'lib: Line: set_moment_dft: only 0th and 1st moments are supported: set which=0 or 1.'

  ###############
  def get_hjd(self):
    return self.hjd

  ###############
  def get_snr(self):
    return self.snr

  ###############
  def get_min_snr(self):
    return np.min(self.snr)

  ###############
  def get_max_snr(self):
    return np.max(self.snr)

  ###############
  def get_mean_snr(self):
    return np.mean(self.snr)

  ###############
  def get_median_snr(self):
    snr = np.sort(self.snr)
    return np.median(snr)

  ###############
  def get_weight(self):
    return self.weight 

  ###############
  def get_moment_dft(self, which):
    """
    Return (freq, amp) tuple if DFT for the specified moment is available
    """
    if which == 0:
      if not self.has_v0:
        logging.error('lib: Line: get_moment_dft: v0 moment not available yet. Call set_moment() first.')
        raise SystemExit, 'lib: Line: get_moment_dft: v0 moment not available yet. Call set_moment() first.'
      return self.v0_dft_freq, self.v0_dft_amp

    elif which == 1:
      if not self.has_v1:
        logging.error('lib: Line: get_moment_dft: v1 moment not available yet. Call set_moment() first.')
        raise SystemExit, 'lib: Line: get_moment_dft: v1 moment not available yet. Call set_moment() first.'
      return self.v1_dft_freq, self.v1_dft_amp

    else:
      logging.error('lib: Line: get_moment_dft: only 0th and 1st moments are supported: set which=0 or 1.')
      raise SystemExit, 'lib: Line: get_moment_dft: only 0th and 1st moments are supported: set which=0 or 1.'

  ###############
  ###############

  ###################################################################################################

  ###########################################
  ### L I N E   C L A S S   M E T H O D S ###
  ###########################################
  def filter_by_hjd(self, lo=None, hi=None):
    """
    Exclude all spectra that lie outside the lower and upper HJD limit(s). If you leave lo (or hi)
    argument as None, then all spectra after lo (before hi) will be retained. The relevant attributes
    are adapted accordingly. The lower and upper limites are both inclusive; e.g. if lo=12345.0, and a
    datapoint with exactly the same HJD is available, we keep that point.
    """
    if lo is None and hi is None:
      logging.error('lib: filter_by_hjd: lo and hi thresholds cannot be both None. Set one or both.')
      raise SystemExit, 'lib: filter_by_hjd: lo and hi thresholds cannot be both None. Set one or both.'

    hjd = self.hjd
    if lo is None:
      ind = np.where(hjd <= hi)[0]
    elif hi is None:
      ind = np.where(hjd >= lo)[0]
    else:
      ind = np.where((hjd >= lo) & (hjd <= hi))[0]

    n   = len (ind)
    if n == 0:
      logging.warning('lib: filter_by_hjd: No effective filtering applied. Check bounds again.')
      print 'lib: filter_by_hjd: No effective filtering applied. Check bounds again.'
      return

    hjd = hjd[ind]
    OK  = [i for i in ind]

    self.rec_times  = self.rec_times[OK]
    self.spec_files = [self.spec_files[i] for i in OK]
    self.list_spec  = [self.list_spec[i] for i in OK]
    self.n_spec     = len(self.spec_files)

    # observation properties
    self.hjd = hjd
    self.weight = self.weight[OK]
    self.snr = snr[OK]

    self.timespan = hjd[-1] - hjd[0]


















    

  ###################################################################################################
  def filter_by_snr(self, threshold):
    """
    Exclude all spectra with their SNR below this threshold. Once such datapoints are found, various
    attributes of the Line object will be updated. Thus, other attributes which might be affected by 
    the omission of datapoints, such as DFT, etc. must be called again separately. It is advisible to 
    call this filtering function before doing any other operation. However, filtering before or after
    reading the moment file(s) does not matter, since in either case, the filtering is consistently
    applied.
    """
    snr = self.get_snr()
    ind = np.where(snr <= threshold)[0]
    n   = len(ind)
    if n == 0:
      print ' - lib: filter_by_snr: All spectra have SNR > {0}'.format(threshold)
      return

    OK  = [i for i in range(self.n_spec) if i not in ind] 

    self.rec_times  = self.rec_times[OK]
    self.spec_files = [self.spec_files[i] for i in OK]
    self.list_spec  = [self.list_spec[i] for i in OK]
    self.n_spec     = len(self.spec_files)

    # observation properties
    self.hjd = self.hjd[OK]
    self.weight = self.weight[OK]
    self.snr = snr[OK]

    self.timespan = self.hjd[-1] - self.hjd[0]

    # weighted average spectrum and the one-sigma standard deviation around the mean spectrum
    eval_mean_spectrum(self)
    eval_std_spectrum(self)

    # line moment information
    if self.has_v0:
      self.v0      = self.v0[OK]
      self.v0_err  = self.v0_err[OK]
    if self.has_v0_dft:
      logging.warning('lib: filter_by_snr: Call set_moment_dft() again.') 
      print 'lib: filter_by_snr: Call set_moment_dft() again.'

    if self.has_v1:
      self.v1      = self.v1[OK]
      self.v1_err  = self.v1_err[OK]
    if self.has_v1_dft:
      logging.warning('lib: filter_by_snr: Call set_moment_dft() again.') 
      print 'lib: filter_by_snr: Call set_moment_dft() again.'

    # store the phase information
    if self.n_phase > 0:
      logging.warning('lib: filter_by_snr: Call all methods for phase binning again') 
      print 'lib: filter_by_snr: Call all methods for phase binning again'

    if self.has_pxbpx:
      logging.warning('lib: filter_by_snr: Call pixel_by_pixel() method again') 
      print 'lib: filter_by_snr: Call pixel_by_pixel() method again'

    self.snr_filtered = True
    self.snr_OK = OK
    print ' lib: filter_by_snr: "{0}" spectra with SNR<{1} are rejected. Attributes adapted ...'.format(n, threshold)

  ###################################################################################################
  def pixel_by_pixel(self, method='scargle', f0=None, fn=None, df=None, norm='amplitude', weights=None,
                     Nbin=20, Ncover=2):
    """
    Compute the 2D Scargle periodogram along the line profile, treating each pixel independently from
    another pixel. Each dispersion bin is treated as a single time series (along all epochs).
    @param method: Either 'scargle' for the classical Scargle periodogram, or the 'pdm' for Phase Dispersion
           Minimization method.
    @type method: string
    """
    if method not in ['scargle', 'pdm']:
      logging.error('lib: pixel_by_pixel: Wrong method selected. Choose either "scargle", or "pdm". ')
      raise SystemExit, 'lib: pixel_by_pixel: Wrong method selected. Choose either "scargle", or "pdm". '

    n_spec = len(self.list_spec)
    first  = self.list_spec[0]
    disp_v = first.disp_v
    n_disp = len(disp_v)
    profs  = np.array( [ self.list_spec[i].flux for i in range(n_spec) ] ).reshape((n_spec, n_disp))

    data   = []
    for i in range(n_disp):
      hjd  = self.hjd
      flux = profs[:, i]
      flux -= np.mean(flux)
      if method == 'scargle':
        f, a = do_scargle(hjd, flux, f0=f0, fn=fn, df=df, norm='amplitude', weights=self.weight)
      else:
        f, a = do_pdm(hjd, flux, f0=f0, fn=fn, df=df, Nbin=Nbin, Ncover=Ncover, D=0, 
                 forbit=None, asini=None, e=None, omega=None, nmax=10)
      data.append(a)


    nf     = len(f)
    dft_2D = np.empty((nf, n_disp))
    for i in range(n_disp):
      dft_2D[:, i] = data[i][:]

    self.pxbpx_freq   = f 
    self.pxbpx_disp_v = disp_v
    self.pxbpx        = dft_2D
    self.has_pxbpx    = True
    self.pxbpx_method = method

  ###################################################################################################
  def subtract_mean_from_binned_spectrum(self):
    """
    One can optionally subtract the mean spectrum from the phase-binned spectra (after having applied
    the sort_spec_by_phase() and average_spectrum_per_phase_bin() methods). This will produce the 
    residual matrix of phased line profiles across the full dispersion range.
    """
    if len(self.list_phase_binned) == 0:
      logging.error('lib: subtract_mean_from_binned_spectrum: call average_spectrum_per_phase_bin() first.')
      print 'lib: subtract_mean_from_binned_spectrum: call average_spectrum_per_phase_bin() first.'
      return False

    disp_v      = self.mean_spec.disp_v
    mean_flux   = self.mean_spec.flux 
    list_binned = self.list_phase_binned
    n_disp      = len(disp_v)
    n_phase_bins= self.n_phase_bins
    residuals   = []
    matrix      = np.zeros((n_phase_bins, n_disp))

    for i, sp in enumerate(list_binned):
      new       = copy.deepcopy(sp)
      new.flux  -= mean_flux
      residuals.append(new)
      matrix[i, :] = new.flux[:]

    self.list_phase_binned_residuals = residuals
    self.matrix_phase_binned_residuals = matrix

    return residuals

  ###################################################################################################
  def average_spectrum_per_phase_bin(self):
    """
    After the spectra are all phase-binned, then, they are stored as self.list_phased, which is basically
    a list of lists, e.g. [[...], [...], ..., [...]], where there are self.n_phase lists within the outer
    list, and all spectra within each sublist have roughly identical phases (which we intend to average out).
    In return, we substitute each sublist with one instance of the "spectrum" class, where the flux is 
    substituted by the weighted mean of flux inside each list [...].
    @param self: an instance of the Line class
    @type self: object
    @return: list of weighted average spectra
    """
    list_list = self.list_phased
    if len(list_list) == 0:
      logging.error('lib: average_spectrum_per_phase_bin: First apply the sort_spec_by_phase() method')
      print 'Error: lib: average_spectrum_per_phase_bin: First apply the sort_spec_by_phase() method'
      return None 

    # n_phi     = self.n_phase
    n_phase_bins = self.n_phase_bins
    phases    = self.phases
    freq      = self.phase_freq
    first     = list_list[0][0]
    n_disp    = len(first.disp_v)
    output    = []

    # make an average per bin
    for i in range(n_phase_bins):
      list_sp = list_list[i]
      n_sp    = len(list_sp)
      w       = np.array( [list_sp[k].weight for k in range(n_sp)] )
      w_use   = w / np.sum(w)
      vals    = np.array( [list_sp[k].flux for k in range(n_sp)] ).reshape((n_sp, n_disp))

      avr     = np.average(vals, axis=0, weights=w_use)

      # make a copy of the "spectrum" object, and modify the attributes accordingly
      sp      = copy.deepcopy(first)
      sp.filename = ''
      sp.flux = avr
      sp.hjd  = 0
      sp.snr  = 0  # proper evaluation of snr not implemented yet
      sp.weight = np.sum(w)
      sp.phase= phases[i]
      sp.phase_freq = freq

      output.append(sp)

    self.list_phase_binned = output

    return output

  ###################################################################################################
  def sort_spec_by_phase(self, freq, n_phi):
    """
    Sort the spectra according to their phases, w.r.t. to the frequency "freq", and between "n_phi" 
    phase bins.
    @param self: an instance of the Line class
    @type self: class object
    @param freq: the trial frequency to phase-fold the spectra
    @type freq: float
    @param n_phi: the number of phase bins to chop the data in. Odd numbers are recommended.
    @type n_phi: integer
    @return: We pack those spectra that fall inside the same phase bin together. The return object 
           is a list of lists. The number of lists within the return list is equal to n_phi, and each
           list contains several spectra with roughly equal phases between phi_{i}  and phi_{i+1}, 
           where phi is always inclusively between 0 and 1.
           Note: for n_phi bin edges, there will be n_bins-1 bins to sort the spectra into; thus, 
                 the size of the returned list is one less than n_phi
    @rtype: list of lists
    """
    self.n_phase = n_phi
    self.n_phase_bins = n_phi - 1
    self.phase_freq = freq
    bins    = np.linspace(0, 1, n_phi, endpoint=True)
    self.phases  = bins
    phases  = self.eval_phase(freq=freq)
    phased  = []
    
    for i, phi in enumerate(bins[:-1]): 
      phi_lo = phi 
      phi_hi = bins[i+1]
  
      ind   = np.where( (phases >= phi_lo) & (phases < phi_hi) )[0]
      if len(ind) == 0: 
        phased.append([])
      else:
        phased.append( [self.list_spec[k] for k in ind] )

    self.list_phased = phased

    return phased

  ###################################################################################################
  def eval_phase(self, freq):
    """
    The phase, phi_i of the spectra i, w.r.t. the frequency freq is the decimal (residual) part of the division
        phi_i = [ freq * (t_i - t_0) ]
    where t_i is the hjd of the observation of spectra i, and t_0 is the hjd of the first observation
    """
    t_i  = self.hjd
    t_0  = t_i[0]
    prod = freq * (t_i - t_0)

    phi  = prod % 1

    return phi

  ###################################################################################################

###################################################################################################



##########################################################
### G E N E R A L  --- P U R P O S E   R O U T I N E S ###
##########################################################

###################################################################################################
###################################################################################################
def eval_mean_spectrum(self):
  """
  This method computes the weighted average of the line profile by essentially carrying out an average
  over each dispersion bin. 
  @param self: an instance of the Line class
  @type self: object
  @return: the self.mean_spec is updated with an instance of the "spectrum" class, containing the mean
           spectrum of the line profile across the entire dispersion range.
  """
  first  = self.list_spec[0]
  disp_v = first.disp_v
  n_disp = len(disp_v)
  n_spec = self.n_spec
  weight = self.weight

  profs  = np.array( [ self.list_spec[i].flux for i in range(n_spec) ] ).reshape((n_spec, n_disp))
  avr    = np.average(profs, axis=0, weights=weight)

  mean_sp= copy.deepcopy(first)
  mean_sp.filename = ''
  mean_sp.name += ': mean spectrum'
  mean_sp.flux = avr 
  mean_sp.hjd = 0
  mean_sp.snr = 0  # not supported to calculate, yet.
  mean_sp.weight = 1.0
  mean_sp.phase = 0.0

  self.mean_spec = mean_sp

###################################################################################################
def eval_std_spectrum(self):
  """
  Compute the weighted standard deviation of the spectral line profile across the dispersion range.
  @param self: an instance of the Line class
  @type self: object
  @return: the self.std_spec is updated with an instance of the "spectrum" class, containing the standard
           deviation of the line profile across the entire dispersion range
  """
  avr    = self.mean_spec.flux
  weight = self.weight 

  # first  = self.list_spec[0]
  disp_v = self.mean_spec.disp_v
  n_disp = len(disp_v)
  n_spec = self.n_spec

  profs  = np.array( [ self.list_spec[i].flux for i in range(n_spec) ] ).reshape((n_spec, n_disp))
  var    = np.average((profs - avr)**2, axis=0, weights=weight)
  std    = np.sqrt(var)

  std_sp = copy.deepcopy(self.mean_spec)
  std_sp.filename = ''
  std_sp.name += ': std. dev. spectrum'
  std_sp.flux = std 
  std_sp.hjd  = 0
  std_sp.snr  = 0  # not supported to calculate, yet.
  std_sp.weight = 1.0
  std_sp.phase = 0.0

  self.std_spec = std_sp

  # return std


###################################################################################################
def update_spectra_with_line_info(self):
  """
  Update each of the individual spectra in the line archive with the critical
  information that is contained in the times.suffix file. This is a very important
  linking between different components of this archive.
  @param self: this is the Line class instance object, which contains a list of subobjects for each
         spectra
  @type self: class obj
  """

  line_name = self.name 
  if line_name != '':
    for sp in self.list_spec: sp.name = line_name

  line_wave = self.wavelength
  if line_name != 0:
    for sp in self.list_spec: sp.wavelength = line_wave

  hjd       = self.hjd 
  weight    = self.weight
  snr       = self.snr
  for i, sp in enumerate(self.list_spec):
    sp.hjd  = hjd[i]
    sp.weight = weight[i]
    sp.snr  = snr[i]

###################################################################################################
def sort_spectra(repos, search_string):
  """
  Basically, glob for the files residing in the "repos" directory, using the "search_string", and 
  return a sorted file list.
  """
  if repos[-1] != '/': repos += '/'
  files   = sorted( glob.glob( repos + search_string ) )
  n_files = len(files)
  print ' lib: sort_spectra: Found {0} files in "{1}".'.format(n_files, repos)

  return files

###################################################################################################
def gen_spectra_full_path_from_times_file(repos, rec_times):
  """
  The way FAMIAS is designed, the output spectra are written with enumerated file names, and then 
  the filenames are linked to the observing times through the times.suffix file. Thus, one needs
  to link the filenames with their associated repository folder, and the observation time.
  This routine generates the full path for all spectra that reside in the "repos" directory, based
  on the first column of the rec_times array (which contains the times.suffix data). The full path
  to each spectra is essentially the concatenation of the "repos" path, and the relative filenames
  passed by the times.suffix file (available in "rec_times" as an input argument).

  @param repos: full path to the repository folder where the whole spectra are sitting
  @type repos: string
  @param rec_times: record array containing the entire information from the times.suffix file. 
         This array can be generated by calling e.g. lib.read_times_file(), which we refer to for 
         more information
  @return: list of full path to all spectra
  @rtype: list of strings
  """ 
  if repos[-1] != '/': repos += '/'
  filenames = rec_times['filename']
  # n_files   = len(filenames)

  return [repos + fn for fn in filenames]

###################################################################################################
def read_all_spectra(repos, rec_times):
  """
  This is a convenience function to read the entire spectra by generating the spectra filenames after
  calling gen_spectra_full_path_from_times_file(), and subsequently making an iterative call to 
  read_one_spectra() routine. Since the output of read_one_spectra() is a numpy record array, 
  we return a list of record arrays for the whole archive.

  @param repos: full path to the repository folder where the whole spectra are sitting
  @type repos: string
  @param rec_times: record array containing the entire information from the times.suffix file. 
         This array can be generated by calling e.g. lib.read_times_file(), which we refer to for 
         more information
  """
  filenames = gen_spectra_full_path_from_times_file(repos=repos, rec_times=rec_times)

  return [read_one_spectra(f) for f in filenames]

###################################################################################################
def read_one_spectra(filename):
  """
  FAMIAS exports spectra dataset by creating two sets of files:
  1. times.suffix file which contains four columns:
     a. filename: string; the relative path is assumed. the absolute path is created by adding "repos"
     b. hjd: float; the observation time
     c. weight: float; the weight assigned to each spectra. It is a number around 1
     d. snr: float; the SNR calculated per each spectra. Acutally, the weights are computed using SNR
  2. number.suffix files which contain all spectra. Their filenames are listed as the first column in
     the times.suffix file, so, that is a way to correlate and identify each file.
  This routine, reads the second type of FAMIAS output file, and returns a numpy record array containing
  the whole information in the number.suffix file. Such files are essentially two-column files, with 
  the first column being the dispersion (in km/sec), and the second column being the line depth
  @param filename: full path to the spectra ASCII file to be read.
  @type filename: string
  @return: numpy array containing the whole data, in two columns: 
        a. v: the velocity dispertion across the line profile
        b. f: the flux per each dispersion bin
        In case the file does not exist, this routine does not crash. It raises an error to be logged, 
        but returns "False", so that one can keep going with reading more files, while some cannot be 
        found!
  @rtype: numpy recarray
  """
  if not os.path.exists(filename):
    logging.error('lib: read_one_spectra: "{0}" does not exist!'.format(filename))
    print 'lib: read_one_spectra: "{0}" does not exist!'.format(filename)
    return False

  with open(filename, 'r') as r: lines = r.readlines()
  n_rows = len(lines)
  prof   = np.empty(n_rows, dtype=[('v', 'f8'), ('f', 'f8')])
  for i, line in enumerate(lines):
    row  = line.rstrip('\n\p').split()
    prof[i]['v'] = float(row[0])
    prof[i]['f'] = float(row[1])

  return prof

###################################################################################################
def read_times_file(repos, time_file):
  """
  FAMIAS exports spectra dataset by creating two sets of files:
  1. times.suffix file which contains four columns:
     a. filename: string; the relative path is assumed. the absolute path is created by adding "repos"
     b. hjd: float; the observation time
     c. weight: float; the weight assigned to each spectra. It is a number around 1
     d. snr: float; the SNR calculated per each spectra. Acutally, the weights are computed using SNR
  2. number.suffix files which contain all spectra. Their filenames are listed as the first column in
     the times.suffix file, so, that is a way to correlate and identify each file.
  This routine, reads the first type of FAMIAS output file, and returns a numpy record array containing
  the whole information in times.suffix file.
  @param repos: full path to the directory where the time_file and the whole spectra are stored.
         Note that the combined full path "repos + time_file" must give an already present file.
  @type repos: string
  @param time_file: relative filename of the times.suffix file. It must be stored in the "repos" folder.
         E.g. time_file = 'obs-times.txt'
  @type time_file: string
  @return: array containing the whole information. It has four columns called "filename", "hjd", "weight",
         and "snr".
  @rtype: numpy recarray
  """
  if repos[-1] != '/': repos += '/'
  full_path = repos + time_file
  if not os.path.exists(full_path):
    logging.error('lib: read_times_file: {0} does  not exist!'.format(full_path))
    raise SystemExit, 'lib: read_times_file: {0} does  not exist!'.format(full_path)

  with open(full_path, 'r') as r: lines = r.readlines()
  n_rows  = len(lines)
  dtype   = [('filename', 'S8'), ('hjd', 'f8'), ('weight', 'f8'), ('snr', 'f8')]
  times   = np.empty(n_rows, dtype=dtype)

  # Now, read the data column by column
  for i, line in enumerate(lines):
    line  = line.rstrip('\n\p').split()
    times[i]['filename'] = line[0]
    times[i]['hjd']      = float(line[1])
    times[i]['weight']   = float(line[2])
    times[i]['snr']      = float(line[3])

  return times

###################################################################################################
def read_moment(filename):
  """
  FAMIAS stores each moment dataset as a separate ASCII file, with three coloumns:
  - time: normally in HJD, or BJD
  - moment: the n-th moment of the line profile
  - moment_err: the uncertainty of the line moment
  This routine reads such an ascii file, and retruns a numpy recarray with the above three fields
  """
  if not os.path.exists(filename):
    logging.error('lib: read_moment: the file "{0}" does not exist.'.format(filename))
    raise SystemExit, 'lib: read_moment: the file "{0}" does not exist.'.format(filename)

  with open(filename, 'r') as r: lines = r.readlines()
  n_lines = len(lines)
  dtype   = [('hjd', 'f8'), ('moment', 'f8'), ('moment_err', 'f8')]

  rec     = []
  for i, line in enumerate(lines):
    row   = line.rstrip('\n\p').split()
    rec.append(row)

  rec     = np.core.records.fromarrays(np.array(rec).transpose(), dtype=dtype)

  return rec 

###################################################################################################


#########################################
### A U X I L A R Y   R O U T I N E S ###
#########################################

###################################################################################################
###################################################################################################
###################################################################################################
def gen_frequency_range(lo, hi, step):
  """
  Return an array of trial frequency, including both lower and upper ranges, with the given step.
  @return: numpy ndarray with the list of desired frequencies.
  """
  if lo >= hi:
    logging.error('lib: gen_frequency_range: the lower limit must be smaller than the higher limit.')
    raise SystemExit, 'lib: gen_frequency_range: the lower limit must be smaller than the higher limit.'

  if step <= 0:
    logging.error('lib: gen_frequency_range: the step must be a positive float')
    raise SystemExit, 'lib: gen_frequency_range: the step must be a positive float'

  n = int( np.around((hi - lo) / step + 1) )

  return np.linspace(lo, hi, n, endpoint=True)

###################################################################################################

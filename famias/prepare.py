
import sys, os, glob
import numpy as np
import pylab as plt


source_file = 'Shariati-2.txt'
output_dir = 'inputs/'
if not os.path.exists(output_dir): os.makedirs(output_dir)
plot_dir     = 'plots/'
if not os.path.exists(plot_dir): os.makedirs(plot_dir)
make_plots = True
get_mean_spec = True

# read the whole file in one snapshot, everything is stored as a list in source_lines
with open(source_file, 'r') as file_object: source_lines = file_object.readlines()

# put everything in a new list with columns separated, exclude empty lines
raw_data = []
ind_time = []
obs_time = []
for i, line in enumerate(source_lines):
    line = line.rstrip('\r\t\n').split()
    num = len(line)
    if num == 0: continue
    if num == 1: raw_data.append(line[0])
    if num == 2: raw_data.append(line)

# loop over raw data, and get the index and value of the observation times in HJD
for i, line in enumerate(raw_data):
    if type(line) is type('string'):
        ind_time.append(i)
        obs_time.append(float(line))

num_time = len(ind_time)
print obs_time[0:4]
print 'There are {0} HJD, so expect {1} output files in {2}'.format(num_time, num_time, output_dir)

# This list only stores the names of the files, and will be later written in the master_file.txt
list_master = []

for i in range(num_time-1):
    time        = obs_time[i]
    ind_from = ind_time[i] + 1
    ind_to     =  ind_time[i+1]
    temp_list = []
    for j in np.arange(ind_from, ind_to):
        temp_list.append('{0}    {1} \n'.format(raw_data[j][0], raw_data[j][1]))

    # Now, just write down the content of temp_list as a new file
    output_file = output_dir + '{0:.8f}'.format(time) + '.txt'
    master_line = '{0}    {1:.10f}    {2}'.format(output_file, time, 1.0)
    list_master.append(master_line + '\n')
    with open(output_file, 'w') as file_object: file_object.writelines(temp_list)

with open('master_file.txt', 'w') as file_object: file_object.writelines(list_master)

print 'Voila !'
print 'All new files are now stored in {0}'.format(output_dir)

# get the mean spectrum
if get_mean_spec:
    input_files = sorted(glob.glob(output_dir + '*.txt'))
    n_files       = len(input_files)
    data          = np.loadtxt(input_files[0])
    n_row, n_col = data.shape
    mtrx_wave = np.zeros((n_row, n_files))
    mtrx_flux   = np.zeros((n_row, n_files))

    for i, a_file in enumerate(input_files):
        data = np.loadtxt(a_file, dtype={'names': ('wave', 'flux'), 'formats': ('f8', 'f8')})
        mtrx_wave[:, i] = data['wave']
        mtrx_flux[:,i]    = data['flux']

    mean_wave = np.zeros(n_row)
    mean_flux   = np.zeros(n_row)
    for i in range(n_row):
        mean_wave[i] = np.mean(mtrx_wave[i, :])
        mean_flux[i]   = np.mean(mtrx_flux[i, :])

    ind_min = np.argmin(mean_flux)
    lambda_0 = mean_wave[ind_min]
    print 'Rest Wavelength from Mean Spectra = {0} [A]'.format(lambda_0)

    fig = plt.figure(figsize=(6,4))
    ax  = fig.add_subplot(111)
    ax.plot(mean_wave,mean_flux, lw=1, color='red')
    ax.set_xlabel(r'Wavelength $\lambda \, [\AA]$')
    plt.tight_layout()
    plt.savefig(plot_dir + 'Mean-spectra.png')
    plt.close()

# Now, start reading every file and plot them as png files, and see if they are OK
if make_plots:
    input_files = sorted(glob.glob(output_dir + '*.txt'))

    for i, a_file in enumerate(input_files):
        data = np.loadtxt(a_file, dtype={'names': ('wave', 'flux'), 'formats': ('f8', 'f8')})
        png_name = plot_dir + 'T-' + str(obs_time[i]) + '.png' #list_master[i].replace('.txt', '.png')
        fig = plt.figure(figsize=(6,4), dpi=200)
        ax  = fig.add_subplot(111)
        ax.plot(mean_wave, mean_flux, lw=2, color='grey', zorder=1)
        ax.plot(data['wave'], data['flux'], lw=1, zorder=2)

        ax.set_xlim(int(data['wave'][0]), int(data['wave'][-1])+1)
        ax.set_xlabel(r'Wavelength $\lambda \, [\AA]$')

        plt.tight_layout()
        plt.savefig(png_name)
        plt.close()






import sys, os, glob
import logging
import copy
import numpy as np
import pylab as plt

###################################################################################################
__author__  = 'Ehsan Moravveji'
__version__ = '1'
__date__    = '27.07.2016'

###################################################################################################
###################################################################################################
###################################################################################################
###################################################################################################
###################################################################################################
def show_pixel_by_pixel(self, file_out=None):
  """
  The outcome of the pixel by pixel analysis is a 2D array of shape NxM, where N is the number of 
  trial frequencies (fixed row), and M is the number of dispersion bins (fixed column).
  """
  if file_out is None: return False
  if not self.has_pxbpx: 
    logging.error('plotter: show_pixel_by_pixel: Call lib.pixel_by_pixel() method first!')
    raise SystemExit, 'plotter: show_pixel_by_pixel: Call lib.pixel_by_pixel() method first!'
    
  freq   = self.pxbpx_freq 
  disp_v = self.pxbpx_disp_v
  pxbpx  = self.pxbpx
  nf, nv = pxbpx.shape
  if nf != len(freq):
    logging.error('lib: show_pixel_by_pixel: The size of frequency array does not agree.')
    raise SystemExit, 'lib: show_pixel_by_pixel: The size of frequency array does not agree.'

  if nv != len(disp_v):
    logging.error('lib: show_pixel_by_pixel: The size of dispersion array does not agree.')
    raise SystemExit, 'lib: show_pixel_by_pixel: The size of dispersion array does not agree.'

  ###########################
  # Do the plotting
  ###########################
  fig, ax = plt.subplots(1, figsize=(5, 5))
  plt.subplots_adjust(left=0.125, right=0.98, bottom=0.10, top=0.90)

  for xval in np.arange(-400, 400, 50):
    ax.axvline(xval, linestyle='dotted', color='b', zorder=1)

  C   = ax.imshow(pxbpx, cmap='gnuplot2_r', interpolation='nearest', aspect='auto',
                  extent=(disp_v[0], disp_v[-1], freq[0], freq[-1]), vmin=0, vmax=np.max(pxbpx), 
                  alpha=0.90, origin='lower', zorder=2)
  
  pos = ax.get_position()
  cax = fig.add_axes([pos.x0, pos.y1+0.01, pos.x1-pos.x0, 0.995-pos.y1-0.05])
  tks = np.linspace(0, np.max(pxbpx), 5, endpoint=True)
  B = fig.colorbar(C, cax=cax, ticks=tks, orientation='horizontal')
  cax.xaxis.set_ticks_position('top')
  B.ax.set_xticklabels( [ '{0:.2f}'.format(val*100.0) for val in tks ], fontsize=8)
  # B.ax.set_xticklabels(())

  ###########################
  # Cosmetics
  ###########################
  ax.set_xlabel(r'Dispersion range [km sec$^{-1}$]')
  ax.set_xlim(disp_v[0], disp_v[-1])
  ax.set_ylabel(r'Frequency [day$^{-1}$]')
  if self.pxbpx_method == 'scargle':
    cax.annotate(r'Amplitude [$\%$]', xy=(0.03, 0.45), va='center', fontsize=10)
  elif self.pxbpx_method == 'pdm':
    cax.annotate(r'$\theta_{\rm PDM}$ [$\%$]', xy=(0.03, 0.45), va='center', fontsize=10)
  else:
    logging.error('plotter: show_pixel_by_pixel: self.pxbpx_method="{0}" is not supported!'.format(self.pxbpx_method))
    raise SystemExit, 'plotter: show_pixel_by_pixel: self.pxbpx_method="{0}" is not supported!'.format(self.pxbpx_method)

  txt = self.name + ' {0:.2f}'.format(self.wavelength)
  ax.annotate(txt, xy=(0.03, 0.90), xycoords='axes fraction', fontsize=14)


  ###########################
  # Save the figure
  ###########################
  plt.savefig(file_out, transparent=True)
  print ' - plotter: show_phase_binned_mean_subtracted: saved {0}'.format(file_out)
  plt.close()

  return None

###################################################################################################
def show_phase_binned_mean_subtracted(self, dir_out=None):
  """
  Present a color-coded 2-D map of the residuals of the phase binned line profile, with a trial 
  frequency self.phase_freq. If there exists any coherent and long-term frequency in the dataset,
  it shall represent itself as a clear S-shaped feature in this figure. Before creating this figure,
  the subtract_mean_from_binned_spectrum() method must have already been applied.
  @param self: an instance of the lib.Line class 
  @type self: object
  @param dir_out: the directory where the plot file will be stored. The full filename is constructed
         internally to ensure that the line name, wavelength and the phase_freq are all appended to 
         the figure name. The figure name is constructed like the following:
         <dir_out>/<>
  """
  if dir_out is None: return False

  # Check if the residual matrix is already computed.
  matrix  = self.matrix_phase_binned_residuals
  if isinstance(matrix, int):
    logging.error('plotter: show_phase_binned_mean_subtracted: matrix not computed.')
    raise SystemExit, 'plotter: show_phase_binned_mean_subtracted: matrix not computed.'

  mean_spec = self.mean_spec
  disp_v    = mean_spec.disp_v
  phases    = self.phases[:-1] 

  v_2d, phi_2d = np.meshgrid(disp_v, phases)

  # print v_2d.shape, phi_2d.shape, matrix.shape

  fig, ax = plt.subplots(1, figsize=(5, 5), sharex=True)
  plt.subplots_adjust(left=0.08, right=0.98, bottom=0.10, top=0.95)

  for xval in np.arange(-400, 400, 50):
    ax.axvline(xval, linestyle='dotted', color='k', zorder=1)
  min_v   = np.min(matrix)
  max_v   = np.max(matrix)
  ceil_v  = max(abs(min_v), max_v)
  levels  = np.linspace(-ceil_v, ceil_v, 21, endpoint=True)
  # C = ax.contourf(v_2d, phi_2d, matrix, cmap=plt.get_cmap('Greys'), vmin=-ceil_v, vmax=ceil_v,
  #                 levels=levels, alpha=0.90, zorder=2)
  C   = ax.imshow(matrix, cmap='Greys', interpolation='nearest', aspect='auto',
                  extent=(disp_v[0], disp_v[-1], 0, 1), vmin=-ceil_v, vmax=ceil_v, 
                  alpha=0.90, origin='lower', zorder=2)
  pos = ax.get_position()
  cax = fig.add_axes([pos.x0, pos.y1+0.01, pos.x1-pos.x0, 0.995-pos.y1-0.01])
  B = fig.colorbar(C, cax=cax, ticks=[-ceil_v, 0, ceil_v], orientation='horizontal')
  # B.ax.set_xticklabels(['< -1', '0', '> 1'])
  B.ax.set_xticklabels(())

  ###########################
  # Cosmetics
  ###########################
  ax.set_xlabel(r'Dispersion range [km sec$^{-1}$]')
  ax.set_xlim(disp_v[0], disp_v[-1])

  txt = self.name + ' {0:.2f}'.format(self.wavelength)
  ax.annotate(txt, xy=(0.03, 0.10), xycoords='axes fraction', fontsize=14)
  ax.annotate(r'$f$ = {0:.6f}'.format(self.phase_freq)+r' d$^{-1}$', xy=(0.03, 0.04), xycoords='axes fraction', fontsize=12)
  # ax.annotate('Num. bins={0}'.format(self.n_phase_bins), xy=(0.04, 0.05), xycoords='axes fraction', fontsize=12)

  ###########################
  # Generate output filename
  # and save the figure
  ###########################
  if dir_out[-1] != '/': dir_out += '/'
  name     = self.name
  if '\,' in name:
    name   = name.split('\,')
    name   = ''.join(name)
  file_out = '{0}{1}-{2:.2f}-phase-binned-at-freq{3:.6f}.png'.format(
              dir_out, name, self.wavelength, self.phase_freq)
  plt.savefig(file_out, transparent=True)
  print ' - plotter: show_phase_binned_mean_subtracted: saved {0}'.format(file_out)
  plt.close()

  return None

###################################################################################################
def show_phase_binned_mean_spectrum(self, file_out=None):
  """
  Stack the phase binned spectra on top of each other to visualize the line profile variability folded
  on the desired phase frequency.
  @param self: an instance of the lib.Line class 
  @type self: object
  """
  if file_out is None: return False

  fig, ax = plt.subplots(1, figsize=(4, 6), sharex=True)
  plt.subplots_adjust(left=0.17, right=0.97, bottom=0.09, top=0.97)

  ph_binned = self.list_phase_binned
  phases    = self.phases
  n_spec    = len(ph_binned)

  for i, sp in enumerate(ph_binned):
    phase   = phases[i]
    disp_v  = sp.disp_v
    flux    = sp.flux 
    shifted = flux + phase
    
    ax.plot(disp_v, shifted, lw=0.5, color='grey')

  ###########################
  # Cosmetics
  ###########################
  ax.set_xlabel(r'Dispersion range [km sec$^{-1}$]')

  ax.annotate(self.name, xy=(0.04, 0.17), xycoords='axes fraction', fontsize=14)
  ax.annotate(r'$f$={0:.5f}'.format(self.phase_freq)+r' d$^{-1}$', xy=(0.04, 0.10), xycoords='axes fraction', fontsize=12)
  ax.annotate('Num. bins={0}'.format(self.n_phase_bins), xy=(0.04, 0.05), xycoords='axes fraction', fontsize=12)

  plt.savefig(file_out, transparent=True)
  print ' - plotter: show_phase_binned_mean_spectrum: saved {0}'.format(file_out)
  plt.close()

  return None

###################################################################################################
def show_mean_spectrum(self, file_out=None):
  """
  Plot the weighted average of the line across the whole dispersion range, including the standard
  deviation around the line profile. We use the self.mean_spec and self.std_spec attributes from 
  self
  @param self: an instance of the lib.Line class 
  @type self: object
  """
  if file_out is None: return False

  fig, (top, bot) = plt.subplots(2, 1, figsize=(4, 6), sharex=True)
  plt.subplots_adjust(left=0.17, right=0.97, bottom=0.09, top=0.97)

  mean_spec = self.mean_spec
  std_spec  = self.std_spec
  dispersion= mean_spec.disp_v
  profile   = mean_spec.flux
  std_one   = std_spec.flux

  top.fill_between(dispersion, y1=profile-std_one, y2=profile+std_one, color='grey', zorder=1)
  top.plot(dispersion, profile, linestyle='solid', color='k', zorder=2)

  bot.plot(dispersion, std_one, linestyle='solid', color='grey', zorder=1)

  ###########################
  # Cosmetics
  ###########################
  top.set_ylabel(r'Average Line Profile')
  bot.set_xlabel(r'Dispersion range [km sec$^{-1}$]')
  bot.set_ylabel(r'Standard Deviation')

  top.annotate(mean_spec.name, xy=(0.02, 0.06), xycoords='axes fraction', fontsize=10)

  plt.savefig(file_out, transparent=True)
  print ' - plotter: show_mean_spectrum: saved {0}'.format(file_out)
  plt.close()

  return None

###################################################################################################


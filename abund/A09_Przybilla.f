
     ! Stuff to go on top of the the chem_def module:
     real(dp) :: A09_Prz_zfrac(num_chem_elements)  ! Fraction by mass of the total Z
     ! Abundances are abased on Asplund, Grevesse, Sauval, and Scott 2009, ARA&A, 47:481–522
     ! but that of the some key elements are updated based on:
     ! "Present-day cosmic abundances ..." by Nieva, M.-F. & Przybilla, N. 2012, A&A, 539, 143 
     ! and the proceeding paper
     ! "Hot Stars and Cosmic Abundances" by Przybilla N. Nieva M. F., Irrgang A. and Butler K. 2013, EAS Publ. Ser. in preparation
     ! The modified abundances w.r.t. A09 are (eps_El = log(El/H)+12.0) 
     ! eps_He = 10.99
     ! eps_C  = 8.33
     ! eps_N  = 7.79
     ! eps_O  = 8.76
     ! eps_Ne = 8.09    ! Cunha et al. (2006) give 8.11
     ! eps_Mg = 7.56
     ! eps_Al = 6.30
     ! eps_Si = 7.50
     ! eps_S  = 7.14
     ! eps_Ar = 6.50
     ! eps_Fe = 7.52   
     real(dp), parameter :: A09_Prz_zsol = 0.014d0, A09_Prz_ysol = 0.276d0

     contains
     
      subroutine init_A09_Przybilla_data ! fraction by mass of total Z
         ! The mass fraction is taken from Asplund et al. (2009), and modified by the
         ! B-star measurement of Nieva & Przybilla 2012, A&A, 539, 143 and
         ! Przybilla et al. (2013), EAS proceeding to be published
         ! The modified elements are: he, c, n, o, ne, mg, al, si, s, ar, fe
         include 'formats.dek'
         
         A09_Prz_zfrac(:)  = 0d0

!         A09_Prz_zfrac(e_h ) = 12.00
!         A09_Prz_zfrac(e_he) = 10.99
         A09_Prz_zfrac(e_li) = 3.26
         A09_Prz_zfrac(e_be) = 1.38
         A09_Prz_zfrac(e_b ) = 2.70
         A09_Prz_zfrac(e_c ) = 8.33
         A09_Prz_zfrac(e_n ) = 7.79
         A09_Prz_zfrac(e_o ) = 8.76
         A09_Prz_zfrac(e_f ) = 4.56
         A09_Prz_zfrac(e_ne) = 8.09
         A09_Prz_zfrac(e_na) = 6.24
         A09_Prz_zfrac(e_mg) = 7.56
         A09_Prz_zfrac(e_al) = 6.30
         A09_Prz_zfrac(e_si) = 7.50
         A09_Prz_zfrac(e_p ) = 5.41
         A09_Prz_zfrac(e_s ) = 7.14
         A09_Prz_zfrac(e_cl) = 5.50
         A09_Prz_zfrac(e_ar) = 6.50
         A09_Prz_zfrac(e_k ) = 5.03
         A09_Prz_zfrac(e_ca) = 6.34
         A09_Prz_zfrac(e_sc) = 3.15
         A09_Prz_zfrac(e_ti) = 4.95
         A09_Prz_zfrac(e_v ) = 3.93
         A09_Prz_zfrac(e_cr) = 5.64
         A09_Prz_zfrac(e_mn) = 5.43
         A09_Prz_zfrac(e_fe) = 7.52
         A09_Prz_zfrac(e_co) = 4.99
         A09_Prz_zfrac(e_ni) = 6.22
         A09_Prz_zfrac(e_cu) = 4.19
         A09_Prz_zfrac(e_zn) = 4.56
         A09_Prz_zfrac(e_ga) = 3.04
         A09_Prz_zfrac(e_ge) = 3.65
         A09_Prz_zfrac(e_as) = 2.30
         A09_Prz_zfrac(e_se) = 3.34
         A09_Prz_zfrac(e_br) = 2.54
         A09_Prz_zfrac(e_kr) = 3.25
         A09_Prz_zfrac(e_rb) = 2.52
         A09_Prz_zfrac(e_sr) = 2.87
         A09_Prz_zfrac(e_y ) = 2.21
         A09_Prz_zfrac(e_zr) = 2.58
         A09_Prz_zfrac(e_nb) = 1.46
         A09_Prz_zfrac(e_mo) = 1.88
         A09_Prz_zfrac(e_ru) = 1.75
         A09_Prz_zfrac(e_rh) = 0.91
         A09_Prz_zfrac(e_pd) = 1.57
         A09_Prz_zfrac(e_ag) = 0.94
         A09_Prz_zfrac(e_cd) = 1.71
         A09_Prz_zfrac(e_in) = 0.80
         A09_Prz_zfrac(e_sn) = 2.04
         A09_Prz_zfrac(e_sb) = 1.01
         A09_Prz_zfrac(e_te) = 2.18
         A09_Prz_zfrac(e_i ) = 1.55
         A09_Prz_zfrac(e_xe) = 2.24
         A09_Prz_zfrac(e_cs) = 1.08
         A09_Prz_zfrac(e_ba) = 2.18
         A09_Prz_zfrac(e_la) = 1.10
         A09_Prz_zfrac(e_ce) = 1.58
         A09_Prz_zfrac(e_pr) = 0.72
         A09_Prz_zfrac(e_nd) = 1.42
         A09_Prz_zfrac(e_sm) = 0.96
         A09_Prz_zfrac(e_eu) = 0.52
         A09_Prz_zfrac(e_gd) = 1.07
         A09_Prz_zfrac(e_tb) = 0.30
         A09_Prz_zfrac(e_dy) = 1.10
         A09_Prz_zfrac(e_ho) = 0.48
         A09_Prz_zfrac(e_er) = 0.92
         A09_Prz_zfrac(e_tm) = 0.10
         A09_Prz_zfrac(e_yb) = 0.84
         A09_Prz_zfrac(e_lu) = 0.10
         A09_Prz_zfrac(e_hf) = 0.85
         A09_Prz_zfrac(e_ta) = -0.12
         A09_Prz_zfrac(e_w ) = 0.85
         A09_Prz_zfrac(e_re) = 0.26
         A09_Prz_zfrac(e_os) = 1.40
         A09_Prz_zfrac(e_ir) = 1.38
         A09_Prz_zfrac(e_pt) = 1.62
         A09_Prz_zfrac(e_au) = 0.92
         A09_Prz_zfrac(e_hg) = 1.17
         A09_Prz_zfrac(e_tl) = 0.90
         A09_Prz_zfrac(e_pb) = 1.75
         A09_Prz_zfrac(e_bi) = 0.65
         A09_Prz_zfrac(e_th) = 0.02
         A09_Prz_zfrac(e_u ) = -0.54
                           
         ! convert to mass fractions
         Y_H = 1.0 - (A09_Prz_zsol + A09_Prz_ysol)/element_atomic_weight(e_h)
         do i = e_li, e_u
            Y_i = Y_H*10**(A09_Prz_zfrac(i) - 12d0)
            A09_Prz_zfrac(i) = Y_i*element_atomic_weight(i)
         end do
                           
         if (report_solar .or.  abs(sum(A09_Prz_zfrac)-A09_Prz_zsol) > 1d-3 ) then
            write(*,1)'sum(A09_Prz_zfrac)', sum(A09_Prz_zfrac)
            write(*,1)'A09_Prz_zsol: ', A09_Prz_zsol
            write(*,1)'sum - A09_Prz_zsol', sum(A09_Prz_zfrac)-A09_Prz_zsol
            write(*,*)
            write(*,1) 'X_C/Z', A09_Prz_zfrac(e_c)/A09_Prz_zsol
            write(*,1) 'X_N/Z', A09_Prz_zfrac(e_n)/A09_Prz_zsol
            write(*,1) 'X_O/Z', A09_Prz_zfrac(e_o)/A09_Prz_zsol
            write(*,1) 'X_Ne/Z', A09_Prz_zfrac(e_ne)/A09_Prz_zsol
            write(*,*)
            !stop 1
         end if

         ! Renormalize the z_frac to ensure that the sum would yield 1.0
         A09_Prz_zfrac(:) = A09_Prz_zfrac(:) / sum(A09_Prz_zfrac(:))
         
      end subroutine init_A09_Przybilla_data

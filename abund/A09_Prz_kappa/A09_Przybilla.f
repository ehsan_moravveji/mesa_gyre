
     ! Stuff to go on top of the the chem_def module:
     real(dp) :: A09_Prz_mass_frac(num_chem_elements)
     real(dp), parameter :: A09_Prz_zsol = 0.014d0, A09_Prz_ysol = 0.276d0

      subroutine init_A09_Przybilla_data ! fraction by mass of total Z
         ! The mass fraction is taken from Asplund et al. (2009), and modified by the
         ! B-star measurement of Nieva & Przybilla 2012, A&A, 539, 143 and
         ! Przybilla et al. (2013), proceeding to be published
         ! The modified elements are: he, c, n, o, ne, mg, al, si, s, ar, fe
         include 'formats.dek'
         
         A09_Prz_mass_frac(:) = 0
         
         A09_Prz_mass_frac(e_h ) = 7.104402e-01  
         A09_Prz_mass_frac(e_he) = 2.756990e-01  
         A09_Prz_mass_frac(e_li) = 8.902560e-09  
         A09_Prz_mass_frac(e_be) = 1.523751e-10  
         A09_Prz_mass_frac(e_b ) = 3.819080e-09  
         A09_Prz_mass_frac(e_c ) = 1.809928e-03  
         A09_Prz_mass_frac(e_n ) = 6.087356e-04  
         A09_Prz_mass_frac(e_o ) = 6.489281e-03  
         A09_Prz_mass_frac(e_f ) = 4.861944e-07  
         A09_Prz_mass_frac(e_ne) = 1.749877e-03  
         A09_Prz_mass_frac(e_na) = 2.815967e-05  
         A09_Prz_mass_frac(e_mg) = 6.219973e-04  
         A09_Prz_mass_frac(e_al) = 3.794544e-05  
         A09_Prz_mass_frac(e_si) = 6.260010e-04  
         A09_Prz_mass_frac(e_p ) = 5.611601e-06  
         A09_Prz_mass_frac(e_s ) = 3.119781e-04  
         A09_Prz_mass_frac(e_cl) = 7.902161e-06  
         A09_Prz_mass_frac(e_ar) = 8.904056e-05  
         A09_Prz_mass_frac(e_k ) = 2.952914e-06  
         A09_Prz_mass_frac(e_ca) = 6.180148e-05  
         A09_Prz_mass_frac(e_sc) = 4.475893e-08  
         A09_Prz_mass_frac(e_ti) = 3.006970e-06  
         A09_Prz_mass_frac(e_v ) = 3.056079e-07  
         A09_Prz_mass_frac(e_cr) = 1.599792e-05  
         A09_Prz_mass_frac(e_mn) = 1.042236e-05  
         A09_Prz_mass_frac(e_fe) = 1.303398e-03  
         A09_Prz_mass_frac(e_co) = 4.059316e-06  
         A09_Prz_mass_frac(e_ni) = 6.865657e-05  
         A09_Prz_mass_frac(e_cu) = 6.937150e-07  
         A09_Prz_mass_frac(e_zn) = 1.673903e-06  
         A09_Prz_mass_frac(e_ga) = 5.388513e-08  
         A09_Prz_mass_frac(e_ge) = 2.287013e-07  
         A09_Prz_mass_frac(e_as) = 1.053650e-08  
         A09_Prz_mass_frac(e_se) = 1.217587e-07  
         A09_Prz_mass_frac(e_br) = 1.952816e-08  
         A09_Prz_mass_frac(e_kr) = 1.050332e-07  
         A09_Prz_mass_frac(e_rb) = 1.994782e-08  
         A09_Prz_mass_frac(e_sr) = 4.578214e-08  
         A09_Prz_mass_frac(e_y ) = 1.016293e-08  
         A09_Prz_mass_frac(e_zr) = 2.444567e-08  
         A09_Prz_mass_frac(e_nb) = 1.888585e-09  
         A09_Prz_mass_frac(e_mo) = 5.129707e-09  
         A09_Prz_mass_frac(e_ru) = 4.006039e-09  
         A09_Prz_mass_frac(e_rh) = 5.895618e-10  
         A09_Prz_mass_frac(e_pd) = 2.786866e-09  
         A09_Prz_mass_frac(e_ag) = 6.621956e-10  
         A09_Prz_mass_frac(e_cd) = 4.063513e-09  
         A09_Prz_mass_frac(e_in) = 5.106259e-10  
         A09_Prz_mass_frac(e_sn) = 9.174453e-09  
         A09_Prz_mass_frac(e_sb) = 8.782082e-10  
         A09_Prz_mass_frac(e_te) = 1.361268e-08  
         A09_Prz_mass_frac(e_i ) = 3.173717e-09  
         A09_Prz_mass_frac(e_xe) = 1.608179e-08  
         A09_Prz_mass_frac(e_cs) = 1.126248e-09  
         A09_Prz_mass_frac(e_ba) = 1.465038e-08  
         A09_Prz_mass_frac(e_la) = 1.232572e-09  
         A09_Prz_mass_frac(e_ce) = 3.754719e-09  
         A09_Prz_mass_frac(e_pr) = 5.212276e-10  
         A09_Prz_mass_frac(e_nd) = 2.674106e-09  
         A09_Prz_mass_frac(e_sm) = 9.665519e-10  
         A09_Prz_mass_frac(e_eu) = 3.546798e-10  
         A09_Prz_mass_frac(e_gd) = 1.302217e-09  
         A09_Prz_mass_frac(e_tb) = 2.235044e-10  
         A09_Prz_mass_frac(e_dy) = 1.441937e-09  
         A09_Prz_mass_frac(e_ho) = 3.510697e-10  
         A09_Prz_mass_frac(e_er) = 9.805843e-10  
         A09_Prz_mass_frac(e_tm) = 1.499030e-10  
         A09_Prz_mass_frac(e_yb) = 8.437997e-10  
         A09_Prz_mass_frac(e_lu) = 1.552562e-10  
         A09_Prz_mass_frac(e_hf) = 8.906493e-10  
         A09_Prz_mass_frac(e_ta) = 9.674898e-11  
         A09_Prz_mass_frac(e_w ) = 9.173454e-10  
         A09_Prz_mass_frac(e_re) = 2.388300e-10  
         A09_Prz_mass_frac(e_os) = 3.367998e-09  
         A09_Prz_mass_frac(e_ir) = 3.250060e-09  
         A09_Prz_mass_frac(e_pt) = 5.731992e-09  
         A09_Prz_mass_frac(e_au) = 1.154743e-09  
         A09_Prz_mass_frac(e_hg) = 2.091232e-09  
         A09_Prz_mass_frac(e_tl) = 1.144296e-09  
         A09_Prz_mass_frac(e_pb) = 8.212637e-09  
         A09_Prz_mass_frac(e_bi) = 6.579583e-10  
         A09_Prz_mass_frac(e_th) = 1.712585e-10  
         A09_Prz_mass_frac(e_u ) = 4.838632e-11  
                 
         if (report_solar .or. abs(sum(A09_Prz_mass_frac)-A09_Prz_zsol) > 1d-3 ) then
            write(*,1)'sum(A09_Prz_mass_frac)', sum(A09_Prz_mass_frac)
            write(*,1)'A09_Prz_zsol', A09_Prz_zsol
            write(*,1)'sum - A09_Prz_zsol', sum(A09_Prz_mass_frac)-A09_Prz_zsol
            write(*,*)
            write(*,1) 'X_C/Z', A09_Prz_mass_frac(e_c)/A09_Prz_zsol
            write(*,1) 'X_N/Z', A09_Prz_mass_frac(e_n)/A09_Prz_zsol
            write(*,1) 'X_O/Z', A09_Prz_mass_frac(e_o)/A09_Prz_zsol
            write(*,1) 'X_Ne/Z', A09_Prz_mass_frac(e_ne)/A09_Prz_zsol
            write(*,*)
            !stop 1
         end if
         
         A09_Prz_mass_frac(:) = A09_Prz_mass_frac(:) / sum(A09_Prz_mass_frac(:))
         
         return
         
         do i = e_li, e_u
            if (A09_Prz_mass_frac(i) > 1d-3) 
     >            write(*,1) trim(chem_element_Name(i)), A09_Prz_mass_frac(i)
         end do
         stop 'init_A09_Przybilla_data'
         
      end subroutine init_A09_Przybilla_data

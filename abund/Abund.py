
import numpy as np
import os
from collections import OrderedDict
from copy import deepcopy
import pylab as plt

epsilon = 1e-12

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================
def Cunha_06():
  """
  This function returns the revised Ne abundance for B stars based on their NLTE study of B stars in Orion association.
  The reference is:
  - Cunha, K.; Hubeny, I.; Lanz, T. 2006, ApJL, 647, 143
  They revised A(Ne)=7.84 to A(Ne)=8.11 +/- 0.04
  @return: a list with a single tuple in it: [('ne', 8.11)]. This can be called and passed e.g. to Abund.Asplund_09 as 
     an argument for change_zfrac.
  @type: list with a tuple
  """
  return [('ne', 8.11)]
  
#=================================================================================================================
def Przybilla_13():
  """
  This function returns a list of tuples specifying the new cosmic abundances for B stars based on the following papers:
  - Nieva & Przybilla, 2012, A&A, 539, 143
  - Przybilla, et al. 2013, proceeding to appear in EAS Pub. Ser.
  @return: list of tuples. Each tuple has two items, the first being the element name according to gen_element_atomic_weight
    and the second the revised z_fraction; e.g. ('he', 10.99). This can be called and passed e.g. to Abund.Asplund_09 as 
    an argument for change_zfrac.
  @rtype: list of tuple
  """
  przybilla_he = ('he', 10.99)
  przybilla_c  = ('c', 8.33)
  przybilla_n  = ('n', 7.79) 
  przybilla_o  = ('o', 8.76)
  przybilla_ne = ('ne', 8.09)
  przybilla_mg = ('mg', 7.56)
  przybilla_al = ('al', 6.30)
  przybilla_si = ('si', 7.50)
  przybilla_s  = ('s', 7.14)
  przybilla_ar = ('ar', 6.50)
  przybilla_fe = ('fe', 7.52)
  przybilla_list = [przybilla_he, przybilla_c, przybilla_n, przybilla_o, przybilla_ne, przybilla_mg, przybilla_al, 
                    przybilla_si, przybilla_s, przybilla_ar, przybilla_fe]
  return przybilla_list

#=================================================================================================================
def gen_TeX_table_abund(file_out=None):
  """
  This function calls three different composition lists which are already available, and writes a LaTeX compatible 
  table to an output file.
  @param file_out: full path to the output .tex table; default=None. If not specified, the table will be written to 
      "Composition.tex" file. At the end, a plot file is saved showing X_i/Z for the three compositions. The pdf 
      file name is the same as file_out with .pdf extension.
  @type file_out: string
  @return: None
  @rtype: None
  """
  if not file_out: file_out = 'Composition.tex'
  change_ne = Cunha_06()
  change_przybilla = Przybilla_13()
  change_nothing = []
  XYZ = {'X':0.710, 'Y':0.286, 'Z':0.014}
  
  z_frac_A05_Ne, m_frac_A05_Ne = Asplund_05(change_zfrac=change_ne)
  z_frac_A09, m_frac_A09 = Asplund_09(change_zfrac=change_nothing)
  z_frac_A09_Prz, m_frac_A09_Prz = Asplund_09(change_zfrac=change_przybilla, dic_XYZ=XYZ)

  Z_A05_Ne = m_frac_A05_Ne['Z_sun']
  Z_A09    = m_frac_A09['Z_sun']
  Z_A09_Prz= m_frac_A09_Prz['Z_sun']

  XYZ_to_file  = ['X_sun', 'Y_sun', 'Z_sun']
  elem_to_file = ['h', 'he', 'c', 'n', 'o', 'ne', 'na', 'mg', 'al', 'si', 'p', 's', 
                  'cl', 'ar', 'k', 'ca', 'ti', 'cr', 'mn', 'fe', 'ni']  
  yaxis_1 = []; yaxis_2 = []; yaxis_3 = []
  yaxis_4 = []; yaxis_5 = []; yaxis_6 = []
  lines = []
  nl = '\n'
  line = r'Element & & A05$+$Ne & & & A09 & & & A09$+$P13 & \\ ' + nl
  lines.append(line)
  line = r'        & $\epsilon_i$ & $X_i$ & $X_i/Z$ & $\epsilon_i$ & $X_i$ & $X_i/Z$ & $\epsilon_i$ & $X_i$ & $X_i/Z$ \\ '+nl
  lines.append(line)
  lines.append('\hline' + nl)

  for x in XYZ_to_file:
    line = r'%s & & %6.4f & & & %6.4f & & & %6.4f & \\ %s' % (x[0], m_frac_A05_Ne[x], m_frac_A09[x], m_frac_A09_Prz[x], nl)
    lines.append(line)
  lines.append(r' \\' + nl)
    
  for e in elem_to_file:
    name   = e.capitalize()
    first  = z_frac_A05_Ne[e]
    second = m_frac_A05_Ne[e]
    third  = m_frac_A05_Ne[e]/Z_A05_Ne
    fourth = z_frac_A09[e]
    fifth  = m_frac_A09[e]
    sixth  = m_frac_A09[e]/Z_A09
    seventh= z_frac_A09_Prz[e]
    eighth = m_frac_A09_Prz[e]
    ninth  = m_frac_A09_Prz[e]/Z_A09_Prz
    
    line = r"%3s & %5.2f & $%10.4e$ & $%10.4e$ & %5.2f & $%10.4e$ & $%10.4e$ & %5.2f & $%10.4e$ & $%10.4e$ \\ %s" % (name, first, second, third, fourth, fifth, sixth, seventh, 
eighth, ninth, nl)
    lines.append(line)

    yaxis_1.append(third)
    yaxis_2.append(sixth)
    yaxis_3.append(ninth)
    
    yaxis_4.append(second)
    yaxis_5.append(fifth)
    yaxis_6.append(eighth)
    
  lines.append('\hline' + nl)
    
  w = open(file_out, 'w')
  w.writelines(lines)
  w.close()
  
  fig = plt.figure(figsize=(8,6))
  ax0 = fig.add_subplot(211)
  ax1 = fig.add_subplot(212)
  plt.subplots_adjust(left=0.10, right=0.99, bottom=0.07, top=0.98, hspace=0.02, wspace=0.02)
  
  xaxis = np.arange(len(elem_to_file))
  xlabels = []
  for e in elem_to_file: xlabels.append(e.capitalize())
  yaxis_1 = np.asarray(yaxis_1)
  yaxis_2 = np.asarray(yaxis_2)
  yaxis_3 = np.asarray(yaxis_3)
  yaxis_4 = np.asarray(yaxis_4)
  yaxis_5 = np.asarray(yaxis_5)
  yaxis_6 = np.asarray(yaxis_6)

  ax0.plot(xaxis, yaxis_4, color='black', label=r'A05 + Ne')
  ax0.scatter(xaxis, yaxis_4, color='black', marker='o')

  ax0.plot(xaxis, yaxis_5, color='blue', label=r'A09')
  ax0.scatter(xaxis, yaxis_5, color='blue', marker='s')

  ax0.plot(xaxis, yaxis_6, color='red', label=r'A09 + P13')
  ax0.scatter(xaxis, yaxis_6, color='red', marker='^')

  ax0.set_xticks(xaxis, [' ']*len(xaxis))
  #ax0.set_xticklabels(())
  ax0.set_xlim(0, len(elem_to_file))
  ax0.set_ylabel(r'$X_i$ [Mass Fraction]')
  ax0.set_ylim(-0.0001, 0.008)
  leg = ax0.legend(loc=1, shadow=True)

  # Next panel
  ax1.plot(xaxis, yaxis_1, color='black', label=r'A05 + Ne')
  ax1.scatter(xaxis, yaxis_1, color='black', marker='o')

  ax1.plot(xaxis, yaxis_2, color='blue', label=r'A09')
  ax1.scatter(xaxis, yaxis_2, color='blue', marker='s')

  ax1.plot(xaxis, yaxis_1, color='red', label=r'A09 + P13')
  ax1.scatter(xaxis, yaxis_1, color='red', marker='^')
  
  plt.xticks(xaxis, xlabels)
  ax1.set_xlim(0, len(elem_to_file))
  ax1.set_ylabel(r'$X_i/Z$')
  ax1.set_ylim(-0.01, 0.5)
  leg = ax1.legend(loc=1, shadow=True)
  
  plt.savefig(file_out+'.pdf')
  print ' - Abund: gen_TeX_table_abund: Figure %s saved.' % (file_out+'.pdf')
  plt.close()
  
  return None


#=================================================================================================================
def give_OP_X_Z_list(func=None, change_zfrac=[], dic_XYZ={}):
  """
  This function gives the mtal relative number fraction Xi/Z for 15 species which are needed to query the OP Tables website.
  They are C, N, O, Ne, Na, Mg, Al, Si, S, Ar, Ca, Cr, Mn, Fe and Ni.
  First, the full list based on func is retrieved, and the Xi for the above 15 elements are extracted. We then 
  fix X and Y, but re-scale (i.e. renormalize) the metal mass fraction Xi to match the Z required for that specific list.
  @param func: is a function object and is one of the following suppoerted options:
         - Abund.Asplund_05 for Asplund et al. (2005) list
         - Abund.Asplund_09 for Asplund et al. (2009) list
  @type func: function
  @param change_zfrac: if you want to change abundances of specific elements, you pass this list of tuples. Each tuple
     contains the element name and the new abundance in number fraction (not mass!). 
     For example: change_zfrac = [('he', 10.99), ('ne', 8.11)]
  @type change_zfrac: list of tuples
  @param dic_XYZ: dictionary with three keys: X, Y and Z to pinpoint the initial abundances specifically to compile the 
     composition table. This dictionary, if provided, will be passed as an argument to the function "func"
  @type dic_XYZ: dictionary
  @return: None
  @rtype: None
  """
  if func is None:
    message = 'Error: Abund.py: give_OP_X_Z_list: Input function "func" is not specified'
    raise SystemExit, message
  
  z_frac, m_frac = func(change_zfrac=change_zfrac, dic_XYZ=dic_XYZ)
  X_sun = m_frac['X_sun']
  Y_sun = m_frac['Y_sun']
  Z_sun = m_frac['Z_sun']
  
  OP_elem = ['c', 'n', 'o', 'ne', 'na', 'mg', 'al', 'si', 's', 'ar', 'ca', 'cr', 'mn', 'fe', 'ni']
  dic_OP = OrderedDict({})
  dic_OP['X_sun'] = X_sun
  dic_OP['Y_sun'] = Y_sun    
  dic_OP['Z_sun'] = Z_sun    
  metal_fraction = 0.0
  Z_num_frac = 0.0
  for elem in OP_elem:
    metal_fraction += m_frac[elem]
    N_H = 1.0 - (Z_sun + Y_sun)/1.00794
    N_Xi = N_H * np.power(10.0, z_frac[elem]-12.0)
    Z_num_frac += N_Xi
    dic_OP[elem] = N_Xi
  #norm_factor = Z_sun / metal_fraction
  norm_factor = 1.0 / Z_num_frac

  # Reset and Rescale the metal_fraction
  #metal_fraction = 0.0
  #Z_num_frac = 0.0
  #for elem in OP_elem:
    ##dic_OP[elem] = dic_OP[elem] #* norm_factor
    ##metal_fraction += dic_OP[elem]
    #Z_num_frac += dic_OP[elem]

  print ' - Abund: give_OP_X_Z_list:'
  print '   Function: %s' % (func, )
  print '   Number of modified elements = %s' % (len(change_zfrac), )
  #print '   Z_sun = %s versus Metal Mass Fraction = %s' % (Z_sun, metal_fraction)
  print '\n   Isotope       X_i              X_i/Z'
  print '                 [Num. Frac.]     [Num. Frac.]'
  lines = []
  for elem in OP_elem: 
    print '   %5s         %8.6f         %8.6f' % (elem.capitalize(), dic_OP[elem], dic_OP[elem]/Z_num_frac)
    line = '%8.6f %s' % (dic_OP[elem]/Z_num_frac, '\n')
    lines.append(line)
  
  w = open('OpClient/' + str(func.__name__) + '.list', 'w')
  w.writelines(lines)
  w.close()

  return None 
  
#=================================================================================================================
def gen_OP_table_grid(logT_from=3.75, logT_to=8.0, delta_logT=0.025, 
                      logR_from=-7.5, logR_to=-0.5, delta_logR=0.25, 
                      file_out=''):
  """
  This function writes a simple file to disk which serves as an starting point for the OP server to set up its own
  grid. It covers the default range in logT and logR, with the mesh which is currently interpolated in MESA.
  @param logT_from, logT_to: float, giving the starting, ending in logT. The default values are currently coverd
         by the OP server
  @type logT_from, logT_to: float
  @param logR_from, logR_to: float, giving the starting, ending in logR. The default values are currently coverd
         by the OP server
  @type logR_from, logR_to: float
  @param file_out: full path to the output file 
  @type file_out: string
  @return: None
  @rtype: None
  """
  lines = []
  logT_arr = []
  logR_arr = []
  #delta_logT = (logT_to-logT_from)/(n_interval-1)
  #delta_logR = (logR_from-logR_to)/(n_interval-1)
  n_logT = int((logT_to-logT_from)/delta_logT + 1)
  n_logR = int((logR_to-logR_from)/delta_logR + 1)

  for n in range(n_logT): logT_arr.append(logT_from + n*delta_logT)
  for n in range(n_logR): logR_arr.append(logR_from + n*delta_logR)
  
  for i_logT in range(n_logT):
    for i_logR in range(n_logR):
      line = '  %8E %8E' % (logT_arr[i_logT], logR_arr[i_logR])
      lines.append(line + ' \n')
    
  if not file_out: file_out = 'OP_Table_Grid.txt'
  
  f = open(file_out, 'w')
  f.writelines(lines)
  f.close()  
  print '\n - Abund: gen_OP_table_grid: Output written to %s' % (file_out, )
  print '   logT_from: %s, logT_to: %s, delta_logT: %s, intervals: %s' % (logT_arr[0], logT_arr[-1], delta_logT, n_logT)
  print '   logR_from: %s, logR_to: %s, delta_logR: %s, intervals: %s' % (logR_arr[0], logR_arr[-1], delta_logR, n_logR)
  print
    
  return None
  

#=================================================================================================================
def write_abund(func=None, list_elem=None, change_zfrac=[], dic_XYZ={}, file_out=''):
  """
  This function collects a subset of elements given by list_elem, and collects their corresponding abundance in mass
  fraction. Then, it sets a residual (i.e. rest), and passes the dictionary of the requested species to check_normal
  for normalization and writing to disk.
  @param func: is a function object and is one of the following suppoerted options:
         - Abund.Asplund_05 for Asplund et al. (2005) list
         - Abund.Asplund_09 for Asplund et al. (2009) list
  @type func: function
  @param list_elem: list of species, like 'h', 'he' etc for which we want to retrieve their abundance in mass fraction.
         if the element is not found as the key of species in Asplund_05 or Asplund_09, then that element will be ignored.
  @type list_elem: list of strings
  @param change_zfrac: if you want to change abundances of specific elements, you pass this list of tuples. Each tuple
     contains the element name and the new abundance in number fraction (not mass!). 
     For example: change_zfrac = [('he', 10.99), ('ne', 8.11)]
  @type change_zfrac: list of tuples
  @param dic_XYZ: dictionary with three keys: X, Y and Z to pinpoint the initial abundances specifically to compile the 
     composition table. This dictionary, if provided, will be passed as an argument to the function "func"
  @type dic_XYZ: dictionary
  @param file_out: full path to the output file which contains the list of element and its abundance in mass fraction
  @type file_out: string
  @return: None
  @rtype: None
  """
  if func is None or list_elem is None:
    message = 'Error: Abund.py: write_abund: function %s or list_elem %s is empty.' % (func, list_elem)
    raise SystemExit, message

  z_frac, m_frac = func(change_zfrac=change_zfrac, dic_XYZ=dic_XYZ)
  n_elem = len(list_elem)
  Z_sun = m_frac['Z_sun']
  Y_sun = m_frac['Y_sun']
  X_sun = m_frac['X_sun']
  
  #dic_elem = OrderedDict({})
  #rest = 0.0
  #net = 0.0
  #for i, elem in enumerate(list_elem):
    #if m_frac.has_key(elem):
      #net += m_frac[elem]
      #dic_elem[elem] = m_frac[elem]
    #else:
      #print ' - Warning: Abund.py: write_abund: %s does not exist in the input list' % (elem, )
  #rest = 1.0 - net
  #dic_elem['rest'] = rest
  
  if file_out:
    print '\n - Abund.py: write_abund: directing the collected list to redist_abund. \n'
    dic_redist = redist_abund(dic_in=m_frac, file_out=file_out)

  # Get Z (number fraction) from metals
  z_frac_Z = 0.0
  n_H = 1.0 - (Z_sun + Y_sun) / 1.00794
  for i_elem, elem in enumerate(list_elem):
    if not z_frac.has_key(elem): continue
    if elem == 'h' or elem == 'he': continue
    z_frac_Z += n_H * np.power(10.0, z_frac[elem] - 12.0)  

  # Get Z (mass fraction) from metals:
  mass_frac_Z = 0.0
  for i_elem, elem in enumerate(list_elem):
    if not m_frac.has_key(elem): continue
    if elem == 'h' or elem == 'he': continue
    mass_frac_Z += m_frac[elem]
    
  for i_elem, elem in enumerate(list_elem):
    if not z_frac.has_key(elem): 
      print '  . write_abund skipped: %s ' % (elem.capitalize(), )
      continue
    rel_z_frac = n_H * np.power(10.0, z_frac[elem]-12.0)/z_frac_Z
    rel_m_frac = m_frac[elem]/mass_frac_Z
    print '%4s ... %10e ... %10e ...  %10e ... %10e' % (elem.capitalize(), z_frac[elem], rel_z_frac, 
                                                        m_frac[elem], rel_m_frac)

  print '\n - Abund.py: write_abund: z_frac_Z= %s, mass_frac_Z = %s\n' % (z_frac_Z, mass_frac_Z)
    

  return None

#=================================================================================================================
def get_CNO_Z_frac(file_in=''):
  """
  In MESA, there is an option in &star_job inlist to enhance the opacities due to specific CNO fractions with respect
  to the Z. They are called:
     set_kap_base_CNO_Z_fracs = .false. ! fraction of Z from C, N, and O
      ! e.g., here are the values from Grevesse and Sauval 1998
      kap_base_fC = 0.172062d0
      kap_base_fN = 0.050417d0
      kap_base_fO = 0.468017d0
  This function receives the name of an initial list, and redistributes the 'rest' among other species. 
  Then it prints out the kap_base_fC, kap_base_fN and kap_base_fO, kap_base_fNe
  @param file_in: full path to the input list of species.
  @type file_in: string
  @return: None
  @rtype: None
  """
  if file_in and not os.path.isfile(file_in):
    message = 'Error: Abund.py: redist_abund: %s does not exist' % (file_in, )
    raise SystemExit, message
  
  dic_elem = gen_element_atomic_weight()
  
  dic_redist = redist_abund(file_in)
  dic_Z = deepcopy(dic_redist)
  if dic_Z.has_key('file'): del dic_Z['file']
  if dic_Z.has_key('rest'): del dic_Z['rest']
  if dic_Z.has_key('h1'):   del dic_Z['h1']
  if dic_Z.has_key('h2'):   del dic_Z['h2']
  if dic_Z.has_key('he3'):  del dic_Z['he3']
  if dic_Z.has_key('he4'):  del dic_Z['he4']
  
  if dic_Z.has_key('c12'): c12 = dic_Z['c12']
  else: c12 = 0.0
  if dic_Z.has_key('c13'): c13 = dic_Z['c13']
  else: c13 = 0.0
  if dic_Z.has_key('n14'): n14 = dic_Z['n14']
  else: n14 = 0.0
  if dic_Z.has_key('n15'): n15 = dic_Z['n15']
  else: n15 = 0.0
  if dic_Z.has_key('o16'): o16 = dic_Z['o16']
  else: o16 = 0.0
  if dic_Z.has_key('o17'): o17 = dic_Z['o17']
  else: o17 = 0.0
  if dic_Z.has_key('o18'): o18 = dic_Z['o18']
  else: o18 = 0.0
  if dic_Z.has_key('ne20'): ne20 = dic_Z['ne20']
  else: ne20 = 0.0
  if dic_Z.has_key('ne21'): ne21 = dic_Z['ne21']
  else: ne21 = 0.0
  if dic_Z.has_key('ne22'): ne22 = dic_Z['ne22']
  else: ne22 = 0.0
    
  net_C = c12 + c13
  net_N = n14 + n15
  net_O = o16 + o17 + o18
  net_Ne = ne20 + ne21 + ne22
  net_Z = 0.0
  for key, value in dic_Z.items():
    net_Z += dic_Z[key]
  
  fC = net_C / net_Z
  fN = net_N / net_Z
  fO = net_O / net_Z
  fNe = net_Ne / net_Z
  
  print ' - get_CNO_Z_frac:'
  print '   Just Z in mass fraction: %s' % (net_Z, )
  print '   net_C = %10e, net_N = %10e, net_O = %10e and net_Ne = %10e' % (net_C, net_N, net_O, net_Ne)
  print '   =>'
  print '   fC = %s, fN = %s, fO = %s and fNe = %s \n' % (fC, fN, fO, fNe)
  
  return None
  
#=================================================================================================================
def redist_abund(file_in='', file_out='', dic_in={}):
  """
  This function receives the list of abundances, and tries to redistribute the "rest" upon everything, weighted by
  the abundance of each isotope. It means, the new abundance of the ith isotope (X_i) is the original abundance 
  (x_i) enhanced a bit by a factor alpha_i, where alpha_i = x_i/(Sum_i x_i). Note that i excludes the "rest".
  X_i = x_i + x_i * (rest/Sum_i x_i) = x_i [1. +  rest/Sum_i x_i] = x_i [(rest + Sum_i x_i)/Sum_i x_i]
      ~ ~ x_i/Sum_i x_i
  Note that the abundaces which is read in, must already be normalized to unity, so that rest + Sum_i x_i = 1.0
  up to an acceptable level (set e.g. by epsilon).
  @param file_in: full path to the input list of species.
  @type file_in: string
  @param file_out: string, giving the full path to the output file to write the information to; default = ''
  @type file_out: string
  @return: dictionary with the list of new abundances, and having the value of the 'rest' set to zero
  @rtype: OrderedDict
  """
  if file_in and not os.path.isfile(file_in):
    message = 'Error: Abund.py: redist_abund: %s does not exist' % (file_in, )
    raise SystemExit, message

  if file_in:  
    dic = check_normal(file_in)
  else:
    dic = deepcopy(dic_in)
  rest = 0.0
  if dic.has_key('rest'): rest = dic['rest']
  sum_xi = 0.0
  for key, value in dic.items():
    if key == 'rest' or key == 'file' or key == 'Ref': continue
    if key=='X_sun' or key=='Y_sun' or key=='Z_sun': continue
    sum_xi += value
   
  #sum_new_xi = 0.0
  #dic_new = OrderedDict({})
  #if dic.has_key('file'): dic_new['file'] = dic['file']
  #for key, value in dic.items():
    #if key == 'rest' or key == 'file' or key == 'Ref': continue
    #if key=='X_sun' or key=='Y_sun' or key=='Z_sun': continue
    #new_xi = dic[key] / sum_xi
    #sum_new_xi += new_xi
    #dic_new[key] = new_xi
  #dic_new['rest'] = 0.0

  dic_new = deepcopy(dic)

  # get Z
  Z = 0.0
  for key, value in dic_new.items():
    if key == 'file' or key == 'rest' or key=='Ref': continue
    if key=='X_sun' or key=='Y_sun' or key=='Z_sun': continue
    if key == 'h' or key == 'he': continue
    Z += value
  print ' - Z = %s' % (Z, )

  print '\n   Isotope       X_i              X_i/Z'
  print '                 [Mass Frac.]     [Mass Frac.] \n'
  lines = []
  for key, value in dic_new.items():
    if key == 'file' or key == 'rest' or key=='Ref': continue
    #print '   %5s         %10e         %10e' % (key, value, value/Z)
    print '   %5s         %8.6f         %8.6f' % (key.capitalize(), value, value/Z)
    
    line = ' %6s  %12e %s' % (key, value, ' \n')
    #line = '         A09_Prz_element_zfrac(e_%s) = %5e %s' % (key, value, ' \n')
    lines.append(line)

  # Only write to a file if file_out is not empty
  if not file_out: file_out = 'redist-' + file_in    
  f = open(file_out, 'w')
  f.writelines(lines)
  f.close()
  print ' - New Abundances are stored now in %s \n' % (file_out, )
  
  return dic_new


#=================================================================================================================
def modify_abund(file_in, elem, abund, file_out='new.list'):
  """
  This function receives a filename with full abundance list, checks if it is normalized to unity. Then, it changes
  the abundance of the given element, and rechecks the normalization, until a proper list is found out. Then, the 
  new list is written as a new file.
  @param file_in: full path to the list of abundances
  @type file_in: string
  @param elem: string giving the name of the emelent to change, which is indeed a key in the dictionary of element
         information returned by read_abund_list(). If the name does not match, the program quits.
  @type elem: string
  @param abund: float, the improved abundance of the element "elem"
  @type abund: floag
  @param file_out: string, giving the full path to the output file to write the information to; default = 'new.list'
  @return:
  @rtype:
  """
  
  if not os.path.isfile(file_in):
    message = 'Error: Abund.py: check_normal: %s does not exist' % (filename, )
    raise SystemExit, message

  dic = check_normal(file_in)
  keys = dic.keys()
  if elem not in keys:
    message = 'Error: Abund.py: modify_abund: the requested element: "%s" does not exist in the file %s' % (elem, file_in)
    raise SystemExit, message
  
  #if dic['rest'] > epsilon:
    #print 'Warning: The size of rest (=%s) in the file %s is above the threshold (=%s).' % (dic['rest'], file_in, epsilon)
    #print 'Run the check_normal() and revisit the file %s' % (file_in)
    #return None
  
  dic[elem] = abund
  dic_new = check_normal(file_in, dic)
  new_rest = dic_new['rest']
  if new_rest <= epsilon:
    print ' - The abundance of the rest is %s' % (new_rest, )
    print '   The new list will be written to %s' % (file_out, )
    w = open(file_out, 'w')
    lines = []
    for key, value in dic_new.items():
      if key == 'file': continue
      line = ' ' + key + '  ' + str(value) + ' \n'
      lines.append(line)
    w.writelines()
    w.close()
    return dic_new
  else:
    print ' - Warning: Could not manage to modify %s to %s, due to large "rest" = %s' % (elem, abund, dic['rest'])
    print '   Returns the dictionary of original abundances.'
    return dic
     
  
#=================================================================================================================
def check_normal(filename='', dic_in={}):
  """
  This function reads the list of abundances by calling read_abund_list(), and checks if the sum of all entries in 
  the list add to unity, i.e. 1.0. 
  This function can also independently check a given dictionary.
  @param filename: full path to the file containing the abundance list
  @type filename: string
  @return: dictionary with the original/updated dictionary of abundances, in addition to some on-screen messages.
  @rtype: OrderedDict
  """
  if filename and (not os.path.isfile(filename)):
    message = 'Error: Abund.py: check_normal: Input file: %s does not exist' % (filename, )
    raise SystemExit, message
  
  if len(dic_in) == 0: 
    dic = read_abund_list(filename)
  else:
    dic = deepcopy(dic_in)
    
  net = 0.0
  for key, value in dic.items():
    if key == 'file': continue
    net += value
  dev = net - 1.0
  abund_species = net - dic['rest']
  improved_rest = dic['rest'] - dev
  orig_rest = dic['rest']
  dic['rest'] = improved_rest
  
  if np.abs(dev) <= epsilon:
    flag_norm = True
  else:
    flag_norm = False
    
  print '\n - Abund.py: check_normal:'
  print '   Sum of Abundances for file %s is %s' % (filename, net)
  print '   Deviation from norm = %s; epsilon = %s' % (dev, epsilon)
  if not flag_norm:
    print '   Suggestion: Change "rest" from %s to %s \n' % (orig_rest, improved_rest)
  
  return dic


#=================================================================================================================
def read_abund_list(filename):
  """
  This function reads the list of abundances from a file, and returns a dictionary with isotope names as keys
  and their abundances as their values.
  @param filename: full path to the abundance list
  @type filename: string
  @return: dictionary with key:value pairs representing the element in the list and its abundance.
  @rtype: OrderedDict
  """
  
  if not os.path.isfile(filename):
    message = 'Error: Abund.py: read_abund_list: %s does not exist' % (filename, )
    raise SystemExit, message
  
  dic = OrderedDict({})
  dic['file'] = filename
  
  f = open(filename, 'r')
  lines = f.readlines()
  for ind, line in enumerate(lines):
    key, val_str = line.strip().split()
    dic[key] = float(val_str)
  
  f.close()
  
  return dic
  
#=================================================================================================================
def Asplund_09(report=True, change_zfrac=[], dic_XYZ=None):
  """
  This function sets the abundances in mass fraction and number fraction from Asplund, Grevesse, Sauval, and Scott 
  2009, ARA&A, 47, 481-522 and returns two dictionaries with such information.
  Both dictionaries share the same keys. The keys are:
   - Ref: giving the reference to the published table
   - Z_sun: The adopted Z_sun
   - Y_sun: The adopted Y_sun
   - h: for hydrogen
   - he: for helium
   - ... and so on 
   - the whole available element list can be retrieved by calling Abund.get_all_elem_names()
  @param report: To put on screen reports or not; default = False
  @type report: boolean
  @param change_zfrac: if you want to change abundances of specific elements, you pass this list of tuples. Each tuple
     contains the element name and the new abundance in number fraction (not mass!). 
     For example: change_zfrac = [('he', 10.99), ('ne', 8.11)]
  @type change_zfrac: list of tuples
  @param dic_XYZ: if you like to provide a different X, Y and Z than the default, this dictionary provides them through
     three different key value pairs: X, Y and Z
  @type dic_XYZ: dictionary
  @return: two dictionaries for the abundances in number fraction and mass fraction, respectively
  @rtype: OrderedDict
  """
  if not dic_XYZ:
    #Z_sun = 0.0134  # photospheric
    #Y_sun = 0.2485  # photospheric
    Z_sun = 0.0142
    Y_sun = 0.2703
    X_sun = 1.0 - (Y_sun + Z_sun)
  else:
    Z_sun = dic_XYZ['Z']
    Y_sun = dic_XYZ['Y']
    X_sun = dic_XYZ['X']
  
  # first store log abundances from the paper
  AGSS09_element_zfrac = OrderedDict({})
  AGSS09_element_mass_frac = OrderedDict({})
  AGSS09_element_zfrac['Ref'] = 'Asplund Grevesse Sauval and Scott 2009 ARAA 47 481-522'
  AGSS09_element_mass_frac['Ref'] = 'Asplund Grevesse Sauval and Scott 2009 ARAA 47 481-522'
  
  AGSS09_element_mass_frac['X_sun'] = X_sun 
  AGSS09_element_mass_frac['Y_sun'] = Y_sun 
  AGSS09_element_mass_frac['Z_sun'] = Z_sun 
  
  AGSS09_element_zfrac['h']  = 12.0
  AGSS09_element_zfrac['he'] = 10.93
  AGSS09_element_zfrac['li'] = 3.26
  AGSS09_element_zfrac['be'] = 1.38
  AGSS09_element_zfrac['b'] = 2.70
  AGSS09_element_zfrac['c'] = 8.43
  AGSS09_element_zfrac['n'] = 7.83
  AGSS09_element_zfrac['o'] = 8.69
  AGSS09_element_zfrac['f'] = 4.56
  AGSS09_element_zfrac['ne'] = 7.93
  AGSS09_element_zfrac['na'] = 6.24
  AGSS09_element_zfrac['mg'] = 7.60
  AGSS09_element_zfrac['al'] = 6.45
  AGSS09_element_zfrac['si'] = 7.51
  AGSS09_element_zfrac['p'] = 5.41
  AGSS09_element_zfrac['s'] = 7.12
  AGSS09_element_zfrac['cl'] = 5.50
  AGSS09_element_zfrac['ar'] = 6.40
  AGSS09_element_zfrac['k'] = 5.03
  AGSS09_element_zfrac['ca'] = 6.34
  AGSS09_element_zfrac['sc'] = 3.15
  AGSS09_element_zfrac['ti'] = 4.95
  AGSS09_element_zfrac['v'] = 3.93
  AGSS09_element_zfrac['cr'] = 5.64
  AGSS09_element_zfrac['mn'] = 5.43
  AGSS09_element_zfrac['fe'] = 7.50
  AGSS09_element_zfrac['co'] = 4.99
  AGSS09_element_zfrac['ni'] = 6.22
  AGSS09_element_zfrac['cu'] = 4.19
  AGSS09_element_zfrac['zn'] = 4.56
  AGSS09_element_zfrac['ga'] = 3.04
  AGSS09_element_zfrac['ge'] = 3.65
  AGSS09_element_zfrac['as'] = 2.30  # meteor
  AGSS09_element_zfrac['se'] = 3.34  # meteor
  AGSS09_element_zfrac['br'] = 2.54  # meteor
  AGSS09_element_zfrac['kr'] = 3.25  # indirect
  AGSS09_element_zfrac['rb'] = 2.52
  AGSS09_element_zfrac['sr'] = 2.87
  AGSS09_element_zfrac['y'] = 2.21
  AGSS09_element_zfrac['zr'] = 2.58
  AGSS09_element_zfrac['nb'] = 1.46
  AGSS09_element_zfrac['mo'] = 1.88
  AGSS09_element_zfrac['ru'] = 1.75
  AGSS09_element_zfrac['rh'] = 0.91
  AGSS09_element_zfrac['pd'] = 1.57
  AGSS09_element_zfrac['ag'] = 0.94
  AGSS09_element_zfrac['cd'] = 1.71
  AGSS09_element_zfrac['in'] = 0.80
  AGSS09_element_zfrac['sn'] = 2.04
  AGSS09_element_zfrac['sb'] = 1.01
  AGSS09_element_zfrac['te'] = 2.18
  AGSS09_element_zfrac['i'] = 1.55
  AGSS09_element_zfrac['xe'] = 2.24
  AGSS09_element_zfrac['cs'] = 1.08
  AGSS09_element_zfrac['ba'] = 2.18
  AGSS09_element_zfrac['la'] = 1.10
  AGSS09_element_zfrac['ce'] = 1.58
  AGSS09_element_zfrac['pr'] = 0.72
  AGSS09_element_zfrac['nd'] = 1.42
  AGSS09_element_zfrac['sm'] = 0.96
  AGSS09_element_zfrac['eu'] = 0.52
  AGSS09_element_zfrac['gd'] = 1.07
  AGSS09_element_zfrac['tb'] = 0.30
  AGSS09_element_zfrac['dy'] = 1.10
  AGSS09_element_zfrac['ho'] = 0.48
  AGSS09_element_zfrac['er'] = 0.92
  AGSS09_element_zfrac['tm'] = 0.10
  AGSS09_element_zfrac['yb'] = 0.84
  AGSS09_element_zfrac['lu'] = 0.10
  AGSS09_element_zfrac['hf'] = 0.85
  AGSS09_element_zfrac['ta'] = -0.12
  AGSS09_element_zfrac['w'] = 0.85
  AGSS09_element_zfrac['re'] = 0.26
  AGSS09_element_zfrac['os'] = 1.40
  AGSS09_element_zfrac['ir'] = 1.38
  AGSS09_element_zfrac['pt'] = 1.62
  AGSS09_element_zfrac['au'] = 0.92
  AGSS09_element_zfrac['hg'] = 1.17
  AGSS09_element_zfrac['tl'] = 0.90
  AGSS09_element_zfrac['pb'] = 1.75
  AGSS09_element_zfrac['bi'] = 0.65
  AGSS09_element_zfrac['th'] = 0.02
  AGSS09_element_zfrac['u'] = -0.54

  if change_zfrac:
    n_changes = len(change_zfrac)
    # unpack the tuples:
    for tup in change_zfrac:
      key, value = tup
      if key not in AGSS09_element_zfrac.keys():
        print 'Warning: Abund.py: Asplund_09: Cannot modify %s. the element not in the list' % (key, )
        continue
      else:
        old_value = AGSS09_element_zfrac[key]
        print ' - Asplund_09: %s changed from %s to %s' % (key, old_value, value)
      AGSS09_element_zfrac[key] = value
         
  # convert to mass fractions
  sum_AGSS09_element_mass_frac = 0.0
  sum_AGSS09_metal_mass_frac = 0.0
  element_atomic_weight = gen_element_atomic_weight()
  Y_H = 1.0 - (Z_sun + Y_sun)/element_atomic_weight['h']
  for key, value in AGSS09_element_zfrac.items():
    if key == 'Ref': continue
    if key == 'Z_sun' or key == 'Y_sun' or key == 'X_sun': continue
    if key == 'h':
      AGSS09_element_mass_frac['h'] = X_sun
      sum_AGSS09_element_mass_frac += X_sun
      continue
    if key == 'he':
      AGSS09_element_mass_frac['he'] = Y_sun
      sum_AGSS09_element_mass_frac += Y_sun
      continue
    Y_i = Y_H * np.power(10.0, (AGSS09_element_zfrac[key] - 12.0))
    AGSS09_element_mass_frac[key] = Y_i * element_atomic_weight[key]
    sum_AGSS09_element_mass_frac += AGSS09_element_mass_frac[key]
    if key != 'h' and key != 'he':
      sum_AGSS09_metal_mass_frac += AGSS09_element_mass_frac[key]
    
  for key, value in AGSS09_element_mass_frac.items():
    # Distribute the left-overs over the whole elements from H, He to the end
    #if key == 'Ref' or key == 'Z_sun' or key == 'Y_sun': continue
    #AGSS09_element_mass_frac[key] = value / sum_AGSS09_element_mass_frac
    # OR
    # Distribute the left-overs ONLY over the metals excluding H and He
    if key == 'Ref' or key == 'Z_sun' or key == 'Y_sun' or key == 'X_sun' or key == 'h' or key == 'he' : continue
    AGSS09_element_mass_frac[key] = value / sum_AGSS09_metal_mass_frac * Z_sun

  sum_AGSS09_element_mass_frac = 0.0       # recheck for normalization
  for key, value in AGSS09_element_mass_frac.items():
    if key == 'Ref' or key == 'Z_sun' or key == 'Y_sun' or key == 'X_sun': continue
    sum_AGSS09_element_mass_frac += AGSS09_element_mass_frac[key]

  if report:
    print '\n - sum(AGSS09_element_mass_frac) = %s' % (sum_AGSS09_element_mass_frac, )
    print ' - AGSS09 X_sun = %s, Y_sun = %s, Z_sun = %s' % (AGSS09_element_mass_frac['h'], AGSS09_element_mass_frac['he'], sum_AGSS09_metal_mass_frac)
    print '   X_C/Z = ', AGSS09_element_mass_frac['c']/sum_AGSS09_metal_mass_frac
    print '   X_N/Z = ', AGSS09_element_mass_frac['n']/sum_AGSS09_metal_mass_frac
    print '   X_O/Z = ', AGSS09_element_mass_frac['o']/sum_AGSS09_metal_mass_frac
    print '   X_Ne/Z= ', AGSS09_element_mass_frac['ne']/sum_AGSS09_metal_mass_frac
    print
         
  return AGSS09_element_zfrac, AGSS09_element_mass_frac
         
         
#=================================================================================================================
def Asplund_05(report=True, change_zfrac=[], dic_XYZ=None):
  """
  This function sets the abundances in mass fraction and number fraction from Asplund, Grevesse and Sauval 2005 and       
  returns two dictionaries with such information.
  Both dictionaries share the same keys. The keys are:
   - Ref: giving the reference to the published table
   - Z_sun: The adopted Z_sun
   - Y_sun: The adopted Y_sun
   - h: for hydrogen
   - he: for helium
   - ... and so on 
   - the whole available element list can be retrieved by calling Abund.get_all_elem_names()
  @param report: To put on screen reports or not; default = False
  @type report: boolean
  @param change_zfrac: if you want to change abundances of specific elements, you pass this list of tuples. Each tuple
     contains the element name and the new abundance in number fraction (not mass!). 
     For example: change_zfrac = [('he', 10.99), ('ne', 8.11)]
  @type change_zfrac: list of tuples
  @param dic_XYZ: if you like to provide a different X, Y and Z than the default, this dictionary provides them through
     three different key value pairs: X, Y and Z
  @type dic_XYZ: dictionary
  @return: two dictionaries for the abundances in number fraction and mass fraction, respectively
  @rtype: OrderedDict
  """
  if not dic_XYZ:
    Z_sun = 0.0122
    Y_sun = 0.2485
    X_sun = 1.0 - (Y_sun + Z_sun)
  else:
    Z_sun = dic_XYZ['Z']
    Y_sun = dic_XYZ['Y']
    X_sun = dic_XYZ['X']
  
  AGS05_element_zfrac = OrderedDict({})
  AGS05_element_mass_frac = OrderedDict({})
  
  AGS05_element_zfrac['Ref'] = 'Asplund Grevesse and Sauval 2004'
  AGS05_element_mass_frac['Ref'] = 'Asplund Grevesse and Sauval 2004'
  
  AGS05_element_mass_frac['X_sun'] = X_sun
  AGS05_element_mass_frac['Y_sun'] = Y_sun
  AGS05_element_mass_frac['Z_sun'] = Z_sun

  AGS05_element_zfrac['h']  = 12.0
  AGS05_element_zfrac['he'] = 10.93  
  AGS05_element_zfrac['li'] = 3.25  #meteor
  AGS05_element_zfrac['be'] = 1.38
  AGS05_element_zfrac['b']  = 2.70
  AGS05_element_zfrac['c']  = 8.39
  AGS05_element_zfrac['n']  = 7.78
  AGS05_element_zfrac['o']  = 8.66
  AGS05_element_zfrac['f']  = 4.56
  AGS05_element_zfrac['ne'] = 7.84
  AGS05_element_zfrac['na'] = 6.17
  AGS05_element_zfrac['mg'] = 7.53
  AGS05_element_zfrac['al'] = 6.37
  AGS05_element_zfrac['si'] = 7.51
  AGS05_element_zfrac['p']  = 5.36
  AGS05_element_zfrac['s']  = 7.14
  AGS05_element_zfrac['cl'] = 5.50
  AGS05_element_zfrac['ar'] = 6.18
  AGS05_element_zfrac['k']  = 5.08
  AGS05_element_zfrac['ca'] = 6.31
  AGS05_element_zfrac['sc'] = 3.05
  AGS05_element_zfrac['ti'] = 4.90
  AGS05_element_zfrac['v']  = 4.00
  AGS05_element_zfrac['cr'] = 5.64
  AGS05_element_zfrac['mn'] = 5.39
  AGS05_element_zfrac['fe'] = 7.45
  AGS05_element_zfrac['co'] = 4.92
  AGS05_element_zfrac['ni'] = 6.23
  AGS05_element_zfrac['cu'] = 4.21
  AGS05_element_zfrac['zn'] = 4.60
  AGS05_element_zfrac['ga'] = 2.88
  AGS05_element_zfrac['ge'] = 3.58
  AGS05_element_zfrac['as'] = 2.29  #meteor
  AGS05_element_zfrac['se'] = 3.33  #meteor
  AGS05_element_zfrac['br'] = 2.56  #meteor
  AGS05_element_zfrac['kr'] = 3.28  #indirect
  AGS05_element_zfrac['rb'] = 2.60
  AGS05_element_zfrac['sr'] = 2.92
  AGS05_element_zfrac['y']  = 2.21
  AGS05_element_zfrac['zr'] = 2.59
  AGS05_element_zfrac['nb'] = 1.42
  AGS05_element_zfrac['mo'] = 1.92
  AGS05_element_zfrac['ru'] = 1.84
  AGS05_element_zfrac['rh'] = 1.12
  AGS05_element_zfrac['pd'] = 1.69
  AGS05_element_zfrac['ag'] = 0.94
  AGS05_element_zfrac['cd'] = 1.77
  AGS05_element_zfrac['in'] = 1.60
  AGS05_element_zfrac['sn'] = 2.00
  AGS05_element_zfrac['sb'] = 1.00
  AGS05_element_zfrac['te'] = 2.19
  AGS05_element_zfrac['i']  = 1.51
  AGS05_element_zfrac['xe'] = 2.27
  AGS05_element_zfrac['cs'] = 1.07
  AGS05_element_zfrac['ba'] = 2.17
  AGS05_element_zfrac['la'] = 1.13
  AGS05_element_zfrac['ce'] = 1.58
  AGS05_element_zfrac['pr'] = 0.71
  AGS05_element_zfrac['nd'] = 1.45
  AGS05_element_zfrac['sm'] = 1.01
  AGS05_element_zfrac['eu'] = 0.52
  AGS05_element_zfrac['gd'] = 1.12
  AGS05_element_zfrac['tb'] = 0.28
  AGS05_element_zfrac['dy'] = 1.14
  AGS05_element_zfrac['ho'] = 0.51
  AGS05_element_zfrac['er'] = 0.93
  AGS05_element_zfrac['tm'] = 0.00
  AGS05_element_zfrac['yb'] = 1.08
  AGS05_element_zfrac['lu'] = 0.06
  AGS05_element_zfrac['hf'] = 0.88
  AGS05_element_zfrac['ta'] = -0.17
  AGS05_element_zfrac['w']  = 1.11
  AGS05_element_zfrac['re'] = 0.23
  AGS05_element_zfrac['os'] = 1.45
  AGS05_element_zfrac['ir'] = 1.38
  AGS05_element_zfrac['pt'] = 1.64
  AGS05_element_zfrac['au'] = 1.01
  AGS05_element_zfrac['hg'] = 1.13
  AGS05_element_zfrac['tl'] = 0.90
  AGS05_element_zfrac['pb'] = 2.00
  AGS05_element_zfrac['bi'] = 0.65
  AGS05_element_zfrac['th'] = 0.06
  AGS05_element_zfrac['u']  = -0.52

  if change_zfrac:
    n_changes = len(change_zfrac)
    # unpack the tuples:
    for tup in change_zfrac:
      key, value = tup
      if key not in AGS05_element_zfrac.keys():
        print ' - Warning: Abund.py: Asplund_05: Cannot change %s. the element not in the list' % (key, )
        continue
      else:
        old_value = AGS05_element_zfrac[key]
        print ' - Asplund_09: %s changed from %s to %s' % (key, old_value, value)
      AGS05_element_zfrac[key] = value

  # convert to mass fractions
  sum_AGS05_element_mass_frac = 0.0
  sum_AGS05_metal_mass_frac = 0.0
  element_atomic_weight = gen_element_atomic_weight()
  Y_H = 1.0 - (Z_sun + Y_sun)/element_atomic_weight['h']
  for key, value in AGS05_element_zfrac.items():
    if key == 'Ref': continue
    if key == 'Z_sun' or key == 'Y_sun' or key == 'X_sun': continue
    if key == 'h':
      AGS05_element_mass_frac['h'] = X_sun
      sum_AGS05_element_mass_frac += X_sun
      continue
    if key == 'he':
      AGS05_element_mass_frac['he'] = Y_sun
      sum_AGS05_element_mass_frac += Y_sun
      continue
    Y_i = Y_H * np.power(10.0, (AGS05_element_zfrac[key] - 12.0))
    AGS05_element_mass_frac[key] = Y_i * element_atomic_weight[key]
    sum_AGS05_element_mass_frac += AGS05_element_mass_frac[key]
    if key != 'h' and key != 'he':
      sum_AGS05_metal_mass_frac += AGS05_element_mass_frac[key]

  for key, value in AGS05_element_mass_frac.items():
    # Distribute the left-overs over the whole elements from H, He to the end
    #if key == 'Ref' or key == 'Z_sun' or key == 'Y_sun': continue
    #AGS05_element_mass_frac[key] = value / sum_AGS05_element_mass_frac
    #
    # Distribute the left-overs ONLY over the metals excluding H and He
    if key == 'Ref' or key == 'Z_sun' or key == 'Y_sun' or key == 'X_sun' or key == 'h' or key == 'he': continue
    AGS05_element_mass_frac[key] = value / sum_AGS05_metal_mass_frac * Z_sun
    

  sum_AGS05_element_mass_frac = 0.0       # recheck for normalization
  for key, value in AGS05_element_mass_frac.items():
    if key == 'Ref' or key == 'Z_sun' or key == 'Y_sun' or key == 'X_sun': continue
    sum_AGS05_element_mass_frac += AGS05_element_mass_frac[key]

           
  if report:
    print '\n - sum(AGS05_element_mass_frac) = %s' % (sum_AGS05_element_mass_frac, )
    print ' - AGS05 X_sun = %s, Y_sun = %s, Z_sun = %s' % (AGS05_element_mass_frac['h'], AGS05_element_mass_frac['he'], sum_AGS05_metal_mass_frac)
    print '   X_C/Z  = ', AGS05_element_mass_frac['c']/sum_AGS05_metal_mass_frac
    print '   X_N/Z  = ', AGS05_element_mass_frac['n']/sum_AGS05_metal_mass_frac
    print '   X_O/Z  = ', AGS05_element_mass_frac['o']/sum_AGS05_metal_mass_frac
    print '   X_Ne/Z = ', AGS05_element_mass_frac['ne']/sum_AGS05_metal_mass_frac

  return AGS05_element_zfrac, AGS05_element_mass_frac         
  
#=================================================================================================================
def gen_element_atomic_weight():
  """
  """
  element_atomic_weight = OrderedDict({})
  element_atomic_weight['Ref'] = 'de Laeter et al Pure and Applied Chemistry 75 683 799 2003' 

  # periodic table, row 1
  element_atomic_weight['h'] = 1.00794
  element_atomic_weight['he'] = 4.002602

  #periodic table, row 2
  element_atomic_weight['li'] = 6.941
  element_atomic_weight['be'] = 9.012
  element_atomic_weight['b']  = 10.811
  element_atomic_weight['c']  = 12.0107
  element_atomic_weight['n']  = 14.0067
  element_atomic_weight['o']  = 15.9994
  element_atomic_weight['f']  = 18.9984032
  element_atomic_weight['ne'] = 20.1797

  # periodic table, row 3
  element_atomic_weight['na'] = 22.989770
  element_atomic_weight['mg'] = 24.3050
  element_atomic_weight['al'] = 26.981538
  element_atomic_weight['si'] = 28.0855
  element_atomic_weight['p']  = 30.973761
  element_atomic_weight['s']  = 32.065
  element_atomic_weight['cl'] = 35.453
  element_atomic_weight['ar'] = 39.948

  # periodic table, row 4
  element_atomic_weight['k']  = 39.0983
  element_atomic_weight['ca'] = 40.078
  element_atomic_weight['sc'] = 44.955910
  element_atomic_weight['ti'] = 47.867
  element_atomic_weight['v']  = 50.9415
  element_atomic_weight['cr'] = 51.9961
  element_atomic_weight['mn'] = 54.938049
  element_atomic_weight['fe'] = 55.845
  element_atomic_weight['co'] = 58.933200
  element_atomic_weight['ni'] = 58.6934
  element_atomic_weight['cu'] = 63.546
  element_atomic_weight['zn'] = 65.409
  element_atomic_weight['ga'] = 69.723
  element_atomic_weight['ge'] = 72.64
  element_atomic_weight['as'] = 74.921
  element_atomic_weight['se'] = 78.96
  element_atomic_weight['br'] = 79.904
  element_atomic_weight['kr'] = 83.798

  # periodic table, row 5
  element_atomic_weight['rb'] = 85.4678
  element_atomic_weight['sr'] = 87.62
  element_atomic_weight['y'] =  88.905
  element_atomic_weight['zr'] = 91.224
  element_atomic_weight['nb'] = 92.906
  element_atomic_weight['mo'] = 95.94
  element_atomic_weight['tc'] = 97.9072
  element_atomic_weight['ru'] = 101.07
  element_atomic_weight['rh'] = 102.905
  element_atomic_weight['pd'] = 106.42
  element_atomic_weight['ag'] = 107.8682
  element_atomic_weight['cd'] = 112.411
  element_atomic_weight['in'] = 114.818
  element_atomic_weight['sn'] = 118.710
  element_atomic_weight['sb'] = 121.760
  element_atomic_weight['te'] = 127.60
  element_atomic_weight['i']  = 126.904
  element_atomic_weight['xe'] = 131.293

  # periodic table, row 6
  element_atomic_weight['cs'] = 132.905
  element_atomic_weight['ba'] = 137.327
  element_atomic_weight['la'] = 138.9055
  element_atomic_weight['ce'] = 140.115
  element_atomic_weight['pr'] = 140.90765
  element_atomic_weight['nd'] = 144.24
  element_atomic_weight['pm'] = 144.9127
  element_atomic_weight['sm'] = 150.36
  element_atomic_weight['eu'] = 151.965
  element_atomic_weight['gd'] = 157.25
  element_atomic_weight['tb'] = 158.92534
  element_atomic_weight['dy'] = 162.50
  element_atomic_weight['ho'] = 164.93032
  element_atomic_weight['er'] = 167.26
  element_atomic_weight['tm'] = 168.93421
  element_atomic_weight['yb'] = 173.04
  element_atomic_weight['lu'] = 174.967
  element_atomic_weight['hf'] = 178.49
  element_atomic_weight['ta'] = 180.9479
  element_atomic_weight['w']  = 183.84
  element_atomic_weight['re'] = 186.207
  element_atomic_weight['os'] = 190.23
  element_atomic_weight['ir'] = 192.22
  element_atomic_weight['pt'] = 195.08
  element_atomic_weight['au'] = 196.96654
  element_atomic_weight['hg'] = 200.59
  element_atomic_weight['tl'] = 204.3833
  element_atomic_weight['pb'] = 207.2
  element_atomic_weight['bi'] = 208.98037
  element_atomic_weight['po'] = 208.9824   # where did this come from?
  element_atomic_weight['at'] = 209.9871   # where did this come from?
  element_atomic_weight['th'] = 232.0381
  element_atomic_weight['pa'] = 231.03588
  element_atomic_weight['u']  = 238.02891
  
  return element_atomic_weight
#=================================================================================================================
def get_all_elem_names():
  """
  This function returns a list of names for which the elemental atomic weight is available, and their corresponding
  abundances might have been listed by Asplund_05() or Asplund_09().
  @param : None
  @type  : None
  @return: list of strings giving the element names, e.g. ['h', 'he', ..., 'u']
  @rtype: list of strings
  """
  
  dic = gen_element_atomic_weight()
  Ref = dic.pop('Ref')
  
  return dic
  
#=================================================================================================================
#=================================================================================================================

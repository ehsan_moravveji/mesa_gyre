program opmix
  implicit none
  integer, parameter :: no=25
  type :: atoms
    character(len=2), dimension(no) :: label
    integer, dimension(no) :: atno
    real(kind=kind(1d0)), dimension(no) :: atmass    
    real(kind=kind(1d0)), dimension(no) :: logabn    
  end type atoms
  type(atoms) :: at
  integer :: i
  real(kind=kind(1d0)) :: all,MM,NN

!atomic masses (not necessary for Sc, V and Co as redistribution is not computed for these elements
  at%atmass=[1.00790,4.00260,12.01100,14.00670,15.99940,20.17900,22.98977,&
             24.30500,26.98154,28.08550,30.97376,32.06000,35.45300,39.94800,&
             39.09830,40.08000,47.90000,51.99600,54.93800,55.84700,58.70000,&
             18.99840,1.,1.,1.]
  at%label=[" H","He"," C"," N"," O","Ne","Na","Mg","Al","Si"," P"," S",&
            "Cl","Ar"," K","Ca","Ti","Cr","Mn","Fe","Ni",&
            " F","Sc"," V","Co"]
  
!gn93 logarithmic abundances
  at%logabn=[12.00d0,10.99d0,8.55d0,7.97d0,8.87d0,8.08d0,6.33d0,7.58d0,&
             6.47d0,7.55d0,5.45d0,7.21d0,5.50d0,6.52d0,5.12d0,6.36d0,&
             5.02d0,5.67d0,5.39d0,7.50d0,6.25d0,&
             4.56d0,3.17d0,4.00d0,4.92d0]

   all=0d0
   do i=3,25
      all=all+10d0**(at%logabn(i)-12d0)
   end do

!This loop reproduces column two Tab.1 of IR96
   do i=3,25
      write(*,fmt='(A2,tr2,F8.6)')at%label(i),10d0**(at%logabn(i)-12d0)/all
   end do

!reduction to 19 metals F, Sc, V and Co go to heighbouring elements. Example:
!F goes to O and Ne, such that molecular weight and particle number are conserved
   MM=10d0**(at%logabn(5)-12d0)*at%atmass(5)+&
      10d0**(at%logabn(6)-12d0)*at%atmass(6)+&
      10d0**(at%logabn(22)-12d0)*at%atmass(22)
   NN=10d0**(at%logabn(5)-12d0)+&
      10d0**(at%logabn(6)-12d0)+&
      10d0**(at%logabn(22)-12d0)
   print *,"Ne, redist.",(MM-NN*at%atmass(5))/(at%atmass(6)-at%atmass(5))/all
   print *,"O, redistr.",(NN-(MM-NN*at%atmass(5))/(at%atmass(6)-at%atmass(5)))/all
!In analogous way for Ca, Ti, Cr, Fe and Ni


end program opmix

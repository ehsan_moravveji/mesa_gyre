#! /usr/bin/python

import abund_lib
#list_elem_op = ['h', 'he', 'li', 'be', 'b', 'c', 'n', 'o', 'f', 'ne', 'na', 'mg', 'al', 'si', 'p', 's', 'cl',
                #'ar', 'k', 'ca', 'ti', 'cr', 'mn', 'fe', 'ni']

list_elem_names = abund_lib.get_all_elem_names()

przybilla_he = ('he', 10.99)
przybilla_c  = ('c', 8.33)
przybilla_n  = ('n', 7.79)
przybilla_o  = ('o', 8.76)
przybilla_ne = ('ne', 8.09)
przybilla_mg = ('mg', 7.56)
przybilla_al = ('al', 6.30)
przybilla_si = ('si', 7.50)
przybilla_s  = ('s', 7.14)
przybilla_ar = ('ar', 6.50)
przybilla_fe = ('fe', 7.52)
przybilla_list = [przybilla_he, przybilla_c, przybilla_n, przybilla_o, przybilla_ne, przybilla_mg, przybilla_al,
                  przybilla_si, przybilla_s, przybilla_ar, przybilla_fe]

cunha_ne = ('ne', 8.11)
just_ne = [cunha_ne]

change_nothing = []

## To produce the pure Asplund_05:
#abund_lib.write_abund(abund_lib.Asplund_05, list_elem=list_elem_names, change_zfrac = change_nothing, file_out='A05.abund')

## To produce Asplund_05 + Ne (taken from Cunha et al. 2006):
abund_lib.write_abund(abund_lib.Asplund_05, list_elem=list_elem_names, change_zfrac = just_ne, file_out='A05+Ne.abund')

## To produce the pure Asplund_09:
#abund_lib.write_abund(abund_lib.Asplund_09, list_elem = list_elem_names, change_zfrac=change_nothing, file_out='A09.abund')

## To produce Asplund_09 + Ne (taken from Cunha et al. 2006):
#abund_lib.write_abund(abund_lib.Asplund_09, list_elem = list_elem_names, change_zfrac=just_ne, file_out='A09+Ne.abund')

# To produce Przybilla et al. (2013; proceeding) abundances with a basis on Asplund_09:
dic_XYZ = {'X':0.710, 'Y':0.276, 'Z':0.014}
#abund_lib.write_abund(abund_lib.Asplund_09, list_elem = list_elem_names, change_zfrac=przybilla_list, dic_XYZ=dic_XYZ,
                  #file_out='A09+Przybilla.abund')

# To generate an ascii file with the grid for OP server:
#abund_lib.gen_OP_table_grid()

# To print out OP-compatible metal mass fractions:
dic_XYZ = {'X':0.710, 'Y':0.276, 'Z':0.014}
abund_lib.give_OP_X_Z_list(func=abund_lib.Asplund_09, change_zfrac=przybilla_list, dic_XYZ=dic_XYZ)
#abund_lib.give_OP_X_Z_list(func=abund_lib.Asplund_05, change_zfrac=just_ne)

# To store the z_frac, m_frac and m_frac/Z for three compositions as a LaTeX compatible table:
#abund_lib.gen_TeX_table_abund()



.. _readme:

******************************************************
Solver for Partial Differential Equations (pde_solver)
******************************************************

Introduction 
============

.. _intro:

This module provides four solvers for solving basic (textbook examples) partial differential
equations using Python, Numpy and Scipy. A great introduction to the topic of PDEs are given 
in the *Numerical Solution of Partial Differential Equations; An Introduction* by K. W. Morton,
and D. F. Mayers. The book is available through this link `book`_. By a simple googling, seems
like some chinese dude have put the book online for a `direct download`_. 

.. _book: https://www.amazon.com/Numerical-Solution-Partial-Differential-Equations/dp/0521607930
.. _direct download: http://mathcenter.hust.edu.cn/Uploads/file/20160128/56a98740d59f8.pdf

The parabolic PDEs have the following general form (perhaps, some of the math formulae below
do not render well)

.. math:: 
   \frac{\partial u}{\partial t} = b(x,t) \frac{\partial^2 u}{\partial x^2} 
   -a(x,t) \frac{\partial u}{\partial x} + c(x,t) u + d(x,t).
   :label: parabolic

In Eq. :eq:`parabolic` :math:`b=b(x,t)` is the diffusion coefficient (and must be positive values),
:math:`a=a(x,t)` is the advection term, :math:`c=c(x,t)` is the reaction term, and :math:`d=d(x,t)`
is the source term. For a definite problem, the set of PDEs must be coupled with proper boundary
conditions and an initial value over the domain of solution.

The following four schemes for the solution of parabolic problems are currently supported in 
the pde_solver module:
 
 - Explicit Euler method,
 - Implicit scheme,
 - The :math:`theta` method,
 - The Crank-Nicolson method.

Module Contents
===============

The module contains the :file:`lib.py` for the import of basic functinalities, and :file:`plotter.py`
for basic plotting of the results. Furthermore, two example files are provided to demonstrate how to
use the module in practice; they are :file:`example-explicit.py` and :file:`example-implicit.py`.

Documentation
=============

The :file:`lib.py` is briefly documented. Below, we give a general example on how to solve a simple PDE.

::
  >>> from pde_solvers import lib, plotter
  >>>
  >>> def bc0(t):      # left boundary condition
  >>>   return 0.0
  >>> def bc1(t):      # right boundary condition
  >>>   return 1.0
  >>> def ic(x):       # initial condition at t=0, and for all x
  >>>   return np.sin(2.5 * np.pi * x)
  >>>
  >>> explicit = lib.problem(scheme='explicit', bc_left=bc0, bc_right=bc1, ic=gx,
                          x0=0, x1=1, dx=0.0.05, t0=0, t1=0.5, dt=0.00125)
  >>>                          
  >>> explicit.integrate()



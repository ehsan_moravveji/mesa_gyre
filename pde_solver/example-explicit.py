
import sys, os, glob
import logging
import numpy as np 
import lib, plotter

def bc0(t):
  return 0.0

def bc1(t):
  return 1.0

def gx(x):
  return np.sin(2.5 * np.pi * x)

x0 = 0.0
x1 = 1.0
dx = 1.0 / 20.
t0 = 0.0
t1 = 0.5
dt = 0.00125

explicit = lib.problem(scheme='explicit', bc_left=bc0, bc_right=bc1, ic=gx,
                          x0=x0, x1=x1, dx=dx, t0=t0, t1=t1, dt=dt)
explicit.integrate()

if True:
  plotter.plot_solution(explicit, file_out='explicit.png')

if True:
  plotter.plot_solution_selected_times(explicit, which_times=[0, 0.05, 0.5], file_out='explicit-few.png')

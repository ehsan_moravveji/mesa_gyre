
import sys, os, glob
import logging
import numpy as np 
try:
  from scipy.linalg import solve_banded
except:
  raise SystemExit, 'try installing scipy.'

class problem:

  """
  The problem class object.
  For any problem, the object can be easily instantiated by the following call
  
  >>> from pde_solvers import lib, plotter
  >>>
  >>> def bc0(t):      # left boundary condition
  >>>   return 0.0
  >>> def bc1(t):      # right boundary condition
  >>>   return 1.0
  >>> def ic(x):       # initial condition at t=0, and for all x
  >>>   return np.sin(2.5 * np.pi * x)
  >>>
  >>> explicit = lib.problem(scheme='explicit', bc_left=bc0, bc_right=bc1, ic=gx,
                          x0=0, x1=1, dx=0.0.05, t0=0, t1=0.5, dt=0.00125)
  >>>                          
  >>> explicit.integrate()

  You can pass the class object, e.g. "explicit" here, to the plotting routines supplied
  in the "plotter" module. E.g.

  >>> plotter.plot_solution_selected_times(explicit, which_times=[0, 0.05, 0.5], file_out='explicit.png')

  Below, we define the meaning of different parameters that are used to initialize the "problem" class.

  @param scheme: Specify which scheme to use for integration. The current supported schemes are:
                 - Euler explicit
                 - Implicit
                 - Theta method
                 - Crank-Nicolson method (subset of the theta method, with theta=0.5)
  @type scheme: str; default=None
  @param bc_left: callable, specifies the boundary condition on the left domain as a function of time, t
  @type bc_left: func; default=None
  @param bc_right: callable, specifies the boundary condition on the right domain as a function of time, t
  @type bc_left: func; default=None
  @param ic: callable, specifies the initial condition at t=0, and for all x across the domain
  @type ic: func; default=None  
  param x0, x1, dx: the x-coordinate of the left and right domain boundaries, in addition to the uniform mesh 
  @type x0, x1, dx: float; default=None 
  param t0, t1, dt: the initial and end times for the solution, in addition to the uniform timestep
  @type t0, t1, dt: float; default=None
  """

  def __init__(self,
               scheme=None,
               bc_left=None, bc_right=None,
               ic=None,
               x0=None, x1=None, dx=None,
               t0=None, t1=None, dt=None):
    self.scheme = scheme          # implicit, explicit, Crank-Nicolson
    self.bc_left = bc_left        # function of t: f1(x0,t)
    self.bc_right = bc_right      # function of t: f2(x1,t)
    self.ic = ic                  # function of x: g(x,t=0)
    self.x0 = x0
    self.x1 = x1
    self.dx = dx
    self.t0 = t0
    self.t1 = t1
    self.dt = dt

    self.nx = 0
    self.nt = 0
    self.x  = 0
    self.t  = 0

    self.mu = 0

    self.theta = -1                # Explicit Euler (0), implicit (1) and Crank-Nicolson (0.5)

    self.solution = None          # matrix of shape: (nt, nx)


  def set_nx(self, nx):
    self.nx = nx 
  
  def set_nt(self, nt):
    self.nt = nt

  def set_x(self, x):
    self.x = x 

  def set_t(self, t):
    self.t = t

  def set_mu(self, mu):
    self.mu = mu

  def set_theta(self, theta):
    self.theta = theta

  def set_solution(self, solution):
    self.solution = solution

  def print_info(self):
    print 'scheme = {0}'.format(self.scheme)
    print 'x0={0:.4f}, x1={1:.4f}, dx={2:.4f}, nx={3}'.format(self.x0, self.x1, self.dx, self.nx)
    print 't0={0:.4f}, t1={1:.4f}, dt={2:.4f}, nt={3}'.format(self.t0, self.t1, self.dt, self.nt)
    print 'mu={0:.4f}'.format(self.mu)

  def integrate_explicit(self):
    self.set_theta(0)
    nx = self.nx 
    x  = self.x
    nt = self.nt
    t  = self.t

    mu = self.mu

    sol= self.solution

    for it in range(nt):
      U    = np.zeros(nx) 
      U[0] = self.bc_left(t[it])
      U[nx-1] = self.bc_right(t[it])

      if it == 0:  # use initial condition
        U[1:nx-1] = self.ic(x)[1:nx-1]
      else:
        Un = sol[it-1, 0:nx]
        U[1:nx-1] = mu * Un[0:nx-2] + (1.0-2*mu) * Un[1:nx-1] + mu * Un[2:nx]

      sol[it, 0:nx] = U[0:nx]

    self.set_solution(sol)

  def integrate_implicit(self):
    self.set_theta(1)
    nx = self.nx 
    x  = self.x
    nt = self.nt
    t  = self.t

    mu = self.mu
    ic = self.ic(x)
    sol= self.solution

    # construct coefficient matrices of A u = B
    m     = nx - 2
    A     = np.zeros((3, m))
    A[0]  = -mu
    A[1]  = 1 + 2.0 * mu
    A[2]  = -mu

    for it in range(nt):
  
      if it == 0:
        Z   = self.ic 
        sol[0,:] = ic 
        continue
      else:
        Z   = sol[it-1, :]

      B     = np.zeros(m)
      B[:]  = Z[1:nx-1]
      B[0]  += mu * Z[0]
      B[-1] += mu * Z[-1]

      res   = solve_banded((1, 1), A, B, check_finite=True)
      sol[it,1:nx-1] = res[:]
      sol[it,0]  = self.bc_left(t[it])
      sol[it,-1] = self.bc_right(t[it])
    
    self.set_solution(sol)

  def integrate_theta_method(self):
    theta = self.theta 
    if theta < 0:
      print 'lib: problem: integrate_theta_method: first do self.set_theta(theta)'
      logging.error('lib: problem: integrate_theta_method: self.theta not set')
      return False
    elif theta > 1:
      print 'lib: problem: integrate_theta_method: theta cannot exceed unity!'
      logging.error('lib: problem: integrate_theta_method: theta cannot exceed unity!')
      return False
    th = theta  
    nx = self.nx 
    x  = self.x
    nt = self.nt
    t  = self.t

    mu = self.mu
    ic = self.ic(x)
    sol= self.solution

    # construct coefficient matrices of A u = B
    m     = nx - 2
    A     = np.zeros((3, m))
    A[0]  = -mu * th
    A[1]  = 1 + 2.0 * mu * th
    A[2]  = -mu * th

    for it in range(nt):
  
      if it == 0:
        U   = self.ic 
        sol[0,:] = ic 
        continue
      else:
        U   = sol[it-1, :]

      B     = np.zeros(m)
      B[0:m]= (1 - th) * mu * U[0:m] + \
              (1 - 2 * mu * (1 - th)) * U[1:m+1] + \
              (1 - th) * mu * U[2:m+2] 
      BC0   = self.bc_left(t[it])
      BC1   = self.bc_right(t[it])
      B[0]  += th * mu * BC0
      B[-1] += th * mu * BC1

      res   = solve_banded((1, 1), A, B, check_finite=True)
      sol[it,1:nx-1] = res[:]
      sol[it,0]      = BC0
      sol[it,-1]     = BC1

  def integrate_crank_nicolson(self):
    self.set_theta(0.5)
    self.integrate_theta_method()


  def integrate(self):
    scheme = self.scheme
    if scheme not in ['implicit', 'explicit', 'theta-method', 'crank-nicolson']:
      logging.error('lib: problem: integrate: the requested scheme not supported')
      print 'Error: lib: problem: integrate: the requested scheme not supported'
      print 'The supported schemes are: implicit, explicit, crank-nicolson'
      raise SystemExit

    if self.bc_left is None:
      raise SystemExit, 'Error: lib: problem: integrate: bc_left function not set yet'

    if self.bc_right is None:
      raise SystemExit, 'Error: lib: problem: integrate: bc_right function not set yet'

    if self.ic is None:
      raise SystemExit, 'Error: lib: problem: integrate: ic function not set yet'

    if self.x0 is None:
      raise SystemExit, 'Error: lib: problem: integrate: x0 not set yet'

    if self.x1 is None:
      raise SystemExit, 'Error: lib: problem: integrate: x1 not set yet'

    if self.dx is None:
      raise SystemExit, 'Error: lib: problem: integrate: dx not set yet'

    if self.t0 is None:
      raise SystemExit, 'Error: lib: problem: integrate: t0 not set yet'

    if self.t1 is None:
      raise SystemExit, 'Error: lib: problem: integrate: t1 not set yet'

    if self.dt is None:
      raise SystemExit, 'Error: lib: problem: integrate: dt not set yet'

    if self.nx == 0:
      nx = int((self.x1 - self.x0) / self.dx + 1)
      self.set_nx(nx)
      x  = np.linspace(self.x0, self.x1, self.nx, endpoint=True)
      self.set_x(x)

    if self.nt == 0:
      nt = int((self.t1 - self.t0) / self.dt + 1)
      self.set_nt(nt)
      t  = np.linspace(self.t0, self.t1, self.nt, endpoint=True)
      self.set_t(t)

    if self.mu == 0:
      mu = self.dt / np.power(self.dx, 2)
      self.set_mu(mu)

    # prepare the empty solution matrix
    if self.solution is None:
      solution = np.empty((nt, nx), dtype=float)
      self.solution = solution
    else:
      pass

    # Get the global info
    self.print_info()

    # call the proper integrator, based on the requested scheme
    if scheme == 'explicit':
      self.integrate_explicit()
    elif scheme == 'implicit':
      self.integrate_implicit()
    elif scheme == 'theta-method':
      self.integrate_theta_method()
    elif scheme == 'crank-nicolson':
      self.integrate_crank_nicolson()
    else:
      raise SystemExit, 'Error: lib: problem: integrate: Unexpected error occured'




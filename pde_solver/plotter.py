import sys, os, glob
import logging
import numpy as np 
import pylab as plt

###############################################################################
def plot_solution(self, file_out=None):
  if file_out is None: return False 

  fig, ax = plt.subplots(1, figsize=(4, 8))
  plt.subplots_adjust(left=0.14, right=0.98, bottom=0.06, top=0.98)

  x = self.x
  t = self.t
  nx = self.nx 
  nt = self.nt
  solution = self.solution 

  for it in range(nt):
    U = solution[it, 0:nx]
    plt.plot(x, U, marker='o', ms=1)

  ax.set_xlabel(r'$x_j$')
  ax.set_ylabel(r'$U_j^n$')
  ax.set_xlim(self.x0-self.dx/2., self.x1+self.dx/2.)
  ax.set_ylim(np.min(self.solution), np.max(solution)*1.05)

  txt = 'Scheme={0}\n'.format(self.scheme) + r'$\mu$={0:.4f}'.format(self.mu)
  ax.annotate(txt, xy=(0.03, 0.94), xycoords='axes fraction', fontsize=12)

  plt.savefig(file_out, transparent=False)
  print ' - plotter: plot_solution: saved {0}'.format(file_out)
  plt.close()

  return None 

###############################################################################
def plot_solution_selected_times(self, which_times=[], file_out=None):
  if file_out is None: return False 
  if not which_times: return False

  fig, ax = plt.subplots(1, figsize=(4, 8))
  plt.subplots_adjust(left=0.14, right=0.98, bottom=0.06, top=0.98)

  x = self.x
  t = self.t
  nx = self.nx 
  nt = self.nt
  solution = self.solution 
  ind_time = []
  for time in which_times:
    ind = np.argmin(np.abs(t - time))
    ind_time.append(ind)

  for ind in ind_time:
    U = solution[ind, 0:nx]
    offset = t[ind]
    y = U + offset
    plt.plot(x, U, marker='o', ms=1)

  ax.set_xlabel(r'$x_j$')
  ax.set_ylabel(r'$U_j^n$')
  ax.set_xlim(self.x0-self.dx/2., self.x1+self.dx/2.)
  ax.set_ylim(np.min(self.solution), np.max(solution)*1.05)

  txt = 'Scheme={0}\n'.format(self.scheme) + r'$\mu$={0:.4f}'.format(self.mu)
  ax.annotate(txt, xy=(0.03, 0.94), xycoords='axes fraction', fontsize=12)

  plt.savefig(file_out, transparent=False)
  print ' - plotter: plot_solution: saved {0}'.format(file_out)
  plt.close()

  return None 

###############################################################################



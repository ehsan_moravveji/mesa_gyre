__version__ = 1
__author__ = 'Ehsan Moravveji'
__affiliation__ = 'KU Leuven'
__date__ = '19 Nov 2016'
__all__ = ['lib', 'plotter']
from pde_solver import lib
from pde_solver import plotter
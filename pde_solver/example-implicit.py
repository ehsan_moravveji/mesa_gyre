
import sys, os, glob
import logging
import numpy as np 
import lib, plotter

def bc0(t):
  return 0.0

def bc1(t):
  return 0.0

def gx(x):
  return np.sin(np.pi * x)

x0 = 0.0
x1 = 1.0
dx = 1.0 / 20.
t0 = 0.0
t1 = 0.5
dt = 0.00125

if True:
  print 'The Implicit Scheme:'
  implicit = lib.problem(scheme='implicit', bc_left=bc0, bc_right=bc1, ic=gx,
                            x0=x0, x1=x1, dx=dx, t0=t0, t1=t1, dt=dt)
  implicit.integrate()

  if True:
    plotter.plot_solution(implicit, file_out='implicit.png')

  if True:
    plotter.plot_solution_selected_times(implicit, which_times=[0, 0.05, 0.5], file_out='implicit-few.png')


print 
if True:  
  print 'The Crank-Nicolson Scheme:'
  crank_nicolson = lib.problem(scheme='crank-nicolson', bc_left=bc0, bc_right=bc1, ic=gx,
                               x0=x0, x1=x1, dx=dx, t0=t0, t1=t1, dt=dt)
  crank_nicolson.integrate()

  if True:
    plotter.plot_solution(crank_nicolson, file_out='CN.png')

  if True:
    plotter.plot_solution_selected_times(crank_nicolson, which_times=[0, 0.05, 0.5], file_out='CN-few.png')


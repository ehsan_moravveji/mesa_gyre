
import numpy as np 

#########################################################################################
# Define angles, pi and different useful fractions of pi
pi    = np.pi      # 180
pi_2  = pi / 2.0   # 90
pi_3  = pi / 3.0   # 60
pi_4  = pi / 4.0   # 45
pi_6  = pi / 6.0   # 30

pi_32 = 3.0 * pi_2
pi_34 = 3.0 * pi_4
pi_56 = pi - pi_6
pi_76 = pi + pi_6
pi_116= 2.0 * pi - pi_6

#########################################################################################
# convert between degrees and radians, and the other way around
rad_to_deg = 180.0 / pi
deg_to_rad = pi / 180.0

#########################################################################################

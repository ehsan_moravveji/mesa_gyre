
import logging
import numpy as np 

import objects_def as odef 

#########################################################################################
#########################################################################################
#########################################################################################
def gen_hexagon_layers(n_layers):
  """
  Starting from inner hexagon outwards, we pile up layers and layers of hexagons around
  the central hexagon. The center hexagon has layer=0, and is a unique one.
  @param n_layers: number of desired hexagon layers, must be greater than or equal to zero.
         The zero layer corresponds to the central hexagon
  @tyep n_layers: integer
  @return: list of hexagons, each having a unique id, and with the layer attribute already
         assigned.
  @rtype: list
  """
  if n_layers < 0:
    logging.error('gen_hexagon_layers: n_layers cannot be negative')
    raise SystemExit, 'gen_hexagon_layers: n_layers cannot be negative'

  center = odef.hexagon(id=0)
  center.set_layer(0)

  if n_layers == 0: return [center]

  mesh = [center]
  hid  = 1

  for layer in range(1, n_layers + 1):
    num_hexagons = 6 * layer
    this_layer   = []
    for k in range(1, num_hexagons+1):
      this = odef.hexagon(id=hid)
      hid  += 1
      this.set_layer(layer)
      this_layer.append(this)
    mesh   += this_layer

  return mesh

#########################################################################################
def gen_origin():
  """
  The origin of the grid is an object of "particle" class, with a unique particle id=0.
  No other particle in the mesh can have this id number! Else, we will end up with 
  multiple origin points, which is not really desirable ;-)
  """
  return odef.particle(id=0)

#########################################################################################

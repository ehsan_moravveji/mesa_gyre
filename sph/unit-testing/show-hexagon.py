
import sys, os, glob
import logging
import numpy as np 
from sph import objects_def, objects_plotter


#########################################################################################
def main():
  
  if True:
    print ' - plot one hexagon as a test'
    hexagon = objects_def.hexagon(id=1)
    objects_plotter.show_one_hexagon(hexagon, file_out='plots/Hexagon.png')

  return True

#########################################################################################
if __name__ == '__main__':
  stat = main()
  sys.exit(stat)
#########################################################################################


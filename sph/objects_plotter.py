
import logging
import numpy as np 
import pylab as plt

import constants_def as cdef
import objects_def as odef

#########################################################################################
rad_to_deg = cdef.rad_to_deg
deg_to_rad = cdef.deg_to_rad

#########################################################################################
#########################################################################################
#########################################################################################
def show_mesh_hexagon(self, file_out):
  """
  Plot
  """
#########################################################################################
def show_one_hexagon(self, file_out):
  """
  Plot one hexagon, which is an object of the "hexagon" class, defined in objects_def module.
  """
  vertices   = self.vertices
  list_r     = np.array([v.r for v in vertices])
  list_theta = np.array([v.theta for v in vertices])
  list_phi   = np.array([v.phi   for v in vertices])

  fig, ax    = plt.subplots(1, figsize=(4, 4), subplot_kw=dict(polar=True))
  fig.subplots_adjust(left=0.01, right=0.99, bottom=0.01, top=0.99)

  ax.scatter(list_phi, list_r, marker='o', c='grey', edgecolors='red', s=10)
  ax.plot(list_phi, list_r, color='grey')

  plt.savefig(file_out)
  print ' - show_one_hexagon: saved {0}'.format(file_out)
  plt.close()

  return None

#########################################################################################

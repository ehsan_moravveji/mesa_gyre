
import numpy as np 
import constants_def 

pi    = constants_def.pi
pi_2  = constants_def.pi_2
pi_3  = constants_def.pi_3
pi_4  = constants_def.pi_4
pi_6  = constants_def.pi_6

pi_32 = constants_def.pi_32
pi_34 = constants_def.pi_34
pi_56 = constants_def.pi_56
pi_76 = constants_def.pi_76
pi_116= constants_def.pi_116


#########################################################################################

# + + + + + + + + + + + + +
#     P A R T I C L E     #
# + + + + + + + + + + + + +
class particle:

  ############################
  def __init__(self, id):
    """
    Particle constructor
    """
    self.id = id  # mandatory input, unique for each particle

    # mass & density
    self.mass = 0.
    self.rho  = 0.

    # coordinates
    self.r = 0.
    self.theta = 0.
    self.phi = 0.
    self.x = 0.
    self.y = 0.
    self.z = 0.

    # velocities
    self.v_r = 0.
    self.v_theta = 0.
    self.v_phi = 0.
    self.v_x = 0.
    self.v_y = 0.
    self.v_z = 0.

    # momentum
    self.p_r = self.mass * self.v_r
    self.p_theta = self.mass * self.v_theta
    self.p_phi = self.mass * self.v_phi
    self.p_x = self.mass * self.v_x
    self.p_y = self.mass * self.v_y
    self.p_z = self.mass * self.v_z

    # accelerations
    self.a_r = 0.
    self.a_theta = 0.
    self.a_phi = 0.
    self.a_x = 0.
    self.a_y = 0.
    self.a_z = 0.

    # forces
    self.f_r = self.mass * self.a_r
    self.f_theta = self.mass * self.a_theta
    self.f_phi = self.mass * self.a_phi
    self.f_x = self.mass * self.a_x
    self.f_y = self.mass * self.a_y
    self.f_z = self.mass * self.a_z


  ############################
  #      G E T T E R S
  ############################
  def get_id(self):
    return self.id 

  ############################
  def get_mass(self):
    return self.mass 

  ############################
  def get_rho(self):
    return self.rho

  ############################
  def get_r(self):
    return self.r 

  ############################
  def get_theta(self):
    return self.theta

  ############################
  def get_phi(self):
    return self.phi

  ############################
  def get_all_other_vars(self):
    return False

  ############################


  ############################
  #      S E T T E R S
  ############################
  def set_mass(self, mass): 
    self.mass = mass

  ############################
  def set_rho(self, rho):
    self.rho = rho 

  ############################
  def set_r(self, r):
    self.r = r

  ############################
  def set_theta(self, theta):
    self.theta = theta 

  ############################
  def set_phi(self, phi):
    self.phi = phi 

  ############################
  def set_x(self, x):
    self.x = x 

  ############################
  def set_y(self, y):
    self.y = y 

  ############################
  def set_z(self, z):
    self.z = z

  ############################
  def set_v_r(self, v_r):
    self.v_r = v_r
    self.p_r = self.mass * v_r

  ############################
  def set_v_theta(self, v_theta):
    self.v_theta = v_theta
    self.p_theta = self.mass * v_theta

  ############################
  def set_v_phi(self, v_phi):
    self.v_phi = v_phi
    self.p_phi = self.mass * v_phi

  ############################
  def set_v_x(self, v_x):
    self.v_x = v_x
    self.p_x = self.mass * v_x

  ############################
  def set_v_y(self, v_y):
    self.v_y = v_y
    self.p_y = self.mass * v_y

  ############################
  def set_v_z(set, v_z):
    self.v_z = v_z
    self.p_z = self.mass * v_z

  ############################
  def set_p_r(self, p_r):
    self.p_r = p_r
    self.v_r = p_r / self.mass

  ############################
  def set_p_theta(self, p_theta):
    self.p_theta = p_theta
    self.v_theta = p_theta / self.mass 

  ############################
  def set_p_phi(self, p_phi):
    self.p_phi = p_phi
    self.v_phi = p_phi / self.mass 

  ############################
  def set_p_x(self, p_x):
    self.p_x = p_x
    self.v_x = p_x / self.mass 

  ############################
  def set_p_y(self, p_y):
    self.p_y = p_y
    self.v_y = p_y / self.mass 

  ############################
  def set_p_z(self, p_z):
    self.p_z = p_z
    self.v_z = p_z / self.mass 

  ############################
  def set_all_forces_and_accelerations(self):
    return False

  ############################
  def convert_polar_to_cartesian(self):
    return False 

  ############################
  def convert_cartesian_to_polar(self):
    return False 

#########################################################################################



# + + + + + + + + + + + + +
#      H E X A G O N      #
# + + + + + + + + + + + + +
class hexagon:

  ############################
  def __init__(self, id):
    """
    A hexagon is defined/used as an object with six vertices, where on each vertix sits one
    object of "particle" class. The distance between the origin of a hexagon to any of its 
    vertices is unity.
    """
    self.id = id

    # define six vertices as particle objects
    vert1   = particle(id=1)
    vert2   = particle(id=2)
    vert3   = particle(id=3)
    vert4   = particle(id=4)
    vert5   = particle(id=5)
    vert6   = particle(id=6)

    # vert1 is the North vertex, at r=1, phi=pi/2, and theta=0
    vert1.set_r(1.0)
    vert1.set_theta(0.0)
    vert1.set_phi(pi_2)

    # vert2 is the North-West vertex, at r=1, phi=5 pi/6, and theta=0
    vert2.set_r(1.0)
    vert2.set_theta(0.0)
    vert2.set_phi(pi_56)

    # vert3 is the South-West vertex, at r=1, phi=7 pi/6, and theta=0
    vert3.set_r(1.0)
    vert3.set_theta(0.0)
    vert3.set_phi(pi_76)

    # vert4 is the South vertext, at r=1, phi=3 pi/4, and theta=0
    vert4.set_r(1.0)
    vert4.set_theta(0.0)
    vert4.set_phi(pi_32)

    # vert5 is the South-East vertex, at r=1, phi=11 pi/6, and theta=0
    vert5.set_r(1.0)
    vert5.set_theta(0.0)
    vert5.set_phi(pi_116)

    # vert6 is the North-East vertex, at r=1, phi=pi/6, and theta=0
    vert6.set_r(1.0)
    vert6.set_theta(0.0)
    vert6.set_phi(pi_6)

    # add vertices to the hexagon object
    self.vert1 = vert1
    self.vert2 = vert2
    self.vert3 = vert3
    self.vert4 = vert4
    self.vert5 = vert5
    self.vert6 = vert6

    # pack all vertices in one list
    self.vertices = [vert1, vert2, vert3, vert4, vert5, vert6]

    # each hexagon is some "layer" away from the central hexagon, where layer is an integer
    self.layer = 0

    # each hexagon can have neighbors, specified by their ids, as we collect in a list. 
    # The only hexagon which has no neighbor is the central one.
    self.neighbors = []


  ############################
  #      G E T T E R S
  ############################
  def get_id(self):
    return self.id 

  ############################
  def get_vertices(self):
    return self.vertices

  ############################
  def get_layer(self):
    return self.layer

  ############################

  ############################
  #      S E T T E R S
  ############################

  ############################
  def set_layer(self, layer):
    self.layer = layer

  ############################
  ############################
  ############################
  ############################
  ############################
  ############################
  ############################

#########################################################################################

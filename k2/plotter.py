
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Convenience functions to visualize histograms #
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import numpy as np 
import pylab as plt
import lib 

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#### T A R G E T   S E L E C T I O N   V I S U A L I Z A T I O N ####
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def hist_sp_types(rec, file_out=None):

  if file_out is None: return

  fig, ax = plt.subplots(1, figsize=(10,4), dpi=150)

  dic_sp  = lib.stat_sp_types(rec)
  labels  = dic_sp.keys()
  xlabels = [lbl.replace('n_', '') for lbl in labels]
  vals    = np.array([dic_sp[lbl] for lbl in labels])
  n_vals  = len(vals)

  left    = np.arange(n_vals) + 0.10
  height  = vals 
  width   = 0.80
  bottom  = None

  ax.grid(b=True, which='both', axis='both', color='grey', linestyle='solid', lw=1, alpha=0.25, zorder=1)
  ax.bar(left, height, width, bottom, color='purple', zorder=2)

  total = np.sum(vals)
  txt   = '{0} Targets'.format(int(total))
  ax.annotate(txt, xy=(0.05, 0.90), xycoords='axes fraction', fontsize=12)

  # ax.set_xticklabels(xlabels, ha='right')
  plt.xticks(np.arange(n_vals) + 0.5, xlabels)

  ax.set_xlabel('Spectral Types (Simbad)')
  ax.set_ylabel('Number of stars per bin')
  ax.set_xlim(0, n_vals)

  plt.tight_layout()
  plt.savefig(file_out)
  print ' - hist_sp_types: saved {0}'.format(file_out)
  plt.close()

  return None

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def hist_lum_classes(rec, file_out=None):

  if file_out is None: return

  fig, ax = plt.subplots(1, figsize=(6,4), dpi=150)

  dic_lum = lib.stat_lum_classes(rec)
  labels  = dic_lum.keys()
  vals    = np.array([dic_lum[key] for key in labels])
  if labels[-1] == 'n_': labels[-1] = 'Unclassified'
  xlabels = [lbl.replace('n_', '') for lbl in labels]
  n_vals  = len(vals)

  left    = np.arange(n_vals) + 0.10
  height  = vals 
  width   = 0.80
  bottom  = None

  ax.grid(b=True, which='both', axis='both', color='grey', linestyle='solid', lw=1, alpha=0.25, zorder=1)
  ax.bar(left, height, width, bottom, color='purple', zorder=2)

  total = np.sum(vals)
  txt   = '{0} Targets'.format(int(total))
  ax.annotate(txt, xy=(0.05, 0.90), xycoords='axes fraction', fontsize=12)

  # ax.set_xticklabels(xlabels, ha='right')
  plt.xticks(np.arange(n_vals) + 0.5, xlabels)

  ax.set_xlabel('Luminosity Classes (Simbad)')
  ax.set_ylabel('Number of stars per bin')
  ax.set_xlim(0, n_vals)

  plt.tight_layout()
  plt.savefig(file_out)
  print ' - hist_lum_classes: saved {0}'.format(file_out)
  plt.close()

  return None

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def hist_magnitudes(rec, bin_size=0.5, file_out=None):

  if file_out is None: return

  fig, ax = plt.subplots(1, figsize=(6,4), dpi=150)

  mags = rec['mag']

  mag_from = int(np.min(mags))
  l_shift  = 1.
  while np.min(mags) >= mag_from + l_shift * bin_size:
    mag_from += l_shift * bin_size
    l_shift  += 1
  mag_to   = int(np.max(mags)) + 1
  u_shift  = 1.
  while np.max(mags) <= mag_to - u_shift * bin_size:
    mag_to   -= u_shift * bin_size
    u_shift  += 1
  n_bins   = int((mag_to - mag_from)/float(bin_size))
  print ' - hist_magnitudes: from:{0}, to:{1}, in steps:{2}, has {3} bins'.format(
            mag_from, mag_to, bin_size, n_bins)

  ax.grid(b=True, which='both', axis='both', color='grey', lw=1, linestyle='solid', alpha=0.15)
  counts, bins, patches = ax.hist(mags, bins=n_bins, range=(mag_from, mag_to))

  total = np.sum(counts)
  txt   = '{0} Targets'.format(int(total))
  ax.annotate(txt, xy=(0.05, 0.90), xycoords='axes fraction', fontsize=12)

  ax.set_xlim(mag_from, mag_to)
  ax.set_ylim(0, np.max(counts)+2)
  ax.set_xlabel('V magnitude')
  ax.set_ylabel('Number of stars per bin')

  plt.savefig(file_out)
  print ' - hist_magnitudes: saved {0}'.format(file_out)
  plt.close()

  return None

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


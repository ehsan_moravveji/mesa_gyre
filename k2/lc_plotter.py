

import sys, os, glob
import numpy as np 
import pylab as plt

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
##### L I G H T   C U R V E   A N A L Y S I S   P L O T S       #####
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def show_fits_lc(dic, file_out=None):
  if file_out is None: return 

  fig, (top, bot) = plt.subplots(2, 1, figsize=(6, 4))

  bjd    = dic['TIME']
  raw    = dic['SAP_FLUX']    # raw flux
  corr   = dic['PDCSAP_FLUX']

  # check for NaN values in flux
  l_nan  = np.isnan(corr)
  if any(l_nan):
    n    = len(bjd)
    i_OK = ~np.isnan(corr)
    bjd  = bjd[i_OK]
    raw  = raw[i_OK]
    corr = corr[i_OK]
    m    = len(bjd)
    print ' - show_fits_lc: removed {0} rows with NaNs'.format(n - m)

  corr_mean = np.sum(corr) / float(len(corr))
  relf   = corr / np.mean(corr)
  # p    = raw_rec['p']    # polynomial fit
  # c    = raw_rec['c']    # corrected (detrended) flux

  # top.scatter(bjd, raw, marker=',', s=1, color='grey', zorder=1)
  top.scatter(bjd, relf, marker=',', s=1, color='r', zorder=2)
  # top.plot(bjd, p, '-k', zorder=2)
  # bot.scatter(bjd, c * 1e6, marker=',', s=1, color='r', zorder=1)


  top.set_xticklabels(())
  top.set_ylabel('Raw Flux')

  bot.set_xlabel('BJD')
  bot.set_ylabel('Detrended Flux [e/sec]')

  plt.savefig(file_out)
  print ' - lc_plotter: show_fits_lc: saved {0}'.format(file_out)
  plt.close()

  return None

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def show_dft(freq, amp, file_out=None):
  """
  Show the result of the Scargle DFT
  """

  fig, ax = plt.subplots(1, figsize=(6,4))
  ax.plot(freq, amp*1e6, 'b-')

  ax.set_xlabel(r'Frequency [d$^{-1}$]')
  ax.set_ylabel(r'Amplitude [ppm]')

  plt.savefig(file_out)
  print ' - lc_plotter: show_dft: saved {0}'.format(file_out)
  plt.close()

  return None

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
### F O R   V A N D E R B U R G   O U T P U T S   ###
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def show_detrended_lc_vanderburg(raw_rec, file_out=None):
  if file_out is None: return 

  fig, (top, bot) = plt.subplots(2, 1, figsize=(6, 4))

  bjd  = raw_rec['bjd']
  f    = raw_rec['f']    # raw flux
  p    = raw_rec['p']    # polynomial fit
  c    = raw_rec['c']    # corrected (detrended) flux

  top.scatter(bjd, f, marker=',', s=1, color='grey', zorder=1)
  top.plot(bjd, p, '-k', zorder=2)
  bot.scatter(bjd, c * 1e6, marker=',', s=1, color='r', zorder=1)

  top.set_xticklabels(())
  top.set_ylabel('Raw Flux')

  bot.set_xlabel('BJD')
  bot.set_ylabel('Detrended Flux [ppm]')

  plt.savefig(file_out)
  print ' - lc_plotter: show_detrended_lc_vanderburg: saved {0}'.format(file_out)
  plt.close()

  return None

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def show_data_and_dft_vanderburg(rec, freq, amp, file_out=None):
  """
  This routine is essentially the superposition of show_detrended_lc_vanderburg(), and show_dft()
  """

  if file_out is None: return 

  fig, (top, mid, bot) = plt.subplots(3, 1, figsize=(6, 4))

  bjd  = rec['bjd']
  f    = rec['f']    # raw flux
  p    = rec['p']    # polynomial fit
  c    = rec['c']    # corrected (detrended) flux

  top.scatter(bjd, f, marker=',', s=1, color='grey', zorder=1)
  top.plot(bjd, p, '-k', zorder=2)
  mid.scatter(bjd, c * 1e3, marker=',', s=1, color='r', zorder=1)

  top.set_xticklabels(())
  top.set_ylabel('Raw')

  mid.set_xlabel('BJD')
  mid.set_ylabel('Detrended [ppt]')

  # and the bottom is thd DFT
  bot.plot(freq, amp*1e6, 'b-', lw=0.5)

  bot.set_xlabel(r'Frequency [d$^{-1}$]')
  bot.set_ylabel(r'Amp. [ppt]')

  plt.savefig(file_out)
  print ' - lc_plotter: show_detrended_lc_vanderburg: saved {0}'.format(file_out)
  plt.close()

  return None

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

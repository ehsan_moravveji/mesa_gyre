
"""
This module is meant to deal with the K2 light curves, and specifically detrend them
for later Fourier analysis.
For a basic example, see the "~/my/data/K2/plot-lc.py"
"""

import os, sys, glob
import logging
import numpy as np 

from astropy.io import fits
from ivs.timeseries.pergrams import scargle

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def ivs_get_dft(rec):
  """
  evaluate DFT by calling a routine from the ivs repository
  """
  bjd = rec['bjd']
  c   = rec['c']
  f,a = scargle(bjd, c)

  return f, a

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def write_lc_ascii(rec, file_out):
  """

  """
  bjd = rec['bjd']
  f   = rec['f']
  lines = []
  for i, row in enumerate(rec):
    line = '{0:.9f}  {1:.9f} \n'.format(row['bjd'], row['f'])
    lines.append(line)

  with open(file_out, 'w') as w:
    w.writelines(lines)

  print ' - lc_lib: write_lc_ascii: saved {0}'.format(file_out)

  return None

# #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# def detrend_and_update_flux(rec, n_poly):
#   """

#   """
#   y, res = detrend_lc(rec_lc=rec, n_poly=n_poly)
#   rec['f'] = res[:]

#   return rec

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def detrend_lc(rec_lc, n_poly):
  """

  """
  def poly0(a, x):
    return a

  def poly1(a, b, x):
    return a + b*x

  def poly2(a, b, c, x):
    return a + b * x + c * x**2

  def poly3(a, b, c, d, x):
    return a + b * x + c * x**2 + d * x**3

  if n_poly == 0:
    func = poly0
  elif n_poly == 1: 
    func = poly1
  elif n_poly == 2:
    func = poly2
  elif n_poly == 3:
    func = poly3
  else:
    logging.error('lc_lib: detrened_lc: n_poly={0} is not supported.'.format(n_poly))
    raise SystemExit, 'lc_lib: detrened_lc: n_poly={0} is not supported.'.format(n_poly)

  bjd = rec_lc['bjd']
  f   = rec_lc['f']
  coeff = np.polyfit(bjd, f, deg=n_poly)

  # y == polynomial fit to the data
  if n_poly == 0:
    a = coeff[0]
    y = poly0(a, bjd)
  elif n_poly == 1:
    b = coeff[0]
    a = coeff[1]
    y = poly1(a, b, bjd)
  elif n_poly == 2:
    c = coeff[0]
    b = coeff[1]
    a = coeff[2]
    y = poly2(a, b, c, bjd)
  elif n_poly == 3:
    d = coeff[0]
    c = coeff[1]
    b = coeff[2]
    a = coeff[3]
    y = poly3(a, b, c, d, bjd)

  # res == the residual or say, the detrended profile
  res = f - y
  rec_lc['p'] = y
  rec_lc['c'] = res

  return rec_lc

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def read_vandenberg_ascii(lc_file):
  """
  The Vandenberg reduction (through his personal website, or MAST) provides two-column
  ASCII files for light curves.
  Keys: 
  bjd: barycentric julian date
  f:   raw flux
  p:   polynomial model
  c:   corrected (detrended) flux
  """
  if not os.path.exists(lc_file):
    logging.error('lc_lib: read_vandenberg_ascii: file "{0}" does not exist'.format(lc_file))
    sys.exit('lc_lib: read_vandenberg_ascii: file "{0}" does not exist'.format(lc_file))

  with open(lc_file, 'r') as r: lines = r.readlines()
  header = lines.pop(0)
  header = header.strip('\p\n').split(',')
  n_rows = len(lines)
  
  dtypes = [('bjd', 'f8'), ('f', 'f8'), ('p', 'f8'), ('c', 'f8')]
  rows   = np.empty(n_rows, dtype=dtypes)

  for i, row in enumerate(lines):
    row  = row.strip('\p\n').split(',')
    rows[i][0] = float(row[0])
    rows[i][1] = float(row[1])

  rows['p'] = 0.0
  rows['c'] = 0.0

  print ' - read_vandenberg_ascii: successfully read "{0}", with {1} lines'.format(lc_file, n_rows)

  return rows

def get_lc(dic):
  """
  Return the corrected flux as a function of time for every light curve that is
  already read (by e.g. read_fits()), and is passed as a dictionary.
  @return: time and flux (in electron per second) as two ndarrays.
  @rtype: tuple
  """
  
  return dic['TIME'], dic['PDCSAP_FLUX']

###############################################################################
def read_fits(fits_file, info=False):
  """
  Read the NASA K2 fits files
  @param fits_file: full path to the fits file that containst the whole data
  @type fits_file: string
  @param info: print out additional info on the screen
  @type info: boolean
  @return: dictionary containing most (not all) the contents of the fits file.
     The columns/fields that have a repeated name are omited.
     The whole light curve data are stored and passed as ndarrays.
  @rtype: dict
  """
  if not os.path.exists(fits_file):
    logging.error('lib_lc: read_fits: "{0}" does not exist'.format(fits_file))
    raise SystemExit, 'lib_lc: read_fits: "{0}" does not exist'.format(fits_file)

  hdulist = fits.open(fits_file)
  if info: hdulist.info()

  prim   = hdulist[0]  # the primary 
  table  = hdulist[1]  # the light curve
  apert  = hdulist[2]  # the apperture

  p_hdr  = prim.header
  p_keys = p_hdr.keys()  
  p_data = prim.data

  lc_hdr = table.header
  lc_keys= lc_hdr.keys()
  lc     = table.data
  lc_cols= lc.dtype.names

  a_hdr  = apert.header
  a_keys = a_hdr.keys()
  mask   = apert.data

  # store and pass the whole info as a dictionary
  dic    = {}

  # insert the header keys as a field in this dictionary
  for key in p_keys: dic[key] = p_hdr[key]

  # insert the header of data light curve
  for key in lc_keys: 
    if key in p_keys or key in a_keys:
      if info: print 'repeated key in light curve:', key, lc_hdr[key]
    else:
      dic[key] = lc_hdr[key]

  for key in a_keys:
    if key in p_keys or key in lc_keys:
      if info: print 'repeated key in aperture:', key, a_hdr[key]
    else:
      dic[key] = a_hdr[key]

  # insert the data into the dictionary
  for key in lc_cols:
    dic[key] = lc[key]

  # insert the aperture image into the dictionary
  dic['mask'] = mask

  # print repr(lc_hdr)

  hdulist.close()

  return dic

###############################################################################


"""
Library to interpret the Simbad input list of stars.
For an example of a typical script, refer to example().
"""


import sys, os, glob
import logging
from collections import OrderedDict
import numpy as np 


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Routines to analyse the target list #
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def targets_to_K2FoV(rec, file_out=None):

  if file_out is None: return 

  output = []
  for i_row, row in enumerate(rec):
    txt = '{0:.5f}, {1:.5f}, {2:.2f} \n'.format(row['ra']*15, row['dec'], row['mag'])
    output.append(txt)

  with open(file_out, 'w') as w: w.writelines(output)

  print ' - targets_to_K2FoV: saved {0}'.format(file_out)

  return None

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def targets_to_table(rec, file_out=None):

  if file_out is None: return 

  n_rec  = len(rec)
  hdrs   = ['Object', 'RA', 'DEC', 'Kp', 'Cadence', 'd_RA', 'd_DEC', 'extent', 'comment']
  header = ''
  for hdr in hdrs:
    header += '{0:>20s}'.format(hdr)
  lines  = [header + '\n']
  cadence= 30

  for i in range(n_rec):
    row  = rec[i]
    line = '{0:>20s}{1:>20.6f}{2:>20.6f}{3:>20.2f}{4:>20d}{5:>20s}{6:>20s}{7:>20s}{8:>20s}\n'.format(
           row['name'], row['ra'], row['dec'], row['mag'], cadence, ' ', ' ', ' ', ' ')
    lines.append(line)

  with open(file_out, 'w') as w: w.writelines(lines)

  return lines

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def keep_stars_by_mag(rec, mag_from, mag_to):

  if mag_from > mag_to:
    raise SystemExit, 'Error: keep_stars_by_mag: mag_from > mag_to; reverse the ranges'
  n_rec = len(rec)
  mags  = rec['mag']
  keep  = [i for i in range(n_rec) if mags[i] >= mag_from and mags[i] <= mag_to]

  n_keep = len(keep)
  if n_keep == 0:
    print ' - keep_stars_by_mag: No stars left in this magnitude range!'
    return np.empty_like(rec)

  return rec[keep]

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def exclude_stars_by(rec, by='lum', exclude='V'):

  names = rec.dtype.names 
  if by not in names:
    raise SystemExit, 'Error: exclude_stars_by: {0} not among recor columns'.format(by)

  vals  = rec[by]
  n_rec = len(vals)
  if n_rec == 0:
    raise SystemExit, 'Error: exclude_stars_by: The input recarray is already empty!'

  # print vals
  # print type(vals), vals.shape
  # sys.exit()
  # ind   = np.where(vals == exclude)[0]
  ind   = [i for i in range(n_rec) if vals[i] != exclude]

  print ' - Exclude by: "{0}={1}": N_in={2}, and N_out={3}'.format(by, exclude, n_rec, len(ind))

  return rec[ind]

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def print_filtered_list(rec):

  n_rec  = len(rec)
  if n_rec == 0: return 

  k      = ['Num.'] + [name for name in rec.dtype.names]
  print '   {0:>4s}{1:>24s}{2:>10s}{3:>12s}{4:>8s}{5:>8s}'.format(
    k[0], k[1], k[2], k[3], k[4], k[5])
  for i, a in enumerate(rec):
    print '   {0:>4d}{1:>24s}{2:>10.2f}{3:>12s}{4:>8s}{5:>8s}'.format(
      i+1, a['name'], a['mag'], a['type'], a['sp'], a['lum'])
  print

  return None

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def filter_by_key(rec, key='lum', which='Ia'):

  if key not in rec.dtype.names:
    raise SystemExit, 'Error: filter_by_key: the requested key "{0}" not in rec.dtype.names'.format(key)

  n_rec = len(rec)
  new   = np.empty_like(rec)
  i_new = 0
  for i_row, row in enumerate(rec):
    if row[key] == which:
      new[i_new] = row 
      i_new += 1

  new   = new[:i_new]

  return new 

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def stat_sp_types(rec):

  sp_types   = get_all_sp_types(rec)
  n_types    = len(sp_types)
  counters   = define_counters(sp_types)
  dic_counts = get_null_dic_counts(counters)
  classified = []

  n_rec      = len(rec)
  for i_star, star in enumerate(rec):

    sp       = star['sp']

    for i_sp, sp_cls in enumerate(sp_types):
      if star['name'] in classified: 
        break
      counter = counters[i_sp]
      if sp == sp_cls:
        dic_counts[counter] += 1
        classified.append(star['name'])
        break

  return dic_counts

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def get_all_sp_types(rec):

  # return [
  #         'O',
  #         'B',
  #         'A',
  #         'e',     # emmision-line star (B[e])
  #         'p',     # Chemically peculiar stars (Ap, Bp)
  #         'z',     # Jesus!
  #         ]

  return sorted(list(set(rec['sp'])))

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def stat_lum_classes(rec):

  classes    = get_all_lum_classes()
  counters   = define_counters(classes)
  dic_counts = get_null_dic_counts(counters)
  name_other = []
  classified = []

  n_rec      = len(rec)
  for i_star, star in enumerate(rec):

    done  = False
    lum   = star['lum']

    for i_cls, cls in enumerate(classes):
      if star['name'] in classified: 
        done  = True
        break
      counter = counters[i_cls]
      if lum == cls:
        dic_counts[counter] += 1
        classified.append(star['name'])
        done = True
        break

    if not done:
      print star

  n_tot = 0
  for name in dic_counts:
    n_tot += dic_counts[name]
  if n_tot != n_rec:
    print 'n_tot={0}; n_rec={1}'.format(n_tot, n_rec)
    print dic_counts
    raise SystemExit, 'Error: stat_lum_classes: n_tot != n_rec'

  print ' - Statistics for Luminosity Classes:'
  for i, name in enumerate(dic_counts.keys()):
    print '   Class: {0}, {1} = {2}'.format(classes[i], counters[i], dic_counts[counters[i]])
  print

  return dic_counts

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def get_all_lum_classes():
  """
  Get pre-defined luminosity classes
  """

  return [ 
          # 'I',
          'Ia',
          'Ib', 
          # 'Iab', 
          'II',
          'III',
          'IV',
          'V',
          '',   # for unknown/unspecified luminosity classes
          ]

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def define_counters(list_classes):
  n_classes = len(list_classes)
  if n_classes == 0:
    raise SystemExit, 'Error: define_counters: Empty classes!'

  counter_names = []
  for i, cls in enumerate(list_classes):
    name = 'n_{0}'.format(cls)
    counter_names.append(name)

  return counter_names

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def get_null_dic_counts(list_counters):

  n_counters = len(list_counters)
  if n_counters == 0:
    raise SystemExit, 'Error: get_null_dic_counts: Empty enumerators!'

  dic = OrderedDict()
  for i in range(n_counters):
    counter_name = list_counters[i]
    dic[counter_name] = 0

  return dic

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def read_multiple_lists(files):
  """
  calls read_target_list multiple times, and stacks all record arrays,
  and returns a single combined recarray.
  """
  n_files = len(files)
  if n_files == 0:
    raise SystemExit, 'Error: lib: read_multiple_lists: Input file list is empty'

  for f in files:
    if not os.path.exists(f):
      raise SystemExit, 'Error: lib: read_multiple_lists: {0} does not exist!'.format(f)

  list_recs = []
  len_recs  = 0
  for i, f in enumerate(files):
    rec = read_target_list(f)
    len_recs += len(rec)
    list_recs.append(rec)

  dtype = rec.dtype
  data  = np.empty(len_recs, dtype=dtype)

  k     = 0
  for i, rec in enumerate(list_recs):
    n   = len(rec)

    if rec.dtype != dtype:
      raise SystemExit, 'Error: lib: read_multiple_lists: Conflicting rec.dtype discovered.'

    data[k : k+n] = rec
    k   += n

  return data

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def read_target_list(fname):
  """
  Note: during reading, we exclude the secondary components of binaries, 
        by trimming the string after '+' character.
  """
  if not os.path.exists(fname):
    raise SystemExit, 'Error: read_target_list: {0} does not exist'.format(fname)

  with open(fname, 'r') as r: lines = r.readlines()
  n_stars = len(lines)
  print ' - read_target_list: {0} has {1} entries'.format(fname, n_stars)

  dtype = [('name', 'S25'), ('mag', np.float32), 
           ('type', 'S25'), ('sp', 'S10'), ('lum', 'S15'),
           ('ra', np.float32), ('dec', np.float32), 
           ('ra_str', 'S25'), ('dec_str', 'S25')]
  rec   = np.empty(n_stars, dtype=dtype)

  for i, line in enumerate(lines):
    line = line.rstrip('\r\n').split()
    star_name = line[0]
    mag = float(line[-2])
    sp_lum = line[-1]
    ind_I  = 101
    ind_V  = 101
    if 'I' in sp_lum: ind_I = sp_lum.find('I')
    if 'V' in sp_lum: ind_V = sp_lum.find('V')
    ind = min([ind_I, ind_V])
    if ind  == 101:
      if 'e' in sp_lum: sp_lum = sp_lum.replace('e', '')
      sp    = sp_lum[:ind]
      lum   = ''
    else:
      sp    = sp_lum[:ind]
      lum   = sp_lum[ind:]
      ind_pp= 101 # for '+'
      ind_e = 101
      ind_p = 101
      ind_n = 101
      ind_z = 101
      if '+' in lum: ind_pp = lum.find('+')
      if 'e' in lum: ind_e  = lum.find('e')
      if 'p' in lum: ind_p  = lum.find('p')
      if 'n' in lum: ind_n  = lum.find('n')
      if 'z' in lum: ind_z  = lum.find('z')
      ind_m = min([len(lum)+1, ind_pp, ind_e, ind_p, ind_n, ind_z])
      lum   = lum[:ind_m]
    # print sp_lum, sp, lum
    
    dec_deg = float(line[-6])
    dec_min = float(line[-5])
    dec_sec = float(line[-4])
    dec_sign= dec_deg > 0
    if dec_sign:
      dec   = dec_deg + dec_min/60. + dec_sec/3600.
    else:
      dec   = dec_deg - dec_min/60. - dec_sec/3600.
    dec_str = '{0} {1} {2}'.format(line[-6], line[-5], line[-4])

    try:
      ra_hour = float(line[-9])
    except:
      print line 
      print len(line)
      sys.exit()
    ra_min  = float(line[-8])
    ra_sec  = float(line[-7])
    ra_sign = ra_hour > 0
    if ra_sign:
      ra    = ra_hour + ra_min/60. + ra_sec/3600.
    else:
      ra    = ra_hour - ra_min/60. - ra_sec/3600.
    ra_str  = '{0} {1} {2}'.format(line[-9], line[-8], line[-7])

    rec['name'][i]   = star_name
    rec['mag'][i]    = mag 
    rec['type'][i]   = sp_lum
    rec['sp'][i]     = sp 
    rec['lum'][i]    = lum
    rec['ra'][i]     = ra 
    rec['dec'][i]    = dec 
    rec['ra_str'][i] = ra_str
    rec['dec_str'][i]= dec_str

  print '   Magnitudes range: {0:.2f} <= Mag <= {1:.2f}'.format(np.max(rec['mag']), np.min(rec['mag']))
  print

  return rec

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def example():

  if True:
    print ' Trim off uninteresting stars, prepare target list for NASA'
    file_stars = 'all-targets.txt'
    rec_stars  = read_target_list(file_stars)
    exclude_sp = ['O', 'O6', 'O7', 'O7.5', 'O8', 'O9', 'O9.5', 
                  'A0', 'A1', 'Ap']
    exclude_lum= ['V', 'IV']              
    rec_BSGs   = np.empty_like(rec_stars)
    rec_BSGs[:]= rec_stars
    for i, sp in enumerate(exclude_sp):
      rec_BSGs = exclude_stars_by(rec_BSGs, by='sp', exclude=sp)
    for i, lum in enumerate(exclude_lum):
      rec_BSGs = exclude_stars_by(rec_BSGs, by='lum', exclude=lum)

    rec_BSGs   = keep_stars_by_mag(rec_BSGs, mag_from=8, mag_to=12)
    print_filtered_list(rec_BSGs)

    # And some supporting plots
    dic_BSG_mags = hist_magnitudes(rec_BSGs, file_out='plots/BSGs-Hist-Magnitude.png')
    dic_BSG_lum  = hist_lum_classes(rec_BSGs, file_out='plots/BSGs-Hist-Lum-Classes.png')
    dic_BSG_sp   = hist_sp_types(rec_BSGs, file_out='plots/BSGs-Hist-Spectral-Types.png')

    # write out the filtered targets as a formatted ASCII file for NASA
    targets_to_table(rec_BSGs, file_out='BSG-Target-list.txt')

    # write an ASCII input for K2FoV to check targets on silicon
    targets_to_K2FoV(rec_BSGs, file_out='on-silicon/BSGs-k2FoV.txt')


  if False:
    print ' A bit of info ...'

    file_stars = 'all-targets.txt'
    rec_stars  = read_target_list(file_stars)
    rec_A0     = filter_by_key(rec_stars, key='sp', which='A0')
    print_filtered_list(rec_A0)
    rec_A1     = filter_by_key(rec_stars, key='sp', which='A1')
    print_filtered_list(rec_A1)
    rec_Ap     = filter_by_key(rec_stars, key='sp', which='Ap')
    print_filtered_list(rec_Ap)
    rec_B      = filter_by_key(rec_stars, key='sp', which='B')
    print_filtered_list(rec_B)
    rec_B8     = filter_by_key(rec_stars, key='sp', which='B8')
    print_filtered_list(rec_B8)
    rec_B9     = filter_by_key(rec_stars, key='sp', which='B9')
    print_filtered_list(rec_B9)



  if False:
    print ' Get histograms of all different stellar classes for "Potential" targets'
    file_stars = 'all-targets.txt'
    rec_stars  = read_target_list(file_stars)
    # dic_lum_classes = stat_lum_classes(rec_stars)
    # dic_sp_types    = stat_sp_types(rec_stars)

    # dic_hist_mag    = hist_magnitudes(rec_stars, file_out='plots/Hist-magnitudes.png')
    # dic_hist_classes  = hist_lum_classes(rec_stars, file_out='plots/Hist-Luminosity-Classes.png')
    # dic_sp_types      = hist_sp_types(rec_stars, file_out='plots/Hist-Spectral-Types.png')

    # Print prime targets of interest
    rec_Ia            = filter_by_key(rec_stars, key='lum', which='Ia')
    print_filtered_list(rec_Ia)

    rec_Ib            = filter_by_key(rec_stars, key='lum', which='Ib')
    print_filtered_list(rec_Ib)
    
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import os
from copy import deepcopy
import read
import matplotlib.pylab as plt
import numpy as np

c_SI = 2.99792458e8
pc_to_m = 3.085677e16
Lsun = 3.839e26
dic_filter = {'name':'', 'lambda_A':0, 'flux':0, 'e_flux':0}

dic_B = {'name':'B', 'lambda_A':4400,  'flux':4.73e-10, 'e_flux':2.8781e-11}
dic_V = {'name':'V', 'lambda_A':5500,  'flux':2.75e-9, 'e_flux':8.27e-10}

dic_J = {'name':'J', 'lambda_A':12400, 'flux':4.61e-9, 'e_flux':6.29e-11}
dic_H = {'name':'H', 'lambda_A':16600, 'flux':3.76e-9, 'e_flux':5.123e-11}
dic_K = {'name':'K', 'lambda_A':21590, 'flux':1.64e-9, 'e_flux':2.46e-11}

dic_d1 = {'name':'Denis1', 'lambda_A':7910, 'flux':5.89e-9, 'e_flux':1.58e-10}
dic_d2 = {'name':'Denis2', 'lambda_A':12300, 'flux':4.60e-9, 'e_flux':2.78e-10}
dic_d3 = {'name':'Denis3', 'lambda_A':21500, 'flux':1.81e-9, 'e_flux':1.45e-10}

dic_w1 = {'name':'W1', 'lambda_A':3.35e4, 'flux':5.26e-10, 'e_flux':6.22e-12}
dic_w2 = {'name':'W2', 'lambda_A':4.6e4, 'flux':1.56e-10, 'e_flux':1.85e-12}
dic_w3 = {'name':'W3', 'lambda_A':1.16e5, 'flux':9.31e-11, 'e_flux':1.31e-12}
dic_w4 = {'name':'W4', 'lambda_A':2.21e5, 'flux':3.34e-11, 'e_flux':2.34e-12}

model_file = 'fm05t6250g05k2odfnew.dat'
#model_file = 'fm05t6000g00k2odfnew.dat'
#model_file = 'fm05t5750g00k2odfnew.dat'
#model_file = 'fm05t4500g00k2odfnew.dat'
#model_file = 'fm05t3500g00k2odfnew.dat'
dic_model = read.read_castellini(model_file)
dic_model['lambda_m'] = dic_model['lambda_cm'] * 1e-2
dic_model['Hnu_SI'] = dic_model['Hnu'] * 1e6
dic_model['Flambda_SI'] = 4. * c_SI * dic_model['Hnu_SI'] / (dic_model['lambda_m']**2.)

# get the scale factor
ind_lambda_J = np.argmin(np.abs( dic_model['lambda_m']-dic_J['lambda_A']*1e-10 ))
Flambda_J = dic_model['Flambda_SI'][ind_lambda_J]
scale_factor = Flambda_J / dic_J['flux']
print ' - Scale Factor = %s' % (scale_factor, )
dic_model['Flambda_SI_scaled'] = dic_model['Flambda_SI'] / scale_factor

R_lambda_file = 'Rv_3_1.dat'

list_filters = [dic_B, dic_V, dic_J, dic_H, dic_K, dic_d1, dic_d2, dic_d3, dic_w1, dic_w2, dic_w3, dic_w4]
flux_Gauss = []
# Create normal distributions of fluxes with size n_try
for i, dic in enumerate(list_filters):
  dic['lambda_cm'] = dic['lambda_A'] / 1e8
  dic['lambda_m']  = dic['lambda_A'] / 1e10
  dic['R_lambda']  = read.get_R_lambda_at_lambda(R_lambda_file, lambda_A=dic['lambda_A'])





# First, loop over E(B-V) and calculate chisq
n_E   = 1000
arr_E_B_V = np.linspace(0, 2.0, num=n_E)
list_chisq = []

for i_E in range(n_E):
  E_B_V = arr_E_B_V[i_E]
  sum_deviation = 0.0
  for i_filter, dic in enumerate(list_filters):
      A_lambda = E_B_V * dic['R_lambda']
      F_obs_err    = dic['e_flux']
      F_obs_uncorr = dic['flux']
      F_obs_corr   = F_obs_uncorr * np.power(10.0, A_lambda/2.5)
      F_model      = read.get_Flambda_at_lambda(dic_model, lambda_cm=dic['lambda_cm'], cgs=False, SI=True)
      F_model_scaled  = F_model / scale_factor
      sum_deviation += (F_obs_corr - F_model_scaled)**2. / F_obs_err**2.0
  list_chisq.append(sum_deviation / (12. - 2.))
list_chisq = np.array(list_chisq)
ind_min_chisq = np.argmin(list_chisq)
chisq_min = list_chisq[ind_min_chisq]
best_E_B_V = arr_E_B_V[ind_min_chisq]
print ' - Type 1:'
print '   Min(chisq) = %d, and Best E(B-V) = %0.4f \n' % (chisq_min, best_E_B_V)

# and now correct observations with the best excess
for i_filter, dic in enumerate(list_filters):
    A_lambda = best_E_B_V * dic['R_lambda']
    F_obs_uncorr = dic['flux']
    dic['flux_corr']   = F_obs_uncorr * np.power(10.0, A_lambda/2.5)

#F_model_J    = read.get_Flambda_at_lambda(dic_model, lambda_cm=dic_J['lambda_cm'], cgs=False, SI=True)
scale_factor = dic_model['Flambda_SI_scaled'][ind_lambda_J] / dic_J['flux_corr']
dic_model['Flambda_SI_scaled'] = dic_model['Flambda_SI_scaled'] / scale_factor


# Integretae Flambda to get bolometric model flux
n_lambda = len(dic_model['Flambda_SI_scaled'])
dlambda = np.zeros((n_lambda, ))
F_bol = 0.0
for i in range(0,n_lambda-1):
  dlambda[i] = dic_model['lambda_m'][i+1] - dic_model['lambda_m'][i]
  F_bol += dic_model['Flambda_SI_scaled'][i] * dlambda[i]
F_bol_cgs = F_bol * 1e-3

print ' - Bolometric Flux = %s [SI]' % (F_bol, )
d_SMC = 61e3  #pc
Teff  = 6250
Lum   = d_SMC**2. * F_bol_cgs / 3.26e-5   # Lsun
R     = np.sqrt(Lum/(Teff/5777.)**4.)
print ' - Luminosity=%s, and Radius=%s ' % (Lum, R)
# T_dust= Teff * np.sqrt(R/(2.*d_SMC))
# print ' - T_dust = ', T_dust

#list_Lum = [2e3, 6e3, 1e4]
#for l in list_Lum:
  #d = np.sqrt(3.26e-5*l/F_bol_cgs)
  #print 'L=%s => d=%0.2f [kpc]' % (l, d/1e3)

print ' - T_dust = %s' % (2.8978e-3/dic_w4['lambda_m'], )
print ' - 4pi * flux * d^2 = ', 4.*np.pi * (d_SMC*pc_to_m)**2. * F_bol / Lsun

for i_dic, dic in enumerate(list_filters):
  print dic['name'], dic['flux_corr'], dic['e_flux']

# Plottings
if False:
  fig = plt.figure(figsize=(6,4))
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.18,right=0.98,bottom=0.12,top=0.98)
  ax.set_xlabel(r'E(B-V)')
  ax.set_ylabel(r'$\chi^2$')
  ax.set_xlim(0,2)
  ax.set_ylim(0, chisq_min*500)
  ax.plot(arr_E_B_V, list_chisq, color='black')
  ax.scatter([best_E_B_V], [chisq_min], marker='v', color='red')
  plt.savefig('plots/Type1-Chisq-vs-E(B-V).pdf')
  plt.close()

if False:
  fig = plt.figure(figsize=(6,4))
  plt.subplots_adjust(left=0.12, bottom=0.12)
  ax1  = fig.add_subplot(111)
  ax1.set_xlim(-7.,-5)
  ax1.set_xlabel(r'Wavelength $\log \lambda$ [m]')
  ax1.plot(np.log10(dic_model['lambda_m']), dic_model['Flambda_SI_scaled'], color='black')
  list_flux_orig = []
  list_flux_corr = []
  list_lambdas   = []
  list_flux_errs = []
  for i_dic, dic in enumerate(list_filters):
    list_lambdas.append(dic['lambda_m'])
    list_flux_orig.append(dic['flux'])
    list_flux_corr.append(dic['flux_corr'])
    list_flux_errs.append(dic['e_flux'])
  list_lambdas = np.array(list_lambdas); list_flux_errs = np.array(list_flux_errs)
  list_flux_orig = np.array(list_flux_orig); list_flux_corr = np.array(list_flux_corr)
  list_log_lambda_Flambda_orig = np.log10(list_lambdas * list_flux_orig)
  list_log_lambda_Flambda_corr = np.log10(list_lambdas * list_flux_corr)

  ax1.errorbar(np.log10(list_lambdas), list_flux_orig, yerr=list_flux_errs, linestyle='None',
               color='red', ecolor='red', marker='s', ms=5, label=r'Observed Fluxes')
  ax1.errorbar(np.log10(list_lambdas), list_flux_corr, yerr=list_flux_errs, linestyle='None',
               color='blue', ecolor='blue', marker='o', ms=5, label=r'Corrected Fluxes')
  leg = ax1.legend(loc=1)

  ax1.scatter([np.log10(dic_J['lambda_m'])], [dic_J['flux']], s=40, color='black', marker='o')

  plt.savefig('plots/Obs-Model-'+model_file[:-12]+'.pdf')
  plt.close()

  # 2nd plot
  fig = plt.figure(figsize=(6,4))
  plt.subplots_adjust(left=0.13,bottom=0.12, hspace=0.08, wspace=0.08)
  ax1  = fig.add_subplot(111)
  ax1.set_xlim(-7.2,-3.6)
  ax1.set_xlabel(r'Wavelength $\log\lambda$ [m]')
  ax1.set_ylim(-17, -13)
  ax1.set_ylabel(r'$\lambda F_\lambda$ [SI]')
  ax1.plot(np.log10(dic_model['lambda_m']), np.log10(dic_model['lambda_m']*dic_model['Flambda_SI_scaled']))
  #for i_dic, dic in enumerate(list_filters):
    #ax1.scatter([np.log10(dic['lambda_m'])], [np.log10(dic['lambda_m']*dic['flux'])], s=40, color='red', marker='s')
  #ax1.errorbar(np.log10(list_lambdas), list_flux_orig, yerr=list_flux_errs, linestyle='None',
               #color='red', ecolor='red', marker='s', ms=5, label=r'Observed Fluxes')
  ax1.errorbar(np.log10(list_lambdas), list_log_lambda_Flambda_orig, yerr=list_flux_errs, linestyle='None',
               color='red', ecolor='red', marker='s', ms=5, label=r'Observed Fluxes')
  ax1.errorbar(np.log10(list_lambdas), list_log_lambda_Flambda_corr, yerr=list_flux_errs, linestyle='None',
               color='blue', ecolor='blue', marker='o', ms=5, label=r'Corrected Fluxes')
  leg = ax1.legend(loc=1)

  plt.savefig('plots/log-lambda-log-lambda-Flambda.pdf')
  plt.close()

if False:
  R_lambda = read.read_R_lambda(R_lambda_file, from_lambda=0, to_lambda=10000, plot_name = 'plots/R_lambda.pdf')




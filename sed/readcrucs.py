import numpy as np
import readdata
import pylab as pl
f=open("asli.dat",'r')
header1=f.readline()
header2=f.readline()
wavelength=[]
frequency=[]
hnu=[]
hcount=[]
hnu_hcount=[]
for line in f:
 columns = line.split()
 if ("E" not in columns[4]):
   tochange=columns[4].split("-")
   
   tochange=tochange[0]+"E-"+tochange[1]
   hnu.append(float(tochange))
 else:
  hnu.append(float(columns[4]))
 
 if("E" not in (columns[5]) ):
 
  tochange=columns[5].split("-")
 # print tochange
  tochange=tochange[0]+"E-"+tochange[1]
  hcount.append(float(tochange))  
 else:
  hcount.append(float(columns[5]))
 wavelength.append(float(columns[2]))
  
 frequency.append(float(columns[3]))
 
 hnu_hcount.append(float(columns[6]))
frequency=np.asarray(frequency)
wavelength=np.asarray(wavelength)
hnu=np.asarray(hnu)*1000
f.close()
c=3.e8
wavelength2=wavelength*1.e-9
flambda=1000*4*hnu*c/((wavelength2)**2)
#print np.amax(1.36e-25*flambda)
obs_wavelength,obs_flux=readdata.function()
pl.plot(np.log10(wavelength2),np.log10(flambda*wavelength2))
pl.scatter(np.log10(obs_wavelength),np.log10(obs_flux*obs_wavelength))

#print np.amax(np.log10(flambda*wavelength2))
#pl.ylim(0,14)
#pl.show()
pl.savefig('lambda-Flambda-log.pdf')
pl.close()

index= np.argmin(np.abs(wavelength2-1.24E-006))
scalefactor=flambda[index]/obs_flux[2]
flambda_scaled=flambda/scalefactor
pl.plot(np.log10(wavelength2),np.log10(flambda_scaled*wavelength2))
pl.scatter(np.log10(obs_wavelength),np.log10(obs_flux*obs_wavelength))
pl.savefig('lambda-Flambda-log-corrected.pdf')
pl.close()
scaled_obs_flux=scalefactor*obs_flux
print ("scaling factor equals to"), 1/scalefactor,scalefactor


###########################################################################################################################################################################
model_b=flambda[np.argmin(np.abs(wavelength2-440e-9))]
model_v=flambda[np.argmin(np.abs(wavelength2-550e-9))]
print model_b,model_v

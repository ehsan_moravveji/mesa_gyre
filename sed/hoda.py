import numpy as np
import pylab as pl
#f=open("fp02t4500g20k2odfnew.dat",'r')
f=open('fm10t6250g40k2odfnew.dat', 'r')
header1=f.readline()
header2=f.readline()
wavelength=[]
frequency=[]
hnu=[]
hcount=[]
hnu_hcount=[]
for line in f:
 columns = line.split() 
 if ("E" not in columns[4]): 
   tochange=columns[4].split("-")
    
   tochange=tochange[0]+"E-"+tochange[1]
   hnu.append(float(tochange))
 else:
  hnu.append(float(columns[4])) 
 
 if("E" not in (columns[5]) ): 
  
  tochange=columns[5].split("-")
 # print tochange
  tochange=tochange[0]+"E-"+tochange[1]
  hcount.append(float(tochange))   
 else:
  hcount.append(float(columns[5]))
 wavelength.append(float(columns[2]))
   
 frequency.append(float(columns[3]))
 
 hnu_hcount.append(float(columns[6]))
frequency=np.asarray(frequency)
wavelength=np.asarray(wavelength)
hnu=np.asarray(hnu)
f.close()
c=3.e10
wavelength2=wavelength*1.e-7
flambda=4*hnu*c/((wavelength2)**2)

maxi=np.amax(wavelength2*flambda)
fig=pl.figure()
pl.plot(wavelength2,flambda)
pl.savefig('hoda.pdf')
pl.close()
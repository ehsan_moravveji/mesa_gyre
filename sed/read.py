import os, glob
import numpy as np
import matplotlib
import matplotlib.pylab as plt


# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
def get_Flambda_at_lambda(dic, lambda_cm, cgs=True, SI=False):
  """
  This function returns the Flambda from the Kurucz model at a specified lambda [micron]
  """
  ind_min = np.argmin(np.abs(dic['lambda_cm']-lambda_cm)) 

  if cgs and not SI: return dic['Flambda'][ind_min]
  if not cgs and SI: return dic['Flambda_SI'][ind_min] 
  
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
def eval_error_F_nu(mag, e_mag, zp, flux, e_flux, unit):
  """
  Evaluate the error in individual observation per filter
  """
  if unit == 'mag':
    print 'mag: ', (np.abs( np.power(10., -0.4*(mag-e_mag)) - np.power(10., -0.4*(mag+e_mag)) ))/2.
    return (np.abs( np.power(10., -0.4*(mag-e_mag)) - np.power(10., -0.4*(mag+e_mag)) ))/2.
  elif unit == 'Jy':
    print 'Jy: ', np.abs(zp*np.power(10.,-(mag+e_mag)/2.5) - zp*np.power(10.,-(mag-e_mag)/2.5) )/2.
    return np.abs(zp*np.power(10.,-(mag+e_mag)/2.5) - zp*np.power(10.,-(mag-e_mag)/2.5) )/2.
  elif unit == 'mJy':
    print 'mJy: ', e_flux * 1e-26
    return e_flux * 1e-26
  
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
def update_obs_with_error_Fnu(recarr):
  """
  This function updates the input record array with the error in Fnu. It iteratively calls 
  eval_error_F_nu.
  """
  n_row = len(recarr)
  for i in range(n_row):
    recarr['e_F_nu'] = eval_error_F_nu(recarr['mag'][i], recarr['e_mag'][i], recarr['zero_point'][i],
                                       recarr['F_nu'][i], recarr['e_F_nu'][i],
                                       recarr['unit'][i])
  return recarr
    
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
def update_obs_with_Fnu(recarr):
  """
  For each row in the input recarr, this functin calls convert_mag_to_flux, and updates the
  record array with the value of the flux
  """
  n_row = len(recarr)
  for i in range(n_row):
    recarr['F_nu'][i] = convert_mag_to_flux(recarr['mag'][i], recarr['zero_point'][i], recarr['F_nu'][i],
                               recarr['unit'][i])
  return recarr
  
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
def convert_mag_to_flux(mag, zp, flux, unit):
  """
  convert mag to flux based on this formula:
  log F_nu = -0.4 * mag + ZeroPoint 
  OR
  F_nu = ZeroPoint * 10^(-m/2.5)
  """
  if len(unit)==0:
    raise SystemExit, 'Error: convert_mag_to_flux: unit is empty'
  if unit == 'mag':
    return  np.power(10., -0.4 * mag + zp) * 1e-3
  elif unit == 'Jy':
    return zp * np.power(10., -0.4 * mag) * 1e-23       # Jy to cgs
  elif unit == 'mJy':
    return flux * 1e-26
  else:
    raise SystemExit, 'Error: convert_mag_to_flux: something is wrong with "unit" specification'

# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
def read_obs_mag(filename):
  """
  Read in the ascii file of the observed magnitudes, and retrun a numpy record array with 
  the column names given by the first header line of the filename
  """
  if not os.path.isfile(filename): raise SystemExit, '%s does not exist!' % (filename, )

  r = open(filename, 'r')
  lines = r.readlines()
  r.close()
  header = lines.pop(0).split()
  header_units = lines.pop(0).split()
  col_fmt = lines.pop(0).split()

  str_fmt = ''
  str_hdr = ''
  for i in range(len(header)):
    str_hdr += header[i] + ', '
    str_fmt += col_fmt[i] + ', '
  str_hdr = str_hdr[:-2]
  str_fmt = str_fmt[:-2]
  list_data = []
  for i in range(len(lines)): list_data.append(lines[i].split())
  
  obs_data = np.core.records.fromarrays(np.array(list_data).transpose(), names=str_hdr, formats=str_fmt)
  
  return obs_data

# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
def get_R_lambda_at_lambda(filename, lambda_A=5500):
  """
  Get the R_lambda = A_lambda/E(B-V) at a specified wavelength (in A)
  """
  if not os.path.isfile(filename): 
    message = 'Error: read: get_R_lambda_at_lambda: %s does not exist' % (filename, )
    raise SystemExit, message
  
  recarr = read_R_lambda(filename)
  wavelength = recarr['wavelength']
  if lambda_A < wavelength[0] or lambda_A > wavelength[-1]:
    print 'Error: read: get_R_lambda_at_lambda: lambda=%s not covered in the input file:%s' % (lambda_A, filename)
    if lambda_A < wavelength[0]:
      print 'Returns the first element of R_lambda'
      return recarr['R_lambda'][0]
    if lambda_A > wavelength[-1]:
      print 'Returns the last element of R_lambda'
      return recarr['R_lambda'][-1]
  else:
    ind = np.argmin(np.abs(wavelength - lambda_A))
    return recarr['R_lambda'][ind]

# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
def read_R_lambda(filename, from_lambda=0, to_lambda=1000, plot_name=None):
  """
  Read the file which gives R_lambda as a function of lambda (A)
  """
  if not os.path.isfile(filename): 
    message = 'Error: read: read_R_lambda: %s does not exist' % (filename, )
    raise SystemExit, message
  
  f = open(filename, 'r')
  lines = f.readlines()
  f.close()
  
  reference = lines[0]
  column_names = lines[2].split()
  units = lines[3].split()
  for i in range(4): out = lines.pop(0)
  n_lines = len(lines) 
  print ' - read: read_R_lambda: %s file has %s lines.' % (filename, n_lines)
  
  list_arr = []
  for i, line in enumerate(lines): 
    arr = line.split()
    arr = [float(arr[0]), float(arr[1])]
    list_arr.append(arr)
  
  #convert the list to a record array
  recarr = np.core.records.fromarrays(np.array(list_arr).transpose(), names=column_names, formats='f8,f8')
  
  if plot_name:
    fig = plt.figure(figsize=(6,4))
    ax = fig.add_subplot(111)
    plt.subplots_adjust(bottom=0.12, top=0.98)
    ax.set_xlabel(r'Wavelength $\lambda$ [$\AA$]')
    ax.set_xlim(from_lambda, to_lambda)
    ax.set_ylabel(r'$R_\lambda=A_\lambda/$E(B-V) [mag]')
    ax.set_ylim(0, 16)
    
    ax.plot(recarr['wavelength'], recarr['R_lambda'], lw=1.5, color='grey')
    ax.scatter(recarr['wavelength'], recarr['R_lambda'], s=20, marker='o', facecolor='blue', edgecolor='red')
    
    plt.savefig(plot_name)
    print ' - read: read_R_lambda: %s saved' % (plot_name, )
    plt.close()
  
  return recarr
  
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
def read_castellini(filename, plot_name=None):
  """
  This function reads receives a filename with model atmosphere data, and returns a dictionary with these keys: 
   - lambda in cm 
   - F_lambda
   You can retrieve the data in the dictionary like 

   import sed
   dic_model = sed.read_castellini(yourfile)
   x = dic_model['lambda']

  in you like to have a plot, then specify the name of the output plot in the argument list, like: plot_name='SED.png'

  Good luck.
  Ehsan.
  """
  file_obj = open(filename, 'r')
  lines = file_obj.readlines()
  file_obj.close()

  # Let's get some information out of the header
  header_1 = lines[0].split()
  Teff = float(header_1[1])
  logg = float(header_1[3])
  header_2 = lines[1].split()
  Z = float(header_2[1][1:-1])
  vturb = float(header_2[2][-1])
  print '\n - Model Parameters:'
  print '   Teff=%s [K], logg=%s, [M/H]=%s, vturb=%s [km/s] \n' % (Teff, logg, Z, vturb)

  n_lines = len(lines)

  column_1 = []
  column_2 = []
  column_3 = []
  column_4 = []
  column_5 = []
  column_6 = []

  for i_line, line in enumerate(lines):
    if i_line <= 1: continue      # skip the header

    row = line.split()
    column_1.append(int(row[1]))
    column_2.append(float(row[2]))
    column_3.append(float(row[3]))
    if 'E' not in row[4]:
      index = row[4].rfind('-')
      row[4] = row[4][:index] + 'E' + row[4][index:]
    column_4.append(float(row[4]))
  
    if 'E' not in row[5]:
      index = row[5].rfind('-')
      row[5] = row[5][:index] + 'E' + row[5][index:]
    column_5.append(float(row[5]))
    column_6.append(float(row[6]))
  
  # Name columns properly, and convert them to numpy array  
  line_id = np.asarray(column_1)
  lambda_nm = np.asarray(column_2)
  nu_Hz = np.asarray(column_3)
  Hnu = np.asarray(column_4)
  Hcont = np.asarray(column_5)
  Hnu_Hcont = np.asarray(column_6)

  c_light = 2.99792458e10
  lambda_cm = lambda_nm[:] * 1e-7
  lambda_A = lambda_nm * 10.
  log_lambda_cm = np.log10(lambda_cm)
  Flambda = 4.0 * Hnu * c_light / (lambda_cm**2.0)

  dic = {}
  dic['lambda_cm'] = lambda_cm
  dic['F_lambda'] = Flambda
  dic['Hnu'] = Hnu
  dic['Hnu_Hcont'] = Hnu_Hcont
  dic['Flambda'] = Flambda

  if plot_name:
    fig = plt.figure(figsize=(6,4.5))
    ax = fig.add_subplot(111)
    plt.subplots_adjust(left=0.25, right=0.98, bottom=0.105, top=0.98, hspace=0.02, wspace=0.02) 
    #ax.plot(log_lambda_cm, Flambda)
    ax.plot(lambda_cm, np.log10(lambda_cm * Flambda) )
    ax.set_xlim(np.min(lambda_cm), 0.001)
    ax.set_ylim(-20, 12)
    ax.set_xlabel(r'Wavelength $\lambda$ [$\AA$]')

    plt.savefig(plot_name)
    plt.close()

  return dic
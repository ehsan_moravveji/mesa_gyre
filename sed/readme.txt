
Name                    J-number MIPS       Teff  approx. SpT.    Metallicity
                        First release           lowest possible logg from the models

Jels Boulangier         J052043.86-692341.0  5750    F5Ib(e) LMC   -1.0 
Ana Escorza Santos      J050632.11-714229.7  6750    F3Iab   LMC   -1.0
Skralan Hosteaux        J003643.94-723722.1  7500    F0      SMC   -0.5 
Alexandar Mechev        J052315.15-695801.7  6000    F5      LMC   -1.0
Hoda Shariati           J010814.67-721306.2  6250    F6      SMC   -0.5
Christine Verbeke       J005529.48-715312.2  8500    A2      SMC   -1.5

-----------------------------------------------------------------------------
DATA:
=====
You have for every object the name of the object detected by Spitzer.
The name of the object is connected to the coordinates of the object. 
(e.g. J052043.86-692341.0 means alpha(2000) = 05h20m43.86s 
and delta_2000=-69d23m41.0s)

Where do you find additional measurements of your specific object:

- 2mass near-IR fluxes as well as Spitzer IR fluxes:

http://irsa.ipac.caltech.edu/data/SPITZER/SAGE/  for the LMC
http://irsa.ipac.caltech.edu/data/SPITZER/SAGE-SMC/ for the SMC
http://irsa.ipac.caltech.edu/cgi-bin/Gator/nph-scan?projshort=SPITZER
http://vizier.ast.cam.ac.uk/viz-bin/VizieR-2

- Optical Magnitudes:

* The UBVR CCD catalog of Massey, 2002
http://vizier.u-strasbg.fr/viz-bin/VizieR?-source=II/236 

* The photometric catalogue of the LMC of Zaritsky et al. (2004, AJ 128, 1606)
http://www.iop.org/EJ/article/1538-3881/128/4/1606/204189.text.html

I have a copy of the catalog in the following place:
/STER/hans/SAGE/FinalVersion/datazaritsky.txt
The catalog is big so do not copy it ! The file is accessible.


* The photometric survey of the SMC:
(Zaritsky et al., 2002, Astronomical Journal 123, 855)
http://vizier.u-strasbg.fr/viz-bin/VizieR?-source=J%2FAJ%2F123%2F855

* Use Visier to obtain additional photometric data of these objects
(additional IR data of the WISE satellite, The I band of the Denis catalog)

-------------------------------------------------------------------------
SED-Models:
-----------
 model SEDs of photospheres:

We use model atmospheres and model SED computed with the Kurucz suite of
programmes. The latest updates are downloadable from the website of
Fiorella Castelli. Take the models with metallicity -1.0 or -1.5.
e.g.:
http://wwwuser.oat.ts.astro.it/castelli/grids/gridm10k2odfnew/fm10k2tab.html

Look in the file to get more information e.g. on the units used.
- a Kurucz photospheric model The atmosphere model of your star
contains seven columns. The third one is wavelength in nanometer (nm),
the forth one is flux (H_nu) in erg/(sec cm^2 Hz sr). sr is steradian or the
solid angle of the star ( 1 radian = 57.2958 degrees = 206 264 arcsec;
1 steradian = 4.2510^10 arcsecond^2). The solid angle equals the
square of the star's radius, divided by the squared distance of the
star. Due to the big distances, stellar solid angles are very
small. By scaling your model to the observed photometry, you actually
correct for this solid angle, implying that the 1/sr dependence
cancels out.

-----------------------------------

For the determination of reddening E(B-V), you can only use the
non-infrared data. The infrared data can have a component coming from
the thermal emission of the circumstellar dust. Schematically, the
determination of the reddening goes as follows: Choose a value for
E(B-V) --> deredden the observed photometry --> scale model --> calculate
the squared distance between the model and the (non-infrared) data --> 
repeat until you found the E(B-V) giving the smallest chi-squared
difference.  This is your final E(B-V). This iteration scheme will be
repeated during the monte-carlo error simulation to get a good estimate on
the error of E(B-V)



import numpy as np
def function():
 f=open("data.dat",'r')
 header1=f.readline()
 flux=[]
 wavelength=[]
 for line in f:
  columns = line.split()
  flux.append(float(columns[1]))
  wavelength.append(float(columns[3]))
 flux = np.asarray(flux)
 wavelength=np.asarray(wavelength)
 f.close()
 return wavelength,flux


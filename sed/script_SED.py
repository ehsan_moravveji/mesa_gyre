
import read 
import fluxconversion as conv
import matplotlib.pylab as plt
import numpy as np

c_light = 3e10    # cgs

obs_file = 'obs_mags.txt'
obs_data = read.read_obs_mag(obs_file)
n_obs = len(obs_data)

obs_data = read.update_obs_with_Fnu(obs_data)
obs_data = read.update_obs_with_error_Fnu(obs_data)

obs_lambda_cm = obs_data['lambda_0'] * 1e-4   # cm

arr_F_lambda = c_light * obs_data['F_nu'] / obs_lambda_cm**2. 
#arr_e_F_lambda = obs_lambda_cm * obs_data['e_F_nu'] / (obs_data['F_nu']*np.log(10.0)) * c_light / obs_lambda_cm**2.0
F_lambda_upper = c_light * (obs_data['F_nu']+obs_data['e_F_nu']) / obs_lambda_cm**2.
F_lambda_lower = c_light * (obs_data['F_nu']-obs_data['e_F_nu']) / obs_lambda_cm**2.
arr_e_F_lambda = (F_lambda_upper - F_lambda_lower) / 2.
arr_err_yaxis  = obs_lambda_cm * arr_e_F_lambda

# model_file = 'fm10t6250g40k2odfnew.dat'
model_file = 'fm05t3500g00k2odfnew.dat'
dic_model = read.read_castellini(model_file)

model_F_lambda_at_J = read.get_Flambda_at_lambda(dic_model, 1.228e-4)
obs_F_lambda_at_J   = arr_F_lambda[3]
print 'Row[3]: ', obs_data[3]
print 'Model F_lambda at J band = ', model_F_lambda_at_J
scale_factor = obs_F_lambda_at_J / model_F_lambda_at_J 
print 'Scale Factor = ', scale_factor

# arr_scaled_F_lambda = arr_F_lambda * scale_factor

solid_angle  = 1e0
model_lambda = dic_model['lambda_cm']
model_flux   = dic_model['Flambda'] * solid_angle
model_flux_scaled = model_flux * scale_factor


# Plotting:
fig = plt.figure(figsize=(6,4), dpi=400)
ax = fig.add_subplot(111)
plt.subplots_adjust(left=0.12, right=0.97, bottom=0.12, top=0.97)

ax.plot(np.log10(model_lambda), np.log10(model_lambda*model_flux_scaled), label=r'Kurucz Model')
ax.scatter(np.log10(obs_lambda_cm), np.log10(obs_lambda_cm*arr_F_lambda), marker='o', 
	s=50, color='red', label=r'Scaled Observation')
ax.errorbar(np.log10(obs_lambda_cm), np.log10(obs_lambda_cm*arr_F_lambda), yerr=arr_err_yaxis)
ax.scatter(np.log10(obs_lambda_cm), np.log10(obs_lambda_cm*arr_F_lambda), marker='o', color='blue')

ax.set_xlim(-5.,-2)
ax.set_ylim(-20, -10)
ax.set_xlabel(r'Wavelength $\log\lambda$ [cm]')
ax.set_label(r'Logarithm Wavelength $\log\lambda$ [cm]')
ax.set_ylabel(r'$\log\lambda F_\lambda$ [cgs]')
leg = ax.legend(loc=4, fontsize='medium')
plot_name = 'plots/Obs-vs-Model.png'
plt.savefig(plot_name)
print 'Create: {0}'.format(plot_name)
plt.close()


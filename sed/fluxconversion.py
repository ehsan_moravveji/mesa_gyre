

def conv_mag_to_flux():
 import numpy as np
 import pylab as pl
 bmag=19.473
 wavelegth=[]
 b_zero_point=-22.353
 b_lambda=440e-7
 wavelength=[]
 c=3.e10
 wavelength.append(b_lambda)
 bflux=np.power(10,-0.4*bmag+b_zero_point)*c/b_lambda**2
 print bflux
 flux=[]
 flux.append(bflux)
 vmag=18.01
 v_zero_point=-22.419
 v_lambda=550e-7
 wavelength.append(v_lambda)
 vflux=np.power(10,-0.4*vmag+v_zero_point)*c/v_lambda**2
 print vflux
 flux.append(vflux)
 table=[[1.236e-4, 1594e-23,14.611],[1.662e-4,1024e-23,13.73],[0.791e-4,2499e-23,15.743],[2.145e-4,665e-23,1313],[1.228e-4,1595e-23,14.490]]
 for filters in table:
  wavelength.append(filters[0])
  flux.append(filters[1]*np.power(10, -filters[2]/2.5)*c/filters[0]**2)
 flux.append(1.44e-26*c/3.6e-4**2)
 flux.append(0.903e-26*c/4.5e-4**2)
 wavelength.append(3.6e-4)
 wavelength.append(4.5e-4)
 wavelength=np.asarray(wavelength)
 flux=np.asarray(flux)
 #pl.scatter(np.log10(wavelength),np.log10(wavelength*flux))
 #pl.show()
 return wavelength, flux

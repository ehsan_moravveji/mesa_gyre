
import os
from copy import deepcopy
import read
import matplotlib.pylab as plt
import numpy as np

c_SI = 2.99792458e8
dic_filter = {'name':'', 'lambda_A':0, 'mag':0, 'e_mag':0, 'flux':0, 'e_flux':0}

dic_B = {'name':'B', 'lambda_A':4400,  'mag':0, 'e_mag':0, 'flux':4.73e-10, 'e_flux':2.8781e-11}
dic_V = {'name':'V', 'lambda_A':5500,  'mag':0, 'e_mag':0, 'flux':2.75e-9, 'e_flux':8.27e-10}

dic_J = {'name':'J', 'lambda_A':12400, 'mag':0, 'e_mag':0, 'flux':4.61e-9, 'e_flux':6.29e-11}
dic_H = {'name':'H', 'lambda_A':16600, 'mag':0, 'e_mag':0, 'flux':3.76e-9, 'e_flux':5.123e-11}
dic_K = {'name':'K', 'lambda_A':21590, 'mag':0, 'e_mag':0, 'flux':1.64e-9, 'e_flux':2.46e-11}

dic_d1 = {'name':'Denis1', 'lambda_A':7910, 'mag':0, 'e_mag':0, 'flux':5.89e-9, 'e_flux':1.58e-10}
dic_d2 = {'name':'Denis2', 'lambda_A':12300, 'mag':0, 'e_mag':0, 'flux':4.60e-9, 'e_flux':2.78e-10}
dic_d3 = {'name':'Denis3', 'lambda_A':21500, 'mag':0, 'e_mag':0, 'flux':1.81e-9, 'e_flux':1.45e-10}

dic_w1 = {'name':'W1', 'lambda_A':3.35e4, 'mag':0, 'e_mag':0, 'flux':5.26e-10, 'e_flux':6.22e-12}
dic_w2 = {'name':'W2', 'lambda_A':4.6e4, 'mag':0, 'e_mag':0, 'flux':1.56e-10, 'e_flux':1.85e-12}
dic_w3 = {'name':'W3', 'lambda_A':1.16e5, 'mag':0, 'e_mag':0, 'flux':9.31e-11, 'e_flux':1.31e-12}
dic_w4 = {'name':'W4', 'lambda_A':2.21e5, 'mag':0, 'e_mag':0, 'flux':3.34e-11, 'e_flux':2.34e-12}

model_file = 'fm05t6250g05k2odfnew.dat'
#model_file = 'fm05t6000g00k2odfnew.dat'
#model_file = 'fm05t5750g00k2odfnew.dat'
#model_file = 'fm05t4500g00k2odfnew.dat'
#model_file = 'fm05t3500g00k2odfnew.dat'
dic_model = read.read_castellini(model_file)
dic_model['lambda_m'] = dic_model['lambda_cm'] * 1e-2
dic_model['Hnu_SI'] = dic_model['Hnu'] * 1e6
dic_model['Flambda_SI'] = 4. * c_SI * dic_model['Hnu_SI'] / (dic_model['lambda_m']**2.)

# get the scale factor
ind_lambda_J = np.argmin(np.abs( dic_model['lambda_m']-dic_J['lambda_A']*1e-10 ))
Flambda_J = dic_model['Flambda_SI'][ind_lambda_J]
scale_factor = Flambda_J / dic_J['flux']
print ' - Scale Factor = %s' % (scale_factor, )
dic_model['Flambda_SI_scaled'] = dic_model['Flambda_SI'] / scale_factor

R_lambda_file = 'Rv_3_1.dat'

list_filters = [dic_B, dic_V, dic_J, dic_H, dic_K, dic_d1, dic_d2, dic_d3, dic_w1, dic_w2, dic_w3, dic_w4]
flux_Gauss = []
# Create normal distributions of fluxes with size n_try
for i, dic in enumerate(list_filters):
  dic['lambda_cm'] = dic['lambda_A'] / 1e8
  dic['lambda_m']  = dic['lambda_A'] / 1e10
  dic['R_lambda']  = read.get_R_lambda_at_lambda(R_lambda_file, lambda_A=dic['lambda_A'])




# Now, go ahead with Monte Carlo evaluation of E(B-V) 

n_try = 1000   
n_E   = 200
arr_E_B_V = np.linspace(0.5, 1.5, num=n_E)
for i, dic in enumerate(list_filters):
  flux_Gauss.append(np.random.normal(loc=dic['flux'], scale=dic['e_flux'], size=(n_try,) ))

arr_min_chisq = []
arr_min_E_B_V = []

for i_try in range(n_try):
  
  chisq_for_E_B_V = []
  for i_E in range(n_E):
    E_B_V = arr_E_B_V[i_E]
    sum_deviation   = 0.0 
     
    for i_filter, dic in enumerate(list_filters): 
      fn = dic['name']   # filter name
      #if fn != 'B' or fn != 'V' or fn != 'H' or fn != 'K': continue
      #print dic['name']
        
      A_lambda = E_B_V * dic['R_lambda']
      F_obs_err    = dic['e_flux']
      F_obs_uncorr = flux_Gauss[i_filter][i_try]
      F_obs_corr   = F_obs_uncorr * np.power(10.0, A_lambda/2.5)
      F_model      = read.get_Flambda_at_lambda(dic_model, lambda_cm=dic['lambda_cm'], cgs=False, SI=True) 
      F_model_scaled  = F_model / scale_factor
      sum_deviation += (F_obs_corr - F_model_scaled)**2. / F_obs_err**2.0
      #print i_try, i_E, i_filter, (F_obs_corr - F_model_scaled)**2. / F_obs_err**2.0, sum_deviation
    chisq_for_E_B_V.append(sum_deviation / (12. - 2.))
  
  chisq_for_E_B_V = np.asarray(chisq_for_E_B_V)  
  ind_min_chisq = np.argmin(chisq_for_E_B_V)
  min_chisq     = chisq_for_E_B_V[ind_min_chisq]
  min_E_B_V     = arr_E_B_V[ind_min_chisq]
  #print i_try, min_chisq, min_E_B_V, ind_min_chisq
  
  arr_min_chisq.append(min_chisq)
  arr_min_E_B_V.append(min_E_B_V)  

arr_min_chisq = np.array(arr_min_chisq)
arr_min_E_B_V = np.array(arr_min_E_B_V)

best_E_B_V = np.mean(arr_min_E_B_V)
std_E_B_V  = np.std(arr_min_E_B_V)
print '\n - Result: Best E(B-V)=%s, and STD E(B-V)=%s\n' % (best_E_B_V, std_E_B_V)

# now, correct the fluxes, and re-scale the model
for i_dic, dic in enumerate(list_filters):
    A_lambda = best_E_B_V * dic['R_lambda']
    F_obs_uncorr = dic['flux']
    dic['flux_corr']   = F_obs_uncorr * np.power(10.0, A_lambda/2.5)

scale_factor = dic_model['Flambda_SI_scaled'][ind_lambda_J] / dic_J['flux_corr']
dic_model['Flambda_SI_scaled'] = dic_model['Flambda_SI_scaled'] / scale_factor
print ' Re-scaled flux = %s' % (scale_factor, )   

#print F_obs_uncorr, F_obs_err, F_obs_corr, F_model, F_model_scaled
#print (F_obs_corr - F_model_scaled)**2. / F_obs_err**2.0, sum_deviation

# Plottings

if True:
  fig = plt.figure(figsize=(6,4))
  ax = fig.add_subplot(111)
  plt.subplots_adjust()
  ax.scatter(arr_min_E_B_V, arr_min_chisq, s=20)
  ax.set_xlim(0.5,1.5)
  plt.savefig('plots/MC-Chisq-vs-E(B-V).pdf')
  plt.close()

if True:
  fig = plt.figure(figsize=(6,4))
  plt.subplots_adjust(left=0.12, bottom=0.12)
  ax1  = fig.add_subplot(111)
  ax1.set_xlim(-7.,-5)
  ax1.set_xlabel(r'Wavelength $\log \lambda$ [m]')
  ax1.plot(np.log10(dic_model['lambda_m']), dic_model['Flambda_SI_scaled'], color='black')
  list_flux_orig = []
  list_flux_corr = []
  list_lambdas   = []
  list_flux_errs = []
  for i_dic, dic in enumerate(list_filters):
    list_lambdas.append(dic['lambda_m'])
    list_flux_orig.append(dic['flux'])
    list_flux_corr.append(dic['flux_corr'])
    list_flux_errs.append(dic['e_flux'])
  list_lambdas = np.array(list_lambdas); list_flux_errs = np.array(list_flux_errs)
  list_flux_orig = np.array(list_flux_orig); list_flux_corr = np.array(list_flux_corr)
  list_log_lambda_Flambda_orig = np.log10(list_lambdas * list_flux_orig)
  list_log_lambda_Flambda_corr = np.log10(list_lambdas * list_flux_corr)
  
  ax1.errorbar(np.log10(list_lambdas), list_flux_orig, yerr=list_flux_errs, linestyle='None',
               color='red', ecolor='red', marker='s', ms=5, label=r'Observed Fluxes')
  ax1.errorbar(np.log10(list_lambdas), list_flux_corr, yerr=list_flux_errs, linestyle='None',
               color='blue', ecolor='blue', marker='o', ms=5, label=r'Corrected Fluxes')
  leg = ax1.legend(loc=1)

  ax1.scatter([np.log10(dic_J['lambda_m'])], [dic_J['flux']], s=40, color='black', marker='o')
    
  plt.savefig('plots/MC-Obs-Model-'+model_file[:-12]+'.pdf')
  plt.close()
  
  # 2nd plot
  fig = plt.figure(figsize=(6,4))
  plt.subplots_adjust(left=0.13,bottom=0.12, hspace=0.08, wspace=0.08)
  ax1  = fig.add_subplot(111)
  ax1.set_xlim(-7.2,-3.6)
  ax1.set_xlabel(r'Wavelength $\log\lambda$ [m]')
  ax1.set_ylim(-17, -13)
  ax1.set_ylabel(r'$\lambda F_\lambda$ [SI]')
  ax1.plot(np.log10(dic_model['lambda_m']), np.log10(dic_model['lambda_m']*dic_model['Flambda_SI_scaled']))
  #for i_dic, dic in enumerate(list_filters):
    #ax1.scatter([np.log10(dic['lambda_m'])], [np.log10(dic['lambda_m']*dic['flux'])], s=40, color='red', marker='s')
  #ax1.errorbar(np.log10(list_lambdas), list_flux_orig, yerr=list_flux_errs, linestyle='None',
               #color='red', ecolor='red', marker='s', ms=5, label=r'Observed Fluxes')
  ax1.errorbar(np.log10(list_lambdas), list_log_lambda_Flambda_orig, yerr=list_flux_errs, linestyle='None',
               color='red', ecolor='red', marker='s', ms=5, label=r'Observed Fluxes')
  ax1.errorbar(np.log10(list_lambdas), list_log_lambda_Flambda_corr, yerr=list_flux_errs, linestyle='None',
               color='blue', ecolor='blue', marker='o', ms=5, label=r'Corrected Fluxes')
  leg = ax1.legend(loc=1)
    
  plt.savefig('plots/MC-log-lambda-log-lambda-Flambda.pdf')
  plt.close()


if True:
  fig = plt.figure(figsize=(8,8))
  plt.subplots_adjust(hspace=0.08, wspace=0.08)
  ax1  = fig.add_subplot(2,2,1)
  ax2  = fig.add_subplot(2,2,2)
  ax3  = fig.add_subplot(2,2,3)
  ax4  = fig.add_subplot(2,2,4)

  ax1.hist(flux_Gauss[0])
  ax2.hist(flux_Gauss[1])
  ax3.hist(flux_Gauss[2])
  ax4.hist(flux_Gauss[3])

  plt.savefig('plots/Hist-Fluxes.pdf')
  plt.close()


if True:
  fig = plt.figure(figsize=(13,6))
  plt.subplots_adjust(bottom=0.10, top=0.98, left=0.06, right=0.98, hspace=0.10, wspace=0.10)
  ax1  = fig.add_subplot(121)
  ax2  = fig.add_subplot(122)

  ax1.hist(arr_min_chisq)
  ax1.set_xlabel(r'$\chi^2$')
 
  ax2.hist(arr_min_E_B_V, bins=20)
  txt_excess = r'E(B-V)=' + str(round(np.mean(arr_min_E_B_V),3)) + r' $\pm$ ' + str(round(np.std(arr_min_E_B_V),3)) 
  ax2.annotate(txt_excess, xy=(0.04, 0.95), xycoords='axes fraction', fontsize='medium')
  ax2.set_xlabel(r'E(B-V)')

  plt.savefig('plots/Hist-Chisq-E(B-V).pdf')
  plt.close()

if False:
  R_lambda = read.read_R_lambda(R_lambda_file, from_lambda=0, to_lambda=10000, plot_name = 'plots/R_lambda.pdf')
  


